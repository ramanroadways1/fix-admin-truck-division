<?php
require_once("./connect.php");

$date = date("Y-m-d");
$timestamp = date("Y-m-d H:i:s");

$pump_name = escapeString($conn,strtoupper($_POST['pump_name']));
$pump_code = escapeString($conn,strtoupper($_POST['pump_code']));
$branch = escapeString($conn,($_POST['branch']));
$fuel_company = escapeString($conn,($_POST['fuel_company']));
$type = escapeString($conn,($_POST['type']));

if($pump_name=='')
{
	AlertRightCornerError("PUMP name not found !");
	echo "<script>$('#add_btn').attr('disabled',false);</script>";
	exit();
}

if($pump_code=='')
{
	AlertRightCornerError("PUMP code not found !");
	echo "<script>$('#add_btn').attr('disabled',false);</script>";
	exit();
}

if($branch=='')
{
	AlertRightCornerError("Branch not found !");
	echo "<script>$('#add_btn').attr('disabled',false);</script>";
	exit();
}

if($fuel_company=='')
{
	AlertRightCornerError("Fuel company not found !");
	echo "<script>$('#add_btn').attr('disabled',false);</script>";
	exit();
}

if($type=='')
{
	AlertRightCornerError("Fuel Station Type not found !");
	echo "<script>$('#add_btn').attr('disabled',false);</script>";
	exit();
}

if($type=='MARKET')
{
	$chk_pump = Qry($conn,"SELECT id FROM diesel_pump WHERE code='$pump_code'");

	if(!$chk_pump){
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		AlertRightCornerError("Error while processing request !");
		echo "<script>$('#add_btn').attr('disabled',false);</script>";
		exit();
	}
}
else
{
	$chk_pump = Qry($conn,"SELECT id FROM dairy.diesel_pump_own WHERE code='$pump_code'");

	if(!$chk_pump){
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		AlertRightCornerError("Error while processing request !");
		echo "<script>$('#add_btn').attr('disabled',false);</script>";
		exit();
	}
}
	
if(numRows($chk_pump) > 0)
{
	AlertRightCornerError("Duplicate record found !");
	echo "<script>$('#add_btn').attr('disabled',false);</script>";
	exit();
}
	
StartCommit($conn);
$flag = true;

if($type=='MARKET')
{
	$chk_pump = Qry($conn,"INSERT INTO diesel_pump(branch,name,code,comp,timestamp,active) VALUES ('$branch','$pump_name','$pump_code',
	'$fuel_company','$timestamp','1')");

	if(!$chk_pump){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
}
else
{
	$chk_pump = Qry($conn,"INSERT INTO dairy.diesel_pump_own(branch,name,code,comp,timestamp,active) VALUES ('$branch','$pump_name','$pump_code',
	'$fuel_company','$timestamp','1')");

	if(!$chk_pump){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
}
	
if($flag)
{
	MySQLCommit($conn);
	closeConnection($conn);
	AlertRightCornerSuccess("Added Successfully !");
	echo "<script>LoadData();$('#add_btn').attr('disabled',false);</script>";
	exit();
}
else
{
	MySQLRollBack($conn);
	closeConnection($conn);
	AlertRightCornerError("Error while processing request !");
	exit();
}
?>