<?php
require_once("connect.php");

$date=date("Y-m-d");
$timestamp=date("Y-m-d H:i:s");

$from_loc_id = escapeString($conn,($_POST['from_loc_id']));
$to_loc_id = escapeString($conn,($_POST['to_loc_id']));
$from_loc = escapeString($conn,($_POST['from_loc']));
$to_loc = escapeString($conn,($_POST['to_loc']));

$chk_lane = Qry($conn,"SELECT id FROM dairy.fix_lane WHERE from_id='$from_loc_id' AND to_id='$to_loc_id'");

if(!$chk_lane){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Trip not Found.","./");
	exit();
}

if(numRows($chk_lane)>0)
{
	AlertError("Duplicate lane found !!");
	echo "<script>
		$('#button1').attr('disabled',false);
	</script>"; 
	exit();
}

$chk_from = Qry($conn,"SELECT name FROM station WHERE id='$from_loc_id'");

if(!$chk_from){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Trip not Found.","./");
	exit();
}

if(numRows($chk_from)==0)
{
	AlertError("From location found !!");
	echo "<script>
		$('#button1').attr('disabled',false);
	</script>"; 
	exit();
}

$chk_to = Qry($conn,"SELECT name FROM station WHERE id='$to_loc_id'");

if(!$chk_to){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Trip not Found.","./");
	exit();
}

if(numRows($chk_to)==0)
{
	AlertError("Destination location found !!");
	echo "<script>
		$('#button1').attr('disabled',false);
	</script>"; 
	exit();
}

$row_from = fetchArray($chk_from);
$row_to = fetchArray($chk_to);

if($row_from['name'] != $from_loc)
{
	AlertError("From location not verified !!");
	echo "<script>
		$('#button1').attr('disabled',false);
	</script>"; 
	exit();
}

if($row_to['name'] != $to_loc)
{
	AlertError("Destination location not verified !!");
	echo "<script>
		$('#button1').attr('disabled',false);
	</script>"; 
	exit();
}

StartCommit($conn);
$flag = true;

$insert_new_lane = Qry($conn,"INSERT INTO dairy.fix_lane(from_id,to_id,is_active,timestamp) VALUES ('$from_loc_id','$to_loc_id','1','$timestamp')");

if(!$insert_new_lane){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}	

if($flag)
{
	MySQLCommit($conn);
	closeConnection($conn);
	
	AlertRightCornerSuccess("Lane added successfully !");
	echo "<script>
		$('#from_loc').val('');
		$('#from_loc_id').val('');
		$('#to_loc').val('');
		$('#to_loc_id').val('');
		$('#button1').attr('disabled',false);
		LoadData();
	</script>"; 
	exit();
}
else
{
	MySQLRollBack($conn);
	closeConnection($conn);
	Redirect("Error While Processing Request.","./");
	exit();
}	
?>