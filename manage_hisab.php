<?php
include("header.php");

$chk = Qry($conn,"SELECT id FROM _access_control WHERE username='$_SESSION[ediary_fix_admin]' AND func_id=(SELECT id FROM 
_access_control_func_list WHERE session_role='8' AND func_name='Manage_Hisab') AND u_view='1'");
			  
if(numRows($chk)==0)
{
	echo "<script>window.location.href='./';</script>";
	exit();
}
?>
<div class="content-wrapper">
      <section class="content-header">
          <h1 style="font-size:16px;">Manage Hisab : </h1>
       </section>
       
	   <section class="content">
          <div class="row">
            <div class="col-xs-12">
			<div class="box">
                <div class="box-body">
				
				<div class="col-md-12">&nbsp;</div>
				
				<div class="col-md-12">
					<div class="row">
						<div class="form-group col-md-3">
							<label>Trip Number</label>
							<input autocomplete="off" oninput="this.value=this.value.replace(/[^0-9]/,'');" type="text" class="form-control" id="trip_no" />
						</div>
						
						<div class="form-group col-md-2">
							<?php if(!isMobile()) { echo "<label>&nbsp;</label><br />"; } ?>
							<button type="button" style="margin-top:3px" onclick="SearchHisab()" class="btn btn-sm btn-success <?php if(isMobile()) { echo "btn-block"; } ?>" id="search_add_btn"><i class="fa fa-search" aria-hidden="true"></i> &nbsp; Search Hisab</button>
						</div>
					</div>
				</div>
				
				<div class="col-md-12">&nbsp;</div>
				
				<div class="col-md-12 table-responsive" id="load_table">
              
				 </div> 
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

<div id="func_result"></div>  
<div id="div_modal_load"></div>  

<script>	
function SearchHisab()
{
	var trip_no = $('#trip_no').val();
	
	if(trip_no=='')
	{
		Swal.fire({icon: 'warning',html: '<font size=\'2\' color=\'black\'>Enter Trip number first !</font>',});
	}
	else
	{
		$('#loadicon').show();
			jQuery.ajax({
				url: "_load_hisab_manage.php",
				data: 'trip_no=' + trip_no,
				type: "POST",
				success: function(data) {
					$("#load_table").html(data);
					$('#example').DataTable({ 
						"destroy": true, //use for reinitialize datatable
					});
				},
				error: function() {}
		});
	}
}

function EditDA(trip_no,hisab_id)
{
	var da_per_day = $('#da_amount_per_day_'+trip_no).val();
	var da_from_date = $('#da_from_date_'+trip_no).val();
	var da_to_date = $('#da_to_date_'+trip_no).val();
	var tno = $('#tno_'+trip_no).val();
	var driver_code = $('#driver_code_'+trip_no).val();
	
	$('#loadicon').show();
		jQuery.ajax({
		url: "modal_da_edit.php",
		data: 'da_per_day=' + da_per_day + '&da_from_date=' + da_from_date + '&da_to_date=' + da_to_date + '&tno=' + tno + '&driver_code=' + driver_code + '&trip_no=' + trip_no + '&hisab_id=' + hisab_id,
		type: "POST",
		success: function(data) {
			$("#modal_da_result_load").html(data);
		},
		error: function() {}
	});
}

function ChangeHisab(id)
{
	$('#loadicon').show();
		jQuery.ajax({
		url: "modal_edit_hisab.php",
		data: 'id=' + id,
		type: "POST",
		success: function(data) {
			$("#div_modal_load").html(data);
		},
		error: function() {}
	});
}

function ViewHisab(trip_no,secret_key)
{
	$('#trip_no_1').val(trip_no);
	$('#secret_key').val(secret_key);
	
	$('#HisabViewForm')[0].submit();
}
</script>

<button id="da_modal_btn" style="display:none" data-toggle="modal" data-target="#ModalDaEdit"></button>

<form id="FormDaEdit" autocomplete="off"> 
<div class="modal fade" id="ModalDaEdit" role="dialog" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
		<div class="modal-content" style="">
			<div class="modal-header bg-primary">
				<span style="font-size:13px">Update DA : <span id="modal_update_span_header"></span>
			</div>
		<div class="modal-body" id="modal_da_result_load">
		</div>		
		<div class="modal-footer">
			<button type="submit" id="button_da_form" class="pull-left btn btn-sm btn-danger">Update DA</button>
			<button type="button" class="btn btn-sm btn-primary" onclick="modal_da_update_close_btn" data-dismiss="modal">Close</button>
		</div>
	 
      </div>
    </div>
  </div>
</form> 

<script>
$(document).ready(function (e) {
$("#FormDaEdit").on('submit',(function(e) {
$("#loadicon").show();
$("#button_da_form").attr("disabled",true);
e.preventDefault();
	$.ajax({
	url: "./update_da_amount.php",
	type: "POST",
	data:  new FormData(this),
	contentType: false,
	cache: false,
	processData:false,
	success: function(data){
		$("#result_modal").html(data);
	},
	error: function() 
	{} });}));});
</script>

<form action="https://rrpl.online/diary/hisab/hisab_view.php" id="HisabViewForm" method="POST" target="_blank">
	<input type="hidden" id="trip_no_1" name="trip_no">
	<input type="hidden" id="secret_key" name="secret_key">
</form>

<?php include("footer.php") ?>