<?php
require_once("./connect.php");

$item_name = escapeString($conn,strtoupper($_POST['item_name']));
$item_group = escapeString($conn,strtoupper($_POST['item_group']));
$timestamp = date("Y-m-d H:i:s");

StartCommit($conn);
$flag = true;

// $update = Qry($conn,"SELECT id FROM lritems WHERE iname='$item_name'");

// if(!$update)
// {
	// $flag = false;
	// errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
// }

$insert = Qry($conn,"INSERT INTO lritems(igroup,iname,username,itimestamp) VALUES ('$item_group','$item_name','$_SESSION[ediary_fix_admin]',
'$timestamp')");

if(!$insert)
{
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

$id1 = getInsertID($conn);

$update_code = Qry($conn,"UPDATE lritems SET icode='$id1' WHERE id='$id1'");

if(!$update_code)
{
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}
	
if($flag)
{
	MySQLCommit($conn);
	closeConnection($conn);
	
	AlertRightCornerSuccess("Added Successfully !");
	echo "<script>
		$('#item_name').val('');
		$('#item_group').val('');
	</script>";
	echo "<script>$('#add_btn').attr('disabled',false);</script>";
	exit();
}
else
{
	MySQLRollBack($conn);
	closeConnection($conn);
	AlertError("Error While Processing Request !");
	echo "<script>$('#add_btn').attr('disabled',false);</script>";
	exit();
}
?>