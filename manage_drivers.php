<?php include("header.php"); ?>

<?php
$chk = Qry($conn,"SELECT id FROM _access_control WHERE username='$_SESSION[ediary_fix_admin]' AND func_id=(SELECT id FROM 
_access_control_func_list WHERE session_role='8' AND func_name='Edit_Driver') AND u_view='1'");
			  
if(numRows($chk)==0)
{
	echo "<script>window.location.href='./';</script>";
	exit();
}
?>

<div class="content-wrapper">
      <section class="content-header">
          <h1 style="font-size:16px;">Manage Driver : </h1>
       </section>
       
	   <section class="content">
          <div class="row">
            <div class="col-xs-12">
			<div class="box">
                <div class="box-body">
				<div class="col-md-12">&nbsp;</div>
<?php
$chk_update = Qry($conn,"SELECT id FROM _access_control WHERE username='$_SESSION[ediary_fix_admin]' AND func_id=(SELECT id FROM 
_access_control_func_list WHERE session_role='8' AND func_name='Edit_Driver') AND u_update='1'");
			  
if(numRows($chk_update)>0)
{
?>	

<script>
$(function() {
		$("#driver_name").autocomplete({
		source: 'autofill/get_own_truck_driver.php',
		// appendTo: "#LaneRuleModal",
		select: function (event, ui) { 
              $('#driver_name').val(ui.item.value);   
              $('#driver_name2').val(ui.item.value);   
              $('#driver_id').val(ui.item.id);   
             return false;
		},
		change: function (event, ui) {
		if(!ui.item){
		    $(event.target).val("");
			Swal.fire({icon: 'warning',html: '<font size=\'2\' color=\'black\'>Driver does not exists !</font>',});
			$("#driver_name").val('');
			$("#driver_name2").val('');
			$("#driver_id").val('');
			$("#driver_name").focus();
		}}, 
		focus: function (event, ui){
			return false;}
});});

$(function() {
		$("#guarantor").autocomplete({
		source: 'autofill/get_guarantor_for_own_truck.php',
		// appendTo: "#LaneRuleModal",
		select: function (event, ui) { 
              $('#guarantor').val(ui.item.value);   
              $('#guarantor_code').val(ui.item.code);   
             return false;
		},
		change: function (event, ui) {
		if(!ui.item){
		    $(event.target).val("");
			Swal.fire({icon: 'warning',html: '<font size=\'2\' color=\'black\'>Guarantor driver does not exists !</font>',});
			$("#guarantor").val('');
			$("#guarantor_code").val('');
			$("#guarantor").focus();
		}}, 
		focus: function (event, ui){
			return false;}
});});
</script>
		
<form action="#" method="POST" id="Form1" autocomplete="off" style="font-size:12px">
			
				<div class="col-md-8">
					
					<div class="row">
						 
						<div class="form-group col-md-6">
							<label>Search Driver <font color="red"><sup>*</sup></font></label>
							<input required autocomplete="off" oninput="this.value=this.value.replace(/[^A-Z a-z0-9]/,'')" type="text" class="form-control" name="driver_name" id="driver_name" />
						</div>
						
						<input type="hidden" name="driver_id" id="driver_id">
						
						<div class="form-group col-md-6">
							<?php if(!isMobile()) { echo "<label>&nbsp;</label><br />"; } ?>
							<button type="button" onclick="SearchDriver()" class="btn btn-sm btn-success <?php if(isMobile()) { echo "btn-block"; } ?>" id="search_btn"><i class="fa fa-search" aria-hidden="true"></i> &nbsp; Search</button>
						</div>
					</div>
					
				<div class="row">	
				
						<div class="form-group col-md-4">
							<label>Mobile <font color="red"><sup>*</sup></font></label>
							<input required autocomplete="off" oninput="this.value=this.value.replace(/[^A-Z0-9]/,'')" type="text" class="form-control" name="mobile1" id="mobile1" />
						</div>
						
						<div class="form-group col-md-4">
							<label>Alternate Mobile </label>
							<input autocomplete="off" oninput="this.value=this.value.replace(/[^A-Z0-9]/,'')" type="text" class="form-control" name="mobile2" id="mobile2" />
						</div>
						
						<div class="form-group col-md-4">
							<label>Driver Code <font color="red"><sup>*</sup></font></label>
							<input required autocomplete="off" readonly oninput="this.value=this.value.replace(/[^0-9]/,'')" type="text" class="form-control" name="driver_code" id="driver_code" />
						</div>
						
						<div class="form-group col-md-6">
							<label>Driver Name <font color="red"><sup>*</sup></font></label>
							<input required autocomplete="off" oninput="this.value=this.value.replace(/[^A-Z a-z0-9]/,'')" type="text" class="form-control" name="driver_name2" id="driver_name2" />
						</div>
						
						<div class="form-group col-md-6">
							<label>Guarantor <font color="red"><sup>*</sup></font></label>
							<input required autocomplete="off" oninput="this.value=this.value.replace(/[^A-Z a-z0-9]/,'')" type="text" class="form-control" name="guarantor" id="guarantor" />
						</div>
						
						<input type="hidden" name="guarantor_code" id="guarantor_code">
	
<script>	
function FuncChk()
{		
	var ckbox = $('#ac_checkbox');
	
	if (ckbox.is(':checked')) {
		$('#ac_holder').attr('readonly',false);
		$('#ac_no').attr('readonly',false);
		$('#bank_name').attr('readonly',false);
		$('#ifsc_code').attr('readonly',false);
	
	}else {
		$('#ac_holder').attr('readonly',true);
		$('#ac_no').attr('readonly',true);
		$('#bank_name').attr('readonly',true);
		$('#ifsc_code').attr('readonly',true);
	}
}
</script>
	
						<div class="form-group col-md-12">
							<div class="bg-gray col-md-12">
								<label>Update A/C details ? &nbsp; <input type="checkbox" onchange="FuncChk()" id="ac_checkbox" name="ac_update" /> </label>
							</div>
						</div>
						
						<div class="form-group col-md-6">
							<label>A/c holder <font color="red"><sup>*</sup></font></label>
							<input required readonly autocomplete="off" oninput="this.value=this.value.replace(/[^A-Z a-z0-9]/,'')" type="text" class="form-control" name="ac_holder" id="ac_holder" />
						</div>
						
						<div class="form-group col-md-6">
							<label>A/c Number <font color="red"><sup>*</sup></font></label>
							<input required readonly autocomplete="off" oninput="this.value=this.value.replace(/[^A-Z a-z0-9]/,'')" type="text" class="form-control" name="ac_no" id="ac_no" />
						</div>
						
						<div class="form-group col-md-6">
							<label>Bank Name <font color="red"><sup>*</sup></font></label>
							<input required readonly autocomplete="off" oninput="this.value=this.value.replace(/[^A-Z a-z0-9]/,'')" type="text" class="form-control" name="bank_name" id="bank_name" />
						</div>
						
						<div class="form-group col-md-6">
							<label>IFSC Code <font color="red"><sup>*</sup></font></label>
							<input required readonly autocomplete="off" oninput="this.value=this.value.replace(/[^A-Z a-z0-9]/,'')" type="text" class="form-control" name="ifsc_code" id="ifsc_code" />
						</div>
						
						<div class="form-group col-md-6">
							<button disabled style="display:none" type="submit" class="btn btn-sm btn-success <?php if(isMobile()) { echo "btn-block"; } ?>" id="submit_btn"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> &nbsp; Update</button>
						</div>	
						
					</div>
				</div>
</form>				
			<?php
}
?>			
	<div class="col-md-12">&nbsp;</div>
				
				<div class="col-md-12 table-responsive" id="load_table">
				</div> 
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

<div id="func_result"></div>  
<div id="load_modal_div"></div>  


<script type="text/javascript">
$(document).ready(function (e) {
$("#Form1").on('submit',(function(e) {
$("#loadicon").show();
$("#submit_btn").attr("disabled", true);
e.preventDefault();
	$.ajax({
	url: "./save_edit_driver_save.php",
	type: "POST",
	data:  new FormData(this),
	contentType: false,
	cache: false,
	processData:false,
	success: function(data){
		$("#func_result").html(data);
	},
	error: function() 
	{} });}));});
	
function SearchDriver()
{
	var driver_id = $('#driver_id').val();
	
	if(driver_id=='')
	{
		Swal.fire({icon: 'warning',html: '<font size=\'2\' color=\'black\'>Select driver first !</font>',});
	}
	else
	{
		$('#search_btn').attr('disabled',true);
		$("#loadicon").show();
		jQuery.ajax({
			url: "load_driver_by_id.php",
			data: 'driver_id=' + driver_id,
			type: "POST",
			success: function(data) {
			$("#func_result").html(data);
		},
		error: function() {}
		});
	}
}	
</script>
 
<?php include("footer.php") ?>