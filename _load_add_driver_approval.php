<?php
require_once("connect.php");
?>

			<table id="example" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th>#</th>
                       <th>Name</th>
						<th>Mobile</th>
						<th>LicenseNo</th>
						<th>AadharNo</th>
						<th>Timestamp</th>
						<th>Branch</th>
						<th>A/c details</th>
						<th>Driver_Photo</th>
						<th>License_Copy</th>
						<th>Aaadhar_Copy</th>
						<th>Approve</th>
						<th>Reject</th>
                      </tr>
                    </thead>
                    <tbody>
	<?php
	$get_approvals = Qry($conn,"SELECT * FROM dairy.driver_temp WHERE supervisor_timestamp IS NOT NULL AND supervisor_timestamp!=0");
	
	if(!$get_approvals){
		AlertErrorTopRight("Error while processing request !");
		echo getMySQLError($conn);
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		exit();
	}
	
	if(numRows($get_approvals)==0)
	{
		echo "<tr>
			<td colspan='13'>No record found !</td>
			<td style='display: none'></td>
			<td style='display: none'></td>
			<td style='display: none'></td>
			<td style='display: none'></td>
			<td style='display: none'></td>
			<td style='display: none'></td>
			<td style='display: none'></td>
			<td style='display: none'></td>
			<td style='display: none'></td>
			<td style='display: none'></td>
			<td style='display: none'></td>
			<td style='display: none'></td>
			<td style='display: none'></td>
		</tr>";
	}
	else
	{
		$i=1;
		while($row = fetchArray($get_approvals))
		{
			$timestamp = date("d-m-y h:i A",strtotime($row['timestamp']));
			
			if($row['ac_holder']!=''){
				$ac_details="Ac Holder: $row[ac_holder]<br>
				Ac No: $row[ac_no]<br>
				Bank: $row[bank]<br>
				Ifsc: $row[ifsc]";
			}
			else{
				$ac_details="";
			}
		
			echo "<tr>
				<td>$i</td>
				<td>$row[name]</td>
				<td>$row[mobile]</td>
				<td>$row[lic]</td>
				<td>$row[aadhar_no]</td>
				<td>$timestamp</td>
				<td>$row[branch]</td>
				<td>$ac_details</td>
				
			<input type='hidden' id='driver_name_$row[id]' value='$row[name]'>
			<input type='hidden' id='d_photo_$row[id]' value='$row[driver_photo]'>
			<input type='hidden' id='d_lic_front_$row[id]' value='$row[lic_front]'>
			<input type='hidden' id='d_lic_rear_$row[id]' value='$row[lic_rear]'>
			<input type='hidden' id='d_aadhar_front_$row[id]' value='$row[aadhar_front]'>
			<input type='hidden' id='d_aadhar_rear_$row[id]' value='$row[aadhar_rear]'>
			
			<td><button type='button' class='btn btn-xs btn-primary' onclick='ViewFile(\"PHOTO\",$row[id])'>View Photo</button></td>
			<td>
				<button type='button' class='btn btn-xs btn-primary' onclick='ViewFile(\"LIC_FRONT\",$row[id])'>Lic-Front</button>
				&nbsp; &nbsp;
				<button type='button' class='btn btn-xs btn-primary' onclick='ViewFile(\"LIC_REAR\",$row[id])'>Lic-Rear</button>
			</td>
			<td>
				<button type='button' class='btn btn-xs btn-primary' onclick='ViewFile(\"AADHAR_FRONT\",$row[id])'>Aadhar-Front</button>
				&nbsp; &nbsp;
				<button type='button' class='btn btn-xs btn-primary' onclick='ViewFile(\"AADHAR_REAR\",$row[id])'>Aadhar-Rear</button>
			</td>
			<td><button type='button' id='approve_btn_$row[id]' class='btn_allow btn btn-xs btn-success' onclick='Approve($row[id])'><i class='fa fa-thumbs-o-up' aria-hidden='true'></i> Approve</button></td>
			<td><button type='button' id='reject_btn_$row[id]' class='btn_allow btn btn-xs btn-danger' onclick='Reject($row[id])'><i class='fa fa-thumbs-o-down' aria-hidden='true'></i> Reject</button></td>
		</tr>";
		$i++;	
		}
	}
	?>	
                    </tbody>
                  </table>

<script>				  
$("#loadicon").fadeOut('slow');
</script>	

<?php
$chk_update = Qry($conn,"SELECT id FROM _access_control WHERE username='$_SESSION[ediary_fix_admin]' AND func_id=(SELECT id FROM 
_access_control_func_list WHERE session_role='8' AND func_name='Driver_Approval') AND u_update='1'");
			  
if(numRows($chk_update)>0)
{
	echo "<script>
		$('.btn_allow').attr('disabled',false);
	</script>";
}
?>	