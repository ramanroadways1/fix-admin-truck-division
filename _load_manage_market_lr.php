<?php
require_once 'connect.php';

$lrno = escapeString($conn,($_POST['lrno']));
$tno = escapeString($conn,($_POST['tno']));

$from_date = date('Y-m-d', strtotime(escapeString($conn,$_POST['duration']), strtotime(date("Y-m-d"))));
$to_date = date("Y-m-d");

if($lrno!='')
{
	$sql = Qry($conn,"SELECT f.frno,f.company,f.branch,f.date,f.truck_no,f.lrno,f.fstation,f.tstation,f.consignor,f.consignee,f.wt12,f.weight 
	FROM freight_form_lr AS f 
	WHERE f.lrno='$lrno' AND f.frno like '___F%' GROUP BY f.frno");
}
else if($tno!='')
{
	$sql = Qry($conn,"SELECT f.frno,f.company,f.branch,f.date,f.truck_no,f.lrno,f.fstation,f.tstation,f.consignor,f.consignee,f.wt12,f.weight 
	FROM freight_form_lr AS f 
	WHERE f.date BETWEEN '$from_date' AND '$to_date' AND f.truck_no='$tno' GROUP BY f.frno");
}
else
{
	AlertRightCornerError("Invalid inputs !");
	exit();
}

if(!$sql){
	echo getMySQLError($conn);
	errorLog(getMySQLError($conn),$conn,$page_url,__LINE__);
	exit();
}
	
if(numRows($sql)==0)
{
	AlertRightCornerError("No record found !");
	exit();
}
	?>
	<table id="example" class="table table-bordered table-striped" style="font-size:13px;">
        <thead>
		<tr>
			<th>#SN</th>
			<th>Vou_No</th>
			<th>Company</th>
			<th>Branch</th>
			<th>LR_Date</th>
			<th>Vehicle_No</th>
			<th>LR_No</th>
			<th>Trip</th>
			<th>Party</th>
			<th>Weight</th>
			<th>#Edit</th>
			<th>#Delete</th>
		</tr>
		</thead>
    <tbody id=""> 
	
<?php
$sn=1;

while($row = fetchArray($sql))
{	
		$lr_date = date('d-m-y', strtotime($row['date']));
		
		echo "<tr>	
			<td>$sn</td>
			<td>$row[frno]</td>
			<td>$row[company]</td>
			<td>$row[branch]</td>
			<td>$lr_date</td>
			<td>$row[truck_no]</td>
			<td>$row[lrno]</td>
			<td>
				From: $row[fstation]
				<br>
				To: $row[tstation]
			</td>
			<td>
				Consignor: $row[consignor]
				<br>
				Consignee: $row[consignee]
			</td>
			<td>
				Actual: $row[wt12]
				<br>
				Charge: $row[weight]
			</td>
			<td><button class='btn edit_btn_1 btn-xs btn-primary' type='button' onclick=EditVoucher('$row[frno]')><i class='fa fa-pencil-square-o' aria-hidden='true'></i></button></td>
			<td><button class='btn delete_btn_1 btn-xs btn-danger' type='button' onclick=DeleteVoucher('$row[frno]')><i class='fa fa-trash' aria-hidden='true'></i></button></td>
		</tr>";
		
$sn++;		
}
	echo "</tbody>
</table>";

$chk_update = Qry($conn,"SELECT id FROM _access_control WHERE username='$_SESSION[ediary_fix_admin]' AND func_id=(SELECT id FROM 
_access_control_func_list WHERE session_role='8' AND func_name='Manage_Market_LR') AND u_update='1'");
			  
if(numRows($chk_update)==0)
{
	echo "<script>
		$('.edit_btn_1').attr('disabled',true);
		$('.edit_btn_1').attr('onclick','');
	</script>";
}

$chk_delete = Qry($conn,"SELECT id FROM _access_control WHERE username='$_SESSION[ediary_fix_admin]' AND func_id=(SELECT id FROM 
_access_control_func_list WHERE session_role='8' AND func_name='Manage_Market_LR') AND u_delete='1'");
			  
if(numRows($chk_delete)==0)
{
	echo "<script>
		$('.delete_btn_1').attr('disabled',true);
		$('.delete_btn_1').attr('onclick','');
	</script>";
}
?>
	
<script> 
	$("#loadicon").fadeOut('slow');
</script> 
