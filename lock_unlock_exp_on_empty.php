<?php
require_once 'connect.php';

$date = date("Y-m-d");
$timestamp = date("Y-m-d H:i:s");

$id=escapeString($conn,(trim($_POST['id'])));
$type=escapeString($conn,(trim($_POST['type'])));

StartCommit($conn);
$flag = true;

$update = Qry($conn,"UPDATE dairy.exp_head SET lock_on_empty='$type' WHERE id='$id'");

if(!$update){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

// $insert_role = Qry($conn,"INSERT INTO _access_control(session_role,username,func_id,timestamp) SELECT '8','$username',id,'$timestamp' FROM 
// _access_control_func_list WHERE session_role='8'");

// if(!$insert_role){
	// $flag = false;
	// errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
// }

if($flag)
{
	MySQLCommit($conn);
	closeConnection($conn);
	// AlertRightCornerSuccess("User successfully added !");
	echo "<script>LoadTable();</script>";
	exit();
}
else
{
	MySQLRollBack($conn);
	closeConnection($conn);
	AlertErrorTopRight("Error while processing request !");
	exit();
}	
?>