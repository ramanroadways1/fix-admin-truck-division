<?php
require_once("connect.php");

$id = escapeString($conn,$_POST['id']);

$get_data = Qry($conn,"SELECT trip_id,trans_id,vou_id,tno,amount,date,branch,narration,timestamp FROM dairy.freight_rcvd WHERE id='$id'");

if(!$get_data)
{
	AlertErrorTopRight("Error while processing request !");
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	exit();
}

if(numRows($get_data) == 0)
{
	AlertErrorTopRight("Entry not found !");
	exit();
}

$row = fetchArray($get_data);

$tno = $row['tno'];
$trip_id = $row['trip_id'];
$trans_id = $row['trans_id'];
$amount = $row['amount'];
$vou_id = $row['vou_id'];
$trans_date = $row['date'];
$narration = $row['narration'];
?>
<button id="modal_freight_coll_btn" style="display:none" data-toggle="modal" data-target="#FrtCollModal"></button>

<form id="EditFrt_CollectedForm" autocomplete="off" style="font-size:12px !important">   
<div class="modal fade" id="FrtCollModal" role="dialog" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-md">
      <div class="modal-content" style="">
		
		<div class="modal-header bg-primary">
			Edit Freight Collected at Branch :
		  </div>
		  
		<div class="modal-body">
		<div class="row">
			
			<div class="form-group col-md-6">
				<label>Truck No. <sup><font color="red">*</font></sup></label>
				<input style="font-size:12px !important" type="text" name="tno" value="<?php echo $tno; ?>" class="form-control" readonly required />
			</div>
				
			<div class="form-group col-md-6">
				<label>Amount. <sup><font color="red">*</font></sup></label>
				<input style="font-size:12px !important" min="1" value="<?php echo $amount; ?>" type="number" name="amount" class="form-control" required />
			</div>
			
			<input type="hidden" name="id" value="<?php echo $id; ?>" />
			
			<div class="form-group col-md-12">
				<label>Narration. <sup><font color="red">*</font></sup></label>
				<textarea style="font-size:12px !important" class="form-control" name="narration" required><?php echo $narration; ?></textarea>
			</div>
		</div>
        </div>
        <div class="modal-footer">
          <button type="submit" id="f_coll_button_submit" class="btn btn-sm btn-danger">Update</button>
          <button type="button" class="btn btn-sm btn-primary" id="hide_frt_coll" data-dismiss="modal">Close</button>
		</div>
      </div>
    </div>
  </div>
</form>

 <script> 
$('#modal_freight_coll_btn')[0].click();
$('#loadicon').fadeOut('slow');
</script>  
  
 <script type="text/javascript">
$(document).ready(function (e) {
$("#EditFrt_CollectedForm").on('submit',(function(e) {
$("#loadicon").show();
$("#f_coll_button_submit").attr("disabled", true);
e.preventDefault();
	$.ajax({
	url: "./save_freight_collected_edit.php",
	type: "POST",
	data:  new FormData(this),
	contentType: false,
	cache: false,
	processData:false,
	success: function(data)
	{
		$("#result_edits").html(data);
		$("#loadicon").hide();
	},
	error: function() 
	{} });}));});
</script> 