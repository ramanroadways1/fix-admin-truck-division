<?php
require_once 'connect.php';

$date = date("Y-m-d");
$timestamp = date("Y-m-d H:i:s");

$branch = escapeString($conn,strtoupper(trim($_POST['branch'])));
$driver_name = escapeString($conn,strtoupper(trim($_POST['driver_name'])));
$mobile = escapeString($conn,(trim($_POST['mobile'])));
$mobile2 = escapeString($conn,(trim($_POST['mobile2'])));
$lic_no = escapeString($conn,strtoupper(trim($_POST['lic_no'])));
$aadhar_no = escapeString($conn,strtoupper(trim($_POST['aadhar_no'])));
$addr = escapeString($conn,strtoupper(trim($_POST['addr'])));

if(!preg_match('/^[0-9\.]*$/', $mobile))
{
	AlertErrorTopRight("Invalid mobile number !");
	echo "<script>$('#add_btn').attr('disabled', false);</script>";
	exit();
}

if(strlen($mobile)!=10)
{
	AlertErrorTopRight("Check mobile number !");
	echo "<script>$('#add_btn').attr('disabled', false);</script>";
	exit();
}

if(strlen($mobile2)!=10 AND $mobile2!='')
{
	AlertErrorTopRight("Check alternate mobile number !");
	echo "<script>$('#add_btn').attr('disabled', false);</script>";
	exit();
}

if($mobile=="0000000000" || $mobile=="1234567890" || $mobile=="1234512345" || $mobile=="1234567899")
{
	AlertErrorTopRight("Check mobile number !");
	echo "<script>$('#add_btn').attr('disabled', false);</script>";
	exit();
}

if(($mobile2=="0000000000" || $mobile2=="1234567890" || $mobile2=="1234512345" || $mobile2=="1234567899") AND $mobile2!='')
{
	AlertErrorTopRight("Check alternate mobile number !");
	echo "<script>$('#add_btn').attr('disabled', false);</script>";
	exit();
}

$check_driver = Qry($conn,"SELECT id FROM mk_driver WHERE pan = '$lic_no' AND pan!='NA' AND pan!=''");

if(!$check_driver){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	AlertErrorTopRight("Error while processing request !");
	echo "<script>$('#add_btn').attr('disabled', false);</script>";
	exit();
}

if(numRows($check_driver)>0)
{
	AlertErrorTopRight("Duplicate license found !");
	echo "<script>$('#add_btn').attr('disabled', false);</script>";
	exit();
}

$source_lic_front = $_FILES['lic_front']['tmp_name'];
$source_lic_rear = $_FILES['lic_rear']['tmp_name'];
$source_aadhar_front = $_FILES['addhar_front']['tmp_name'];
$source_aadhar_rear = $_FILES['addhar_rear']['tmp_name'];

$valid_types = array("image/jpg", "image/jpeg", "image/bmp", "image/gif","image/png");

if(!in_array($_FILES['lic_front']['type'], $valid_types))
{
	AlertErrorTopRight("Only image upload allowed : Lic. Front !");
	echo "<script>$('#add_btn').attr('disabled', false);</script>";
	exit();
}

if(!in_array($_FILES['lic_rear']['type'], $valid_types))
{
	AlertErrorTopRight("Only image upload allowed : Lic. Rear !");
	echo "<script>$('#add_btn').attr('disabled', false);</script>";
	exit();
}

if(!in_array($_FILES['addhar_front']['type'], $valid_types))
{
	AlertErrorTopRight("Only image upload allowed : Aadhar Front !");
	echo "<script>$('#add_btn').attr('disabled', false);</script>";
	exit();
}

if(!in_array($_FILES['addhar_rear']['type'], $valid_types))
{
	AlertErrorTopRight("Only image upload allowed : Aadhar Rear !");
	echo "<script>$('#add_btn').attr('disabled', false);</script>";
	exit();
}

$fix_name = mt_rand().date('dmyhis');

$target_lic_front = "../b5aY6EZzK52NA8F/driver_pan/"."Front_".$fix_name.".".pathinfo($_FILES['lic_front']['name'],PATHINFO_EXTENSION);
$target_lic_rear = "../b5aY6EZzK52NA8F/driver_pan/"."Rear_".$fix_name.".".pathinfo($_FILES['lic_rear']['name'],PATHINFO_EXTENSION);
$target_aadhar_front = "../b5aY6EZzK52NA8F/driver_aadhar/"."Front_".$fix_name.".".pathinfo($_FILES['addhar_front']['name'],PATHINFO_EXTENSION);
$target_aadhar_rear = "../b5aY6EZzK52NA8F/driver_aadhar/"."Rear_".$fix_name.".".pathinfo($_FILES['addhar_rear']['name'],PATHINFO_EXTENSION);

$target_lic_front2 = "driver_pan/"."Front_".$fix_name.".".pathinfo($_FILES['lic_front']['name'],PATHINFO_EXTENSION);
$target_lic_rear2 = "driver_pan/"."Rear_".$fix_name.".".pathinfo($_FILES['lic_rear']['name'],PATHINFO_EXTENSION);
$target_aadhar_front2 = "driver_aadhar/"."Front_".$fix_name.".".pathinfo($_FILES['addhar_front']['name'],PATHINFO_EXTENSION);
$target_aadhar_rear2 = "driver_aadhar/"."Rear_".$fix_name.".".pathinfo($_FILES['addhar_rear']['name'],PATHINFO_EXTENSION);

StartCommit($conn);
$flag = true;

if(!move_uploaded_file($source_lic_front,$target_lic_front))
{
	$flag = false;
	errorLog("License front upload failed.",$conn,$page_name,__LINE__);
}

if(!move_uploaded_file($source_lic_rear,$target_lic_rear))
{
	$flag = false;
	errorLog("License front upload failed.",$conn,$page_name,__LINE__);
}

if(!move_uploaded_file($source_aadhar_front,$target_aadhar_front))
{
	$flag = false;
	errorLog("License front upload failed.",$conn,$page_name,__LINE__);
}

if(!move_uploaded_file($source_aadhar_rear,$target_aadhar_rear))
{
	$flag = false; 
	errorLog("License front upload failed.",$conn,$page_name,__LINE__);
}

$insert_driver = Qry($conn,"INSERT INTO mk_driver(name,mo1,mo2,pan,up5,dl_rear,aadhar_front,aadhar_rear,branch,branch_user,full,aadhar_no,timestamp) 
VALUES ('$driver_name','$mobile','$mobile2','$lic_no','$target_lic_front2','$target_lic_rear2','$target_aadhar_front2','$target_aadhar_rear2',
'$branch','Truck_Division_Admin','$addr','$aadhar_no','$timestamp')");

if(!$insert_driver){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

if($flag)
{
	MySQLCommit($conn);
	closeConnection($conn);
	AlertRightCornerSuccess("Driver successfully added !");
	echo "<script>
		$('#Form1')[0].reset();
		$('#driver_name').attr('readonly', false);
		$('#driver_addr').attr('readonly', false);
		$('#lic_fetch_btn').attr('disabled', false);
	</script>";
	exit();
}
else
{
	MySQLRollBack($conn);
	closeConnection($conn);
	AlertErrorTopRight("Error while processing request !");
	echo "<script>$('#add_btn').attr('disabled', false);</script>";
	exit();
}	
?>