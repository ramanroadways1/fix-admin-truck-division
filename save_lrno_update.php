<?php
require_once("./connect.php");

$id = escapeString($conn,($_POST['id']));
$vou_no = escapeString($conn,($_POST['vou_no']));
$lrno_old = escapeString($conn,strtoupper($_POST['lrno_old']));
$lrno = escapeString($conn,strtoupper($_POST['lrno_new']));
$con1_id = escapeString($conn,($_POST['con1_id']));
$ediary_trip_done = escapeString($conn,($_POST['ediary_trip_done']));
$ediary_trip_id = escapeString($conn,($_POST['ediary_trip_id']));
$market_veh_pod = escapeString($conn,($_POST['market_veh_pod']));
$own_veh_pod = escapeString($conn,($_POST['own_veh_pod']));
$is_coal_branch = escapeString($conn,($_POST['is_coal_branch']));

$date = date("Y-m-d");
$timestamp = date("Y-m-d H:i:s");

if(empty($con1_id))
{
	AlertErrorTopRight("Consignor not found !");
	echo "<script> $('#update_btn_modal').attr('disabled',false); </script>";
	exit();
}

if(empty($lrno_old))
{
	AlertErrorTopRight("Current LR number found !");
	echo "<script> $('#update_btn_modal').attr('disabled',false); </script>";
	exit();
}

if(empty($lrno))
{
	AlertErrorTopRight("Updated LR number found !");
	echo "<script> $('#update_btn_modal').attr('disabled',false); </script>";
	exit();
}

if(count(explode(' ', $lrno)) > 1)
{
	AlertErrorTopRight("Please check LR number !");
	echo "<script> $('#update_btn_modal').attr('disabled',false); </script>";
	exit();
}
	
if(strpos($lrno,'0')===0 AND $con1_id!='848')
{
	AlertErrorTopRight("Invalid LR number !");
	echo "<script> $('#update_btn_modal').attr('disabled',false); </script>";
	exit();
}
	
$chk_lr_no = Qry($conn,"SELECT id FROM lr_sample WHERE lrno='$lrno'");
	
if(!$chk_lr_no){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	AlertErrorTopRight("Error while Processing Request !");
	echo "<script> $('#update_btn_modal').attr('disabled',false); </script>";
	exit();
}
	
if(numRows($chk_lr_no) > 0)
{
	AlertErrorTopRight("Duplicate LR number : $lrno !");
	echo "<script> $('#update_btn_modal').attr('disabled',false); </script>";
	exit();
}
	
$chk_open_lr = Qry($conn,"SELECT id FROM open_lr WHERE party_id='$con1_id'");
	
if(!$chk_open_lr){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	AlertErrorTopRight("Error while Processing Request !");
	echo "<script> $('#update_btn_modal').attr('disabled',false); </script>";
	exit();
}
	
if(numRows($chk_open_lr) == 0)
{
	$get_bilty_id_current = Qry($conn,"SELECT bilty_id FROM lr_check WHERE lrno='$lrno_old'");
	
	if(!$get_bilty_id_current){
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		AlertErrorTopRight("Error while Processing Request !");
		echo "<script> $('#update_btn_modal').attr('disabled',false); </script>";
		exit();
	}
		
	if(numRows($get_bilty_id_current) == 0)
	{
		AlertErrorTopRight("LR not found !");
		echo "<script> $('#update_btn_modal').attr('disabled',false); </script>";
		exit();
	}
		
	$row_bilty_id_curr = fetchArray($get_bilty_id_current);
	
	$chk_lr_bank = Qry($conn,"SELECT id,branch,company,stock_left FROM lr_bank WHERE $lrno>=from_range AND $lrno<=to_range");
		
	if(!$chk_lr_bank){
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		AlertErrorTopRight("Error while Processing Request !");
		echo "<script> $('#update_btn_modal').attr('disabled',false); </script>";
		exit();
	}
	
	if(numRows($chk_lr_bank) == 0)
	{
		AlertErrorTopRight("LR stock not found !");
		echo "<script> $('#update_btn_modal').attr('disabled',false); </script>";
		exit();
	}
		
	if(numRows($chk_lr_bank) > 1)
	{
		AlertErrorTopRight("Multiple LR stock found !");
		echo "<script> $('#update_btn_modal').attr('disabled',false); </script>";
		exit();
	}
		
	$row_bilty_id_new = fetchArray($chk_lr_bank);
		
	$bilty_book_id = $row_bilty_id_new['id'];
		
	if($row_bilty_id_curr['bilty_id'] != $bilty_book_id)
	{
		AlertErrorTopRight("LR : $lrno does not belongs to old LR\'s stock !");
		echo "<script> $('#update_btn_modal').attr('disabled',false); </script>";
		exit();
	}
}
else
{
	$bilty_book_id = "0";
}

StartCommit($conn);
$flag = true;

if($ediary_trip_id!=0)
{
	$update_lrno = Qry($conn,"UPDATE dairy.trip SET lrno=REPLACE(lrno,'$lrno_old','$lrno') WHERE id='$ediary_trip_id'");
	
	if(!$update_lrno){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
	
	if(AffectedRows($conn))
	{
		$update_lrno_1 = Qry($conn,"UPDATE dairy.trip_final SET lrno=REPLACE(lrno,'$lrno_old','$lrno') WHERE trip_id='$ediary_trip_id'");
	
		if(!$update_lrno_1){
			$flag = false;
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		}
	}
}
	
$update_lrno_2 = Qry($conn,"UPDATE freight_form_lr SET lrno='$lrno' WHERE id='$id'");
		
if(!$update_lrno_2){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}
		
$update_lrno_3 = Qry($conn,"UPDATE lr_sample SET lrno='$lrno' WHERE lrno='$lrno_old'");
		
if(!$update_lrno_3){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

$update_lrno_4 = Qry($conn,"UPDATE lr_check SET lrno='$lrno' WHERE lrno='$lrno_old'");
		
if(!$update_lrno_4){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

if($market_veh_pod!=0 || $own_veh_pod!=0)
{
	$update_pod_lrno = Qry($conn,"UPDATE rcv_pod SET lrno='$lrno' WHERE lrno='$lrno_old'");
		
	if(!$update_pod_lrno){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
	
	$update_pod_lrno = Qry($conn,"UPDATE dairy.rcv_pod SET lrno='$lrno' WHERE lrno='$lrno_old'");
		
	if(!$update_pod_lrno){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
}

$update_rtgs_lrno = Qry($conn,"UPDATE rtgs_fm SET lrno=REPLACE(lrno,'$lrno_old','$lrno') WHERE fno='$vou_no'");
	
if(!$update_rtgs_lrno){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

$update_diesel_lrno = Qry($conn,"UPDATE diesel_fm SET lrno=REPLACE(lrno,'$lrno_old','$lrno') WHERE fno='$vou_no'");
	
if(!$update_diesel_lrno){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}
		
if($flag)
{
	MySQLCommit($conn);
	closeConnection($conn);
	AlertRightCornerSuccess("Updated Successfully !");
	echo "<script>$('#close_modal_btn')[0].click(); $('#ReloadPage')[0].submit();</script>";
	exit();
}
else
{
	MySQLRollBack($conn);
	closeConnection($conn);
	AlertErrorTopRight("Error while Processing Request !");
	echo "<script> $('#update_btn_modal').attr('disabled',false); </script>";
	exit();
}
?>