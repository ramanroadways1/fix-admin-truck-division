<?php include("header.php"); ?>

<?php
$chk = Qry($conn,"SELECT id FROM _access_control WHERE username='$_SESSION[ediary_fix_admin]' AND func_id=(SELECT id FROM 
_access_control_func_list WHERE session_role='8' AND func_name='LR_Bank') AND u_view='1'");
			  
if(numRows($chk)==0)
{
	echo "<script>window.location.href='./';</script>";
	exit();
}
?>

<div class="content-wrapper">
      <section class="content-header">
          <h1 style="font-size:16px;">LR Bank : </h1>
       </section>
       
	   <section class="content">
          <div class="row">
            <div class="col-xs-12">
			<div class="box">
                <div class="box-body">
	<div class="col-md-12">&nbsp;</div>			
<?php
$chk_insert = Qry($conn,"SELECT id FROM _access_control WHERE username='$_SESSION[ediary_fix_admin]' AND func_id=(SELECT id FROM 
_access_control_func_list WHERE session_role='8' AND func_name='LR_Bank') AND u_insert='1'");
			  
if(numRows($chk_insert)>0)
{

?>			
<form action="#" method="POST" id="Form1" autocomplete="off">
			
				<div class="col-md-12">
					
					<div class="row">
						
						<div class="form-group col-md-2">
							<label>Company <font color="red"><sup>*</sup></font></label>
							<select style='font-size:12px' class="form-control" name="company" required>
								<option style='font-size:12px' value="">--Select Company--</option>
								<option style='font-size:12px' value="RRPL">RRPL</option>
								<option style='font-size:12px' value="RAMAN_ROADWAYS">RAMAN_ROADWAYS</option>
							</select>
						</div>
						
						<div class="form-group col-md-3">
							<label>From series <font color="red"><sup>*</sup></font></label>
							<input autocomplete="off" oninput="this.value=this.value.replace(/[^0-9]/,'');CalculateStock()" type="text" class="form-control" name="from_series" id="from_series" />
						</div>
						
						<div class="form-group col-md-3">
							<label>To series <font color="red"><sup>*</sup> <span id="bilty_stock"></span></font></label>
							<input autocomplete="off" oninput="this.value=this.value.replace(/[^0-9]/,'');CalculateStock()" type="text" class="form-control" name="to_series" id="to_series" />
						</div>
						
						<div class="form-group col-md-2">
							<label>Branch <font color="red"><sup>*</sup></font></label>
							<select style='font-size:12px' onchange="Lock();" id="branch" class="form-control" name="branch" required>
							
							<option style='font-size:12px' value="">--Select Branch--</option>
							<?php
							$fetch_branch = Qry($conn,"SELECT username FROM rrpl_database.user WHERE role='2' ORDER BY username ASC");
							
							if(!$fetch_branch){
								errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
								Redirect("Error while Processing Request","./");
								exit();
							}
							
							if(numRows($fetch_branch)>0)
							{
								while($row_branch=fetchArray($fetch_branch))
								{
									echo "<option style='font-size:12px' value='$row_branch[username]'>$row_branch[username]</option>";
								}
							}
							?>
							</select>
						</div>
						
				<input type="hidden" name="bilty_stock1" id="bilty_stock1"/>
						
				<script>
				 function Lock()
				 {
					 if($("#from_series").val()=='' || $("#to_series").val()=='' || $("#bilty_stock1").val()<=0)
					 {
						  $("#from_series").val('');
						  $("#to_series").val('');
						  
						  $("#from_series").focus();
						  $("#branch").val('');
						  
						  $("#bilty_stock1").val('');
						  $("#bilty_stock").html('NA');
					 }
					 else
					 {
						$("#from_series").attr('readonly',true);
						$("#to_series").attr('readonly',true); 
					 }
				 }
				 
				 function CalculateStock()
				 {
					 $("#bilty_stock1").val('');
					 $("#bilty_stock").html('');
					 
					 var from1 = $("#from_series").val();
					 var to1 = $("#to_series").val();
					 
					 if(from1==''){
						 var from1 = 0;
					 }
					 else{
						 var from1 = Number($("#from_series").val());
					 }
					 
					 if(to1==''){
						 var to1 = 0;
					 }
					 else{
						 var to1 = Number($("#to_series").val());
					 }
					 
					$("#bilty_stock").html((Number(to1) - Number(from1))+1);
					$("#bilty_stock1").val((Number(to1) - Number(from1))+1);
					 
					 $("#branch").val('');
				 }
			 </script>
						
						<div class="form-group col-md-2">
							<?php if(!isMobile()) { echo "<label>&nbsp;</label><br />"; } ?>
							<button type="submit" class="btn btn-sm btn-success <?php if(isMobile()) { echo "btn-block"; } ?>" id="add_btn"><i class="fa fa-plus-square-o" aria-hidden="true"></i> &nbsp; Add Stock</button>
						</div>
					</div>
				</div>
</form>				
			<?php
}
?>			
	<div class="form-group col-md-12">
		<button type="button" onclick="ViewDb()" class="btn btn-danger btn-sm"><i class="fa fa-street-view" aria-hidden="true"></i> View Completed Stock</button>
	</div>
				
				<div class="col-md-12 table-responsive" id="load_table">
				</div> 
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

<div id="func_result"></div>  
<div id="load_modal_div"></div>  


<script type="text/javascript">
$(document).ready(function (e) {
$("#Form1").on('submit',(function(e) {
$("#loadicon").show();
$("#add_btn").attr("disabled", true);
e.preventDefault();
	$.ajax({
	url: "./save_lr_bank.php",
	type: "POST",
	data:  new FormData(this),
	contentType: false,
	cache: false,
	processData:false,
	success: function(data){
		$("#func_result").html(data);
	},
	error: function() 
	{} });}));});
</script>
 
<script>
function Delete1(id,stock,stock_left)
{
	$('#del_button'+id).attr('disabled',true);
				 
	if(stock == stock_left)
	{
		$("#loadicon").show();
			jQuery.ajax({
			url: "lr_bank_delete.php",
			data: 'id=' + id,
			type: "POST",
			success: function(data) {
				$("#result_data").html(data);
			},
			error: function() {}
		});
	}
	else
	{
		Swal.fire({icon: 'warning',html: '<font size=\'2\' color=\'black\'>Stock already in use !</font>',});
	}
}
			 
function Edit(branch,id,from_range,to_range,company)
{
	$('#edit_button'+id).attr('disabled',true);
	
	$("#loadicon").show();
			jQuery.ajax({
			url: "modal_lr_bank_edit.php",
			data: 'id=' + id,
			type: "POST",
			success: function(data) {
				$("#load_modal_div").html(data);
			},
			error: function() {}
		});
}

function ViewDb()
{
	$("#loadicon").show();
			jQuery.ajax({
			url: "modal_lr_bank_completed_stock.php",
			data: 'id=' + 'ok',
			type: "POST",
			success: function(data) {
				$("#load_modal_div").html(data);
				$('#example_db').DataTable({ 
					"destroy": true, //use for reinitialize datatable
				});
			},
			error: function() {}
		});
}
</script>
			 
<script>	
function LoadTable()
{
	$("#loadicon").show();
	jQuery.ajax({
		url: "_load_lr_bank.php",
		data: 'id=' + 'ok',
		type: "POST",
		success: function(data) {
			$("#load_table").html(data);
			$('#example').DataTable({ 
				"destroy": true, //use for reinitialize datatable
			});
		},
		error: function() {}
	});
}
 LoadTable();
</script>

<?php include("footer.php") ?>