<?php
require_once("./connect.php");

$stock_id = escapeString($conn,strtoupper($_POST['id']));

$date = date("Y-m-d");
$timestamp = date("Y-m-d H:i:s");

$get_lr_bank = Qry($conn,"SELECT branch,from_range,to_range,company,stock,stock_left FROM lr_bank WHERE id='$stock_id'");

if(!$get_lr_bank){
	AlertRightCornerError("Error while processing request !");
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	exit();
}
	
if(numRows($get_lr_bank)==0)
{
	AlertRightCornerError("Stock not found !");
	exit();
}

$row = fetchArray($get_lr_bank);

$from_range_ext = $row['from_range'];
$to_range_ext = $row['to_range'];
$company = $row['company'];
$branch_ext = $row['branch'];

$branch_new = escapeString($conn,strtoupper($_POST['branch']));
$from_new = escapeString($conn,strtoupper($_POST['from_series']));
$to_new = escapeString($conn,strtoupper($_POST['to_series']));

if(empty($branch_new))
{
	AlertRightCornerError("Branch not found !");
	exit();
}

if(empty($from_new) || empty($to_new))
{
	AlertRightCornerError("LR series not found !");
	exit();
}

$master_series = $from_range_ext."-".$to_range_ext;

$stock_new = ($to_new - $from_new)+1;

$next_range_from = $to_new+1;
$next_range_to = $to_range_ext;

if($next_range_to <= $next_range_from)
{
	AlertRightCornerError("Invalid Next Stock !");
	exit();
}

$stock_next = ($next_range_to - $next_range_from)+1;

$chk_for_current_records = Qry($conn,"SELECT branch,from_range,to_range FROM lr_bank WHERE id='$stock_id'");

if(!$chk_for_current_records)
{
	AlertRightCornerError("Error while processing request !");
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	exit();
}

$row_current_records = fetchArray($chk_for_current_records);

if($row_current_records['branch'] == $branch_new AND $row_current_records['from_range'] == $from_new AND $row_current_records['to_range'] == $to_new)
{
	AlertRightCornerError("Nothing to update !");
	echo "<script>$('#button_update_modal_confirm').attr('disabled',false);</script>";
	exit();
}

if($to_new <= $from_range_ext)
{
	AlertRightCornerError("Value must be grater than : $from_range_ext !");
	echo "<script>$('#button_update_modal_confirm').attr('disabled',false);</script>";
	exit();
}

if($to_new > $to_range_ext)
{
	AlertRightCornerError("Max Value is : $to_range_ext !");
	echo "<script>$('#button_update_modal_confirm').attr('disabled',false);</script>";
	exit();
}

$chk_for_ext_stock = Qry($conn,"SELECT DISTINCT branch FROM lr_check WHERE lrno>=$from_new and lrno<=$to_new AND lrno NOT LIKE '0%'");

if(!$chk_for_ext_stock)
{
	AlertRightCornerError("Error while processing request !");
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	exit();
}

$total_branches = numRows($chk_for_ext_stock);

if($total_branches > 1)
{
	$branch_name = array();
	
	while($row_check1 = fetchArray($chk_for_ext_stock))
	{
		$branch_name[] = $row_check1['branch'];
	}
	
	$branch_name = implode(",",$branch_name);
	
	AlertRightCornerError("Stock in use with multiple branches : $branch_name !");
	echo "<script>$('#button_update_modal_confirm').attr('disabled',false);</script>";
	exit();
}
else if($total_branches==1)
{
	$row_check1 = fetchArray($chk_for_ext_stock);
	
	if($row_check1['branch'] != $branch_new)
	{
		AlertRightCornerError("Stock in use with branch : $row_check1[branch] !");
		echo "<script>$('#button_update_modal_confirm').attr('disabled',false);</script>";
		exit();
	}
}

$get_total_used = Qry($conn,"SELECT id FROM lr_check WHERE lrno>=$from_new and lrno<=$to_new AND lrno NOT LIKE '0%'");

if(!$get_total_used)
{
	AlertRightCornerError("Error while processing request !");
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	exit();
}
	
$total_used = numRows($get_total_used);
$stock_left_new = $stock_new - $total_used;
	
StartCommit($conn);
$flag = true;
	
$update = Qry($conn,"UPDATE lr_bank SET branch='$branch_new',from_range='$from_new',to_range='$to_new',stock='$stock_new',
stock_left='$stock_left_new',description='Master Series : $master_series' WHERE id='$stock_id'");
	
if(!$update){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

$get_total_used_next_stock = Qry($conn,"SELECT id FROM lr_check WHERE lrno>=$next_range_from and lrno<=$next_range_to AND lrno NOT LIKE '0%'");
	
if(!$get_total_used_next_stock){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}
	
$total_used_next = numRows($get_total_used_next_stock);
$stock_left_next = $stock_next - $total_used_next;
	
$insert=Qry($conn,"INSERT INTO lr_bank (branch,from_range,to_range,company,stock,stock_left,description,date) VALUES 
('$branch_ext','$next_range_from','$next_range_to','$company','$stock_next','$stock_left_next','Master Series : $master_series','$date')");
	
if(!$insert){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}
	
$new_stock_id = getInsertID($conn);
	
if($total_used_next>0)
{
	$update_new_stock_id = Qry($conn,"UPDATE lr_check SET bilty_id='$new_stock_id' WHERE lrno>=$next_range_from and lrno<=$next_range_to 
	AND lrno NOT LIKE '0%' AND bilty_id='$stock_id'");
		
	if(!$update_new_stock_id){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
}
	
$log_data = "Master Series : $master_series ==> Edited to $from_new to $to_new. Stock Id : $stock_id. (Remaining Stock Added ==> $next_range_from 
to $next_range_to. Stock Id : $new_stock_id.)";	
	
$insert_log = Qry($conn,"INSERT INTO dairy.ediary_admin_log(code,action,desct,timestamp) VALUES ('$stock_id','BILTY_STOCK_EDIT','$log_data','$timestamp')");
	
if(!$insert_log){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}
	
if($flag)
{
	MySQLCommit($conn);
	closeConnection($conn);
	AlertRightCornerSuccessFadeFast("Record udpated successfully !");
	
	echo "<script>
		$('#button_update_modal_confirm').attr('disabled',false);
		$('#close_btn_lr_bank_edit_modal')[0].click();
		 LoadTable();
	</script>";	
	exit();
}
else
{
	MySQLRollBack($conn);
	closeConnection($conn);
	
	AlertRightCornerError("Error while processing request !");
	echo "<script>$('#button_update_modal_confirm').attr('disabled',false);</script>";
	exit();
}
?>