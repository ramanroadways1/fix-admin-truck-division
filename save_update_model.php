<?php
require_once("connect.php");

$timestamp = date("Y-m-d H:i:s");

$truck_no = escapeString($conn,strtoupper($_POST['truck_no']));
$id = escapeString($conn,$_POST['vehicle_id']);
$model_name = escapeString($conn,$_POST['model_name']);

echo "<script>$('#submit_btn_model_update').attr('disabled',true);</script>";

if($truck_no=='')
{
	AlertError("Vehicle number not found !");
	echo "<script>$('#submit_btn_model_update').attr('disabled',false);$('#loadicon').fadeOut('slow');</script>";
	exit();
}

if($id=='')
{
	AlertError("Data not found !");
	echo "<script>$('#submit_btn_model_update').attr('disabled',false);$('#loadicon').fadeOut('slow');</script>";
	exit();
}

if($model_name=='')
{
	AlertError("Model not found !");
	echo "<script>$('#submit_btn_model_update').attr('disabled',false);$('#loadicon').fadeOut('slow');</script>";
	exit();
}

$chk_model = Qry($conn,"SELECT id FROM dairy.model_list WHERE model='$model_name' AND is_active='1'");
	
if(!$chk_model){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	AlertError("Erorr !");
	echo "<script>$('#submit_btn_model_update').attr('disabled',false);$('#loadicon').fadeOut('slow');</script>";
	exit();
}

if(numRows($chk_model)==0)
{
	AlertError("Model not found !");
	echo "<script>$('#submit_btn_model_update').attr('disabled',false);$('#loadicon').fadeOut('slow');</script>";
	exit();
}

$get_vehicle = Qry($conn,"SELECT t.tno,t.model WHERE t.id='$id'");
	
if(!$get_vehicle){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	AlertError("Erorr !");
	echo "<script>$('#submit_btn_model_update').attr('disabled',false);$('#loadicon').fadeOut('slow');</script>";
	exit();
}

if(numRows($get_vehicle)==0)
{
	AlertError("Vehicle not found !");
	echo "<script>$('#submit_btn_model_update').attr('disabled',false);$('#loadicon').fadeOut('slow');</script>";
	exit();
}

$row_vehicle = fetchArray($get_vehicle);

if($row_vehicle['tno'] != $truck_no)
{
	AlertError("Vehicle not verified !");
	echo "<script>$('#submit_btn_model_update').attr('disabled',false);$('#loadicon').fadeOut('slow');</script>";
	exit();
}

$ext_model = $row_vehicle['model'];

if($ext_model==$model_name)
{
	AlertError("Nothing to update !");
	echo "<script>$('#submit_btn_model_update').attr('disabled',false);$('#loadicon').fadeOut('slow');</script>";
	exit();
}

StartCommit($conn);
$flag = true;
	
$mark_sold = Qry($conn,"UPDATE dairy.own_truck SET model='$model_name' WHERE id='$id'");
	
if(!$mark_sold){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

$insert_log = Qry($conn,"INSERT INTO dairy.ediary_admin_log(code,tno,action,desct,timestamp) VALUES ('$id','$truck_no','Model_Update',
'$ext_model) to $model_name','$timestamp')");
	
if(!$insert_log){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

if($flag)
{
	MySQLCommit($conn);
	closeConnection($conn);
	echo "<script>
		$('#model_name_row_$id').html('$model_name');
		$('#model_$id').val('$model_name');
		$('#loadicon').fadeOut('slow');
	</script>";
	AlertRightCornerSuccessFadeFast("Update Success !");
}
else
{
	MySQLRollBack($conn);
	closeConnection($conn);
	AlertError("Error While Processing Request !");
	echo "<script>$('#submit_btn_model_update').attr('disabled',false);$('#loadicon').fadeOut('slow');</script>";
	exit();
}
?>