<?php
require_once("connect.php");

$id = escapeString($conn,($_POST['id']));

if(empty($id))
{
	AlertRightCornerError("Hisab not found !");
	exit();
}

$get_hisab_data = Qry($conn,"SELECT tno,trip_no,loaded_final,hisab_type,row_cfwd,row_pay,row_credit_ho,row_da,row_salary,row_tel_exp,row_ag_exp,
row_asset_naame,driver_down,down_type,cash_vou,chq_vou,rtgs_vou,credit_cash,driver,branch,branch_user,date,timestamp 
FROM dairy.log_hisab WHERE id='$id'");

if(!$get_hisab_data){
	AlertRightCornerError("Error while processing request !");
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	exit();
}
	
if(numRows($get_hisab_data)==0)
{
	AlertRightCornerError("Hisab not found !");
	exit();
}

$row = fetchArray($get_hisab_data);

$tno = $row['tno'];
$driver_code = $row['driver'];
$trip_no = $row['trip_no'];
$hisab_type = $row['hisab_type'];

if($hisab_type!='1' AND $hisab_type!='2')
{
	AlertRightCornerError("Only carry-forward and pay-hisab allowed !");
	exit();
}

$get_driver_data = Qry($conn,"SELECT d.name as driver_name,d2.acname,d2.acno,d2.bank,d2.ifsc 
FROM dairy.driver AS d 
LEFT OUTER JOIN dairy.driver_ac AS d2 ON d2.code = d.code 
WHERE d.code = '$driver_code'");

if(!$get_driver_data){
	AlertRightCornerError("Error while processing request !");
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	exit();
}
	
if(numRows($get_driver_data)==0)
{
	AlertRightCornerError("Driver not found !");
	exit();
}

$row_driver = fetchArray($get_driver_data);

$driver_name = $row_driver['driver_name'];
$acname = $row_driver['acname'];
$acno = $row_driver['acno'];
$bank_name = $row_driver['bank'];
$ifsc = $row_driver['ifsc'];

$hisab_timestamp="NA";

if($row['hisab_type']=="1")
{
	$cfwd_hisab_data = Qry($conn,"SELECT id,driver_code,tno,credit,debit,balance,timestamp FROM dairy.driver_book WHERE trip_no='$row[trip_no]' 
	AND desct='HISAB-CARRY-FWD'");

	if(!$cfwd_hisab_data){
		AlertRightCornerError("Error while processing request !");
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		exit();
	}
	
	if(numRows($cfwd_hisab_data)==0)
	{
		AlertRightCornerError("Carry-Forward hisab record not found !");
		exit();
	}
	
	$row_cfwd = fetchArray($cfwd_hisab_data);
	$cfwd_amount = $row_cfwd['balance'];
	$paid_amount = 0;
	$cr_amount = 0;
	
	$hisab_row_id = $row_cfwd['id'];
}
else if($row['hisab_type']=="2")
{
	$paid_hisab_data = Qry($conn,"SELECT id,driver_code,tno,credit,debit,balance,timestamp FROM dairy.driver_book WHERE trip_no='$row[trip_no]' 
	AND desct='HISAB-PAID'");

	if(!$paid_hisab_data){
		AlertRightCornerError("Error while processing request !");
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		exit();
	}
	
	if(numRows($paid_hisab_data)==0)
	{
		AlertRightCornerError("Paid hisab record not found !");
		exit();
	}
	
	$row_paid = fetchArray($paid_hisab_data);
	
	$cfwd_amount = 0;
	$paid_amount = $row_paid['credit'];
	$cr_amount = 0;
	
	$hisab_row_id = $row_paid['id'];
	$hisab_timestamp = $row_paid['timestamp'];
}
else if($row['hisab_type']=="3")
{
	$cfwd_hisab_data = Qry($conn,"SELECT id,driver_code,tno,credit,debit,balance,timestamp FROM dairy.driver_book WHERE trip_no='$row[trip_no]' 
	AND desct='HISAB-CARRY-FWD'");

	if(!$cfwd_hisab_data){
		AlertRightCornerError("Error while processing request !");
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		exit();
	}
	
	if(numRows($cfwd_hisab_data)==0)
	{
		AlertRightCornerError("Carry-Forward hisab record not found !");
		exit();
	}
	
	$row_cfwd = fetchArray($cfwd_hisab_data);
	
	$paid_hisab_data = Qry($conn,"SELECT id,driver_code,tno,credit,debit,balance,timestamp FROM dairy.driver_book WHERE trip_no='$row[trip_no]' 
	AND desct='HISAB-PAID'");

	if(!$paid_hisab_data){
		AlertRightCornerError("Error while processing request !");
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		exit();
	}
	
	if(numRows($paid_hisab_data)==0)
	{
		AlertRightCornerError("Paid hisab record not found !");
		exit();
	}
	
	$row_paid = fetchArray($paid_hisab_data);
	
	$cfwd_amount = $row_cfwd['balance'];
	$paid_amount = $row_paid['credit'];
	$cr_amount = 0;
	
	$hisab_row_id = $row_paid['id'];
	$hisab_timestamp = $row_paid['timestamp'];
}
else if($row['hisab_type']=="4")
{
	$crho_hisab_data = Qry($conn,"SELECT id,driver_code,tno,credit,debit,balance,timestamp FROM dairy.driver_book WHERE trip_no='$row[trip_no]' 
	AND desct='CREDIT-HO'");

	if(!$crho_hisab_data){
		AlertRightCornerError("Error while processing request !");
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		exit();
	}
	
	if(numRows($crho_hisab_data)==0)
	{
		AlertRightCornerError("Credit-HO hisab record not found !");
		exit();
	}
	
	$row_crho = fetchArray($crho_hisab_data);
	
	$cfwd_amount = 0;
	$paid_amount = 0;
	$cr_amount = $row_crho['debit'];
	
	$hisab_row_id = $row_crho['id'];
}
else
{
	AlertRightCornerError("Invalid hisab type !");
	exit();
}

$cash_paid_amount=0;
$rtgs_paid_amount=0;

if($hisab_timestamp!='NA')
{
	$get_paid_cash_amount = Qry($conn,"SELECT id,tdvid,amt as amount FROM mk_tdv WHERE timestamp='$hisab_timestamp' AND hisab_vou='1' AND 
	truckno='$tno' AND mode='CASH'");

	if(!$get_paid_cash_amount){
		AlertRightCornerError("Error while processing request !");
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		exit();
	}
		
	if(numRows($get_paid_cash_amount)>0)
	{
		$cash_vou_row = fetchArray($get_paid_cash_amount);
		$cash_paid_amount = $cash_vou_row['amount'];
	}
		
	$get_paid_rtgs_amount = Qry($conn,"SELECT id,tdvid,amt as amount FROM mk_tdv WHERE timestamp='$hisab_timestamp' AND hisab_vou='1' AND 
	truckno='$tno' AND mode='NEFT'");

	if(!$get_paid_rtgs_amount){
		AlertRightCornerError("Error while processing request !");
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		exit();
	}
		
	if(numRows($get_paid_rtgs_amount)>0)
	{
		$rtgs_vou_row = fetchArray($get_paid_rtgs_amount);
		$rtgs_paid_amount = $rtgs_vou_row['amount'];
	}
}

$get_closing_amount = Qry($conn,"SELECT id,balance FROM dairy.driver_book WHERE trip_no='$row[trip_no]' AND id<'$hisab_row_id' 
ORDER BY id DESC LIMIT 1");

if(!$get_closing_amount){
	AlertRightCornerError("Error while processing request !");
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	exit();
}
	
if(numRows($get_closing_amount)==0)
{
	AlertRightCornerError("Closing balance not found !");
	exit();
}
	
$closing_row = fetchArray($get_closing_amount);
$closing_balance = $closing_row['balance'];
?>
<button id="modal_hisab_open_btn" style="display:none" data-toggle="modal" data-target="#ModalEditHisab"></button>
<div class="modal fade" id="ModalEditHisab" role="dialog" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
		<div class="modal-content" style="">
			<div class="modal-header bg-primary">
				<span style="font-size:13px">Edit Hisab : <?php echo $row['tno']." : $row[trip_no]";?></span>
			</div>
		<div class="modal-body">
		
	<div class="row">
		
		<div class="form-group col-md-12">
			<label>Driver Name <font color="red">*</font></label>
			<input readonly value="<?php echo $driver_name; ?>" name="driver_name" type="text" class="form-control" />
		</div>
		
		<input type="hidden" name="cfwd_amount_db" value="<?php echo $cfwd_amount; ?>" id="modal_cfwd_amount_db">
		<input type="hidden" name="paid_amount_db" value="<?php echo $paid_amount; ?>" id="modal_paid_amount_db">
		<input type="hidden" name="crho_amount_db" value="<?php echo $cr_amount; ?>" id="modal_crho_amount_db">
		<input type="hidden" name="closing_balance" value="<?php echo $closing_balance; ?>" id="closing_balance">
		<input type="hidden" name="cash_paid_amount" value="<?php echo $cash_paid_amount; ?>" id="cash_paid_amount">
		<input type="hidden" name="rtgs_paid_amount" value="<?php echo $rtgs_paid_amount; ?>" id="rtgs_paid_amount">
		<input type="hidden" name="trip_no_db" value="<?php echo $row['trip_no']; ?>" id="trip_no_db">
		<input type="hidden" name="hisab_id" value="<?php echo $id; ?>" id="hisab_id">
		
		<div class="form-group col-md-6">
			<label>Closing Balance <font color="red">*</font></label>
			<input readonly value="<?php echo $closing_balance; ?>" name="closing_amount" type="text" class="form-control" />
		</div>
		
		<div class="form-group col-md-6">
			<label>Hisab Type <font color="red">*</font></label>
			<select id="hisab_type" name="hisab_type" onchange="HisabTypeChange(this.value)" class="form-control" required="required"> 
				<option value="">--select hisab type--</option>
				<option <?php if($row['hisab_type']=="1") { echo "selected"; } ?> value="1">Carry-Forward</option>
				<option <?php if($row['hisab_type']=="2") { echo "selected"; } if($closing_balance>0) { echo "disabled"; } else if($closing_balance==0) { echo "disabled"; }?> value="2">Pay Hisab</option>
				<option <?php if($row['hisab_type']=="3") { echo "selected"; } if($closing_balance>0) { echo "disabled"; } else if($closing_balance==0) { echo "disabled"; }?> disabled value="3">Carry-Forward + Pay</option>
				<option <?php if($row['hisab_type']=="4") { echo "selected"; } if($closing_balance<0) { echo "disabled"; } else if($closing_balance==0) { echo "disabled"; }?>  disabled value="4">Credit HO</option>
			</select>
		</div>
		
<script>		
function HisabTypeChange(elem)
{
	var amount_cfwd = Number($('#modal_cfwd_amount_db').val());
	var amount_pay = Number($('#modal_paid_amount_db').val());
	var amount_crho = Number($('#modal_crho_amount_db').val());
	var closing_balance = Number($('#closing_balance').val());
	var cash_paid_amount = Number($('#cash_paid_amount').val());
	var rtgs_paid_amount = Number($('#rtgs_paid_amount').val());
	
	$('#modal_cfwd_amount').attr('readonly',true);
	$('#modal_paid_amount').attr('readonly',true);
	$('#modal_cr_amount').attr('readonly',true);
	$('#modal_cash_pay').attr('readonly',true);
	$('#modal_rtgs_pay').attr('readonly',true);
	
	$('#modal_cfwd_amount').val('');
	$('#modal_paid_amount').val('');
	$('#modal_cr_amount').val('');
	$('#modal_cash_pay').val('');
	$('#modal_rtgs_pay').val('');
	
	$('.pay_options').hide();
		
	if(elem=='1')
	{
		$('#modal_cfwd_amount').val(closing_balance);
		$('#modal_paid_amount').val('0');
		$('#modal_cr_amount').val('0');
	}
	else if(elem=='2')
	{
		if(closing_balance>=0)
		{
			Swal.fire({icon: 'warning',html: '<font size=\'2\' color=\'black\'>Insufficient balance to pay !</font>',});
			$('#hisab_type').val('');
		}
		
		$('#modal_cfwd_amount').val('0');
		$('#modal_paid_amount').val(Math.abs(closing_balance));
		$('#modal_cr_amount').val('0');
		
		$('#modal_cash_pay').val(cash_paid_amount);
		$('#modal_rtgs_pay').val(rtgs_paid_amount);
		$('#modal_cash_pay').attr('readonly',false);
		$('#modal_rtgs_pay').attr('readonly',false);
		
		$('.pay_options').show();
	}
	else if(elem=='3')
	{
		if(closing_balance>=0)
		{
			Swal.fire({icon: 'warning',html: '<font size=\'2\' color=\'black\'>Insufficient balance to pay and carry-fwd !</font>',});
			$('#hisab_type').val('');
		}
		
		$('#modal_cfwd_amount').val(Math.abs(amount_cfwd));
		$('#modal_paid_amount').val(amount_pay);
		
		$('#modal_cfwd_amount').attr('readonly',false);
		$('#modal_paid_amount').attr('readonly',false);
	
		$('#modal_cr_amount').val('0');
		
		$('#modal_cash_pay').val(cash_paid_amount);
		$('#modal_rtgs_pay').val(rtgs_paid_amount);
		$('#modal_cash_pay').attr('readonly',false);
		$('#modal_rtgs_pay').attr('readonly',false);
		
		$('.pay_options').show();
	}
	else if(elem=='4')
	{
		if(closing_balance<=0)
		{
			Swal.fire({icon: 'warning',html: '<font size=\'2\' color=\'black\'>Insufficient balance to credit to cashbook !</font>',});
			$('#hisab_type').val('');
		}
		
		$('#modal_cfwd_amount').val('0');
		$('#modal_paid_amount').val('0');
		$('#modal_cr_amount').val(amount_crho);
	}
	else
	{
		$('#modal_cfwd_amount').val('0');
		$('#modal_paid_amount').val('0');
		$('#modal_cr_amount').val('0');
	}
}

function CalcAmount1(cfwd_amount)
{
	var closing_balance = Number(Math.abs($('#closing_balance').val()));
	var pay_amount = closing_balance - Number(cfwd_amount);
	$('#modal_paid_amount').val(pay_amount);
}

function CalcAmount2(pay_amount)
{
	var closing_balance = Number(Math.abs($('#closing_balance').val()));
	var cfwd_amount = closing_balance - Number(pay_amount);
	$('#modal_cfwd_amount').val(cfwd_amount);
}

function CashPayIp(cash_amount)
{
	var pay_amount = Number($('#modal_paid_amount').val());
	
	if(pay_amount=='' || pay_amount<=0)
	{
		Swal.fire({icon: 'warning',html: '<font size=\'2\' color=\'black\'>Payment amount not found !</font>',});
		$('#modal_cash_pay').val('');
		$('#modal_rtgs_pay').val('');
	}
	else
	{
		var rtgs_amount = pay_amount - Number(cash_amount);
		$('#modal_rtgs_pay').val(rtgs_amount);
	}
}

function RtgsPayIp(rtgs_amount)
{
	var pay_amount = Number($('#modal_paid_amount').val());
	
	if(pay_amount=='' || pay_amount==0)
	{
		Swal.fire({icon: 'warning',html: '<font size=\'2\' color=\'black\'>Payment amount not found !</font>',});
		$('#modal_cash_pay').val('');
		$('#modal_rtgs_pay').val('');
	}
	else
	{
		var cash_amount = pay_amount - Number(rtgs_amount);
		$('#modal_cash_pay').val(cash_amount);
	}
}
</script>		
		
		<div class="form-group col-md-6">
			<label>Vehicle Number <font color="red">*</font></label>
			<input readonly value="<?php echo $tno; ?>" name="tno" type="text" class="form-control" />
		</div>
		
		<div class="form-group col-md-6">
			<label>Carry-Forward <font color="red">*</font></label>
			<input oninput="CalcAmount1(this.value)" readonly value="<?php echo $cfwd_amount; ?>" id="modal_cfwd_amount" name="cfwd_amount" type="text" class="form-control" />
		</div>
		
		<div class="form-group col-md-6">
			<label>Paid Amount <font color="red">*</font></label>
			<input min="0" oninput="CalcAmount2(this.value)" readonly value="<?php echo $paid_amount; ?>" id="modal_paid_amount" name="paid_amount" type="text" class="form-control" />
		</div>
		
		<div class="form-group col-md-6">
			<label>Credit HO <font color="red">*</font></label>
			<input readonly value="<?php echo $cr_amount; ?>" id="modal_cr_amount" name="cr_amount" type="text" class="form-control" />
		</div>
		
		<div class="form-group pay_options col-md-6">
			<label>Cash Pay <font color="red">*</font></label>
			<input readonly max="3000" min="0" oninput="CashPayIp(this.value)" id="modal_cash_pay" name="cash_pay" type="text" class="form-control" />
		</div>
		
		<div class="form-group pay_options col-md-6">
			<label>Neft/Rtgs Pay <font color="red">*</font></label>
			<input readonly oninput="RtgsPayIp(this.value)" min="0" id="modal_rtgs_pay" name="rtgs_pay" type="text" class="form-control" />
		</div>
		
		<div class="form-group col-md-12" id="result_modal_22"></div>
		
		</div>
        </div>

	 <div class="modal-footer">
          <button type="button" onclick="UpdateHisabFunc()" id="button_form_hisab" class="pull-left btn btn-sm btn-danger">Update Hisab</button>
          <button type="button" id="hisab_edit_modal_cls_btn" class="btn btn-sm btn-primary" data-dismiss="modal">Close</button>
	 </div>
	 
      </div>
    </div>
  </div>
 
<script>
function UpdateHisabFunc()
{
	var cfwd_db = $('#modal_cfwd_amount_db').val();
	var pay_db = $('#modal_paid_amount_db').val();
	var crho_db = $('#modal_crho_amount_db').val();
	var closing_balance = $('#closing_balance').val();
	var cash_paid_db = $('#cash_paid_amount').val();
	var rtgs_paid_db = $('#rtgs_paid_amount').val();
	
	var hisab_type = $('#hisab_type').val();
	var cfwd = $('#modal_cfwd_amount').val();
	var pay = $('#modal_paid_amount').val();
	var crho = $('#modal_cr_amount').val();
	var cash_pay = $('#modal_cash_pay').val();
	var rtgs_pay = $('#modal_rtgs_pay').val();
	
	var hisab_id = $('#hisab_id').val();
	var trip_no = $('#trip_no_db').val();
	
	// var cash_paid_amount = Number($('#cash_paid_amount').val());
	// var rtgs_paid_amount = Number($('#rtgs_paid_amount').val());
	
	$('#loadicon').show();
	$('#button_form_hisab').attr('disabled',true);
		jQuery.ajax({
		url: "update_hisab_pay.php",
		data: 'cfwd_db=' + cfwd_db + '&hisab_id=' + hisab_id + '&trip_no=' + trip_no + '&pay_db=' + pay_db + '&crho_db=' + crho_db + '&closing_balance=' + closing_balance + '&cash_paid_db=' + cash_paid_db + '&rtgs_paid_db=' + rtgs_paid_db + '&hisab_type=' + hisab_type + '&cfwd=' + cfwd + '&pay=' + pay + '&crho=' + crho + '&cash_pay=' + cash_pay + '&rtgs_pay=' + rtgs_pay,
		type: "POST",
		success: function(data) {
			$("#result_modal_22").html(data);
		},
		error: function() {}
	});
}
	
HisabTypeChange('<?php echo $hisab_type; ?>');	
$('#modal_hisab_open_btn')[0].click();	
$('#loadicon').fadeOut('slow');	
</script> 
