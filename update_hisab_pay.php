<?php
require_once 'connect.php';

$timestamp = date("Y-m-d H:i:s");
$date = date("Y-m-d");

if(empty($_POST['hisab_type']))
{
	AlertRightCornerError("Hisab type not found !");
	echo "<script>$('#button_form_hisab').attr('disabled',false);</script>";
	exit();
}

$hisab_type = escapeString($conn,($_POST['hisab_type']));
$pay = escapeString($conn,($_POST['pay']));
$cash_pay = escapeString($conn,($_POST['cash_pay']));
$rtgs_pay = escapeString($conn,($_POST['rtgs_pay']));
$hisab_id = escapeString($conn,($_POST['hisab_id']));
$trip_no = escapeString($conn,($_POST['trip_no']));
$cash_paid_db = escapeString($conn,($_POST['cash_paid_db']));
$rtgs_paid_db = escapeString($conn,($_POST['rtgs_paid_db']));

if($hisab_type!='2')
{
	AlertRightCornerError("Check Hisab type !");
	echo "<script>$('#button_form_hisab').attr('disabled',false);</script>";
	exit();
}

$get_hisab_data = Qry($conn,"SELECT tno,trip_no,closing_balance,hisab_type,driver,branch,branch_user,date,cash_vou,chq_vou,rtgs_vou,credit_cash,
timestamp FROM dairy.log_hisab WHERE id='$hisab_id'");

if(!$get_hisab_data){
	AlertRightCornerError("Error while processing request !");
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	exit();
}
	
if(numRows($get_hisab_data)==0)
{
	AlertRightCornerError("Hisab not found !");
	echo "<script>$('#button_form_hisab').attr('disabled',false);</script>";
	exit();
}

$row = fetchArray($get_hisab_data);

$tno = $row['tno'];
$closing_balance_log_hisab = $row['closing_balance'];

if($row['trip_no'] != $trip_no)
{
	AlertRightCornerError("Trip number not verified !");
	exit();
}

if($row['hisab_type'] != "1" AND $row['hisab_type'] != "2")
{
	AlertRightCornerError("Only carry-forward and pay-hisab allowed !");
	echo "<script>$('#button_form_hisab').attr('disabled',false);</script>";
	exit();
}

$get_company = Qry($conn,"SELECT comp as company FROM dairy.own_truck WHERE tno='$tno'");

if(!$get_company){
	AlertRightCornerError("Error while processing request !");
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	exit();
}
	
if(numRows($get_company)==0)
{
	AlertRightCornerError("Company not found !");
	echo "<script>$('#button_form_hisab').attr('disabled',false);</script>";
	exit();
}

$row_get_company = fetchArray($get_company);
$company = $row_get_company['company'];

$driver_code = $row['driver'];
$hisab_type_db = $row['hisab_type'];

$get_driver_data = Qry($conn,"SELECT d.name as driver_name,d2.acname,d2.acno,d2.bank,d2.ifsc 
FROM dairy.driver AS d 
LEFT OUTER JOIN dairy.driver_ac AS d2 ON d2.code = d.code 
WHERE d.code = '$driver_code'");

if(!$get_driver_data){
	AlertRightCornerError("Error while processing request !");
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	exit();
}
	
if(numRows($get_driver_data)==0)
{
	AlertRightCornerError("Driver not found !");
	echo "<script>$('#button_form_hisab').attr('disabled',false);</script>";
	exit();
}

$row_driver = fetchArray($get_driver_data);

$driver_name = $row_driver['driver_name'];
$acname = $row_driver['acname'];
$acno = $row_driver['acno'];
$bank_name = $row_driver['bank'];
$ifsc = $row_driver['ifsc'];

if($hisab_type_db=="1" AND $hisab_type=="1")
{
	AlertRightCornerError("Nothing to update !");
	echo "<script>$('#button_form_hisab').attr('disabled',false);</script>";
	exit();
}

$get_hisab_row = Qry($conn,"SELECT id,driver_code,tno,branch,date,desct,timestamp,balance FROM dairy.driver_book WHERE trip_no='$trip_no' 
AND desct like 'HISAB%'");

if(!$get_hisab_row){
	AlertRightCornerError("Error while processing request !");
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	exit();
}
	
if(numRows($get_hisab_row)==0)
{
	AlertRightCornerError("Hisab record not found !");
	echo "<script>$('#button_form_hisab').attr('disabled',false);</script>";
	exit();
}

if(numRows($get_hisab_row)>1)
{
	AlertRightCornerError("Mulitple Hisab record found !");
	echo "<script>$('#button_form_hisab').attr('disabled',false);</script>";
	exit();
}

$row_hisab = fetchArray($get_hisab_row);

if($row_hisab['desct'] != 'HISAB-CARRY-FWD' AND $row_hisab['desct'] != 'HISAB-PAID')
{
	AlertRightCornerError("Only carry-forward and pay-hisab allowed !");
	echo "<script>$('#button_form_hisab').attr('disabled',false);</script>";
	exit();
}

$hisab_row_id = $row_hisab['id'];
$hisab_branch = $row_hisab['branch'];
$branch = $hisab_branch;
$hisab_timestamp = $row_hisab['timestamp'];
$hisab_date = $row_hisab['date'];

$get_closing_amount = Qry($conn,"SELECT id,balance FROM dairy.driver_book WHERE trip_no='$trip_no' AND id<'$hisab_row_id' ORDER BY id DESC LIMIT 1");

	if(!$get_closing_amount){
		AlertRightCornerError("Error while processing request !");
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		exit();
	}
		
	if(numRows($get_closing_amount)==0)
	{
		AlertRightCornerError("Closing balance not found !");
		echo "<script>$('#button_form_hisab').attr('disabled',false);</script>";
		exit();
	}
		
	$closing_row = fetchArray($get_closing_amount);
	$closing_balance_db = $closing_row['balance'];
	$closing_balance_id = $closing_row['id'];

	if($closing_balance_db != $closing_balance_log_hisab)
	{
		AlertRightCornerError("Closing balance not verified !");
		echo "<script>$('#button_form_hisab').attr('disabled',false);</script>";
		exit();
	}

	if($closing_balance_db>=0)
	{
		AlertRightCornerError("Not sufficient amount to pay !");
		echo "<script>$('#button_form_hisab').attr('disabled',false);</script>";
		exit();
	}

	if($pay != abs($closing_balance_db))
	{
		AlertRightCornerError("Amount not verified !");
		echo "<script>$('#button_form_hisab').attr('disabled',false);</script>";
		exit();
	}
		
	if(($cash_pay+$rtgs_pay) != $pay)
	{
		AlertRightCornerError("Amount not verified. Check Cash & RTGS !");
		echo "<script>$('#button_form_hisab').attr('disabled',false);</script>";
		exit();
	}

	if($cash_pay>3000)
	{
		AlertRightCornerError("Max cash allowed is: 3000 !");
		echo "<script>$('#button_form_hisab').attr('disabled',false);</script>";
		exit();
	}

	if($rtgs_pay>0 AND ($acname=='' || $acno==''))
	{
		AlertRightCornerError("Driver account details not updated !");
		echo "<script>$('#button_form_hisab').attr('disabled',false);</script>";
		exit();
	}

	if($cash_pay>0)
	{
		$check_bal = Qry($conn,"SELECT balance,balance2 FROM user WHERE username='$branch'");

		if(!$check_bal){
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			AlertRightCornerError("Error while processing request !");
			echo "<script>$('#button_form_hisab').attr('disabled',false);</script>";
			exit();
		}

		if(numRows($check_bal)==0)
		{
			errorLog("Branch balance not found.",$conn,$page_name,__LINE__);
			AlertRightCornerError("Branch balance not found !");
			echo "<script>$('#button_form_hisab').attr('disabled',false);</script>";
			exit();
		}

		$row_bal = fetchArray($check_bal);

		$rrpl_cash = $row_bal['balance'];
		$rr_cash = $row_bal['balance2'];

		if($company=='RRPL' && $rrpl_cash>=$cash_pay)
		{
		}
		else if($company=='RAMAN_ROADWAYS' && $rr_cash>=$cash_pay)
		{
		}
		else
		{
			AlertRightCornerError("Insufficient Balance in Company: $company !");
			echo "<script>$('#button_form_hisab').attr('disabled',false);</script>";
			exit();
		}
	}
	
	$get_da_and_salary = Qry($conn,"SELECT 
	(SELECT desct FROM dairy.driver_book WHERE trip_no='$trip_no' AND desct like 'DA%') as 'da_desc',
	(SELECT debit FROM dairy.driver_book WHERE trip_no='$trip_no' AND desct like 'DA%') as 'da_amount',
	(SELECT desct FROM dairy.driver_book WHERE trip_no='$trip_no' AND desct like 'SALARY%') as 'sal_desc',
	(SELECT debit FROM dairy.driver_book WHERE trip_no='$trip_no' AND desct like 'SALARY%') as 'sal_amount',
	(SELECT debit FROM dairy.driver_book WHERE trip_no='$trip_no' AND desct='EXP-TELEPHONE') as 'tel_exp_amount'");

	if(!$get_da_and_salary){
		AlertRightCornerError("Error while processing request !");
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		exit();
	}

	$row_get_da_sal = fetchArray($get_da_and_salary);
		
	if($row_get_da_sal['sal_amount']>0)
	{
		$nrr = "$tno/$driver_name, DA: $row_get_da_sal[da_amount], TEL: $row_get_da_sal[tel_exp_amount], AIR & GREASE : $row_get_da_sal[tel_exp_amount], $row_get_da_sal[sal_desc].";
	}
	else
	{
		$nrr="$tno/$driver_name, DA: $row_get_da_sal[da_amount], TEL: $row_get_da_sal[tel_exp_amount], AIR & GREASE : $row_get_da_sal[tel_exp_amount].";
	}
	
if($hisab_type_db==$hisab_type)
{
	if($cash_paid_db==$cash_pay AND $rtgs_paid_db==$rtgs_pay)
	{
		AlertRightCornerError("Nothing to update !");
		echo "<script>$('#button_form_hisab').attr('disabled',false);</script>";
		exit();
	}
	
	if($cash_paid_db==0 AND $rtgs_paid_db==0)
	{
		AlertRightCornerError("Payment methods not found !");
		echo "<script>$('#button_form_hisab').attr('disabled',false);</script>";
		exit();
	}
	
$cash_vou_id2 = 0;	
$rtgs_vou_id2 = 0;	
	
$old_rtgs_vou = "NO";	
$old_cash_vou = "NO";	
	
	if($rtgs_paid_db>0)
	{
		$chk_rtgs_vou = Qry($conn,"SELECT id,tdvid FROM mk_tdv WHERE timestamp='$hisab_timestamp' AND truckno='$tno' AND hisab_vou='1' AND mode='NEFT'");	

		if(!$chk_rtgs_vou){
			AlertRightCornerError("Error while processing request !");
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			echo "<script>$('#button_form_hisab').attr('disabled',false);</script>";
			exit();
		}
		
		if(numRows($chk_rtgs_vou)!=1)
		{
			AlertRightCornerError("RTGS Voucher not found !");
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			echo "<script>$('#button_form_hisab').attr('disabled',false);</script>";
			exit();
		}
		
		$row_rtgs_vou = fetchArray($chk_rtgs_vou);
		
		$rtgs_vou_no = $row_rtgs_vou['tdvid'];
		$rtgs_vou_id1 = $row_rtgs_vou['id'];
		
		$chk_approval = Qry($conn,"SELECT id FROM rtgs_fm WHERE fno='$rtgs_vou_no' AND colset_d='1'");
		
		if(!$chk_approval){
			AlertRightCornerError("Error while processing request !");
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			echo "<script>$('#button_form_hisab').attr('disabled',false);</script>";
			exit();
		}
		
		if(numRows($chk_approval)>0)
		{
			AlertRightCornerError("Rtgs payment approved already !");
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			echo "<script>$('#button_form_hisab').attr('disabled',false);</script>";
			exit();
		}
		
		$old_rtgs_vou = "YES";	
	}
	
	if($cash_paid_db>0)
	{
		$chk_cash_vou = Qry($conn,"SELECT id,tdvid FROM mk_tdv WHERE timestamp='$hisab_timestamp' AND truckno='$tno' AND hisab_vou='1' AND mode='CASH'");	

		if(!$chk_cash_vou){
			AlertRightCornerError("Error while processing request !");
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			echo "<script>$('#button_form_hisab').attr('disabled',false);</script>";
			exit();
		}
		
		if(numRows($chk_cash_vou)!=1)
		{
			AlertRightCornerError("Cash Voucher not found !");
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			echo "<script>$('#button_form_hisab').attr('disabled',false);</script>";
			exit();
		}
		
		$row_cash_vou = fetchArray($chk_cash_vou);
		
		$cash_vou_no = $row_cash_vou['tdvid'];
		$cash_vou_id1 = $row_cash_vou['id'];
		
		$old_cash_vou = "YES";
	}
	
	StartCommit($conn);
	$flag = true;	
	
	if($old_rtgs_vou=='YES' AND $rtgs_pay==0)
	{
		$delete_rtgs_vou = Qry($conn,"DELETE FROM mk_tdv WHERE id='$rtgs_vou_id1'");
		
		if(!$delete_rtgs_vou){
			$flag = false;
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		}
		
		if(AffectedRows($conn)==0)
		{
			$flag = false;
			errorLog("Rtgs voucher not deleted. Id: $rtgs_vou_id1. And Vou_no: $rtgs_vou_no.",$conn,$page_name,__LINE__);
		}
		
		$delete_rtgs_vou_req = Qry($conn,"DELETE FROM rtgs_fm WHERE fno='$rtgs_vou_no' AND colset_d!='1'");
		
		if(!$delete_rtgs_vou_req){
			$flag = false;
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		}
		
		if(AffectedRows($conn)==0)
		{
			$flag = false;
			errorLog("Rtgs voucher request not deleted. Id: Vou_no: $rtgs_vou_no.",$conn,$page_name,__LINE__);
		}
		
		$update_passbook_rtgs = Qry($conn,"DELETE FROM passbook WHERE vou_no='$rtgs_vou_no'");
			
		if(!$update_passbook_rtgs){
			$flag = false;
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		}
	}
	
	if($old_cash_vou=='YES' AND $cash_pay==0)
	{
		$delete_rtgs_vou = Qry($conn,"DELETE FROM mk_tdv WHERE id='$cash_vou_id1'");
		
		if(!$delete_rtgs_vou){
			$flag = false;
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		}
		
		if(AffectedRows($conn)==0)
		{
			$flag = false;
			errorLog("Rtgs voucher not deleted. Id: $cash_vou_id1. And Vou_no: $cash_vou_no.",$conn,$page_name,__LINE__);
		}
		
		$select_cashbook_entry = Qry($conn,"SELECT id,comp,user,debit,debit2,balance FROM cashbook WHERE vou_no='$cash_vou_no'");
		
		if(!$select_cashbook_entry){
			$flag = false;
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		}
		
		if(AffectedRows($select_cashbook_entry)==0)
		{
			$flag = false;
			errorLog("Cash voucher entry not found in cashbook. Vou_no: $cash_vou_no.",$conn,$page_name,__LINE__);
		}
		
		$row_cashbook = fetchArray($select_cashbook_entry);
		
		$cash_company = $row_cashbook['comp'];
		$cash_branch = $row_cashbook['user'];
		$cashbook_id1 = $row_cashbook['id'];
		
		if($cash_company=='RRPL'){
			$cash_amount = $row_cashbook['debit'];
			$bal_col_name = "balance";
			$dr_col_name = "debit";
			$pre_cash_balance = $row_cashbook['balance'] + $row_cashbook['debit'];
		}
		else{
			$cash_amount = $row_cashbook['debit2'];
			$bal_col_name = "balance2";
			$dr_col_name = "debit2";
			$pre_cash_balance = $row_cashbook['balance2'] + $row_cashbook['debit2'];
		}
		
		if($_POST['cash_paid_amount'] != $cash_amount)
		{
			$flag = false;
			errorLog("Cash amount not verified. Paid earlier is: $_POST[cash_paid_amount] and Cashbook entry is: $cash_amount.",$conn,$page_name,__LINE__);
		}
		
		$update_cash_balance = Qry($conn,"UPDATE user SET `$bal_col_name`=`$bal_col_name`+'$cash_amount' WHERE username='$cash_branch'");
		
		if(!$update_cash_balance){
			$flag = false;
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		}
		
		$update_cashbook_balance = Qry($conn,"UPDATE cashbook SET `$bal_col_name`=`$bal_col_name`+'$cash_amount' WHERE id>'$cashbook_id1' AND 
		user='$cash_branch' AND comp='$cash_company'");
		
		if(!$update_cashbook_balance){
			$flag = false;
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		}
		
		$delete_cashbook_entry = Qry($conn,"DELETE FROM cashbook WHERE id='$cashbook_id1'");
		
		if(!$delete_cashbook_entry){
			$flag = false;
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		}
		
		if(AffectedRows($conn)==0)
		{
			$flag = false;
			errorLog("Cashbook voucher entry not deleted. Id: Cashbook Id: $cashbook_id1.",$conn,$page_name,__LINE__);
		}
	}
	
	if($rtgs_pay>0)
	{
		if($old_rtgs_vou=='YES')
		{
			$update_rtgs_amount = Qry($conn,"UPDATE mk_tdv SET amt='$rtgs_pay' WHERE id='$rtgs_vou_id1'");
			
			if(!$update_rtgs_amount){
				$flag = false;
				errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			}
			
			$rtgs_vou_id2 = $rtgs_vou_id1;	
			
			if(AffectedRows($conn)==0)
			{
				$flag = false;
				errorLog("Voucher amount not updated. Id: $rtgs_vou_id1.",$conn,$page_name,__LINE__);
			}
			
			$update_rtgs_req = Qry($conn,"UPDATE rtgs_fm SET totalamt='$rtgs_pay',amount='$rtgs_pay' WHERE fno='$rtgs_vou_no' AND colset_d!='1'");
			
			if(!$update_rtgs_req){
				$flag = false;
				errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			}
			
			if(AffectedRows($conn)==0)
			{
				$flag = false;
				errorLog("Rtgs voucher request not updated. Vou_no: $rtgs_vou_no.",$conn,$page_name,__LINE__);
			}
			
			if($company=='RRPL'){
				$debit_col='debit';		
			}
			else if($company=='RAMAN_ROADWAYS'){
				$debit_col='debit2';
			}
			
			$update_passbook_rtgs = Qry($conn,"UPDATE passbook SET `$debit_col`='$rtgs_pay' WHERE vou_no='$rtgs_vou_no'");
			
			if(!$update_passbook_rtgs){
				$flag = false;
				errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			}
		}
		else
		{
			if($company=='RRPL'){
				$debit='debit';		
			}
			else if($company=='RAMAN_ROADWAYS'){
				$debit='debit2';
			}

			$get_vou_id = GetVouId("T",$conn,"mk_tdv","tdvid",$branch);
			
			if(!$get_vou_id || $get_vou_id=="0" || $get_vou_id=="")
			{
				$flag = false;
				errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			}
			
			$tids = $get_vou_id;
			
			$insert_vou = Qry($conn,"INSERT INTO mk_tdv(user,branch_user,company,tdvid,date,newdate,truckno,dname,amt,mode,hisab_vou,ac_name,ac_no,
			bank,ifsc,pan,dest,timestamp) VALUES ('$branch','ADMIN','$company','$tids','$date','$hisab_date','$tno','$driver_name',
			'$rtgs_pay','NEFT','1','$acname','$acno','$bank_name','$ifsc','','HISAB-PAYMENT : $nrr','$hisab_timestamp')");	

			if(!$insert_vou){
				$flag = false;
				errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			}	

			$rtgs_vou_id = getInsertID($conn);
			
			$rtgs_vou_id2 = $rtgs_vou_id;	

			$update_passbook=Qry($conn,"INSERT INTO passbook(user,vou_no,date,vou_date,comp,vou_type,desct,`$debit`,timestamp) 
			VALUES ('$branch','$tids','$date','$hisab_date','$company','Truck_Voucher','HISAB-PAYMENT : $nrr','$rtgs_pay','$hisab_timestamp')");

			if(!$update_passbook){
				$flag = false;
				errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			}	

			if($company=='RRPL')
			{
				$get_Crn = GetCRN("RRPL-T",$conn);
				if(!$get_Crn || $get_Crn=="0" || $get_Crn=="")
				{
					$flag = false;
					errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
				}
				
				$crnnew = $get_Crn;		
			}
			else if($company=='RAMAN_ROADWAYS')
			{
				$get_Crn = GetCRN("RR-T",$conn);
				if(!$get_Crn || $get_Crn=="0" || $get_Crn=="")
				{
					$flag = false;
					errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
				}
				
				$crnnew = $get_Crn;
			}

			$qry_rtgs=Qry($conn,"INSERT INTO rtgs_fm(fno,branch,com,totalamt,amount,acname,acno,bank_name,ifsc,pay_date,fm_date,tno,type,
			crn,timestamp) VALUES ('$tids','$branch','$company','$rtgs_pay','$rtgs_pay','$acname','$acno','$bank_name','$ifsc','$date',
			'$hisab_date','$tno','TRUCK_ADVANCE','$crnnew','$hisab_timestamp')");

			if(!$qry_rtgs){
				$flag = false;
				errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			}
		}
	}
	
	if($cash_pay>0)
	{
		if($old_cash_vou=='YES')
		{
			$update_cash_amount = Qry($conn,"UPDATE mk_tdv WHERE amt='$cash_pay' WHERE id='$cash_vou_id1'");
			
			if(!$update_cash_amount){
				$flag = false;
				errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			}
			
			if(AffectedRows($conn)==0)
			{
				$flag = false;
				errorLog("Voucher amount not updated. Id: $cash_vou_id1.",$conn,$page_name,__LINE__);
			}
			
			$cash_vou_id2 = $cash_vou_id1;
			
			$balance_diff = $cash_amount - $cash_pay;
			
			$update_cash_entry = Qry($conn,"UPDATE cashbook WHERE `$dr_col_name`='$cash_pay',`$bal_col_name`=`$bal_col_name`+('$balance_diff') 
			WHERE id='$cashbook_id1'");
			
			if(!$update_cash_entry){
				$flag = false;
				errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			}
			
			$update_cash_book1 = Qry($conn,"UPDATE cashbook WHERE `$bal_col_name`=`$bal_col_name`+('$balance_diff') WHERE id>'$cashbook_id1' AND 
			user='$cash_branch' AND comp='$cash_company'");
			
			if(!$update_cash_book1){
				$flag = false;
				errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			}
			
			$update_cash_balance1 = Qry($conn,"UPDATE user WHERE `$bal_col_name`=`$bal_col_name`+('$balance_diff') WHERE username='$cash_branch'");
			
			if(!$update_cash_balance1){
				$flag = false;
				errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			}
			
			if(AffectedRows($conn)==0)
			{
				$flag = false;
				errorLog("Rtgs voucher request not updated. Vou_no: $rtgs_vou_no.",$conn,$page_name,__LINE__);
			}
		}
		else
		{
			$check_bal = Qry($conn,"SELECT balance,balance2 FROM user WHERE username='$branch'");

			if(!$check_bal){
				$flag = false;
				errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			}

			if(numRows($check_bal)==0)
			{
				$flag = false;
				errorLog("Branch balance not found.",$conn,$page_name,__LINE__);
			}

			$row_bal = fetchArray($check_bal);

			$rrpl_cash = $row_bal['balance'];
			$rr_cash = $row_bal['balance2'];

			if($company=='RRPL' && $rrpl_cash>=$cash_pay)
			{
				$newbal_cashbook = $rrpl_cash - $cash_pay;
				$debit = 'debit';			
				$balance_cashbook = 'balance';
				
			}
			else if($company=='RAMAN_ROADWAYS' && $rr_cash>=$cash_pay)
			{
				$newbal_cashbook = $rr_cash - $cash_pay;
				$debit = 'debit2';			
				$balance_cashbook = 'balance2';
			}
			else
			{
				$flag = false;
				errorLog("Company not found. or insufficient Balance.",$conn,$page_name,__LINE__);
			}

			$get_vou_id = GetVouId("T",$conn,"mk_tdv","tdvid",$branch);
			
			if(!$get_vou_id || $get_vou_id=="0" || $get_vou_id=="")
			{
				$flag = false;
				errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			}
			
			$tids = $get_vou_id;
			
			$update_cashbook=Qry($conn,"INSERT INTO cashbook(user,user_code,date,vou_date,comp,vou_no,vou_type,desct,`$debit`,`$balance_cashbook`,
			timestamp) VALUES ('$branch','ADMIN','$date','$hisab_date','$company','$tids','Truck_Voucher','HISAB-PAYMENT : $nrr',
			'$cash_pay','$newbal_cashbook','$hisab_timestamp')");

			if(!$update_cashbook){
				$flag = false;
				errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			}	

			$query_update = Qry($conn,"update user set `$balance_cashbook`='$newbal_cashbook' where username='$branch'");

			if(!$query_update){
				$flag = false;
				errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			}	

			if(AffectedRows($conn)==0)
			{
				$flag = false;
				errorLog("ZERO ROWS AFFECTED BALANCE UPDATE.",$conn,$page_name,__LINE__);
			}

			$insert_vou = Qry($conn,"INSERT INTO mk_tdv(user,branch_user,company,tdvid,date,newdate,truckno,dname,amt,mode,hisab_vou,dest,timestamp) 
			VALUES ('$branch','ADMIN','$company','$tids','$date','$hisab_date','$tno','$driver_name','$cash_pay','CASH','1','HISAB-PAYMENT : $nrr',
			'$hisab_timestamp')");	

			if(!$insert_vou){
				$flag = false;
				errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			}
			
			$cash_vou_id2 = getInsertID($conn);
		}
	}
	
	$update_log_hisab = Qry($conn,"UPDATE dairy.log_hisab SET hisab_type='2',row_cfwd='0',cash_vou='$cash_vou_id2',
	rtgs_vou='$rtgs_vou_id2' WHERE trip_no='$trip_no'");

	if(!$update_log_hisab){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
	 
	if($flag)
	{
		MySQLCommit($conn);
		closeConnection($conn);
		
		AlertRightCornerSuccessFadeFast("Update Success !");
		echo "<script>
			$('#hisab_edit_modal_cls_btn')[0].click();
			$('#search_add_btn')[0].click();
		</script>";
	}
	else
	{
		MySQLRollBack($conn);
		closeConnection($conn);
		AlertRightCornerError("Error While Processing Request !");
		echo "<script>$('#button_form_hisab').attr('disabled',false);</script>";
		exit();
	}
}
else
{
	$pay_amount = abs($closing_balance_db);	
		
	StartCommit($conn);
	$flag = true;	

	$update_hisab_paid = Qry($conn,"UPDATE dairy.driver_book SET credit='$pay_amount',desct='HISAB-PAID',balance=0 WHERE id='$hisab_row_id'");

	if(!$update_hisab_paid){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
		
	$update_d_balance = Qry($conn,"UPDATE dairy.driver_up SET amount_hold=amount_hold+'$pay_amount' WHERE code='$driver_code' 
	ORDER BY id DESC LIMIT 1");
		
	if(!$update_d_balance){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
		
	$updt_bal_rows = Qry($conn,"UPDATE dairy.driver_book SET balance=balance+'$pay_amount' WHERE id>'$hisab_row_id' AND driver_code='$driver_code' 
	AND desct NOT IN('DRIVER_UP','DRIVER_DOWN')"); 
			
	if(!$updt_bal_rows){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}

	$updt_bal_rows1 = Qry($conn,"UPDATE dairy.driver_book SET credit=credit+'$pay_amount' WHERE id>'$hisab_row_id' AND driver_code='$driver_code' 
	AND desct NOT IN('DRIVER_UP','DRIVER_DOWN')"); 
			
	if(!$updt_bal_rows1){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
		
	$get_opening_closing_id = Qry($conn,"SELECT id FROM dairy.opening_closing WHERE trip_no='$trip_no'");
		
	if(!$get_opening_closing_id){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}

	if(numRows($get_opening_closing_id)==0)
	{
		$flag = false;
		errorLog("Opening closing not found. Trip-no: $trip_no.",$conn,$page_name,__LINE__);
	}

	$row_opn_cls = fetchArray($get_opening_closing_id);

	$update_opening_bal = Qry($conn,"UPDATE dairy.opening_closing SET opening=opening+'$pay_amount' WHERE id>'$row_opn_cls[id]' AND driver='$driver_code'");
			
	if(!$update_opening_bal){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}

	$update_closing_bal = Qry($conn,"UPDATE dairy.opening_closing SET closing=closing+'$pay_amount' WHERE id>='$row_opn_cls[id]' AND driver='$driver_code'");
			
	if(!$update_closing_bal){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}

	$cash_vou_id = 0;
	$rtgs_vou_id = 0;

	if($cash_pay>0)
	{
		$check_bal = Qry($conn,"SELECT balance,balance2 FROM user WHERE username='$branch'");

		if(!$check_bal){
			$flag = false;
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		}

		if(numRows($check_bal)==0)
		{
			$flag = false;
			errorLog("Branch balance not found.",$conn,$page_name,__LINE__);
		}

		$row_bal = fetchArray($check_bal);

		$rrpl_cash = $row_bal['balance'];
		$rr_cash = $row_bal['balance2'];

		if($company=='RRPL' && $rrpl_cash>=$cash_pay)
		{
			$newbal_cashbook = $rrpl_cash - $cash_pay;
			$debit = 'debit';			
			$balance_cashbook = 'balance';
			
		}
		else if($company=='RAMAN_ROADWAYS' && $rr_cash>=$cash_pay)
		{
			$newbal_cashbook = $rr_cash - $cash_pay;
			$debit = 'debit2';			
			$balance_cashbook = 'balance2';
		}
		else
		{
			$flag = false;
			errorLog("Company not found. or insufficient Balance.",$conn,$page_name,__LINE__);
		}

		$get_vou_id = GetVouId("T",$conn,"mk_tdv","tdvid",$branch);
		
		if(!$get_vou_id || $get_vou_id=="0" || $get_vou_id=="")
		{
			$flag = false;
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		}
		
		$tids = $get_vou_id;
		
		$update_cashbook=Qry($conn,"INSERT INTO cashbook(user,user_code,date,vou_date,comp,vou_no,vou_type,desct,`$debit`,`$balance_cashbook`,
		timestamp) VALUES ('$branch','ADMIN','$date','$hisab_date','$company','$tids','Truck_Voucher','HISAB-PAYMENT : $nrr',
		'$cash_pay','$newbal_cashbook','$hisab_timestamp')");

		if(!$update_cashbook){
			$flag = false;
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		}	

		$query_update = Qry($conn,"update user set `$balance_cashbook`='$newbal_cashbook' where username='$branch'");

		if(!$query_update){
			$flag = false;
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		}	

		if(AffectedRows($conn)==0)
		{
			$flag = false;
			errorLog("ZERO ROWS AFFECTED BALANCE UPDATE.",$conn,$page_name,__LINE__);
		}

		$insert_vou = Qry($conn,"INSERT INTO mk_tdv(user,branch_user,company,tdvid,date,newdate,truckno,dname,amt,mode,hisab_vou,dest,timestamp) 
		VALUES ('$branch','ADMIN','$company','$tids','$date','$hisab_date','$tno','$driver_name','$cash_pay','CASH','1','HISAB-PAYMENT : $nrr',
		'$hisab_timestamp')");	

		if(!$insert_vou){
			$flag = false;
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		}
		
		$cash_vou_id = getInsertID($conn);
	}

	if($rtgs_pay>0)
	{
		if($company=='RRPL'){
			$debit='debit';		
		}
		else if($company=='RAMAN_ROADWAYS'){
			$debit='debit2';
		}

		$get_vou_id = GetVouId("T",$conn,"mk_tdv","tdvid",$branch);
		
		if(!$get_vou_id || $get_vou_id=="0" || $get_vou_id=="")
		{
			$flag = false;
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		}
		
		$tids = $get_vou_id;
		
		$insert_vou = Qry($conn,"INSERT INTO mk_tdv(user,branch_user,company,tdvid,date,newdate,truckno,dname,amt,mode,hisab_vou,ac_name,ac_no,
		bank,ifsc,pan,dest,timestamp) VALUES ('$branch','ADMIN','$company','$tids','$date','$hisab_date','$tno','$driver_name',
		'$rtgs_pay','NEFT','1','$acname','$acno','$bank_name','$ifsc','','HISAB-PAYMENT : $nrr','$hisab_timestamp')");	

		if(!$insert_vou){
			$flag = false;
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		}	

		$rtgs_vou_id = getInsertID($conn);
		
		$update_passbook=Qry($conn,"INSERT INTO passbook(user,vou_no,date,vou_date,comp,vou_type,desct,`$debit`,timestamp) 
		VALUES ('$branch','$tids','$date','$hisab_date','$company','Truck_Voucher','HISAB-PAYMENT : $nrr','$rtgs_pay','$hisab_timestamp')");

		if(!$update_passbook){
			$flag = false;
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		}	

		if($company=='RRPL')
		{
			$get_Crn = GetCRN("RRPL-T",$conn);
			if(!$get_Crn || $get_Crn=="0" || $get_Crn=="")
			{
				$flag = false;
				errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			}
			
			$crnnew = $get_Crn;		
		}
		else if($company=='RAMAN_ROADWAYS')
		{
			$get_Crn = GetCRN("RR-T",$conn);
			if(!$get_Crn || $get_Crn=="0" || $get_Crn=="")
			{
				$flag = false;
				errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			}
			
			$crnnew = $get_Crn;
		}

		$qry_rtgs=Qry($conn,"INSERT INTO rtgs_fm(fno,branch,com,totalamt,amount,acname,acno,bank_name,ifsc,pay_date,fm_date,tno,type,
		crn,timestamp) VALUES ('$tids','$branch','$company','$rtgs_pay','$rtgs_pay','$acname','$acno','$bank_name','$ifsc','$date',
		'$hisab_date','$tno','TRUCK_ADVANCE','$crnnew','$hisab_timestamp')");

		if(!$qry_rtgs){
			$flag = false;
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		}
	}

	$update_log_hisab = Qry($conn,"UPDATE dairy.log_hisab SET hisab_type='2',row_cfwd='0',row_pay='$hisab_row_id',cash_vou='$cash_vou_id',
	rtgs_vou='$rtgs_vou_id' WHERE trip_no='$trip_no'");

	if(!$update_log_hisab){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
	 
	if($flag)
	{
		MySQLCommit($conn);
		closeConnection($conn);
		
		AlertRightCornerSuccessFadeFast("Update Success !");
		echo "<script>
			$('#hisab_edit_modal_cls_btn')[0].click();
			$('#search_add_btn')[0].click();
		</script>";
	}
	else
	{
		MySQLRollBack($conn);
		closeConnection($conn);
		AlertRightCornerError("Error While Processing Request !");
		echo "<script>$('#button_form_hisab').attr('disabled',false);</script>";
		exit();
	}
}
?>