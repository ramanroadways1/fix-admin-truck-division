<?php
require_once 'connect.php';

$date = date("Y-m-d");
$timestamp = date("Y-m-d H:i:s");

$vou_no = escapeString($conn,(trim($_POST['vou_no'])));

if(empty($vou_no)){
	AlertRightCornerError("Voucher not found !");
	exit();
}

$sql = Qry($conn,"SELECT id FROM freight_form_lr WHERE lrno IN(SELECT lrno FROM freight_form_lr WHERE frno='$vou_no') AND frno!='$vou_no'");

if(!$sql){
	AlertErrorTopRight("Error while processing request !");
	errorLog(getMySQLError($conn),$conn,$page_url,__LINE__);
	exit();
}

if(numRows($sql) > 0)
{
	AlertErrorTopRight("Multiple voucher found !");
	exit();
}

$chk_dsl_req = Qry($conn,"SELECT id FROM lr_sample WHERE lrno IN(SELECT lrno FROM freight_form_lr WHERE frno='$vou_no') AND diesel_req>0");

if(!$chk_dsl_req){
	AlertErrorTopRight("Error while processing request !");
	errorLog(getMySQLError($conn),$conn,$page_url,__LINE__);
	exit();
}

if(numRows($chk_dsl_req) > 0)
{
	AlertErrorTopRight("Diesel request found ! Delete diesel request first.");
	exit();
}

$chk_brk_lr = Qry($conn,"SELECT id FROM lr_sample WHERE lrno IN(SELECT lrno FROM freight_form_lr WHERE frno='$vou_no') AND break>0");

if(!$chk_brk_lr){
	AlertErrorTopRight("Error while processing request !");
	errorLog(getMySQLError($conn),$conn,$page_url,__LINE__);
	exit();
}

if(numRows($chk_brk_lr) > 0)
{
	AlertErrorTopRight("Unable to delete ! Breaking LR found.");
	exit();
}

$chk_fm = Qry($conn,"SELECT ptob FROM freight_form WHERE frno='$vou_no'");

if(!$chk_fm){
	AlertErrorTopRight("Error while processing request !");
	errorLog(getMySQLError($conn),$conn,$page_url,__LINE__);
	exit();
}

if(numRows($chk_fm) > 0)
{
	$row_fm = fetchArray($chk_fm);
	
	if($row_fm['ptob']!='')
	{
		AlertErrorTopRight("Reset FM advance first !");
		exit();
	}
}

$chk_trip = Qry($conn,"SELECT done FROM freight_form_lr WHERE frno='$vou_no'");

if(!$chk_trip){
	AlertErrorTopRight("Error while processing request !");
	errorLog(getMySQLError($conn),$conn,$page_url,__LINE__);
	exit();
}

if(numRows($chk_trip) == 0)
{
	AlertErrorTopRight("Voucher not found !");
	exit();
}

$row_trip = fetchArray($chk_trip);

if($row_trip['done']=="1")
{
	AlertErrorTopRight("Delete e-diary trip frist !");
	exit();
}

StartCommit($conn);
$flag = true;

$update_lr_sample = Qry($conn,"UPDATE lr_sample SET crossing='' WHERE lrno IN(SELECT lrno FROM freight_form_lr WHERE frno='$vou_no')");

if(!$update_lr_sample){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

$copy_to_lr_cache = Qry($conn,"INSERT INTO lr_sample_pending(dl_id,oxygen_lr,lr_id,company,branch,branch_user,lrno,bilty_type,lr_type,wheeler,
date,create_date,truck_no,fstation,tstation,billing_station,billing_station_id,dest_zone,consignor,consignor_before,bill_to,bill_to_id,consignee,
shipment_party,shipment_party_id,sub_consignor,con2_addr,from_id,to_id,con1_id,sub_con1_id,con2_id,zone_id,po_no,bank,freight_type,sold_to_pay,
lr_name,wt12,gross_wt,weight,weight2,bill_rate,bill_amt,t_type,do_no,shipno,invno,invoice_value,item,item_id,item_name,plant,articles,
goods_desc,goods_value,con1_gst,sub_con1_gst,con2_gst,shipment_party_gst,hsn_code,timestamp,crossing,cancel,break,diesel_req,nrr,downloadid,
downtime,billrate,billparty,partyname,pod_rcv_date,download,billfreight,lrstatus) SELECT dl_id,oxygen_lr,id,company,branch,branch_user,
lrno,bilty_type,lr_type,wheeler,date,create_date,truck_no,fstation,tstation,billing_station,billing_station_id,dest_zone,consignor,
consignor_before,bill_to,bill_to_id,consignee,shipment_party,shipment_party_id,sub_consignor,con2_addr,from_id,to_id,con1_id,sub_con1_id,
con2_id,zone_id,po_no,bank,freight_type,sold_to_pay,lr_name,wt12,gross_wt,weight,weight2,bill_rate,bill_amt,t_type,do_no,shipno,invno,
invoice_value,item,item_id,item_name,plant,articles,goods_desc,goods_value,con1_gst,sub_con1_gst,con2_gst,shipment_party_gst,hsn_code,
timestamp,crossing,cancel,break,diesel_req,nrr,downloadid,downtime,billrate,billparty,partyname,pod_rcv_date,download,billfreight,lrstatus 
FROM lr_sample WHERE lrno IN(SELECT lrno FROM freight_form_lr WHERE frno='$vou_no')");

if(!$copy_to_lr_cache){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

$dlt_f_lr = Qry($conn,"DELETE FROM freight_form_lr WHERE frno='$vou_no'");

if(!$dlt_f_lr){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

$dlt_fm = Qry($conn,"DELETE FROM freight_form WHERE frno='$vou_no'");

if(!$dlt_fm){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

if($flag)
{
	MySQLCommit($conn);
	closeConnection($conn);
	AlertRightCornerSuccess("Deleted successfully !");
	echo "<script>$('#add_btn')[0].click();</script>";
	exit();
}
else
{
	MySQLRollBack($conn);
	closeConnection($conn);
	AlertErrorTopRight("Error while processing request !");
	exit();
}	
?>