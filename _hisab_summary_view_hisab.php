<?php
require("./connect.php");

if(!isset($_POST['hisab_id']))
{
	echo "No record found..";
	exit();
}

$hisab_id = escapeString($conn,strtoupper($_POST['hisab_id']));

$get_hisab = Qry($conn,"SELECT h.trip_no,h.opening,h.opening_branch,h.opening_date,h.closing,h.closing_branch,h.closing_date,h.gps_km_of_trip,h.avg,
h.hisab_check,l.tno,l.driver,l.branch as hisab_branch,l.timestamp as hisab_datetime,truck.comp
FROM dairy.opening_closing as h 
LEFT OUTER JOIN dairy.log_hisab AS l ON l.trip_no=h.trip_no 
LEFT OUTER JOIN dairy.own_truck AS truck ON truck.tno=l.tno 
WHERE h.id='$hisab_id'");

if(!$get_hisab)
{
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while Processing Request","./hisab_summary.php");
	exit();
}

if(numRows($get_hisab)==0)
{
	echo "Hisab not found..";
	exit();
}

$row_main = fetchArray($get_hisab);

$trip_no = $row_main['trip_no'];
$company = $row_main['comp'];
$opening_balance = $row_main['opening'];
$closing_balance = $row_main['closing'];
$gps_km = $row_main['gps_km_of_trip'];
$tno = $row_main['tno'];
$opening_date = $row_main['opening_date'];
$closing_date = $row_main['closing_date'];
$hisab_datetime = $row_main['hisab_datetime'];
$hisab_branch = $row_main['hisab_branch'];
$driver_code = $row_main['driver'];
$avg = $row_main['avg'];

$get_trip = Qry($conn,"SELECT 
(SELECT from_station FROM dairy.trip_final WHERE trip_no='$trip_no' ORDER BY id ASC LIMIT 1) as 'from_station',
(SELECT to_station FROM dairy.trip_final WHERE trip_no='$trip_no' ORDER BY id DESC LIMIT 1) as 'to_station',
(SELECT trip_id FROM dairy.trip_final WHERE trip_no='$trip_no' ORDER BY id ASC LIMIT 1) as 'trip_id',
(SELECT date(date) FROM dairy.trip_final WHERE trip_no='$trip_no' ORDER BY id ASC LIMIT 1) as 'start_date',
(SELECT date(hisab_date) FROM dairy.trip_final WHERE trip_no='$trip_no' ORDER BY id ASC LIMIT 1) as 'hisab_date',
(SELECT date(end_date) FROM dairy.trip_final WHERE trip_no='$trip_no' ORDER BY id DESC LIMIT 1) as 'end_date',
(SELECT desct FROM dairy.driver_book WHERE trip_no='$trip_no' AND desct like 'DA%') as 'da_desc',
(SELECT debit FROM dairy.driver_book WHERE trip_no='$trip_no' AND desct like 'DA%') as 'da_amount',
(SELECT desct FROM dairy.driver_book WHERE trip_no='$trip_no' AND desct like 'SALARY%') as 'sal_desc',
(SELECT debit FROM dairy.driver_book WHERE trip_no='$trip_no' AND desct like 'SALARY%') as 'sal_amount',
(SELECT debit FROM dairy.driver_book WHERE trip_no='$trip_no' AND desct='EXP-TELEPHONE') as 'tel_exp_amount',
(SELECT credit FROM dairy.driver_book WHERE trip_no='$trip_no' AND desct='TRISHUL_CARD_CREDIT') as 'trishul_credit',
(SELECT credit FROM dairy.driver_book WHERE trip_no='$trip_no' AND desct='ASSET_FORM_CREDIT') as 'asset_form_credit',
(SELECT sum(debit) FROM dairy.driver_book WHERE trip_no='$trip_no' AND desct='CASH_DIESEL') as 'cash_diesel'
");

if(!$get_trip)
{
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while Processing Request","./hisab_summary.php");
	exit();
}

if(numRows($get_trip)==0)
{
	Redirect("NO RESULT FOUND.","./hisab_summary.php");
	exit();
}

$get_driver_name = Qry($conn,"SELECT name FROM dairy.driver WHERE code='$driver_code'");

if(!$get_driver_name)
{
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while Processing Request","./hisab_summary.php");
	exit();
}

$row_driver_name = fetchArray($get_driver_name);
$driver_name = $row_driver_name['name'];

$row_get_data = fetchArray($get_trip);

$from_station = $row_get_data['from_station'];
$to_station = $row_get_data['to_station'];
$trip_id = $row_get_data['trip_id'];
$start_date = $row_get_data['start_date'];
$end_date = $row_get_data['end_date'];
$da_desc = $row_get_data['da_desc'];
$da_amount = $row_get_data['da_amount'];
$sal_desc = $row_get_data['sal_desc'];
$sal_amount = $row_get_data['sal_amount'];

$hisab_date = date('d/m/Y', strtotime($row_get_data['hisab_date']));

$hisab_date2=$row_get_data['hisab_date'];

$diff = (strtotime($end_date)- strtotime($start_date))/24/3600; 
$diff_days=$diff+1;

$hisab_type = Qry($conn,"SELECT desct,timestamp FROM dairy.driver_book WHERE trip_no='$trip_no' AND desct like 'HISAB%'");

if(!$hisab_type)
{
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while Processing Request","./hisab_summary.php");
	exit();
}

$hisab_rows=numRows($hisab_type);

if($hisab_rows==0)
{
	$hisab_type_cr_ho = Qry($conn,"SELECT desct,debit FROM dairy.driver_book WHERE trip_no='$trip_no' AND desct='CREDIT-HO'");
	if(!$hisab_type_cr_ho){
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		Redirect("Error While Processing Request.","./hisab_summary.php");
		exit();
	}

	if(numRows($hisab_type_cr_ho)==0)
	{
		echo "<script type='text/javascript'>
				alert('NO HISAB FOUND.');
				window.close();
			</script>";
		exit();
	}
	else
	{
		$hisab_type_db = "CREDIT_HO";
	}
}
else
{
	$hisab_type_db = "HISAB";
}


if($hisab_type_db=='HISAB')
{
	
	$hisab_types=array();

	while($row_hisab=fetchArray($hisab_type))
	{
		$hisab_types[]=$row_hisab['desct'];
		$hisab_time=$row_hisab['timestamp'];
	}

	$hisab_time=$hisab_time;

	$hisab_types=implode(" + ",$hisab_types);

	if(strpos($hisab_types,'HISAB-PAID')!== false)
	{
		$vouchers=Qry($conn,"SELECT tdvid,amt,mode FROM mk_tdv WHERE newdate='$hisab_date2' AND timestamp='$hisab_time' AND 
		truckno='$tno'");
		
		if(!$vouchers){
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			Redirect("Error While Processing Request.","./");
			exit();
		}
		
		if(numRows($vouchers)==0)
		{
			errorLog("HISAB Voucher not found. Truck No : $tno. Trip No : $trip_no.",$conn,$page_name,__LINE__);
			Redirect("HISAB Voucher not found.","../");
			exit();
		}
		
		$voucher_types=array();
		
		while($row_vouchers=fetchArray($vouchers))
		{
			if($row_vouchers['mode']=='NEFT')
			{
				$chk_utr=Qry($conn,"SELECT bank,utr_date FROM rtgs_fm WHERE fno='$row_vouchers[tdvid]'");
				if(!$chk_utr){
					errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
					Redirect("Error While Processing Request.","./");
					exit();
				}
				
				$row_utr = fetchArray($chk_utr);
				
				if($row_utr['bank']!='')
				{
					$utr_date = date("d-m-y",strtotime($row_utr['utr_date']));
					
					$voucher_types[]=$row_vouchers['tdvid']."=> ".$row_vouchers['amt']." (".$row_vouchers['mode']."- Date: $utr_date)";
				}
				else
				{
					$voucher_types[]=$row_vouchers['tdvid']."=> ".$row_vouchers['amt']." (".$row_vouchers['mode']."- Payment Pending)";
				}
			}
			else
			{
				$voucher_types[]=$row_vouchers['tdvid']."=> ".$row_vouchers['amt']." (".$row_vouchers['mode'].")";
			}
		}
		
		$voucher_types=implode(", ",$voucher_types);
	}
	else
	{
		$voucher_types="";
	}

}
else
{
	$fetch_record_ho_cr = fetchArray($hisab_type_cr_ho);
	
	$Cr_amount = $fetch_record_ho_cr['debit'];
	
	$hisab_types="CREDIT-HO";
	$voucher_types="Amount : $Cr_amount.";
}

$credit_debit=Qry($conn,"SELECT SUM(credit) as credit,SUM(debit) as debit FROM dairy.driver_book WHERE trip_no='$trip_no'");
if(!$credit_debit)
{
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while Processing Request","./hisab_summary.php");
	exit();
}

$row_cr_dr=fetchArray($credit_debit);
$credit=$row_cr_dr['credit'];
$debit=$row_cr_dr['debit'];
?>
<!doctype html>
<html>
<head>
	<meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate" />
	<meta http-equiv="Pragma" content="no-cache" />
	<meta http-equiv="Expires" content="0" />
	<meta name="robots" content="noindex,nofollow"/>
	<title>View Hisab : <?php echo $trip_no; ?></title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	
	
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
    <script src="//code.jquery.com/jquery-1.10.2.js"></script>
    <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>  
	<meta name="google" content="notranslate">
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:600&display=swap" rel="stylesheet">
</head>

<body style="font-family: 'Open Sans', sans-serif !important" onkeypress="return disableCtrlKeyCombination(event);" onkeydown = "return disableCtrlKeyCombination(event);">

<style>
.form-control
	{
		border:1px solid #000;
		text-transform:uppercase;
	}
	.modal { overflow: auto !important; }
</style>

<style>
th {
  white-space: normal !important; 
  word-wrap: break-word;  
}
td {
  white-space: normal !important; 
  word-wrap: break-word;  
}
table {
  table-layout: fixed;
}
</style>

<style>
.ui-autocomplete { z-index:2147483647; }
 
.div1{
	border-right:5px solid red;
} 
</style>

<script>
function FetchTripData(id)
{
	$('#loadicon').show();
	$('.main').attr('class','form-group col-md-12 main');
	$('#'+id).attr('class','form-group col-md-12 div1 main');
	 jQuery.ajax({
		url: "hisab_summary_fetch_trip_data.php",
		data: 'trip_id=' + id,
		type: "POST",
		success: function(data) {
			$("#trip_data_div").html(data);
		},
		error: function() {}
	});
}
</script>

<style>
.modal2 {
  text-align: center;
  padding: 0!important;
}

.modal2:before {
  content: '';
  display: inline-block;
  height: 100%;
  vertical-align: middle;
  margin-right: -4px;
}

.modal-dialog2 {
  display: inline-block;
  text-align: left;
  vertical-align: middle;
}
</style>

<button style="margin:10px;" type="button" onclick="window.close();" class="btn btn-sm btn-primary">Close window</button>

<div class="container-fluid" style="font-family: 'Open Sans', sans-serif !important">

<div class="row">
		<div class="form-group col-md-12">
		<div style="background-color:#DDD !important;color:#000;" class="form-group col-md-12">
			<center><h5 style="padding:5px;font-size:15px;">
			Trip No : <?php echo $trip_no; ?>, 
			<?php echo $from_station." to ".$to_station." (".$diff_days." days)"; ?>
			</h5></center>
		</div>
			<table class="table table-bordered" style="font-size:11px;">
				<tr>
					<th>Truck No</th><td><?php echo $tno; ?></td>
					<th>Trip No</th><td><?php echo $trip_no; ?></td>
					<th>Driver Name</th><td colspan="4"><?php echo $driver_name; ?></td>
				</tr>	
				
				<tr>
					<th>Hisab By </th><td><?php echo $hisab_date." (".$hisab_branch.")"; ?></td>
					<th>Trip Start</th><td><?php echo date('d/m/y', strtotime($start_date)); ?></td>
					<th>Trip END</th><td><?php echo date('d/m/y', strtotime($end_date)); ?></td>
					<th>Trip Duration</th><td colspan="2"><?php echo $diff_days." days"; ?></td>
				</tr>
				
				<tr>
					<th>Opening Bal</th><td><?php echo $opening_balance; ?></td>
					<th>Credit</th><td><?php echo $credit; ?></td>
					<th>Debit</th><td><?php echo $debit; ?></td>
					<th>Closing Bal</th><td colspan="2"><?php echo $closing_balance; ?></td>
				</tr>
				
				<tr>
					<th>DA </th><td colspan="2"><?php echo $da_amount." : ".$da_desc; ?></td>
					<th>SALARY </th><td colspan="2"><?php echo $sal_amount." : ".$sal_desc; ?></td>
					<th>Telephone & Air-Grease </th><td colspan="2"><?php echo $row_get_data['tel_exp_amount']*2; ?></td>
				</tr>
				
				<tr>
					<th>GPS KM </th><td><?php echo "<font size='3'>".$gps_km." KM </font>"; ?></td>
					<th>Average</th><td><?php echo "<font size='3'>".$avg." kmpl </font>"; ?></td>
					<th>Hisab Description :</th><td colspan="4"><?php echo $hisab_types."<br>".$voucher_types; ?></td>
				</tr>
				
				<tr>
					<th colspan="5">Other Credits : 
					<?php if($row_get_data['trishul_credit']>0){ echo "Trishul-Card credit : $row_get_data[trishul_credit]"; }; ?>
					<?php if($row_get_data['asset_form_credit']>0){ echo "&nbsp; Asset-Form credit : $row_get_data[asset_form_credit]"; }; ?>
					</th>
					<th colspan="4">Other Debits : 
					<?php if($row_get_data['cash_diesel']>0){ echo "Cash Diesel : $row_get_data[cash_diesel]"; }; ?>
					</th>
				</tr>
				
			</table>
		</div>	
</div>	
			
<div class="row">	
		<div class="form-group col-md-4" style="overflow:auto">
			<?php
			$trip_chk=Qry($conn,"SELECT trip_id,from_station,to_station,from_id,to_id,km,pincode,act_wt,charge_wt,lr_type,lr_type2,
			date(date) as trip_date,lrno,cash,diesel_qty FROM dairy.trip_final WHERE trip_no='$trip_no' ORDER by id ASC");
			
			if(numRows($trip_chk)>0)
			{
				while($row_trip=fetchArray($trip_chk))
				{
					echo "<input type='hidden' id='FromLoc$row_trip[trip_id]' value='$row_trip[from_station]'>";
					echo "<input type='hidden' id='ToLoc$row_trip[trip_id]' value='$row_trip[to_station]'>";
				
					$LRNo1=array();
					foreach(explode(",",$row_trip['lr_type']) as $lr_list)
					{
						if(substr($lr_list,3,1)=='M')
						{
							$LRNo1[]="<a href='#' style='color:#03F' onclick=ViewMB('$lr_list')>$lr_list</a>";
						}
						else
						{
							$LRNo1[]=$lr_list;
						}
					}
					
					echo "<div id='$row_trip[trip_id]' style='border-radius:10px;background:#FFFF33;
						color:#000;font-size:10px;padding:10px;' onclick='FetchTripData($row_trip[trip_id])' class='form-group main col-md-12'>
						
						<table border='0' style='width:100%;font-size:10px;'>
						<tr>
							<td>From: $row_trip[from_station]</td>
							<td></td>	
							<td>To: $row_trip[to_station]</td>
						</tr>
						
						<tr>
							<td>Act Wt: $row_trip[act_wt]</td>
							<td>Chrg Wt: $row_trip[charge_wt]</td>
							<td>Date: ".convertDate("d-m-y",$row_trip['trip_date'])."</td>
						</tr>
						
						<tr>
							<td colspan='3'>LR Type: ".implode(", ",$LRNo1)."</td>
						</tr>
						
						<tr>
							<td colspan='3'>LR No: $row_trip[lrno]</td>
						</tr>
						
						<tr>
							<td>Advance : <span id=''>$row_trip[cash]</span></td>
							<td></td>
							<td>Diesel : <span id=''>$row_trip[diesel_qty] Ltr</span></td>
						</tr>
					</table>
				</div>";	
					
				}
				
			}
			else
			{
				echo "
				<div class='form-group col-md-12' style='color:red'>No TRIP Found.</div>";
			}
			?>
		
	</div>

<script>	
function PrintTrip(id)
{
	$("#trip_id_print").val(id);
	
	function submitform22()
		{
			document.getElementById('PrintForm1').submit();
		}
	submitform22();	
}

function ViewMB(mbno)
{
	$('#loadicon').show();
	jQuery.ajax({
		url: "./load_market_bilty_for_view.php",
		data: 'mbno=' + mbno,
		type: "POST",
		success: function(data) {
			$("#modal_res1").html(data);
		},
		error: function() {}
	});
}
</script>	

<div id="modal_res1"></div>

	<div class="col-md-8">
		<div class="form-group col-md-12 bg-primary" style="color:#FFF;padding:5px;">
			<span>Trip Summary : <span id="station_div"></span> &nbsp; <span id="print_button_div"></span></span>
		</div>
		
		<div class="row" id="trip_data_div">	
		</div>
	</div>
	
</div>
</div>

</body>
</html>

<form action='./print/single_trip.php' target="_blank" id='PrintForm1' method='POST'>
	<input type='hidden' id="trip_id_print" name='trip_id_print'>
	<input type='hidden' value="<?php echo $driver_name; ?>" name='driver_name'>
</form>

<form action='./mkt_bilty_show.php' method='POST' id="ViewMBForm" target='_blank'>
	<input type='hidden' name='bilty_no' id="market_bilty_number" />
</form>

<script>
FetchTripData('<?php echo $trip_id; ?>');
</script>