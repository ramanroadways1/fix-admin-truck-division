<?php
require_once("connect.php");

$date = date("Y-m-d");
$timestamp = date("Y-m-d H:i:s");

$id = escapeString($conn,($_POST['id']));

$chk_lane = Qry($conn,"SELECT l.id,l.tno,l.model,c1.name as con1,c1.gst as con1_gst,l.empty_loaded,l.truck_type,l.is_active,l.total_exp,l.adv_amount,
l.fix_diesel_type,l.diesel_value,(SELECT COUNT(id) FROM dairy.own_truck WHERE model=l.model) as total_vehicles,(SELECT COUNT(id) 
FROM dairy.fix_lane_model_escap WHERE model=l.model) as escaped_veh,c2.name as con2,c2.gst as con2_gst,l.multi_del_consignee 
FROM dairy.fix_lane_rules AS l 
LEFT OUTER JOIN consignor as c1 ON c1.id=l.consignor 
LEFT OUTER JOIN consignee as c2 ON c2.id=l.consignee 
WHERE l.lane_id='$id'");

if(!$chk_lane){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error.","./");
	exit();
}

echo '<table class="table table-bordered table-striped" style="font-size:12px">
	<tr>
		<th>#</th>
		<th>Vehicle_No.</th>
		<th>Model</th>
		<th>Consignor & Consignee</th>
		<th>Empty/Loaded</th>
		<th>Remove</th>
		<th>Expense</th>
		<th>Advance & Diesel</th>
	</tr>';
	
if(numRows($chk_lane)==0)
{
	echo "<tr><td colspan='8'>Record added yet !</td></tr>";
}
else
{
$i_sn=1;

while($row = fetchArray($chk_lane))
{
					if($row['empty_loaded']=="0"){
						$empty_loaded="Empty";
					}
					else if($row['empty_loaded']=="1"){
						$empty_loaded="Loaded";
					}
					else if($row['empty_loaded']=="2"){
						$empty_loaded="Both";
					}
					else{
						$empty_loaded="";
					}
					
				if($row['multi_del_consignee']!='')
				{
					$con1_con2="Consignor: <a style='color:blue' href='#'>$row[con1] ($row[con1_gst])</a>
					<br>
					Consignee: <a style='color:blue' href='#'>$row[con2] ($row[con2_gst])</a>
					<br>";	
					
					$get_consignees = Qry($conn,"SELECT id,name,gst FROM consignee WHERE 
					FIND_IN_SET(id,(SELECT multi_del_consignee from dairy.fix_lane_rules WHERE id='$row[id]'))");
					
					while($row_con2 = fetchArray($get_consignees))
					{
						$con1_con2.="
						<input type='hidden' id='consignee_full_name_$row_con2[id]' value='$row_con2[name]' />
						<a id='remove_link_id_$row_con2[id]' style='color:maroon' onclick=RemoveConsignee('$row[id]','$id','$row_con2[id]') href='#'>$row_con2[name] ($row_con2[gst])</a><br>";
					}
					
					$con1_con2.="<button class='btn btn-primary btn-xs' onclick=AddMultiDelConsignee('$row[id]','$id') type='button'>
					Add consignee</button>";
				}
				else
				{
					$con1_con2="Consignor: <a style='color:blue' href='#'>$row[con1] ($row[con1_gst])</a>
					<br>
					Consignee: <a style='color:blue' href='#'>$row[con2] ($row[con2_gst])</a><br>";
					
					if($row['con2']!='')
					{
						$con1_con2.="<button class='btn btn-primary btn-xs' onclick=AddMultiDelConsignee('$row[id]','$id') type='button'>Add consignee</button>";
					}
				}
						
					echo "<tr class='tr_' id='tr_$row[id]'>
						<td>$i_sn <img class='img_right' id='img_right_$row[id]' src='icon_right.png' 
						style='display:none;width:18px;height:15px'></td>
						<td>$row[tno]</td>
						<td>$row[model]";
						if($row['model']!='')
						{
							// $get_total_vehicles = Qry($conn,"SELECT id FROM dairy.own_truck WHERE model='$row[model]'");
							// $get_disabled_vehicles = Qry($conn,"SELECT id FROM dairy.fix_lane_model_escap WHERE model='$row[model]'");
							
							// $active_veh = (numRows($get_total_vehicles)-numRows($get_disabled_vehicles))."/".numRows($get_total_vehicles);
							
							$active_veh = ($row['total_vehicles']-$row['escaped_veh'])."/".$row['total_vehicles'];
							
							echo "<input type='hidden' id='model_name_$row[id]' value='$row[model]'>";
							echo "<br><button type='button' style='margin-top:3px' onclick=ViewModelVehicles('$row[id]','$id') class='btn btn-primary btn-xs'><span class='fa fa-search'></span> View Vehicles $active_veh</button>";
						}
						echo "</td>
						<td>$con1_con2</td>
						<td>$empty_loaded</td>
						<td><button id='dlt_rule_btn_$row[id]' onclick='DeleteRule($row[id])' type='button' class='btn_delete_rule btn btn-xs btn-danger'><span class='fa fa-trash'></span></button></td>
						<td>
							<button onclick=ExpLoad('$row[id]','$row[empty_loaded]') type='button' class='btn btn-xs btn-primary'>
							<span class='fa fa-rupee-sign'></span> Expense (<span id='exp_count_$row[id]'>$row[total_exp]</span>)</button>
						</td>
						<td>
						<button onclick='AdvLoad($row[id])' type='button' class='btn_adv_update1 btn btn-xs btn-primary'>₹ Advance</button>
						<br>
						Adv: <span id='advance_value_row_$row[id]'>$row[adv_amount]</span>
						<br>
						Dsl: <span id='diesel_value_row_$row[id]'>$row[diesel_value]";if($row['fix_diesel_type']!='') { echo "($row[fix_diesel_type])"; }
						echo "</span></td>
					</tr>";
				$i_sn++;	
		}
}

echo "</table>

<script>
	$('#lane_id_modal').val('$id');
	$('#loadicon').fadeOut('slow');
</script>";

$qry_rights_update = Qry($conn,"SELECT id FROM _access_control WHERE username='$_SESSION[ediary_fix_admin]' AND func_id=(SELECT id FROM 
_access_control_func_list WHERE session_role='8' AND func_name='Fix_Lane') AND u_insert='1' AND u_update='1'");
							  
if(numRows($qry_rights_update)==0)
{
	echo "<script>
		$('.btn_adv_update1').hide();
		$('.btn_adv_update1').attr('onclick','');
	</script>";
}	

$qry_rights_delete = Qry($conn,"SELECT id FROM _access_control WHERE username='$_SESSION[ediary_fix_admin]' AND func_id=(SELECT id FROM 
_access_control_func_list WHERE session_role='8' AND func_name='Fix_Lane') AND u_delete='1'");
							  
if(numRows($qry_rights_delete)==0)
{
	echo "<script>
		$('.btn_delete_rule').hide();
		$('.btn_delete_rule').attr('onclick','');
	</script>";
}
?>