<?php include("header.php"); ?>

<?php
$chk = Qry($conn,"SELECT id FROM _access_control WHERE username='$_SESSION[ediary_fix_admin]' AND func_id=(SELECT id FROM 
_access_control_func_list WHERE session_role='8' AND func_name='Driver_Approval') AND u_view='1'");
			  
if(numRows($chk)==0)
{
	echo "<script>window.location.href='./';</script>";
	exit();
}
?>

<div class="content-wrapper">
      <section class="content-header">
          <h1 style="font-size:16px;">NEW Driver Approval : </h1>
       </section>
       
	   <section class="content">
          <div class="row">
            <div class="col-xs-12">
			<div class="box">
                <div class="box-body">

				<div class="col-md-12">&nbsp;</div>
				
				<div class="col-md-12 table-responsive" id="load_table">
				</div> 
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

<div id="func_result"></div>  
<div id="load_modal_div"></div>  
 
<button type="button" id="ModalButton" style="display:none" class="btn btn-primary" data-toggle="modal" data-target="#myModal"></button>

<div class="modal fade" id="myModal" data-keyboard="false" data-backdrop="static">
  <div class="modal-dialog modal-lg modal-dialog-centered">
    <div class="modal-content" style="max-height: calc(100vh - 70px);overflow: auto;">

      <div class="bg-primary modal-header">
        <h4 class="modal-title" style="font-size:13px;color:#FFF">Driver : <span style="color:" id="driver_name_html"></span></h4>
      </div>
	<div class="modal-body">
		<iframe class="responsive-iframe" src=""></iframe>
    </div>

      <div class="modal-footer">
        <button type="button" id="close_modal_button" class="btn btn-sm btn-danger" data-dismiss="modal">Close</button>
       </div>
	</div>
  </div>
</div>	

<style>
.responsive-iframe {
  position: absolute;
  top: 0;
  left: 0;
  bottom: 0;
  right: 0;
  width: 100%;
  height: 100%;
}

.modal-body {
  position: relative;
  overflow: hidden;
  width: 100%;
  padding-top: 56.25%; /* 16:9 Aspect Ratio (divide 9 by 16 = 0.5625) */
}
</style>

<script>
function ViewFile(type1,id)
{
	$("#loadicon").show();
	
	if(type1=='PHOTO')
	{
		var link1 = $('#d_photo_'+id).val();
	}
	else if(type1=='LIC_FRONT')
	{
		var link1 = $('#d_lic_front_'+id).val();
	}
	else if(type1=='LIC_REAR')
	{
		var link1 = $('#d_lic_rear_'+id).val();
	}
	else if(type1=='AADHAR_FRONT')
	{
		var link1 = $('#d_aadhar_front_'+id).val();
	}
	else if(type1=='AADHAR_REAR')
	{
		var link1 = $('#d_aadhar_rear_'+id).val();
	}
	
	var driver_name = $('#driver_name_'+id).val();
	$("#myModal iframe").attr("src", "../diary_admin/"+link1);
    $("#driver_name_html").html(driver_name);
    $("#ModalButton")[0].click();
	$("#loadicon").fadeOut('slow');
}
</script>
 
<?php
$chk_update = Qry($conn,"SELECT id FROM _access_control WHERE username='$_SESSION[ediary_fix_admin]' AND func_id=(SELECT id FROM 
_access_control_func_list WHERE session_role='8' AND func_name='Driver_Approval') AND u_update='1'");
			  
if(numRows($chk_update)>0)
{
?>	
<script>	
function Approve(id)
{
	$("#loadicon").show();
	jQuery.ajax({
		url: "add_driver_approval_approve_reject.php",
		data: 'id=' + id + '&type=' + 'approve',
		type: "POST",
		success: function(data) {
		$("#func_result").html(data);
	},
	error: function() {}
	});
}

function Reject(id)
{
	$("#loadicon").show();
	jQuery.ajax({
		url: "add_driver_approval_approve_reject.php",
		data: 'id=' + id + '&type=' + 'reject',
		type: "POST",
		success: function(data) {
		$("#func_result").html(data);
	},
	error: function() {}
	});
}
</script>
<?php
}
?>
	
<script>	
function LoadTable()
{
	$("#loadicon").show();
	jQuery.ajax({
		url: "_load_add_driver_approval.php",
		data: 'id=' + 'ok',
		type: "POST",
		success: function(data) {
			$("#load_table").html(data);
			$('#example').DataTable({ 
				"destroy": true, //use for reinitialize datatable
			});
		},
		error: function() {}
	});
}
 LoadTable();
</script>

<?php include("footer.php") ?>