<?php
include("header.php");

$chk = Qry($conn,"SELECT id FROM _access_control WHERE username='$_SESSION[ediary_fix_admin]' AND func_id=(SELECT id FROM 
_access_control_func_list WHERE session_role='8' AND func_name='Current_Trip_View') AND u_view='1'");
			  
if(numRows($chk)==0)
{
	echo "<script>window.location.href='./';</script>";
	exit();
}
?>
<script>
$(function() {
		$("#hisab_tno").autocomplete({
		source: 'autofill/get_tno.php',
		appendTo: "#LaneRuleModal",
		select: function (event, ui) { 
              $('#hisab_tno').val(ui.item.value);   
             // $("#button1").attr('disabled',false);				   
             return false;
		},
		change: function (event, ui) {
		if(!ui.item){
		    $(event.target).val("");
			alert('Vehicle Number does not exists.');
			$("#hisab_tno").val('');
			$("#hisab_tno").focus();
			// $("#button1").attr('disabled',true);
		}}, 
		focus: function (event, ui){
			return false;}
});});
</script>

<style>
th {
  white-space: normal !important; 
  /* word-wrap: break-word; */
}
td {
  white-space: normal !important; 
  /* word-wrap: break-word;  */
}
table {
  /*table-layout: fixed;*/
}
</style>

<div class="content-wrapper">
      <section class="content-header">
          <h1 style="font-size:16px;">View Running Trip : </h1>
       </section>
       
	   <section class="content">
          <div class="row">
            <div class="col-xs-12">
			<div class="box">
                <div class="box-body">
				
				<div class="col-md-12">&nbsp;</div>
				
				<div class="col-md-12">
					<div class="row">
						<div class="form-group col-md-3">
							<label>Trip Number</label>
							<input autocomplete="off" oninput="this.value=this.value.replace(/[^0-9]/,'');CheckInput('trip_no');" type="text" class="form-control" id="trip_no" />
						</div>
						
						<div class="form-group col-md-1">
							<label>&nbsp;</label>
							<?php if(!isMobile()) { echo "<br />"; } ?>
							<span style="font-size:12px;color:maroon">-- OR --</span>
						</div>
						
						<div class="form-group col-md-3">
							<label>Vehicle Number</label>
							<input autocomplete="off" type="text" oninput="this.value=this.value.replace(/[^A-Za-z0-9]/,'');CheckInput('tno');" class="form-control" id="hisab_tno" />
						</div>
						
						<div class="form-group col-md-2">
							<?php if(!isMobile()) { echo "<label>&nbsp;</label><br />"; } ?>
							<button type="button" style="margin-top:3px" onclick="SearchHisab()" class="btn btn-sm btn-success <?php if(isMobile()) { echo "btn-block"; } ?>" id="add_btn"><i class="fa fa-search" aria-hidden="true"></i> &nbsp; Search Trip</button>
						</div>
					</div>
				</div>
				
<script>				
function CheckInput(elem)
{
	if(elem=='trip_no')
	{
		$('#hisab_tno').val('');
	}
	else
	{
		$('#trip_no').val('');
	}
}
</script>				
				
				<div class="col-md-12">&nbsp;</div>
				
				<div class="col-md-12 table-responsive" style="overflow:auto" id="load_table">
              
				 </div> 
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

<div id="func_result"></div>  
<div id="modal_load_div"></div>  

<script>	
function SearchHisab()
{
	var tno = $('#hisab_tno').val();
	var trip_no = $('#trip_no').val();
	
	if(tno=='' && trip_no=='')
	{
		Swal.fire({icon: 'warning',html: '<font size=\'2\' color=\'black\'>Enter Vehicle No. or Trip No. first !</font>',});
	}
	else
	{
		$('#loadicon').show();
			jQuery.ajax({
				url: "_load_running_trips.php",
				data: 'tno=' + tno + '&trip_no=' + trip_no,
				type: "POST",
				success: function(data) {
					$("#load_table").html(data);
					$('#example').DataTable({ 
						"destroy": true, //use for reinitialize datatable
					});
				},
				error: function() {}
		});
	}
}

function ViewTrip(id)
{
	$('#loadicon').show();
			jQuery.ajax({
				url: "_load_trip_by_id.php",
				data: 'id=' + id,
				type: "POST",
				success: function(data) {
					$("#modal_load_div").html(data);
				},
				error: function() {}
		});
}
</script>

<?php include("footer.php") ?>