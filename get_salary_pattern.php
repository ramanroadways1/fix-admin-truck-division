<?php
require_once("./connect.php");

$model = escapeString($conn,($_POST['model']));
$driver_code = escapeString($conn,($_POST['driver_code']));
$tno = escapeString($conn,($_POST['tno']));

$timestamp = date("Y-m-d H:i:s");

$get_salary = Qry($conn,"SELECT salary,type FROM dairy.salary_master WHERE model='$model'");

if(!$get_salary){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	AlertErrorTopRight("Error while Processing Request !");
	exit();
}

$get_current_salary = Qry($conn,"SELECT salary_amount,salary_type FROM dairy.driver_up WHERE down=0 AND code='$driver_code' ORDER BY id DESC LIMIT 1");

if(!$get_current_salary){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	AlertErrorTopRight("Error while Processing Request !");
	exit();
}

if(numRows($get_current_salary)==0)
{
	AlertErrorTopRight("Active driver not found: $driver_code !");
	exit();
}

$row_c = fetchArray($get_current_salary);

echo '<option style="font-size:12px" value="">--Select Salary--</option>';

while($row = fetchArray($get_salary))
{
	if($row['type']=="1")
	{
		?>
		<option <?php if($row_c['salary_amount']==$row['salary'] AND $row_c['salary_type']==$row['type']) { echo "selected"; } ?> style="font-size:12px" value="<?php echo $row['salary']."_".$row['type']; ?>"><?php echo $row['salary']." / Trip."?></option>
		<?php
	}
	else
	{
		?>
		<option <?php if($row_c['salary_amount']==$row['salary'] AND $row_c['salary_type']==$row['type']) { echo "selected"; } ?> style="font-size:12px" value="<?php echo $row['salary']."_".$row['type']; ?>"><?php echo $row['salary']." / Month."?></option>
		<?php
	}
}

	echo "<script>
		$('#driver_code_model').val('$driver_code');
		$('.modal_tno').val('$tno');
		$('#model_update_salary_btn')[0].click();
		$('#loadicon').fadeOut('slow');
	</script>";
	exit();

?>