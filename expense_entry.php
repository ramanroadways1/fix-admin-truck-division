<?php
include("header.php");

$chk = Qry($conn,"SELECT id FROM _access_control WHERE username='$_SESSION[ediary_fix_admin]' AND func_id=(SELECT id FROM 
_access_control_func_list WHERE session_role='8' AND func_name='Expense_Entry') AND u_view='1'");
			  
if(numRows($chk)==0)
{
	echo "<script>window.location.href='./';</script>";
	exit();
}
?>

<script>
$(function() {
		$("#own_tno").autocomplete({
		source: '../diary/autofill/own_tno.php',
		// appendTo: '#appenddiv',
		select: function (event, ui) { 
            $('#own_tno').val(ui.item.value);   
            return false;},
		change: function (event, ui) {
		if(!ui.item){
			$(event.target).val("");
            $(event.target).focus();
			$('#own_tno').val("");   
			alert('Vehicle does not exists.');
		}}, 
	focus: function (event, ui){
	return false;
	}
});});
</script>

<div class="content-wrapper">
      <section class="content-header">
          <h1 style="font-size:16px;">Expense ENTRY </h1>
       </section>
       
	   <section class="content">
          <div class="row">
            <div class="col-xs-12">
			<div class="box">
                <div class="box-body">
				
				<div class="col-md-12">&nbsp;</div>
				
				<div class="col-md-12">
					<div class="row">
						
						<div class="form-group col-md-3">
							<label style="font-size:12px !important">Vehicle Number <font color="red"><sup>*</sup></font></label>
							<input type="text" oninput="this.value=this.value.replace(/[^a-z 0-9A-Z.-]/,'');" class="form-control" style="font-size:12px !important;text-transform:uppercase" required="required" id="own_tno" />
						</div>
						
						<div class="form-group col-md-2">
							<?php if(!isMobile()) { echo "<label>&nbsp;</label><br />"; } ?>
							<button type="button" style="margin-top:1px;font-size:12px !important" onclick="Search()" class="btn btn-sm btn-success <?php if(isMobile()) { echo "btn-block"; } ?>" id="add_btn"><i class="fa fa-search" aria-hidden="true"></i> &nbsp; Search Trips</button>
						</div>
					</div>
				</div>
				
				<div class="col-md-12">&nbsp;</div>
				
				<div class="col-md-12 table-responsive" id="load_table">
              
				 </div> 
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

<div id="func_result"></div>  
<div id="modal_res1"></div>  

<script>	
function Search()
{
	var tno = $('#own_tno').val();
	
	if(tno=='')
	{
		Swal.fire({icon: 'warning',html: '<font size=\'2\' color=\'black\'>Enter vehicle number first !</font>',});
	}
	else
	{
		$('#loadicon').show();
			jQuery.ajax({
				url: "_load_trips_for_expense_entry.php",
				data: 'tno=' + tno,
				type: "POST",
				success: function(data) {
					$("#load_table").html(data);
					$('#example').DataTable({ 
					"lengthMenu": [ [50, 100, 500, -1], [50, 100,500, "All"] ], 
						"destroy": true, //use for reinitialize datatable
					});
				},
				error: function() {}
		});
	}
}

function AddExpense(trip_id)
{
	var from_loc = $('#from_loc_'+trip_id).val();
	var to_loc = $('#to_loc_'+trip_id).val();
	
	$('#trip_locations').html(from_loc+" to "+to_loc);
	$('#modal_trip_id').val(trip_id);
	
	$('#modal_open_btn')[0].click();
	
	// $('#add_btn').attr('disabled',true);
		// jQuery.ajax({
			// url: "save_add_new_pump.php",
			// data: 'pump_name=' + pump_name + '&pump_code=' + pump_code + '&branch=' + branch + '&fuel_company=' + fuel_company + '&type=' + 'MARKET',
			// type: "POST",
			// success: function(data) {
				// $("#func_result").html(data);
			// },
			// error: function() {}
		// });
	// }
}
</script>

<button id="modal_open_btn" style="display:none" data-toggle="modal" data-target="#EntryModal"></button>

<script type="text/javascript">
$(document).ready(function (e) {
$("#ExpForm").on('submit',(function(e) {
$("#loadicon").show();
$("#entry_save_btn").attr("disabled", true);
e.preventDefault();
	$.ajax({
	url: "./save_expense_entry.php",
	type: "POST",
	data:  new FormData(this),
	contentType: false,
	cache: false,
	processData:false,
	success: function(data){
		$("#result_edits").html(data);
	},
	error: function() 
	{} });}));});
</script>   

<form id="ExpForm" autocomplete="off" style="font-size:12px !important">   
<div class="modal fade" id="EntryModal" role="dialog" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
      <div class="modal-content" style="">
		
		<div class="modal-header bg-primary">
			Expense ENTRY : Trip --> <span id="trip_locations"></span>
		  </div>
	  
		<div class="modal-body">
			<div class="row">
				
				<div class="form-group col-md-6"> 
					<label>Amount. <sup><font color="red">*</font></sup></label>
					<input type="number" max="20000" id="expense_amount" min="1" name="amount" class="form-control" required />
				</div>
				
				<div class="form-group col-md-6">
					<label>Txn Date. <sup><font color="red">*</font></sup></label>
					<input name="exp_date" max="<?php echo date("Y-m-d"); ?>" min="<?php echo date('Y-m-d', strtotime('-7 days')); ?>" type="date" class="form-control" required pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}" />
				</div>
				
				<input type="hidden" name="exp_code" id="exp_code_function" />
				<input type="hidden" name="exp_name" id="exp_name_function" />
				
				<script>
				function ExpenseSel(elem)
				{
					var code = elem.split("_")[0];
					var name = elem.split("_")[1];
					
					$('#exp_code_function').val(code);
					$('#exp_name_function').val(name);
					
					if(code=='TR00013')
					{
						$('#rto_upload').attr('required',true);
						$('#rto_upload_div').show();
					}
					else
					{
						$('#rto_upload').attr('required',false);
						$('#rto_upload_div').hide();
					}
					
					var trip_id = $('#modal_trip_id').val();
					var expense_amount = $('#expense_amount').val();
					
					if(trip_id=='' || expense_amount=='')
					{
						Swal.fire({icon: 'warning',html: '<font size=\'2\' color=\'black\'>Enter expense amount first !</font>',});
						$('#exp_sel_option').val('');
					}
					else
					{
						jQuery.ajax({
						url: "check_expense_limit.php",
						data: 'trip_id=' + trip_id + '&expense_amount=' + expense_amount + '&elem=' + elem,
						type: "POST",
						success: function(data) {
							$("#exp_sel_result").html(data);
						},
						error: function() {}
						});
					}
				}
				</script>
			
				<div class="form-group col-md-6">
					<label>Select Expense. <sup><font color="red">*</font></sup></label>
					<select style="font-size:12px !important" name="exp" id="exp_sel_option" onchange="ExpenseSel(this.value)" class="form-control" required>
						<option style="font-size:12px !important" value="">select an option</option>
						<?php
						$fetch_exp=Qry($conn,"SELECT exp_code,name FROM dairy.exp_head ORDER BY name ASC");
								
						if(numRows($fetch_exp)>0)
						{
							while($row_exp=fetchArray($fetch_exp))
							{
							?>
								<option style="font-size:12px !important" value="<?php echo $row_exp["exp_code"]."_".$row_exp["name"]; ?>"><?php echo $row_exp["name"]; ?></option>
							<?php
							}					
						}
						?>
					</select>
				</div>
				
				<div class="form-group col-md-6">
					<label>Select Branch. <sup><font color="red">*</font></sup></label>
					<select style="font-size:12px !important" name="branch" class="form-control" required>
						<option style="font-size:12px !important" value="">select branch</option>
						<?php
						$fetch_branches = Qry($conn,"SELECT username FROM user WHERE role='2' AND username NOT IN('HEAD','DUMMY') ORDER BY username ASC");
								
						if(numRows($fetch_branches)>0)
						{
							while($row_b=fetchArray($fetch_branches))
							{
							?>
								<option style="font-size:12px !important" value="<?php echo $row_b["username"]; ?>"><?php echo $row_b["username"]; ?></option>
							<?php
							}					
						}
						?>
					</select>
				</div>
			
				<div id="rto_upload_div" style="display:none" class="form-group col-md-12">
					<label>Upload RTO Receipt. <sup><font color="red">* (multiple upload support)</font></sup></label>
					<input style="font-size:12px !important;padding-bottom:28px;" type="file" name="rto_upload[]" accept=".pdf,.jpg,.jpeg,.png" multiple="multiple" required id="rto_upload" class="form-control" />
				</div>
				
				<div class="form-group col-md-12">
					<label>Narration. <sup><font color="red">*</font></sup></label>
					<textarea style="font-size:12px !important" oninput="this.value=this.value.replace(/[^a-z A-Z0-9.,/-]/,'')" class="form-control" name="narration" required></textarea>
				</div>
				
				<div class="form-group col-md-12" id="exp_sel_result"></div>
			
			</div>
		</div>
		
		<div id="result_edits"></div>
		
		<input type="hidden" name="id" id="modal_trip_id">
        <div class="modal-footer">
          <button type="submit" id="entry_save_btn" class="btn btn-sm btn-danger">Save</button>
          <button type="button" class="btn-sm btn btn-primary" id="modal_close_btn" data-dismiss="modal">Close</button>
		</div>
      </div>
    </div>
  </div>
 </form>

<?php include("footer.php") ?>