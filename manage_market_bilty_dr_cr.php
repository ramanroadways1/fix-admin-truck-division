<?php
include("header.php");

$chk = Qry($conn,"SELECT id FROM _access_control WHERE username='$_SESSION[ediary_fix_admin]' AND func_id=(SELECT id FROM 
_access_control_func_list WHERE session_role='8' AND func_name='Market_Bilty_dr_cr') AND u_view='1'");
			  
if(numRows($chk)==0)
{
	echo "<script>window.location.href='./';</script>";
	exit();
}
?>
<style>
label{
	font-size:11px !important;
}
input{
	font-size:12px !important;
}
.ui-autocomplete { z-index:2147483647; font-size:11px !important;}
</style>

<div class="content-wrapper">
      <section class="content-header">
          <h1 style="font-size:16px;">Manage : MARKET Bilty Entries</h1>
       </section>
       
	   <section class="content">
          <div class="row">
            <div class="col-xs-12">
			<div class="box">
                <div class="box-body">
				
				<div class="col-md-12">&nbsp;</div>
				
				<div class="col-md-12">
					<div class="row">
						
						<div class="form-group col-md-3">
							<label style="font-size:12px !important">Bilty Number</label>
							<input style="font-size:12px !important" autocomplete="off" oninput="this.value=this.value.replace(/[^A-Za-z0-9]/,'');" type="text" class="form-control" id="bilty_no" />
						</div>
						
						<div class="form-group col-md-2">
							<?php if(!isMobile()) { echo "<label>&nbsp;</label><br />"; } ?>
							<button type="button" style="margin-top:1px;font-size:12px !important" onclick="Search()" class="btn btn-sm btn-success <?php if(isMobile()) { echo "btn-block"; } ?>" id="add_btn"><i class="fa fa-search" aria-hidden="true"></i> &nbsp; Search</button>
						</div>
					</div>
				</div>
	
				<div class="col-md-12">&nbsp;</div>
				
				<div class="col-md-12 table-responsive" id="load_table" style="overflow:auto">
              
				 </div> 
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

<div id="func_result"></div>  

<div id="modal_load_res"></div>  

<script>
function EditVoucher(id)
{
	$('#loadicon').show();
		jQuery.ajax({
		url: "modal_edit_market_bilty_txns.php",
		data: 'id=' + id,
		type: "POST",
		success: function(data) {
			$("#modal_load_res").html(data);
		},
		error: function() {}
	});
}

function DeleteVoucher(id)
{
	if(confirm("Are you sure ?") == true)
	{
		$('#dlt_btn_'+id).attr('disabled',true);
		$('#loadicon').show();
			jQuery.ajax({
			url: "delete_market_bilty_txns.php",
			data: 'id=' + id,
			type: "POST",
			success: function(data) {
				$("#func_result").html(data);
			},
			error: function() {}
		});
	}
}
</script>

<script>	
function Search()
{
	var bilty_no = $('#bilty_no').val();
	
	if(bilty_no=='')
	{
		Swal.fire({icon: 'warning',html: '<font size=\'2\' color=\'black\'>Enter bilty number first !</font>',});
	}
	else
	{
		$('#loadicon').show();
			jQuery.ajax({
				url: "_load_manage_market_bilty_dr_cr.php",
				data: 'bilty_no=' + bilty_no,
				type: "POST",
				success: function(data) {
					$("#load_table").html(data);
				},
				error: function() {}
		});
	}
}
</script>

<?php include("footer.php") ?>
