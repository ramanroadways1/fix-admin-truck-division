<?php
require_once("connect.php");

$date=date("Y-m-d");
$timestamp=date("Y-m-d H:i:s");

$rule_id = escapeString($conn,($_POST['rule_id']));
$con2_id = escapeString($conn,($_POST['con2_id']));
$lane_id = escapeString($conn,($_POST['lane_id']));

if(empty($lane_id))
{
	AlertError("Lane ID not found !");
	echo "<script>$('#save_btn_fix_add_consignee').attr('disabled',false);</script>";
	exit();
}

if(empty($rule_id))
{
	AlertError("Rule ID not found !");
	echo "<script>$('#save_btn_fix_add_consignee').attr('disabled',false);</script>";
	exit();
}

if(empty($con2_id))
{
	AlertError("Consignee not found !");
	echo "<script>$('#save_btn_fix_add_consignee').attr('disabled',false);</script>";
	exit();
}

$check_consignee = Qry($conn,"SELECT consignee,multi_del_consignee FROM dairy.fix_lane_rules WHERE id='$rule_id'");

if(!$check_consignee){
	AlertError("Error while processing request !");
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	echo "<script>$('#save_btn_fix_add_consignee').attr('disabled',false);</script>";
	exit();
}

if(numRows($check_consignee) == 0)
{
	AlertError("Rule not found !");
	echo "<script>$('#save_btn_fix_add_consignee').attr('disabled',false);</script>";
	exit();
}

$row = fetchArray($check_consignee);

if($row['consignee']==$con2_id)
{
	AlertError("Duplicate consignee found !");
	echo "<script>$('#save_btn_fix_add_consignee').attr('disabled',false);</script>";
	exit();
}

$con2_array = explode(',',$row['multi_del_consignee']);

if(in_array($con2_id, $con2_array))
{
	AlertError("Duplicate consignee found !");
	echo "<script>$('#save_btn_fix_add_consignee').attr('disabled',false);</script>";
	exit();
}
 
StartCommit($conn);
$flag = true;

$chk_for_consignee = Qry($conn,"SELECT consignee,multi_del_consignee FROM dairy.fix_lane_rules WHERE id='$rule_id'");

if(!$chk_for_consignee){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

if(numRows($chk_for_consignee) == 0)
{
	$flag = false;
	errorLog("Rule not found.",$conn,$page_name,__LINE__);
}

$row_chk_consignee = fetchArray($chk_for_consignee);

if($row_chk_consignee['consignee']=='0')
{
	if($row_chk_consignee['multi_del_consignee']=='')
	{
		$update_consignee = Qry($conn,"UPDATE dairy.fix_lane_rules SET consignee = '$con2_id' WHERE id='$rule_id'");

		if(!$update_consignee){
			$flag = false;
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		}
		
		// $add_consignee = Qry($conn,"UPDATE dairy.fix_lane_rules SET multi_del_consignee = '$con2_id' WHERE id='$rule_id'");

		// if(!$add_consignee){
			// $flag = false;
			// errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		// }
	}
	else
	{
		$get_new_record = Qry($conn,"SELECT multi_del_consignee FROM dairy.fix_lane_rules WHERE id='$rule_id'");

		if(!$get_new_record){
			$flag = false;
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		}

		$row_new = fetchArray($get_new_record);

		$curr_string = $row2['multi_del_consignee'].",".$con2_id;
		$parts = explode(",",$curr_string);
		asort($parts);
		// $parts = array_filter($parts);
		$new_string = (implode(",",$parts));

		$add_consignee = Qry($conn,"UPDATE dairy.fix_lane_rules SET multi_del_consignee = '$new_string' WHERE id='$rule_id'");

		if(!$add_consignee){
			$flag = false;
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		}
	}
}
else
{
	if($row_chk_consignee['multi_del_consignee']=='')
	{
		$curr_string = $row_chk_consignee['consignee'].",".$con2_id;
		$parts = explode(",",$curr_string);
		asort($parts);
		$new_string = (implode(",",$parts));
		
		$add_consignee = Qry($conn,"UPDATE dairy.fix_lane_rules SET multi_del_consignee = '$new_string' WHERE id='$rule_id'");

		if(!$add_consignee){
			$flag = false;
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		}
		
		$set_consignee_null = Qry($conn,"UPDATE dairy.fix_lane_rules SET consignee='0' WHERE id='$rule_id'");

		if(!$set_consignee_null){
			$flag = false;
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		}
	}
	else
	{
		$flag = false; 
		errorLog("Invalid option found.",$conn,$page_name,__LINE__);
			
		// $add_consignee = Qry($conn,"UPDATE dairy.fix_lane_rules SET multi_del_consignee = CONCAT(multi_del_consignee,',','$con2_id') 
		// WHERE id='$rule_id'");

		// if(!$add_consignee){
			// $flag = false;
			// errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		// }
	}
}


if($flag)
{
	MySQLCommit($conn);
	closeConnection($conn);
	
	AlertRightCornerSuccess("OK : Success !");
	echo "<script>
		$('#save_btn_fix_add_consignee').attr('disabled',false);
		$('#hide_modal_btn_con2_add')[0].click();
		LoadAllRule('$lane_id');
	</script>";
}
else
{
	MySQLRollBack($conn);
	closeConnection($conn);
	AlertError("Error while processing request !");
	echo "<script>$('#save_btn_fix_add_consignee').attr('disabled',false);</script>";
	exit();
}	
?>