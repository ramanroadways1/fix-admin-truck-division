<?php include("header.php"); ?>

<?php
$chk = Qry($conn,"SELECT id FROM _access_control WHERE username='$_SESSION[ediary_fix_admin]' AND func_id=(SELECT id FROM 
_access_control_func_list WHERE session_role='8' AND func_name='Owner_Change_Req') AND u_view='1'");
			  
if(numRows($chk)==0)
{
	echo "<script>window.location.href='./';</script>";
	exit();
}
?>

<div class="content-wrapper">
      <section class="content-header">
          <h1 style="font-size:16px;">Owner Change Approval : </h1>
       </section>
       
	   <section class="content">
          <div class="row">
            <div class="col-xs-12">
			<div class="box">
                <div class="box-body">

				<div class="col-md-12">&nbsp;</div>
				
				<div class="col-md-12 table-responsive" id="load_table">
				</div> 
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

<div id="func_result"></div>  
<div id="load_modal_div"></div>  
 
<div class="modal fade" id="imagemodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">  
			<div class="bg-primary modal-header">
				<h4 class="modal-title" style="font-size:13px;color:#FFF">Owner Name : <span style="color:" id="name_box"></span>, PAN : <span style="color:" id="pan_box"></span></h4>
			</div>
	  
			<div class="modal-body" style="overflow:auto;height:450px !important">
				<img src="" class="imagepreview" style="width: 100%;" >
			</div> 
		
		<div class="modal-footer">
		<button type="button" class="btn btn-danger btn-sm pull-right" data-dismiss="modal">Close</button>	
		<a target="_blank" id="FullLink">
			<button type="button" class="btn btn-primary btn-sm pull-left">View FULL Size</button>
		</a>
        </div> 
	</div>
 </div>
 </div>
 
 <button style="display:none" id="btnmodal1" data-toggle="modal" data-target="#imagemodal"></button>
 
 <a href="" style="display:none" target="_blank" id="img_path"></a>
 
 <script>
 function ViewImg(id,chk,img,pan,ext)
 {
	 $('#loadicon').show();
	 
		if(chk=='OLD')
		 {
			if(img=='rc')
			{
				var img = $('#rc_old'+id).val();
			}
			else if(img=='rc_r')
			{
				var img = $('#rc_old_rear'+id).val();
			}
			else
			{
				var img = $('#pan_old'+id).val();
			}		

			var name = $('#old_name1'+id).val();		
		}
		else
		{
			var name = $('#new_name1'+id).val();
		}
		
		
		if(ext=='')
		{
			Swal.fire({
				position: 'top-end',
				icon: 'warning',
				html: '<font size=\'2\' color=\'black\'>File not found !</font>',
				showConfirmButton: false,
				timer: 3000
			})
			$('#loadicon').fadeOut('slow');
		}
		else
		{
			if(ext=='pdf')
			{
				$('#img_path').attr('href',img);
				document.getElementById("img_path").click();
				$('#loadicon').fadeOut('slow');
			}
			else
			{
				$('.imagepreview').attr('src',img);
				$('#name_box').html(name);
				$('#pan_box').html(pan);
				$('#FullLink').attr('href',img);
				$('#btnmodal1').click();  
				$('#loadicon').fadeOut('slow');
			}
		}
	
 }
 </script>
 
<?php
$chk_update = Qry($conn,"SELECT id FROM _access_control WHERE username='$_SESSION[ediary_fix_admin]' AND func_id=(SELECT id FROM 
_access_control_func_list WHERE session_role='8' AND func_name='Owner_Change_Req') AND u_update='1'");
			  
if(numRows($chk_update)>0)
{
?>	
<script>	
function Approve(id)
{
	if (confirm('Do you really want to approve ?'))
	{
		$("#loadicon").show();
		jQuery.ajax({
			url: "owner_change_request_approve.php",
			data: 'id=' + id,
			type: "POST",
			success: function(data) {
			$("#func_result").html(data);
		},
		error: function() {}
		});
	}
}

function Reject(id)
{
	$("#loadicon").show();
	jQuery.ajax({
		url: "_load_modal_reject_owner_change.php",
		data: 'id=' + id,
		type: "POST",
		success: function(data) {
		$("#func_result").html(data);
	},
	error: function() {}
	});
}
</script>
<?php
}
?>
	
<script>	
function LoadTable()
{
	$("#loadicon").show();
	jQuery.ajax({
		url: "_load_market_vehicle_owner_change.php",
		data: 'id=' + 'ok',
		type: "POST",
		success: function(data) {
			$("#load_table").html(data);
			$('#example').DataTable({ 
				"destroy": true, //use for reinitialize datatable
			});
		},
		error: function() {}
	});
}
 LoadTable();
</script>

<?php include("footer.php") ?>