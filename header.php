<?php
require_once("./connect.php");
$menu_page_name = basename($_SERVER['PHP_SELF']);
?>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate" />
	<meta http-equiv="Pragma" content="no-cache" />
	<meta http-equiv="Expires" content="0" />
	<meta name="robots" content="noindex,nofollow"/>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Truck Division: : Raman Roadways Pvt. Ltd.</title>
	<link rel="shortcut icon" type="image/png" href="favicon_2.png"/>
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
	
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.5.2/jquery.min.js"></script>
  <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
  <script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap.min.js"></script>
  
  <!-- Morris chart -->
	  <link rel="stylesheet" href="../happay/bower_components/morris.js/morris.css">
	  <link rel="stylesheet" href="../happay/bower_components/Ionicons/css/ionicons.min.css">
	  <link rel="stylesheet" href="../happay/bower_components/jvectormap/jquery-jvectormap.css">
	  <!--
	  <link rel="stylesheet" href="../happay/plugins/select2/select2.min.css">
	  -->
	  <link rel="stylesheet" href="../happay/dist/css/AdminLTE.min.css">
	  <link rel="stylesheet" href="../happay/dist/css/skins/_all-skins.min.css">
	  <link rel="stylesheet" href="../happay/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
	  <link rel="stylesheet" href="../happay/bower_components/bootstrap-daterangepicker/daterangepicker.css">
	  <link rel="stylesheet" href="../happay/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
	  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
	  <script src="https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.2/modernizr.js"></script>
	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
	<link href="https://fonts.googleapis.com/css?family=Open+Sans+Condensed:300&display=swap" rel="stylesheet">
	<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
	<link href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap.min.css" rel="stylesheet">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.1/css/bootstrap-select.css" />
	<link href="google_font.css" rel="stylesheet">
	
	<script src="../diary/sweet_alert_lib/dist/sweetalert2.min.js"></script>
	<link rel="stylesheet" href="../diary/sweet_alert_lib/dist/sweetalert2.min.css">
	<script src="https://kit.fontawesome.com/d5ec20b4be.js" crossorigin="anonymous"></script>
<style>
@media screen and (min-width: 769px) {

    #logo_mobile { display: none; }
    #logo_desktop { display: block; }

}

@media screen and (max-width: 768px) {

    #logo_mobile { display: block; }
    #logo_desktop { display: none; }

}

@media (min-width: 768px) {
  .modal-xl-mini {
    width: 75%;
   max-width:100%;
  }
}

@media (min-width: 768px) {
  .modal-xl {
    width: 95%;
   max-width:100%;
  }
}

.modal { overflow: auto !important; } 

.selectpicker { width:auto; font-size: 12px !important;}

::-webkit-scrollbar{
    width:4px;
    height:4px;
}
::-webkit-scrollbar-track {
    -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.2); 
    border-radius: 5px;
}
::-webkit-scrollbar-thumb {
    border-radius: 5px;
    -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.9); 
}

table{
	font-family:Calibri !important;
}

.ui-autocomplete { z-index:2147483647;font-family: Verdana,Arial,sans-serif; font-size:11px !important;}

.filter-option {
    font-size: 12px !important;
}
</style>

</head>

<style type="text/css">
/*
.no-js #loader { display: none;  }
.js #loader { display: block; position: absolute; left: 100px; top: 0; }
.se-pre-con {
  position: fixed;
  left: 0px;
  top: 0px;
  width: 100%;
  height: 100%;
  z-index: 9999;
  background: url(load_icon.gif) center no-repeat #fff;
  background: url(loading_truck1.gif) center no-repeat #fff;
*/
}
</style>

<style type="text/css">
label{
	font-family: 'Open Sans', sans-serif !important;
	font-size:12.5px !important;
}
input[type='date'] { font-size: 12.5px !important;}
input[type='text'] { font-size: 12.5px !important;}
select { font-size: 12.5px !important; }
textarea { font-size: 12.5px !important; }
</style>

<script type="text/javascript">
  $(window).load(function() {
    $(".se-pre-con").fadeOut("slow");;
  });
</script>

<body style="font-family: 'Open Sans', sans-serif !important" class="hold-transition skin-blue sidebar-mini">

<?php
$sql_notifications = Qry($conn,"SELECT id FROM dairy.pending_master_lane WHERE is_approved='0'");

if(!$sql_notifications){
	echo getMySQLError($conn);
	errorLog(getMySQLError($conn),$conn,$page_url,__LINE__);
	exit();
}

$pending_lanes_approval = numRows($sql_notifications);
?> 
 <div class="se-pre-con"></div>
<div class="wrapper">

  <header class="main-header" style="font-size:12px;">

	<a href="./" class="logo" style="background:#FFF">
      <span class="logo-mini"><img src="../_logo/logo_raman_small.png" style="width:100%;height:50px" class="" /></span>
      <span class="logo-lg" id="logo_desktop"><img src="../_logo/logo_raman_main.png" style="margin-top:5px;width:100%;height:40px" class="img-responsive" /></span>
      <span class="logo-lg" id="logo_mobile"><center><img src="../_logo/logo_raman_main.png" style="margin-top:5px;width:50%;height:40px" class="img-responsive" /></center></span>
    </a>

    <nav class="navbar navbar-static-top">
      <a  class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>
      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
		
		<li class="dropdown notifications-menu">
            <a style="cursor: pointer;" class="dropdown-toggle" data-toggle="dropdown">
              <i class="fa fa-bell-o"></i>
              <span class="label label-warning"><?php echo $pending_lanes_approval; ?> </span>
            </a>
            <ul class="dropdown-menu">
              <?php
			  if($pending_lanes_approval>0)
			  {
				 echo "<li style='font-size:13px;' class='header'>$pending_lanes_approval Route approval pending !</li>"; 
			  }
			  ?>
			  <li>
                <ul  style="font-size:11px;" class="menu">
                  <li>
                    <a style="cursor: pointer;">
                      <i class="fa fa-users text-aqua"></i> My notification
                    </a>
                  </li>
                </ul>
              </li>
              <li class="footer"><a style="cursor: pointer;">View all</a></li>
            </ul>
          </li>
		  
		   <li class="user-menu">
            <a style="cursor: pointer;" class="dropdown-toggle" data-toggle="dropdown">
              <img src="avtar2.png" class="user-image" alt="User Image">
              <span class="hidden-xs"><?php echo  "Truck Division: $_SESSION[ediary_fix_admin]"; ?></span>
				&nbsp; &nbsp; <button onclick="LogoutFunc1()" type="button" 
				class="btn btn-xs btn-danger"><i class="fa fa-power-off"></i> Logout</button></a>
			</a>
          </li>
		  
          <!-- Control Sidebar Toggle Button -->
          <!--
		  <li>
            <a  data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
          </li> -->
        </ul>
      </div>

    </nav>
  </header>
  
<script>
function LogoutFunc1()
{
	Swal.fire({
	  title: 'Are you sure ??',
	  // text: "",
	  icon: 'warning',
	  showCancelButton: true,
	  confirmButtonColor: '#3085d6',
	  cancelButtonColor: '#d33',
	  confirmButtonText: 'Yes, i Confirm !'
	}).then((result) => {
	  if (result.isConfirmed) {
		window.location.href='https://rrpl.online/Truck_Division/logout.php';
	  }
	})
}

function CallUrl(url)
{
	$('#loadicon').show();
	if(url!='')
	{
		window.location.href=url;
	}
}
</script>	
  
<style>
.sidebar-menu>li>a{
	cursor:pointer;
}

.treeview>ul>li>a{
	font-family:Verdana !important;
	font-size:12px !important;
	cursor:pointer;
}

.fa-circle-o{
	font-size:10px !important
}
</style>
 
  
  <aside class="main-sidebar">
    <section class="sidebar">
      <div class="user-panel">
       
      </div>
      
	  <ul style="font-size:12px;" class="sidebar-menu" data-widget="tree">          
             
 	<li class="<?php if($menu_page_name=="index.php") {echo "active";} ?>">
        <a onclick="CallUrl('./index_main.php')"><i class="fa fa-dashboard"></i> <span>Dashboard</span></a>
     </li>
	
	<li class="treeview <?php if($menu_page_name=="alerts_fix_entry_limit_crossed.php") {echo "active";} ?>">
        <a>
            <i class="fa fa-exclamation-triangle"></i>
            <span>Alerts </span>
            <i class="fa fa-angle-left pull-right"></i>
        </a>
        <ul class="treeview-menu">
			<li class="<?php if($menu_page_name=="alerts_fix_entry_limit_crossed.php") {echo "active";} ?>"><a onclick="CallUrl('./alerts_fix_entry_limit_crossed.php')"><i class="fa fa-circle-o"></i> Fix Lane Adv/Exp Limit</a></li>
		</ul>
     </li>
	
	
	 <?php
	$check1 = Qry($conn,"SELECT id FROM _access_control WHERE username='$_SESSION[ediary_fix_admin]' AND func_id IN(SELECT id FROM _access_control_func_list 
	WHERE session_role='8' AND func_name IN('Fix_Queue','Fix_Lane')) AND u_view='1'");
	
	if(numRows($check1)>0)
	{
	?>
		<li class="treeview <?php if($menu_page_name=="fix_lane_summary.php" || $menu_page_name=="fix_function.php" || $menu_page_name=="fix_function.php" || $menu_page_name=="fix_entry_in_queue.php" || $menu_page_name=="force_fuel_update.php") {echo "active";} ?>">
              <a >
                <i class="fa fa-road"></i>
                <span>FIX Lane Master </span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
			  <?php
			  $chk_1 = Qry($conn,"SELECT id FROM _access_control WHERE username='$_SESSION[ediary_fix_admin]' AND func_id=(SELECT id FROM 
			  _access_control_func_list WHERE session_role='8' AND func_name='Fix_Lane') AND u_view='1'");
			  
			  if(numRows($chk_1)>0)
			  {
				?>
                <li class="<?php if($menu_page_name=="fix_function.php") {echo "active";} ?>"><a onclick="CallUrl('./fix_function.php')"><i class="fa fa-circle-o"></i> Fix Lane Manage</a></li>
			  <?php
			  }
			  
			  $chk_2 = Qry($conn,"SELECT id FROM _access_control WHERE username='$_SESSION[ediary_fix_admin]' AND func_id=(SELECT id FROM 
			  _access_control_func_list WHERE session_role='8' AND func_name='Fix_Queue') AND u_view='1'");
			  
			  if(numRows($chk_2)>0)
			  {
				?>
                <li class="<?php if($menu_page_name=="fix_entry_in_queue.php") {echo "active";} ?>"><a onclick="CallUrl('./fix_entry_in_queue.php')"><i class="fa fa-circle-o"></i> Fix Queue</a></li>
				 <li class="<?php if($menu_page_name=="force_fuel_update.php") {echo "active";} ?>"><a onclick="CallUrl('./force_fuel_update.php')"><i class="fa fa-circle-o"></i> Force Fuel Qty Update</a></li>
				 <li class="<?php if($menu_page_name=="fix_lane_summary.php") {echo "active";} ?>"><a onclick="CallUrl('./fix_lane_summary.php')"><i class="fa fa-circle-o"></i> Fix Lane Summary</a></li>
			<?php
			  }
			?>  
			</ul>
     </li>
	<?php
	}
	?>
	 
	<li class="<?php if($menu_page_name=="lr_item_master.php") {echo "active";} ?>">
		<a onclick="CallUrl('./lr_item_master.php')"><i class="fa fa-bookmark-o"></i> <span>LR Item Master</span></a>
    </li>
	
	<li class="<?php if($menu_page_name=="opening_closing_error.php") {echo "active";} ?>">
		<a onclick="CallUrl('./opening_closing_error.php')"><i class="fa fa-bookmark-o"></i> <span>Opening Closing Error</span></a>
    </li>
	 
	<?php
	$qry_approvals = Qry($conn,"SELECT id FROM _access_control WHERE username='$_SESSION[ediary_fix_admin]' AND func_id IN(SELECT id FROM _access_control_func_list 
	WHERE session_role='8' AND func_name IN('Standby_Approval','Allow_Hisab','Driver_Approval','Owner_Change_Req','By_pass_driver_otp')) AND u_view='1'");
	
	if(numRows($qry_approvals)>0)
	{
	?>
		<li class="treeview <?php if($menu_page_name=='by_pass_driver_verify_otp.php' || $menu_page_name=="driver_standby_approval.php" || $menu_page_name=="allow_hisab.php" || $menu_page_name=="add_driver_approval.php" || $menu_page_name=="market_vehicle_owner_change.php") {echo "active";} ?>">
              <a >
                <i class="fa fa-thumbs-o-up"></i>
                <span>Approvals </span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
			  <?php
			  $chk_1 = Qry($conn,"SELECT id FROM _access_control WHERE username='$_SESSION[ediary_fix_admin]' AND func_id=(SELECT id FROM 
			  _access_control_func_list WHERE session_role='8' AND func_name='Standby_Approval') AND u_view='1'");
			  
			  if(numRows($chk_1)>0)
			  {
				?>
                <li class="<?php if($menu_page_name=="driver_standby_approval.php") {echo "active";} ?>"><a onclick="CallUrl('./driver_standby_approval.php')"><i class="fa fa-circle-o"></i> Driver Standby Approval</a></li>
			  <?php
			  }
			  
			  $chk_2 = Qry($conn,"SELECT id FROM _access_control WHERE username='$_SESSION[ediary_fix_admin]' AND func_id=(SELECT id FROM 
			  _access_control_func_list WHERE session_role='8' AND func_name='Allow_Hisab') AND u_view='1'");
			  
			  if(numRows($chk_2)>0)
			  {
				?>
                <li class="<?php if($menu_page_name=="allow_hisab.php") {echo "active";} ?>"><a onclick="CallUrl('./allow_hisab.php')"><i class="fa fa-circle-o"></i> Allow Hisab</a></li>
			  <?php
			  }
			  
			   $chk_3 = Qry($conn,"SELECT id FROM _access_control WHERE username='$_SESSION[ediary_fix_admin]' AND func_id=(SELECT id FROM 
			  _access_control_func_list WHERE session_role='8' AND func_name='Driver_Approval') AND u_view='1'");
			  
			  if(numRows($chk_3)>0)
			  {
				?>
                <li class="<?php if($menu_page_name=="add_driver_approval.php") {echo "active";} ?>"><a onclick="CallUrl('./add_driver_approval.php')"><i class="fa fa-circle-o"></i> New Driver Approval</a></li>
			  <?php
			  }
			  
			   $chk_4 = Qry($conn,"SELECT id FROM _access_control WHERE username='$_SESSION[ediary_fix_admin]' AND func_id=(SELECT id FROM 
			  _access_control_func_list WHERE session_role='8' AND func_name='Owner_Change_Req') AND u_view='1'");
			  
			   if(numRows($chk_4)>0)
			  {
				?>
                <li class="<?php if($menu_page_name=="market_vehicle_owner_change.php") {echo "active";} ?>"><a onclick="CallUrl('./market_vehicle_owner_change.php')"><i class="fa fa-circle-o"></i> Owner Change Req.</a></li>
			  <?php
			  }
			  
			   $chk_5 = Qry($conn,"SELECT id FROM _access_control WHERE username='$_SESSION[ediary_fix_admin]' AND func_id=(SELECT id FROM 
			  _access_control_func_list WHERE session_role='8' AND func_name='By_pass_driver_otp') AND u_view='1'");
			  
			    if(numRows($chk_5)>0)
			  {
				?>
                <li class="<?php if($menu_page_name=="by_pass_driver_verify_otp.php") {echo "active";} ?>"><a onclick="CallUrl('./by_pass_driver_verify_otp.php')"><i class="fa fa-circle-o"></i> By-Pass Driver OTP</a></li>
			  <?php
			  }
			?>  
			</ul>
            </li>
	 
	 <?php
	}
	
	$qry_reports2 = Qry($conn,"SELECT id FROM _access_control WHERE username='$_SESSION[ediary_fix_admin]' AND func_id IN(SELECT id FROM _access_control_func_list 
	WHERE session_role='8' AND func_name IN('Excel_Downloads')) AND u_view='1'");
	
	if(numRows($qry_reports2)>0)
	{
	?>
	
	<li class="<?php if($menu_page_name=="excel_download.php") {echo "active";} ?>">
        <a onclick="CallUrl('./excel_download.php')"><i class="fa fa-file-excel-o"></i> <span>Excel Downloads</span></a>
     </li>
	 <?php
	}

	$qry_reports = Qry($conn,"SELECT id FROM _access_control WHERE username='$_SESSION[ediary_fix_admin]' AND func_id IN(SELECT id FROM _access_control_func_list 
	WHERE session_role='8' AND func_name IN('Report_Driver_Book','Report_Salary_Book','Report_Da_Book')) AND u_view='1'");
	
	if(numRows($qry_reports)>0)
	{
	?>
		<li class="treeview <?php if($menu_page_name=="report_driver_book.php" || $menu_page_name=="report_salary_book.php" || $menu_page_name=="report_da_book.php") {echo "active";} ?>">
              <a >
                <i class="fa fa-newspaper-o"></i>
                <span>Reports </span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
			  <?php
			  $chk_1 = Qry($conn,"SELECT id FROM _access_control WHERE username='$_SESSION[ediary_fix_admin]' AND func_id=(SELECT id FROM 
			  _access_control_func_list WHERE session_role='8' AND func_name='Report_Driver_Book') AND u_view='1'");
			  
			  if(numRows($chk_1)>0)
			  {
				?>
                <li class="<?php if($menu_page_name=="report_driver_book.php") {echo "active";} ?>"><a onclick="CallUrl('./report_driver_book.php')"><i class="fa fa-circle-o"></i> Driver Book</a></li>
			  <?php
			  }
			  
			  $chk_2 = Qry($conn,"SELECT id FROM _access_control WHERE username='$_SESSION[ediary_fix_admin]' AND func_id=(SELECT id FROM 
			  _access_control_func_list WHERE session_role='8' AND func_name='Report_Salary_Book') AND u_view='1'");
			  
			  if(numRows($chk_2)>0)
			  {
				?>
                <li class="<?php if($menu_page_name=="report_salary_book.php") {echo "active";} ?>"><a onclick="CallUrl('./report_salary_book.php')"><i class="fa fa-circle-o"></i> SALARY Book</a></li>
			  <?php
			  }
			  
			   $chk_3 = Qry($conn,"SELECT id FROM _access_control WHERE username='$_SESSION[ediary_fix_admin]' AND func_id=(SELECT id FROM 
			  _access_control_func_list WHERE session_role='8' AND func_name='Report_Da_Book') AND u_view='1'");
			  
			  if(numRows($chk_3)>0)
			  {
				?>
                <li class="<?php if($menu_page_name=="report_da_book.php") {echo "active";} ?>"><a onclick="CallUrl('./report_da_book.php')"><i class="fa fa-circle-o"></i> DA Book</a></li>
			  <?php
			  }
			  
			?>  
			</ul>
            </li>
	 
	 <?php
	}
	?>
	
	<?php
	$qry_chk_hisab = Qry($conn,"SELECT id FROM _access_control WHERE username='$_SESSION[ediary_fix_admin]' AND func_id IN(SELECT id FROM _access_control_func_list 
	WHERE session_role='8' AND func_name='HISAB_Check') AND u_view='1'");
	
	if(numRows($qry_chk_hisab)>0)
	{
	?>
	<li class="<?php if($menu_page_name=="check_hisab.php") {echo "active";} ?>">
		<a onclick="CallUrl('./check_hisab.php')"><i class="fa fa-calendar"></i> <span> Hisab Check/Approve</span> </a>
	</li>
	<?php
	}
	?>	
	
	<?php
	$qry_lr_section = Qry($conn,"SELECT id FROM _access_control WHERE username='$_SESSION[ediary_fix_admin]' AND func_id IN(SELECT id FROM _access_control_func_list 
	WHERE session_role='8' AND func_name IN('Manage_Market_LR','Manage_Own_LR','Enable_Crossing','Extend_Vou_Val','Market_Bilty','Market_Bilty_dr_cr')) AND u_view='1'");
	
	if(numRows($qry_lr_section)>0)
	{
	?>
		<li class="treeview <?php if($menu_page_name=='extend_vou_validity.php' || $menu_page_name=="manage_market_veh_lr.php" || $menu_page_name=="manage_own_veh_lr.php" || $menu_page_name=="enable_crossing.php" || $menu_page_name=="cancel_lr.php" || $menu_page_name=="manage_market_bilty.php" || $menu_page_name=="manage_market_bilty_dr_cr.php") {echo "active";} ?>">
              <a >
                <i class="fa fa-sticky-note-o"></i>
                <span>#Manage LR </span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
			 
			 <?php
			  $chk_1 = Qry($conn,"SELECT id FROM _access_control WHERE username='$_SESSION[ediary_fix_admin]' AND func_id=(SELECT id FROM 
			  _access_control_func_list WHERE session_role='8' AND func_name='Manage_Market_LR') AND u_view='1'");
			  
			  if(numRows($chk_1)>0)
			  {
				?>
                <li class="<?php if($menu_page_name=="manage_market_veh_lr.php") {echo "active";} ?>"><a onclick="CallUrl('./manage_market_veh_lr.php')"><i class="fa fa-circle-o"></i> MARKET Vehicle LR</a></li>
			  <?php
			  }
			  
			  $chk_2 = Qry($conn,"SELECT id FROM _access_control WHERE username='$_SESSION[ediary_fix_admin]' AND func_id=(SELECT id FROM 
			  _access_control_func_list WHERE session_role='8' AND func_name='Manage_Own_LR') AND u_view='1'");
			  
			  if(numRows($chk_2)>0)
			  {
				?>
                <li class="<?php if($menu_page_name=="manage_own_veh_lr.php") {echo "active";} ?>"><a onclick="CallUrl('./manage_own_veh_lr.php')"><i class="fa fa-circle-o"></i> OWN Vehicle LR</a></li>
			  <?php
			  }
			  
			   $chk_3 = Qry($conn,"SELECT id FROM _access_control WHERE username='$_SESSION[ediary_fix_admin]' AND func_id=(SELECT id FROM 
			  _access_control_func_list WHERE session_role='8' AND func_name='Enable_Crossing') AND u_view='1'");
			  
			  if(numRows($chk_3)>0)
			  {
				?>
                <li class="<?php if($menu_page_name=="enable_crossing.php") {echo "active";} ?>"><a onclick="CallUrl('./enable_crossing.php')"><i class="fa fa-circle-o"></i> Enable Crossing</a></li>
			  <?php
			  }
			  
			   $chk_4 = Qry($conn,"SELECT id FROM _access_control WHERE username='$_SESSION[ediary_fix_admin]' AND func_id=(SELECT id FROM 
			  _access_control_func_list WHERE session_role='8' AND func_name='Cancel_LR') AND u_view='1'");
			  
			  if(numRows($chk_4)>0)
			  {
				?>
                <li class="<?php if($menu_page_name=="cancel_lr.php") {echo "active";} ?>"><a onclick="CallUrl('./cancel_lr.php')"><i class="fa fa-circle-o"></i> Cancel LR</a></li>
			  <?php
			  }
			  
			  $chk_5 = Qry($conn,"SELECT id FROM _access_control WHERE username='$_SESSION[ediary_fix_admin]' AND func_id=(SELECT id FROM 
			  _access_control_func_list WHERE session_role='8' AND func_name='Market_Bilty') AND u_view='1'");
			  
			  if(numRows($chk_5)>0)
			  {
				?>
                <li class="<?php if($menu_page_name=="manage_market_bilty.php") {echo "active";} ?>"><a onclick="CallUrl('./manage_market_bilty.php')"><i class="fa fa-circle-o"></i> Market Bilty</a></li>
			  <?php
			  }
			  
			  $chk_6 = Qry($conn,"SELECT id FROM _access_control WHERE username='$_SESSION[ediary_fix_admin]' AND func_id=(SELECT id FROM 
			  _access_control_func_list WHERE session_role='8' AND func_name='Market_Bilty_dr_cr') AND u_view='1'");
			  
			  if(numRows($chk_6)>0)
			  {
				?>
                <li class="<?php if($menu_page_name=="manage_market_bilty_dr_cr.php") {echo "active";} ?>"><a onclick="CallUrl('./manage_market_bilty_dr_cr.php')"><i class="fa fa-circle-o"></i> Market Bilty Dr/Cr</a></li>
			  <?php
			  }
			  
			  $chk_111 = Qry($conn,"SELECT id FROM _access_control WHERE username='$_SESSION[ediary_fix_admin]' AND func_id=(SELECT id FROM 
			  _access_control_func_list WHERE session_role='8' AND func_name='Extend_Vou_Val') AND u_view='1'");
			  
			  if(numRows($chk_111)>0)
			  {
				?>
                <li class="<?php if($menu_page_name=="extend_vou_validity.php") {echo "active";} ?>"><a onclick="CallUrl('./extend_vou_validity.php')"><i class="fa fa-circle-o"></i> Extend Voucher Validity</a></li>
			  <?php
			  }
			  ?>
			  
			</ul>
            </li>
	 
	 <?php
	}
	?>
	
	<?php
	$qry_diesel_section = Qry($conn,"SELECT id FROM _access_control WHERE username='$_SESSION[ediary_fix_admin]' AND func_id IN(SELECT id FROM _access_control_func_list 
	WHERE session_role='8' AND func_name IN('Manage_Fuel_Station')) AND u_view='1'");
	
	if(numRows($qry_diesel_section)>0)
	{
	?>
		<li class="treeview <?php if($menu_page_name=="manage_own_vehicle_pump.php" || $menu_page_name=="manage_market_vehicle_pump.php") {echo "active";} ?>">
              <a >
                <i class="fa fa-beer"></i>
                <span>#Manage Fuel Station </span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
				<li class="<?php if($menu_page_name=="manage_own_vehicle_pump.php") {echo "active";} ?>"><a onclick="CallUrl('./manage_own_vehicle_pump.php')"><i class="fa fa-circle-o"></i> For Own Vehicle</a></li>
				<li class="<?php if($menu_page_name=="manage_market_vehicle_pump.php") {echo "active";} ?>"><a onclick="CallUrl('./manage_market_vehicle_pump.php')"><i class="fa fa-circle-o"></i> For Market Vehicle</a></li>
			 </ul>
            </li>
	 
	 <?php
	}
	?>
	
	<?php
	$qry_driver_section = Qry($conn,"SELECT id FROM _access_control WHERE username='$_SESSION[ediary_fix_admin]' AND func_id IN(SELECT id FROM _access_control_func_list 
	WHERE session_role='8' AND func_name IN('Edit_Driver','Modify_DA_SALARY')) AND u_view='1'");
	
	if(numRows($qry_driver_section)>0)
	{
	?>
		<li class="treeview <?php if($menu_page_name=="manage_drivers.php" || $menu_page_name=="update_da_salary_dates.php") {echo "active";} ?>">
              <a >
                <i class="fa fa-user-plus"></i>
                <span>#Driver </span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
			  <?php
			  $chk_1 = Qry($conn,"SELECT id FROM _access_control WHERE username='$_SESSION[ediary_fix_admin]' AND func_id=(SELECT id FROM 
			  _access_control_func_list WHERE session_role='8' AND func_name='Edit_Driver') AND u_view='1'");
			  
			  if(numRows($chk_1)>0)
			  {
				?>
                <li class="<?php if($menu_page_name=="manage_drivers.php") {echo "active";} ?>"><a onclick="CallUrl('./manage_drivers.php')"><i class="fa fa-circle-o"></i> Edit Driver Details</a></li>
			  <?php
			  }
			  
			  $chk_2 = Qry($conn,"SELECT id FROM _access_control WHERE username='$_SESSION[ediary_fix_admin]' AND func_id=(SELECT id FROM 
			  _access_control_func_list WHERE session_role='8' AND func_name='Modify_DA_SALARY') AND u_view='1'");
			  
			  if(numRows($chk_2)>0)
			  {
				?>
                <li class="<?php if($menu_page_name=="update_da_salary_dates.php") {echo "active";} ?>"><a onclick="CallUrl('./update_da_salary_dates.php')"><i class="fa fa-circle-o"></i> Modify DA/SALARY Dates</a></li>
			  <?php
			  }
			?>  
			</ul>
            </li>
	 
	 <?php
	}
	?>
	
	<?php
	$qry_trip_section = Qry($conn,"SELECT id FROM _access_control WHERE username='$_SESSION[ediary_fix_admin]' AND func_id IN(SELECT id FROM _access_control_func_list 
	WHERE session_role='8' AND func_name IN('Edit_Trip','Edit_Trip_Dates','Add_Trips','Trip_Order','Trip_Diesel_Entry','Cash_Diesel_Entry','Expense_Entry')) AND u_view='1'");
	
	if(numRows($qry_trip_section)>0)
	{
	?>
		<li class="treeview <?php if($menu_page_name=="edit_trip.php" || $menu_page_name=="edit_trip_dates.php" || $menu_page_name=="add_trips.php" || $menu_page_name=="manage_trip_order.php" || $menu_page_name=="trip_diesel_entry.php" || $menu_page_name=="cash_diesel_entry.php" || $menu_page_name=="expense_entry.php") {echo "active";} ?>">
              <a >
                <i class="fa fa-exchange"></i>
                <span>#Manage Trip </span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
			  <?php
			  $chk_1 = Qry($conn,"SELECT id FROM _access_control WHERE username='$_SESSION[ediary_fix_admin]' AND func_id=(SELECT id FROM 
			  _access_control_func_list WHERE session_role='8' AND func_name='Edit_Trip') AND u_view='1'");
			  
			  if(numRows($chk_1)>0)
			  {
				?>
                <li class="<?php if($menu_page_name=="edit_trip.php") {echo "active";} ?>"><a onclick="CallUrl('./edit_trip.php')"><i class="fa fa-circle-o"></i> Edit Trip</a></li>
			  <?php
			  }
			  
			  $chk_2 = Qry($conn,"SELECT id FROM _access_control WHERE username='$_SESSION[ediary_fix_admin]' AND func_id=(SELECT id FROM 
			  _access_control_func_list WHERE session_role='8' AND func_name='Edit_Trip_Dates') AND u_view='1'");
			  
			  if(numRows($chk_2)>0)
			  {
				?>
                <li class="<?php if($menu_page_name=="edit_trip_dates.php") {echo "active";} ?>"><a onclick="CallUrl('./edit_trip_dates.php')"><i class="fa fa-circle-o"></i> Edit Trip Dates</a></li>
			  <?php
			  }
			  
			   $chk_3 = Qry($conn,"SELECT id FROM _access_control WHERE username='$_SESSION[ediary_fix_admin]' AND func_id=(SELECT id FROM 
			  _access_control_func_list WHERE session_role='8' AND func_name='Add_Trips') AND u_view='1'");
			  
			  if(numRows($chk_3)>0)
			  {
				?>
                <li class="<?php if($menu_page_name=="add_trips.php") {echo "active";} ?>"><a onclick="CallUrl('./add_trips.php')"><i class="fa fa-circle-o"></i> Add Trips</a></li>
			  <?php
			  }
			  
			   $chk_4 = Qry($conn,"SELECT id FROM _access_control WHERE username='$_SESSION[ediary_fix_admin]' AND func_id=(SELECT id FROM 
			  _access_control_func_list WHERE session_role='8' AND func_name='Trip_Order') AND u_view='1'");
			  
			  if(numRows($chk_4)>0)
			  {
				?>
                <li class="<?php if($menu_page_name=="manage_trip_order.php") {echo "active";} ?>"><a onclick="CallUrl('./manage_trip_order.php')"><i class="fa fa-circle-o"></i> Manage Trip Order</a></li>
			  <?php
			  }
			  
			    $chk_5 = Qry($conn,"SELECT id FROM _access_control WHERE username='$_SESSION[ediary_fix_admin]' AND func_id=(SELECT id FROM 
			  _access_control_func_list WHERE session_role='8' AND func_name='Trip_Diesel_Entry') AND u_view='1'");
			  
			  if(numRows($chk_5)>0)
			  {
				?>
                <li class="<?php if($menu_page_name=="trip_diesel_entry.php") {echo "active";} ?>"><a onclick="CallUrl('./trip_diesel_entry.php')"><i class="fa fa-circle-o"></i> Diesel Entry</a></li>
			  <?php
			  }
			  
			    $chk_6 = Qry($conn,"SELECT id FROM _access_control WHERE username='$_SESSION[ediary_fix_admin]' AND func_id=(SELECT id FROM 
			  _access_control_func_list WHERE session_role='8' AND func_name='Cash_Diesel_Entry') AND u_view='1'");
			  
			  if(numRows($chk_6)>0)
			  {
				?>
                <li class="<?php if($menu_page_name=="cash_diesel_entry.php") {echo "active";} ?>"><a onclick="CallUrl('./cash_diesel_entry.php')"><i class="fa fa-circle-o"></i> Cash Diesel Entry</a></li>
			  <?php
			  }
			  
			    $chk_7 = Qry($conn,"SELECT id FROM _access_control WHERE username='$_SESSION[ediary_fix_admin]' AND func_id=(SELECT id FROM 
			  _access_control_func_list WHERE session_role='8' AND func_name='Expense_Entry') AND u_view='1'");
			  
			  if(numRows($chk_7)>0)
			  {
				?>
                <li class="<?php if($menu_page_name=="expense_entry.php") {echo "active";} ?>"><a onclick="CallUrl('./expense_entry.php')"><i class="fa fa-circle-o"></i> Expense Entry</a></li>
			  <?php
			  }
			?>

			
			</ul>
            </li>
	 
	 <?php
	}
	?>
	
	<?php
	$qry_lr_bank = Qry($conn,"SELECT id FROM _access_control WHERE username='$_SESSION[ediary_fix_admin]' AND func_id IN(SELECT id FROM _access_control_func_list 
	WHERE session_role='8' AND func_name='LR_Bank') AND u_view='1'");
	
	if(numRows($qry_lr_bank)>0)
	{
	?>
	<li class="<?php if($menu_page_name=="lr_bank.php") {echo "active";} ?>">
		<a onclick="CallUrl('./lr_bank.php')"><i class="fa fa-calendar"></i> <span> LR Bank</span> </a>
	</li>
	<?php
	}
	?>	
	
	<?php
	$qry_locations = Qry($conn,"SELECT id FROM _access_control WHERE username='$_SESSION[ediary_fix_admin]' AND func_id IN(SELECT id FROM _access_control_func_list 
	WHERE session_role='8' AND func_name='Manage_Location') AND u_view='1'");
	
	if(numRows($qry_locations)>0)
	{
	?>
	<li class="<?php if($menu_page_name=="manage_locations.php") {echo "active";} ?>">
		<a onclick="CallUrl('./manage_locations.php')"><i class="fa fa-map-marker"></i> <span> Manage Locations</span> </a>
	</li>
	<?php
	}
	?>	
	
	<?php
		if($_SESSION['ediary_fix_admin']=='TRUCKS') 
		{
		?>	<li class="<?php if($menu_page_name=="manage_user_rights.php") {echo "active";} ?>">
              <a onclick="CallUrl('./manage_user_rights.php')"><i class="fa fa-edit"></i> <span>Manage Users Rights</span> </a>
			</li>
			
			<li class="<?php if($menu_page_name=="manage_users.php") {echo "active";} ?>">
              <a onclick="CallUrl('./manage_users.php')"><i class="fa fa-users"></i> <span>Manage Users</span> </a>
			</li>
		<?php
		}
		?>
	
	<?php
	$check1 = Qry($conn,"SELECT id FROM _access_control WHERE username='$_SESSION[ediary_fix_admin]' AND func_id IN(SELECT id FROM _access_control_func_list 
	WHERE session_role='8' AND func_name='ManageSupervisors') AND u_view='1'");
	
	if(numRows($check1)>0)
	{
	?>
	<li class="<?php if($menu_page_name=="supervisors.php") {echo "active";} ?>">
		<a onclick="CallUrl('./supervisors.php')"><i class="fa fa-users"></i> <span>Supervisors</span> </a>
	</li>
	<?php
	}
	?>	
	
	<li class="<?php if($menu_page_name=="salary_master.php") {echo "active";} ?>">
		<a onclick="CallUrl('./salary_master.php')"><i class="fa fa-money"></i> <span>SALARY Master</span> </a>
	</li>
	
	<?php
	$checkExp = Qry($conn,"SELECT id FROM _access_control WHERE username='$_SESSION[ediary_fix_admin]' AND func_id IN(SELECT id FROM _access_control_func_list 
	WHERE session_role='8' AND func_name='Expenses') AND u_view='1'");
	
	if(numRows($checkExp)>0)
	{
	?>
	<li class="<?php if($menu_page_name=="expense_list.php") {echo "active";} ?>">
		<a onclick="CallUrl('./expense_list.php')"><i class="fa fa-money"></i> <span>Expense Master</span> </a>
	</li>
	
	<li class="<?php if($menu_page_name=="expense_list_fix_lane.php") {echo "active";} ?>">
		<a onclick="CallUrl('./expense_list_fix_lane.php')"><i class="fa fa-money"></i> <span>Expense Master - Fix</span> </a>
	</li>
	<?php
	}
	?>	
	
	<li class="<?php if($menu_page_name=="adv_exp_limit_alert.php") {echo "active";} ?>">
		<a onclick="CallUrl('./adv_exp_limit_alert.php')"><i class="fa fa-ban"></i> <span>Adv/Exp Limit Alert - Fix</span> </a>
	</li>
	
	<?php
	$checkExpLimit = Qry($conn,"SELECT id FROM _access_control WHERE username='$_SESSION[ediary_fix_admin]' AND func_id IN(SELECT id FROM _access_control_func_list 
	WHERE session_role='8' AND func_name='Expense_limit') AND u_view='1'");
	
	if(numRows($checkExpLimit)>0)
	{
	?>
	<li class="<?php if($menu_page_name=="expense_limit.php") {echo "active";} ?>">
		<a onclick="CallUrl('./expense_limit.php')"><i class="fa fa-ban"></i> <span>Expense Limit</span> </a>
	</li>
	<?php
	}
	?>	
	
	<?php
	$check2 = Qry($conn,"SELECT id FROM _access_control WHERE username='$_SESSION[ediary_fix_admin]' AND func_id IN(SELECT id FROM _access_control_func_list 
	WHERE session_role='8' AND func_name IN('Route_Approval','Route_Master')) AND u_view='1'");
	
	if(numRows($check2)>0)
	{
	?>
		<li class="treeview <?php if($menu_page_name=="route_pending_approval.php" || $menu_page_name=="route_master.php") {echo "active";} ?>">
              <a >
                <i class="fa fa-road"></i>
                <span>Route Master </span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
			  <?php
			  $chk_1 = Qry($conn,"SELECT id FROM _access_control WHERE username='$_SESSION[ediary_fix_admin]' AND func_id=(SELECT id FROM 
			  _access_control_func_list WHERE session_role='8' AND func_name='Route_Approval') AND u_view='1'");
			  
			  if(numRows($chk_1)>0)
			  {
				?>
                <li class="<?php if($menu_page_name=="route_pending_approval.php") {echo "active";} ?>"><a onclick="CallUrl('./route_pending_approval.php')"><i class="fa fa-circle-o"></i> Route Approval</a></li>
			  <?php
			  }
			  
			  $chk_2 = Qry($conn,"SELECT id FROM _access_control WHERE username='$_SESSION[ediary_fix_admin]' AND func_id=(SELECT id FROM 
			  _access_control_func_list WHERE session_role='8' AND func_name='Route_Master') AND u_view='1'");
			  
			  if(numRows($chk_2)>0)
			  {
				?>
                <li class="<?php if($menu_page_name=="route_master.php") {echo "active";} ?>"><a onclick="CallUrl('./route_master.php')"><i class="fa fa-circle-o"></i> Route Master</a></li>
			  <?php
			  }
			?>  
			</ul>
            </li>
	 
	 <?php
	}
	?>
	
	<?php
	$check3 = Qry($conn,"SELECT id FROM _access_control WHERE username='$_SESSION[ediary_fix_admin]' AND func_id IN(SELECT id FROM _access_control_func_list 
	WHERE session_role='8' AND func_name IN('Manage_Own_Truck','Gps_Status','Avg_Master','Diesel_Qty')) AND u_view='1'");
	
	if(numRows($check3)>0)
	{
	?>
		<li class="treeview <?php if($menu_page_name=="trishul_card.php" || $menu_page_name=="all_vehicles.php" || $menu_page_name=="gps_status.php" || $menu_page_name=="average_master.php" || $menu_page_name=="update_dsl_qty.php") {echo "active";} ?>">
              <a >
                <i class="fa fa-truck"></i>
                <span>Manage Own Vehicle </span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
			  <?php
			  $chk_1 = Qry($conn,"SELECT id FROM _access_control WHERE username='$_SESSION[ediary_fix_admin]' AND func_id=(SELECT id FROM 
			  _access_control_func_list WHERE session_role='8' AND func_name='Manage_Own_Truck') AND u_view='1'");
			  
			  if(numRows($chk_1)>0)
			  {
				?>
                <li class="<?php if($menu_page_name=="all_vehicles.php") {echo "active";} ?>"><a onclick="CallUrl('./all_vehicles.php')"><i class="fa fa-circle-o"></i> All Vehicles</a></li>
			  <?php
			  }
			  
			  $chk_2 = Qry($conn,"SELECT id FROM _access_control WHERE username='$_SESSION[ediary_fix_admin]' AND func_id=(SELECT id FROM 
			  _access_control_func_list WHERE session_role='8' AND func_name='Gps_Status') AND u_view='1'");
			  
			  if(numRows($chk_2)>0)
			  {
				?>
                <li class="<?php if($menu_page_name=="gps_status.php") {echo "active";} ?>"><a onclick="CallUrl('./gps_status.php')"><i class="fa fa-circle-o"></i> GPS Status</a></li>
			  <?php
			  }
			?>  
			
			<?php
			 $chk_3 = Qry($conn,"SELECT id FROM _access_control WHERE username='$_SESSION[ediary_fix_admin]' AND func_id=(SELECT id FROM 
			  _access_control_func_list WHERE session_role='8' AND func_name='Avg_Master') AND u_view='1'");
			  
			  if(numRows($chk_3)>0)
			  {
				?>
                <li class="<?php if($menu_page_name=="average_master.php") {echo "active";} ?>"><a onclick="CallUrl('./average_master.php')"><i class="fa fa-circle-o"></i> Avg. Master</a></li>
			  <?php
			  }
			?>  
			
			<?php
			$chk_4 = Qry($conn,"SELECT id FROM _access_control WHERE username='$_SESSION[ediary_fix_admin]' AND func_id=(SELECT id FROM 
			  _access_control_func_list WHERE session_role='8' AND func_name='Diesel_Qty') AND u_view='1'");
			  
			  if(numRows($chk_4)>0)
			  {
				?>
                <li class="<?php if($menu_page_name=="update_dsl_qty.php") {echo "active";} ?>"><a onclick="CallUrl('./update_dsl_qty.php')"><i class="fa fa-circle-o"></i> Diesel Data</a></li>
			  <?php
			  }
			?>  
			
			<?php
			$chk_5 = Qry($conn,"SELECT id FROM _access_control WHERE username='$_SESSION[ediary_fix_admin]' AND func_id=(SELECT id FROM 
			  _access_control_func_list WHERE session_role='8' AND func_name='Trishul_Card') AND u_view='1'");
			  
			  if(numRows($chk_5)>0)
			  {
				?>
                <li class="<?php if($menu_page_name=="trishul_card.php") {echo "active";} ?>"><a onclick="CallUrl('./trishul_card.php')"><i class="fa fa-circle-o"></i> Trishul Card</a></li>
			  <?php
			  }
			?>  
			
			</ul>
     </li>
	<?php
	}
	?>
	
	<?php
	$check4 = Qry($conn,"SELECT id FROM _access_control WHERE username='$_SESSION[ediary_fix_admin]' AND func_id IN(SELECT id FROM _access_control_func_list 
	WHERE session_role='8' AND func_name IN('Manage_Hisab','Hisab_View','Current_Trip_View')) AND u_view='1'");
	
	if(numRows($check4)>0)
	{
	?>
		<li class="treeview <?php if($menu_page_name=="manage_hisab.php" || $menu_page_name=="view_hisab.php" || $menu_page_name=="view_trip.php") {echo "active";} ?>">
              <a >
                <i class="fa fa-bookmark-o"></i>
                <span>Hisab Section </span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
			  <?php
			  $chk_1 = Qry($conn,"SELECT id FROM _access_control WHERE username='$_SESSION[ediary_fix_admin]' AND func_id=(SELECT id FROM 
			  _access_control_func_list WHERE session_role='8' AND func_name='Manage_Hisab') AND u_view='1'");
			  
			  if(numRows($chk_1)>0)
			  {
				?>
                <li class="<?php if($menu_page_name=="manage_hisab.php") {echo "active";} ?>"><a onclick="CallUrl('./manage_hisab.php')"><i class="fa fa-circle-o"></i> Manage Hisab</a></li>
			  <?php
			  }
			  
			  $chk_2 = Qry($conn,"SELECT id FROM _access_control WHERE username='$_SESSION[ediary_fix_admin]' AND func_id=(SELECT id FROM 
			  _access_control_func_list WHERE session_role='8' AND func_name='Hisab_View') AND u_view='1'");
			  
			  if(numRows($chk_2)>0)
			  {
				?>
                <li class="<?php if($menu_page_name=="view_hisab.php") {echo "active";} ?>"><a onclick="CallUrl('./view_hisab.php')"><i class="fa fa-circle-o"></i> Hisab View/Summary</a></li>
			  <?php
			  }
			?>  
			
			<?php
			 $chk_3 = Qry($conn,"SELECT id FROM _access_control WHERE username='$_SESSION[ediary_fix_admin]' AND func_id=(SELECT id FROM 
			  _access_control_func_list WHERE session_role='8' AND func_name='Current_Trip_View') AND u_view='1'");
			  
			  if(numRows($chk_3)>0)
			  {
				?>
                <li class="<?php if($menu_page_name=="view_trip.php") {echo "active";} ?>"><a onclick="CallUrl('./view_trip.php')"><i class="fa fa-circle-o"></i> View Running Trip</a></li>
			  <?php
			  }
			?>  
		</ul>
     </li>
	<?php
	}
	?>
	
	<?php
	$check_add_func = Qry($conn,"SELECT id FROM _access_control WHERE username='$_SESSION[ediary_fix_admin]' AND func_id IN(SELECT id FROM _access_control_func_list 
	WHERE session_role='8' AND func_name IN('Add_Market_Driver','Add_Market_Truck','Add_Ummeed_Broker','Add_Own_Truck_Driver','Add_Mkt_Bilty_Broker','Add_Mkt_Bilty_Bill_Party')) AND u_view='1'");
	
	if(numRows($check_add_func)>0)
	{
	?>
		<li class="treeview <?php if($menu_page_name=="add_market_driver.php" || $menu_page_name=="add_market_truck.php" || $menu_page_name=="add_market_broker.php" || $menu_page_name=="add_own_truck_driver.php" || $menu_page_name=="add_mb_broker.php" || $menu_page_name=="add_mb_bill_party.php") {echo "active";} ?>">
              <a >
                <i class="fa fa-truck"></i>
                <span>Add Functions </span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
			  <?php
			  $chk_1 = Qry($conn,"SELECT id FROM _access_control WHERE username='$_SESSION[ediary_fix_admin]' AND func_id=(SELECT id FROM 
			  _access_control_func_list WHERE session_role='8' AND func_name='Add_Market_Driver') AND u_view='1'");
			  
			  if(numRows($chk_1)>0)
			  {
				?>
                <li class="<?php if($menu_page_name=="add_market_driver.php") {echo "active";} ?>"><a onclick="CallUrl('./add_market_driver.php')"><i class="fa fa-circle-o"></i> Add Market Driver</a></li>
			  <?php
			  }
			  
			  $chk_2 = Qry($conn,"SELECT id FROM _access_control WHERE username='$_SESSION[ediary_fix_admin]' AND func_id=(SELECT id FROM 
			  _access_control_func_list WHERE session_role='8' AND func_name='Add_Market_Truck') AND u_view='1'");
			  
			  if(numRows($chk_2)>0)
			  {
				?>
                <li class="<?php if($menu_page_name=="add_market_truck.php") {echo "active";} ?>"><a onclick="CallUrl('./add_market_truck.php')"><i class="fa fa-circle-o"></i> Add Market Truck</a></li>
			  <?php
			  }
			?>  
			
			<?php
			 $chk_3 = Qry($conn,"SELECT id FROM _access_control WHERE username='$_SESSION[ediary_fix_admin]' AND func_id=(SELECT id FROM 
			  _access_control_func_list WHERE session_role='8' AND func_name='Add_Ummeed_Broker') AND u_view='1'");
			  
			  if(numRows($chk_3)>0)
			  {
				?>
                <li class="<?php if($menu_page_name=="add_market_broker.php") {echo "active";} ?>"><a onclick="CallUrl('./add_market_broker.php')"><i class="fa fa-circle-o"></i> Add Market Broker</a></li>
			  <?php
			  }
			?>  
			
			<?php
			$chk_4 = Qry($conn,"SELECT id FROM _access_control WHERE username='$_SESSION[ediary_fix_admin]' AND func_id=(SELECT id FROM 
			  _access_control_func_list WHERE session_role='8' AND func_name='Add_Own_Truck_Driver') AND u_view='1'");
			  
			  if(numRows($chk_4)>0)
			  {
				?>
                <li class="<?php if($menu_page_name=="add_own_truck_driver.php") {echo "active";} ?>"><a onclick="CallUrl('./add_own_truck_driver.php')"><i class="fa fa-circle-o"></i> Add Own Truck Driver</a></li>
			  <?php
			  }
			?>  
			
			<?php
			$chk_5 = Qry($conn,"SELECT id FROM _access_control WHERE username='$_SESSION[ediary_fix_admin]' AND func_id=(SELECT id FROM 
			  _access_control_func_list WHERE session_role='8' AND func_name='Add_Mkt_Bilty_Broker') AND u_view='1'");
			  
			  if(numRows($chk_5)>0)
			  {
				?>
                <li class="<?php if($menu_page_name=="add_mb_broker.php") {echo "active";} ?>"><a onclick="CallUrl('./add_mb_broker.php')"><i class="fa fa-circle-o"></i> Mkt Bilty Broker</a></li>
			  <?php
			  }
			?>  
			
			<?php
			$chk_6 = Qry($conn,"SELECT id FROM _access_control WHERE username='$_SESSION[ediary_fix_admin]' AND func_id=(SELECT id FROM 
			  _access_control_func_list WHERE session_role='8' AND func_name='Add_Mkt_Bilty_Bill_Party') AND u_view='1'");
			  
			  if(numRows($chk_6)>0)
			  {
				?>
                <li class="<?php if($menu_page_name=="add_mb_bill_party.php") {echo "active";} ?>"><a onclick="CallUrl('./add_mb_bill_party.php')"><i class="fa fa-circle-o"></i> Mkt Bilty Bill.Party</a></li>
			  <?php
			  }
			?>  
			
			</ul>
     </li>
	<?php
	}
	?>
	
	<li>
		<a onclick="LogoutFunc1()"><i class="glyphicon glyphicon-log-out"></i> <span>Logout</span></a>
     </li>
		
      </ul>
    </section>
  </aside>
  
<div id="loadicon" style="position: fixed; right: 0px; top: 0px; width: 100%;height: 100%; background-color:#FFF; z-index: 30001; opacity:1; cursor: wait">
	<center><img style="margin-top:140px" src="loading_truck1.gif" /><br><br><span style="letter-spacing:1px;font-weight:bold;font-size:14px">कृप्या प्रतीक्षा करे ..</span></center>
</div> 

<style>
label{
	font-size:12.5px;
}
input[type='date'] { font-size: 12px;}
input[type='text'] { font-size: 12px;}
select { font-size: 12px; }
textarea { font-size: 12px; }

::-webkit-scrollbar{
    width: 4px;
    height: 4px;
}
::-webkit-scrollbar-track {
    -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.2); 
    border-radius: 4px;
}
::-webkit-scrollbar-thumb {
    border-radius: 4px;
    -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.9); 
}
</style>