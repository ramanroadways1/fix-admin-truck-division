<?php
require_once 'connect.php';

$trip_no = escapeString($conn,($_POST['trip_no']));

$sql = Qry($conn,"SELECT 
	(SELECT tno FROM dairy.trip_final WHERE trip_no='$trip_no' ORDER BY id ASC LIMIT 1) as 'tno',
	(SELECT date(date) FROM dairy.trip_final WHERE trip_no='$trip_no' ORDER BY id ASC LIMIT 1) as 'from',
	(SELECT date(end_date) FROM dairy.trip_final WHERE trip_no='$trip_no' ORDER BY id DESC LIMIT 1) as 'to',
	(SELECT from_station FROM dairy.trip_final WHERE trip_no='$trip_no' ORDER BY id ASC LIMIT 1) as 'from_station',
	(SELECT to_station FROM dairy.trip_final WHERE trip_no='$trip_no' ORDER BY id DESC LIMIT 1) as 'to_station',
	(SELECT date(hisab_date) FROM dairy.trip_final WHERE trip_no='$trip_no' ORDER BY id ASC LIMIT 1) as 'hisab_date',
	(SELECT hisab_branch FROM dairy.trip_final WHERE trip_no='$trip_no' ORDER BY id ASC LIMIT 1) as 'hisab_branch',
	(SELECT driver_code FROM dairy.trip_final WHERE trip_no='$trip_no' ORDER BY id ASC LIMIT 1) as 'driver_code',
	(SELECT desct FROM dairy.driver_book WHERE trip_no='$trip_no' AND desct like 'DA%') as 'da_desc',
	(SELECT debit FROM dairy.driver_book WHERE trip_no='$trip_no' AND desct like 'DA%') as 'da_amount',
	(SELECT desct FROM dairy.driver_book WHERE trip_no='$trip_no' AND desct like 'SALARY%') as 'sal_desc',
	(SELECT debit FROM dairy.driver_book WHERE trip_no='$trip_no' AND desct like 'SALARY%') as 'sal_amount',
	(SELECT debit FROM dairy.driver_book WHERE trip_no='$trip_no' AND desct='EXP-AIR_AND_GREASE') as 'exp_ag',
	(SELECT branch_user FROM dairy.log_hisab WHERE trip_no='$trip_no') as 'branch_user',
	(SELECT hisab_type FROM dairy.log_hisab WHERE trip_no='$trip_no') as 'hisab_type',
	(SELECT id FROM dairy.log_hisab WHERE trip_no='$trip_no') as 'hisab_id',
	(SELECT debit FROM dairy.driver_book WHERE trip_no='$trip_no' AND desct='EXP-TELEPHONE') as 'exp_tel'");

if(!$sql){
	AlertRightCornerError("Error while processing request !");
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	exit();
}
	
$row_trip = fetchArray($sql);

if($row_trip['from_station']=='')
{
	AlertRightCornerError("Invalid trip number !");
	exit();
}

$get_hisab_user = Qry($conn,"SELECT name FROM emp_attendance WHERE code='$row_trip[branch_user]'");	

if(!$get_hisab_user){
	AlertRightCornerError("Error while processing request !");
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	exit();
}

$row_hisab_user = fetchArray($get_hisab_user);

$get_driver = Qry($conn,"SELECT name FROM dairy.driver WHERE code='$row_trip[driver_code]'");	

if(!$get_driver){
	AlertRightCornerError("Error while processing request !");
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	exit();
}

$row_driver = fetchArray($get_driver);

$hisab_user = $row_hisab_user['name'];
$tno = $row_trip['tno'];
$hisab_type = $row_trip['hisab_type'];
$driver_name = $row_driver['name'];
$driver_code = $row_trip['driver_code'];

	?>
	<table id="example" class="table table-bordered table-striped" style="font-size:13px;">
        <thead>
		<tr>
			<th>TripNo</th>
			<th>Vehicle_No</th>
			<th>DriverName</th>
			<th>DA</th>
			<th>SALARY</th>
			<th>Telephone<br>Air-Grease</th>
			<th>Trip & Date</th>
			<th>Hisab-Date & Branch & User</th>
			<th>Hisab_Type</th>
		</tr>
		</thead>
    <tbody id=""> 
	
	<?php
		$start_date = date('d-m-y', strtotime($row_trip['from']));
		$end_date = date('d-m-y', strtotime($row_trip['to']));
		$hisab_date = date('d-m-y', strtotime($row_trip['hisab_date']));

		if(!empty($row_trip['da_amount']))
		{
			$da_from_date = substr(str_replace('DA-','',str_replace(' ','',trim($row_trip['da_desc']))),0,10);
			$da_to_date = str_replace('to','',str_replace('DA-','',str_replace(' ','',trim($row_trip['da_desc']))));
			$da_to_date = str_replace($da_from_date,'',$da_to_date);
		
			// $da_from_date = date("d-m-y",strtotime($da_from_date));
			// $da_to_date = date("d-m-y",strtotime($da_to_date));
			
			$da_desc = "<b>$row_trip[da_amount]</b><br>".date("d-m-y",strtotime($da_from_date))." to ".date("d-m-y",strtotime($da_to_date))."</b><br>";
			
			$days_diff = round((strtotime($da_to_date)-strtotime($da_from_date)) / (60 * 60 * 24))+1;
			$da_per_day = $row_trip['da_amount']/$days_diff;
		}
		else
		{
			$da_desc = "";
			$da_per_day = "0";
			$da_from_date = "";
			$da_to_date = "";
		}
		
		if(!empty($row_trip['sal_amount']))
		{
			$sal_from_date = substr(str_replace('SALARY-','',str_replace(' ','',trim($row_trip['sal_desc']))),0,10);
			$sal_to_date = str_replace('to','',str_replace('SALARY-','',str_replace(' ','',trim($row_trip['sal_desc']))));
			$sal_to_date = str_replace($sal_from_date,'',$sal_to_date);
		
			// $sal_from_date = date("d-m-y",strtotime($sal_from_date));
			// $sal_to_date = date("d-m-y",strtotime($sal_to_date));
			
			$sal_desc = "<b>$row_trip[sal_amount]</b><br>".date("d-m-y",strtotime($sal_from_date))." to ".date("d-m-y",strtotime($sal_to_date))."</b>";
			
			$days_diff = round((strtotime($sal_to_date)-strtotime($sal_from_date)) / (60 * 60 * 24))+1;
			$salary_per_day = $row_trip['da_amount']/$days_diff;
		}
		else
		{
			$sal_desc = "";
			$salary_per_day = "0";
			$sal_from_date = "";
			$sal_to_date = "";
		}
		
		$secret_key = md5($trip_no.$tno.date("ymd"));
		
		if($hisab_type=='1')
		{
			$hisab_desc = "Carry-Fwd
			<br>
			<button class='btn btn-xs btn-warning' style='margin-top:3px' type='button' onclick='ChangeHisab($row_trip[hisab_id])'>Edit Hisab</button>";
		}
		else if($hisab_type=="2")
		{
			$hisab_desc = "Paid
			<br>
			<button class='btn btn-xs btn-warning' style='margin-top:3px' type='button' onclick='ChangeHisab($row_trip[hisab_id])'>Edit Hisab</button>";
		}
		else if($hisab_type=="3")
		{
			$hisab_desc = "Carry-Fwd+Paid
			<br>
			<button class='btn btn-xs btn-warning' style='margin-top:3px' type='button' onclick='ChangeHisab($row_trip[hisab_id])'>Edit Hisab</button>";
		}
		else if($hisab_type=="4")
		{
			$hisab_desc = "HO-Credit
			<br>
			<button class='btn btn-xs btn-warning' style='margin-top:3px' type='button' onclick='ChangeHisab($row_trip[hisab_id])'>Edit Hisab</button>";
		}
		else
		{
			$hisab_desc = "<font color='red'>NA</font>
			<br>
			<button class='btn btn-xs btn-warning' style='margin-top:3px' type='button'disabled>Invaid Hisab-Type</button>";
		}
		
		echo "<tr>	
			<td><button type='button' onclick=ViewHisab('$trip_no','$secret_key') class='btn btn-primary btn-xs'>$trip_no</button></td>
			<td>$tno</td>
			<td>$driver_name</td>
			<td>$da_desc
			<input type='hidden' value='$da_per_day' id='da_amount_per_day_$trip_no'>
			<input type='hidden' value='$da_from_date' id='da_from_date_$trip_no'>
			<input type='hidden' value='$da_to_date' id='da_to_date_$trip_no'>
			
			<input type='hidden' value='$salary_per_day' id='salary_amount_per_day_$trip_no'>
			<input type='hidden' value='$sal_from_date' id='salary_from_date_$trip_no'>
			<input type='hidden' value='$sal_to_date' id='salary_to_date_$trip_no'>
			
			<input type='hidden' value='$tno' id='tno_$trip_no'>
			<input type='hidden' value='$driver_code' id='driver_code_$trip_no'>
			
			<button style='display:none' type='button' onclick=EditDA('$trip_no','$row_trip[hisab_id]') disabled class='btn btn-warning btn-xs'>Edit DA</button>
			</td>
			<td>$sal_desc
			<br>
			<button style='display:none' type='button' onclick=EditSALARY('$trip_no') disabled class='btn btn-warning btn-xs'>Edit SALARY</button>
			</td>
			<td>
				Telephone : $row_trip[exp_tel]<br>
				Air-Grease : $row_trip[exp_ag]
			</td>
			<td>$row_trip[from_station] to $row_trip[to_station] <br> $start_date to $end_date</td>
			<td>$hisab_date<br>$row_trip[hisab_branch]<br>($hisab_user)</td>
			<td>$hisab_desc</td>
		</tr>";
	
	echo "</tbody>
</table>";
?>
	
<script> 
$("#loadicon").fadeOut('slow');
$(document).ready(function() {
    $('#example').DataTable( {
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ]
    } );
} );
</script>