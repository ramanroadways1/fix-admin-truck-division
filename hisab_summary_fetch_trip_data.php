<?php
require("./connect.php");

$trip_id=escapeString($conn,strtoupper($_POST['trip_id']));

if($trip_id=='')
{
	Redirect("EMPTY Trip Id.","hisab_summary.php");
	exit();
}

$check_trip=Qry($conn,"SELECT from_station,to_station,date,end_date,branch,lr_type,freight,cash,cheque,rtgs,diesel,diesel_qty,
freight_collected,driver_naame,expense,toll_tax,pod,pod_date FROM dairy.trip_final WHERE trip_id='$trip_id'");

if(!$check_trip)
{
	echo getMySQLError($conn);
	errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
	exit();
}

if(numRows($check_trip)==0)
{
	echo "<center><b><font color='red'>No result found.</font></b></center>";
	exit();
}

$row_trip=fetchArray($check_trip);

$from_stn=escapeString($conn,$row_trip['from_station']);
$to_stn=escapeString($conn,$row_trip['to_station']);
$trip_start_date=convertDate("d-m-y","$row_trip[date]");
$trip_end_date=convertDate("d-m-y","$row_trip[end_date]");
$trip_dates = $trip_start_date." to ".$trip_end_date;
$trip_branch=escapeString($conn,$row_trip['branch']);
$pod_rcvd = $row_trip['pod'];
$lr_type = $row_trip['lr_type'];
?>

	<script>
		$("#station_div").html("<b> <?php echo $from_stn;?> to <?php echo $to_stn." (".$trip_dates.")";?> - Created By : <?php echo $trip_branch;?></b>");
		// $("#print_button_div").html('<button class="btn btn-xs btn-default pull-right" onclick="PrintTrip(<?php echo $trip_id; ?>)" style="color:#000"><span class="glyphicon glyphicon-print"></span> Print</button>');
		// $("#trip_created_by_name").html('<?php echo $trip_branch;?>');
	</script>
	
	<div class="form-group col-md-6" style="border-right:0px solid gray;overflow:auto">
	
<div class="form-group col-md-12 table-responsive" style="overflow:auto">	
	<div class="row">
		<span class="col-md-12" style="background:#CCC;color:#000;padding:4px;">
			<span style="font-size:13.5px;">Advance (Cash/Happay) : <?php echo "₹ ".$row_trip['cash']; ?></span>
		</span>
				
	<?php
		$qry_cash=Qry($conn,"SELECT id,trans_type,vou_no,amount,date FROM dairy.cash WHERE trip_id='$trip_id'");
		if(!$qry_cash)
		{
			echo getMySQLError($conn);
			errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
			exit();
		}
		echo "<table class='table table-bordered col-md-12' style='color:#000;font-size:11px;'>";
		if(numRows($qry_cash)>0)
		{
			echo "
				<tr>
					<th>VouNo</th>
					<th>Type</th>
					<th>Amt</th>
					<th>Date</th>
					<th>#</th>
				</tr>	
			";	
			$i_cash=1;
			while($row_cash=fetchArray($qry_cash))
			{
				if($row_cash['trans_type']=='CASH'){
					$cash_txn_type = "CASH";
				}
				else{
					$cash_txn_type = "ATM";
				}
				echo "<tr>
						<td>".escapeString($conn,$row_cash['vou_no'])."</td>
						<td>$cash_txn_type</td>
						<td>".escapeString($conn,$row_cash['amount'])."</td>
						<td>".date('d-m-y',strtotime($row_cash['date']))."</td>
						<td><button style='padding:3px;' class='btn btn-xs btn-primary' 
						onclick=ViewEntry('$row_cash[id]','CASH')>
						<span class='glyphicon glyphicon-search'></span></button></td>
					</tr>	
				";
			$i_cash++;	
			}
		}
		else
		{
			echo "<td><font color='red'><center>No result found.</center></font></td>";
		}
		echo "</table>";
		?>		
	</div>
</div>
	
	<?php
	/*
	<div class="form-group col-md-12" style="background:#CCC;color:#000">
		<span>Cheque : <?php echo "&#x20B9; ".$row_trip['cheque']; ?> &nbsp; &nbsp;
		<button id="cheque_modal_button" style="color:black" class="btn btn-sm btn-warning" data-toggle="modal" data-target="#ChequeAdvModal" disabled>
		<span class="glyphicon glyphicon-edit"></span> Add</button></span>
	</div>
	
	<div class="form-group col-md-12 table-responsive" style="overflow:auto">
		<?php
		$qry_chq_rtgs=mysqli_query($conn,"SELECT amount,cheq_no,date,bank FROM cheque WHERE trip_id='$trip_id'");
		if(numRows($qry_chq_rtgs)>0)
		{
			echo "<table class='table table-bordered table-striped' style='font-size:11px;'>
				<tr>
					<th>Id</th>
					<th>Amt</th>
					<th>ChqNo</th>
					<th>Date</th>
					<th>AcNo</th>
				</tr>	
			";	
			$i_rtgs=1;
			while($row_rtgs=fetchArray($qry_chq_rtgs))
			{
				echo "<tr>
						<td>$i_rtgs</td>
						<td>".escapeString($conn,$row_rtgs['amount'])."</td>
						<td>".escapeString($conn,$row_rtgs['cheq_no'])."</td>
						<td>".escapeString($conn,$row_rtgs['date'])."</td>
						<td>".escapeString($conn,$row_rtgs['bank'])."</td>
					</tr>	
				";
			$i_rtgs++;	
			}
			echo "</table>";
		}
		else
		{
			echo "<font color='red'><center>No result found.</center></font>";
		}
		?>		
	</div>
	<?php */ ?>
	
	<div class="form-group col-md-12 table-responsive" style="overflow:auto">
	
 <div class="row">
	<div class="col-md-12" style="background:#CCC;color:#000;padding:4px">
		<span style="font-size:13.5px;">RTGS : <?php echo "₹ ".$row_trip['rtgs']; ?></span>
	</div>
	
	<?php
		$qry_chq_rtgs2=Qry($conn,"SELECT id,amount,driver_code,acname,acno,date FROM dairy.rtgs WHERE trip_id='$trip_id'");
		if(!$qry_chq_rtgs2)
		{
			echo getMySQLError($conn);
			errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
			exit();
		}
		echo "<table class='table table-bordered col-md-12' style='color:#000;font-size:11px;'>";
		if(numRows($qry_chq_rtgs2)>0)
		{
			echo "
			<tr>
					<th>Benf.Name</th>
					<th>AcNo</th>
					<th>Amt</th>
					<th>Date</th>
					<th>#</th>
				</tr>	
			";	
			$i_rtgs2=1;
			while($row_rtgs2=fetchArray($qry_chq_rtgs2))
			{
				echo "<tr>
						<td>".escapeString($conn,$row_rtgs2['acname'])."</td>
						<td>".escapeString($conn,$row_rtgs2['acno'])."</td>
						<td>".escapeString($conn,$row_rtgs2['amount'])."</td>
						<td>".date('d-m-y',strtotime($row_rtgs2['date']))."</td>
						<td><button style='padding:3px;' class='btn btn-xs btn-primary' 
						onclick=ViewEntry('$row_rtgs2[id]','RTGS')>
						<span class='glyphicon glyphicon-search'></span></button></td>
					</tr>	
				";
			$i_rtgs2++;	
			}
			
		}
		else
		{
			echo "<td><font color='red'><center>No result found.</center></font></td>";
		}
		echo "</table>";
		?>	
		</div>		
	</div>
	
	<div class="form-group col-md-12 table-responsive" style=";overflow:auto">
	
	<div class="row">
		<div class="col-md-12" style="background:#CCC;color:#000;padding:4px">
			<span style="font-size:13.5px;">Freight to Driver : <?php echo "₹ ".$row_trip['freight']; ?> </span>
		</div>
		
	<?php
		$qry_f1=Qry($conn,"SELECT id,amount,vou_id,date,narration FROM dairy.freight_adv WHERE trip_id='$trip_id'");
		if(!$qry_f1)
		{
			echo getMySQLError($conn);
			errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
			exit();
		}
		echo "<table class='table table-bordered col-md-12' style='color:#000;font-size:11px;'>";
		if(numRows($qry_f1)>0)
		{
			echo "
				<tr>
					<th>Amt</th>
					<th>Date</th>
					<th>LR</th>
					<th>Narration</th>
					<th>#</th>
				</tr>	
			";	
			$i_f1=1;
			while($row_f1=fetchArray($qry_f1))
			{
				echo "<tr>
						<td>".escapeString($conn,$row_f1['amount'])."</td>
						<td>".date('d-m-y',strtotime($row_f1['date']))."</td>
						<td>$row_f1[vou_id]</td>
						<td>".escapeString($conn,$row_f1['narration'])."</td>
						<td><button style='padding:3px;' class='btn btn-xs btn-primary' 
						onclick=ViewEntry('$row_f1[id]','FRT_ADV')>
						<span class='glyphicon glyphicon-search'></span></button></td>
					</tr>	
				";
			$i_f1++;	
			}
		}
		else
		{
			echo "<td><font color='red'><center>No result found.</center></font></td>";
		}
		echo "</table>";
		?>	
		</div>
	 				
	</div>
	
	
	<div class="form-group col-md-12 table-responsive" style="overflow:auto">
	
	<div class="row">
		<div class="col-md-12" style="background:#CCC;color:#000;padding:4px">
			<span style="font-size:13.5px">Credit Driver <font size="2">ड्राइवर नामें : </font>&nbsp;<?php echo "₹ ".$row_trip['driver_naame']; ?></span>
		</div>
	 			
	<?php
		$qry_naame2=Qry($conn,"SELECT id,amount,naame_type,lrno,date,narration FROM dairy.driver_naame WHERE trip_id='$trip_id'");
		if(!$qry_naame2)
		{
			echo getMySQLError($conn);
			errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
			exit();
		}
		echo "<table class='table table-bordered col-md-12' style='color:#000;font-size:11px;'>";
		if(numRows($qry_naame2)>0)
		{
			echo "
				<tr>
					<th>Amt</th>
					<th>Date</th>
					<th>Narration</th>
					<th>Type</th>
					<th>#</th>
				</tr>	
			";	
			$i_naame=1;
			while($row_naame2=fetchArray($qry_naame2))
			{
				if($row_naame2['lrno']!='')
				{
					$naame_type = $row_naame2['naame_type']."0-".$row_naame2['lrno'];
				}
				else
				{
					$naame_type = $row_naame2['naame_type'];
				}
				
				echo "<tr>
						<td>".escapeString($conn,$row_naame2['amount'])."</td>
						<td>".date('d-m-y',strtotime($row_naame2['date']))."</td>
						<td>".escapeString($conn,$row_naame2['narration'])."</td>
						<td>$naame_type</td>
						<td><button style='padding:3px;' class='btn btn-xs btn-primary' 
						onclick=ViewEntry('$row_naame2[id]','NAAME')>
						<span class='glyphicon glyphicon-search'></span></button></td>
					</tr>	
				";
			$i_naame++;	
			}
			
		}
		else
		{
			echo "<td><font color='red'><center>No result found.</center></font></td>";
		}
		echo "</table>";
		?>		
		</div>
	</div>
			
	</div>
		
	<div class="col-md-6" style="overflow:auto;">
	
	<div class="form-group col-md-12 table-responsive" style="overflow:auto">
	
	<div class="row">
		<div class="col-md-12" style="background:#CCC;color:#000;padding:4px">
		<span style="font-size:13.5px">Diesel : <?php echo "₹ ".$row_trip['diesel']. "($row_trip[diesel_qty] LTR)"; ?> </span>
		</div>
	
	<?php
		$qry_diesel=Qry($conn,"SELECT d.id,d.rate,d.qty,d.amount,d.date,de.card_pump FROM dairy.diesel as d 
		LEFT OUTER JOIN dairy.diesel_entry as de on de.id=d.id WHERE d.trip_id='$trip_id'");
		if(!$qry_diesel)
		{
			echo getMySQLError($conn);
			errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
			exit();
		}
		echo "<table class='table table-bordered col-md-12' style='color:#000;font-size:11px;'>";
		if(numRows($qry_diesel)>0)
		{
			$dieselQty=0;
			echo "
				<tr>
					<th>Qty</th>
					<th>Rate</th>
					<th>Amt</th>
					<th>Date</th>
					<th>By</th>
					<th>#</th>
				</tr>";	
			$i_diesel=1;
			while($row_diesel=fetchArray($qry_diesel))
			{
				$dieselQty+=$row_diesel['qty'];
				echo "<tr>
						<td>".escapeString($conn,$row_diesel['qty'])."</td>
						<td>".escapeString($conn,$row_diesel['rate'])."</td>
						<td>".escapeString($conn,$row_diesel['amount'])."</td>
						<td>".date('d-m',strtotime($row_diesel['date']))."</td>
						<td>".escapeString($conn,$row_diesel['card_pump'])."</td>
						<td><button style='padding:3px;' class='btn btn-xs btn-primary' 
						onclick=ViewEntry('$row_diesel[id]','DIESEL')>
						<span class='glyphicon glyphicon-search'></span></button></td>
					</tr>	
				";
			$i_diesel++;	
			}
			
		}
		else
		{
			$dieselQty=0;
			echo "<td><font color='red'><center>No result found.</center></font></td>";
		}
		echo "</table>";
		?>	
	<script>
		$("#DieselQtyTrip<?php echo $trip_id; ?>").html('<?php echo $dieselQty; ?> Ltr');		
	</script>
	
		</div>
	</div>
	
	<div class="form-group col-md-12 table-responsive" style="overflow:auto">
	
	<div class="row">
		<div class="col-md-12" style="background:#CCC;color:#000;padding:4px">
		<span style="font-size:13.5px">Expenses : <?php echo "₹ ".$row_trip['expense']; ?></span>
		</div>
	
	<?php
		$qry_exp=Qry($conn,"SELECT id,exp_name,branch,amount,date FROM dairy.trip_exp WHERE trip_id='$trip_id'");
		if(!$qry_exp)
		{
			echo getMySQLError($conn);
			errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
			exit();
		}
		echo "<table class='table table-bordered col-md-12' style='color:#000;font-size:11px;'>";
		if(numRows($qry_exp)>0)
		{
			echo "
				<tr>
					<th>Exp</th>
					<th>Amt</th>
					<th>Date</th>
					<th>Branch</th>
					<th>#</th>
				</tr>	
			";	
			$i_exp=1;
			while($row_exp=fetchArray($qry_exp))
			{
				echo "<tr>
						<td>".escapeString($conn,$row_exp['exp_name'])."</td>
						<td>".escapeString($conn,$row_exp['amount'])."</td>
						<td>".date('d-m-y',strtotime($row_exp['date']))."</td>
						<td>$row_exp[branch]</td>
						<td><button style='padding:3px;' class='btn btn-xs btn-primary' 
						onclick=ViewEntry('$row_exp[id]','EXPENSE')>
						<span class='glyphicon glyphicon-search'></span></button></td>
					</tr>	
				";
			$i_exp++;	
			}
			
		}
		else
		{
			echo "<td><font color='red'><center>No result found.</center></font></td>";
		}
		echo "</table>";
		?>		
		</div>
	</div>
	
	<div class="form-group col-md-12 table-responsive" style="overflow:auto">
	
	<div class="row">
		<div class="col-md-12" style="background:#CCC;color:#000;padding:4px">
			<span style="font-size:13.5px">Freight collected at branch : 
			<?php echo "₹ ".$row_trip['freight_collected']; ?> </span>
		</div>
	
	<?php
		$qry_f2=Qry($conn,"SELECT id,amount,vou_id,date,narration FROM dairy.freight_rcvd WHERE trip_id='$trip_id'");
		if(!$qry_f2)
		{
			echo getMySQLError($conn);
			errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
			exit();
		}
		echo "<table class='table table-bordered col-md-12' style='color:#000;font-size:11px;'>";
		if(numRows($qry_f2)>0)
		{
			echo "
				<tr>
					<th>Amt</th>
					<th>Date</th>
					<th>Narration</th>
					<th>LR</th>
				</tr>	
			";	
			$i_f2=1;
			while($row_f2=fetchArray($qry_f2))
			{
				echo "<tr>
						<td>".escapeString($conn,$row_f2['amount'])."</td>
						<td>".date('d-m-y',strtotime($row_f2['date']))."</td>
						<td>".escapeString($conn,$row_f2['narration'])."</td>
						<td>$row_f2[vou_id]</td>
						<td><button style='padding:3px;' class='btn btn-xs btn-primary' 
						onclick=ViewEntry('$row_f2[id]','FRT_COLL')>
						<span class='glyphicon glyphicon-search'></span></button></td>
					</tr>	
				";
			$i_f2++;	
			}
			
		}
		else
		{
			echo "<td><font color='red'><center>No result found.</center></font></td>";
		}
		echo "</table>";
		?>		
		</div>
	</div>
	
<div class="form-group col-md-12 table-responsive" style="border:.5px solid #ddd;overflow:auto;font-size:13px;">
	
	<div class="row">
	
		<div class="form-group col-md-12" style="background:#CCC;color:#000;padding:4px">
			<span style="font-size:13.5px">Rcv POD & END TRIP : </span>
		</div>
	
	<form id="PODViewForm" target="_blank" action="./view_pod.php" method="POST">
		<input type="hidden" name="trip_id" value="<?php echo $trip_id; ?>">
	</form>
	
	<div id="get_view_data"></div>
	
	<script>
	
	function ViewPOD()
	{
		$('#PODViewForm').submit();
	}
	
	function ViewEntry(id,trans_type)
	{
		$("#loadicon").show();
				jQuery.ajax({
				url: "modal_view_txns.php",
				data: 'id=' + id + '&trans_type=' + trans_type,
				type: "POST",
				success: function(data) {
					$("#get_view_data").html(data);
				},
				error: function() {}
		});
	}
	</script>	
	
	<div class="form-group col-md-12" style="overflow:auto">
				<?php
					if($lr_type!='EMPTY')
					{
						echo '<span style="color:blue"><b>POD Received</b></span> 
						&nbsp; <button onclick="ViewPOD('.$trip_id.')" class="btn btn-xs btn-primary">View-POD</button>';
					}
					?>
		</div>
	</div>
	</div>
</div>

<script>
	$("#loadicon").hide();	
</script>
