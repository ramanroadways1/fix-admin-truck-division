<?php
require_once("connect.php");

$get_total_roles = Qry($conn,"SELECT id FROM _access_control_func_list WHERE session_role='8'");
?>
<style>
  .slow .toggle-group { transition: left 0.7s; -webkit-transition: left 0.7s; }
</style>

<link href="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.6.1/css/bootstrap4-toggle.min.css" rel="stylesheet">
<script src="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.6.1/js/bootstrap4-toggle.min.js"></script>

    <table id="example" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th>#</th>
                        <th>Username</th>
                        <th>Mobile</th>
                        <th>Role</th>
                        <th>Vehicles</th>
                        <th>Added_On</th>
                        <th>#Status#</th>
                      </tr>
                    </thead>
                    <tbody>
	<?php
	$get_roles = Qry($conn,"SELECT u.id as superv_id,u.username,u.type,u.timestamp,s.mobile_no,s.active_login,s.id,
	(SELECT count(id) FROM dairy.own_truck WHERE superv_id=u.id) as vehicle_count 
	FROM dairy.user AS u 
	LEFT OUTER JOIN user AS s ON s.username=u.username 
	WHERE u.role='1'");
	
	if(numRows($get_roles)==0)
	{
		echo "<tr>.
			<td colspan='6'>No record found !</td>
			<td style='display: none'></td>
			<td style='display: none'></td>
			<td style='display: none'></td>
			<td style='display: none'></td>
			<td style='display: none'></td>
			<td style='display: none'></td>
			<td style='display: none'></td>
		</tr>";
	}
	else
	{
		$i=1;
		while($row = fetchArray($get_roles))
		{
			$timestamp = date("d-m-y h:i A",strtotime($row['timestamp']));
			
			if($row['type']=="1"){
				$type = "Supervisor";
			}else{
				$type = "<font color='blue'>Vehicle_Placer</font>";
			}
			
			echo "<tr>
				<td>$i</td>
				<td>$row[username]</td>
				<td>$row[mobile_no]</td>
				<td>$type</td>
				<input type='hidden' value='$row[username]' id='s_name_$row[superv_id]'>
				<td><button type='button' class='btn btn-xs btn-primary' onclick='ShowVehicle($row[superv_id])' style='padding:6px;font-size:13px;'>$row[vehicle_count]</button></td>
				<td>$timestamp</td>
				";
				?>
				<td>
				<input type="hidden"  value="<?php echo $row['active_login']; ?>" id="user_status_html_<?php echo $row['id']; ?>" />
				<input type="checkbox" onchange="ToggleUserStatus('<?php echo $row['id']; ?>')" id="button_check_<?php echo $row['id']; ?>" checked data-toggle="toggle" 
				<?php if($row['active_login']=='1') { echo "data-on='Active' data-off='Disabled' data-onstyle='success' data-offstyle='danger'"; } else { echo "data-on='Disabled' data-off='Active' data-onstyle='danger' data-offstyle='success'"; }?> data-style="slow" />
				</td>
				<?php				
		echo "</tr>";
		$i++;	
		}
	}
	?>	
                    </tbody>
                  </table>
				  
<script> 
$("#loadicon").fadeOut('slow');
$(document).ready(function() {
    $('#example').DataTable();
} );
</script> 			  