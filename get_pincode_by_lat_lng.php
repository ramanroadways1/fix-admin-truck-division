<?php
require_once("./connect.php");

$timestamp = date("Y-m-d H:i:s");

$lat = escapeString($conn,($_POST['lat_1']));
$long = escapeString($conn,($_POST['long_1']));

if($lat=='' || $long=='')
{
	AlertErrorTopRight("Location coordinated not found !");
	echo "<script>$('#state_name').val('');</script>";
	exit();
}

$get_pincode = getZipcode($lat.",".$long);

if(strlen($get_pincode)!=6)
{
	AlertErrorTopRight("Unable to fetch pincode !");
	echo "<script>$('#state_name').val('');</script>";
	exit();
}
	
$pincode = $get_pincode;

echo "<script>$('#pincode_db').val('$pincode');$('#loadicon').fadeOut('slow');</script>";
?>