<?php
require_once 'connect.php';

$sql = Qry($conn,"SELECT t.id,t.tno,t.driver,t.amount,t.entry_no,t.amount_limit,t.entry_limit,t.expense,t.timestamp,u.title,d.name as driver_name
FROM dairy._alert_fix_adv_exp AS t 
LEFT OUTER JOIN dairy.driver AS d ON d.code = t.driver
LEFT OUTER JOIN dairy.user AS u ON u.id = t.supervisor");

if(!$sql){
	echo getMySQLError($conn);
	errorLog(getMySQLError($conn),$conn,$page_url,__LINE__);
	exit();
}
	
if(numRows($sql)==0)
{
	echo "<tr><td colspan='10'>No record found..</td></tr>";
}
else
{
	?>
	<table id="example" class="table table-bordered table-striped" style="font-size:13px;">
        <thead>
		<tr>
			  <th>#</th>
			  <th>Vehicle_No</th>
			  <th>Driver</th>
			  <th>Amount</th>
			  <th>Entry_No.</th>
			  <th>Amount_Limit</th>
			  <th>Entry_Limit</th>
			  <th>Expense</th>
			  <th>Supervisor</th>
			  <th>Timestamp</th>
		</tr>
		</thead>
    <tbody id=""> 
	
<?php
$sn=1;

while($row = fetchArray($sql))
{	
	$timestamp = date('d-m-y h:i A', strtotime($row['timestamp']));
	
	echo "<tr>	
			<td>$sn</td>
			<td>$row[tno]</td>
			<td>$row[driver_name]</td>
			<td>$row[amount]</td>
			<td>$row[entry_no]</td>
			<td>$row[amount_limit]</td>
			<td>$row[entry_limit]</td>
			<td>$row[expense]</td>
			<td>$row[title]</td>
			<td>$timestamp</td>
		</tr>";
$sn++;		
}
	echo "</tbody>
</table>";
}
?>
	
<script> 
	$("#loadicon").fadeOut('slow');
</script> 
