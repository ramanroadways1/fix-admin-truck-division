<?php
require_once("./connect.php");

$id = escapeString($conn,($_POST['id']));
$trans_type = escapeString($conn,strtoupper($_POST['trans_type']));

if($trans_type=='CASH')
{
	$trans_name = "CASH ADVANCE";
}
else if($trans_type=='RTGS')
{
	$trans_name = "RTSG ADVANCE";
}
else if($trans_type=='NAAME')
{
	$trans_name = "DRIVER NAAME";
}
else if($trans_type=='DIESEL')
{
	$trans_name = "DIESEL";
}
else if($trans_type=='FRT_ADV')
{
	$trans_name = "FREIGHT TO DRIVER";
}
else if($trans_type=='FRT_COLL')
{
	$trans_name = "FREIGHT COLLECTED AT BRANCH";
}
else if($trans_type=='EXPENSE')
{
	$trans_name = "TRIP EXPENSES";
}
else
{
	AlertErrorTopRight("Invalid Txn !");
	exit();
}
?>
<button id="modal_view_txn_btn" style="display:none" data-toggle="modal" data-target="#ViewTxnModal"></button>

<div class="modal fade" id="ViewTxnModal" role="dialog" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-md">
      <div class="modal-content" style="">
	  
	  <div class="modal-header bg-primary">
		<?php echo $trans_name; ?> :
	</div>
		  
		<div class="modal-body">
		<div class="row">

<?php

if($trans_type=='CASH')
{
	$get_data = Qry($conn,"SELECT t.trans_id,t.trans_type,t.vou_no,t.amount,t.date,t.narration,t.branch,e.name as emp_name 
	FROM dairy.cash AS t 
	LEFT OUTER JOIN emp_attendance AS e ON e.code = t.branch_user 
	WHERE t.id='$id'");
	
	if(!$get_data){
		AlertErrorTopRight("Error while processing request !");
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		exit();
	}
	
	$row_data = fetchArray($get_data);
	?>
	<div class="form-group col-md-12">
		<table class="table table-bordered table-striped" style="font-size:12px !important;font-family:Calibri !important">
			<tr>
				<th>Txn Type</th>
				<td><?php echo $row_data['trans_type']; ?></td>
				<th>Txn Id</th>
				<td><?php echo $row_data['trans_id']; ?></td>
			</tr>	
			
			<tr>
				<th>Vou No.</th>
				<td><?php echo $row_data['vou_no']; ?></td>
				<th>Amount</th>
				<td><?php echo $row_data['amount']; ?></td>
			</tr>
			
			<tr>
				<th>Date</th>
				<td><?php echo $row_data['date']; ?></td>
				<th>Branch</th>
				<td><?php echo $row_data['branch']." ($row_data[emp_name])"; ?></td>
			</tr>
			
			<tr>
				<th>Narration</th>
				<td colspan="3"><?php echo $row_data['narration']; ?></td>
			</tr>
			
		</table>
	</div>
	<?php
	
}
else if($trans_type=='RTGS')
{
	$get_data = Qry($conn,"SELECT t.trans_id,t.vou_no,t.amount,t.acname,t.acno,t.bank,t.ifsc,t.date,t.narration,t.branch,e.name as emp_name 
	FROM dairy.rtgs AS t 
	LEFT OUTER JOIN emp_attendance AS e ON e.code = t.branch_user 
	WHERE t.id='$id'");
	
	if(!$get_data)
	{
		AlertErrorTopRight("Error while processing request !");
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		exit();
	}
	
	$row_data = fetchArray($get_data);
	?>
	<div class="form-group col-md-12">
		<table class="table table-bordered table-striped" style="font-size:12px !important;font-family:Calibri !important">
			<tr>
				<th>Txn Type</th>
				<td>RTGS</td>
				<th>Txn Id</th>
				<td><?php echo $row_data['trans_id']; ?></td>
			</tr>	
			
			<tr>
				<th>Vou No.</th>
				<td><?php echo $row_data['vou_no']; ?></td>
				<th>Amount</th>
				<td><?php echo $row_data['amount']; ?></td>
			</tr>
			
			<tr>
				<th>Date</th>
				<td><?php echo $row_data['date']; ?></td>
				<th>Branch</th>
				<td><?php echo $row_data['branch']." ($row_data[emp_name])"; ?></td>
			</tr>
			
			<tr>
				<th>Ac Holder</th>
				<td><?php echo $row_data['acname']; ?></td>
				<th>Ac No.</th>
				<td><?php echo $row_data['acno']; ?></td>
			</tr>
			
			<tr>
				<th>Bank</th>
				<td><?php echo $row_data['bank']; ?></td>
				<th>IFSC</th>
				<td><?php echo $row_data['ifsc']; ?></td>
			</tr>
			
			<tr>
				<th>Narration</th>
				<td colspan="3"><?php echo $row_data['narration']; ?></td>
			</tr>
			
		</table>
	</div>
	<?php
}
else if($trans_type=='NAAME')
{
	$get_data = Qry($conn,"SELECT t.trans_id,t.naame_type,t.lrno,t.claim_vou_no,t.qty,t.rate,t.amount,t.date,t.narration,t.branch,e.name as emp_name 
	FROM dairy.driver_naame AS t 
	LEFT OUTER JOIN emp_attendance AS e ON e.code = t.branch_user 
	WHERE t.id='$id'");
	
	if(!$get_data)
	{
		AlertErrorTopRight("Error while processing request !");
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		exit();
	}
	
	$row_data = fetchArray($get_data);
	
	if($row_data['claim_vou_no']!='' AND $row_data['lrno']=='')
	{
		$vou_no = $row_data['claim_vou_no'];
	}
	else if($row_data['claim_vou_no']='' AND $row_data['lrno']!='')
	{
		$vou_no = $row_data['lrno'];
	}
	else if($row_data['claim_vou_no']!='' AND $row_data['lrno']!='')
	{
		$vou_no = $row_data['claim_vou_no']." & ".$row_data['lrno'];
	}
	else
	{
		$vou_no = "NA";
	}
	?>
	<div class="form-group col-md-12">
		<table class="table table-bordered table-striped" style="font-size:12px !important;font-family:Calibri !important">
			<tr>
				<th>Naame Type</th>
				<td><?php echo $row_data['naame_type']; ?></td>
				<th>Txn Id</th>
				<td><?php echo $row_data['trans_id']; ?></td>
			</tr>	
			
			<tr>
				<th>Qty</th>
				<td><?php echo $row_data['qty']; ?></td>
				<th>Rate</th>
				<td><?php echo $row_data['rate']; ?></td>
			</tr>
			
			<tr>
				<th>LR/Vou No</th>
				<td><?php echo $vou_no; ?></td>
				<th>Amount</th>
				<td><?php echo $row_data['amount']; ?></td>
			</tr>
			
			<tr>
				<th>Date</th>
				<td><?php echo $row_data['date']; ?></td>
				<th>Branch</th>
				<td><?php echo $row_data['branch']." ($row_data[emp_name])"; ?></td>
			</tr>
			
			<tr>
				<th>Narration</th>
				<td colspan="3"><?php echo $row_data['narration']; ?></td>
			</tr>
			
		</table>
	</div>
	<?php
}
else if($trans_type=='DIESEL')
{
	$get_data = Qry($conn,"SELECT d.trans_id,d.rate,d.qty,d.amount,d.date,d.cash_diesel,d.branch,de.card_pump,de.card,de.dsl_company,e.name as emp_name 
	FROM dairy.diesel as d 
	LEFT OUTER JOIN dairy.diesel_entry AS de ON de.id=d.id 
	LEFT OUTER JOIN emp_attendance AS e ON e.code = d.branch_user 
	WHERE d.id='$id'");
	
	if(!$get_data)
	{
		AlertErrorTopRight("Error while processing request !");
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		exit();
	}
	
	$row_data = fetchArray($get_data);
	
	if($row_data['cash_diesel']=='1')
	{
		$cash_diesel = "<font color='red'>Yes</font>";
	}
	else
	{
		$cash_diesel = "No";
	}
	?>
	<div class="form-group col-md-12">
		<table class="table table-bordered table-striped" style="font-size:12px !important;font-family:Calibri !important">
			<tr>
				<th>Txn Id</th>
				<td><?php echo $row_data['trans_id']; ?></td>
				<th>Cash Diesel</th>
				<td><?php echo $cash_diesel; ?></td>
			</tr>	
			
			<tr>
				<th>Qty</th>
				<td><?php echo $row_data['qty']; ?></td>
				<th>Rate</th>
				<td><?php echo $row_data['rate']; ?></td>
			</tr>
			
			<tr>
				<th>Amount</th>
				<td><?php echo $row_data['amount']; ?></td>
				<th>Txn Type</th>
				<td><?php echo $row_data['card_pump']; ?></td>
			</tr>
			
			<tr>
				<th>Card/Pump</th>
				<td><?php echo $row_data['card']; ?></td>
				<th>Company</th>
				<td><?php echo $row_data['dsl_company']; ?></td>
			</tr>
			
			<tr>
				<th>Date</th>
				<td><?php echo $row_data['date']; ?></td>
				<th>Branch</th>
				<td><?php echo $row_data['branch']." ($row_data[emp_name])"; ?></td>
			</tr>
			
		</table>
	</div>
	<?php
}
else if($trans_type=='FRT_ADV')
{
	$get_data = Qry($conn,"SELECT t.trans_id,t.vou_id,t.amount,t.date,t.narration,t.branch,e.name as emp_name 
	FROM dairy.freight_adv AS t 
	LEFT OUTER JOIN emp_attendance AS e ON e.code = t.branch_user 
	WHERE t.id='$id'");
	
	if(!$get_data)
	{
		AlertErrorTopRight("Error while processing request !");
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		exit();
	}
	
	$row_data = fetchArray($get_data);
	?>
	<div class="form-group col-md-12">
		<table class="table table-bordered table-striped" style="font-size:12px !important;font-family:Calibri !important">
			<tr>
				<th>Txn Id</th>
				<td><?php echo $row_data['trans_id']; ?></td>
				<th>Vou_No.</th>
				<td><?php echo $row_data['vou_id']; ?></td>
			</tr>	
			
			<tr>
				<th>Date</th>
				<td><?php echo $row_data['date']; ?></td>
				<th>Branch</th>
				<td><?php echo $row_data['branch']." ($row_data[emp_name])"; ?></td>
			</tr>
			
			<tr>
				<th>Amount</th>
				<td colspan="3"><?php echo $row_data['amount']; ?></td>
			</tr>
			
			<tr>
				<th>Narration</th>
				<td colspan="3"><?php echo $row_data['narration']; ?></td>
			</tr>
			
		</table>
	</div>
	<?php
}
else if($trans_type=='FRT_COLL')
{
	$get_data = Qry($conn,"SELECT t.trans_id,t.vou_id,t.amount,t.date,t.narration,t.branch,e.name as emp_name 
	FROM dairy.freight_rcvd AS t 
	LEFT OUTER JOIN emp_attendance AS e ON e.code = t.branch_user 
	WHERE t.id='$id'");	
	
	if(!$get_data)
	{
		AlertErrorTopRight("Error while processing request !");
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		exit();
	}
	
	$row_data = fetchArray($get_data);
	?>
	<div class="form-group col-md-12">
		<table class="table table-bordered table-striped" style="font-size:12px !important;font-family:Calibri !important">
			<tr>
				<th>Txn Id</th>
				<td><?php echo $row_data['trans_id']; ?></td>
				<th>Vou_No.</th>
				<td><?php echo $row_data['vou_id']; ?></td>
			</tr>	
			
			<tr>
				<th>Date</th>
				<td><?php echo $row_data['date']; ?></td>
				<th>Branch</th>
				<td><?php echo $row_data['branch']." ($row_data[emp_name])"; ?></td>
			</tr>
			
			<tr>
				<th>Amount</th>
				<td colspan="3"><?php echo $row_data['amount']; ?></td>
			</tr>
			
			<tr>
				<th>Narration</th>
				<td colspan="3"><?php echo $row_data['narration']; ?></td>
			</tr>
			
		</table>
	</div>
	<?php
}
else if($trans_type=='EXPENSE')
{
	$get_data = Qry($conn,"SELECT t.trans_id,t.exp_name,t.exp_code,t.amount,t.copy,t.date,t.narration,t.branch,e.name as emp_name 
	FROM dairy.trip_exp AS t 
	LEFT OUTER JOIN emp_attendance AS e ON e.code = t.branch_user  
	WHERE t.id='$id'");	
	
	if(!$get_data)
	{
		AlertErrorTopRight("Error while processing request !");
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		exit();
	}
	
	$row_data = fetchArray($get_data);
	
	if($row_data['copy']!='')
	{
		$count_files=1;
		
		$copy_files=array();
		
		foreach(explode(",",$row_data['copy']) as $copy)
		{
			$copy_files[] = "<button type='button' style='font-size:10px' class='btn btn-xs btn-danger'><a style='color:#FFF;font-size:10px' target='_blank' href='https://rrpl.online/diary/$copy'>Upload: $count_files</a></button>";
			$count_files++;
		}
		
		$copy_files = implode("&nbsp;",$copy_files);
	}
	else
	{
		$copy_files = "<font color='red'>NA</font>";
	}
	?>
	<div class="form-group col-md-12">
		<table class="table table-bordered table-striped" style="color:#000;font-size:12px !important;font-family:Calibri !important">
			
			<tr>
				<th>Txn Id</th>
				<td><?php echo $row_data['trans_id']; ?></td>
				<th>Expense</th>
				<td><?php echo $row_data['exp_name']." ($row_data[exp_code])"; ?></td>
			</tr>	
			
			<tr>
				<th>Date</th>
				<td><?php echo $row_data['date']; ?></td>
				<th>Amount</th>
				<td><?php echo $row_data['amount']; ?></td>
			</tr>
			
			<tr>
				<th>Branch</th>
				<td colspan="3"><?php echo $row_data['branch']." ($row_data[emp_name])"; ?></td>
			</tr>
			
			<tr>
				<th>Uploads</th>
				<td colspan="3"><?php echo $copy_files; ?></td>
			</tr>
			
			<tr>
				<th>Narration</th>
				<td colspan="3"><?php echo $row_data['narration']; ?></td>
			</tr>
			
		</table>
	</div>
	<?php
}
else
{
	AlertErrorTopRight("Invalid Txn !");
	exit();
}
?>
		</div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-primary btn-sm" data-dismiss="modal">Close</button>
		</div>
      </div>
    </div>
  </div>
 
<script>
	$('#modal_view_txn_btn').click();
	$('#loadicon').fadeOut('slow');
</script>