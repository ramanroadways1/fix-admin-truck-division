<?php
require_once("connect.php");
?>

			<table id="example" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th>#</th>
                        <th>Truck No</th>
						<th>Trip No</th>
						<th>Trip Start</th>
						<th>No. of days</th>
						<th>Last Allowed</th>
						<th>Allow Hisab</th>
                      </tr>
                    </thead>
                    <tbody>
	<?php
	
$get_trip_lock_days = Qry($conn,"SELECT no_of_days FROM dairy._trip_lock_func ORDER BY id DESC LIMIT 1");

if(!$get_trip_lock_days){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while Processing Request","./");
	exit();
}

$row_trip_days = fetchArray($get_trip_lock_days);	
	
	$get_approvals = Qry($conn,"SELECT tno,trip_no,date(date) as start_date,(SELECT timestamp FROM dairy.hisab_branches_lock_allow WHERE 
	trip_no=dairy.trip.trip_no ORDER BY id DESC LIMIT 1) as last_allowed FROM dairy.trip GROUP by tno ORDER BY date ASC");
	
	if(!$get_approvals){
		AlertErrorTopRight("Error while processing request !");
		echo getMySQLError($conn);
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		exit();
	}
	
	if(numRows($get_approvals)==0)
	{
		echo "<tr>
			<td colspan='7'>No record found !</td>
			<td style='display: none'></td>
			<td style='display: none'></td>
			<td style='display: none'></td>
			<td style='display: none'></td>
			<td style='display: none'></td>
			<td style='display: none'></td>
			<td style='display: none'></td>
		</tr>";
	}
	else
	{
		$i=1;
		while($row = fetchArray($get_approvals))
		{
			$last_allowed = date("d-m-y h:i A",strtotime($row['last_allowed']));
			$datediff = strtotime(date("Y-m-d")) - strtotime($row['start_date']);
			$datediff = round($datediff / (60 * 60 * 24));
			
			// $timestamp = date("d-m-y h:i A",strtotime($row['timestamp']));
			
			$hisab_hours =  round((strtotime(date("Y-m-d H:i:s")) - strtotime($row['last_allowed']))/3600, 1);
			// $last_allowed_timestamp = date("d-m-y h:i A",strtotime($row['timestamp']));
				
			if($hisab_hours <= 24)
			{
				$last_allowed_alert="<font color='red'>Duplicate entry in Last 24 Hours.</font>";
			}
			else
			{
				$last_allowed_alert="";
			}
			
			echo "<tr>
				<td>$i</td>
				<td>$row[tno]</td>
				<td>$row[trip_no]</td>
				<td>".date('d/m/y',strtotime($row['start_date']))."</td>
				<td>".$datediff."</td>
				<td>$last_allowed<br>$last_allowed_alert</td>
				";
			
			if($datediff>$row_trip_days['no_of_days']){
				echo "<td>
				<button id='button$row[tno]' class='btn btn_allow_hisab btn-success btn-xs' disabled='disabled' type='button' onclick=UnlockHisab('$row[tno]','$row[trip_no]')><i class='fa fa-thumbs-o-up' aria-hidden='true'></i> Allow hisab</button>
				</td>";
			}	
			else{
				echo "<td></td>";
			}	
			echo "</tr>";
		$i++;	
		}
	}
	?>	
                    </tbody>
                  </table>

<script>				  
$("#loadicon").fadeOut('slow');
</script>	

<?php
$chk_update = Qry($conn,"SELECT id FROM _access_control WHERE username='$_SESSION[ediary_fix_admin]' AND func_id=(SELECT id FROM 
_access_control_func_list WHERE session_role='8' AND func_name='Allow_Hisab') AND u_update='1'");
			  
if(numRows($chk_update)>0)
{
	echo "<script>
		$('.btn_allow_hisab').attr('disabled',false);
		$('.btn_allow_hisab').attr('disabled',false);
	</script>";
}
?>	