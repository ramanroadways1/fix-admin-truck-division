<?php
include("header.php");

$chk = Qry($conn,"SELECT id FROM _access_control WHERE username='$_SESSION[ediary_fix_admin]' AND func_id=(SELECT id FROM 
_access_control_func_list WHERE session_role='8' AND func_name='Market_Bilty') AND u_view='1'");
			  
if(numRows($chk)==0)
{
	echo "<script>window.location.href='./';</script>";
	exit();
}
?>
<style>
label{
	font-size:11px !important;
}
input{
	font-size:12px !important;
}
.ui-autocomplete { z-index:2147483647; font-size:11px !important;}
</style>

<script>
$(function() {
		$("#own_tno").autocomplete({
		source: '../diary/autofill/get_own_vehicle.php',
		// appendTo: '#appenddiv',
		select: function (event, ui) { 
            $('#own_tno').val(ui.item.value);   
            // $('#veh_no_verify_own_id').val(ui.item.id);      
            // $('#veh_no_verify_own_wheeler').val(ui.item.wheeler);      
            return false;},
		change: function (event, ui) {
		if(!ui.item){
			$(event.target).val("");
            $(event.target).focus();
			$('#own_tno').val("");   
			// $('#veh_no_verify_own_id').val("");   
			// $('#veh_no_verify_own_wheeler').val("");   
			alert('Vehicle does not exists.');
		}}, 
	focus: function (event, ui){
	return false;
	}
});});
</script>


<script>
$(function() {
		$("#mb_from").autocomplete({
		source: '../b5aY6EZzK52NA8F/autofill/get_location.php',
		appendTo: '#EditModal',
		select: function (event, ui) { 
            $('#mb_from').val(ui.item.value);   
            $('#mb_from_id').val(ui.item.id);      
            return false;},
		change: function (event, ui) {
		if(!ui.item){
			$(event.target).val("");
            $(event.target).focus();
			$('#mb_from').val("");   
			$('#mb_from_id').val("");   
			alert('Location does not exists.');
		}}, 
	focus: function (event, ui){
	return false;
	}
});});

$(function() {
		$("#mb_to").autocomplete({
		source: '../b5aY6EZzK52NA8F/autofill/get_location.php',
		appendTo: '#EditModal',
		select: function (event, ui) { 
            $('#mb_to').val(ui.item.value);   
            $('#mb_to_id').val(ui.item.id);      
            return false;},
		change: function (event, ui) {
		if(!ui.item){
			$(event.target).val("");
            $(event.target).focus();
			$('#mb_to').val("");   
			$('#mb_to_id').val("");   
			alert('Location does not exists.');
		}}, 
	focus: function (event, ui){
	return false;
	}
});});
</script>

<script>
  $(function() {  
      $("#mb_broker").autocomplete({
			appendTo: '#EditModal',
			source: function(request, response){ 
			$.ajax({
                  url: "../diary/autofill/broker_name.php",
                  type: 'post',
                  dataType: "json",
                  data: {
                   search: request.term,
                  },
              success: function(data) {
              response(data);}});
             },
              select: function (event, ui) { 
              $(event.target).val(ui.item.value);   
               $('#mb_broker_pan').val(ui.item.pan);     
               $('#broker_id').val(ui.item.id);     
			   return false;
              },
              change: function (event, ui) {  
                if(!ui.item){
                $(event.target).val(""); 
                $(event.target).focus();
				$("#mb_broker_pan").val('');
				$("#broker_id").val('');
				alert('Broker does not exist !'); 
              } 
              },
			}); 
      }); 
</script>

<script>
  $(function() {  
      $("#mb_bill_party").autocomplete({
		  appendTo: '#EditModal',
			source: function(request, response){ 
			$.ajax({
                  url: "../diary/autofill/billing_party.php",
                  type: 'post',
                  dataType: "json",
                  data: {
                   search: request.term,
                  },
              success: function(data) {
              response(data);}});
             },
              select: function (event, ui) { 
              $(event.target).val(ui.item.value);   
               $('#billing_party_pan').val(ui.item.pan);     
               $('#billing_party_id').val(ui.item.id);     
               // $('#billing_party_addr').val(ui.item.addr);     
			   return false;
              },
              change: function (event, ui) {  
                if(!ui.item){
                $(event.target).val(""); 
                $(event.target).focus();
				$("#billing_party_pan").val('');
				$("#billing_party_id").val('');
				// $("#billing_party_addr").val('');
				alert('Billing Party does not exist !'); 
              } 
              },
			}); 
      }); 
</script> 



<div class="content-wrapper">
      <section class="content-header">
          <h1 style="font-size:16px;">Manage : MARKET Bilty</h1>
       </section>
       
	   <section class="content">
          <div class="row">
            <div class="col-xs-12">
			<div class="box">
                <div class="box-body">
				
				<div class="col-md-12">&nbsp;</div>
				
				<div class="col-md-12">
					<div class="row">
						
						<div class="form-group col-md-3">
							<label style="font-size:12px !important">Bilty Number</label>
							<input style="font-size:12px !important" autocomplete="off" oninput="this.value=this.value.replace(/[^A-Z .a-z0-9]/,'');CheckInput('lrno');" type="text" class="form-control" id="lrno" />
						</div>
						
						<div class="form-group col-md-1">
							<label>&nbsp;</label>
							<?php if(!isMobile()) { echo "<br />"; } ?>
							<span style="font-size:12px !important;color:maroon">-- OR --</span>
						</div>
						
						<div class="form-group col-md-3">
							<label style="font-size:12px !important">Vehicle No.</label>
							<input style="font-size:12px !important" autocomplete="off" type="text" oninput="this.value=this.value.replace(/[^A-Za-z0-9]/,'');CheckInput('tno');" class="form-control" id="own_tno" />
						</div>
						
						<div class="form-group col-md-3">
							<label style="font-size:12px !important">Date range.</label>
							<select style="font-size:12px !important" name="duration" id="duration" class="form-control" required>
								<option style="font-size:12px !important" value="">---SELECT---</option>
								<option style="font-size:12px !important" value="-0 days">Today's</option>
								<option style="font-size:12px !important" value="-1 days">Last 2 days</option>
								<option style="font-size:12px !important" value="-4 days">Last 5 days</option>
								<option style="font-size:12px !important" value="-6 days">Last 7 days</option>
								<option style="font-size:12px !important" value="-9 days">Last 10 days</option>
								<option style="font-size:12px !important" value="-14 days">Last 15 days</option>
								<option style="font-size:12px !important" value="-29 days">Last 30 days</option>
								<option style="font-size:12px !important" value="-59 days">Last 60 days</option>
								<option style="font-size:12px !important" value="-89 days">Last 90 days</option>
								<option style="font-size:12px !important" value="-119 days">Last 120 days</option>
								<option style="font-size:12px !important" value="FULL">FULL REPORT</option>
							</select>
						</div>
						
						<div class="form-group col-md-2">
							<?php if(!isMobile()) { echo "<label>&nbsp;</label><br />"; } ?>
							<button type="button" style="margin-top:1px;font-size:12px !important" onclick="Search()" class="btn btn-sm btn-success <?php if(isMobile()) { echo "btn-block"; } ?>" id="add_btn"><i class="fa fa-search" aria-hidden="true"></i> &nbsp; Search</button>
						</div>
					</div>
				</div>
				
<script>				
function CheckInput(elem)
{
	if(elem=='tno')
	{
		$('#lrno').val('');
	}
	else if(elem=='lrno')
	{
		$('#market_tno').val('');
		$('#duration').val('');
	}
}
</script>				
				
				<div class="col-md-12">&nbsp;</div>
				
				<div class="col-md-12 table-responsive" id="load_table" style="overflow:auto">
              
				 </div> 
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

<div id="func_result"></div>  
<div id="modal_load_res"></div>  

<script>
function EditVoucher(id)
{
	$('#loadicon').show();
		jQuery.ajax({
		url: "modal_edit_market_bilty.php",
		data: 'id=' + id,
		type: "POST",
		success: function(data) {
			$("#func_result").html(data);
		},
		error: function() {}
	});
}

function DeleteVoucher(id,vou_no)
{
	$('#loadicon').show();
	jQuery.ajax({
		url: "delete_market_bilty.php",
		data: 'vou_no=' + vou_no + '&id=' + id,
		type: "POST",
		success: function(data) {
			$("#func_result").html(data);
		},
		error: function() {}
	});
}
</script>

<button id="modal_open_btn" style="display:none" data-toggle="modal" data-target="#EditModal"></button>

<script type="text/javascript">
$(document).ready(function (e) {
$("#EditMbForm").on('submit',(function(e) {
$("#loadicon").show();
$("#edit_modal_btn").attr("disabled", true);
e.preventDefault();
	$.ajax({
	url: "./save_market_bilty_edit.php",
	type: "POST",
	data:  new FormData(this),
	contentType: false,
	cache: false,
	processData:false,
	success: function(data){
		$("#result_edits").html(data);
	},
	error: function() 
	{} });}));});
</script>   

<form id="EditMbForm" autocomplete="off" style="font-size:12px !important">   
<div class="modal fade" id="EditModal" role="dialog" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-lg">
      <div class="modal-content" style="">
		
		<div class="modal-header bg-primary">
			Edit MARKET Bilty : <span id="bilty_no1"></span>
		  </div>
	  
		<div class="modal-body">
		<div class="row">
			
		<div class="form-group col-md-4">
			<label>LR Date. <sup><font color="red">*</font></sup></label>
			<input style="font-size:12px !important" id="mb_lr_date" type="date" name="lr_date" max="<?php echo date("Y-m-d"); ?>" required 
			pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}" class="form-control" />
		</div>	
		
		<div class="form-group col-md-4">
			<label>Party LR's No. <sup><font color="red">* (1234,1245)</font></sup></label>
			<input style="font-size:12px !important" id="mb_plr" type="text" oninput="this.value=this.value.replace(/[^a-zA-Z0-9,]/,'')" name="lr_no" class="form-control" />
		</div>	
		
		<div class="form-group col-md-4">
			<label>From. <sup><font color="red">*</font></sup></label>
			<input style="font-size:12px !important" oninput="this.value=this.value.replace(/[^a-z A-Z0-9.]/,'')" type="text" name="from" id="mb_from" class="form-control" required />
		</div>	
		
		<div class="form-group col-md-4">
			<label>To. <sup><font color="red">*</font></sup></label>
			<input style="font-size:12px !important" type="text" oninput="this.value=this.value.replace(/[^a-z A-Z0-9.]/,'')" name="to" id="mb_to" class="form-control" required />
		</div>	
		
		<div class="form-group col-md-4">
			<label>Broker Name. <sup><font color="red">*</font></sup></label>
			<input style="font-size:12px !important" type="text" name="broker" id="mb_broker" class="form-control" required />
		</div>	
		
		<div class="form-group col-md-4">
			<label>Broker PAN. <sup><font color="red">*</font></sup></label> 
			<input style="font-size:12px !important" type="text" name="broker_pan" id="mb_broker_pan" class="form-control" readonly required />
		</div>
		
		<input type="hidden" id="broker_id" name="broker_id">
		<input type="hidden" id="billing_party_id" name="billing_party_id">
		
		<input  type="hidden" id="mb_from_id" name="from_id">
		<input type="hidden" id="mb_to_id" name="to_id">
		<input type="hidden" id="modal_id" name="id">
		
		<div class="form-group col-md-4">
			<label>Billing Party. <sup><font color="red">*</font></sup></label>
			<input style="font-size:12px !important" oninput="this.value=this.value.replace(/[^a-z A-Z0-9.]/,'')" type="text" name="billing_party" id="mb_bill_party" class="form-control" required />
		</div>	
		
		<div class="form-group col-md-4">
			<label>Billing Party PAN. <sup><font color="red">*</font></sup></label>
			<input style="font-size:12px !important" type="text" oninput="this.value=this.value.replace(/[^a-z A-Z0-9.]/,'')" name="billing_party_pan" id="billing_party_pan" class="form-control" readonly />
		</div>	
		
		<input type="hidden" id="awt_db">
		<input type="hidden" id="cwt_db">
		<input type="hidden" id="rate_db">
		<input type="hidden" id="freight_db">
		
		<script>
		function BillingType(elem)
		{
			if(elem=='FOC')
			{
				$('#act_weight').val('0');
				$('#charge_weight').val('0');
				$('#rate_pmt').val('0');
				$('#freight_bilty').val('0');
				
				$('#act_weight').attr('readonly',true);
				$('#charge_weight').attr('readonly',true);
				$('#rate_pmt').attr('readonly',true);
				$('#freight_bilty').attr('readonly',true);
			}
			else
			{
				$('#act_weight').val($('#awt_db').val());
				$('#charge_weight').val($('#cwt_db').val());
				$('#rate_pmt').val($('#rate_db').val());
				$('#freight_bilty').val($('#freight_db').val());
				
				$('#act_weight').attr('readonly',false);
				$('#charge_weight').attr('readonly',false);
				$('#rate_pmt').attr('readonly',false);
				$('#freight_bilty').attr('readonly',false);
			}
		}
		</script>
		
		<div class="form-group col-md-4">
			<label>LR BY : <sup><font color="red">*</font></sup></label>
			<select style="font-size:12px !important" name="lr_by" id="mb_lr_by" onchange="LRBy(this.value)" class="form-control" required>
				<option style="font-size:12px !important" value="">Select an option</option>
				<option style="font-size:12px !important" value="SHIVANI">SHIVANI</option>
				<option style="font-size:12px !important" value="OTHERS">OTHERS</option>
			</select>
		</div>
		
		<script>
		function LRBy(elem)
		{
			if(elem=='SHIVANI')
			{
				$('#billing_type_others').hide();
				$('#billing_type_shivani').show();
				$('#charge_weight').attr('oninput','');
				$('#rate_pmt').attr('oninput','');
				$('#freight_bilty').attr('oninput','');
				
				$('#rate_pmt').attr('readonly',true);
				$('#billing_type').attr('required',false);
				$('#freight_bilty').attr('readonly',true);
				
				$('#rate_pmt').val('0');
				$('#freight_bilty').val('0');
			}
			else if(elem=='OTHERS')
			{
				$('#billing_type_others').show();
				$('#billing_type').attr('required',true);
				$('#billing_type_shivani').hide();
				$('#charge_weight').attr('oninput','CalculateByWeight()');
				$('#rate_pmt').attr('oninput','SumRate1()');
				$('#freight_bilty').attr('oninput','SumRate2()');
				
				$('#rate_pmt').attr('readonly',false);
				$('#freight_bilty').attr('readonly',false);
				
				$('#rate_pmt').val($('#rate_db').val());
				$('#freight_bilty').val($('#freight_db').val());
			}
			else
			{
				$('#billing_type_others').show();
				$('#billing_type_shivani').hide();
				$('#billing_type').attr('required',true);
				$('#charge_weight').attr('oninput','CalculateByWeight()');
				$('#rate_pmt').attr('oninput','SumRate1()');
				$('#freight_bilty').attr('oninput','SumRate2()');
				
				$('#rate_pmt').attr('readonly',false);
				$('#freight_bilty').attr('readonly',false);
				
				$('#rate_pmt').val($('#rate_db').val());
				$('#freight_bilty').val($('#freight_db').val());
			}
		}
		</script>
		
		<div id="billing_type_others" class="form-group col-md-3">
			<label>Billing TYPE. <sup><font color="red">*</font></sup></label>
			<select style="font-size:12px !important" name="billing_type" id="billing_type" onchange="BillingType(this.value)" class="form-control" required>
				<option style="font-size:12px !important" value="">Select an option</option>
				<option style="font-size:12px !important" value="TO_PAY">TO PAY</option>
				<option style="font-size:12px !important" value="TBB">TO BE BILLED</option>
				<option style="font-size:12px !important" value="FOC" disabled>FREE OF COST</option>
			</select>
		</div>
		
		<div id="billing_type_shivani" style="display:none" class="form-group col-md-3">
			<label>Billing TYPE. <sup><font color="red">*</font></sup></label>
			<input style="font-size:12px !important" type="text" name="billing_type2" id="billing_type2" class="form-control" value="TBB" required readonly />
		</div>
		
		<div class="form-group col-md-3">
			<label>Actual Weight. <sup><font color="red">*</font></sup></label>
			<input style="font-size:12px !important" type="number" min="0" step="any" max="90" name="act_weight" id="act_weight" class="form-control" required />
		</div>	
		
		<script>
		function CalculateByWeight()
		{
			if($("#rate_pmt").val()=='')
			{
				var rate_pmt = 0;
			}
			else
			{
				var rate_pmt = $("#rate_pmt").val();
			}
			
			$("#freight_bilty").val(Math.round(Number($("#charge_weight").val()) * Number(rate_pmt)).toFixed(2));
		}
		
		function SumRate1()
		{
			var weight = $('#charge_weight').val();
			
			if(weight=='')
			{
				$('#rate_pmt').val('');
				$('#freight_bilty').val('');
				$('#charge_weight').focus();
			}
			else
			{
				$("#freight_bilty").val(Math.round(Number($("#charge_weight").val()) * Number($("#rate_pmt").val())).toFixed(2));
			}
		}
		
			function SumRate2() 
			{
				var weight = $('#charge_weight').val();
						 
				if(weight=='')
				{
					$('#rate_pmt').val('');
					$('#freight_bilty').val('');
					$('#charge_weight').focus();
				}	
				else
				{
					$("#rate_pmt").val((Number($("#freight_bilty").val()) / Number($("#charge_weight").val())).toFixed(2));
				}	
			}
		</script>
		
		<div class="form-group col-md-3">
			<label>Charge Weight. <sup><font color="red">*</font></sup></label>
			<input style="font-size:12px !important" type="number" min="0" max="90" oninput="CalculateByWeight();" step="any" name="charge_weight" id="charge_weight" class="form-control" required />
		</div>	
		
		<div class="form-group col-md-3">
			<label>Rate. <sup><font color="red">*</font></sup></label>
			<input style="font-size:12px !important" type="number" min="0" step="any" oninput="SumRate1();" name="rate_pmt" id="rate_pmt" class="form-control" required />
		</div>
		
		<div class="form-group col-md-4">
			<label>Freight. <sup><font color="red">*</font></sup></label>
			<input style="font-size:12px !important" type="number" min="0" oninput="SumRate2();" name="freight_bilty" id="freight_bilty" class="form-control" required />
		</div>
		
		<div class="form-group col-md-4">
			<label>Vehicle Placed By. <sup><font color="red">*</font></sup></label>
			<select style="font-size:12px !important" name="veh_placer" id="mb_veh_placer" class="form-control" required>
				<option style="font-size:12px !important" value="">Select an option</option>
				<?php
				$qry_veh_placer=Qry($conn,"SELECT username FROM dairy.user WHERE role='1' ORDER BY username ASC");
				
				if(!$qry_veh_placer){
					echo getMySQLError($conn);
					errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
					exit();
				}
				
				$qry_veh_placer_branch=Qry($conn,"SELECT username FROM user WHERE role='2' AND username NOT IN('DUMMY','HEAD') ORDER BY username ASC");
				
				if(!$qry_veh_placer_branch){
					echo getMySQLError($conn);
					errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
					exit();
				}
				
				if(numRows($qry_veh_placer)>0)
				{
					while($row_veh=fetchArray($qry_veh_placer))
					{
						?>
							<option style='font-size:12px !important' value="<?php echo $row_veh['username']; ?>"><?php echo $row_veh['username']; ?></option>
						<?php
					}
				}
				
				if(numRows($qry_veh_placer_branch)>0)
				{
					while($row_veh_branch=fetchArray($qry_veh_placer_branch))
					{
						?>
							<option style='font-size:12px !important' value="<?php echo $row_veh_branch['username']; ?>"><?php echo $row_veh_branch['username']; ?></option>
						<?php
					}
				}
				?>
			</select>
		</div>
		
		</div>
        </div>
		
		<div id="result_edits"></div>
		
        <div class="modal-footer">
          <button type="submit" id="edit_modal_btn" class="btn btn-sm btn-danger">Update</button>
          <button type="button" class="btn-sm btn btn-primary" id="modal_edit_mb_close_btn" data-dismiss="modal">Close</button>
		</div>
      </div>
    </div>
  </div>
 </form>

<script>	
function Search()
{
	var tno = $('#own_tno').val();
	var lrno = $('#lrno').val();
	var duration = $('#duration').val();
	
	if(tno=='' && lrno=='' && duration=='')
	{
		Swal.fire({icon: 'warning',html: '<font size=\'2\' color=\'black\'>Atleast one field is required !</font>',});
	}
	else
	{
		$('#loadicon').show();
			jQuery.ajax({
				url: "_load_market_bilty.php",
				data: 'tno=' + tno + '&duration=' + duration + '&lrno=' + lrno,
				type: "POST",
				success: function(data) {
					$("#load_table").html(data);
					$('#example').DataTable({ 
					"lengthMenu": [ [50, 100, 500, -1], [50, 100,500, "All"] ], 
						"destroy": true, //use for reinitialize datatable
					});
				},
				error: function() {}
		});
	}
}
</script>

<?php include("footer.php") ?>
