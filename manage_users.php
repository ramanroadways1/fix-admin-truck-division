<?php include("header.php"); ?>

<?php
if($_SESSION['ediary_fix_admin']!='TRUCKS')
{
	echo "<script>window.location.href='./';</script>";
	exit();
}
?>

<div class="content-wrapper">
      <section class="content-header">
          <h1 style="font-size:16px;">Manage Users : </h1>
       </section>
       
	   <section class="content">
          <div class="row">
            <div class="col-xs-12">
			<div class="box">
                <div class="box-body">
				<div class="col-md-12">
					<div class="row">
						<div class="form-group col-md-3">
							<label>Username <font color="red"><sup>*</sup></font></label>
							<input autocomplete="off" oninput="this.value=this.value.replace(/[^A-Za-z0-9-@_#.]/,'')" type="text" class="form-control" id="username" />
						</div>
						
						<div class="form-group col-md-3">
							<label>Password <font color="red"><sup>*</sup></font></label>
							<input autocomplete="off" type="text" oninput="this.value=this.value.replace(/[^A-Za-z0-9-_@#.]/,'')" class="form-control" id="password" />
						</div>
						
						<div class="form-group col-md-3">
							<label>Mobile No. <font color="red"><sup>*</sup></font></label>
							<input autocomplete="off" maxlength="10" type="text" oninput="this.value=this.value.replace(/[^0-9]/,'')" class="form-control" id="mobile_no" />
						</div>
						
						<div class="form-group col-md-3">
							<?php if(!isMobile()) { echo "<label>&nbsp;</label><br />"; } ?>
							<button type="button" onclick="AddUserFunc()" class="btn btn-sm btn-success <?php if(isMobile()) { echo "btn-block"; } ?>" id="add_btn"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> &nbsp; Add User</button>
						</div>
					</div>
				</div>
				
				<div class="col-md-12">&nbsp;</div>
				
				<div class="col-md-12 table-responsive" id="load_table">
              
				 </div> 
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

<div id="func_result"></div>  
 
<script>
function AddUserFunc()
{
	var username = $('#username').val();
	var password = $('#password').val();
	var mobile_no = $('#mobile_no').val();
	
	if(username=='')
	{
		Swal.fire({icon: 'warning',html: '<font size=\'2\' color=\'black\'>Enter username first !</font>',});
	}
	else if(password=='')
	{
		Swal.fire({icon: 'warning',html: '<font size=\'2\' color=\'black\'>Enter password first !</font>',});
	}
	else if(mobile_no=='')
	{
		Swal.fire({icon: 'warning',html: '<font size=\'2\' color=\'black\'>Enter mobile number first !</font>',});
	}
	else
	{
		$('#add_btn').attr('disabled',true);
		$('#loadicon').show();
		jQuery.ajax({
			url: "save_add_user.php",
			data: 'username=' + username + '&password=' + password + '&mobile_no=' + mobile_no,
			type: "POST",
			success: function(data) {
			$("#func_result").html(data);
			},
			error: function() {}
		});
	}
}
</script>
 
<script>	
function ToggleUserStatus(id)
{
	var value1 = $('#user_status_html_'+id).val();
		
	if(value1=="1"){
		var value2 = "0";
	}
	else{
		var value2 = "1";
	}
		
	$('#loadicon').show();
		jQuery.ajax({
		url: "manage_users_save.php",
		data: 'id=' + id + '&value=' + value2,
		type: "POST",
		success: function(data) {
		$("#func_result").html(data);
		},
	error: function() {}
	});
}
</script>	

<script>	
function LoadTable()
{
	jQuery.ajax({
		url: "_load_users.php",
		data: 'id=' + 'ok',
		type: "POST",
		success: function(data) {
			$("#load_table").html(data);
			$('#example').DataTable({ 
				"destroy": true, //use for reinitialize datatable
			});
		},
		error: function() {}
	});
}

LoadTable();
</script>

<?php include("footer.php") ?>