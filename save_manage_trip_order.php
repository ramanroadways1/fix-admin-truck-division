<?php
require_once("./connect.php");

$add_after_trip = trim(escapeString($conn,strtoupper($_POST['add_after_trip'])));
$trip_id = trim(escapeString($conn,strtoupper($_POST['trip_id'])));
$timestamp = date("Y-m-d H:i:s");
$date = date("Y-m-d");

if($add_after_trip=='' || $add_after_trip==0)
{
	AlertErrorTopRight("Select trip first !");
	echo "<script> $('#btn_confirm_$trip_id').attr('disabled',false); </script>";
	exit();
}
	
if($trip_id=='' || $trip_id==0)
{
	AlertErrorTopRight("Trip not found !");
	echo "<script> $('#btn_confirm_$trip_id').attr('disabled',false); </script>";
	exit();
}

$chk_trip = Qry($conn,"SELECT tno,from_station,to_station,lr_type,date FROM dairy.trip WHERE id='$trip_id'");

if(!$chk_trip){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	AlertErrorTopRight("Error while Processing Request !");
	exit();
}
	
if(numRows($chk_trip)==0)
{
	AlertErrorTopRight("Running Trip not found !");
	echo "<script> $('#btn_confirm_$trip_id').attr('disabled',false); </script>";
	exit();
}

$row_trip1 = fetchArray($chk_trip);

$old_trip_from = $row_trip1['from_station'];
$old_trip_to = $row_trip1['to_station'];
$old_trip_vou_no = $row_trip1['lr_type'];
$old_trip_date = $row_trip1['date'];

$tno = $row_trip1['tno'];

$chk_trip2 = Qry($conn,"SELECT tno,from_station,to_station,lr_type,date FROM dairy.trip WHERE id='$add_after_trip'"); 

if(!$chk_trip2){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	AlertErrorTopRight("Error while Processing Request !");
	exit();
}
	
if(numRows($chk_trip2)==0)
{
	AlertErrorTopRight("Trip - 2 not found !");
	echo "<script> $('#btn_confirm_$trip_id').attr('disabled',false); </script>";
	exit();
}

$row_trip2 = fetchArray($chk_trip2);

$new_trip_from = $row_trip2['from_station'];
$new_trip_to = $row_trip2['to_station'];
$new_trip_vou_no = $row_trip2['lr_type'];
$new_trip_date = $row_trip2['date'];

if($row_trip1['tno'] != $row_trip2['tno'])
{
	AlertErrorTopRight("Vehicle not verified !");
	echo "<script> $('#btn_confirm_$trip_id').attr('disabled',false); </script>";
	exit();
}

if($trip_id == $add_after_trip)
{
	AlertErrorTopRight("Something went wrong !");
	echo "<script> $('#btn_confirm_$trip_id').attr('disabled',false); </script>";
	exit();
}

include("../diary/close_trip/_hisab_func.php");
				
StartCommit($conn);
$flag = true;

$copy_trips_to_cache = CopyTripToTempTable($conn,$tno,$page_name,$trip_id,$add_after_trip);

if($copy_trips_to_cache==false){
	$flag = false;
}
	
$get_new_trip_id = Qry($conn,"UPDATE dairy.trip_cache t1 
INNER JOIN dairy.trip t2 
ON t1.trip_id_ext = t2.trip_id_backup 
SET t1.trip_id_new = t2.id 
WHERE t1.tno='$tno' AND t1.trip_id_ext = t2.trip_id_backup");

if(!$get_new_trip_id){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

$get_cache = Qry($conn,"SELECT trip_id_ext,trip_id_new,cash,cheque,rtgs,diesel,freight,freight_collected,driver_naame,expense 
FROM dairy.trip_cache WHERE tno='$tno'");

if(!$get_cache){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

if(numRows($get_cache)>0)
{
	while($row_trip_id = fetchArray($get_cache))
	{
		$new_trip_id = $row_trip_id['trip_id_new'];
		$old_trip_id = $row_trip_id['trip_id_ext'];
		
		if($row_trip_id['cash']>0)
		{
			$update_adv_trip_id = Qry($conn,"UPDATE dairy.cash SET trip_id='$new_trip_id' WHERE trip_id='$old_trip_id'");	
			
			if(!$update_adv_trip_id){
				$flag = false;
				errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			}
		}
		
		if($row_trip_id['cheque']>0)
		{
			$update_chq_trip_id = Qry($conn,"UPDATE dairy.cheque SET trip_id='$new_trip_id' WHERE trip_id='$old_trip_id'");	
			
			if(!$update_chq_trip_id){
				$flag = false;
				errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			}
		}
		
		if($row_trip_id['rtgs']>0)
		{
			$update_rtgs_trip_id = Qry($conn,"UPDATE dairy.rtgs SET trip_id='$new_trip_id' WHERE trip_id='$old_trip_id'");	
			
			if(!$update_rtgs_trip_id){
				$flag = false;
				errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			}
		}
		
		if($row_trip_id['diesel']>0)
		{
			$update_diesel_trip_id = Qry($conn,"UPDATE dairy.diesel SET trip_id='$new_trip_id' WHERE trip_id='$old_trip_id'");	
			
			if(!$update_diesel_trip_id){
				$flag = false;
				errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			}
		}
		
		if($row_trip_id['freight']>0)
		{
			$update_freight_adv_trip_id = Qry($conn,"UPDATE dairy.freight_adv SET trip_id='$new_trip_id' WHERE trip_id='$old_trip_id'");	
			
			if(!$update_freight_adv_trip_id){
				$flag = false;
				errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			}
		}
		
		if($row_trip_id['freight_collected']>0)
		{
			$update_freight_rcvd_trip_id = Qry($conn,"UPDATE dairy.freight_rcvd SET trip_id='$new_trip_id' WHERE trip_id='$old_trip_id'");	
			
			if(!$update_freight_rcvd_trip_id){
				$flag = false;
				errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			}
		}
		
		if($row_trip_id['driver_naame']>0)
		{
			$update_naame_trip_id = Qry($conn,"UPDATE dairy.driver_naame SET trip_id='$new_trip_id' WHERE trip_id='$old_trip_id'");	
			
			if(!$update_naame_trip_id){
				$flag = false;
				errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			}
		}
		
		if($row_trip_id['expense']>0)
		{
			$update_exp_trip_id = Qry($conn,"UPDATE dairy.trip_exp SET trip_id='$new_trip_id' WHERE trip_id='$old_trip_id'");	
			
			if(!$update_exp_trip_id){
				$flag = false;
				errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			}
		}
		
		$update_driver_book_trip_id = Qry($conn,"UPDATE dairy.driver_book SET trip_id='$new_trip_id' WHERE trip_id='$old_trip_id'");	
		
		if(!$update_driver_book_trip_id){
			$flag = false;
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		}
	}
}

$get_cache_lrs = Qry($conn,"SELECT trip_id_ext,trip_id_new,lr_type,oxygen_lr,pod FROM dairy.trip_cache WHERE tno='$tno' AND 
lr_type NOT IN('EMPTY','ATLOADING','CON20','CON40')");

if(!$get_cache_lrs){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

while($row_cache2 = fetchArray($get_cache_lrs))
{
	$new_trip_id = $row_cache2['trip_id_new'];
	$old_trip_id = $row_cache2['trip_id_ext'];
	
	foreach(explode(",",$row_cache2['lr_type']) as $vou_no)
	{
		if(substr($vou_no,3,1)=='M')
		{
			$update_trip_id_in_vou = Qry($conn,"UPDATE mkt_bilty SET trip_id='$new_trip_id' WHERE bilty_no='$vou_no'");
			
			if(!$update_trip_id_in_vou){
				$flag = false;
				errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			}
		}
		else 
		{
			$update_trip_id_in_vou = Qry($conn,"UPDATE freight_form_lr SET trip_id='$new_trip_id' WHERE frno='$vou_no'");
			
			if(!$update_trip_id_in_vou){
				$flag = false;
				errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			}
		}
		
		if($row_cache2['pod']>0)
		{
			$update_pod_trip_id = Qry($conn,"UPDATE dairy.rcv_pod SET trip_id='$new_trip_id' WHERE vou_no='$vou_no' AND trip_id='$old_trip_id'");
			
			if(!$update_pod_trip_id){
				$flag = false;
				errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			}
			
			$update_pod_trip_id_main = Qry($conn,"UPDATE rcv_pod SET trip_id='$new_trip_id' WHERE frno='$vou_no' AND trip_id='$old_trip_id'");
			
			if(!$update_pod_trip_id_main){
				$flag = false;
				errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			}
		}
	}
	
	if($row_cache2['oxygen_lr']=="1")
	{
		$update_trip_id_in_oxygen_lr = Qry($conn,"UPDATE oxygen_olr SET trip_id='$new_trip_id' WHERE frno='$vou_no'");
			
		if(!$update_trip_id_in_oxygen_lr){
			$flag = false;
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		}
	}
}

$log_data = "Trip ($old_trip_from to $old_trip_to, VouNo: $old_trip_vou_no, date: $old_trip_date) Added After ($new_trip_from to $new_trip_to, 
VouNo: $new_trip_vou_no, date: $new_trip_date)";
	
$insert_log = Qry($conn,"INSERT INTO dairy.ediary_admin_log(code,action,desct,timestamp) VALUES ('$tno','Trip_Order_Change','$log_data','$timestamp')");	
	
if(!$insert_log){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

$dlt_cache_data2 = Qry($conn,"DELETE FROM dairy.trip_cache WHERE tno='$tno'");

if(!$dlt_cache_data2){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}
	
if($flag)
{
	MySQLCommit($conn);
	closeConnection($conn);
	
	AlertRightCornerSuccess("Updated Successfully !");
	echo "<script>$('#search_btn')[0].click(); $('#btn_confirm_$trip_id').attr('disabled',false); </script>";
	exit();
}
else
{
	MySQLRollBack($conn);
	closeConnection($conn); 
	
	AlertErrorTopRight("Error while Processing Request !");
	echo "<script> $('#btn_confirm_$trip_id').attr('disabled',false); </script>";
	exit();
}
	
?>