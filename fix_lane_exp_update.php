<?php
require_once("connect.php");

$date=date("Y-m-d");
$timestamp=date("Y-m-d H:i:s");

$id = escapeString($conn,($_POST['id']));
$rule_id = escapeString($conn,($_POST['rule_id']));
$empty_loaded = escapeString($conn,($_POST['empty_loaded']));
$expense = escapeString($conn,($_POST['expense']));
$exp_type = escapeString($conn,($_POST['exp_type']));
$exp_amount = escapeString($conn,($_POST['exp_amount']));

$exp_code = explode("_",$expense)[0];
$exp_name = explode("_",$expense)[1];

if($exp_amount<=0)
{
	AlertError("Invalid expense amount !");
	echo "<script>$('#update_exp_fix_btn_$id').attr('disabled',false);</script>";
	exit();
}

$check_exp = Qry($conn,"SELECT exp_code,exp_name,exp_type,exp_amount,timestamp FROM dairy.fix_lane_exp WHERE id='$id'");

if(!$check_exp){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	AlertError("Error while processing request !");
	exit();
}

$row_exp = fetchArray($check_exp);

$update_log = array();
$update_Qry = array();

if($exp_name!=$row_exp['exp_name'])
{
	$update_log[]="Exp_name : $row_exp[exp_name] to $exp_name";
	$update_Qry[]="exp_name='$exp_name'";
}

if($exp_code!=$row_exp['exp_code'])
{
	$update_log[]="Exp_code : $row_exp[exp_code] to $exp_code";
	$update_Qry[]="exp_code='$exp_code'";
}

if($exp_type!=$row_exp['exp_type'])
{
	$update_log[]="Exp_type : $row_exp[exp_type] to $exp_type";
	$update_Qry[]="exp_type='$exp_type'";
}

if($exp_amount!=$row_exp['exp_amount'])
{
	$update_log[]="Exp_amount : $row_exp[exp_amount] to $exp_amount";
	$update_Qry[]="exp_amount='$exp_amount'";
}

$update_log = implode(', ',$update_log); 
$update_Qry = implode(', ',$update_Qry); 

if($update_log=="")
{
	AlertError("Nothing to update !");
	echo "<script>$('#update_exp_fix_btn_$id').attr('disabled',false);</script>";
	exit();
}

$update_log = "Expense: $exp_name --> $update_log";

// if($exp_code==$row_exp['exp_code'] AND $exp_name==$row_exp['exp_name'] AND $exp_type==$row_exp['exp_type'] AND $exp_amount==$row_exp['exp_amount'])
// {
	// AlertError("Nothing to update !");
	// echo "<script>$('#update_exp_fix_btn_$id').attr('disabled',false);</script>";
	// exit();
// }

StartCommit($conn);
$flag = true;

$update_fix_exp = Qry($conn,"UPDATE dairy.fix_lane_exp SET $update_Qry WHERE id='$id'");

if(!$update_fix_exp){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

$insertLog = Qry($conn,"INSERT INTO dairy.ediary_admin_log(code,action,desct,branch,username,timestamp) VALUES ('$rule_id','Fix_Rule_Exp_Update',
'$update_log','Fix_Admin','$_SESSION[ediary_fix_admin]','$timestamp')");

if(!$insertLog){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

if($flag)
{
	MySQLCommit($conn);
	closeConnection($conn);
	AlertRightCornerSuccess("Expense Updated !");
	
	echo "<script>
		$('#update_exp_fix_btn_$id').attr('disabled',false);
	</script>";
	exit();
}
else
{
	MySQLRollBack($conn);
	closeConnection($conn);
	AlertError("Error while processing request !");
	echo "<script>
		$('#update_exp_fix_btn_$id').attr('disabled',false);
	</script>";
	exit();
}	
?>