<?php
require_once 'connect.php';

$output = '';

$timestamp=date("Y-m-d H:i:s");

$type = escapeString($conn,$_POST['report_type']);
$from_date = escapeString($conn,$_POST['from_date']);
$to_date = escapeString($conn,$_POST['to_date']);

if($type=='DRIVER_NAAME')
{
	$qry=Qry($conn,"SELECT n.trip_id,n.trans_id,n.tno,n.naame_type,n.lrno,n.qty,n.rate,n.amount,n.date,n.narration,n.branch,d.name 
	FROM dairy.driver_naame n 
	LEFT OUTER JOIN dairy.driver as d ON d.code=n.driver_code 
	WHERE n.date BETWEEN '$from_date' AND '$to_date' ORDER BY n.id ASC");
	
	if(!$qry)
	{
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		Redirect("Error while Processing Request.","./");
		exit();
	}
	
	if(numRows($qry)>0)
	{
		$output .= '
		 <table border="1">  
            <tr>  
               <th>TripId</th>  
			   <th>TransId</th>  
			   <th>TruckNo</th>  
			   <th>DriverName</th>  
			   <th>NaameType</th>  
			   <th>LR_Number</th>  
			   <th>QTY</th>  
			   <th>Rate</th>  
			   <th>Amount</th>  
			   <th>Date</th>  
			   <th>Narration</th>  
			   <th>Branch</th>  
            </tr>';
			
		 while($row = fetchArray($qry))
		  {
		    $output .= '
			<tr>  
				<td>'.$row["trip_id"].'</td>  
				<td>'.$row["trans_id"].'</td>  
				<td>'.$row["tno"].'</td>  
				<td>'.$row["name"].'</td>  
				<td>'.$row["naame_type"].'</td>  
				<td>'.$row["lrno"].'</td>  
				<td>'.$row["qty"].'</td>  
				<td>'.$row["rate"].'</td>  
				<td>'.$row["amount"].'</td>  
				<td>'.$row["date"].'</td>
			   <td>'.$row["narration"].'</td>
			   <td>'.$row["branch"].'</td>
			</tr>
		   ';
		  }
		  $output .= '</table>';
		  header('Content-Type: application/xls');
		  header('Content-Disposition: attachment; filename=DRIVER_NAAME.xls');
		  echo $output;
		  mysqli_close($conn);
	}
	else
	{
		echo "<script>
			alert('NO RESULT FOUND.');
			window.close();
		</script>";
		exit();
	}
}
else if($type=='EXP_SHEET')
{
	$qry=Qry($conn,"SELECT e.tno,e.trip_id,e.trans_id,e.exp_name,e.exp_code,e.amount,e.copy,e.date,e.narration,e.branch,
	o.comp,date(t.date) as trip_date,t.trip_no 
	FROM dairy.trip_exp as e 
	LEFT OUTER JOIN dairy.own_truck as o ON o.tno=e.tno
	LEFT OUTER JOIN dairy.trip_final as t ON t.trip_id=e.trip_id
	WHERE e.date BETWEEN '$from_date' AND '$to_date' ORDER BY e.id ASC");
	
	if(!$qry)
	{
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		Redirect("Error while Processing Request.","./");
		exit();
	}
	
	if(numRows($qry)>0)
	{
		$output .= '
		 <table border="1">  
            <tr>  
               <th>COMPANY</th>  
               <th>VOU DATE</th>  
			   <th>TRIP DATE</th>  
			   <th>BRANCH</th>  
			   <th>TRUCK NO</th>  
			   <th>TRIP NO</th>  
			   <th>EXP LEADGER CODE</th>  
			   <th>EXPENSE NAME</th>  
			   <th>AMOUNT</th>  
			   <th>NARRATION</th>  
			   <th>UPLOADS</th>  
			   <th>TRANS.ID</th>  
			</tr>';
			
		 while($row = fetchArray($qry))
		{
			if($row['copy']!='')
			{
				$url_file=array();				
			  foreach(explode(",",$row['copy']) as $rto_copy)
			  {
				$url_file[]="<a target='_blank' href='https://rrpl.online/diary/$rto_copy'>Upload</a>";
			  }
				  
			  $files = implode("<br>",$url_file);
			}
			else
			{
				$files = "";
			}
			  
		$output .= '
			<tr>  
				<td>'.$row["comp"].'</td>  
				<td>'.$row["date"].'</td>  
				<td>'.$row["trip_date"].'</td>  
				<td>'.$row["branch"].'</td>  
				<td>'.$row["tno"].'</td>  
				<td>'.$row["trip_no"].'</td>  
				<td>'.$row["exp_code"].'</td>  
				<td>'.$row["exp_name"].'</td>  
				<td>'.$row["amount"].'</td>  
				<td>'.$row["narration"].'</td>
				<td>'.$files.'</td>  
				<td>'.$row["trans_id"].'</td>  
			</tr>
		   ';
		  }
		  $output .= '</table>';
		  header('Content-Type: application/xls');
		  header('Content-Disposition: attachment; filename=EXP_SHEET.xls');
		  echo $output;
		  mysqli_close($conn);
	}
	else
	{
		echo "<script>
			alert('NO RESULT FOUND.');
			window.close();
		</script>";
		exit();
	}
}
else if($type=='ACTIVE_DRIVER')
{
	$qry=Qry($conn,"SELECT o.tno,d.name,d.lic,up.up as up_date,up.branch 
	FROM dairy.own_truck as o 
	LEFT OUTER JOIN dairy.driver as d ON d.code=o.driver_code 
	LEFT OUTER JOIN dairy.driver_up as up ON up.id=(SELECT MAX(id) FROM driver_up WHERE tno=o.tno)
	WHERE o.driver_code>0 ORDER BY o.tno ASC");
	
	if(!$qry)
	{
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		Redirect("Error while Processing Request.","./");
		exit();
	}
	
	if(numRows($qry)>0)
	{
		$output .= '
		 <table border="1">  
            <tr>  
               <th>TruckNo</th>  
               <th>DriverName</th>  
			   <th>LicNo</th>  
			   <th>UpDate</th>  
			   <th>UpBranch</th>  
			</tr>';
			
		 while($row = fetchArray($qry))
		  {
		    $output .= '
			<tr>  
				<td>'.$row["tno"].'</td>  
				<td>'.$row["name"].'</td>  
				<td>'.$row["lic"].'</td>  
				<td>'.$row["up_date"].'</td>  
				<td>'.$row["branch"].'</td>  
			</tr>
		   ';
		  }
		  $output .= '</table>';
		  header('Content-Type: application/xls');
		  header('Content-Disposition: attachment; filename=ACTIVE_DRIVER_e-Diary.xls');
		  echo $output;
		  mysqli_close($conn);
	}
	else
	{
		echo "<script>
			alert('NO RESULT FOUND.');
			window.close();
		</script>";
		exit();
	}
}
else if($type=='DRIVER')
{
	$qry=Qry($conn,"SELECT name,mobile,pan,lic,date,code FROM dairy.driver ORDER BY id ASC");
	
	if(!$qry)
	{
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		Redirect("Error while Processing Request.","./");
		exit();
	}
	
	if(numRows($qry)>0)
	{
		$output .= '
		 <table border="1">  
            <tr>  
               <th>Name</th>  
               <th>Mobile</th>  
               <th>PAN_No</th>  
			   <th>LicNo</th>  
			   <th>Date</th>  
			   <th>Code</th>  
			</tr>';
			
		 while($row = fetchArray($qry))
		  {
		    $output .= '
			<tr>  
				<td>'.$row["name"].'</td>  
				<td>'.$row["mobile"].'</td>  
				<td>'.$row["pan"].'</td>  
				<td>'.$row["lic"].'</td>  
				<td>'.$row["date"].'</td>  
				<td>'.$row["code"].'</td>  
			</tr>
		   ';
		  }
		  $output .= '</table>';
		  header('Content-Type: application/xls');
		  header('Content-Disposition: attachment; filename=ALL_DRIVER_DB.xls');
		  echo $output;
		  mysqli_close($conn);
	}
	else
	{
		echo "<script>
			alert('NO RESULT FOUND.');
			window.close();
		</script>";
		exit();
	}
}
else if($type=='DA')
{
	$qry=Qry($conn,"SELECT d.trip_no,d.tno,d.from_date,d.to_date,d.amount,d.branch,d.date,d.narration,d2.name
	FROM dairy.da_book as d 
	LEFT OUTER JOIN dairy.driver as d2 ON d2.code=d.driver_code 
	WHERE d.date BETWEEN '$from_date' AND '$to_date' ORDER BY d.id ASC");
	
	if(!$qry)
	{
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		Redirect("Error while Processing Request.","./");
		exit();
	}
	
	if(numRows($qry)>0)
	{
		$output .= '
		 <table border="1">  
            <tr>  
               <th>TripNo</th>  
               <th>TruckNo</th>  
			   <th>DriverName</th>  
			   <th>FromDate</th>  
			   <th>ToDate</th>  
			   <th>Amount</th>  
			   <th>Branch</th>  
			   <th>Date</th>  
			   <th>Narration</th>  
			</tr>';
			
		 while($row = fetchArray($qry))
		  {
		    $output .= '
			<tr>  
				<td>'.$row["trip_no"].'</td>  
				<td>'.$row["tno"].'</td>  
				<td>'.$row["name"].'</td>  
				<td>'.$row["from_date"].'</td>  
				<td>'.$row["to_date"].'</td>  
				<td>'.$row["amount"].'</td>  
				<td>'.$row["branch"].'</td>  
				<td>'.$row["date"].'</td>
			   <td>'.$row["narration"].'</td>
			</tr>
		   ';
		  }
		  $output .= '</table>';
		  header('Content-Type: application/xls');
		  header('Content-Disposition: attachment; filename=DA_BOOK.xls');
		  echo $output;
		  mysqli_close($conn);
	}
	else
	{
		echo "<script>
			alert('NO RESULT FOUND.');
			window.close();
		</script>";
		exit();
	}
}
else if($type=='SALARY')
{
	$qry=Qry($conn,"SELECT s.trip_no,s.tno,s.from_date,s.to_date,s.amount,s.branch,s.date,s.narration,s.narration2,d.name
	FROM dairy.salary as s 
	LEFT OUTER JOIN dairy.driver as d ON d.code=s.driver_code 
	WHERE s.date BETWEEN '$from_date' AND '$to_date' ORDER BY s.id ASC");
	
	if(!$qry)
	{
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		Redirect("Error while Processing Request.","./");
		exit();
	}
	
	if(numRows($qry)>0)
	{
		$output .= '
		 <table border="1">  
            <tr>  
               <th>TripNo</th>  
               <th>TruckNo</th>  
			   <th>DriverName</th>  
			   <th>FromDate</th>  
			   <th>ToDate</th>  
			   <th>Amount</th>  
			   <th>Branch</th>  
			   <th>Date</th>  
			   <th>Narration</th>  
			   <th>Narration2</th>  
			</tr>';
			
		 while($row = fetchArray($qry))
		  {
		    $output .= '
			<tr>  
				<td>'.$row["trip_no"].'</td>  
				<td>'.$row["tno"].'</td>  
				<td>'.$row["name"].'</td>  
				<td>'.$row["from_date"].'</td>  
				<td>'.$row["to_date"].'</td>  
				<td>'.$row["amount"].'</td>  
				<td>'.$row["branch"].'</td>  
				<td>'.$row["date"].'</td>
			   <td>'.$row["narration"].'</td>
			   <td>'.$row["narration2"].'</td>
			</tr>
		   ';
		  }
		  $output .= '</table>';
		  header('Content-Type: application/xls');
		  header('Content-Disposition: attachment; filename=SALARY_BOOK.xls');
		  echo $output;
		  mysqli_close($conn);
	}
	else
	{
		echo "<script>
			alert('NO RESULT FOUND.');
			window.close();
		</script>";
		exit();
	}
}
else if($type=='F1')
{
	$qry=Qry($conn,"SELECT f.trans_id,f.vou_id,f.tno,f.amount,f.date,f.narration,f.branch,f.narr_admin
	FROM dairy.freight_adv as f 
	WHERE f.date BETWEEN '$from_date' AND '$to_date' ORDER BY f.id ASC");
	
	if(!$qry)
	{
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		Redirect("Error while Processing Request.","./");
		exit();
	}
	
	if(numRows($qry)>0)
	{
		$output .= '
		 <table border="1">  
            <tr>  
               <th>TransId</th>  
               <th>VouId</th>  
			   <th>TruckNo</th>  
			   <th>Amount</th>  
			   <th>Branch</th>  
			   <th>Date</th>  
			   <th>Narration</th>  
			   <th>Narration2</th>  
			</tr>';
			
		 while($row = fetchArray($qry))
		  {
		    $output .= '
			<tr>  
				<td>'.$row["trans_id"].'</td>  
				<td>'.$row["vou_id"].'</td>  
				<td>'.$row["tno"].'</td>  
				<td>'.$row["amount"].'</td>  
				<td>'.$row["branch"].'</td>  
				<td>'.$row["date"].'</td>
			   <td>'.$row["narration"].'</td>
			   <td>'.$row["narr_admin"].'</td>
			</tr>
		   ';
		  }
		  $output .= '</table>';
		  header('Content-Type: application/xls');
		  header('Content-Disposition: attachment; filename=FREIGHT_TO_DRIVER.xls');
		  echo $output;
		  mysqli_close($conn);
	}
	else
	{
		echo "<script>
			alert('NO RESULT FOUND.');
			window.close();
		</script>";
		exit();
	}
}
else if($type=='F2')
{
	$qry=Qry($conn,"SELECT f.trans_id,f.vou_id,f.tno,f.amount,f.date,f.narration,f.branch
	FROM dairy.freight_rcvd as f 
	WHERE f.date BETWEEN '$from_date' AND '$to_date' ORDER BY f.id ASC");
	
	if(!$qry)
	{
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		Redirect("Error while Processing Request.","./");
		exit();
	}
	
	if(numRows($qry)>0)
	{
		$output .= '
		 <table border="1">  
            <tr>  
               <th>TransId</th>  
               <th>VouId</th>  
			   <th>TruckNo</th>  
			   <th>Amount</th>  
			   <th>Branch</th>  
			   <th>Date</th>  
			   <th>Narration</th>  
			</tr>';
			
		 while($row = fetchArray($qry))
		  {
		    $output .= '
			<tr>  
				<td>'.$row["trans_id"].'</td>  
				<td>'.$row["vou_id"].'</td>  
				<td>'.$row["tno"].'</td>  
				<td>'.$row["amount"].'</td>  
				<td>'.$row["branch"].'</td>  
				<td>'.$row["date"].'</td>
			   <td>'.$row["narration"].'</td>
			</tr>
		   ';
		  }
		  $output .= '</table>';
		  header('Content-Type: application/xls');
		  header('Content-Disposition: attachment; filename=FREIGHT_COLLECTED_AT_BRANCH.xls');
		  echo $output;
		  mysqli_close($conn);
	}
	else
	{
		echo "<script>
			alert('NO RESULT FOUND.');
			window.close();
		</script>";
		exit();
	}
}
else if($type=='RUNNING')
{
	$qry=Qry($conn,"SELECT tno,trip_no,date(date) as start_date FROM dairy.trip GROUP by tno ORDER BY date ASC");
	
	if(!$qry)
	{
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		Redirect("Error while Processing Request.","./");
		exit();
	}
	
	if(numRows($qry)>0)
	{
		$output .= '
		 <table border="1">  
            <tr>  
               <th>TruckNo</th>  
			   <th>TripNo</th>  
			   <th>StartDate</th>  
			   <th>No of days</th>  
			</tr>';
			
		 while($row = fetchArray($qry))
		  {
			  $datediff = strtotime(date("Y-m-d")) - strtotime($row['start_date']);
			$datediff = round($datediff / (60 * 60 * 24));
			
		    $output .= '
			<tr>  
				<td>'.$row["tno"].'</td>  
				<td>'.$row["trip_no"].'</td>  
				<td>'.$row["start_date"].'</td>
			   <td>'.$datediff.'</td>
			</tr>
		   ';
		  }
		  $output .= '</table>';
		  header('Content-Type: application/xls');
		  header('Content-Disposition: attachment; filename=RUNNING_DURATION.xls');
		  echo $output;
		  mysqli_close($conn);
	}
	else
	{
		echo "<script>
			alert('NO RESULT FOUND.');
			window.close();
		</script>";
		exit();
	}
}
else if($type=='KM')
{
	$qry=Qry($conn,"SELECT k.from_loc,k.to_loc,k.km,k.branch,k.approved,k.narration,k.timestamp
	FROM dairy.master_km as k");
	
	if(!$qry)
	{
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		Redirect("Error while Processing Request.","./");
		exit();
	}
	
	if(numRows($qry)>0)
	{
		$output .= '
		 <table border="1">  
            <tr>  
               <th>FromLocation</th>  
               <th>ToLocation</th>  
               <th>KMs</th>  
			   <th>AddedBy</th>  
			   <th>ApprovalStatus</th>  
			   <th>Narration</th>  
			   <th>TimeStamp</th>  
			</tr>';
			
		 while($row = fetchArray($qry))
		  {
			  
			 if($row['approved']==0)
		     {
				 $ap_status="<font color='red'><b>PENDING</b></font>";
			 }
			 else if($row['approved']==1)
			 {
				 $ap_status="<font color='blue'><b>ADMIN PENDING</b></font>";
			 }
			 else
			 {
				 $ap_status="<font color='green'><b>APPROVED</b></font>";
			 }
			  
		    $output .= '
			<tr>  
				<td>'.$row["from_loc"].'</td>  
				<td>'.$row["to_loc"].'</td>  
				<td>'.$row["km"].'</td>  
				<td>'.$row["branch"].'</td>  
				<td>'.$ap_status.'</td>
			   <td>'.$row["narration"].'</td>
			   <td>'.$row["timestamp"].'</td>
			</tr>
		   ';
		  }
		  $output .= '</table>';
		  header('Content-Type: application/xls');
		  header('Content-Disposition: attachment; filename=MASTER_LANE_KM_DATA.xls');
		  echo $output;
		  mysqli_close($conn);
	}
	else
	{
		echo "<script>
			alert('NO RESULT FOUND.');
			window.close();
		</script>";
		exit();
	}
}
else
{
	echo "<script>
		alert('Invalid Report Selected.');
		window.close();
	</script>";
	mysqli_close($conn); 
	exit();
}
?>