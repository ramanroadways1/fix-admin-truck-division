<?php
require_once 'connect.php';

$driver_code = escapeString($conn,($_POST['driver_code']));
$tno = escapeString($conn,($_POST['tno']));
$sold_tno = escapeString($conn,($_POST['sold_tno']));

$from_date = date('Y-m-d', strtotime(escapeString($conn,$_POST['duration']), strtotime(date("Y-m-d"))));
$to_date = date("Y-m-d");

if($driver_code!='')
{
	$sql = Qry($conn,"SELECT b.driver_code,b.tno,b.trip_id,b.trans_id,b.narration,b.trip_no,b.desct,b.credit,b.debit,b.balance,b.date,b.branch,
	d.name as driver_name 
	FROM dairy.driver_book AS b 
	LEFT OUTER JOIN dairy.driver AS d ON d.code=b.driver_code 
	WHERE b.date BETWEEN '$from_date' AND '$to_date' AND b.driver_code='$driver_code' ORDER BY b.id ASC,b.trip_no ASC");
}
else if($tno!='' || $sold_tno!='')
{
	if($sold_tno!=''){
		$tno = $sold_tno;
	}
	else{
		$tno = $tno;
	}

	$sql = Qry($conn,"SELECT b.driver_code,b.tno,b.trip_id,b.trans_id,b.narration,b.trip_no,b.desct,b.credit,b.debit,b.balance,b.date,b.branch,
	d.name as driver_name
	FROM dairy.driver_book AS b 
	LEFT OUTER JOIN dairy.driver AS d ON d.code=b.driver_code 
	WHERE b.date BETWEEN '$from_date' AND '$to_date' AND b.tno='$tno' ORDER BY b.id ASC,b.trip_no ASC");
}
else
{
	AlertRightCornerError("Invalid inputs !");
	exit();
}

if(!$sql){
	echo getMySQLError($conn);
	errorLog(getMySQLError($conn),$conn,$page_url,__LINE__);
	exit();
}
	
if(numRows($sql)==0)
{
	AlertRightCornerError("No record found !");
	exit();
}
	?>
	<table id="example" class="table table-bordered table-striped" style="font-size:13px;">
        <thead>
		<tr>
			<th>Vehicle_No</th>
			<th>Driver</th>
			<th>Trip_No</th>
			<th>Desct.</th>
			<th>Txn_Id</th>
			<th>Txn_Date</th>
			<th>Cr.</th>
			<th>Dr.</th>
			<th>Balance</th>
			<th>Branch</th>
			<th>Narration</th>
		</tr>
		</thead>
    <tbody id=""> 
	
<?php
while($row = fetchArray($sql))
{	
		$txn_date = date('d-m-y', strtotime($row['date']));
	
		echo "<tr>	
			<td>$row[tno]</td>
			<td>$row[driver_name]<br>($row[driver_code])</td>
			<td>$row[trip_no]</td>
			<td>$row[desct]</td>
			<td>$row[trans_id]</td>
			<td>$txn_date</td>
			<td>$row[credit]</td>
			<td>$row[debit]</td>
			<td>$row[balance]</td>
			<td>$row[branch]</td>
			<td>$row[narration]</td>
		</tr>";
}
	echo "</tbody>
</table>";
?>
	
<script> 
	$("#loadicon").fadeOut('slow');
</script> 
