<?php
include "header.php";

$chk = Qry($conn,"SELECT id FROM _access_control WHERE username='$_SESSION[ediary_fix_admin]' AND func_id=(SELECT id FROM 
_access_control_func_list WHERE session_role='8' AND func_name='Diesel_Qty') AND u_view='1'");
			  
if(numRows($chk)==0)
{
	echo "<script>window.location.href='./';</script>";
	exit();
}
?>
<script type="text/javascript">
$(document).ready(function (e) {
$("#FormUpdate").on('submit',(function(e) {
$("#loadicon").show();
$("#btn_save_qty").attr("disabled", true);
e.preventDefault();
	$.ajax({
	url: "./save_dsl_qty_update.php",
	type: "POST",
	data:  new FormData(this),
	contentType: false,
	cache: false,
	processData:false,
	success: function(data){
		$("#form_submit_result").html(data);
	},
	error: function() 
	{} });}));});
</script>

<div class="content-wrapper">
    <section class="content-header">
      <span style="font-size:15px">Update diesel Qty (Running Trips): </span>
	</section>
		
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box"> 
			<div class="box-body">

<div class="row">
<br />
<div class="form-group col-md-12">
   <div class="form-group col-md-12 table-responsive" id="load_table_div"></div>
</div>

  </div>
</div>

</body>
</html>

          </div>
        </div>
	</div>
</section>
</div>    
	
<form id="FormUpdate" autocomplete="off" style="font-size:13px"> 
	<div id="modal_load_div"></div>
</form>	
	
<script type="text/javascript">
function UpdateDieselQty(tno)
{
	$('#update_qty_btn_'+tno).attr('disabled',true);
	$("#loadicon").show();
	jQuery.ajax({
	url: "./modal_update_dsl_qty.php",
	data: 'tno=' + tno,
	type: "POST",
	success: function(data) {
		$("#modal_load_div").html(data);
	},
		error: function() {}
	});
}

function LoadTable()
{
	// $("#loadicon").show();
	jQuery.ajax({
	url: "_load_diesel_qty_update.php",
	data: 'ok=' + 'ok',
	type: "POST",
	success: function(data) {
		$("#load_table_div").html(data);
		$('#example').DataTable({ 
        "destroy": true, //use for reinitialize datatable
    });
	},
	error: function() {}
	});
}
</script>

<script>
LoadTable();
</script>

<div id="func_result2"></div>
<div id="form_submit_result"></div>

<?php
include "footer.php";
?>