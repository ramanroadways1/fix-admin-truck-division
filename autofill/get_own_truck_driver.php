<?php
include ('../connect.php');

if(isset($_REQUEST['term']))
{
	$search = escapeString($conn,$_REQUEST['term']);
	$query = $conn->query("SELECT id,name,code,last_verify,mobile,mobile2 FROM dairy.driver WHERE (name LIKE '%".$search."%' || code LIKE '%".$search."%' 
	|| mobile LIKE '%".$search."%') ORDER BY name ASC limit 6");

	while($row = $query->fetch_assoc()) 
	{ 
		 $data[] = array("value"=>$row['name'],"label"=>$row['name']." ($row[code]) - $row[mobile]","id"=>$row['id'],"code"=>$row['code'],"mobile"=>$row['mobile'],"mobile2"=>$row['mobile2'],"last_verify"=>$row['last_verify']);
	}
	echo json_encode($data);
	closeConnection($conn);
}
?>