<?php
require_once("../connect.php");

if(isset($_REQUEST['term']))
{
	$search = escapeString($conn,$_REQUEST['term']);
	$query = $conn->query("SELECT id,tno,comp as company,wheeler,model FROM dairy.own_truck WHERE is_sold='0' AND tno LIKE '%".$search."%' ORDER BY 
	tno ASC LIMIT 5");

	while($row = $query->fetch_assoc()) 
	{ 
		 $data[] = array("value"=>$row['tno'],"label"=>$row['tno']." ($row[company]) - $row[model], $row[wheeler]W","id"=>$row['id'],"company"=>$row['company'],"wheeler"=>$row['wheeler']);
	}
	echo json_encode($data);
	closeConnection($conn);
}
?>