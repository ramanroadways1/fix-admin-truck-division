<?php
include ('../connect.php');

if(isset($_REQUEST['term']) AND isset($_SESSION['driver_edit_code']))
{
	$search = escapeString($conn,$_REQUEST['term']);
	$query = $conn->query("SELECT id,driver_name,code,mobile FROM dairy.driver_guarantor WHERE (driver_name LIKE '%".$search."%' || code LIKE '%".$search."%' 
	|| mobile LIKE '%".$search."%') AND code!='$_SESSION[driver_edit_code]' ORDER BY driver_name ASC limit 6");

	while($row = $query->fetch_assoc()) 
	{ 
		 $data[] = array("value"=>$row['driver_name'],"label"=>$row['driver_name']." ($row[code]) - $row[mobile]","code"=>$row['code'],"mobile"=>$row['mobile']);
	}
	echo json_encode($data);
	closeConnection($conn);
}
?>