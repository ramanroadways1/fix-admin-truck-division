<?php
require_once("./function.php");

$id = escapeString($conn,($_POST['id']));
$amount = escapeString($conn,($_POST['amount']));
$narration = escapeString($conn,strtoupper($_POST['narration']));

$get_data = Qry($conn,"SELECT id,trip_id,trans_id,vou_id,tno,amount,date,branch,narration,timestamp FROM dairy.freight_rcvd WHERE id='$id'");

if(!$get_data)
{
	AlertErrorTopRight("Error while processing request !");
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	echo "<script> $('#f_coll_button_submit').attr('disabled',false); </script>";
	exit();
}

if(numRows($get_data) == 0)
{
	AlertErrorTopRight("Entry not found !");
	echo "<script> $('#f_coll_button_submit').attr('disabled',false); </script>";
	exit();
}

$row_db = fetchArray($get_data);

$table_id = $row_db['id'];
$tno = $row_db['tno'];
$trip_id = $row_db['trip_id'];
$trans_id_db = $row_db['trans_id'];
$amount_db = $row_db['amount'];
$vou_id = $row_db['vou_id'];
$narration_db = $row_db['narration'];

if($amount == $amount_db AND $narration == $narration_db)
{
	AlertErrorTopRight("Nothing to update !");
	echo "<script> $('#f_coll_button_submit').attr('disabled',false); </script>";
	exit();
}

require_once("./check_cache.php");

$check_trip = Qry($conn,"SELECT id FROM dairy.trip WHERE id='$trip_id'");

if(!$check_trip)
{
	AlertErrorTopRight("Error while processing request !");
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	echo "<script> $('#f_coll_button_submit').attr('disabled',false); </script>";
	exit();
}

if(numRows($check_trip)==0)
{
	AlertErrorTopRight("Trip not found !");
	echo "<script> $('#f_coll_button_submit').attr('disabled',false); </script>";
	exit();
}

$fetch_txn_db = Qry($conn,"SELECT id,driver_code,tno FROM dairy.driver_book WHERE trans_id='$trans_id_db'");

if(!$fetch_txn_db)
{
	AlertErrorTopRight("Error while processing request !");
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	echo "<script> $('#f_coll_button_submit').attr('disabled',false); </script>";
	exit();
}

if(numRows($fetch_txn_db)==0)
{
	AlertErrorTopRight("Txn not found in driver-book !");
	echo "<script> $('#f_coll_button_submit').attr('disabled',false); </script>";
	exit();
}

$row_txn_db fetchArray($fetch_txn_db);

$driver_book_id = $row_txn_db['id'];
$driver_code = $row_txn_db['driver_code'];

$driver_book_id_prev = $driver_book_id - 1;

$fetch_d_bal = Qry($conn,"SELECT id,tno FROM dairy.driver_up WHERE down=0 AND code='$driver_code' ORDER BY id DESC LIMIT 1");

if(!$fetch_d_bal)
{
	AlertErrorTopRight("Error while processing request !");
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	echo "<script> $('#f_coll_button_submit').attr('disabled',false); </script>";
	exit();
}

if(numRows($fetch_d_bal)==0)
{
	AlertErrorTopRight("Driver balance not found !");
	echo "<script> $('#f_coll_button_submit').attr('disabled',false); </script>";
	exit();
}

$row_d_bal = fetchArray($fetch_d_bal);

$driver_up_id = $row_d_bal['id'];

if($row_d_bal['tno']!=$tno)
{
	AlertErrorTopRight("Vehicle not verified !");
	echo "<script> $('#f_coll_button_submit').attr('disabled',false); </script>";
	exit();
}

if($amount != $amount_db)
{
	$amount_diff = $amount - $amount_db;
}
else
{
	$amount_diff = 0;
}

StartCommit($conn);
$flag = true;

// BRANCH CASHBOOK CODE STARTS

	$sel_cash_id = Qry($conn,"SELECT id,comp,user FROM cashbook WHERE vou_no='$trans_id_db'");
	
	if(!$sel_cash_id)
	{
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
	
	if(numRows($sel_cash_id)==0)
	{
		$flag = false;
		errorLog("Transaction not found in Cashbook. TransId: $trans_id_db.",$conn,$page_name,__LINE__);
	}
	
	$row_cash_id = fetchArray($sel_cash_id);
	
	$cash_id = $row_cash_id['id'];
	$company = $row_cash_id['comp'];
	$branch = $row_cash_id['user'];
	
	if($company=='RRPL')
	{
		$balance='balance';
		$credit_col='credit';
	}
	else if($company=='RAMAN_ROADWAYS')
	{
		$balance='balance2';
		$credit_col='credit2';
	}
	else
	{
		$flag = false;
		errorLog("Invalid Company Name : $company.",$conn,$page_name,__LINE__);
	}	
	
	$update_cashbook_entry = Qry($conn,"UPDATE cashbook SET `$credit_col`='$amount' WHERE id='$cash_id'");
	
	if(!$update_cashbook_entry)
	{
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
	
$update_cashbook = Qry($conn,"UPDATE cashbook SET `$balance`=(`$balance`+($amount_diff)) WHERE id>='$cash_id' AND comp='$company' AND user='$branch'");
	
	if(!$update_cashbook)
	{
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
	
	$query_update_balance = Qry($conn,"update user set `$balance`=(`$balance`+($amount_diff)) where username='$branch'");
	
	if(!$query_update_balance)
	{
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
	
	$update_cr_table=Qry($conn,"UPDATE credit SET amount='$amount' WHERE trans_id='$trans_id_db'");
	
	if(!$update_cr_table)
	{
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}

// BRANCH CASHBOOK CODE ENDS

	$update_driver_book = Qry($conn,"UPDATE dairy.driver_book SET debit='$amount' WHERE id='$driver_book_id'");

	if(!$update_driver_book)
	{
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}

$update_driver_book_next = Qry($conn,"UPDATE dairy.driver_book SET balance=(balance-($amount_diff)) WHERE id>'$driver_book_id_prev' AND tno='$tno'");

	if(!$update_driver_book_next)
	{
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}

	$update_driver_balance = Qry($conn,"UPDATE dairy.driver_up SET amount_hold=(amount_hold-($amount_diff)) WHERE id='$driver_up_id'");

	if(!$update_driver_balance)
	{
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}

$update_freight_entry = Qry($conn,"UPDATE dairy.freight_rcvd SET amount='$amount',narration='$narration' WHERE id='$table_id'");

if(!$update_freight_entry)
{
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

$update_trip = Qry($conn,"UPDATE dairy.trip SET freight_collected=(freight_collected+($amount_diff)) WHERE id='$trip_id'");

if(!$update_trip)
{
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}	

$log_data = "Previous Values: VouId=> $vou_no, Amount=> $amount_db, Narration: $narration_db. New Values: Amount=> $amount, Narration: $narration.";

$insert_log = Qry($conn,"INSERT INTO dairy.ediary_admin_log(code,action,desct,timestamp) VALUES ('$table_id','EDIT_FRT_COLLECTED','$log_data','$timestamp')");	
	
if(!$insert_log)
{
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

if($flag)
{
	MySQLCommit($conn);
	closeConnection($conn);
	
	AlertRightCornerSuccess("Updated Successfully !");
	echo "<script> $('#f_coll_button_submit').attr('disabled',false); </script>";
	exit();
}
else
{
	MySQLRollBack($conn);
	closeConnection($conn);
	
	AlertErrorTopRight("Error while processing request !");
	echo "<script> $('#f_coll_button_submit').attr('disabled',false); </script>";
	exit();
}
?>