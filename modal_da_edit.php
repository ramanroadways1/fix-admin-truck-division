<?php
require_once("connect.php");

$da_per_day = escapeString($conn,($_POST['da_per_day']));
$da_from_date = escapeString($conn,($_POST['da_from_date']));
$da_to_date = escapeString($conn,($_POST['da_to_date']));
$tno = escapeString($conn,($_POST['tno']));
$driver_code = escapeString($conn,($_POST['driver_code']));
$trip_no = escapeString($conn,($_POST['trip_no']));
$hisab_id = escapeString($conn,($_POST['hisab_id']));

$days_diff = round((strtotime($da_to_date)-strtotime($da_from_date)) / (60 * 60 * 24))+1;

$today_date = date("Y-m-d");

if(empty($hisab_id)){
	AlertRightCornerError("Hisab ID not found !");
	exit();
}

if(empty($trip_no)){
	AlertRightCornerError("Trip number not found !");
	exit();
}

if(empty($driver_code)){
	AlertRightCornerError("Driver not found !");
	exit();
}

if(empty($tno)){
	AlertRightCornerError("Vehicle number not found !");
	exit();
}

$first_record_da="NO";

if($da_per_day=="0" || $da_per_day=='')
{
	if($da_from_date!='')
	{
		AlertRightCornerError("Something went wrong !");
		exit();
	}

	$get_da_min_date = Qry($conn,"SELECT id,to_date,narration FROM dairy.da_book WHERE driver_code='$driver_code' ORDER BY id DESC LIMIT 1");

	if(!$get_da_min_date){
		AlertRightCornerError("Error while processing request !");
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		exit();
	}
	
	if(numRows($get_da_min_date)==0)
	{
		AlertRightCornerError("DA record not found !");
		exit();
	}
	else
	{
		$row_min_da_date = fetchArray($get_da_min_date);
	}
	
	if($row_min_da_date['narration']=='ON_LEAVE_UP')
	{
		$da_min_date = $row_min_da_date['to_date'];
	}
	else
	{
		$da_min_date = date('Y-m-d', strtotime('+1 day',strtotime($row_min_da_date['to_date'])));
	}
}
else
{
	$get_da_min_date = Qry($conn,"SELECT id,to_date,narration FROM dairy.da_book WHERE id<(SELECT id FROM dairy.da_book WHERE trip_no='$trip_no') 
	AND driver_code='$driver_code' ORDER BY id DESC LIMIT 1");

	if(!$get_da_min_date){
		AlertRightCornerError("Error while processing request !");
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		exit();
	}
	
	if(numRows($get_da_min_date)==0)
	{
		$get_da_min_date1 = Qry($conn,"SELECT from_date,narration FROM dairy.da_book WHERE trip_no='$trip_no' AND driver_code='$driver_code'");
 
		if(!$get_da_min_date1){
			AlertRightCornerError("Error while processing request !");
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			exit();
		}
		
		$row_min_da_date = fetchArray($get_da_min_date1);
		$da_min_date = $row_min_da_date['from_date'];
		
		$first_record_da="YES";
	}
	else
	{
		$row_min_da_date = fetchArray($get_da_min_date);
		
		if($row_min_da_date['narration']=='ON_LEAVE_UP')
		{
			$da_min_date = $row_min_da_date['to_date'];
		}
		else
		{
			$da_min_date = date('Y-m-d', strtotime('+1 day',strtotime($row_min_da_date['to_date'])));
		}
	}
}

// echo $driver_code."<br>";
// echo $trip_no."<br>";
// echo $da_per_day."<br>";

$get_max_trip_end_date = Qry($conn,"SELECT date(end_date) as end_date FROM dairy.trip_final WHERE trip_no='$trip_no' ORDER BY id DESC LIMIT 1");

if(!$get_max_trip_end_date){
	AlertRightCornerError("Error while processing request !");
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	exit();
}
	
if(numRows($get_max_trip_end_date)==0)
{
	AlertRightCornerError("Trip not found !");
	exit();
}

$row_trip_end_date = fetchArray($get_max_trip_end_date);

if($first_record_da == "YES")
{
	$get_da_date = Qry($conn,"SELECT to_date FROM dairy.da_book WHERE trip_no='$trip_no' AND driver_code='$driver_code'");
}
else
{
	$get_da_date = Qry($conn,"SELECT to_date FROM dairy.da_book WHERE id>'$row_min_da_date[id]' AND driver_code='$driver_code' ORDER BY id ASC LIMIT 1");
}

if(!$get_da_date){
	AlertRightCornerError("Error while processing request !");
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	exit();
}
	
if(numRows($get_da_date)==0)
{
	$da_max_date = $row_trip_end_date['end_date'];
}
else
{
	$row_da_date = fetchArray($get_da_date);
	
	if(strtotime($row_da_date['to_date']) > strtotime($row_trip_end_date['end_date']))
	{
		$da_max_date = date('Y-m-d', strtotime('-1 day',strtotime($row_da_date['to_date'])));
	}
	else
	{
		$da_max_date = $row_trip_end_date['end_date'];
	}
}

$span_header = "$tno. Trip_no: $trip_no";
?>
<div class="row">
		
<script>
function FromDateDA(fromdate)
{
	var nextDay = new Date(fromdate);
	nextDay.setDate(nextDay.getDate() + 0);
	var next_day=nextDay.toISOString().slice(0,10);
	var date_today = '<?php echo $today_date; ?>';
	$("#da_to_date").attr("min",nextDay.toISOString().slice(0,10));
	$("#da_to_date").val("");
}
</script>
		
		<div class="form-group col-md-6">
			<label>From DA date <font color="red">*</font></label>
			<input id="da_from_date" value="<?php echo $da_from_date; ?>" name="da_from_date" onchange="FromDateDA(this.value)" type="date" class="form-control" min="<?php echo $da_min_date; ?>" 
			required pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}" />
		</div>
		
		<div class="form-group col-md-6">
			<label>To DA date <font color="red">*</font></label>
			<input id="da_to_date" value="<?php echo $da_to_date; ?>" name="da_to_date" onchange="ToDateDA(this.value)" type="date" class="form-control" max="<?php echo $da_max_date; ?>" required 
			pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}" />
		</div>
		
		<?php
		if($da_per_day=="0" || $da_per_day=='')
		{
		?>
<script>
function ToDateDA(todate)
{
	if($('#da_from_date').val()=='')
	{
		Swal.fire({icon: 'warning',html: '<font size=\'2\' color=\'black\'>Select DA from date first !</font>',});
		$('#da_to_date').val('');
		$('#da_to_date').val('');
		$('#da_from_date').val('');
	}
	else
	{
		var dafrom = $('#da_from_date').val();
		var dato = todate;
		var diff =  Math.floor(( Date.parse(dato) - Date.parse(dafrom) ) / 86400000 +1); 
		$('#da_date_duration').val(diff);
		
		$("#da_amount_modal").val((Number($("#da_date_duration").val()) * Number($("#da_per_day").val())).toFixed(2));
	}	
}

function CalculateDA(amount)
{
	if($('#da_from_date').val()=='' || $('#da_to_date').val()=='')
	{
		Swal.fire({icon: 'warning',html: '<font size=\'2\' color=\'black\'>Select DA dates first !</font>',});
		$('#da_from_date').val('');
		$('#da_to_date').val('');
		$('#da_amount_modal').val('');
		$('#da_per_day').val('');
		$('#da_date_duration').val('');
	}
	else
	{
		if($('#da_from_date').val()<$('#da_to_date').val())
		{
			$('#da_from_date').val('');
			$('#da_to_date').val('');
			$('#da_amount_modal').val('');
			$('#da_per_day').val('');
			$('#da_date_duration').val('');
			Swal.fire({icon: 'warning',html: '<font size=\'2\' color=\'black\'>DA from-date is invalid !</font>',});
		}
		else
		{
			var diff =  Math.floor(( Date.parse($('#da_to_date').val()) - Date.parse($('#da_from_date').val()) ) / 86400000 +1); 
			$('#da_date_duration').val(diff);
		
			$('#da_from_date').attr('readonly',true);
			$('#da_to_date').attr('readonly',true);
			$("#da_amount_modal").val((Number($("#da_date_duration").val()) * Number($("#da_per_day").val())).toFixed(2));
		}	
	}	
}
</script>		
		<div class="form-group col-md-6">
			<label>DA per Day <font color="red">*</font></label>
			<select name="da_per_day" onchange="CalculateDA(this.value)" id="da_per_day" style="font-size:12px !important" class="form-control" required="required">
				<option style="font-size:12px !important" value="">--select an option--</option>
				<option style="font-size:12px !important" value="250">250</option>
				<option style="font-size:12px !important" value="300">300</option>
				<option style="font-size:12px !important" value="350">350</option>
			</select>
		</div>
		<?php		
		}
		else
		{
		?>
<script>
function ToDateDA(todate)
{
	if($('#da_from_date').val()=='')
	{
		Swal.fire({icon: 'warning',html: '<font size=\'2\' color=\'black\'>Select DA from date first !</font>',});
		$('#da_to_date').val('');
		$('#da_to_date').val('');
		$('#da_from_date').val('');
	}
	else
	{
		var dafrom = $('#da_from_date').val();
		var dato = todate;
		var diff =  Math.floor(( Date.parse(dato) - Date.parse(dafrom) ) / 86400000 +1); 
		$('#da_date_duration').val(diff);
		
		$("#da_amount_modal").val((Number($("#da_date_duration").val()) * Number($("#da_per_day").val())).toFixed(2));
	}	
}
</script>		
		<div class="form-group col-md-6">
			<label>DA per Day <font color="red">*</font></label>
			<input name="da_per_day" value="<?php echo $da_per_day; ?>" id="da_per_day" type="text" class="form-control" readonly required="required" />
		</div>
		<?php
		}
		?>
		
		<div class="form-group col-md-6">
			<label>DA Days <font color="red">*</font></label>
			<input name="da_date_duration" value="<?php echo $days_diff; ?>" id="da_date_duration" type="text" class="form-control" readonly required="required" />
		</div>
		
		<div class="form-group col-md-12">
			<label>DA Amount <font color="red">*</font></label>
			<input name="da_amount" value="<?php echo sprintf("%.2f",$days_diff*$da_per_day); ?>" id="da_amount_modal" type="text" class="form-control" readonly required="required" />
		</div>
		
		<input type="hidden" value="<?php echo $trip_no; ?>" name="trip_no">
		<input type="hidden" value="<?php echo $driver_code; ?>" name="driver_code">
		<input type="hidden" value="<?php echo $tno; ?>" name="tno">
		<input type="hidden" value="<?php echo $da_per_day; ?>" name="da_per_day_db">
		<input type="hidden" value="<?php echo $hisab_id; ?>" name="hisab_id">
		
		<div class="form-group col-md-12" id="result_modal"></div>
</div>

<script>
$('#modal_update_span_header').html('<?php echo $span_header; ?>');
$('#da_modal_btn')[0].click();	
$('#loadicon').fadeOut('slow');	
</script> 
