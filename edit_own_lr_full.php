<?php
require_once("connect.php");

$vou_no = escapeString($conn,($_POST['vou_no']));
$date_today = date("Y-m-d");

if(empty($vou_no))
{
	echo "Voucher number not found !";
	exit();
} 

$get_data = Qry($conn,"SELECT f.id,f.date,f.lrno,f.fstation,f.tstation,f.from_id,f.to_id,f.con1_id,f.con2_id,f.consignor,f.consignee,f.wt12,f.weight,
f.brk_id 
FROM freight_form_lr AS f 
LEFT OUTER JOIN lr_sample AS l ON l.id = f.mother_lr_id 
WHERE f.frno='$vou_no'");

if(!$get_data){
	AlertRightCornerError("Error while processing request !");
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	exit();
}
	
if(numRows($get_data)==0)
{
	echo "Voucher not found !";
	exit();
}

include("../diary/code_disabled_right_click.php");
?>	
<!doctype html>
<html>
<head>
	<meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate" />
	<meta http-equiv="Pragma" content="no-cache" />
	<meta http-equiv="Expires" content="0" />
	<meta name="robots" content="noindex,nofollow"/>
	<link rel="shortcut icon" type="image/png" href="favicon_2.png"/>
	<link rel="icon" type="image/png" href="<?php echo $diary_header; ?>logo_rrpl.png" />
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
	<title>Edit Voucher : <?php echo $vou_no; ?> </title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	
	<!-- SWEET ALERT-->
		<script src="../diary/sweet_alert_lib/dist/sweetalert2.min.js"></script>
		<link rel="stylesheet" href="../diary/sweet_alert_lib/dist/sweetalert2.min.css">
	<!-- END SWEET ALERT-->
	
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
    <script src="//code.jquery.com/jquery-1.10.2.js"></script>
    <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>  
	<!--<link rel="stylesheet" href="./rating/style.css">-->
	<meta name="google" content="notranslate">
	<link href="../diary/google_font.css" rel="stylesheet">
	<!-- Dynamic select box -->
	
</head>

<style>
@media screen and (min-width: 769px) {

    #logo_mobile { display: none; }
    #logo_desktop { display: block; }

}

@media screen and (max-width: 768px) {

    #logo_mobile { display: block; }
    #logo_desktop { display: none; }

}

@media (min-width: 768px) {
  .modal-xl-mini {
    width: 75%;
   max-width:100%;
  }
}

@media (min-width: 768px) {
  .modal-xl {
    width: 95%;
   max-width:100%;
  }
}

.modal { overflow: auto !important; } 

.selectpicker { width:auto; font-size: 12px !important;}

::-webkit-scrollbar{
    width:4px;
    height:4px;
}
::-webkit-scrollbar-track {
    -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.2); 
    border-radius: 5px;
}
::-webkit-scrollbar-thumb {
    border-radius: 5px;
    -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.9); 
}

table{
	font-family:Calibri !important;
}

.ui-autocomplete { z-index:2147483647;font-family: Verdana,Arial,sans-serif; font-size:10px !important;}

.filter-option {
    font-size: 10px !important;
}
</style>

<style type="text/css">
label{
	font-family: 'Open Sans', sans-serif !important;
	font-size:12.5px !important;
}
input[type='date'] { font-size: 12.5px !important;}
input[type='text'] { font-size: 12.5px !important;}
select { font-size: 12.5px !important; }
textarea { font-size: 12.5px !important; }
</style>

<div id="loadicon" style="position: fixed; right: 0px; top: 0px; width: 100%;height: 100%; background-color:#FFF; z-index: 30001; opacity:1; cursor: wait">
	<center><img style="margin-top:140px" src="./loading_truck1.gif" /><br><br><span style="letter-spacing:1px;font-weight:bold;font-size:14px">कृप्या प्रतीक्षा करे ..</span></center>
</div> 

<style>
label{
	font-size:11px !important;
}
input[type='date'] { font-size: 12px;}
input[type='text'] { font-size: 12px;}
input[type='number'] { font-size: 12px;}
select { font-size: 12px; }
textarea { font-size: 12px; }

::-webkit-scrollbar{
    width: 4px;
    height: 4px;
}
::-webkit-scrollbar-track {
    -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.2); 
    border-radius: 4px;
}
::-webkit-scrollbar-thumb {
    border-radius: 4px;
    -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.9); 
}
</style>

<form action="./edit_market_lr_full.php" id="ReloadPage" method="POST">
	<input type="hidden" value="<?php echo $vou_no; ?>" name="vou_no">		
</form>
			
<body style="font-family: 'Open Sans', sans-serif !important" onkeypress="return disableCtrlKeyCombination(event);" onkeydown = "return disableCtrlKeyCombination(event);">

<button style="margin:10px" class="btn btn-danger btn-sm" type="button"><i class="fa fa-window-close-o" aria-hidden="true"></i> Close window</button>
<br />
<br />
<div class="container-fluid">
	
	<div class="row">
		<div class="form-group col-md-12">
			<center>Edit Voucher : <?php echo $vou_no; ?></center>
		</div>
	</div>
	
<div class="row">
	&nbsp;
</div>	
			

				<?php
echo "<hr style='border:1px solid #666;border-style: dashed;'>";				
				// <tr>
							// <th>LR_No</th>
							// <th>LR_Date</th>
							// <th>From Location</th>
							// <th>To Location</th>
							// <th>Consignor & Consignee</th>
							// <th>Actual_Wt</th>
							// <th>Charge_Wt</th>
							// <th>#Update</th>
						// </tr>
				while($row = fetchArray($get_data))
				{
?>
<div class="row" style="font-size:12px !important">
<?php		
		
				$id2 = $row['id'];		

				echo "<script> 
				$(function() {
						$('#from_loc_$id2').autocomplete({ 
						source: '../b5aY6EZzK52NA8F/autofill/get_location.php',
						select: function (event, ui) { 
							$('#from_loc_$id2').val(ui.item.value);   
							$('#from_id_$id2').val(ui.item.id);      
							return false;
						},
						change: function (event, ui) {
						if(!ui.item){
							$(event.target).val('');
							$(event.target).focus();
							$('#from_loc_$id2').val('');   
							$('#from_id_$id2').val('');   
							alert('Location does not exists.');
						}}, 
					focus: function (event, ui){
					return false;
					}
				});});
				</script>";	

			echo "<script> 
				$(function() {
						$('#to_loc_$id2').autocomplete({ 
						source: '../b5aY6EZzK52NA8F/autofill/get_location.php',
						select: function (event, ui) { 
							$('#to_loc_$id2').val(ui.item.value);   
							$('#to_id_$id2').val(ui.item.id);      
							return false;
						},
						change: function (event, ui) {
						if(!ui.item){
							$(event.target).val('');
							$(event.target).focus();
							$('#to_loc_$id2').val('');   
							$('#to_id_$id2').val('');   
							alert('Location does not exists.');
						}}, 
					focus: function (event, ui){
					return false;
					}
				});});
				</script>";
			
			echo "<script> 
				$(function() {  
				  $('#consignee_$id2').autocomplete({
						source: function(request, response) { 
						 $.ajax({
							  url: '../b5aY6EZzK52NA8F/autofill/get_consignee.php',
							  type: 'post',
							  dataType: 'json',
							  data: {
							   search: request.term,
							  },
							  success: function( data ) {
								response( data );
							  }
							 });
						  },
						  select: function (event, ui) { 
						   $('#consignee_$id2').val(ui.item.value);   
						   $('#con2_id_$id2').val(ui.item.id);     
						   return false;
						  },
						  change: function (event, ui) {  
							if(!ui.item){
							$(event.target).val(''); 
							$(event.target).focus();
							$('#consignee_$id2').val('');
							$('#con2_id_$id2').val('');
							alert('Consignee does not exist !'); 
						  } 
						  },
						}); 
				});
				</script>";
				
			echo "<script> 
				$(function() {  
				  $('#consignor_$id2').autocomplete({
						source: function(request, response) { 
						 $.ajax({
							  url: '../b5aY6EZzK52NA8F/autofill/get_consignor.php',
							  type: 'post',
							  dataType: 'json',
							  data: {
							   search: request.term,
							  },
							  success: function( data ) {
								response( data );
							  }
							 });
						  },
						  select: function (event, ui) { 
						   $('#consignor_$id2').val(ui.item.value);   
						   $('#con1_id_$id2').val(ui.item.id);     
						   return false;
						  },
						  change: function (event, ui) {  
							if(!ui.item){
							$(event.target).val(''); 
							$(event.target).focus();
							$('#consignor_$id2').val('');
							$('#con1_id_$id2').val('');
							alert('Consignee does not exist !'); 
						  } 
						  },
						}); 
				});
				</script>
				
<input type='hidden' id='from_id_$row[id]' value='$row[from_id]'>
<input type='hidden' id='to_id_$row[id]' value='$row[to_id]'>
<input type='hidden' id='con1_id_$row[id]' value='$row[con1_id]'>
<input type='hidden' id='con2_id_$row[id]' value='$row[con2_id]'>
";	
				
echo "<div class='form-group col-md-1'>
	<label>LR no. <sup><font color='red'>*</font></sup></label>
	<input readonly class='form-control' style='width:100%;font-size:11px !important;height:23px !important' type='text' id='lrno_$row[id]' 
	value='$row[lrno]' />
	<br />
	<button id='update_lrno_btn_$row[id]' style='font-size:10px' class='btn btn-default btn-xs' onclick=UpdateLRnumber('$row[id]','$row[brk_id]')><i class='fa fa-pencil-square-o' 
	aria-hidden='true'></i> Update LR No.</button>
</div>

<div class='form-group col-md-2'>
	<label>LR Date <sup><font color='red'>*</font></sup></label>
	<input readonly class='form-control' max='$date_today' style='width:100%;font-size:11px !important;height:23px !important' type='date' 
	pattern='[0-9]{4}-[0-9]{2}-[0-9]{2}' id='lr_date_$row[id]' value='$row[date]' />
</div>

<div class='form-group col-md-2'>
	<label>From <sup><font color='red'>*</font></sup></label>
	<input readonly class='form-control' style='width:100%;font-size:11px !important;height:23px !important' type='text' 
	oninput='this.value=this.value.replace(/[^a-z -.A-Z]/,\"\")' id='from_loc_$row[id]' value='$row[fstation]' />
</div>

<div class='form-group col-md-2'>
	<label>To <sup><font color='red'>*</font></sup></label>
	<input readonly class='form-control' style='width:100%;font-size:11px !important;height:23px !important' type='text' 
	oninput='this.value=this.value.replace(/[^a-z -.A-Z]/,\"\")' id='to_loc_$row[id]' value='$row[tstation]' />
</div>

<div class='form-group col-md-3'>
	<div class='row'>
		<div class='form-group col-md-12'>
			<label>Consignor <sup><font color='red'>*</font></sup></label>
			<input readonly class='form-control' style='font-size:11px !important;height:23px !important' type='text' 
			oninput='this.value=this.value.replace(/[^a-z -.A-Z0-9]/,\"\")' id='consignor_$row[id]' value='$row[consignor]' />
		</div>
		
		<div class='form-group col-md-12'>
			<label>Consignee <sup><font color='red'>*</font></sup></label>
			<input readonly class='form-control' style='font-size:11px !important;height:23px !important' type='text' 
			oninput='this.value=this.value.replace(/[^a-z -.A-Z0-9]/,\"\")' id='consignee_$row[id]' value='$row[consignee]' />
		</div>
	</div>
</div>

<div class='form-group col-md-1'>
	<div class='row'>
		<div class='form-group col-md-12'>
			<label>ActWt. <sup><font color='red'>*</font></sup></label>
			<input class='form-control' style='width:100%;font-size:11px !important;height:23px !important' step='any' type='number' min='0.0001' 
			id='actual_wt_$row[id]' value='$row[wt12]' />
		</div>
		 
		<div class='form-group col-md-12'>
			<label>ChrgWt. <sup><font color='red'>*</font></sup></label>
			<input readonly class='form-control' style='width:100%;font-size:11px !important;height:23px !important' step='any' type='number' min='0.0001' 
			id='charge_wt_$row[id]' value='$row[weight]' />
		</div>
	</div>
</div>

<div class='form-group col-md-1'>
";
	if(!isMobile()){
		echo "<br>";
	}
echo "<button id='update_btn_$row[id]' class='btn btn-success btn-xs' onclick='Update($row[id])'><i class='fa fa-pencil-square-o' 
	aria-hidden='true'></i> Update</button>
</div>

</div>
<hr style='border:1px solid #666;border-style: dashed;'>
";
	}
				?>				
		
    </div>
</body>
</html>    

<div id="func_result"></div>
<div id="modal_div"></div>

<script>
function UpdateLRnumber(id,brk_id)
{
	if(brk_id!=0)
	{
		alert('Warning : Breaking LRs not allowed !');
	}
	else
	{
		$('#loadicon').show();
		jQuery.ajax({
			url: "edit_lrno_modal.php",
			data: 'id=' + id + '&lr_type=' + 'OWN',
			type: "POST",
			success: function(data) {
				$("#modal_div").html(data);
			},
			error: function() {}
		});
	}
}

function Update(id)
{
	$('#update_btn_'+id).attr('disabled',true);
	
	// var lrno = $('#lrno_'+id).val();
	// var lr_date = $('#lr_date_'+id).val();
	// var from_id = $('#from_id_'+id).val();
	// var to_id = $('#to_id_'+id).val();
	// var con1_id = $('#con1_id_'+id).val();
	// var con2_id = $('#con2_id_'+id).val();
	var actual_wt = $('#actual_wt_'+id).val();
	// var charge_wt = $('#charge_wt_'+id).val();
	// var consignor = $('#consignor_'+id).val();
	// var consignee = $('#consignee_'+id).val();
	// var from_loc = $('#from_loc_'+id).val();
	// var to_loc = $('#to_loc_'+id).val();
	
	$('#loadicon').show();
	jQuery.ajax({
		url: "save_edit_own_lr.php",
		// data: 'id=' + id + '&from_loc=' + from_loc + '&to_loc=' + to_loc + '&consignor=' + consignor + '&consignee=' + consignee + '&lrno=' + lrno + '&lr_date=' + lr_date + '&from_id=' + from_id + '&to_id=' + to_id + '&con1_id=' + con1_id + '&con2_id=' + con2_id + '&actual_wt=' + actual_wt + '&charge_wt=' + charge_wt,
		data: 'id=' + id + '&actual_wt=' + actual_wt,
		type: "POST",
		success: function(data) {
			$("#func_result").html(data);
		},
		error: function() {}
	});
}

$('#loadicon').fadeOut('slow');	
</script>