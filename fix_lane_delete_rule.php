<?php
require_once("connect.php");

$date=date("Y-m-d");
$timestamp=date("Y-m-d H:i:s");

$id = escapeString($conn,($_POST['id']));

$chk_rule = Qry($conn,"SELECT lane_id,tno,consignor,consignee,empty_loaded,truck_type,total_exp,timestamp FROM dairy.fix_lane_rules WHERE id='$id'");

if(!$chk_rule){
	AlertError("Error while processing request !");
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	exit();
}

if(numRows($chk_rule)==0)
{
	AlertError("Rule not found !");
	exit();
}

$row_lane = fetchArray($chk_rule);
$lane_id = $row_lane['lane_id'];
$lane_tno = $row_lane['tno'];
$lane_con1 = $row_lane['consignor'];
$lane_con2 = $row_lane['consignee'];
$lane_empty_loaded = $row_lane['empty_loaded'];
$lane_wheeler = $row_lane['truck_type'];
$lane_timestamp = $row_lane['timestamp'];

if($row_lane['total_exp']>0)
{
	AlertError("Delete expenses first !");
	echo "<script>$('#dlt_rule_btn_$id').attr('disabled',false);</script>";
	exit();
}

$delete_log = "Lane id: $lane_id. Vehicle_no: $lane_tno, Consignor: $lane_con1, Consignee: $lane_con2, Empty_loaded: $lane_empty_loaded, Wheeler: $lane_wheeler, Timestamp: $lane_timestamp";

StartCommit($conn);
$flag = true;

$delete_rule= Qry($conn,"DELETE FROM dairy.fix_lane_rules WHERE id='$id'");

if(!$delete_rule){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

$update_rule_count = Qry($conn,"UPDATE dairy.fix_lane SET total_rules=total_rules-1 WHERE id='$lane_id'");

if(!$update_rule_count){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

$insert_log = Qry($conn,"INSERT INTO dairy.ediary_admin_log(code,action,desct,timestamp) VALUES ('$lane_id','Fix_Rule_Deleted','$delete_log','$timestamp')");

if(!$insert_log){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

if($flag)
{
	MySQLCommit($conn);
	closeConnection($conn);
	
	AlertRightCornerSuccess("Rule deleted successfully !");
	
	echo "<script>
		Number($('#total_rule_count_$lane_id').html());
		var new_exp_count = Number($('#total_rule_count_$lane_id').html())-1;
		$('#total_rule_count_$lane_id').html(new_exp_count);
		LoadAllRule('$lane_id');
	</script>";
}
else
{
	MySQLRollBack($conn);
	closeConnection($conn);
	
	AlertError("Error while processing request !");
	exit();
}
?>