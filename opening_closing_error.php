<?php include("header.php"); ?>

<?php
// $chk = Qry($conn,"SELECT id FROM _access_control WHERE username='$_SESSION[ediary_fix_admin]' AND func_id=(SELECT id FROM 
// _access_control_func_list WHERE session_role='8' AND func_name='Edit_Driver') AND u_view='1'");
			  
// if(numRows($chk)==0)
// {
	// echo "<script>window.location.href='./';</script>";
	// exit();
// }
?>

<div class="content-wrapper">
      <section class="content-header">
          <h1 style="font-size:16px;">Validate Driver's Opening Closing: </h1>
       </section>
       
	   <section class="content">
          <div class="row">
            <div class="col-xs-12">
			<div class="box">
                <div class="box-body">
				<div class="col-md-12">&nbsp;</div>
<?php
// $chk_update = Qry($conn,"SELECT id FROM _access_control WHERE username='$_SESSION[ediary_fix_admin]' AND func_id=(SELECT id FROM 
// _access_control_func_list WHERE session_role='8' AND func_name='Edit_Driver') AND u_update='1'");
			  
// if(numRows($chk_update)>0)
// {
?>	

<script>
$(function() {
		$("#driver_name").autocomplete({
		source: 'autofill/get_own_truck_driver.php',
		// appendTo: "#LaneRuleModal",
		select: function (event, ui) { 
              $('#driver_name').val(ui.item.value);   
              // $('#driver_name2').val(ui.item.value);   
              $('#driver_id').val(ui.item.id);   
              $('#driver_code').val(ui.item.code);   
             return false;
		},
		change: function (event, ui) {
		if(!ui.item){
		    $(event.target).val("");
			Swal.fire({icon: 'warning',html: '<font size=\'2\' color=\'black\'>Driver does not exists !</font>',});
			$("#driver_name").val('');
			// $("#driver_name2").val('');
			$("#driver_id").val('');
			$("#driver_code").val('');
			$("#driver_name").focus();
		}}, 
		focus: function (event, ui){
			return false;}
});});
</script>
				<div class="col-md-8">
					
					<div class="row">
						 
						<div class="form-group col-md-6">
							<label>Search Driver <font color="red"><sup>*</sup></font></label>
							<input required autocomplete="off" type="text" class="form-control" name="driver_name" id="driver_name" />
						</div>
						
						<input type="hidden" name="driver_id" id="driver_id">
						<input type="hidden" name="driver_code" id="driver_code">
						
						<div class="form-group col-md-6">
							<?php if(!isMobile()) { echo "<label>&nbsp;</label><br />"; } ?>
							<button type="button" onclick="SearchDriver()" class="btn btn-sm btn-success <?php if(isMobile()) { echo "btn-block"; } ?>" id="search_btn"><i class="fa fa-search" aria-hidden="true"></i> &nbsp; Validate Opening Closing</button>
						</div>
					</div>
				
					<div class="row">
						<div class="form-group col-md-12" id="func_result"></div>
					</div>
					
				</div>
			
			<?php
// }
?>			
	
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

<script type="text/javascript">
function SearchDriver()
{
	if(confirm("Are you sure ?")==true)
	{
		var driver_id = $('#driver_id').val();
		var driver_code = $('#driver_code').val();
		var driver_name = $('#driver_name').val();
		
		if(driver_id=='' || driver_code=='' || driver_name=='')
		{
			Swal.fire({icon: 'warning',html: '<font size=\'2\' color=\'black\'>Select driver first !</font>',});
		}
		else
		{
			$('#search_btn').attr('disabled',true);
			$("#loadicon").show();
			jQuery.ajax({
				url: "opening_closing_error_save.php",
				data: 'driver_id=' + driver_id + '&driver_code=' + driver_code + '&driver_name=' + driver_name,
				type: "POST",
				success: function(data) {
				$("#func_result").html(data);
			},
			error: function() {}
			});
		}
	}
}	
</script>
 
<?php include("footer.php") ?>