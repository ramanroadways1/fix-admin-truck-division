<?php
include("header.php");
?>
<div class="content-wrapper">
      <section class="content-header">
          <h1 style="font-size:16px;">Adv/Exp Limit Violation : Fix Lane </h1>
       </section>
       
	   <section class="content">
          <div class="row">
            <div class="col-xs-12">
			<div class="box">
                <div class="box-body">
			
				<div class="col-md-12 table-responsive" id="load_table">
              
				 </div> 
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

<div id="func_result"></div>  
<div id="modal_res1"></div>  

<script>	
function LoadData()
{
	$('#loadicon').show();
			jQuery.ajax({
				url: "_load_fix_lane_adv_exp_alert.php",
				data: 'ok=' + 'ok',
				type: "POST",
				success: function(data) {
					$("#load_table").html(data);
					$('#example').DataTable({ 
					"lengthMenu": [ [10, 100, 500, -1], [10, 100,500, "All"] ], 
						"destroy": true, //use for reinitialize datatable
					});
				},
				error: function() {}
		});
}

LoadData();
</script>

<?php include("footer.php") ?>
