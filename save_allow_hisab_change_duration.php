<?php
require_once("./connect.php");

$trip_days = escapeString($conn,strtoupper($_POST['trip_days']));

if(empty($trip_days))
{
	AlertError("Enter lock over days first !");
	echo "<script> $('#add_btn').attr('disabled',false); </script>";
	exit();
}

$update = Qry($conn,"UPDATE dairy._trip_lock_func SET no_of_days='$trip_days'");

if(!$update){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	AlertError("Error While Processing Request !");
	exit();
}

if(AffectedRows($conn) == 0)
{
	AlertErrorTopRight("No change !");
	echo '<script>$("#add_btn").attr("disabled", false);</script>';
	exit();
}

AlertRightCornerSuccessFadeFast("Updated Successfully !");
echo '<script>$("#add_btn").attr("disabled", false);</script>';
?>