<?php
require_once("connect.php");

$date=date("Y-m-d");
$timestamp=date("Y-m-d H:i:s");

$id = escapeString($conn,($_POST['id']));
$action = escapeString($conn,($_POST['action']));

echo "<script>$('#btn_id_$id').attr('disabled',true);</script>"; 

$chk_lane = Qry($conn,"SELECT is_active FROM dairy.fix_lane WHERE id='$id'");

if(!$chk_lane){
	AlertError("Error while processing request !");
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	exit();
}

if(numRows($chk_lane)==0)
{
	AlertError("Lane not found !");
	exit();
}

$row_chk_lane = fetchArray($chk_lane);

if($row_chk_lane['is_active']==$action)
{
	AlertError("Lane already active/disabled !");
	exit();
}

StartCommit($conn);
$flag = true;

$update_lane_status = Qry($conn,"UPDATE dairy.fix_lane SET is_active='$action' WHERE id='$id'");

if(!$update_lane_status){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}	

if($action=='1'){
	$action_text="Activated";
	$is_active_td="Active";
}
else{
	$action_text="Disabled";
	$is_active_td="Inactive";
}

if($flag)
{
	MySQLCommit($conn);
	closeConnection($conn);
	
	if($action=="1")
	{
		echo "<script>
			$('#btn_id_$id').attr('style','btn btn-sm btn-success');
			$('#btn_id_$id').html('Activated');
			$('#is_active_td_$id').html('Active');
			$('#is_active_td_$id').attr('style','color:green');
			$('#loadicon').hide();
		</script>"; 
	}
	else
	{
		echo "<script>
			$('#btn_id_$id').attr('style','btn btn-sm btn-danger');
			$('#btn_id_$id').html('Disabled');
			$('#is_active_td_$id').html('Inactive');
			$('#is_active_td_$id').attr('style','color:red');
			$('#loadicon').hide();
		</script>"; 
	}
	
	exit();
}
else
{
	MySQLRollBack($conn);
	closeConnection($conn);
	AlertError("Error while processing request !");
	exit();
}	
?>