<?php
require_once("connect.php");

?>
	<table id="example" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th>#</th>
                        <th>Expense</th>
                        <th>Exp_Code</th>
                        <th>Supervisor Entry</th>
                        <th>Timestamp</th>
                      </tr>
                    </thead>
                    <tbody>
	<?php
	$get_roles = Qry($conn,"SELECT * FROM dairy.exp_head ORDER BY name ASC");
	
	if(numRows($get_roles)==0)
	{
		echo "<tr>.
			<td colspan='5'>No record found !</td>
			<td style='display: none'></td>
			<td style='display: none'></td>
			<td style='display: none'></td>
			<td style='display: none'></td>
			<td style='display: none'></td>
		</tr>";
	}
	else
	{
		$i=1;
		while($row = fetchArray($get_roles))
		{
			$timestamp = date("d-m-y h:i A",strtotime($row['timestamp']));
			
			if($row['fix_entry_to_supervisor']=="1"){
				$role_supervisor = "checked='checked'";
			}
			else{
				$role_supervisor = "";
			}
		?>
		<script>			
		$(document).on('click','#supervisor_chk_<?php echo $row["id"]; ?>',function(){
		var ckbox = $('#supervisor_chk_<?php echo $row["id"]; ?>');
			
			if (ckbox.is(':checked'))
			{
				$('#loadicon').show();
				jQuery.ajax({
					url: "update_exp_visibility_fix.php",
					data: 'id=' + '<?php echo $row["id"]; ?>' + '&value=' +  '1',
					type: "POST",
					success: function(data) {
					$("#func_result2").html(data);
					},
					error: function() {}
				});
			}
			else
			{
				$('#loadicon').show();
				jQuery.ajax({
					url: "update_exp_visibility_fix.php",
					data: 'id=' + '<?php echo $row["id"]; ?>' + '&value=' +  '0',
					type: "POST",
					success: function(data) {
					$("#func_result2").html(data);
					},
					error: function() {}
				});
			}
		});
		</script>	
			<?php
			echo "<tr>
				<td>$i</td>
				<td>$row[name]</td>
				<td>$row[exp_code]</td>
				<td><input type='checkbox' id='supervisor_chk_$row[id]' $role_supervisor /></td>
				<td>$timestamp</td>
			</tr>";
		$i++;	
		}
	}
	?>	
                    </tbody>
                  </table>
				  
<script> 
$("#loadicon").fadeOut('slow');
$(document).ready(function() {
    $('#example').DataTable();
} );
</script> 			  