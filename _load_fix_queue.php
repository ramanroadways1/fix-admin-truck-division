<?php
require_once 'connect.php';

$sql = Qry($conn,"SELECT f.id,f.tno,f.lr_type,f.branch,f.timestamp,t.from_station,t.to_station,d.name as driver_name 
FROM dairy.fix_txn_pending AS f 
LEFT OUTER JOIN dairy.trip AS t ON t.id = f.trip_id 
LEFT OUTER JOIN dairy.driver AS d ON d.code = t.driver_code");

if(!$sql){
	echo getMySQLError($conn);
	errorLog(getMySQLError($conn),$conn,$page_url,__LINE__);
	exit();
}

?>
	<table id="example" class="table table-bordered table-striped" style="font-size:13px;">
        <thead>
		<tr>
			<th>#</th>
			<th>Vehicle_no</th>
			<th>Branch</th>
			<th>Trip</th>
			<th>Driver</th>
			<th>LR_Type</th>
			<th>Timestamp</th>
			<th>#</th>
		</tr>
		</thead>
       <tbody id=""> 
<?php
if(numRows($sql)==0)
{
	echo "<tr>
		<td colspan='9'>No record found !</td>
	</tr>";  
}
else
{
	$sn=1;
	while($row = fetchArray($sql))
	{
		$timestamp = date("d/m/y H:i A",strtotime($row['timestamp']));
		
		echo "<tr>
			<td>$sn</td>
			<td>$row[tno]</td>
			<td>$row[branch]</td>
			<td>$row[from_station] to $row[to_station]</td>
			<td>$row[driver_name]</td>
			<td>$row[lr_type]</td>
			<td>$timestamp</td>
			<td><button onclick='FixEntry($row[id])' type='button' id='fix_entry_btn_$row[id]' class='btn btn_fix_queue_proceed btn-xs btn-success'>Proceed</button></td>
		</tr>";
		$sn++;		
     }
}
?>
	</tbody>
		</table>
<script> 
// $("#loadicon").fadeOut('slow');
$(document).ready(function() {
    $('#example').DataTable( {
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ]
    } );
} );
</script>

<?php
$qry_rights_insert_update = Qry($conn,"SELECT id FROM _access_control WHERE username='$_SESSION[ediary_fix_admin]' AND func_id=(SELECT id FROM 
_access_control_func_list WHERE session_role='8' AND func_name='Fix_Queue') AND u_update='1' AND u_insert='1'");
							  
if(numRows($qry_rights_insert_update)==0)
{
	echo "<script> 
		$('.btn_fix_queue_proceed').hide();
		$('.btn_fix_queue_proceed').attr('onclick','');
	</script>";
}	
?>
