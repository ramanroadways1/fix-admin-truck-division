<?php
require_once("connect.php");
?>

			<table id="example" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th>#</th>
                        <th>Trip_No</th>
                        <th>Vehicle_No</th>
                        <th>Narration</th>
                        <th>Timestamp</th>
                      </tr>
                    </thead>
                    <tbody>
	<?php
	$get_approvals = Qry($conn,"SELECT id,tno,trip_no,narration,timestamp FROM dairy.driver_standby_approval");
	
	if(!$get_approvals){
		AlertErrorTopRight("Error while processing request !");
		echo getMySQLError($conn);
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		exit();
	}
	
	if(numRows($get_approvals)==0)
	{
		echo "<tr>
			<td colspan='5'>No record found !</td>
			<td style='display: none'></td>
			<td style='display: none'></td>
			<td style='display: none'></td>
			<td style='display: none'></td>
			<td style='display: none'></td>
		</tr>";
	}
	else
	{
		$i=1;
		while($row = fetchArray($get_approvals))
		{
			$timestamp = date("d-m-y h:i A",strtotime($row['timestamp']));
			
			echo "<tr>
				<td>$i</td>
				<td>$row[trip_no]</td>
				<td>$row[tno]</td>
				<td>$row[narration]</td>
				<td>$timestamp</td>
			</tr>";
		$i++;	
		}
	}
	?>	
                    </tbody>
                  </table>

<script>				  
$("#loadicon").fadeOut('slow');
</script>				  