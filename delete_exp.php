<?php
require_once("./connect.php");

$id = escapeString($conn,($_POST['id']));

$get_data = Qry($conn,"SELECT trip_id,trans_id,exp_name,exp_code,tno,copy,amount,date,branch,timestamp FROM dairy.trip_exp WHERE id='$id'");

if(!$get_data)
{
	AlertErrorTopRight("Error while processing request !");
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	echo "<script> $('#dlt_exp_btn_$id').attr('disabled',false); </script>";
	exit();
}

if(numRows($get_data) == 0)
{
	AlertErrorTopRight("Entry not found !");
	echo "<script> $('#dlt_exp_btn_$id').attr('disabled',false); </script>";
	exit();
}

$row = fetchArray($get_data);

$tno = $row['tno'];
$trip_id = $row['trip_id'];
$trans_id = $row['trans_id'];
$amount = $row['amount'];
$exp_name = $row['exp_name'];
$exp_code = $row['exp_code'];
$trans_date = $row['date'];

require_once("./check_cache.php");

if($exp_code == 'TR00015')
{
	$toll_tax_amt = $amount;
}
else
{
	$toll_tax_amt = 0;
}

$chk_trip = Qry($conn,"SELECT id FROM dairy.trip WHERE id='$trip_id'");

if(!$chk_trip)
{
	AlertErrorTopRight("Error while processing request !");
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	echo "<script> $('#dlt_exp_btn_$id').attr('disabled',false); </script>";
	exit();
}

if(numRows($chk_trip)==0)
{
	AlertErrorTopRight("Running trip not found !");
	echo "<script> $('#dlt_exp_btn_$id').attr('disabled',false); </script>";
	exit();
}

StartCommit($conn);
$flag = true;

$log_data = "ExpName : $exp_name, Trans_date : $trans_date, TripId : $trip_id, TruckNo : $tno, Amount : $amount, Trans_id: $trans_id.";

$insert_log = Qry($conn,"INSERT INTO dairy.ediary_admin_log(code,action,desct,timestamp) VALUES ('$tno','EXP_DELETE','$log_data','$timestamp')");

if(!$insert_log){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}
	
$select_book_id = Qry($conn,"SELECT id,driver_code,debit FROM dairy.driver_book WHERE trans_id='$trans_id'");
	
if(!$select_book_id){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}
	
if(numRows($select_book_id)==0)
{
	$flag = false;
	errorLog("Txn not found in driver book. Trans_id: $trans_id.",$conn,$page_name,__LINE__);
}

if(numRows($select_book_id)>1)
{
	$flag = false;
	errorLog("Multiple txn found in driver-book. Trans_id: $trans_id.",$conn,$page_name,__LINE__);
}
	
$row_book_id = fetchArray($select_book_id);
	
$book_id = $row_book_id['id'];
$driver_code = $row_book_id['driver_code'];

if($row_book_id['debit'] != $amount)
{
	$flag = false;
	errorLog("Expense Amount not verified.",$conn,$page_name,__LINE__);
}
	
$update_book = Qry($conn,"UPDATE dairy.driver_book SET balance=balance+'$amount' WHERE id>'$book_id' AND tno='$tno'");
	
if(!$update_book){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}
	
$delete_row = Qry($conn,"DELETE FROM dairy.driver_book WHERE id='$book_id'");
		
if(!$delete_row){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);	
}
	
if(AffectedRows($conn)==0)
{
	$flag = false;
	errorLog("Transaction not found in driver book. Trans_id: $trans_id. DriverBook Id: $book_id.",$conn,$page_name,__LINE__);
}
		
$update_d_bal = Qry($conn,"UPDATE dairy.driver_up SET amount_hold=amount_hold+'$amount' WHERE down=0 AND code='$driver_code' ORDER BY id DESC LIMIT 1");
	
if(!$update_d_bal){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);	
}
			
$update_trip = Qry($conn,"UPDATE dairy.trip SET expense=expense-'$amount',toll_tax=toll_tax-'$toll_tax_amt' WHERE id='$trip_id'");
			
if(!$update_trip){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);	
}

$delete = Qry($conn,"DELETE FROM dairy.trip_exp WHERE id='$id'");

if(!$delete)
{
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}
		
if($flag)
{
	MySQLCommit($conn);
	closeConnection($conn);
	
	AlertRightCornerSuccess("Deleted Successfully !");
		echo "<script>
			ReLoadPage();
		</script>";
		exit();
}
else
{
	MySQLRollBack($conn);
	closeConnection($conn);
	AlertErrorTopRight("Error while processing Request !");
	echo "<script> $('#dlt_exp_btn_$id').attr('disabled',false); </script>";
	exit();
}
?>