<?php
require_once 'connect.php';

$date = date("Y-m-d");
$timestamp = date("Y-m-d H:i:s");

$title=escapeString($conn,strtoupper(trim($_POST['title'])));
$exp_name=escapeString($conn,strtoupper(trim($_POST['exp_name'])));
$exp_code=escapeString($conn,(trim($_POST['exp_code'])));
// $role=escapeString($conn,(trim($_POST['role'])));
$lock_on_empty=escapeString($conn,(trim($_POST['lock_on_empty'])));

if(empty($title) || empty($exp_name) || empty($exp_code) || empty($lock_on_empty))
{
	AlertErrorTopRight("Fill all fields first !");
	echo "<script>$('#add_btn').attr('disabled', false);</script>";
	exit();
}

$check_code = Qry($conn,"SELECT id FROM dairy.exp_head WHERE exp_code='$exp_code'");

if(!$check_code){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	AlertErrorTopRight("Error while processing request !");
	echo "<script>$('#add_btn').attr('disabled', false);</script>";
	exit();
}

if(numRows($check_code)>0)
{
	AlertErrorTopRight("Duplicate expense code !");
	echo "<script>$('#add_btn').attr('disabled', false);</script>";
	exit();
}


$check_name = Qry($conn,"SELECT id FROM dairy.exp_head WHERE name='$exp_name'");

if(!$check_name){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	AlertErrorTopRight("Error while processing request !");
	echo "<script>$('#add_btn').attr('disabled', false);</script>";
	exit();
}

if(numRows($check_name)>0)
{
	AlertErrorTopRight("Duplicate expense name !");
	echo "<script>$('#add_btn').attr('disabled', false);</script>";
	exit();
}

StartCommit($conn); 
$flag = true;

$insert_exp = Qry($conn,"INSERT INTO dairy.exp_head(exp_code,sys_name,name,lock_on_empty,timestamp) VALUES 
('$exp_code','$exp_name','$title','$lock_on_empty','$timestamp')");

if(!$insert_exp){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

if($flag)
{
	MySQLCommit($conn);
	closeConnection($conn);
	AlertRightCornerSuccess("Expense successfully added !");
	echo "<script>LoadTable();</script>";
	exit();
}
else
{
	MySQLRollBack($conn);
	closeConnection($conn);
	AlertErrorTopRight("Error while processing request !");
	echo "<script>$('#add_btn').attr('disabled', false);</script>";
	exit();
}	
?>