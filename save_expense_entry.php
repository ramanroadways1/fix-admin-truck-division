<?php
require_once("./connect.php");

$timestamp = date("Y-m-d H:i:s"); 

$trip_id=escapeString($conn,strtoupper($_POST['id']));

if(empty($trip_id) || $trip_id=='' || $trip_id==0)
{
	AlertRightCornerError("Trip not found..");
	echo "<script>$('#entry_save_btn').attr('disabled',false);</script>";
	exit();
}

$check_trip = Qry($conn,"SELECT t.id,t.tno,t.driver_code,t.trip_no,t.lr_type,t.loaded_hisab,d.name as driver_name 
FROM dairy.trip AS t 
LEFT OUTER JOIN dairy.driver AS d ON d.code=t.driver_code 
WHERE t.id='$trip_id'");

if(!$check_trip){
	AlertRightCornerError("Error..");
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	echo "<script>$('#entry_save_btn').attr('disabled',false);</script>";
	exit();
}

if(numRows($check_trip)==0)
{
	AlertRightCornerError("Trip not found..");
	errorLog("Running trip not found. TripId: $trip_id.",$conn,$page_name,__LINE__);
	echo "<script>$('#entry_save_btn').attr('disabled',false);</script>";
	exit();
}

$row_trip = fetchArray($check_trip);

$tno = $row_trip['tno'];

$check_fix_lane = Qry($conn,"SELECT id FROM dairy.trip WHERE id='$trip_id' AND fix_lane='1'");

if(!$check_fix_lane){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	AlertRightCornerError("Error..");
	exit();
}

if(numRows($check_fix_lane)>0)
{
	AlertRightCornerError("Trip belongs to fix lane.");
	echo "<script>$('#entry_save_btn').attr('disabled',false);</script>";
	exit();
}

$check_cache = Qry($conn,"SELECT id FROM dairy.hisab_cache WHERE tno='$tno'");

if(!$check_cache){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	AlertRightCornerError("Error..");
	exit();
}

if(numRows($check_cache)>0)
{
	AlertRightCornerError("Vehicle Hisab is in process. Please complete or reset hisab first !");
	echo "<script>$('#entry_save_btn').attr('disabled',false);</script>";
	exit();
}

$check_cache_trip = Qry($conn,"SELECT id FROM dairy.trip_cache WHERE tno='$tno'");

if(!$check_cache_trip){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	AlertRightCornerError("Error..");
	exit();
}

if(numRows($check_cache_trip)>0)
{
	AlertRightCornerError("Please wait ! Try again after some time !");
	echo "<script>$('#entry_save_btn').attr('disabled',false);</script>";
	exit();
}

$check_running_trip = Qry($conn,"SELECT id FROM dairy.running_scripts WHERE file_name!='LOAD_API_TRANS'");

if(!$check_running_trip){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	AlertRightCornerError("Error..");
	exit();
}

if(numRows($check_running_trip)>0)
{
	AlertRightCornerError("Please wait ! Try again after some time !");
	echo "<script>$('#entry_save_btn').attr('disabled',false);</script>";
	exit();
}

$amount = escapeString($conn,$_POST['amount']);
$date = escapeString($conn,$_POST['exp_date']);
$exp = escapeString($conn,($_POST['exp']));
$branch = escapeString($conn,strtoupper($_POST['branch']));
$exp_code2 = escapeString($conn,strtoupper($_POST['exp_code']));
$exp_name2 = escapeString($conn,strtoupper($_POST['exp_name']));

$exp_code1 = explode("_",$exp)[0];
$exp_name1 = explode("_",$exp)[1];

if($exp_code1!=$exp_code2)
{
	AlertRightCornerError("Expense not verified..");
	echo "<script>$('#entry_save_btn').attr('disabled',false);</script>";
	exit();
}

if($exp_name2!=$exp_name1)
{
	AlertRightCornerError("Expense not verified..");
	echo "<script>$('#entry_save_btn').attr('disabled',false);</script>";
	exit();
}

$exp_code = $exp_code1;
$exp = $exp_name1;

$chk_trishul_card = Qry($conn,"SELECT trishul_card FROM dairy.own_truck WHERE tno='$tno'");

if(!$chk_trishul_card){
	AlertRightCornerError("Error..");
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	echo "<script>$('#entry_save_btn').attr('disabled',false);</script>";
	exit();
}

$row_trishul_card = fetchArray($chk_trishul_card);

// if($row_trishul_card['trishul_card']=="1" AND $exp_code=="TR00877")
// {
	// AlertRightCornerError("Expense: Other-allowance not allowed in Trishul-Card enabled vehicles !!");
	// echo "<script>$('#entry_save_btn').attr('disabled',false);</script>";
	// exit();
// }

$chk_exp_lock = Qry($conn,"SELECT lock_on_empty FROM dairy.exp_head WHERE exp_code='$exp_code'");

if(!$chk_exp_lock){
	AlertRightCornerError("Error..");
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	echo "<script>$('#entry_save_btn').attr('disabled',false);</script>";
	exit();
}

if(numRows($chk_exp_lock)==0)
{
	AlertRightCornerError("Invalid Expense !!");
	echo "<script>$('#entry_save_btn').attr('disabled',false);</script>";
	exit();
}

$row_code = fetchArray($chk_exp_lock);
$empty_lock = escapeString($conn,($row_code['lock_on_empty']));
$narration = escapeString($conn,($_POST['narration']));

$driver_name = $row_trip['driver_name'];
$driver_code = $row_trip['driver_code'];
$trip_no = $row_trip['trip_no'];

// if($row_trip['lr_type']=='EMPTY' AND $empty_lock=='1')
// {
	// if($tno!='GJ12BV3924' AND $tno!='GJ18AZ3641' AND $tno!='GJ18AU7730')
	// {
		// AlertRightCornerError("Expense: $exp not allowed iin EMPTY trip !!");
		// echo "<script>$('#entry_save_btn').attr('disabled',false);</script>";
		// exit();
	// }
// }

// if($row_trip['loaded_hisab']=='1' AND ($exp_code=='TR00725' || $exp_code=='TR00009'))
// {
	// AlertRightCornerError("Expense: $exp not allowed in loaded vehicle hisab !!");
	// echo "<script>$('#entry_save_btn').attr('disabled',false);</script>";
	// exit();
// }

// $chk_exp_limit = Qry($conn,"SELECT exp_name,amount FROM dairy.trip_exp_limit WHERE exp='$exp_code' AND type='1' AND $amount>`amount`");

// if(!$chk_exp_limit){
	// AlertRightCornerError("Error..");
	// errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	// echo "<script>$('#entry_save_btn').attr('disabled',false);</script>";
	// exit();
// }

// if(numRows($chk_exp_limit)>0)
// {
	// $row_exp_limit = fetchArray($chk_exp_limit);
	// AlertRightCornerError("Max limit of expense: $row_exp_limit[exp_name] is: $row_exp_limit[amount] !!");
	// echo "<script>$('#entry_save_btn').attr('disabled',false);</script>";
	// exit();
// }

// $can_entry_times=Qry($conn,"SELECT exp_name,entry_limit FROM dairy.trip_exp_limit WHERE exp='$exp_code' AND type='2'");

// if(!$can_entry_times){
	// AlertRightCornerError("Error..");
	// errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	// echo "<script>$('#entry_save_btn').attr('disabled',false);</script>";
	// exit();
// }

// if(numRows($can_entry_times)>0)
// {
	// $row_times=fetchArray($can_entry_times);
	
	// $fetch_total_entry_of_exp=Qry($conn,"SELECT id FROM dairy.trip_exp WHERE trip_id='$trip_id' AND exp_code='$exp_code'");
	
	// if(!$fetch_total_entry_of_exp){
		// AlertRightCornerError("Error..");
		// errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		// echo "<script>$('#entry_save_btn').attr('disabled',false);</script>";
		// exit();
	// }

	// if(numRows($fetch_total_entry_of_exp)>=$row_times['entry_limit'])
	// {
		// AlertRightCornerError("Max limit of expense: $row_times[exp_name] entry is: $row_times[entry_limit] times !!");
		// echo "<script>$('#entry_save_btn').attr('disabled',false);</script>";
		// exit();
	// }
// }

if($exp=='RTO')
{
	if(count($_FILES['rto_upload']['name']) <= 0)
	{
		AlertRightCornerError("Please upload RTO expense attachment !!");
		echo "<script>$('#entry_save_btn').attr('disabled',false);</script>";
		exit();
	}
	
	$valid_types = array("image/jpg", "image/jpeg", "image/bmp", "image/gif","image/png");
	
	foreach($_FILES['rto_upload']['type'] as $file_types)
	{
		if(!in_array($file_types, $valid_types))
		{
			AlertRightCornerError("Only image upload allowed !!");
			echo "<script>$('#entry_save_btn').attr('disabled',false);</script>";
			exit();
		}
	}
	
	for($i=0; $i<count($_FILES['rto_upload']['name']); $i++)
	{
		$sourcePath = $_FILES['rto_upload']['tmp_name'][$i];

        if(empty($sourcePath))
		{
			AlertRightCornerError("Please upload RTO expense attachment !!");
			echo "<script>$('#entry_save_btn').attr('disabled',false);</script>";
			exit();
		}
		
		$fix_name=$trip_id."_".$tno.date('dmYHis').$i;	
        $shortname = "rto_receipt/".$fix_name.".".pathinfo($_FILES['rto_upload']['name'][$i],PATHINFO_EXTENSION);
		$targetPath = "../diary/rto_receipt/".$fix_name.".".pathinfo($_FILES['rto_upload']['name'][$i],PATHINFO_EXTENSION);

		if($_FILES['rto_upload']['type'][$i]!='application/pdf')
		{		
			ImageUpload(1000,1000,$_FILES['rto_upload']['tmp_name'][$i]);
		}
			
		if(!move_uploaded_file($sourcePath, $targetPath))
		{
			AlertRightCornerError("Error while uploading files !!");
			echo "<script>$('#entry_save_btn').attr('disabled',false);</script>";
			exit();
        }
			
		$files[] = $shortname;
		
    }
		
	$file_name=implode(',',$files);
}

if($exp=='RTO' AND empty($file_name))
{
	AlertRightCornerError("RTO upload files not found !!");
	echo "<script>$('#entry_save_btn').attr('disabled',false);</script>";
	exit();
}
else if($exp!='RTO')
{
	$file_name='';
}

$trans_id_Qry = GetTxnId_eDiary($conn,"EXP");
	
if(!$trans_id_Qry || $trans_id_Qry=="" || $trans_id_Qry=="0"){
	AlertRightCornerError("Error !!");
	echo "<script>$('#entry_save_btn').attr('disabled',false);</script>";
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	exit();
}
	
$trans_id=$trans_id_Qry;
		
StartCommit($conn);
$flag = true;
	
$insert_exp = Qry($conn,"INSERT INTO dairy.trip_exp (trip_id,trans_id,tno,exp_name,exp_code,amount,copy,date,narration,branch,branch_user,
timestamp) VALUES ('$trip_id','$trans_id','$tno','$exp','$exp_code','$amount','$file_name','$date','$narration','$branch',
'$_SESSION[ediary_fix_admin]','$timestamp')");

if(!$insert_exp){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}	

if($exp_code=='TR00015'){
	$toll_tax = $amount;
}else{
	$toll_tax = 0;
}

$update_trip = Qry($conn,"UPDATE dairy.trip SET expense=expense+'$amount',toll_tax=toll_tax+'$toll_tax' WHERE id='$trip_id'");

if(!$update_trip){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}	

$nrr = $tno."/".$driver_name." : ".$narration;

$select_amount = Qry($conn,"SELECT id,amount_hold FROM dairy.driver_up WHERE down=0 AND code='$driver_code' ORDER BY id DESC LIMIT 1");

if(!$select_amount){	
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

if(numRows($select_amount)==0){
	$flag = false;
	errorLog("Error While Fetching Driver Balance. Code: $driver_code.",$conn,$page_name,__LINE__);
}

$row_amount = fetchArray($select_amount);
$hold_amt=$row_amount['amount_hold']-$amount;
$driver_id=$row_amount['id'];

$insert_driver_book = Qry($conn,"INSERT INTO dairy.driver_book (driver_code,tno,trip_id,trip_no,trans_id,desct,debit,balance,date,branch,
timestamp) VALUES ('$driver_code','$tno','$trip_id','$trip_no','$trans_id','EXP-$exp','$amount','$hold_amt','$date','$branch',
'$timestamp')");

if(!$insert_driver_book){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

$update_driver_balance = Qry($conn,"UPDATE dairy.driver_up SET amount_hold=amount_hold-$amount WHERE id='$driver_id'");

if(!$update_driver_balance){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

if($flag)
{
	MySQLCommit($conn);
	closeConnection($conn);
	AlertRightCornerSuccess("Expense Entry Success !");
	echo "<script>$('#ExpForm')[0].reset();$('#modal_close_btn')[0].click();$('#add_btn')[0].click();$('#entry_save_btn').attr('disabled',false);</script>";
	exit();
	
}
else
{
	if($exp=='RTO'){
		foreach(explode(",",$file_name) as $uploaded_files){
			unlink($uploaded_files);
		}
	}
		
	MySQLRollBack($conn);
	closeConnection($conn);
	AlertRightCornerError("Error while processing request !!");
	echo "<script>$('#entry_save_btn').attr('disabled',false);</script>";
	exit();
}
?>