<?php
require_once("connect.php");

$model = escapeString($conn,$_POST['model']);
$lane_id = escapeString($conn,$_POST['lane_id']);
?>
<button type="button" style="display:none" id="modal_btn_load_modal_veh" data-toggle="modal" data-target="#ModalViewModel"></button>

<script type="text/javascript">
function ToggleVegicleModel(tno,action)
{
	$("#loadicon").show();
		jQuery.ajax({
			url: "toggle_switch_model.php",
			data: 'tno=' + tno + '&action=' + action + '&model=' + '<?php echo $model; ?>',
			type: "POST",
			success: function(data) {
			$("#result_div_model_view").html(data);
		},
		error: function() {}
	});
}
</script>

<div class="modal fade" id="ModalViewModel" style="background:#DDD" role="dialog" data-backdrop="static" data-keyboard="false">
   <div class="modal-dialog modal-lg">
      <div class="modal-content" style="">
	  
	<div class="modal-header bg-primary">
		Vehicle List of Model : <?php echo $model; ?>
    </div>
	  
	<div class="modal-body">
	
	<div class="row">
	
	<div class="form-group col-md-12 table-responsive">
<?php
$get_model_vehicles = Qry($conn,"SELECT o.tno,o.comp as company,e.id as esp_id FROM dairy.own_truck AS o 
LEFT OUTER JOIN dairy.fix_lane_model_escap AS e ON e.tno = o.tno 
WHERE o.model='$model' ORDER BY o.tno ASC");
			  
if(numRows($get_model_vehicles)>0)
{
?>			<table id="example2" class="table table-bordered table-striped" style="font-size:13px">
				 <thead>	
				<tr>
						<th>#SN</th>
						<th>Vehicle_No</th>
						<th>Company</th>
						<th>Status &nbsp; <button class="btn btn-xs btn-danger" onclick="DeselectAllModelVehicle('<?php echo $model; ?>')">Deselect ALL</button></th>
					</tr>
				 </thead>
 <tbody id=""> 				 
				<?php
			$sn22=1;
			while($row_mv = fetchArray($get_model_vehicles))
			{
				echo "<tr>
					<td>$sn22</td>
					<td>$row_mv[tno]</td>
					<td>$row_mv[company]</td>";
				echo "<td>
				";
?>
<script>			
$(document).on('click','#check_box_veh_<?php echo $row_mv["tno"]; ?>',function(){
var ckbox = $('#check_box_veh_<?php echo $row_mv["tno"]; ?>');
	if (ckbox.is(':checked')) {
		ToggleVegicleModel('<?php echo $row_mv["tno"]; ?>','DELETE');
	}else {
		ToggleVegicleModel('<?php echo $row_mv["tno"]; ?>','INSERT');
	}
});
</script>
<?php				
					if($row_mv['esp_id']=="")
					{
						echo "<input type='checkbox' class='model_checkbox' checked='checked' id='check_box_veh_$row_mv[tno]' />";
					}
					else
					{
						echo "<input type='checkbox' class='model_checkbox' id='check_box_veh_$row_mv[tno]' />";
					}
				echo "</td></tr>";
				$sn22++;	
			}
			?>
 </tbody> 			
		</table>	
	<?php
}
else
{
	echo '<span style="color:red"><center>No record found..</center></span>';
}
?>
		</div>

		<div class="form-group col-md-12" id="result_div_model_view"></div>
		
		</div> 
        <div class="modal-footer">
			<button type="button" class="btn btn-danger btn-sm" onclick="LoadAllRule('<?php echo $lane_id; ?>')" data-dismiss="modal">Close</button>
		</div>
  </div>
 </div>
</div>

<script>
function DeselectAllModelVehicle(model)
{
	$('#loadicon').show();
	jQuery.ajax({
		url: "deselect_all_vehicles_of_model.php",
		data: 'model=' + model,
		type: "POST",
		success: function(data) {
			$("#result_div_model_view").html(data);
		},
		error: function() {}
	});
}

$(document).ready(function() {
    $('#example2').DataTable( {
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ]
    } );
} );

	$('#modal_btn_load_modal_veh')[0].click();
	$('#loadicon').fadeOut('slow');
</script>