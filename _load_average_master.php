<?php
require_once 'connect.php';

$sql = Qry($conn,"SELECT a.veh_no,a.wheeler,a.max_capacity,a.route_type,a.empty_avg,a.normal_avg,a.overload_avg,o.diesel_tank_cap 
FROM dairy._avg_data AS a 
LEFT OUTER JOIN dairy.own_truck AS o ON o.tno = a.veh_no 
ORDER BY a.veh_no,a.id ASC");

if(!$sql){
	echo getMySQLError($conn);
	errorLog(getMySQLError($conn),$conn,$page_url,__LINE__);
	exit();
}

?>
	<table id="example" class="table table-bordered table-striped" style="font-size:13.5px;">
        <thead>
		<tr>
			<th>#</th>
			<th>Vehicle_no</th>
			<th>Wheeler</th>
			<th>Load_capacity</th>
			<th>Route_type</th>
			<th>Empty_avg.</th>
			<th>Loaded_avg.</th>
			<th>Overload_avg.</th>
		</tr>
		</thead>
       <tbody id=""> 
<?php
if(numRows($sql)==0)
{
	echo "<tr>
		<td colspan='9'>No record found !</td>
	</tr>";  
}
else
{
	$sn=1;
	while($row = fetchArray($sql))
	{
		if($row['route_type']!='normal'){
			$route_type = "<font color='red'>Congested/Ghaat</font>";
		} else {
			$route_type = "Normal";
		}
		echo "<tr>
			<td>$sn</td>
			<td>$row[veh_no]</td>
			<td>$row[wheeler]</td>
			<td>$row[max_capacity]</td>
			<td>$route_type</td>
			<td>$row[empty_avg]</td>
			<td>$row[normal_avg]</td>
			<td>$row[overload_avg]</td>
		</tr>";
		$sn++;		
     }
}
?>
	</tbody>
		</table>
<script> 
$("#loadicon").fadeOut('slow');
$(document).ready(function() {
    $('#example').DataTable( {
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ]
    } );
} );
</script> 