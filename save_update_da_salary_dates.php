<?php
require_once("./connect.php");

$date = date("Y-m-d");

$tno = escapeString($conn,strtoupper($_POST['truck_no']));
$driver_code = escapeString($conn,strtoupper($_POST['driver_code']));
$da_found = escapeString($conn,strtoupper($_POST['da_found']));
$salary_found = escapeString($conn,strtoupper($_POST['salary_found']));
$da_date = escapeString($conn,strtoupper($_POST['da_date']));
$salary_date = escapeString($conn,strtoupper($_POST['salary_date']));

$da_date_db = escapeString($conn,strtoupper($_POST['da_date_db']));
$sal_date_db = escapeString($conn,strtoupper($_POST['sal_date_db']));

if($tno=='' || $driver_code=='' || $da_date=='' || $salary_date=='' || $da_date_db=='' || $sal_date_db=='')
{
	AlertErrorTopRight("Fill all fields first !");
	exit();
}

if($da_date != $da_date_db)
{
	$da_update="YES";
}
else
{
	$da_update="NO";
}

if($salary_date != $sal_date_db)
{
	$sal_update="YES";
}
else
{
	$sal_update="NO";
}

if($sal_update=="NO" AND $da_update=="NO")
{
	AlertErrorTopRight("NOTHING TO UPDATE !");
	exit();
}

StartCommit($conn);
$flag = true;

if($sal_update=='YES')
{
	if($salary_found=='YES')
	{
		$update_sal = Qry($conn,"UPDATE dairy.salary SET to_date='$salary_date' WHERE driver_code='$driver_code' ORDER BY id DESC LIMIT 1");
	
		$log_data = "SALARY DATE UPDATED TO $salary_date. Old Date was : $sal_date_db.";
	}
	else if($salary_found=='NO')
	{
		$update_sal = Qry($conn,"UPDATE dairy.driver SET last_salary='$salary_date' WHERE code='$driver_code'");
		
		$log_data = "LAST SALARY DATE UPDATED TO $salary_date. Old Date was : $sal_date_db.";
	}
	else
	{
		$update_sal = Qry($conn,"INSERT INTO dairy.salary(driver_code,to_date,branch,date,narration,timestamp) VALUES 
		('$driver_code','$salary_date','ADMIN','$date','SALARY_START_ADMIN','$timestamp')");
		
		$log_data = "NEW SALARY DATE INSERTED $salary_date";
	}
	
	if(!$update_sal)
	{
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
}

if($da_update=='YES')
{
	if($da_found=='YES')
	{
		$update_da = Qry($conn,"UPDATE dairy.da_book SET to_date='$da_date' WHERE driver_code='$driver_code' ORDER BY id DESC LIMIT 1");
	
		$log_data = "DA DATE UPDATED TO $da_date. Old Date was : $da_date_db.";
	}
	else
	{
		$update_da = Qry($conn,"INSERT INTO dairy.da_book(driver_code,to_date,branch,date,narration,timestamp) VALUES 
		('$driver_code','$da_date','ADMIN','$date','DA_START_ADMIN','$timestamp')");
		
		$log_data = "DA DATE INSERTED $da_date.";
	}
	
	if(!$update_da)
	{
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
}

$insert_log = Qry($conn,"INSERT INTO dairy.ediary_admin_log(code,action,desct,timestamp) VALUES ('$driver_code','DA_SAL_DATE_UPDATE','$log_data',
'$timestamp')");
	
if(!$insert_log)
{
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}
	
if($flag)
{
	MySQLCommit($conn);
	closeConnection($conn);
	AlertRightCornerSuccess("Update Success !");
	echo "<script>
		$('#submit_btn').hide();
		$('#submit_btn').attr('disabled',true);
		
		$('#search_btn').attr('disabled',false);
		$('#search_btn').show();;
	
		document.getElementById('search_btn').click();
	</script>";	
	exit();
}
else
{
	MySQLRollBack($conn);
	closeConnection($conn);
	
	AlertErrorTopRight("Error while processing request !");
	echo "<script> $('#submit_btn').attr('disabled',false); </script>";
	exit();
}
?>