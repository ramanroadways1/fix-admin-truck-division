<button type="button" style="display:none" id="lane_exp_modal" data-toggle="modal" data-target="#LaneExpModal"></button>

<script type="text/javascript">
$(document).ready(function (e) {
$("#ExpForm2").on('submit',(function(e) {
$("#exp_submit_button").attr("disabled", true);
$("#loadicon").show();
e.preventDefault();
	$.ajax({
	url: "./fix_lane_add_new_expense.php",
	type: "POST",
	data:  new FormData(this),
	contentType: false,
	cache: false,
	processData:false,
	success: function(data){
		$("#modal_form_result_exp").html(data);
	},
	error: function() 
	{} });}));});
	
function DeleteExp(id,rule_id,empty_loaded)
{
	$("#loadicon").show();
		jQuery.ajax({
			url: "fix_lane_exp_delete.php",
			data: 'id=' + id + '&empty_loaded=' + empty_loaded + '&rule_id=' + rule_id,
			type: "POST",
			success: function(data) {
			$("#modal_form_result_exp").html(data);
		},
		error: function() {}
	});
}

function UpdateExpSave(id,rule_id,empty_loaded)
{
	var expense = $('#exp_name_edit_new_'+id).val();
	var exp_type = $('#exp_type_edit_new_'+id).val();
	var exp_amount = $('#new_expense_value_'+id).val();
	
	$('#update_exp_fix_btn_'+id).attr('disabled',true);
	$("#loadicon").show();
		jQuery.ajax({
			url: "fix_lane_exp_update.php",
			data: 'id=' + id + '&empty_loaded=' + empty_loaded + '&rule_id=' + rule_id + '&expense=' + expense + '&exp_type=' + exp_type + '&exp_amount=' + exp_amount,
			type: "POST",
			success: function(data) {
			$("#modal_form_result_exp").html(data);
		},
		error: function() {}
	});
}
</script>

<form id="ExpForm2" autocomplete="off">	

<div class="modal fade" id="LaneExpModal" style="background:#DDD" role="dialog" data-backdrop="static" data-keyboard="false">
   <div class="modal-dialog modal-xl-mini">
      <div class="modal-content" style="">
	  
	<div class="modal-header bg-primary">
		Fix Lane Expense :
    </div>
	  
	<div class="modal-body">

		<div class="row">
		
			<div class="form-group col-md-3">
				<label>Expense <sup><font color="red">*</font></sup> </label>
				<select style="font-size:12px !important" id="exp_list" data-live-search="true" data-size="6" class="form-control selectpicker" name="exp_list" required>
				<option style="font-size:12px !important" data-tokens="" value="">--select expense--</option>
				<?php
				$fetch_exps = Qry($conn,"SELECT exp_code,name FROM dairy.exp_head ORDER BY name ASC");
				
				if(!$fetch_exps){
					errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
					exit();
				}
				
				if(numRows($fetch_exps)>0)
				{
					while($row_exps=fetchArray($fetch_exps))
					{
						echo "<option style='font-size:12px !important' data-tokens='$row_exps[name]' value='$row_exps[exp_code]_$row_exps[name]'>$row_exps[name]</option>";
					}
				}
				?>
				</select>
			</div>
			
			<div class="form-group col-md-2">
				<label>Exp. Type <sup><font color="red">*</font></sup></label> 
				<select style="font-size:12px !important" onchange="ExpTypeSel(this.value)" id="exp_type" class="form-control" name="exp_type" required>
					<option style="font-size:12px !important" value="">--select type--</option>
					<option style="font-size:12px !important" value="Fix_Amount">Fix Amount</option>
					<option style="font-size:12px !important" value="On_Weight">On Weight</option>
				</select>
			</div>
			
			<div id="fix_lane_exp_modal_row_data">
			</div>
			
			<script>
			function ExpTypeSel(elem)
			{
				if(elem=='On_Weight'){
					$('#exp_label').show();
				}
				else{
					$('#exp_label').hide();
				}
			}
			</script>
			
			<div class="form-group col-md-2">
				<label>Amount <sup><font color="red"><span style="display:none" id="exp_label">per MT</span> *</font></sup> </label>
				<input style="font-size:12px !important" type="number" min="1" id="exp_amount" name="exp_amount" class="form-control" required />
			</div>
			
			<div class="form-group col-md-2">
				<label>&nbsp;</label>
				<br />
				<button type="submit" id="exp_submit_button" name="exp_submit_button" class="btn btn-sm btn-success">Add Expense</button>
			</div>
			
		</div>
	
	<input type="hidden" id="exp_modal_rule_id" name="exp_modal_rule_id">
	<input type="hidden" id="exp_modal_empty_loaded" name="exp_modal_empty_loaded">
			
	<div class="row"><div class="form-group col-md-12 table-responsive" style="overflow:auto" id="exp_modal_load_exp"></div></div>
		
		<div id="modal_form_result_exp"></div>
		
        </div> 
        <div class="modal-footer">
			<button type="button" class="btn btn-danger btn-sm" id="hide_exp_modal" data-dismiss="modal">Close</button>
		</div>
  </div>
 </div>
</div>
</form>