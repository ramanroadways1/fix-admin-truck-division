<?php include("header.php"); ?>

<?php
$chk = Qry($conn,"SELECT id FROM _access_control WHERE username='$_SESSION[ediary_fix_admin]' AND func_id=(SELECT id FROM 
_access_control_func_list WHERE session_role='8' AND func_name='Modify_DA_SALARY') AND u_view='1'");
			  
if(numRows($chk)==0)
{
	echo "<script>window.location.href='./';</script>";
	exit();
}
?>

<div class="content-wrapper">
      <section class="content-header">
          <h1 style="font-size:16px;">Update DA/SALARY Dates : </h1>
       </section>
       
	   <section class="content">
          <div class="row">
            <div class="col-xs-12">
			<div class="box">
                <div class="box-body">
				<div class="col-md-12">&nbsp;</div>
<?php
$chk_update = Qry($conn,"SELECT id FROM _access_control WHERE username='$_SESSION[ediary_fix_admin]' AND func_id=(SELECT id FROM 
_access_control_func_list WHERE session_role='8' AND func_name='Modify_DA_SALARY') AND u_update='1'");
			  
if(numRows($chk_update)>0)
{
?>	

<script>
$(function() {
		$("#veh_no").autocomplete({
		source: 'autofill/get_tno.php',
		// appendTo: "#LaneRuleModal",
		select: function (event, ui) { 
              $('#veh_no').val(ui.item.value);   
             return false;
		},
		change: function (event, ui) {
		if(!ui.item){
		    $(event.target).val("");
			Swal.fire({icon: 'warning',html: '<font size=\'2\' color=\'black\'>Vehicle does not exists !</font>',});
			$("#veh_no").val('');
			$("#veh_no").focus();
		}}, 
		focus: function (event, ui){
			return false;}
});});
</script>
		
<form action="#" method="POST" id="Form1" autocomplete="off" style="font-size:12px !important">
			
				<div class="col-md-8">
					
					<div class="row">
						 
						<div class="form-group col-md-5">
							<label>Vehicle Number <font color="red"><sup>*</sup></font></label>
							<input style="font-size:12px !important" required autocomplete="off" oninput="this.value=this.value.replace(/[^A-Z a-z0-9]/,'')" type="text" class="form-control" name="veh_no" id="veh_no" />
						</div>
						
						<div class="form-group col-md-3">
							<?php if(!isMobile()) { echo "<label>&nbsp;</label><br />"; } ?>
							<button type="button" onclick="SearchDaSalary()" class="btn btn-sm btn-success <?php if(isMobile()) { echo "btn-block"; } ?>" id="search_btn"><i class="fa fa-search" aria-hidden="true"></i> &nbsp; Search</button>
						</div>
					</div>
					
				<div class="row" id="driver_result_div" style="display:none">	
				
						<div class="form-group col-md-4">
							<label>Driver Name <font color="red"><sup>*</sup></font></label>
							<input style="font-size:12px !important" readonly required autocomplete="off" type="text" class="form-control" name="driver_name" id="driver_name" />
						</div>
						
						<div class="form-group col-md-4">
							<label>Trip start date <font color="red"><sup>*</sup></font></label>
							<input style="font-size:12px !important" readonly required autocomplete="off" type="text" class="form-control" name="trip_start_date" id="trip_start_date" />
						</div>
						
						<div class="form-group col-md-4">
							<label>Trip end date <font color="red"><sup>*</sup></font></label>
							<input style="font-size:12px !important" readonly required autocomplete="off" type="text" class="form-control" name="trip_end_date" id="trip_end_date" />
						</div>
						
						<div class="form-group col-md-6">
							<div id="down_alert" class="alert alert-danger" style="font-weight:bold">
								Last DA : <span id="last_da_paid_result"></span> 
							</div>	
						</div>
						
						<div class="form-group col-md-6">
							<div id="down_alert" class="alert alert-danger" style="font-weight:bold">
								Last SALARY : <span id="last_sal_paid_result"></span> 
							</div>	
						</div>
						
						<div class="form-group col-md-4">
							<label>DA Start Date <font color="red"><sup>*</sup></font></label>
							<input style="font-size:12px !important" id="da_date" name="da_date" pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}" class="form-control" type="date" required>
						</div>
						
						<div class="form-group col-md-4">
							<label>SALARY Start Date <font color="red"><sup>*</sup></font></label>
						    <input style="font-size:12px !important" id="salary_date" pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}" name="salary_date" class="form-control" type="date" required>
						</div>
						
						<input type="hidden" name="da_found" id="da_found">
					   <input type="hidden" name="salary_found" id="salary_found">
					   <input type="hidden" name="driver_code" id="driver_code">
					   <input type="hidden" name="truck_no" id="truck_no">
					   
					   <input type="hidden" name="da_date_db" id="da_date_db">
					   <input type="hidden" name="sal_date_db" id="sal_date_db">
			   
						<div class="form-group col-md-4">
							<?php if(!isMobile()) { echo "<label>&nbsp;</label><br />"; } ?>
							<button disabled style="display:none" type="submit" class="btn btn-sm btn-success 
							<?php if(isMobile()) { echo "btn-block"; } ?>" id="submit_btn"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> 
							&nbsp; Update</button>
						</div>	
						
					</div>
				</div>
</form>				
			<?php
}
?>			
	<div class="col-md-12">&nbsp;</div>
				
				<div class="col-md-12 table-responsive" id="load_table">
				</div> 
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

<div id="func_result"></div>  
<div id="load_modal_div"></div>  


<script type="text/javascript">
$(document).ready(function (e) {
$("#Form1").on('submit',(function(e) {
$("#loadicon").show();
$("#submit_btn").attr("disabled", true);
e.preventDefault();
	$.ajax({
	url: "./save_update_da_salary_dates.php",
	type: "POST",
	data:  new FormData(this),
	contentType: false,
	cache: false,
	processData:false,
	success: function(data){
		$("#func_result").html(data);
	},
	error: function() 
	{} });}));});
	
function SearchDaSalary()
{
	var veh_no = $('#veh_no').val();
	
	if(veh_no=='')
	{
		Swal.fire({icon: 'warning',html: '<font size=\'2\' color=\'black\'>Enter vehicle number first !</font>',});
	}
	else
	{
		$("#loadicon").show();
		jQuery.ajax({
			url: "load_update_da_salary_dates.php",
			data: 'veh_no=' + veh_no,
			type: "POST",
			success: function(data) {
			$("#func_result").html(data);
		},
		error: function() {}
		});
	}
}	
</script>
 
<?php include("footer.php") ?>