<?php
require_once("connect.php");

$vou_no = escapeString($conn,($_POST['vou_no']));
$date_today = date("Y-m-d");

if(empty($vou_no))
{
	AlertRightCornerError("Voucher number not found !");
	exit();
} 

$get_data = Qry($conn,"SELECT f.id,f.date,f.lrno,f.fstation,f.tstation,f.consignor,f.consignee,f.wt12,f.weight 
FROM freight_form_lr AS f 
LEFT OUTER JOIN lr_sample AS l ON l.id = f.mother_lr_id 
WHERE f.frno='$vou_no'");

if(!$get_data){
	AlertRightCornerError("Error while processing request !");
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	exit();
}
	
if(numRows($get_data)==0)
{
	AlertRightCornerError("Trip not found !");
	exit();
}

?>	
<button id="open_modal_btn" style="display:none" data-toggle="modal" data-target="#MarketLREditModal"></button>

<div class="modal fade" id="MarketLREditModal" role="dialog" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-xl">
		<div class="modal-content" style="">
			<div class="modal-header bg-primary">
				<span style="font-size:13px">Update Market LR : <?php echo $vou_no; ?> </span>
			</div>
		<div class="modal-body">
			<div class="row">
				<div class="form-group col-md-12 table-responsive" style="overflow:auto">
					<table class="table table-bordered table-striped" style="font-size:12px !important">
						<tr>
							<th>LR_No</th>
							<th>LR_Date</th>
							<th>From</th>
							<th>To</th>
							<th>Consignor</th>
							<th>Consignee</th>
							<th>Actual_Wt</th>
							<th>Charge_Wt</th>
							<th>#Update</th>
						</tr>

				<?php
				while($row = fetchArray($get_data))
				{
					
$id2 = $row['id'];		

echo "<script> 
$(function() {
		$('#from_loc_$id2').autocomplete({ 
		source: '../b5aY6EZzK52NA8F/autofill/get_location.php',
		select: function (event, ui) { 
            $('#from_loc_$id2').val(ui.item.value);   
            $('#from_id').val(ui.item.id);      
            return false;
		},
		change: function (event, ui) {
		if(!ui.item){
			$(event.target).val('');
            $(event.target).focus();
			$('#from_loc_$id2').val('');   
			$('#from_id').val('');   
			alert('Location does not exists.');
		}}, 
	focus: function (event, ui){
	return false;
	}
});});
</script>";					
					echo "<tr>
						<td><input style='font-size:11px !important;height:20px !important' type='text' id='lrno_$row[id]' value='$row[lrno]' /></td>
						<td><input max='$date_today' style='font-size:11px !important;height:20px !important' type='date' pattern='[0-9]{4}-[0-9]{2}-[0-9]{2}' id='lr_date_$row[id]' value='$row[date]' /></td>
						<td><input style='font-size:11px !important;height:20px !important' type='text' oninput='this.value=this.value.replace(/[^a-z -.A-Z]/,\"\")' id='from_loc_$row[id]' value='$row[fstation]' /></td>
						<td><input style='font-size:11px !important;height:20px !important' type='text' oninput='this.value=this.value.replace(/[^a-z -.A-Z]/,\"\")' id='to_loc_$row[id]' value='$row[tstation]' /></td>
						<td><input style='font-size:11px !important;height:20px !important' type='text' oninput='this.value=this.value.replace(/[^a-z -.A-Z0-9]/,\"\")' id='consignor_$row[id]' value='$row[consignor]' /></td>
						<td><input style='font-size:11px !important;height:20px !important' type='text' oninput='this.value=this.value.replace(/[^a-z -.A-Z0-9]/,\"\")' id='consignee_$row[id]' value='$row[consignee]' /></td>
						<td><input style='font-size:11px !important;height:20px !important' type='number' min='0.0001' id='actual_wt_$row[id]' value='$row[wt12]' /></td>
						<td><input style='font-size:11px !important;height:20px !important' type='number' min='0.0001' id='charge_wt_$row[id]' value='$row[weight]' /></td>
						<td><button class='btn btn-success btn-xs' onclick='Update($row[id])'>Update</button></td>
					</tr>";
				}
				?>				
					</table>
				</div>
			</div>
        </div>

	 <div class="modal-footer">
          <button type="button" id="close_market_lr_edit_modal" class="btn btn-sm btn-primary" data-dismiss="modal">Close</button>
	 </div>
	 
      </div>
    </div>
  </div>
 
<script>
$('#open_modal_btn')[0].click();	
$('#loadicon').fadeOut('slow');	
</script>