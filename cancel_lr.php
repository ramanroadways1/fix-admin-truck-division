<?php include("header.php"); ?>

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBfe7kVsIg9oWfBfV5FCwzBQ8pZKd3iSFI&libraries=places"></script>

<?php
$chk = Qry($conn,"SELECT id FROM _access_control WHERE username='$_SESSION[ediary_fix_admin]' AND func_id=(SELECT id FROM 
_access_control_func_list WHERE session_role='8' AND func_name='Manage_Location') AND u_insert='1'");
			  
if(numRows($chk)==0)
{
	echo "<script>window.location.href='./';</script>";
	exit();
}
?>

<div class="content-wrapper">
      <section class="content-header">
          <h1 style="font-size:16px;">Cance LR : </h1>
       </section>
       
	   <section class="content">
          <div class="row">
            <div class="col-xs-12">
			<div class="box">
                <div class="box-body">
	<div class="col-md-12">&nbsp;</div>			
<form action="#" method="POST" id="Form1" autocomplete="off" style="font-size:12px !important">
			
				<div class="col-md-12">
					
					<div class="row">
						
						<div class="form-group col-md-3">
							<label>Cancel By <font color="red"><sup>*</sup></font></label>
							<select style="font-size:12px !important;" onchange="CancelBy(this.value)" name="cancel_by" class="form-control" required>
								<option style="font-size:12px !important;" value="">--select an option--</option>
								<option style="font-size:12px !important;" value="LR">LR Number</option>
								<option style="font-size:12px !important;" value="RANGE">LR Series</option>
							</select>
						</div>
						
						<script>
						function CancelBy(elem)
						{
							$('#lr_from').val('');
							$('#lr_to').val('');
							$('#lr_no').val('');
							
							if(elem=='RANGE')
							{
								$('.lr_series_div').show();
								$('.lrno_div').hide();
								
								$('#lr_from').attr('required',true);
								$('#lr_to').attr('required',true);
								$('#lr_no').attr('required',false);
							}
							else
							{
								$('.lr_series_div').hide();
								$('.lrno_div').show();
								
								$('#lr_from').attr('required',false);
								$('#lr_to').attr('required',false);
								$('#lr_no').attr('required',true);
							}
						}
						
						function CalculateFunc()
						{
							var from1 = $('#lr_from').val();
							var to1 = $('#lr_to').val();
							
							if(from1==''){
								var from1 = 0;
							}
							
							if(to1==''){
								var to1 = 0;
							}
							
							 $("#lr_count").html((Number(to1) - Number(from1))+1);
							 $("#bilty_stock1").val((Number(to1) - Number(from1))+1);
						}
						</script>
						
						<div class="form-group col-md-3 lrno_div">
							<label>LR Number <font color="red"><sup>*</sup></font></label>
							<input style="font-size:12px !important;" autocomplete="off" oninput="this.value=this.value.replace(/[^A-Za-z0-9]/,'');" type="text" class="form-control" name="lr_no" id="lr_no" />
						</div>
						
						<div class="form-group col-md-3 lr_series_div" style="display:none">
							<label>From LR <font color="red"><sup>*</sup></font></label>
							<input style="font-size:12px !important;" autocomplete="off" oninput="this.value=this.value.replace(/[^A-Za-z0-9]/,'');CalculateFunc()" type="text" class="form-control" name="lr_from" id="lr_from" />
						</div>
						
						<div class="form-group col-md-3 lr_series_div" style="display:none">
							<label>To LR <font color="red"><sup>* <span id="lr_count"></span></sup></font></label>
							<input style="font-size:12px !important;" autocomplete="off" oninput="this.value=this.value.replace(/[^A-Za-z0-9]/,'');CalculateFunc()" type="text" class="form-control" name="lr_to" id="lr_to" />
						</div>
						
						<input type="hidden" name="bilty_stock" id="bilty_stock1">
						
						<div class="form-group col-md-3">
							<?php if(!isMobile()) { echo "<label>&nbsp;</label><br />"; } ?>
							<button type="submit" class="btn btn-sm btn-success <?php if(isMobile()) { echo "btn-block"; } ?>" id="add_btn">Cancel LR</button>
						</div>
					</div>
				</div>
</form>				
				</div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

<div id="func_result"></div>  
<div id="load_modal_div"></div>  
<div id="modal_load_div"></div>  


<script type="text/javascript">
$(document).ready(function (e) {
$("#Form1").on('submit',(function(e) {
$("#loadicon").show();
$("#add_btn").attr("disabled", true);
e.preventDefault();
	$.ajax({
	url: "./save_cancel_lr.php",
	type: "POST",
	data:  new FormData(this),
	contentType: false,
	cache: false,
	processData:false,
	success: function(data){
		$("#func_result").html(data);
	},
	error: function() 
	{} });}));});
</script>
			 
<?php include("footer.php") ?>