<?php
require_once("./connect.php");

$company = escapeString($conn,strtoupper($_POST['company']));
$from = escapeString($conn,strtoupper($_POST['from_series']));
$to = escapeString($conn,strtoupper($_POST['to_series']));
$branch = escapeString($conn,strtoupper($_POST['branch']));
$stock = escapeString($conn,strtoupper($_POST['bilty_stock1']));

$date = date("Y-m-d");
$timestamp = date("Y-m-d");

$chk_stock = Qry($conn,"SELECT id FROM lr_bank WHERE from_range='$from' AND to_range='$to'");

if(!$chk_stock)
{
	AlertRightCornerError("Error while processing request !");
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	exit();
}

if(numRows($chk_stock)>0)
{
	AlertRightCornerError("LR-Bank already Updated !");
	echo "<script>
		$('#add_btn').attr('disabled',false);
	</script>";	
	exit();
}

$chk_lr2 = Qry($conn,"SELECT id,branch,from_range,to_range FROM lr_bank WHERE $from>=from_range AND $to<=to_range");

if(!$chk_lr2)
{
	AlertRightCornerError("Error while processing request !");
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	exit();
}

if(numRows($chk_lr2)>0)
{
	$row_two = fetchArray($chk_lr2);
	
	AlertRightCornerError("Duplicate Series Found. LR Book Belongs to Branch : $row_two[branch]. Given Book Series is : $row_two[from_range] to $row_two[to_range] !");
	echo "<script>
		$('#add_btn').attr('disabled',false);
	</script>";	
	exit();
}
else
{
	$chk_lr_db = Qry($conn,"SELECT id,branch,from_range,to_range FROM lr_bank_db WHERE $from>=from_range AND $to<=to_range");

	if(!$chk_lr_db)
	{
		AlertRightCornerError("Error while processing request !");
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		exit();
	}

	if(numRows($chk_lr_db)>0)
	{
		$row_two_db = fetchArray($chk_lr_db);
		
		AlertRightCornerError("Duplicate Series Found. LR Book Belongs to Branch : $row_two_db[branch]. Given Book Series is : $row_two_db[from_range] to $row_two_db[to_range] !");
		echo "<script>
			$('#add_btn').attr('disabled',false);
		</script>";	
		exit();
	}
}
	
$qry_chk = Qry($conn,"SELECT id,bilty_id,branch FROM lr_check WHERE lrno>=$from AND lrno<=$to AND lrno NOT LIKE '0%' AND branch!='PEN'");

if(!$qry_chk)
{
	AlertRightCornerError("Error while processing request !");
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	exit();
}

if(numRows($qry_chk) > 0)
{
	$row_chk = fetchArray($qry_chk);
	
	$book_id = $row_chk['bilty_id'];
	
	if(empty($book_id))
	{
		AlertRightCornerError("LR already used in branch: $row_chk[branch] !");
		echo "<script>$('#add_btn').attr('disabled',false);</script>";	
		exit();
	}
	
	$qry_stock_fetch = Qry($conn,"SELECT branch,from_range,to_range FROM lr_bank WHERE id='$book_id'");
	
	if(!$qry_stock_fetch)
	{
		AlertRightCornerError("Error while processing request !");
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		echo "<script>$('#add_btn').attr('disabled',false);</script>";	
		exit();
	}
	
	if(numRows($qry_stock_fetch)>0)
	{
		$row_stock = fetchArray($qry_stock_fetch);
		
		AlertRightCornerError("Duplicate LRs found in Series : $from to $to. LR Stock Belongs to $row_stock[branch]. Series $row_stock[from_range] to $row_stock[to_range] !");
		echo "<script>$('#add_btn').attr('disabled',false);</script>";	
		exit();
	}
	else
	{
		$qry_stock_fetch2 = Qry($conn,"SELECT branch,from_range,to_range FROM lr_bank_db WHERE id='$book_id'");
	
		if(!$qry_stock_fetch2)
		{
			AlertRightCornerError("Error while processing request !");
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			echo "<script>$('#add_btn').attr('disabled',false);</script>";	
			exit();
		}
		
		$row_stock2 = fetchArray($qry_stock_fetch2);
		
		AlertRightCornerError("Duplicate LRs found in Series : $from to $to. LR Stock Belongs to $row_stock2[branch]. Series $row_stock2[from_range] to $row_stock2[to_range] !");
		echo "<script>$('#add_btn').attr('disabled',false);</script>";	
		exit();
	}
}
else
{ 
	$insert = Qry($conn,"INSERT INTO lr_bank (branch,from_range,to_range,company,stock,stock_left,date,timestamp) VALUES 
	('$branch','$from','$to','$company','$stock','$stock','$date','$timestamp')");
	
	if(!$insert)
	{
		AlertRightCornerError("Error while processing request !");
		echo "<script>$('#add_btn').attr('disabled',false);</script>";	
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		exit();
	}
	
	AlertRightCornerSuccessFadeFast("Added successfully !");
	echo "<script>$('#add_btn').attr('disabled',false);</script>";	
	echo "<script>
		LoadTable();
	</script>";
}
?>