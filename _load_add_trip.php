<?php
require_once("./connect.php");

$date2 = date("Y-m-d");

$tno = escapeString($conn,strtoupper($_POST['veh_no']));
$trip_type = escapeString($conn,strtoupper($_POST['trip_type']));
$trip_no = escapeString($conn,strtoupper($_POST['trip_no']));

if($trip_type=='RUNNING')
{
	$trip_id="id";
	
	$qry = Qry($conn,"SELECT id,tno,trip_no,branch,from_station,to_station,date(date) as trip_date,date(end_date) as end_date,lr_type 
	FROM dairy.trip WHERE tno='$tno' ORDER by id ASC");
}
else
{
	$trip_id="trip_id";
	
	$qry = Qry($conn,"SELECT id,tno,trip_no,branch,from_station,to_station,date(date) as trip_date,date(end_date) as end_date,lr_type 
	FROM dairy.trip_final WHERE trip_no='$trip_no' ORDER by id ASC");
}

if(!$qry)
{
	AlertErrorTopRight("Error while processing request !");
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	exit();
}

if(numRows($qry)==0)
{
	AlertErrorTopRight("No record found !");
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	exit();
}

	echo "<table id='example' class='table table-bordered table-striped' style='font-size:13px'>
	 <thead>
	<tr>
		<th>Id</th>
		<th>Vehicle_No</th>
		<th>Branch</th>
		<th>From</th>
		<th>To</th>
		<th>LR_Number</th>
		<th>Start_Date</th>
		<th>End_Date</th>
	</tr>
  </thead>
  <tbody>	
	";
	$id=1;
	while($row_qry=fetchArray($qry))
	{
		echo "
			<tr>
				<td colspan='8'>
					<button class='btn_add_trip btn btn-primary btn-xs' onclick=Insert('$row_qry[id]','$row_qry[tno]','$trip_type','$row_qry[trip_no]')>Insert Trip here</button>
				</td>
			</tr>
			
			<tr>
				<td>$id</td>
				<td>$row_qry[tno]</td>
				<td>$row_qry[branch]</td>
				<td>$row_qry[from_station]</td>
				<td>$row_qry[to_station]</td>
				<td>$row_qry[lr_type]</td>
				<td>$row_qry[trip_date]</td>
				<td>$row_qry[end_date]</td>
			</tr>
			";
	$id++;
	}
	echo "
	</tbody>
	</table>";

?>
<script>				  
	$("#loadicon").fadeOut('slow');
</script>

<?php
$chk_update = Qry($conn,"SELECT id FROM _access_control WHERE username='$_SESSION[ediary_fix_admin]' AND func_id=(SELECT id FROM 
_access_control_func_list WHERE session_role='8' AND func_name='Add_Trips') AND u_insert='1'");
			  
if(numRows($chk_update)>0)
{
	echo "<script>
		$('.btn_add_trip').attr('disabled',false);
	</script>";	
}
else
{
	echo "<script>
		$('.btn_add_trip').attr('disabled',true);
		$('.btn_add_trip').attr('onclick','');
	</script>";	
}
?>	