<?php
require_once("./connect.php");

$tno = escapeString($conn,strtoupper($_POST['veh_no']));

if($tno=='')
{
	AlertErrorTopRight("Vehicle number not found !");
	exit();
}

$fetch_driver = Qry($conn,"SELECT up,code FROM dairy.driver_up WHERE tno='$tno' AND down=0 ORDER BY id DESC LIMIT 1");

if(!$fetch_driver)
{
	AlertErrorTopRight("Error while processing request !");
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	exit();
}

if(numRows($fetch_driver)==0)
{
	AlertErrorTopRight("No Active driver found !");
	exit();
}

$row = fetchArray($fetch_driver);
	
$driver_code = $row['code'];
$driver_up_date = $row['up'];
	
$get_driver_name = Qry($conn,"SELECT name,last_salary FROM dairy.driver WHERE code='$driver_code'");

if(!$get_driver_name)
{
	AlertErrorTopRight("Error while processing request !");
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	exit();
}
	
$row_driver_name = fetchArray($get_driver_name);
	
$driver_name = $row_driver_name['name'];
$last_salary = $row_driver_name['last_salary'];
	
$fetch_last_da = Qry($conn,"SELECT to_date,narration FROM dairy.da_book WHERE driver_code='$driver_code' ORDER BY id DESC LIMIT 1");

if(!$fetch_last_da)
{
	AlertErrorTopRight("Error while processing request !");
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	exit();
}
	
$row_last_da = fetchArray($fetch_last_da);
$da_narration = $row_last_da['narration'];
$da_date_Db = $row_last_da['to_date'];
	
$fetch_last_salary = Qry($conn,"SELECT to_date,narration FROM dairy.salary WHERE driver_code='$driver_code' ORDER BY id DESC LIMIT 1");

if(!$fetch_last_salary)
{
	AlertErrorTopRight("Error while processing request !");
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	exit();
}
	
$row_last_salary = fetchArray($fetch_last_salary);
$salary_narration = $row_last_salary['narration'];
$salary_date_Db = $row_last_salary['to_date'];
	
if(numRows($fetch_last_da)>0)
{
	if($da_narration=='ON_LEAVE_UP' || $da_narration=='LEFT_COMP_UP' || $da_narration=='DA_START_ADMIN')
	{
		$driver_da_date = $da_date_Db;
		$da_book_found = "YES";
	}
	else
	{
		$driver_da_date = date('Y-m-d', strtotime($da_date_Db. ' + 1 days'));
		$da_book_found = "NO";
	}
}
else
{
	$driver_da_date = $driver_up_date;
	$da_book_found = "NO";
}
	
if(numRows($fetch_last_salary)>0)
{
	if($salary_narration=='ON_LEAVE_UP' || $salary_narration=='LEFT_COMP_UP' || $salary_narration=='SALARY_START_ADMIN')
	{
		$driver_salary_date = $salary_date_Db;
		$salary_book_found = "YES";
	}
	else
	{
		$driver_salary_date = date('Y-m-d', strtotime($salary_date_Db. ' + 1 days'));
		$salary_book_found = "";
	}
}
else
{
	$driver_salary_date = $last_salary;
	$salary_book_found = "NO";
}
	
$fetch_last_trip = Qry($conn,"SELECT date(end_date) as end_date,date(hisab_date) as hisab_date FROM dairy.trip_final WHERE 
driver_code='$driver_code' ORDER BY id DESC LIMIT 1");
	
if(!$fetch_last_trip)
{
	AlertErrorTopRight("Error while processing request !");
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	exit();
}
	
$fetch_running_trip = Qry($conn,"SELECT date(date) as start_date FROM dairy.trip WHERE driver_code='$driver_code' ORDER BY id 
ASC LIMIT 1");
	
if(!$fetch_running_trip)
{
	AlertErrorTopRight("Error while processing request !");
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	exit();
}
	
	if(numRows($fetch_last_trip)==0)
	{
		$last_trip_end_date = "NULL";
	}
	else
	{
		$row_last_trip = fetchArray($fetch_last_trip);
		
		if($row_last_trip['end_date']==0)
		{
			$last_trip_end_date = $row_last_trip['hisab_date'];
		}
		else
		{
			$last_trip_end_date = $row_last_trip['end_date'];
		}
	}
	
	if(numRows($fetch_running_trip)==0)
	{
		$running_trip_date = "NULL";
	}
	else
	{
		$row_running_trip = fetchArray($fetch_running_trip);
		$running_trip_date = $row_running_trip['start_date'];
	}
	
$fetch_last_da_dates = Qry($conn,"SELECT from_date,to_date FROM dairy.da_book WHERE driver_code='$driver_code' AND trip_no!='' 
ORDER BY id DESC LIMIT 1");

if(!$fetch_last_da_dates)
{
	AlertErrorTopRight("Error while processing request !");
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	exit();
}
	
$fetch_last_sal_dates = Qry($conn,"SELECT from_date,to_date FROM dairy.salary WHERE driver_code='$driver_code' AND trip_no!='' 
ORDER BY id DESC LIMIT 1");

if(!$fetch_last_sal_dates)
{
	AlertErrorTopRight("Error while processing request !");
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	exit();
}	

	if(numRows($fetch_last_da_dates)==0)
	{
		$min_da_date = date('Y-m-d', strtotime($driver_up_date. ' -10 days'));
		
		$da_paid_duration = "NULL";
	}
	else
	{
		$row_last_da_paid_dates = fetchArray($fetch_last_da_dates);
		$min_da_date = date('Y-m-d', strtotime($row_last_da_paid_dates['to_date']. ' + 1 days'));
		
		$last_da_from = $row_last_da_paid_dates['from_date'];
		$last_da_to = $row_last_da_paid_dates['to_date'];
		
		$da_paid_duration = date('d/m/y', strtotime($last_da_from))." to ".date('d/m/y', strtotime($last_da_to));
	}
	
	if(numRows($fetch_last_sal_dates)==0)
	{
		$min_sal_date = date('Y-m-d', strtotime($driver_up_date. ' -10 days'));
		
		$sal_paid_duration = "NULL";
	}
	else
	{
		$row_last_sal_paid_dates = fetchArray($fetch_last_sal_dates);
		$min_sal_date = date('Y-m-d', strtotime($row_last_sal_paid_dates['to_date']. ' + 1 days'));
		
		$last_sal_from = $row_last_sal_paid_dates['from_date'];
		$last_sal_to = $row_last_sal_paid_dates['to_date'];
		
		$sal_paid_duration = date('d/m/y', strtotime($last_sal_from))." to ".date('d/m/y', strtotime($last_sal_to));
	}
	
	
	echo "<script>
		$('#driver_name').val('$driver_name');
		$('#trip_start_date').val('$running_trip_date');
		$('#trip_end_date').val('$last_trip_end_date');
		$('#driver_code').val('$driver_code');
		$('#truck_no').val('$tno');
		
		$('#da_date_db').val('$driver_da_date');
		$('#sal_date_db').val('$driver_salary_date');
		
		$('#da_date').val('$driver_da_date');
		$('#salary_date').val('$driver_salary_date');
		$('#da_found').val('$da_book_found');
		$('#salary_found').val('$salary_book_found');
		
		$('#da_date').attr('min','$min_da_date');
		$('#salary_date').attr('min','$min_sal_date');
		
		$('#last_da_paid_result').html('$da_paid_duration');
		$('#last_sal_paid_result').html('$sal_paid_duration');
		
		$('#loadicon').fadeOut('slow');
		
		$('#driver_result_div').show();
		$('#submit_btn').show();
		$('#search_btn').attr('disabled',true);
		$('#submit_btn').attr('disabled',false);
		$('#veh_no').attr('readonly',true);
		$('#search_btn').hide();
	</script>";
		
?>