<?php include("header.php"); ?>

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBfe7kVsIg9oWfBfV5FCwzBQ8pZKd3iSFI&libraries=places"></script>

<?php
$chk = Qry($conn,"SELECT id FROM _access_control WHERE username='$_SESSION[ediary_fix_admin]' AND func_id=(SELECT id FROM 
_access_control_func_list WHERE session_role='8' AND func_name='Manage_Location') AND u_insert='1'");
			  
if(numRows($chk)==0)
{
	echo "<script>window.location.href='./';</script>";
	exit();
}
?>

<script>
$(function() {
		$("#location_name").autocomplete({
		source: '../b5aY6EZzK52NA8F/autofill/get_location.php',
		select: function (event, ui) { 
            $('#location_name').val(ui.item.value);   
            return false;},
		change: function (event, ui) {
		if(!ui.item){
			// $(event.target).val("");
            // $(event.target).focus();
		}}, 
	focus: function (event, ui){
	return false;
	}
});});
</script>

<div class="content-wrapper">
      <section class="content-header">
          <h1 style="font-size:16px;">Add New Location : </h1>
       </section>
       
	   <section class="content">
          <div class="row">
            <div class="col-xs-12">
			<div class="box">
                <div class="box-body">
	<div class="col-md-12">&nbsp;</div>			
<form action="#" method="POST" id="Form1" autocomplete="off">
			
				<div class="col-md-12">
					
	<div class="row" style="display:none" id="map_div">
			<div class="col-md-12">
				<div class="form-group">
					<!--style="margin-top:10px;width:50%;font-size:14px"-->
					<input oninput="ResetPincode();" style="margin-top:10px;width:50%;font-size:12px !important;display:none" id="searchInput" class="form-control" type="text" placeholder="Enter a location">
					<div class="map" id="map" style="width: 100%; height: 300px;"></div>
				</div> 
			</div> 
			
			<div class="col-md-12">
				<div class="form-group">
					<font color="blue"><b>Address :</b></font> <span id="addr_set"></span>
					<input type="hidden" name="location" id="location">
					<input type="hidden" name="lat" id="lat">
					<input type="hidden" name="lng" id="lng">
				</div>
			</div>
	</div>
	
					<div class="row">
						
						<div class="form-group col-md-3">
							<label>Location Name <font color="red"><sup>*</sup></font></label>
							<input style="font-size:12px !important;" autocomplete="off" oninput="this.value=this.value.replace(/[^A-Za-z .0-9]/,'');" type="text" class="form-control" name="location_name" id="location_name" />
						</div>
						
						<div class="form-group col-md-3">
							<label>State <font color="red"><sup>*</sup></font></label>
							<select style="font-size:12px !important;" onchange="GetPincode()" name="state" id="state_name" class="form-control" required>
								<option style="font-size:12px !important;" value="">--select state--</option>
								<?php
								$qry = Qry($conn,"SELECT name,code FROM state_codes ORDER BY name ASC");
								
								if(numRows($qry)>0)
								{
									while($row=fetchArray($qry))
									{
										echo "<option style='font-size:12px !important;' value='$row[name]'>$row[name]</option>";
									}
								}
								?>
							</select>
						</div>
						
						<script>
						function ResetPincode()
						{
							$('#pincode_db').val('');
							$('#state_name').val('');
						}
						
						function GetPincode()
						{
							var google_location = $('#searchInput').val();
							var lat_1 = $('#lat').val();
							var long_1 = $('#lng').val();
							var location_name = $('#location_name').val();
						
							if(google_location=='' || lat_1=='' || long_1=='' || location_name=='')
							{
								$('#state_name').val('');
							}
							else
							{
								$('#searchInput').attr('readonly',true);
								
								if($('#pincode_db').val()=='')
								{
									$("#loadicon").show();
										$.ajax({
										url: "get_pincode_by_lat_lng.php",
										method: "post",
										data:'lat_1=' + lat_1 + '&long_1=' + long_1,
										success: function(data){
										$("#result_get_pincode").html(data);
									}})
								}
							}
						}
						</script>
						
						<div id="result_get_pincode"></div>
						
						<div class="form-group col-md-3">
							<label>Pincode <font color="red"><sup>*</sup></font></label>
							<input style="font-size:12px !important;" autocomplete="off" readonly type="text" class="form-control" name="pincode_db" id="pincode_db" />
						</div>
						
						<div class="form-group col-md-3">
							<?php if(!isMobile()) { echo "<label>&nbsp;</label><br />"; } ?>
							<button type="submit" class="btn btn-sm btn-success <?php if(isMobile()) { echo "btn-block"; } ?>" id="add_btn"><i class="fa fa-plus-square-o" aria-hidden="true"></i> &nbsp; Add Location</button>
						</div>
					</div>
				</div>
</form>				
				</div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

<div id="func_result"></div>  
<div id="load_modal_div"></div>  
<div id="modal_load_div"></div>  


<script type="text/javascript">
$(document).ready(function (e) {
$("#Form1").on('submit',(function(e) {
$("#loadicon").show();
$("#add_btn").attr("disabled", true);
e.preventDefault();
	$.ajax({
	url: "./save_manage_locations.php",
	type: "POST",
	data:  new FormData(this),
	contentType: false,
	cache: false,
	processData:false,
	success: function(data){
		$("#func_result").html(data);
	},
	error: function() 
	{} });}));});
</script>
 
<script>
function initialize() {
   var latlng = new google.maps.LatLng(28.5355161,77.39102649999995);
    var map = new google.maps.Map(document.getElementById('map'), {
      center: latlng,
      zoom: 13
    });
    var marker = new google.maps.Marker({
      map: map,
      position: latlng,
      draggable: true,
      anchorPoint: new google.maps.Point(0, -29)
   });
    var input = document.getElementById('searchInput');
    map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);
    var geocoder = new google.maps.Geocoder();
    var autocomplete = new google.maps.places.Autocomplete(input);
    autocomplete.bindTo('bounds', map);
    var infowindow = new google.maps.InfoWindow();   
    autocomplete.addListener('place_changed', function() {
        infowindow.close();
        marker.setVisible(false);
        var place = autocomplete.getPlace();
        if (!place.geometry) {
            window.alert("Autocomplete's returned place contains no geometry");
            return;
        }
			
		var addressComponent = place.address_components;
			
		for (var x = 0 ; x < addressComponent.length; x++) {
                var chk = addressComponent[x];
                if (chk.types[0] == 'postal_code') {
                    var zipCode = chk.long_name;
                }
            }
			
			 if (zipCode) {
                document.getElementById('pincode_db').value = zipCode;
            }
            else {
                document.getElementById('pincode_db').value = 'No pincode';
            }
			
        // If the place has a geometry, then present it on a map.
        if (place.geometry.viewport) {
            map.fitBounds(place.geometry.viewport);
        } else {
            map.setCenter(place.geometry.location);
            map.setZoom(17);
        }
       
        marker.setPosition(place.geometry.location);
        marker.setVisible(true);          
    
        bindDataToForm(place.formatted_address,place.geometry.location.lat(),place.geometry.location.lng());
        infowindow.setContent(place.formatted_address);
        infowindow.open(map, marker);
       
    });
    // this function will work on marker move event into map 
    google.maps.event.addListener(marker, 'dragend', function() {
        geocoder.geocode({'latLng': marker.getPosition()}, function(results, status) {
        if (status == google.maps.GeocoderStatus.OK) {
          if (results[0]) {        
              bindDataToForm(results[0].formatted_address,marker.getPosition().lat(),marker.getPosition().lng());
              infowindow.setContent(results[0].formatted_address);
              infowindow.open(map, marker);
          }
        }
        });
    });
	
	$('#map_div').show();
}
function bindDataToForm(address,lat,lng){
   $('#addr_set').html(address);
   document.getElementById('location').value = address;
   document.getElementById('lat').value = lat;
   document.getElementById('lng').value = lng;
}

google.maps.event.addDomListener(window, 'load', initialize);

$(document).ready(function() {
    $("#searchInput").delay(800).fadeIn(500);
});
</script>
			 
<?php include("footer.php") ?>