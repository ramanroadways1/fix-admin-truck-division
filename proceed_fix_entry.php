<?php
require_once 'connect.php';

$id = escapeString($conn,($_POST['id']));
$date = date("Y-m-d");
$timestamp = date("Y-m-d H:i:s");

echo "<script>$('#fix_entry_btn_$id').attr('disabled',true);</script>";

$get_fix_txn = Qry($conn,"SELECT f.tno,f.lane_id,f.trip_id,f.rule_id,t.trip_no,t.branch_user,t.fix_lane,t.driver_code,t.tno as trip_tno,
t.branch as trip_branch,t.act_wt,d.name as driver_name,d.mobile,d.mobile2 
FROM dairy.fix_txn_pending AS f 
LEFT OUTER JOIN dairy.trip AS t ON t.id = f.trip_id 
LEFT OUTER JOIN dairy.driver AS d ON d.code = t.driver_code 
WHERE f.id='$id'");

if(!$get_fix_txn){
	errorLog(getMySQLError($conn),$conn,$page_url,__LINE__);
	AlertError("Error !");
	exit();
}

if(numRows($get_fix_txn)==0)
{
	AlertError("Record not found !");
	exit();
}

$row_fix = fetchArray($get_fix_txn);

if($row_fix['rule_id'] != $row_fix['fix_lane'])
{
	AlertError("Rule not verified !");
	exit();
}

if($row_fix['tno'] != $row_fix['trip_tno'])
{
	AlertError("Vehicle not verified !");
	exit();
}

$tno = $row_fix['tno'];
$actual_wt = $row_fix['act_wt'];
$driver_name = $row_fix['driver_name'];
$driver_mobile1 = $row_fix['mobile'];
$driver_mobile2 = $row_fix['mobile2'];
$driver_code = $row_fix['driver_code'];
$trip_branch = $row_fix['trip_branch'];
$rule_id = $row_fix['rule_id'];
$trip_branch_user = $row_fix['branch_user'];
$trip_id = $row_fix['trip_id'];
$trip_no = $row_fix['trip_no'];

if($driver_name=='')
{
	AlertError("Driver not found !");
	exit();
}

if($driver_code=='' || $driver_code==0)
{
	AlertError("Driver not found !");
	exit();
}

if($trip_branch=='')
{
	AlertError("Trip branch not found !");
	exit();
}

$get_rule_data = Qry($conn,"SELECT fix_diesel_type,diesel_value,total_exp,adv_amount FROM dairy.fix_lane_rules WHERE id='$rule_id'");

if(!$get_rule_data){
	errorLog(getMySQLError($conn),$conn,$page_url,__LINE__);
	AlertError("Error !");
	exit();
}

if(numRows($get_rule_data)==0)
{
	AlertError("Rule not found !");
	exit();
}

$row_rule = fetchArray($get_rule_data);

$fix_adv_amount = $row_rule['adv_amount'];
$total_exp = $row_rule['total_exp'];
$fix_diesel_type = $row_rule['fix_diesel_type'];
$diesel_value = $row_rule['diesel_value'];

$get_company = Qry($conn,"SELECT comp,happay_active FROM dairy.own_truck WHERE tno='$tno'");

if(!$get_company){
	AlertError("Error..");
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	exit();
}
		
if(numRows($get_company)==0)
{
	AlertError("Company not found !");
	exit();
}

$row_comp = fetchArray($get_company);
$company = $row_comp['comp'];
$happay_active = $row_comp['happay_active'];

if($happay_active=="1")
{
	$trans_type="HAPPAY";
}
else
{
	$trans_type="CASH";
}

StartCommit($conn);
$flag = true;

if($fix_adv_amount>0)
{
	$get_vou_id = GetVouId("T",$conn,"mk_tdv","tdvid",$trip_branch);
	
	if(!$get_vou_id || $get_vou_id=="0" || $get_vou_id==""){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
	
	$tids = $get_vou_id;
	
	$trans_id_Qry=GetTransId($trans_type,$conn,"cash");
	
	if(!$trans_id_Qry || $trans_id_Qry=="" || $trans_id_Qry=="0"){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
	
	$trans_id = $trans_id_Qry;
	
	$insert_diary_vou = Qry($conn,"INSERT INTO dairy.cash (trip_id,trans_id,trans_type,tno,vou_no,amount,date,narration,branch,branch_user,
	timestamp) VALUES ('$trip_id','$trans_id','$trans_type','$tno','$tids','$fix_adv_amount','$date','FIX_LANE_ADV','$trip_branch','$trip_branch_user',
	'$timestamp')");

	if(!$insert_diary_vou){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
	
	if($trans_type=='CASH')
	{
		$check_bal = Qry($conn,"SELECT balance,balance2 FROM user WHERE username='$trip_branch'");

		if(!$check_bal){
			$flag = false;
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		}

		if(numRows($check_bal)==0){
			$flag = false;
			errorLog("Branch not found. Branch: $trip_branch.",$conn,$page_name,__LINE__);
		}

		$row_bal = fetchArray($check_bal);

		if($company=='RRPL' && $row_bal['balance']>=$fix_adv_amount)
		{
			$new_cash_bal = $row_bal['balance'] - $fix_adv_amount;
			$cash_debit_col = 'debit';			
			$cash_balance_col = 'balance';
		}
		else if($company=='RAMAN_ROADWAYS' && $row_bal['balance2']>=$fix_adv_amount)
		{
			$new_cash_bal = $row_bal['balance2'] - $fix_adv_amount;
			$cash_debit_col = 'debit2';			
			$cash_balance_col = 'balance2';
		}
		else
		{
			$flag = false;
			errorLog("INSUFFICIENT Balance in Company : $company. Branch: $trip_branch.",$conn,$page_name,__LINE__);
		}
		
		$query_update = Qry($conn,"update user set `$cash_balance_col`='$new_cash_bal' where username='$trip_branch'");
		
		if(!$query_update){
			$flag = false;
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		}
		
		$update_cashbook = Qry($conn,"INSERT INTO cashbook(user,date,vou_date,comp,vou_no,vou_type,desct,`$cash_debit_col`,`$cash_balance_col`,
		timestamp) VALUES ('$trip_branch','$date','$date','$company','$tids','Truck_Voucher','$tno/$driver_name: Fix Lane Advance','$fix_adv_amount',
		'$new_cash_bal','$timestamp')");

		if(!$update_cashbook){
			$flag = false;
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		}
	}
	else if($trans_type=="HAPPAY")
	{
		$get_Card_using = Qry($conn,"SELECT card_using FROM dairy.happay_card WHERE tno='$tno'");
	
		if(!$get_Card_using){
			$flag = false;
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		}

		if(numRows($get_Card_using)==0)
		{
			$flag = false;
			errorLog("Happay card not found: $tno",$conn,$page_name,__LINE__);
		}
		 
		$row_card_using = fetchArray($get_Card_using);

		$card_kit_no = $row_card_using['card_using'];

		if($tno!=$card_kit_no){
			$other_Card_narration="Vehicle_No : $tno. Card Using : $card_kit_no.";
		}else{
			$other_Card_narration="";
		}
	
		$insert_happay = Qry($conn,"INSERT INTO dairy.happay_card_transactions(card_no,card_using,driver_code,narration,trans_id,trans_type,credit,
		branch,date,timestamp) VALUES ('$tno','$card_kit_no','$driver_code','$other_Card_narration','$trans_id','Wallet Credit','$fix_adv_amount',
		'$trip_branch','$date','$timestamp')");
		
		if(!$insert_happay){
			$flag = false;
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		}
		
		$update_happay_main_bal = Qry($conn,"UPDATE dairy.happay_main_balance SET balance=balance-'$fix_adv_amount' WHERE company='$company'");
		
		if(!$update_happay_main_bal){
			$flag = false;
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		}
	}
	else
	{
		$flag = false;
		errorLog("Error : Invalid Transaction Type.",$conn,$page_name,__LINE__);
	}
	
	$insert_vou = Qry($conn,"INSERT INTO mk_tdv(user,branch_user,company,tdvid,date,newdate,truckno,dname,amt,mode,dest,timestamp) VALUES 
	('$trip_branch','$trip_branch_user','$company','$tids','$date','$date','$tno','$driver_name','$fix_adv_amount','$trans_type','FIX_LANE_ADV','$timestamp')");	
		
	if(!$insert_vou){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}	

	$update_trip = Qry($conn,"UPDATE dairy.trip SET cash=cash+'$fix_adv_amount',fix_adv_amount='$fix_adv_amount' WHERE id='$trip_id'");
	
	if(!$update_trip){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}	

	$select_amount = Qry($conn,"SELECT id,amount_hold FROM dairy.driver_up WHERE down=0 AND code='$driver_code' ORDER BY id DESC LIMIT 1");
	
	if(!$select_amount){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}

	$row_amount = fetchArray($select_amount);
	$hold_amt = $row_amount['amount_hold']+$fix_adv_amount;
	$driver_balance_id = $row_amount['id'];

	$insert_d_book = Qry($conn,"INSERT INTO dairy.driver_book (driver_code,tno,trip_id,trip_no,trans_id,desct,credit,balance,date,branch,timestamp) 
	VALUES ('$driver_code','$tno','$trip_id','$trip_no','$trans_id','CASH_ADV','$fix_adv_amount','$hold_amt','$date','$trip_branch','$timestamp')");

	if(!$insert_d_book){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}

	$update_hold_amount = Qry($conn,"UPDATE dairy.driver_up SET amount_hold=amount_hold+$fix_adv_amount WHERE id='$driver_balance_id'");

	if(!$update_hold_amount){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
}

if($diesel_value>0)
{
	// $insert_diesel=Qry($conn,"INSERT INTO dairy._pending_fix_diesel(trip_id,trip_no,tno,rule_id,fix_diesel_type,qty,diesel_left,timestamp) VALUES ('$trip_id',
	// '$trip_no','$tno','$rule_id','$fix_diesel_type','$diesel_value','$diesel_value','$timestamp')");

	// if(!$insert_diesel){
		// $flag = false;
		// errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	// }
	
	$update_dsl_qty = Qry($conn,"UPDATE dairy.own_truck SET diesel_left_fix=diesel_left_fix+'$diesel_value' WHERE tno='$tno'");
	
	if(!$update_dsl_qty){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
	
	$update_dsl_in_trip = Qry($conn,"UPDATE dairy.trip SET fix_dsl_type='$fix_diesel_type',fix_dsl_value='$diesel_value',fix_dsl_left='$diesel_value' WHERE id='$trip_id'");

	if(!$update_dsl_in_trip){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
}
	
if($total_exp>0)
{	
	$get_fix_exps = Qry($conn,"SELECT exp_code,exp_name,exp_type,exp_amount FROM dairy.fix_lane_exp WHERE rule_id='$rule_id'");

	if(!$get_fix_exps){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
		
	if(numRows($get_fix_exps)>0)
	{
		while($row_exp = fetchArray($get_fix_exps))
		{
			if($row_exp['exp_code']=='TR00015')
			{
				if($row_exp['exp_type']=='On_Weight')
				{
					$toll_tax = round($actual_wt*$row_exp['exp_amount']);
				}
				else
				{
					$toll_tax = $row_exp['exp_amount'];
				}
			}
			else
			{
				$toll_tax = 0;
			}
			
			$exp_entry = "NO";
			
			if($row_exp['exp_type']=='On_Weight')
			{
				if($actual_wt>0)
				{
					$exp_amount = round($actual_wt*$row_exp['exp_amount']);
					$exp_entry = "YES";
				}
			}
			else
			{
				$exp_amount = $row_exp['exp_amount'];
				$exp_entry = "YES";
			}

			if($exp_entry=='YES')
			{
				$trans_id_Qry = GetTransId("EXP",$conn,"trip_exp");
					
				if(!$trans_id_Qry || $trans_id_Qry=="" || $trans_id_Qry=="0"){
					$flag = false;
					errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
				}
					
				$trans_id_exp = $trans_id_Qry;
					
				$insert_exp_diary=Qry($conn,"INSERT INTO dairy.trip_exp (trip_id,trans_id,tno,exp_name,exp_code,amount,copy,
				date,narration,branch,branch_user,timestamp) VALUES ('$trip_id','$trans_id_exp','$tno','$row_exp[exp_name]',
				'$row_exp[exp_code]','$exp_amount','','$date','FIX_LANE_EXP','$trip_branch','$trip_branch_user','$timestamp')");

				if(!$insert_exp_diary){
					$flag = false;
					errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
				}
					
				$update_trip_exp = Qry($conn,"UPDATE dairy.trip SET expense=expense+'$exp_amount',toll_tax=toll_tax+'$toll_tax' WHERE id='$trip_id'");
					
				if(!$update_trip_exp){
					$flag = false;
					errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
				}
					
				$select_amount=Qry($conn,"SELECT id,amount_hold FROM dairy.driver_up WHERE down=0 AND code='$driver_code' 
				ORDER BY id DESC LIMIT 1");
				
				if(!$select_amount){	
					$flag = false;
					errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
				}
					
				$row_amount = fetchArray($select_amount);
				$hold_amt = $row_amount['amount_hold']-$exp_amount;
				$driver_id = $row_amount['id'];

				$insert_book=Qry($conn,"INSERT INTO dairy.driver_book(driver_code,tno,trip_id,trip_no,trans_id,desct,debit,
				balance,date,branch,timestamp) VALUES ('$driver_code','$tno','$trip_id','$trip_no','$trans_id_exp',
				'EXP-$row_exp[exp_name]','$exp_amount','$hold_amt','$date','$trip_branch','$timestamp')");

				if(!$insert_book){	
					$flag = false;
					errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
				}
					
				$update_hold_amount=Qry($conn,"UPDATE dairy.driver_up SET amount_hold=amount_hold-$exp_amount WHERE 
				id='$driver_id'");

				if(!$update_hold_amount){	
					$flag = false;
					errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
				}
			}
		}
	}
}

$move_to_db = Qry($conn,"INSERT INTO dairy.fix_txn_db(tno,trip_id,lane_id,rule_id,lr_type,lr_vou_no,branch,branch_user,timestamp,complete_timestamp) 
SELECT tno,trip_id,lane_id,rule_id,lr_type,lr_vou_no,branch,branch_user,timestamp,'$timestamp' FROM dairy.fix_txn_pending WHERE id='$id'"); 

if(!$move_to_db){	
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

$dlt_pending_txn = Qry($conn,"DELETE FROM dairy.fix_txn_pending WHERE id='$id'"); 

if(!$dlt_pending_txn){	
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

if($flag)
{
	MySQLCommit($conn);
	closeConnection($conn);
	AlertRightCornerSuccessFadeFast("OK : Process Complete !");
	echo "<script>
		$('#fix_entry_btn_$id').attr('disabled',true);
		$('#fix_entry_btn_$id').html('Done');
		$('#fix_entry_btn_$id').attr('onclick','');
	</script>";
	exit();
}
else
{
	// if($exp=='RTO'){
		// foreach(explode(",",$file_name) as $uploaded_files){
			// unlink($uploaded_files);
		// }
	// }
		
	MySQLRollBack($conn);
	closeConnection($conn);
	AlertError("Error while processing request !!");
	echo "<script>
		$('#fix_entry_btn_$id').attr('disabled',true);
	</script>";
	exit();
}
?>