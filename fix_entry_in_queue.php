<?php
include "header.php";

$chk = Qry($conn,"SELECT id FROM _access_control WHERE username='$_SESSION[ediary_fix_admin]' AND func_id=(SELECT id FROM 
_access_control_func_list WHERE session_role='8' AND func_name='Fix_Queue') AND u_view='1'");
			  
if(numRows($chk)==0)
{
	echo "<script>window.location.href='./';</script>";
	exit();
}
?>
<div class="content-wrapper">
    <section class="content-header">
      <span style="font-size:15px">Fix entries in queue (adv/exp/diesel) : </span>
	</section>
		
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box"> 
			<div class="box-body">

<div class="row">
<br />
<div class="form-group col-md-12">
   <div class="form-group col-md-12 table-responsive" id="load_table_div"></div>
</div>

  </div>
</div>

</body>
</html>

          </div>
        </div>
	</div>
</section>
</div>    
	
<script type="text/javascript">
function LoadTable()
{
	// $("#loadicon").show();
	jQuery.ajax({
	url: "_load_fix_queue.php",
	data: 'ok=' + 'ok',
	type: "POST",
	success: function(data) {
		$("#load_table_div").html(data);
		$('#example').DataTable({ 
        "destroy": true, //use for reinitialize datatable
    });
	},
	error: function() {}
	});
}

function FixEntry(id)
{
	$('#fix_entry_btn_'+id).attr('disabled',true);
	$("#loadicon").show();
	jQuery.ajax({
	url: "proceed_fix_entry.php",
	data: 'id=' + id,
	type: "POST",
	success: function(data) {
		$("#func_result2").html(data);
	},
	error: function() {}
	});
}
</script>

<script>
LoadTable();
</script>

<div id="func_result2"></div>
<div id="form_submit_result"></div>

<?php
include "footer.php";
?>