<?php
require_once("connect.php");

$id = escapeString($conn,($_POST['id']));

if(empty($id))
{
	AlertRightCornerError("ID not found !");
	exit();
}

$get_lr_bank = Qry($conn,"SELECT branch,from_range,to_range,company,stock,stock_left FROM lr_bank WHERE id='$id'");

if(!$get_lr_bank){
	AlertRightCornerError("Error while processing request !");
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	exit();
}
	
if(numRows($get_lr_bank)==0)
{
	AlertRightCornerError("Stock not found !");
	exit();
}

$row = fetchArray($get_lr_bank);

$from_range = $row['from_range'];
$to_range = $row['to_range'];
$company = $row['company'];
$branch = $row['branch'];

$stock = ($to_range-$from_range)+1;
?>
<button id="modal_lr_bank_edit_btn" style="display:none" data-toggle="modal" data-target="#Modal_LRBankEdit"></button>

<div class="modal fade" id="Modal_LRBankEdit" role="dialog" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
		<div class="modal-content" style="">
			<div class="modal-header bg-primary">
				<span style="font-size:13px">Update LR-Bank : <?php echo "Branch => ".$branch. "  (LR Series : $from_range to $to_range)"; ?> </span>
			</div>
		<div class="modal-body">

<script>
function CalculateNew(to1)
{
	var from1 = $("#from_series_new").val();
	var min_stock = '<?php echo $from_range; ?>';
	var max_stock = '<?php echo $to_range; ?>';
	
	var new_from =  Number(to1)+1;
	var new_to =  Number(max_stock);
	var new_count = Number(new_to-new_from)+1;
	
	$("#total_lrs").val((Number(to1) - Number(from1))+1);
	
	// alert(new_to);
	// alert(new_from);
	$('#from_text').html(new_from);
	$('#to_text').html(new_to+' ('+new_count+')');
	$('#alert_div').show();
}
</script>
		
	<div class="row">
		
		<div class="form-group col-md-6">
			<label>Branch <font color="red"><sup>*</sup></font></label>
			<select style='font-size:12px' name="branch" id="edit_modal_branch" value="<?php echo $branch; ?>" class="form-control" required>
				<option style='font-size:12px' value="">-- Select Branch --</option>
				<?php
						$fetch_branch = Qry($conn,"SELECT username FROM user WHERE role='2' ORDER BY username ASC");
						
						if(!$fetch_branch){
							errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
							Redirect("Error while Processing Request","./");
							exit();
						}
						
						if(numRows($fetch_branch)>0)
						{
							while($row_branch=fetchArray($fetch_branch))
							{
							?>
								<option style='font-size:12px' <?php if($row_branch['username']==$branch) {echo "selected"; } ?> value="<?php echo $row_branch['username']; ?>"><?php echo $row_branch['username']; ?></option>
							<?php
							}
						}
				?>
			</select>
		</div>
		
		<div class="form-group col-md-6">
			<label>Company <font color="red"><sup>*</sup></font></label>
			<input style="font-size:12px" readonly value="<?php echo $company; ?>" type="text" class="form-control" id="lr_bank_comp" />
		</div>
		
		<div class="form-group col-md-6">
			<label>From series <font color="red"><sup>*</sup></font></label>
			<input style="font-size:12px" readonly value="<?php echo $from_range; ?>" type="number" class="form-control" name="from_series" id="from_series_new" />
		</div>
		
		<div class="form-group col-md-6">
			<label>To series <font color="red"><sup>*</sup></font></label>
			<input style="font-size:12px" max="<?php echo $to_range; ?>" oninput="CalculateNew(this.value)" type="number" class="form-control" name="to_series" id="to_series_new" />
		</div>
		
		<div class="form-group col-md-6">
			<label>Total LRs <font color="red"><sup>*</sup></font></label>
			<input style="font-size:12px" type="text" value="<?php echo $stock; ?>" name="total_lrs" id="total_lrs" class="form-control" readonly required>	
		</div>
		
		<div class="form-group col-md-12"></div>
		
		<div class="form-group col-md-12" style="display:none" id="alert_div">
			<div class="alert alert-success" style="font-weight:bold;font-size:12px;">
				Remaining Stock <span id="from_text"></span> to <span id="to_text"></span> will be added to <?php echo $branch; ?>.
			</div>	
		</div>
	 
		<div id="result_edit_modal"></div>
		
		</div>
        </div>

	 <div class="modal-footer">
          <button type="button" onclick="ConfirmEdit()" id="button_update_modal_confirm" class="pull-left btn btn-sm btn-danger">Confirm, Update</button>
          <button type="button" id="close_btn_lr_bank_edit_modal" onclick="$('#edit_button<?php echo $id; ?>').attr('disabled',false)" class="btn btn-sm btn-primary" data-dismiss="modal">Close</button>
	 </div>
	 
      </div>
    </div>
  </div>
 
<script>
function ConfirmEdit()
{
	var branch = $('#edit_modal_branch').val();
	var from_series = $('#from_series_new').val();
	var to_series = $('#to_series_new').val();
	var total_lrs = $('#total_lrs').val();
	var company = $('#lr_bank_comp').val();
	var id = '<?php echo $id; ?>';
	
	if(branch=='' || from_series=='' || to_series=='' || total_lrs=='')
	{
		Swal.fire({icon: 'warning',html: '<font size=\'2\' color=\'black\'>Fill all fields first !</font>',});
	}
	else
	{
		$('#loadicon').show();
		$('#button_update_modal_confirm').attr('disabled',true);
			jQuery.ajax({
			url: "save_edit_lr_bank.php",
			data: 'id=' + id + '&branch=' + branch + '&from_series=' + from_series + '&to_series=' + to_series + '&total_lrs=' + total_lrs + '&company=' + company,
			type: "POST",
			success: function(data) {
				$("#result_edit_modal").html(data);
			},
			error: function() {}
		});
	}
}
	
$('#modal_lr_bank_edit_btn')[0].click();	
$('#loadicon').fadeOut('slow');	
</script> 
