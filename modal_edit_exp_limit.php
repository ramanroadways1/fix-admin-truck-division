<?php
require_once("connect.php");

$id = escapeString($conn,($_POST['id']));

if(empty($id))
{
	AlertRightCornerError("ID not found !");
	exit();
}

$get_type = Qry($conn,"SELECT type,amount,entry_limit FROM dairy.trip_exp_limit WHERE id='$id'");

$row = fetchArray($get_type);

if($row['type']=="1")
{
	$value = $row['amount'];
	$type1 = "Amount";
}
else
{
	$value = $row['entry_limit'];
	$type1 = "Entry";
}
?>
<button id="modal_exp_edit_modal_btn" style="display:none" data-toggle="modal" data-target="#ModalEditExpCap"></button>

<div class="modal fade" id="ModalEditExpCap" role="dialog" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
		<div class="modal-content" style="">
			<div class="modal-header bg-primary">
				<span style="font-size:13px">Update <?php echo $type1;?> Limit :</span>
			</div>
		<div class="modal-body">
		
		<div class="row">
		
		<div class="form-group col-md-12">
			<label><?php echo $type1;?> Limit <font color="red">*</font></label>
			<input min="0" type="number" value="<?php echo $value;?>" id="value11" name="value11" class="form-control" />
		</div>
		
		<div class="form-group col-md-12" id="result_modal_22"></div>
		
		</div>
        </div>
		
	<input type="hidden" id="table_id" value="<?php echo $id; ?>">	
	<input type="hidden" id="type11" value="<?php echo $type1; ?>">	

	 <div class="modal-footer">
          <button type="button" onclick="UpdateExpEdit()" id="button_edit_save_btn" class="pull-left btn btn-sm btn-danger">Update</button>
          <button type="button" id="button_edit_close_btn" class="btn btn-sm btn-primary" data-dismiss="modal">Close</button>
	 </div>
	 
      </div>
    </div>
  </div>
 
<script>
function UpdateExpEdit()
{
	var id = $('#table_id').val();
	var type = $('#type11').val();
	var value1 = $('#value11').val();
	
	$('#loadicon').show();
	$('#button_edit_save_btn').attr('disabled',true);
		jQuery.ajax({
		url: "save_edit_exp_limit.php",
		data: 'id=' + id + '&type=' + type + '&value=' + value1,
		type: "POST",
		success: function(data) {
			$("#result_modal_22").html(data);
		},
		error: function() {}
	});
}
	
$('#modal_exp_edit_modal_btn')[0].click();	
$('#loadicon').fadeOut('slow');	
</script> 
