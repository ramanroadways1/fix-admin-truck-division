<?php
require_once("./connect.php");

$timestamp = date("Y-m-d H:i:s");

$driver_id = escapeString($conn,strtoupper($_POST['driver_id']));
$mobile = trim(escapeString($conn,strtoupper($_POST['mobile1'])));
$mobile2 = trim(escapeString($conn,strtoupper($_POST['mobile2'])));
$driver_code1 = escapeString($conn,strtoupper($_POST['driver_code']));
$driver_name = trim(escapeString($conn,strtoupper($_POST['driver_name2'])));
$guarantor = trim(escapeString($conn,strtoupper($_POST['guarantor'])));
$guarantor_code = escapeString($conn,strtoupper($_POST['guarantor_code']));

if(empty($driver_id))
{
	AlertErrorTopRight("Driver ID not found !");
	echo '<script>$("#submit_btn").attr("disabled", false);</script>';
	exit();
}

if(strlen($mobile) != 10)
{
	AlertErrorTopRight("Check driver mobile number !");
	echo '<script>$("#submit_btn").attr("disabled", false);</script>';
	exit();
}

if(strlen($mobile2) != 10 AND $mobile2!='')
{
	AlertErrorTopRight("Check driver alternate mobile number !");
	echo '<script>$("#submit_btn").attr("disabled", false);</script>';
	exit();
}

if($driver_name=='')
{
	AlertErrorTopRight("Driver name required !");
	echo '<script>$("#submit_btn").attr("disabled", false);</script>';
	exit();
}

if($guarantor=='' || $guarantor_code=='')
{
	AlertErrorTopRight("Guarantor required !");
	echo '<script>$("#submit_btn").attr("disabled", false);</script>';
	exit();
}
	
if(isset($_POST['ac_update']))
{
	$ac_holder = trim(escapeString($conn,strtoupper($_POST['ac_holder'])));
	$ac_no = trim(escapeString($conn,strtoupper($_POST['ac_no'])));
	$bank_name = trim(escapeString($conn,strtoupper($_POST['bank_name'])));
	$ifsc_code = trim(escapeString($conn,strtoupper($_POST['ifsc_code'])));
	
	if($ac_holder=='' || $ac_no=='' || $bank_name=='' || $ifsc_code=='')
	{
		AlertErrorTopRight("Account details required !");
		echo '<script>$("#submit_btn").attr("disabled", false);</script>';
		exit();
	}
}

$chk_guarantor = Qry($conn,"SELECT driver_name,down_date FROM dairy.driver_guarantor WHERE code = '$guarantor_code'");

if(!$chk_guarantor){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	AlertError("Error While Processing Request !");
	exit();
}

if(numRows($chk_guarantor) == 0)
{
	AlertErrorTopRight("Guarantor not found !");
	echo '<script>$("#submit_btn").attr("disabled", false);</script>';
	exit();
}

$row_g = fetchArray($chk_guarantor);

$down_date = $row_g['down_date']; 

if(!empty($down_date))
{
	$diff_days = round((strtotime(date("Y-m-d") - strtotime($down_date))) / (60 * 60 * 24));
	
	if($diff_days > 60)
	{
		AlertErrorTopRight("Guarantor is not active since 60 days !");
		echo '<script>$("#submit_btn").attr("disabled", false);</script>';
		exit();
	}
}

if($row_g['driver_name'] != $guarantor)
{
	AlertErrorTopRight("Guarantor : $guarantor($guarantor_code) not verified !");
	echo '<script>$("#submit_btn").attr("disabled", false);</script>';
	exit();
}
	
$chk_driver = Qry($conn,"SELECT d.code,d.name,d.mobile,d.mobile2,d.guarantor,d_ac.acname,d_ac.acno,d_ac.bank,d_ac.ifsc 
FROM dairy.driver AS d 
LEFT OUTER JOIN dairy.driver_ac AS d_ac ON d_ac.code = d.code 
WHERE d.id = '$driver_id'");

if(!$chk_driver){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	AlertError("Error While Processing Request !");
	exit();
}

if(numRows($chk_driver) == 0)
{
	AlertErrorTopRight("Driver not found !");
	echo '<script>$("#submit_btn").attr("disabled", false);</script>';
	exit();
}

$row = fetchArray($chk_driver);

$driver_code = $row['code'];

if($driver_code1 != $driver_code)
{
	AlertErrorTopRight("Driver not verified !");
	echo '<script>$("#submit_btn").attr("disabled", false);</script>';
	exit();
}

$update_log = array();
$update_Qry = array();

if($driver_name != $row['name'])
{
	$update_log[]="Driver_Name : $row[name] to $driver_name";
	$update_Qry[]="name='$driver_name'";
}

if($mobile != $row['mobile'])
{
	$update_log[]="Driver_Mobile : $row[mobile] to $mobile";
	$update_Qry[]="mobile='$mobile'";
}

if($mobile2 != $row['mobile2'])
{
	$update_log[]="Driver_Alt_Mobile : $row[mobile2] to $mobile2";
	$update_Qry[]="mobile2='$mobile2'";
}

if($guarantor_code != $row['guarantor'])
{
	$update_log[]="Guarantor : $row[guarantor] to $guarantor_code";
	$update_Qry[]="guarantor='$guarantor_code'";
	$update_Qry[]="guarantor_last_active_date='$down_date'";
}

$update_ac_details = "NO";

if(isset($_POST['ac_update']))
{
	if($ac_holder != $row['acname'])
	{
		$update_log[]="Ac_Holder : $row[acname] to $ac_holder";
		$update_ac_details = "YES";
	}
	
	if($ac_no != $row['acno'])
	{
		$update_log[]="Ac_No : $row[acno] to $ac_no";
		$update_ac_details = "YES";
	}
	
	if($bank_name != $row['bank'])
	{
		$update_log[]="Bank : $row[bank] to $bank_name";
		$update_ac_details = "YES";
	}
	
	if($ifsc_code != $row['ifsc'])
	{
		$update_log[]="Ifsc : $row[ifsc] to $ifsc_code";
		$update_ac_details = "YES";
	}
}

$update_log = implode(', ',$update_log); 
$update_Qry = implode(', ',$update_Qry);

if(empty($update_log))
{
	AlertErrorTopRight("Nothing to update !");
	echo '<script>$("#submit_btn").attr("disabled", false);</script>';
	exit();
}

StartCommit($conn);
$flag = true;

$update_Qry = Qry($conn,"UPDATE dairy.driver SET $update_Qry WHERE id='$driver_id'");

if(!$update_Qry){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

if($update_ac_details == "YES")
{
	$update_ac_Qry = Qry($conn,"UPDATE dairy.driver_ac SET acname='$ac_holder',acno='$ac_no',bank='$bank_name',ifsc='$ifsc_code' 
	WHERE code='$driver_code'");

	if(!$update_ac_Qry){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
}

$insert_Log = Qry($conn,"INSERT INTO dairy.ediary_admin_log(code,action,desct,timestamp) VALUES ('$driver_id','Driver_Edit','$update_log','$timestamp')");

if(!$insert_Log){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

if($flag)
{
	MySQLCommit($conn);
	closeConnection($conn);
	
	AlertRightCornerSuccessFadeFast("Updated Success !");
	echo '<script>$("#submit_btn").attr("disabled", false);</script>';
	exit();
}
else
{
	MySQLRollBack($conn);
	closeConnection($conn);

	AlertError("Error While Processing Request !");
	echo '<script>$("#submit_btn").attr("disabled", false);</script>';
	exit();
}
?>