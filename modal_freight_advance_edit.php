<?php
require_once("connect.php");

$id = escapeString($conn,$_POST['id']);

$get_data = Qry($conn,"SELECT trip_id,trans_id,vou_id,tno,amount,date,branch,narration,timestamp FROM dairy.freight_adv WHERE id='$id'");

if(!$get_data)
{
	AlertErrorTopRight("Error while processing request !");
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	exit();
}

if(numRows($get_data) == 0)
{
	AlertErrorTopRight("Entry not found !");
	exit();
}

$row = fetchArray($get_data);

$tno = $row['tno'];
$trip_id = $row['trip_id'];
$trans_id = $row['trans_id'];
$amount = $row['amount'];
$vou_id = $row['vou_id'];
$trans_date = $row['date'];
$narration = $row['narration'];

?>

<button id="freight_to_driver" style="display:none" data-toggle="modal" data-target="#FreightToDriver"></button>

<form id="FrtAdvFormEdit" autocomplete="off" style="font-size:12px !important">   
<div class="modal fade" id="FreightToDriver" role="dialog" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-md">
      <div class="modal-content" style="">
	  
	  <div class="modal-header bg-primary">
			Edit Freight to Driver :
		  </div>
		  
		<div class="modal-body">
		<div class="row">
			
			<div class="form-group col-md-6">
				<label>Vehicle No. <sup><font color="red">*</font></sup></label>
				<input style="font-size:12px !important" type="text" name="tno" value="<?php echo $tno; ?>" class="form-control" readonly required />
			</div>
			
			<div class="form-group col-md-6">
				<label>Amount. <sup><font color="red">*</font></sup></label>
				<input style="font-size:12px !important" type="number" min="1" value="<?php echo $amount; ?>" name="amount" class="form-control" required />
			</div>
			
			<input type="hidden" name="id" value="<?php echo $id; ?>" />
			
			<div class="form-group col-md-12">
				<label>Narration. <sup><font color="red">*</font></sup></label>
				<textarea style="font-size:12px !important" class="form-control" name="narration" required><?php echo $narration; ?></textarea>
			</div>
		</div>
        </div>
        <div class="modal-footer">
          <button type="submit" id="f_to_d_button_edit" class="btn btn-sm btn-danger">Update</button>
          <button type="button" class="btn btn-sm btn-primary" onclick="ReLoadPage();" data-dismiss="modal">Close</button>
		</div>
      </div>
      </form>
    </div>
  </div>
  
 <script> 
$('#freight_to_driver')[0].click();
$('#loadicon').fadeOut('slow');
</script>  
  
<script type="text/javascript">
$(document).ready(function (e) {
$("#FrtAdvFormEdit").on('submit',(function(e) {
$("#loadicon").show();
$("#f_to_d_button_edit").attr("disabled", true);
e.preventDefault();
	$.ajax({
	url: "./save_freight_advance_edit.php",
	type: "POST",
	data:  new FormData(this),
	contentType: false,
	cache: false,
	processData:false,
	success: function(data){
		$("#result_edits").html(data);
	},
	error: function() 
	{} });}));});
</script> 