<?php
require_once("connect.php");

$id = escapeString($conn,$_POST['id']);
$date = date("Y-m-d");

$get_data = Qry($conn,"SELECT date,advbal,trans_value,narration FROM dairy.bilty_book WHERE id='$id'");

if(!$get_data)
{
	AlertErrorTopRight("Error while processing request !");
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	exit();
}

if(numRows($get_data) == 0)
{
	AlertErrorTopRight("LR not found !");
	exit();
}

$row = fetchArray($get_data);

?>
<button id="modal_open_btn" style="display:none" data-toggle="modal" data-target="#EditModal"></button>

<script type="text/javascript">
$(document).ready(function (e) {
$("#EditMbEntryForm").on('submit',(function(e) {
$("#loadicon").show();
$("#edit_modal_btn").attr("disabled", true);
e.preventDefault();
	$.ajax({
	url: "./save_market_bilty_txn_edit.php",
	type: "POST",
	data:  new FormData(this),
	contentType: false,
	cache: false,
	processData:false,
	success: function(data){
		$("#result_edits").html(data);
	},
	error: function() 
	{} });}));});
</script>   

<form id="EditMbEntryForm" autocomplete="off" style="font-size:12px !important">   
<div class="modal fade" id="EditModal" role="dialog" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-md">
      <div class="modal-content" style="">
		
		<div class="modal-header bg-primary">
			Edit Market Bilty Txn
		  </div>
	  
		<div class="modal-body">
		<div class="row">
			
		<div class="form-group col-md-6">
			<label>Txn Date. <sup><font color="red">*</font></sup></label>
			<input style="font-size:12px !important" value="<?php echo $row['date']; ?>" type="date" name="txn_date" max="<?php echo date("Y-m-d"); ?>" required 
			pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}" class="form-control" />
		</div>	
		
		<div class="form-group col-md-6">
			<label>Adv/Bal <sup><font color="red">*</font></sup></label>
			<select style="font-size:12px !important" name="adv_bal" class="form-control" required>
				<option <?php if($row['advbal']=='ADVANCE') { echo "selected"; } ?> style="font-size:12px !important" value="SHIVANI">ADVANCE</option>
				<option <?php if($row['advbal']=='BALANCE') { echo "selected"; } ?> style="font-size:12px !important" value="BALANCE">BALANCE</option>
				<option <?php if($row['advbal']=='') { echo "selected"; } ?> style="font-size:12px !important" value="">NULL</option>
			</select>
		</div>
		
		<div class="form-group col-md-6">
			<label>Txn Value <sup><font color="red">*</font></sup></label>
			<input style="font-size:12px !important" value="<?php echo $row['trans_value']; ?>" type="text" name="txn_value" class="form-control" required />
		</div>	
		
		<div class="form-group col-md-6">
			<label>Narration <sup><font color="red">*</font></sup></label>
			<textarea style="font-size:12px !important" name="narration" class="form-control" required><?php echo $row['narration']; ?></textarea>
		</div>	
		
		</div>
        </div>
		
		<input type="hidden" name="id" value="<?php echo $id; ?>">
		
		<div id="result_edits"></div>
		
        <div class="modal-footer">
          <button type="submit" id="edit_modal_btn" class="btn btn-sm btn-danger">Update</button>
          <button type="button" class="btn-sm btn btn-primary" id="modal_edit_close_btn" data-dismiss="modal">Close</button>
		</div>
      </div>
    </div>
  </div>
 </form>
 
<script> 
$('#modal_open_btn')[0].click();
$('#loadicon').fadeOut('slow');
</script> 