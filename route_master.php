<?php
include "header.php";

$chk = Qry($conn,"SELECT id FROM _access_control WHERE username='$_SESSION[ediary_fix_admin]' AND func_id=(SELECT id FROM 
_access_control_func_list WHERE session_role='8' AND func_name='Route_Master') AND u_view='1'");
			  
if(numRows($chk)==0)
{
	echo "<script>window.location.href='./';</script>";
	exit();
}
?>
<script>
$(function() {
		$("#from_location").autocomplete({
		source: '../b5aY6EZzK52NA8F/autofill/get_location.php',
		// appendTo: '#appenddiv',
		select: function (event, ui) { 
            $('#from_location').val(ui.item.value);   
            $('#from_location_id').val(ui.item.id);      
            return false;},
		change: function (event, ui) {
		if(!ui.item){
			$(event.target).val("");
            $(event.target).focus();
			$('#from_location').val("");   
			$('#from_location_id').val("");   
			alert('Location does not exists.');
		}}, 
	focus: function (event, ui){
	return false;
	}
});});

$(function() {
		$("#to_location").autocomplete({
		source: '../b5aY6EZzK52NA8F/autofill/get_location.php',
		// appendTo: '#appenddiv',
		select: function (event, ui) { 
            $('#to_location').val(ui.item.value);   
            $('#to_location_id').val(ui.item.id);      
            return false;},
		change: function (event, ui) {
		if(!ui.item){
			$(event.target).val("");
            $(event.target).focus();
			$('#to_location').val("");   
			$('#to_location_id').val("");   
			alert('Location does not exists.');
		}}, 
	focus: function (event, ui){
	return false;
	}
});});
</script>

<div class="content-wrapper">
    <section class="content-header">
      <span style="font-size:15px">Route Master: </span>
	</section>
		
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box"> 
			<div class="box-body">

<div class="row">
 
 <div class="form-group col-md-12">

<?php
$qry = Qry($conn,"SELECT id FROM _access_control WHERE username='$_SESSION[ediary_fix_admin]' AND func_id=(SELECT id FROM 
_access_control_func_list WHERE session_role='8' AND func_name='Route_Master') AND u_insert='1'");
			  
if(numRows($qry)>0)
{
?>  
  <br />
	<div class="form-group col-md-3">
		<label>From Location</label>
		<input type="text" id="from_location" oninput="this.value=this.value.replace(/[^a-z A-Z.-]/,'')" class="form_route_master form-control" required="required">
	</div>
	
	<div class="form-group col-md-3">
		<label>To Location</label>
		<input type="text" id="to_location" oninput="this.value=this.value.replace(/[^a-z A-Z.-]/,'')" class="form_route_master form-control" required="required">
	</div>
	
	<div class="form-group col-md-3">
		<label>Route Type</label>
		<select style="font-size:12px" name="route_type" id="route_type" class="form_route_master form-control" required="required">
			<option style="font-size:12px" value="">--select--</option>
			<option style="font-size:12px" value="normal">Normal</option>
			<option style="font-size:12px" value="congested">Congested/Ghaat</option>
		</select>
	</div>
	
	<div class="form-group col-md-3">
		<label>&nbsp;</label>
		<?php
		if(!isMobile()){
			echo "<br>";
		}
		?>
		<button type="button" onclick="AddRoute()" id="add_btn" class="btn-sm btn btn-success">Add Route</button>
	</div>
<?php
}
?>	
	<div id="func_result2"></div>
	<input type="hidden" name="from_id" id="from_location_id">
	<input type="hidden" name="to_id" id="to_location_id">
	
	<div class="form-group col-md-12 table-responsive" id="load_table_div"></div>
</div>

  </div>
</div>

</body>
</html>

          </div>
        </div>
	</div>
</section>
</div>    
	
<script type="text/javascript">
function AddRoute()
{
	var from_id = $('#from_location_id').val();
	var to_id = $('#to_location_id').val();
	var route_type = $('#route_type').val();
	var from_loc = $('#from_location').val();
	var to_loc = $('#to_location').val();
	
	if(from_id=='' || to_id=='' || route_type=='')
	{
		Swal.fire({icon: 'warning',html: '<font size=\'2\' color=\'black\'>Enter location and route type first !</font>',});
	}
	else
	{
		$('#add_btn').attr('disabled',true);
		$("#loadicon").show();
			jQuery.ajax({
			url: "./route_master_save.php",
			data: 'from_id=' + from_id + '&to_id=' + to_id + '&route_type=' + route_type + '&from_loc=' + from_loc + '&to_loc=' + to_loc,
			type: "POST",
			success: function(data) {
				$("#func_result2").html(data);
			},
			error: function() {}
		});
	}
}

function LoadTable()
{
	// $("#loadicon").show();
	jQuery.ajax({
	url: "_load_route_master.php",
	data: 'ok=' + 'ok',
	type: "POST",
	success: function(data) {
		$("#load_table_div").html(data);
		$('#example').DataTable({ 
        "destroy": true, //use for reinitialize datatable
    });
	},
	error: function() {}
	});
}
</script>

<script>
LoadTable();
</script>

<?php
include "footer.php";
?>