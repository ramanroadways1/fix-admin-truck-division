<?php
require_once("connect.php");

$tno = escapeString($conn,$_POST['veh_no']);

?>

			<table id="example" class="table table-bordered table-striped" style="font-size:13px">
                    <thead>
                      <tr>
                        <th>#</th>
                        <th>#View</th>
						<th>#Active</th>
                        <th>Branch & user</th>
                        <th>From</th>
                        <th>To</th>
                        <th>Weight</th>
                        <th>LR_No</th>
                        <th>Trip_Date</th>
                        <th>End_Date</th>
                        <th>#Edit</th>
                        <th>#Delete</th>
                      </tr>
                    </thead>
                    <tbody>
	<?php
	$get_approvals = Qry($conn,"SELECT t.id,t.branch,t.from_station,t.to_station,t.act_wt,t.charge_wt,t.lr_type,t.lrno,t.date as trip_date,t.end_date 
	AS trip_end_date,e.name as trip_user 
	FROM dairy.trip AS t 
	LEFT OUTER JOIN emp_attendance AS e ON e.code = t.branch_user 
	WHERE t.tno = '$tno'");
	
	if(!$get_approvals){
		AlertErrorTopRight("Error while processing request !");
		echo getMySQLError($conn);
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		exit();
	}
	
	if(numRows($get_approvals)==0)
	{
		echo "<tr>
			<td colspan='12'>No record found !</td>
			<td style='display: none'></td>
			<td style='display: none'></td>
			<td style='display: none'></td>
			<td style='display: none'></td>
			<td style='display: none'></td>
			<td style='display: none'></td>
			<td style='display: none'></td>
			<td style='display: none'></td>
			<td style='display: none'></td>
			<td style='display: none'></td>
			<td style='display: none'></td>
			<td style='display: none'></td>
		</tr>";
	}
	else
	{
		$i=1;
		while($row = fetchArray($get_approvals))
		{
			// $timestamp = date("d-m-y h:i A",strtotime($row['timestamp']));
			
			$trip_date = date("d-m-y",strtotime($row['trip_date']));
			$end_date = date("d-m-y",strtotime($row['trip_end_date']));
			
			echo "<tr>
				<td>$i</td>
				<td><button type='button' onclick='ViewTrip($row[id])' class='btn btn-xs btn-primary'><i class='fa fa-street-view' aria-hidden='true'></i> View</button></td>
				<td><button id='active_button_$row[id]' class='active_trip_btn btn btn-success btn-xs' type='button' onclick=ActiveTrip('$row[id]')><i class='fa fa-check-square-o' aria-hidden='true'></i> Active</button></td>
				<td>$row[branch]<br>($row[trip_user])</td>
				<td>$row[from_station]</td>
				<td>$row[to_station]</td>
				<td>Actual: $row[act_wt]<br>Charge: $row[charge_wt]</td>
				<td>$row[lr_type]<br>$row[lrno]</td>
				<td>$trip_date</td>
				<td>$end_date</td>
				<td><button id='edit_button_$row[id]' class='edit_btn btn btn-default btn-xs' type='button' onclick=Edit('$row[id]')><i class='fa fa-edit' aria-hidden='true'></i> Edit</button></td>
				<td><button id='del_button_$row[id]' class='delete_btn btn btn-danger btn-xs' type='button' onclick=Delete('$row[id]')><i class='fa fa-trash' aria-hidden='true'></i> Delete</button></td>
			</tr>";
		$i++;	
		}
	}
	?>	
                    </tbody>
                  </table>

<script>				  
$("#loadicon").fadeOut('slow');
</script>		

<?php
$chk_update = Qry($conn,"SELECT id FROM _access_control WHERE username='$_SESSION[ediary_fix_admin]' AND func_id=(SELECT id FROM 
_access_control_func_list WHERE session_role='8' AND func_name='Edit_Trip') AND u_update='1'");
			  
if(numRows($chk_update)>0)
{
	echo "<script>
		$('.edit_btn').attr('disabled',false);
		$('.active_trip_btn').attr('disabled',false);
	</script>";	
}
else
{
	echo "<script>
		$('.edit_btn').attr('disabled',true);
		$('.edit_btn').attr('onclick','');
		$('.active_trip_btn').attr('onclick','');
	</script>";	
	
}

$chk_delete = Qry($conn,"SELECT id FROM _access_control WHERE username='$_SESSION[ediary_fix_admin]' AND func_id=(SELECT id FROM 
_access_control_func_list WHERE session_role='8' AND func_name='Edit_Trip') AND u_delete='1'");
			  
if(numRows($chk_delete)>0)
{
	echo "<script>
		$('.delete_btn').attr('disabled',false);
	</script>";	
}
else
{
	echo "<script>
		$('.delete_btn').attr('disabled',true);
		$('.delete_btn').attr('onclick','');
	</script>";	
}
?>		  