<?php
include("header.php");

$chk = Qry($conn,"SELECT id FROM _access_control WHERE username='$_SESSION[ediary_fix_admin]' AND func_id=(SELECT id FROM 
_access_control_func_list WHERE session_role='8' AND func_name='Trip_Diesel_Entry') AND u_view='1'");
			  
if(numRows($chk)==0)
{
	echo "<script>window.location.href='./';</script>";
	exit();
}
?>

<script>
$(function() {
		$("#own_tno").autocomplete({
		source: '../diary/autofill/own_tno.php',
		// appendTo: '#appenddiv',
		select: function (event, ui) { 
            $('#own_tno').val(ui.item.value);   
            return false;},
		change: function (event, ui) {
		if(!ui.item){
			$(event.target).val("");
            $(event.target).focus();
			$('#own_tno').val("");   
			alert('Vehicle does not exists.');
		}}, 
	focus: function (event, ui){
	return false;
	}
});});
</script>

<div class="content-wrapper">
      <section class="content-header">
          <h1 style="font-size:16px;">Diesel ENTRY </h1>
       </section>
       
	   <section class="content">
          <div class="row">
            <div class="col-xs-12">
			<div class="box">
                <div class="box-body">
				
				<div class="col-md-12">&nbsp;</div>
				
				<div class="col-md-12">
					<div class="row">
						
						<div class="form-group col-md-3">
							<label style="font-size:12px !important">Vehicle Number <font color="red"><sup>*</sup></font></label>
							<input type="text" oninput="this.value=this.value.replace(/[^a-z 0-9A-Z.-]/,'');" class="form-control" style="font-size:12px !important;text-transform:uppercase" required="required" id="own_tno" />
						</div>
						
						<div class="form-group col-md-3">
							<label style="font-size:12px !important">Trip Type <font color="red"><sup>*</sup></font></label>
							<select name="trip_type" class="form-control" onchange="TripType(this.value)" style="font-size:12px !important;" required="required" id="trip_type">
								<option value="RUNNING">RUNNING</option>
								<option value="COMPLETED">COMPLETED</option>
							</select>
						</div>
						
						<script>
						function TripType(elem)
						{
							$('#trip_no').val('');
							
							if(elem=='COMPLETED')
							{
								$('#trip_no_div').show();
							}
							else
							{
								$('#trip_no_div').hide();
							}
						}
						</script>
						
						<div class="form-group col-md-3" style="display:none" id="trip_no_div">
							<label style="font-size:12px !important">Trip Number <font color="red"><sup>*</sup></font></label>
							<input type="text" oninput="this.value=this.value.replace(/[^a-z 0-9A-Z.-]/,'');" class="form-control" style="font-size:12px !important;text-transform:uppercase" required="required" id="trip_no" />
						</div>
						
						<div class="form-group col-md-2">
							<?php if(!isMobile()) { echo "<label>&nbsp;</label><br />"; } ?>
							<button type="button" style="margin-top:1px;font-size:12px !important" onclick="Search()" class="btn btn-sm btn-success <?php if(isMobile()) { echo "btn-block"; } ?>" id="add_btn"><i class="fa fa-search" aria-hidden="true"></i> &nbsp; Search Trips</button>
						</div>
					</div>
				</div>
				
				<div class="col-md-12">&nbsp;</div>
				
				<div class="col-md-12 table-responsive" id="load_table">
              
				 </div> 
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

<div id="func_result"></div>  
<div id="modal_res1"></div>  

<script>	
function Search()
{
	var tno = $('#own_tno').val();
	var trip_type = $('#trip_type').val();
	var trip_no = $('#trip_no').val();
	
	if(tno=='' || trip_type=='')
	{
		Swal.fire({icon: 'warning',html: '<font size=\'2\' color=\'black\'>Enter vehicle number and select trip type first !</font>',});
	}
	else
	{
		if(trip_type=='COMPLETED' && trip_no=='')
		{
			Swal.fire({icon: 'warning',html: '<font size=\'2\' color=\'black\'>Enter trip number first !</font>',});
		}
		else
		{
			$('#loadicon').show();
				jQuery.ajax({
					url: "_load_trips_for_diesel_entry.php",
					data: 'tno=' + tno + '&trip_type=' + trip_type + '&trip_no=' + trip_no,
					type: "POST",
					success: function(data) {
						$("#load_table").html(data);
						$('#example').DataTable({ 
						"lengthMenu": [ [50, 100, 500, -1], [50, 100,500, "All"] ], 
							"destroy": true, //use for reinitialize datatable
						});
					},
					error: function() {}
			});
		}
	}
}

function AddDiesel(trip_id,trip_type)
{
	var from_loc = $('#from_loc_'+trip_id).val();
	var to_loc = $('#to_loc_'+trip_id).val();
	
	$('#trip_locations').html(from_loc+" to "+to_loc);
	$('#modal_trip_id').val(trip_id);
	$('#modal_trip_type').val(trip_type);
	
	$('#modal_open_btn')[0].click();
	
	// $('#add_btn').attr('disabled',true);
		// jQuery.ajax({
			// url: "save_add_new_pump.php",
			// data: 'pump_name=' + pump_name + '&pump_code=' + pump_code + '&branch=' + branch + '&fuel_company=' + fuel_company + '&type=' + 'MARKET',
			// type: "POST",
			// success: function(data) {
				// $("#func_result").html(data);
			// },
			// error: function() {}
		// });
	// }
}
</script>

<button id="modal_open_btn" style="display:none" data-toggle="modal" data-target="#EntryModal"></button>

<script type="text/javascript">
$(document).ready(function (e) {
$("#Form1").on('submit',(function(e) {
$("#loadicon").show();
$("#entry_save_btn").attr("disabled", true);
e.preventDefault();
	$.ajax({
	url: "./save_diesel_entry.php",
	type: "POST",
	data:  new FormData(this),
	contentType: false,
	cache: false,
	processData:false,
	success: function(data){
		$("#result_edits").html(data);
	},
	error: function() 
	{} });}));});
</script>   

<script type="text/javascript">
                     function sum1() 
					 {
						if($("#qty").val()=='')
						{
							$("#qty").val('0');
						}
						
						if($("#rate").val()=='')
						{
							$("#rate").val('0');
						}
							
						$("#dsl_amt").val(Math.round(Number($("#qty").val()) * Number($("#rate").val())).toFixed(2));
					}
					
					function sum2() 
					{
						if($('#pump_name').val()=='')
						{
							$("#qty").val('');
							$("#pump_name").focus();
						}
						else
						{
							if($("#qty").val()=='')
							{
								$("#qty").val('0');
							}
							
							if($("#rate").val()=='')
							{
								$("#rate").val('0');
							}
								
							$("#dsl_amt").val(Math.round(Number($("#qty").val()) * Number($("#rate").val())).toFixed(2));
						}
					}
					 
					 function sum3() 
					 {
						if($("#qty").val()=='')
						{
							$("#qty").val('0');
						}
						
						if($("#rate").val()=='')
						{
							$("#rate").val('0');
						}
						
						if($("#dsl_amt").val()=='')
						{
							$("#dsl_amt").val('0');
						}
						
						$("#rate").val((Number($("#dsl_amt").val()) / Number($("#qty").val())).toFixed(2));
					}
			</script>
			
<script>	
function FetchPump(branch)
{		
	$('#pump_name').html('<option style="font-size:12px !important" value="">select pump</option>');
	$('#entry_save_btn').attr('disabled',true);
	
	if(branch!='')
	{
		$("#loadicon").show();
		  jQuery.ajax({
           url: "./get_pump_branch_wise.php",
			data: 'branch=' + branch,
           type: "POST",
           success: function(data) {
           $("#pump_name").html(data);
           // $("#loadicon").hide();
           },
           error: function() {}
         });
	}
}

function CheckConsumerPump(code)
{
	$("#loadicon").show();
	$.ajax({
	url: "get_consumer_pump_data.php",
	method: "post",
	data:'code=' + code, 
	success: function(data){
		$("#resultChkPump").html(data);
	}})
}
</script>			

<div id="get_pump_name"></div>
<div id="resultChkPump"></div>
			
<form id="Form1" autocomplete="off" style="font-size:12px !important">   
<div class="modal fade" id="EntryModal" role="dialog" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
      <div class="modal-content" style="">
		
		<div class="modal-header bg-primary">
			Diesel ENTRY : Trip --> <span id="trip_locations"></span>
		  </div>
	  
		<div class="modal-body">
			<div class="row">
				
				<div class="form-group col-md-6">
					<label>Select Branch. <sup><font color="red">*</font></sup></label>
					<select onchange="FetchPump(this.value);$('#pump_name').val('')" style="font-size:12px !important" name="branch" class="form-control" required>
						<option style="font-size:12px !important" value="">select branch</option>
						<?php
						$fetch_branches = Qry($conn,"SELECT username FROM user WHERE role='2' AND username NOT IN('HEAD','DUMMY') ORDER BY username ASC");
								
						if(numRows($fetch_branches)>0)
						{
							while($row_b=fetchArray($fetch_branches))
							{
							?>
								<option style="font-size:12px !important" value="<?php echo $row_b["username"]; ?>"><?php echo $row_b["username"]; ?></option>
							<?php
							}					
						}
						?>
					</select>
				</div>
				
				<div class="form-group col-md-6">
					<label>Pump Name. <sup><font color="red">*</font></sup></label>
					<select onchange="CheckConsumerPump(this.value)" style="font-size:12px !important" name="pump_name" id="pump_name" class="form-control" required>
						<option style="font-size:12px !important" value="">select pump</option>
					</select>
				</div>
				
				<div class="form-group col-md-6"> 
					<label>Qty. <sup><font color="red">*</font></sup></label>
					<input type="number" oninput="sum2();" max="600" step="any" min="1" id="qty" name="qty" class="form-control" required />
				</div>
				
				<div class="form-group col-md-6"> 
					<label>Rate. <sup><font color="red">*</font></sup></label>
					<input type="number" oninput="sum1();" max="150" step="any" min="1" id="rate" name="rate" class="form-control" required />
				</div>
				
				<div class="form-group col-md-6"> 
					<label>Amount. <sup><font color="red">*</font></sup></label>
					<input min="40" oninput="sum3();" type="number" max="60000" id="dsl_amt" min="1" name="amount" class="form-control" required />
				</div>
				
				<div class="form-group col-md-6">
					<label>Txn Date. <sup><font color="red">*</font></sup></label>
					<input name="txn_date" max="<?php echo date("Y-m-d"); ?>" min="<?php echo date('Y-m-d', strtotime('-7 days')); ?>" type="date" class="form-control" required pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}" />
				</div>
				
			</div>
		</div>
		
		<div id="result_edits"></div>
		
		<input type="hidden" name="trip_id" id="modal_trip_id">
		<input type="hidden" name="trip_type" id="modal_trip_type">
		<input type="hidden" name="is_consumer_pump_entry" id="is_consumer_pump_entry">
        <div class="modal-footer">
          <button type="submit" id="entry_save_btn" class="btn btn-sm btn-danger">Save</button>
          <button type="button" class="btn-sm btn btn-primary" id="modal_close_btn" data-dismiss="modal">Close</button>
		</div>
      </div>
    </div>
  </div>
 </form>

<?php include("footer.php") ?>