<?php
require_once("connect.php");

$id = escapeString($conn,$_POST['id']);

$get_data = Qry($conn,"SELECT trip_id,trans_id,tno,claim_vou_no,naame_type,qty,rate,amount,date,branch,narration,timestamp 
FROM dairy.driver_naame WHERE id='$id'");

if(!$get_data)
{
	AlertErrorTopRight("Error while processing request !");
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	exit();
}

if(numRows($get_data) == 0)
{
	AlertErrorTopRight("Entry not found !");
	exit();
}

$row = fetchArray($get_data);

$tno = $row['tno'];
$trip_id = $row['trip_id'];
$trans_id = $row['trans_id'];
$qty = $row['qty'];
$rate = $row['rate'];
$amount = $row['amount'];
$claim_vou_no = $row['claim_vou_no'];
$naame_type = $row['naame_type'];
$trans_date = $row['date'];
$narration = $row['narration'];
?>

<button id="modal_driver_naame" style="display:none" data-toggle="modal" data-target="#NaameModal"></button>

<form id="NaameEditForm" autocomplete="off" style="font-size:12px !important"> 
<div class="modal fade" id="NaameModal" role="dialog" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-md">
      <div class="modal-content" style="">
	  
	  <div class="modal-header bg-primary">
			Edit Driver Naame :
		  </div>
		  
		<div class="modal-body">
		<div class="row">
			
			<div class="form-group col-md-4">
				<label>Vehicle No. <sup><font color="red">*</font></sup></label>
				<input style="font-size:12px !important" type="text" name="tno" value="<?php echo $tno; ?>" class="form-control" readonly required />
			</div>
			
			<div class="form-group col-md-4">
				<label>NAAME Type. <sup><font color="red">*</font></sup></label>
				<select style="font-size:12px !important" name="naame_option" id="naame_option" onchange="NaameOpt(this.value)" class="form-control" required>
					<option style="font-size:12px !important" <?php if($naame_type=='DIESEL') { echo "selected"; } ?> value="DIESEL">DIESEL NAAME</option>
					<option style="font-size:12px !important" <?php if($naame_type=='HAPPAY_CARD_PENALTY') { echo "selected"; } ?> value="HAPPAY_CARD_PENALTY">Happay Card Penalty</option>
					<option style="font-size:12px !important" <?php if($naame_type=='TRISHUL_CARD_PENALTY') { echo "selected"; } ?> value="TRISHUL_CARD_PENALTY">Trishul Card Penalty</option>
					<option style="font-size:12px !important" <?php if($naame_type=='CLAIM') { echo "selected"; } ?> disabled value="CLAIM">Claim</option>
					<option style="font-size:12px !important" <?php if($naame_type=='FREIGHT') { echo "selected"; } ?> disabled value="FREIGHT">Freight Naame</option>
					<option style="font-size:12px !important" <?php if($naame_type=='OTHER') { echo "selected"; } ?> value="OTHER">OTHER NAAME</option>
				</select>
			</div>
			
			<script>
			function NaameOpt(elem)
			{
				if(elem=='DIESEL')
				{
					$('#qty_div').show();
					$('#rate_div').show();
					
					$('#naame_qty').attr('required',true);
					$('#naame_rate').attr('required',true);
					$('#naame_qty').attr('min','1');
					$('#naame_rate').attr('min','1');
					$('#naame_amount').attr('oninput','sum3Naame()');
					$('#naame_qty').attr('oninput','sum2Naame()');
					$('#naame_rate').attr('oninput','sum1Naame()');
				}
				else
				{
					$('#qty_div').hide();
					$('#rate_div').hide();
					
					$('#naame_qty').attr('required',false);
					$('#naame_rate').attr('required',false);
					$('#naame_qty').attr('min','0');
					$('#naame_rate').attr('min','0');
					$('#naame_amount').attr('oninput','');
					$('#naame_qty').attr('oninput','');
					$('#naame_rate').attr('oninput','');
				}
			}
			
			
					function sum1Naame() 
					 {
						if($("#naame_qty").val()=='')
						{
							$("#naame_qty").val('0');
						}
						
						if($("#naame_rate").val()=='')
						{
							$("#naame_rate").val('0');
						}
							
						$("#naame_amount").val(Math.round(Number($("#naame_qty").val()) * Number($("#naame_rate").val())).toFixed(2));
					}
					
					function sum2Naame() 
					{
						if($("#naame_qty").val()=='')
						{
							$("#naame_qty").val('0');
						}
						
						if($("#naame_rate").val()=='')
						{
							$("#naame_rate").val('0');
						}
							
						$("#naame_amount").val(Math.round(Number($("#naame_qty").val()) * Number($("#naame_rate").val())).toFixed(2));
					}
					 
					 function sum3Naame() 
					 {
						if($("#naame_qty").val()=='')
						{
							$("#naame_qty").val('0');
						}
						
						if($("#naame_rate").val()=='')
						{
							$("#naame_rate").val('0');
						}
						
						if($("#naame_amount").val()=='')
						{
							$("#naame_amount").val('0');
						}
						
						$("#naame_rate").val((Number($("#naame_amount").val()) / Number($("#naame_qty").val())).toFixed(2));
					}
			</script>
			
			<div id="qty_div" class="form-group col-md-4">
				<label>QTY. <sup><font color="red">*</font></sup></label>
				<input style="font-size:12px !important" value="<?php echo $qty; ?>" type="number" step="any" max="500" min="1" id="naame_qty" oninput="sum2Naame();" name="naame_qty" class="form-control" />
			</div>
			
			<div id="rate_div" class="form-group col-md-4">
				<label>Rate. <sup><font color="red">*</font></sup></label>
				<input style="font-size:12px !important"value="<?php echo $rate; ?>" type="number" step="any" max="90" min="1" id="naame_rate" oninput="sum1Naame();" name="naame_rate" class="form-control" />
			</div>
			
			<div class="form-group col-md-4">
				<label>Amount. <sup><font color="red">*</font></sup></label>
				<input style="font-size:12px !important"value="<?php echo $amount; ?>" type="number" min="1" oninput="sum3Naame();" id="naame_amount" name="naame_amount" class="form-control" required />
			</div>
			
			<input type="hidden" name="id" value="<?php echo $id; ?>" />
			
			<div class="form-group col-md-12">
				<label>Narration. <sup><font color="red">*</font></sup></label>
				<textarea style="font-size:12px !important" class="form-control" name="narration" required><?php echo $narration; ?></textarea>
			</div>
		</div>
        </div>
        <div class="modal-footer">
          <button type="submit" id="naame_edit_button" class="btn btn-sm btn-danger">Update</button>
          <button type="button" id="naame_modal_hide" onclick="ReLoadPage();" class="btn btn-sm btn-primary" data-dismiss="modal">Close</button>
		</div>
      </div>
    </div>
  </div>
</form> 

<script> 
NaameOpt('<?php echo $naame_type; ?>'); 
$('#modal_driver_naame')[0].click();
$('#loadicon').fadeOut('slow');
</script> 
  
 <script type="text/javascript">
$(document).ready(function (e) {
$("#NaameEditForm").on('submit',(function(e) {
$("#loadicon").show();
$("#naame_edit_button").attr("disabled", true);
e.preventDefault();
	$.ajax({
	url: "./save_driver_naame_edit.php",
	type: "POST",
	data:  new FormData(this),
	contentType: false,
	cache: false,
	processData:false,
	success: function(data)
	{
		$("#result_edits").html(data);
		$("#loadicon").hide();
	},
	error: function() 
	{} });}));});
</script> 