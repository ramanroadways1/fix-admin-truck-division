<?php include("header.php"); ?>

<?php
$chk = Qry($conn,"SELECT id FROM _access_control WHERE username='$_SESSION[ediary_fix_admin]' AND func_id=(SELECT id FROM 
_access_control_func_list WHERE session_role='8' AND func_name='Allow_Hisab') AND u_view='1'");
			  
if(numRows($chk)==0)
{
	echo "<script>window.location.href='./';</script>";
	exit();
}
?>

<div class="content-wrapper">
      <section class="content-header">
          <h1 style="font-size:16px;">Trip Running Duration : </h1>
       </section>
       
	   <section class="content">
          <div class="row">
            <div class="col-xs-12">
			<div class="box">
                <div class="box-body">
				
<?php
$chk_insert = Qry($conn,"SELECT id FROM _access_control WHERE username='$_SESSION[ediary_fix_admin]' AND func_id=(SELECT id FROM 
_access_control_func_list WHERE session_role='8' AND func_name='Allow_Hisab') AND u_insert='1' AND u_update='1'");
			  
if(numRows($chk_insert)>0)
{
	
$get_trip_lock_days = Qry($conn,"SELECT no_of_days FROM dairy._trip_lock_func ORDER BY id DESC LIMIT 1");

if(!$get_trip_lock_days){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while Processing Request","./");
	exit();
}

$row_trip_days = fetchArray($get_trip_lock_days);		

?>			
<form action="#" method="POST" id="Form1" autocomplete="off">
			
				<div class="col-md-12">
					
					<div class="row">
						
						<div class="form-group col-md-3">
							<label>Lock Over Days <font color="red"><sup>*</sup></font></label>
							<input required="required" value="<?php echo $row_trip_days['no_of_days']; ?>" autocomplete="off" oninput="this.value=this.value.replace(/[^A-Za-z0-9]/,'')" type="text" class="form-control" name="trip_days" id="trip_days" />
						</div>
						
						<div class="form-group col-md-3">
							<?php if(!isMobile()) { echo "<label>&nbsp;</label><br />"; } ?>
							<button type="submit" class="btn btn-sm btn-success <?php if(isMobile()) { echo "btn-block"; } ?>" id="add_btn"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> &nbsp; Update</button>
						</div>
					</div>
				</div>
</form>				
			<?php
}
?>			
	<div class="col-md-12">&nbsp;</div>
				
				<div class="col-md-12 table-responsive" id="load_table">
				</div> 
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

<div id="func_result"></div>  
<div id="load_modal_div"></div>  


<script type="text/javascript">
$(document).ready(function (e) {
$("#Form1").on('submit',(function(e) {
$("#loadicon").show();
$("#add_btn").attr("disabled", true);
e.preventDefault();
	$.ajax({
	url: "./save_allow_hisab_change_duration.php",
	type: "POST",
	data:  new FormData(this),
	contentType: false,
	cache: false,
	processData:false,
	success: function(data){
		$("#func_result").html(data);
	},
	error: function() 
	{} });}));});
</script>
 
<?php
$chk_update = Qry($conn,"SELECT id FROM _access_control WHERE username='$_SESSION[ediary_fix_admin]' AND func_id=(SELECT id FROM 
_access_control_func_list WHERE session_role='8' AND func_name='Allow_Hisab') AND u_update='1'");
			  
if(numRows($chk_update)>0)
{
?>	
<script>	
function UnlockHisab(tno,trip_no)
{
	$("#loadicon").show();
	jQuery.ajax({
		url: "save_allow_hisab.php",
		data: 'tno=' + tno + '&trip_no=' + trip_no,
		type: "POST",
		success: function(data) {
		$("#func_result").html(data);
	},
	error: function() {}
	});
}
</script>
<?php
}
?>
	
<script>	
function LoadTable()
{
	$("#loadicon").show();
	jQuery.ajax({
		url: "_load_allow_hisab.php",
		data: 'id=' + 'ok',
		type: "POST",
		success: function(data) {
			$("#load_table").html(data);
			$('#example').DataTable({ 
				"destroy": true, //use for reinitialize datatable
			});
		},
		error: function() {}
	});
}
 LoadTable();
</script>

<?php include("footer.php") ?>