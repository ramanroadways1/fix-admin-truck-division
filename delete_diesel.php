<?php
require_once("./connect.php");

exit();

require_once("./check_cache.php");

$id = escapeString($conn,strtoupper($_POST['id']));
$trip_id = escapeString($conn,strtoupper($_POST['trip_id']));
$trans_id = escapeString($conn,strtoupper($_POST['trans_id']));
$tno = escapeString($conn,strtoupper($_POST['tno']));
$amount = escapeString($conn,strtoupper($_POST['amount']));
$qty = escapeString($conn,strtoupper($_POST['qty']));

$chk_trip = MySQLQry($conn,"SELECT id FROM trip WHERE id='$trip_id'");

if(!$chk_trip)
{
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	echo "<script type='text/javascript'>
		alert('Error while Processing Request !!!');
	</script>";
	exit();
}

if(numRows($chk_trip)==0)
{
	echo "<script type='text/javascript'>
		alert('Running trip not found !!');
	</script>";
	exit();
}

$get_records = MySQLQry($conn,"SELECT unq_id,cash_diesel FROM diesel WHERE id='$id'");

if(!$get_records)
{
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	echo "<script type='text/javascript'>
		alert('Error while Processing Request !!!');
	</script>";
	exit();
}

if(numRows($get_records)==0)
{
	errorLog("Transaction not found in Table. Tabel Id : $table_id.",$conn,$page_name,__LINE__);
	echo "<script type='text/javascript'>
		alert('Transaction not found !!');
	</script>";
	exit();
}

$row_diesel = fetchArray($get_records);

$chk_done = MySQLQry($conn,"SELECT unq_id,card_pump,narration,branch,date,done FROM diesel_entry WHERE id='$id'");
if(!$chk_done)
{
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	echo "<script>alert('Error while processing request !');</script>";
	exit();
}

$row_done_chk = fetchArray($chk_done);

$cash_diesel = $row_diesel['cash_diesel'];

if($row_diesel['unq_id']!=$row_done_chk['unq_id'])
{
	errorLog("Diesel Transaction not verified between tables. Diesel Id: $id",$conn,$page_name,__LINE__);
	echo "<script>alert('Diesel Transaction not verified between tables !');</script>";
	exit();
}

$log_data = "TruckNo: $tno, Trans_id: $trans_id, Trip_id: $trip_id, Amount: $amount, Qty: $qty, Narr: $row_done_chk[narration]. 
Type: $row_done_chk[card_pump]. Branch: $row_done_chk[branch], Date: $row_done_chk[date].";

if($row_done_chk['done']==1)
{
	echo "<script>alert('Diesel has done from diesel department !');</script>";
	exit();
}

if($cash_diesel=='1')
{
	$find_transaction_driver_book = MySQLQry($conn,"SELECT id,driver_code,tno FROM driver_book WHERE trans_id='$trans_id'");

	if(!$find_transaction_driver_book)
	{
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		echo "<script type='text/javascript'>
			alert('Error while Processing Request !!!');
		</script>";
		exit();
	}

	if(numRows($find_transaction_driver_book)==0)
	{
		errorLog("Transaction not found in Driver Book. Transaction Id : $trans_id.",$conn,$page_name,__LINE__);
		echo "<script type='text/javascript'>
			alert('Transaction not found in Driver Book !!');
		</script>";
		exit();
	}

	$row_transaction_driver_book = fetchArray($find_transaction_driver_book);

	$driver_book_id = $row_transaction_driver_book['id'];
	$driver_code = $row_transaction_driver_book['driver_code'];

	$driver_book_id_prev = $driver_book_id - 1;

	$fetch_driver_data = MySQLQry($conn,"SELECT id,tno FROM driver_up WHERE down=0 AND code='$driver_code' ORDER BY id DESC LIMIT 1");

	if(!$fetch_driver_data)
	{
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		echo "<script type='text/javascript'>
			alert('Error while Processing Request !!!');
		</script>";
		exit();
	}

	if(numRows($fetch_driver_data)==0)
	{
		errorLog("Active Driver not found in DATABASE. Driver Code : $driver_code.",$conn,$page_name,__LINE__);
		echo "<script type='text/javascript'>
			alert('Active Driver not found in DATABASE !!!');
		</script>";
		exit();
	}

	$row_fetch_driver = fetchArray($fetch_driver_data);

	$driver_up_id = $row_fetch_driver['id'];

	if($row_fetch_driver['tno']!=$tno)
	{
		echo "<script type='text/javascript'>
			alert('Truck Number does not matching with Active DRIVER. Try Again !!');
		</script>";
		exit();
	}
}

StartCommit($conn);
$flag = true;

if($cash_diesel=='1')
{
	$update_driver_book_next = MySQLQry($conn,"UPDATE driver_book SET balance=balance+'$amount' WHERE id>'$driver_book_id_prev' AND 
	tno='$tno'");

	if(!$update_driver_book_next)
	{
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
	
	$update_driver_book = MySQLQry($conn,"DELETE FROM driver_book WHERE id='$driver_book_id'");

	if(!$update_driver_book)
	{
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}

	$update_driver_balance = MySQLQry($conn,"UPDATE driver_up SET amount_hold=amount_hold+'$amount'	WHERE id='$driver_up_id'");

	if(!$update_driver_balance)
	{
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
	
}

	$delete = MySQLQry($conn,"DELETE FROM diesel WHERE id='$id'");
	if(!$delete)
	{
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}

	$delete2=MySQLQry($conn,"DELETE FROM diesel_entry WHERE id='$id'");
	if(!$delete2)
	{
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
	
	$update_trip = MySQLQry($conn,"UPDATE trip SET diesel=diesel-'$amount',diesel_qty=ROUND(diesel_qty-'$qty',2) WHERE id='$trip_id'");
			
	if(!$update_trip)
	{
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
	
	if(AffectedRows($conn)==0)
	{
		$flag = false;
		errorLog("Running Trip not found. Trip Id: $trip_id.",$conn,$page_name,__LINE__);
	}
		
$insert_log = MySQLQry($conn,"INSERT INTO ediary_admin_log(code,action,desct,timestamp) VALUES ('$tno','DIESEL_DELETE','$log_data',
'$timestamp')");	
	
if(!$insert_log)
{
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}	
		
	if($flag)
	{
		MySQLCommit($conn);
		closeConnection($conn);
		echo "<script>
			alert('ENTRY DELETED SUCCESSFULLY !');
			ReLoadPage();
		</script>";
		exit();
	}
	else
	{
		MySQLRollBack($conn);
		errorLog("COMMIT NOT COMPLETED.",$conn,$page_name,__LINE__);
		closeConnection($conn);
		echo "<script>alert('Error while processing Request !');window.close();</script>";
		exit();
	}

?>