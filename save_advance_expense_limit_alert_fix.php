<?php
require_once("./connect.php");
 
$timestamp = date("Y-m-d H:i:s");
$date = date("Y-m-d");

$limit_on = escapeString($conn,($_POST['limit_on']));

if($limit_on=='Expense')
{
	echo $_POST['exp'];
	$exp_array = escapeString($conn,($_POST['exp']));
	$exp_name = explode("#",$exp_array)[1];
	$exp_code = explode("#",$exp_array)[0];
}

$limit_value = escapeString($conn,($_POST['limit_value']));
$limit_type = escapeString($conn,($_POST['limit_type']));

if(empty($limit_on))
{
	AlertErrorTopRight("Select option limit on first !");
	exit();
}

if(empty($exp_name) AND $limit_on=='Expense')
{
	AlertErrorTopRight("Select expense first !");
	exit();
}

if(empty($exp_code) AND $limit_on=='Expense')
{
	AlertErrorTopRight("Select expense first !");
	exit();
}

if(empty($limit_type))
{
	AlertErrorTopRight("Select limit type first !");
	exit();
}

if(empty($limit_value))
{
	AlertErrorTopRight("Enter limit value first !");
	exit();
}

if($limit_on=='Advance')
{
	$chk_data = Qry($conn,"SELECT id FROM dairy.trip_adv_exp_limit_for_fix WHERE limit_type='$limit_type' AND adv_exp='ADV'");
}
else
{
	$chk_data = Qry($conn,"SELECT id FROM dairy.trip_adv_exp_limit_for_fix WHERE exp='$exp_code' AND limit_type='$limit_type' AND adv_exp='EXP'");
}

if(!$chk_data){
	AlertErrorTopRight("Error while processing request !");
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	exit();
}

if(numRows($chk_data)>0)
{
	AlertErrorTopRight("Duplicate record found !");
	exit();
}

StartCommit($conn);
$flag = true;	

if($limit_type=="1")
{
	$limit_type_col="amount";
}
else 
{
	$limit_type_col="entry_limit";
}

if($limit_on=='Advance')
{
	$insert = Qry($conn,"INSERT INTO dairy.trip_adv_exp_limit_for_fix(adv_exp,limit_type,`$limit_type_col`,timestamp) VALUES 
	('ADV','$limit_type','$limit_value','$timestamp')");
	
	if(!$insert){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
}
else
{
	$insert = Qry($conn,"INSERT INTO dairy.trip_adv_exp_limit_for_fix(adv_exp,exp_name,exp,limit_type,`$limit_type_col`,timestamp) VALUES 
	('EXP','$exp_name','$exp_code','$limit_type','$limit_value','$timestamp')");
	
	if(!$insert){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
}

if($flag)
{
	MySQLCommit($conn);
	closeConnection($conn);
	
	AlertRightCornerSuccess("Record Added Successfully !");
	echo "<script>
			LoadTable();
		</script>";
	exit();
}
else
{
	MySQLRollBack($conn);
	closeConnection($conn);
	AlertErrorTopRight("Error while processing request !");
	exit();
}	
?>