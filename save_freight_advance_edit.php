<?php
require_once("./connect.php");

$id = escapeString($conn,($_POST['id']));
$amount = escapeString($conn,($_POST['amount']));
$narration = escapeString($conn,strtoupper($_POST['narration']));

$get_data = Qry($conn,"SELECT id,trip_id,trans_id,vou_id,tno,amount,date,branch,narration,timestamp FROM dairy.freight_adv WHERE id='$id'");

if(!$get_data)
{
	AlertErrorTopRight("Error while processing request !");
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	echo "<script> $('#f_to_d_button_edit').attr('disabled',false); </script>";
	exit();
}

if(numRows($get_data) == 0)
{
	AlertErrorTopRight("Entry not found !");
	echo "<script> $('#f_to_d_button_edit').attr('disabled',false); </script>";
	exit();
}

$row_db = fetchArray($get_data);

$table_id = $row_db['id'];
$tno = $row_db['tno'];
$trip_id = $row_db['trip_id'];
$trans_id_db = $row_db['trans_id'];
$amount_db = $row_db['amount'];
$vou_id = $row_db['vou_id'];
$narration_db = $row_db['narration'];

if($amount == $amount_db AND $narration == $narration_db)
{
	AlertErrorTopRight("Nothing to update !");
	echo "<script> $('#f_to_d_button_edit').attr('disabled',false); </script>";
	exit();
}

require_once("./check_cache.php");

$check_trip = Qry($conn,"SELECT id FROM dairy.trip WHERE id='$trip_id'");

if(!$check_trip)
{
	AlertErrorTopRight("Error while processing request !");
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	echo "<script> $('#f_to_d_button_edit').attr('disabled',false); </script>";
	exit();
}

if(numRows($check_trip)==0)
{
	AlertErrorTopRight("Running trip not found !");
	echo "<script> $('#f_to_d_button_edit').attr('disabled',false); </script>";
	exit();
}

$find_txn_driver_book = Qry($conn,"SELECT id,driver_code,tno FROM dairy.driver_book WHERE trans_id='$trans_id_db'");

if(!$find_txn_driver_book)
{
	AlertErrorTopRight("Error while processing request !");
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	echo "<script> $('#f_to_d_button_edit').attr('disabled',false); </script>";
	exit();
}

if(numRows($find_txn_driver_book)==0)
{
	AlertErrorTopRight("Txn not found in driver-book !");
	echo "<script> $('#f_to_d_button_edit').attr('disabled',false); </script>";
	errorLog("Transaction not found in Driver Book. Transaction Id : $trans_id_db.",$conn,$page_name,__LINE__);
	exit();
}

$row_txn_db = fetchArray($find_txn_driver_book);

$driver_book_id = $row_txn_db['id'];
$driver_code = $row_txn_db['driver_code'];

$driver_book_id_prev = $driver_book_id - 1;

$fetch_d_bal = Qry($conn,"SELECT id,tno FROM dairy.driver_up WHERE down=0 AND code='$driver_code' ORDER BY id DESC LIMIT 1");

if(!$fetch_d_bal)
{
	AlertErrorTopRight("Error while processing request !");
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	echo "<script> $('#f_to_d_button_edit').attr('disabled',false); </script>";
	exit();
}

if(numRows($fetch_d_bal)==0)
{
	AlertErrorTopRight("Driver balance not found !");
	echo "<script> $('#f_to_d_button_edit').attr('disabled',false); </script>";
	exit();
}

$row_d_bal = fetchArray($fetch_d_bal);

$driver_up_id = $row_d_bal['id'];

if($row_d_bal['tno'] != $tno)
{
	AlertErrorTopRight("Vehicle not verified !");
	echo "<script> $('#f_to_d_button_edit').attr('disabled',false); </script>";
	exit();
}

// Check Bilty Book for Market Bilty Transactions

if(substr($vou_no,3,1)=='M')
{
	$find_bilty_book_trans = Qry($conn,"SELECT id,bilty_no FROM dairy.bilty_book WHERE trans_id='$trans_id_db'");

	if(!$find_bilty_book_trans)
	{
		AlertErrorTopRight("Error while processing request !");
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		echo "<script> $('#f_to_d_button_edit').attr('disabled',false); </script>";
		exit();
	}

	if(numRows($find_bilty_book_trans)==0)
	{
		AlertErrorTopRight("Bilty book txn not found !");
		echo "<script> $('#f_to_d_button_edit').attr('disabled',false); </script>";
		exit();
	}

	$row_bilty_book = fetchArray($find_bilty_book_trans);

	$vou_no_db = $row_bilty_book['bilty_no'];
	$bilty_book_id = $row_bilty_book['id'];

	if($vou_id != $vou_no_db)
	{
		AlertErrorTopRight("Vou number not verified !");
		echo "<script> $('#f_to_d_button_edit').attr('disabled',false); </script>";
		exit();
	}
}

// Market Bilty Transactions Ends

if($amount!=$amount_db)
{
	$amount_diff = $amount - $amount_db;
}
else
{
	$amount_diff = 0;
}

StartCommit($conn);
$flag = true;

if(substr($vou_no,3,1)=='M')
{
	$update_bilty_book = Qry($conn,"UPDATE dairy.bilty_book SET debit='$amount' WHERE id='$bilty_book_id'");

	if(!$update_bilty_book)
	{
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}

	$update_bilty_book_next = Qry($conn,"UPDATE dairy.bilty_book SET balance=(balance-($amount_diff)) WHERE id>='$bilty_book_id' AND bilty_no='$vou_no'");

	if(!$update_bilty_book_next)
	{
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}

	$update_bilty_balance = Qry($conn,"UPDATE dairy.bilty_balance SET rcvd=(rcvd+($amount_diff)),balance=(balance-($amount_diff)) WHERE bilty_no='$vou_no'");

	if(!$update_bilty_balance)
	{
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
}

	$update_driver_book = Qry($conn,"UPDATE dairy.driver_book SET credit='$amount' WHERE id='$driver_book_id'");

	if(!$update_driver_book)
	{
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}

	$update_driver_book_next = Qry($conn,"UPDATE dairy.driver_book SET balance=(balance+($amount_diff)) WHERE id>'$driver_book_id_prev' AND tno='$tno'");

	if(!$update_driver_book_next)
	{
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}

	$update_driver_balance = Qry($conn,"UPDATE dairy.driver_up SET amount_hold=(amount_hold+($amount_diff)) WHERE id='$driver_up_id'");

	if(!$update_driver_balance)
	{
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}


$update_freight_entry = Qry($conn,"UPDATE dairy.freight_adv SET amount='$amount',narration='$narration' WHERE id='$table_id'");

if(!$update_freight_entry)
{
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

$update_trip = Qry($conn,"UPDATE dairy.trip SET freight=(freight+($amount_diff)) WHERE id='$trip_id'");

if(!$update_trip)
{
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}	

$log_data = "Previous Values: VouId=> $vou_no, Amount=> $amount_db, Narration: $narration_db. New Values: Amount=> $amount, Narration: $narration.";

$insert_log = Qry($conn,"INSERT INTO dairy.ediary_admin_log(code,action,desct,timestamp) VALUES ('$table_id','EDIT_FRT_TO_DRIVER','$log_data','$timestamp')");	
	
if(!$insert_log)
{
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

if($flag)
{
	MySQLCommit($conn);
	closeConnection($conn);
	
	AlertRightCornerSuccess("Updated Successfully !");
	echo "<script> $('#f_to_d_button_edit').attr('disabled',false); </script>";
	exit();
}
else
{
	MySQLRollBack($conn);
	closeConnection($conn);
	
	AlertErrorTopRight("Error while processing Request !");
	echo "<script> $('#f_to_d_button_edit').attr('disabled',false); </script>";
	exit();
}
?>