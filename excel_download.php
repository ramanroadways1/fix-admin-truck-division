<?php
include("header.php");

$chk = Qry($conn,"SELECT id FROM _access_control WHERE username='$_SESSION[ediary_fix_admin]' AND func_id=(SELECT id FROM 
_access_control_func_list WHERE session_role='8' AND func_name='Excel_Downloads') AND u_view='1'");
			  
if(numRows($chk)==0)
{
	echo "<script>window.location.href='./';</script>";
	exit();
}

$today_date = date("Y-m-d");
?>
<script>
$(function() {
		$("#tno").autocomplete({
		source: 'autofill/get_tno.php',
		// appendTo: "#LaneRuleModal",
		select: function (event, ui) { 
              $('#tno').val(ui.item.value);   
             // $("#button1").attr('disabled',false);				   
             return false;
		},
		change: function (event, ui) {
		if(!ui.item){
		    $(event.target).val("");
			alert('Vehicle Number does not exists.');
			$("#tno").val('');
			$("#tno").focus();
			// $("#button1").attr('disabled',true);
		}}, 
		focus: function (event, ui){
			return false;}
});});

$(function() {
		$("#driver_name").autocomplete({
		source: 'autofill/get_own_truck_driver.php',
		// appendTo: "#LaneRuleModal",
		select: function (event, ui) { 
              $('#driver_name').val(ui.item.value);   
              $('#driver_code').val(ui.item.code);   
             // $("#button1").attr('disabled',false);				   
             return false;
		},
		change: function (event, ui) {
		if(!ui.item){
		    $(event.target).val("");
			alert('Vehicle Number does not exists.');
			$("#driver_name").val('');
			$("#driver_code").val('');
			$("#driver_name").focus();
			// $("#button1").attr('disabled',true);
		}}, 
		focus: function (event, ui){
			return false;}
});});

$(function() {
		$("#sold_tno").autocomplete({
		source: 'autofill/get_sold_tno.php',
		// appendTo: "#LaneRuleModal",
		select: function (event, ui) { 
              $('#sold_tno').val(ui.item.value);   
             // $("#button1").attr('disabled',false);				   
             return false;
		},
		change: function (event, ui) {
		if(!ui.item){
		    $(event.target).val("");
			alert('Vehicle Number does not exists.');
			$("#sold_tno").val('');
			$("#sold_tno").focus();
			// $("#button1").attr('disabled',true);
		}}, 
		focus: function (event, ui){
			return false;}
});});
</script>

<form action="excel_download_file.php" target="_blank" method="POST">
<div class="content-wrapper">
      <section class="content-header">
          <h1 style="font-size:16px;">Excel Downloads </h1>
       </section>
       
	   <section class="content">
          <div class="row">
            <div class="col-xs-12">
			<div class="box">
                <div class="box-body">
				
				<div class="col-md-12">&nbsp;</div>
				
				<div class="col-md-12">
					<div class="row">
	
<script>					
function GetReport(elem)
{
	if(elem=='ACTIVE_DRIVER' || elem=='DRIVER' || elem=='KM' || elem=='RUNNING')
	{
		$('#from_date').attr('readonly',true);
		$('#to_date').attr('readonly',true);
	}
	else
	{
		$('#from_date').attr('readonly',false);
		$('#to_date').attr('readonly',false);	
	}
}

function CheckDates(to)
{
	var fromdate = $('#from_date').val();
	
	if(fromdate=='')
	{
		alert('ENTER FROM DATE FIRST !');
		$('#to_date').val('');
	}
}

function FromDateSet(fromdate)
{
	$('#to_date').val('');
	
	var nextDay = new Date(fromdate);
		nextDay.setDate(nextDay.getDate() + 0);
		
		var next_day=nextDay.toISOString().slice(0,10);
		
		var newdate1 = new Date(nextDay.setMonth(nextDay.getMonth() + 1));
		var next_day1=newdate1.toISOString().slice(0,10);
		
		var date_today = '<?php echo $today_date; ?>';
		
		$("#to_date").attr("min",next_day);
		$("#to_date").attr("max",next_day1);
}
</script>
							
						<div class="form-group col-md-3">
							<label style="font-size:12px !important">Select Report <sup><font color="red">*</font></sup></label>
							<select onchange="GetReport(this.value)" name="report_type" class="form-control" required>
								<option value="">Select an option</option>
								<option value="DRIVER_NAAME">DRIVER NAAME</option>
								<option value="EXP_SHEET">EXPENSE SHEET</option>
								<option value="ACTIVE_DRIVER">ACTIVE DRIVERS</option>
								<option value="DA">DA BOOK</option>
								<option value="SALARY">SALARY BOOK</option>
								<option value="DRIVER">ALL DRIVER DATA</option>
								<option value="F1">FREIGHT TO DRIVER</option>
								<option value="F2">FREIGHT COLL. AT OFC</option>
								<option value="KM">MASTER KM</option>
								<option value="RUNNING">Trip Running Duration</option>
							</select>
						</div>
						
						<div class="form-group col-md-3">
							<label style="font-size:12px !important">From Date <sup><font color="red">*</font></sup></label>
							<input onchange="FromDateSet(this.value)" type="date" id="from_date" class="form-control" name="from_date" required 
							max="<?php echo date("Y-m-d"); ?>" pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}" />
						</div>
						
						<div class="form-group col-md-3">
							<label style="font-size:12px !important">To Date <sup><font color="red">*</font></sup></label>
							<input type="date" onchange="CheckDates(this.value)" id="to_date" class="form-control" name="to_date" required 
							max="<?php echo date("Y-m-d"); ?>" pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}" />
						</div>
						
						<div class="form-group col-md-3">
							<?php if(!isMobile()) { echo "<label>&nbsp;</label><br />"; } ?>
							<button type="submit" style="margin-top:1px;font-size:12px !important" class="btn btn-sm btn-success <?php if(isMobile()) { echo "btn-block"; } ?>" id="add_btn"><i class="fa fa-download" aria-hidden="true"></i> &nbsp; Download</button>
						</div>
					</div>
				</div>
				
				 </div> 
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
</form>
<div id="func_result"></div>  


<?php include("footer.php") ?>
