<?php include("header.php"); ?>

<?php
$chk = Qry($conn,"SELECT id FROM _access_control WHERE username='$_SESSION[ediary_fix_admin]' AND func_id=(SELECT id FROM 
_access_control_func_list WHERE session_role='8' AND func_name='Salary_Master') AND u_view='1'");
			  
if(numRows($chk)==0)
{
	echo "<script>window.location.href='./';</script>";
	exit();
}
?>

<div class="content-wrapper">
      <section class="content-header">
          <h1 style="font-size:16px;">SALARY Master : </h1>
       </section>
       
	   <section class="content">
          <div class="row">
            <div class="col-xs-12">
			<div class="box">
                <div class="box-body">
<?php
$chk_insert = Qry($conn,"SELECT id FROM _access_control WHERE username='$_SESSION[ediary_fix_admin]' AND func_id=(SELECT id FROM 
_access_control_func_list WHERE session_role='8' AND func_name='Salary_Master') AND u_insert='1'");
			  
if(numRows($chk_insert)>0)
{
?>				
				
				<div class="col-md-12">
					<div class="row">
						
						<div class="form-group col-md-3">
							<label>Select Model <font color="red"><sup>*</sup></font></label>
							<select style="font-size:12px" data-size="8" name="model" id="model" class="form-control selectpicker" data-live-search="true" required="required">
								<option style="font-size:12px" data-tokens="" value="">--select option--</option>
							<?php
							$get_model = Qry($conn,"SELECT model FROM dairy.model_list WHERE is_active='1' ORDER BY model ASC");

							if(numRows($get_model)>0)
							{
								while($row_m = fetchArray($get_model))
								{
									echo "<option style='font-size:12px' data-tokens='$row_m[model]' value='$row_m[model]'>$row_m[model]</option>";
								}
							}
							?>
							</select>
						</div>
						
						<div class="form-group col-md-3">
							<label>Salary Amount <font color="red"><sup>*</sup></font></label>
							<input autocomplete="off" type="number" class="form-control" id="salary_amount" />
						</div>
						
						<div class="form-group col-md-3">
							<label>Salary Pattern <font color="red"><sup>*</sup></font></label>
							<select name="sal_pattern" id="sal_pattern" class="form-control" required="required">
								<option value="">--select option--</option>
								<option value="0">Per Month</option>
								<option value="1">Per Trip</option>
							</select>
						</div>
						
						<div class="form-group col-md-3">
							<?php if(!isMobile()) { echo "<label>&nbsp;</label><br />"; } ?>
							<button type="button" onclick="AddUserFunc()" class="btn btn-sm btn-success <?php if(isMobile()) { echo "btn-block"; } ?>" id="add_btn"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> &nbsp; Add New Salary</button>
						</div>
					</div>
				</div>
				
				<?php
				}
				?>
				<div class="col-md-12">&nbsp;</div>
				
				<div class="col-md-12 table-responsive" id="load_table">
              
				 </div> 
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

<div id="func_result"></div>  
<div id="load_modal_div"></div>  
<div id="func_result2"></div>  
 
<script>
function AddUserFunc()
{
	var model = $('#model').val();
	var salary_amount = $('#salary_amount').val();
	var sal_pattern = $('#sal_pattern').val();
	
	if(model=='')
	{
		Swal.fire({icon: 'warning',html: '<font size=\'2\' color=\'black\'>Select model first !</font>',});
	}
	else if(salary_amount=='')
	{
		Swal.fire({icon: 'warning',html: '<font size=\'2\' color=\'black\'>Enter SALARY amount first !</font>',});
	}
	else if(sal_pattern=='')
	{
		Swal.fire({icon: 'warning',html: '<font size=\'2\' color=\'black\'>Select salary pattern first !</font>',});
	}
	else
	{
		$('#add_btn').attr('disabled',true); 
		$('#loadicon').show();
		jQuery.ajax({
			url: "save_add_salary_master.php",
			data: 'model=' + model + '&salary_amount=' + salary_amount + '&sal_pattern=' + sal_pattern,
			type: "POST",
			success: function(data) {
			$("#func_result").html(data);
			},
			error: function() {}
		});
	}
}
</script>
 
<script>	
function LoadTable()
{
	jQuery.ajax({
		url: "_load_salary_master.php",
		data: 'id=' + 'ok',
		type: "POST",
		success: function(data) {
			$("#load_table").html(data);
			$('#example').DataTable({ 
				"destroy": true, //use for reinitialize datatable
			});
		},
		error: function() {}
	});
}
LoadTable();

</script>

<?php
$chk_update = Qry($conn,"SELECT id FROM _access_control WHERE username='$_SESSION[ediary_fix_admin]' AND func_id=(SELECT id FROM 
_access_control_func_list WHERE session_role='8' AND func_name='Salary_Master') AND u_update='1'");
			  
if(numRows($chk_update)>0)
{
?>
<script>
function ModifyExp(id,type)
{
	$('#loadicon').show();
		jQuery.ajax({
			url: "modal_edit_exp_cap_amount.php",
			data: 'id=' + id + '&type=' +  type,
			type: "POST",
			success: function(data) {
			$("#func_result").html(data);
			},
			error: function() {}
		});
}

function UnlockOnEmpty(id)
{
	$('#loadicon').show();
		jQuery.ajax({
			url: "lock_unlock_exp_on_empty.php",
			data: 'id=' + id + '&type=' + '0',
			type: "POST",
			success: function(data) {
			$("#func_result").html(data);
			},
			error: function() {}
		});
}

function LockOnEmpty(id)
{
	$('#loadicon').show();
		jQuery.ajax({
			url: "lock_unlock_exp_on_empty.php",
			data: 'id=' + id + '&type=' + '1',
			type: "POST",
			success: function(data) {
			$("#func_result").html(data);
			},
			error: function() {}
		});
}
</script>

<?php
}
?>

<?php include("footer.php") ?>