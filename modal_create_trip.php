<script type="text/javascript">
$(document).ready(function (e) {
$("#InsertNewTrip").on('submit',(function(e) {
$("#loadicon").show();
$("#trip_sub").attr("disabled", true);
e.preventDefault();
	$.ajax({
	url: "./insert_trip.php",
	type: "POST",
	data:  new FormData(this),
	contentType: false,
	cache: false,
	processData:false,
	success: function(data){
		$("#result_trip_create").html(data);
	},
	error: function() 
	{} });}));});
</script>

<form id="InsertNewTrip" autocomplete="off">   
<button id="modalbutton" style="display:none" data-toggle="modal" data-target="#modal1">Modal</button>	

  <div class="modal fade" id="modal1" role="dialog" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-xl">
      <div class="modal-content">
		
		<div class="modal-body">
		<div class="row">
			
			<div id="result_trip_create"></div>
			
			<input type="hidden" name="tno" id="truck_no" class="form-control" required />
			<input type="hidden" id="trip_id" name="trip_id">
			<input type="hidden" id="trip_type_db" name="trip_type">
			<input type="hidden" id="trip_no_db" name="trip_no">
			<input type="hidden" name="billing_type_text" id="billing_type_text" class="form-control" required />
			<input type="hidden" name="freight_bilty_text" id="freight_bilty_text" class="form-control" required />
			
			<div class="form-group col-md-12 bg-primary" style="color:#FFF">
				<h4>Create new trip :</h4>
			</div>
		
			<div class="form-group col-md-3">
				<label>Branch. <sup><font color="red">*</font></sup></label>
				<select name="branch" id="branch" onchange="$('#lr_type').val('')" class="form-control" required>
					<option value="">Select branch</option>
					<?php
					$qry_sel=MySQLQry($conn,"SELECT username FROM rrpl_database.user WHERE role='2' ORDER BY username ASC");
					if(!$qry_sel)
					{
						errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
						exit();
					}
					
					if(numRows($qry_sel)>0)
					{
						while($row_branch=fetchArray($qry_sel))
						{
							echo "<option value='$row_branch[username]'>$row_branch[username]</option>";
						}
					}
					?>
				</select>
			</div>
			
			<div class="form-group col-md-4">
				<label>LR Type. <sup><font color="red">*</font></sup></label>
				<select name="lr_type" class="form-control" id="lr_type" onchange="LRType(this)" required>
					<option value="">Select an option</option>
					<option value="OWN">Own Bilty</option>
					<option value="MKT">Market Bilty</option>
					<option value="EMPTY">EMPTY</option>
					<option value="CON20">EMPTY-CONTAINER : 20</option>
					<option value="CON40">EMPTY-CONTAINER : 40</option>
				</select>
			</div>
		
			<script>
			function LRType(lr)
			{
				if($('#branch').val()=='')
				{
					alert('ENTER BRANCH NAME FIRST !!');
					$('#lr_type').val('');
				}
				else
				{
					if($('#from_loc_id').val()=='')
					{
						$('#from_stn').attr('readonly',false);
						$('#from_stn').val('');
					}
					
					$('#edit_branch').val('');
					
						$('#actual').attr('readonly',false);
						$('#charge').attr('readonly',false);
						$('#lrno').attr('readonly',false);
						$('#to_stn').attr('readonly',false);
					
						$('#actual').val('');
						$('#charge').val('');
						$('#lrno').val('');
						$('#to_stn').val('');
						
					if(lr.value=='')
					{
						$('#lr_type_div').html('');
						$('#lr_type_div').hide();
					}
					else
					{
						$("#loadicon").show();
						jQuery.ajax({
						url: "./fetch_lr_type.php",
						data: 'type=' + lr.value + '&tno=' + $('#truck_no').val() + '&branch=' + $('#branch').val(),
						type: "POST",
						success: function(data) {
						$("#lr_type_div").html(data);
						$("#loadicon").hide();
						},
						error: function() {}
						});
					}
				}
			}
			</script>
			
			<script>
			function FetchLRDataFromLR(frno,type)
			{
					$("#loadicon").show();
					jQuery.ajax({
					url: "./fetch_lr_data.php",
					data: 'frno=' + frno + '&type=' + type + '&from_loc=' + $('#from_loc_of_new_trip2').val() + '&tno=' + $('#truck_no').val() + '&lr_type=' + $('#next_trip_lr_type').val() + '&to_loc_set=' + $('#to_loc_set').val() + '&to_loc_name=' + $('#to_loc_name').val() + '&to_loc_id=' + $('#to_loc_id11').val(),
					type: "POST",
					success: function(data) {
					$("#lr_result").html(data);
					$("#loadicon").hide();
					},
					error: function() {}
					});
			}
			</script>
			
			<div id="lr_result"></div>
			
			<div id="lr_type_div" style="display:none" class="col-md-4">
			</div>
			
			<div class="form-group col-md-2">
				<label>Act Wt. <sup><font color="red">*</font></sup></label>
				<input type="number" min="0" step="any" id="actual" name="actual" class="form-control" required />
			</div>
			
			<div class="form-group col-md-2">
				<label>Chrg Wt. <sup><font color="red">*</font></sup></label>
				<input type="number" min="0" step="any" id="charge" name="charge" class="form-control" required />
			</div>
			
			<div class="form-group col-md-4">
				<label>From. <sup><font color="red">*</font></sup></label>
				<input type="text" oninput="this.value=this.value.replace(/[^a-z A-Z0-9]/,'')" id="from_stn" name="from" readonly class="form-control" required />
			</div>
			
			<div class="form-group col-md-4">
				<label>To. <sup><font color="red">*</font></sup></label>
				<input type="text" oninput="this.value=this.value.replace(/[^a-z A-Z0-9]/,'')" name="to" id="to_stn" class="form-control" required />
			</div>
			
			<div class="form-group col-md-4">
				<label>LR No. <sup><font color="red">*</font></sup></label>
				<input type="text" name="lrno" id="lrno" class="form-control" required />
			</div>
			
			<div class="form-group col-md-4">
				<label>Unloading Loc. PinCode. <sup><font color="red">*</font></sup></label>
				<input type="text" maxlength="6" minlength="6" oninput="this.value=this.value.replace(/[^0-9]/,'')" name="pincode" id="pincode" class="form-control" required />
			</div>
			
			<div class="form-group col-md-4">
			<label>Next Edit Branch. <sup><font color="red">*</font></sup></label>
			<select name="next_edit_branch" id="edit_branch" onchange="FetchKm(this.value)" class="form-control" required>
				<option value="">Select an option</option>
				<?php
				$qry_2=MySQLQry($conn,"SELECT username FROM rrpl_database.user WHERE role='2' ORDER BY username ASC");
				if(!$qry_2)
				{
					errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
					exit();
				}
				if(numRows($qry_2)>0)
				{
					echo "<option value='0'>UNKNOWN</option>";
					while($row_branch2=fetchArray($qry_2))
					{
						echo "<option value='$row_branch2[username]'>$row_branch2[username]</option>";
					}
				}
				?>
			</select>
		</div>
		
		<script>
		function FetchKm()
		{
			var from1 = $('#from_stn').val();
			var to1 = $('#to_stn').val();
			
			if(from1=='' || to1=='')
			{
				alert('Enter From and To Location first !');
				$('#trip_km').val('');
				$('#edit_branch').val('');
			}
			else
			{
				$('#from_stn').attr('readonly',true);
				$('#to_stn').attr('readonly',true);
				
					$("#loadicon").show();
					jQuery.ajax({
					url: "./fetch_master_km.php",
					data: 'from=' + from1 + '&to=' + to1 + '&tno=' + $('#truck_no').val() + '&from_id=' + $('#from_stn_idnew').val() + '&to_id=' + $('#to_stn_idnew').val(),
					type: "POST",
					success: function(data) {
					$("#km_result").html(data);
					$("#loadicon").hide();
					},
					error: function() {}
					});
			}
			
		}
		</script>
		
		<script>
		function FuncKm()
		{
			if($('#edit_branch').val()=='')
			{
				$('#trip_km').val('');
				$('#edit_branch').focus();
			}
		}
		</script>
		
		<div id="km_result"></div>
		
		<input type="hidden" id="from_loc_id" name="from_loc_id" />
		<input type="hidden" id="to_loc_id" name="to_loc_id" />
		<input type="hidden" id="set_km" name="set_km" />
		
		<input type="hidden" id="at_loading_set" name="at_loading_set" />
		
		<input type="hidden" id="from_loc_of_new_trip2" name="from_loc_of_new_trip2" />
		
		<input type="hidden" id="start_date1" name="start_date1" />
		<input type="hidden" id="end_date1" name="end_date1" />
		
		<!-- FOR NEXT TRIP's From Location -->
		
		<input type="hidden" id="to_loc_set" name="to_loc_set" />
		<input type="hidden" id="next_trip_lr_type" name="next_trip_lr_type" />
		<input type="hidden" id="to_loc_name" name="to_loc_name" />
		<input type="hidden" id="to_loc_id11" />
		
		<input type="hidden" id="from_stn_idnew" />
		<input type="hidden" id="to_stn_idnew" />
		<input type="hidden" id="location_verification" name="location_verification" />
		
		<!-- FOR NEXT TRIP's From Location -->
		
		<div class="form-group col-md-4">
			<label>Trip KM. <sup><font color="red">*</font></sup></label>
			<input oninput="FuncKm()" type="number" name="trip_km" id="trip_km" class="form-control" required />
		</div>
		
		<div class="form-group col-md-3">
			<label>Trip Start Date. <sup><font color="red">*</font></sup></label>
			<input type="date" name="trip_start" id="start_date" max="<?php echo date("Y-m-d"); ?>" required pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}" class="form-control" />
		</div>
		
		<div class="form-group col-md-3">
			<label>Trip END Date. <sup><font color="red">*</font></sup></label>
			<input type="date" name="trip_end" id="end_date" max="<?php echo date("Y-m-d"); ?>" required pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}" class="form-control" />
		</div>
		
		</div>
        </div>
        <div class="modal-footer">
          <button type="submit" disabled="disabled" id="trip_sub" class="btn btn-danger">Create Trip</button>
          <button type="button" id="trip_modal_close" class="btn btn-primary" data-dismiss="modal">Close</button>
		</div>
      </div>
      </form>
    </div>
  </div>
 