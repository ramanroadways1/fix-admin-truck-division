<?php
require_once("connect.php");
?>
<button id="modal_lr_bank_completed_btn" style="display:none" data-toggle="modal" data-target="#ModalLR_BankDb"></button>

<div class="modal fade" id="ModalLR_BankDb" role="dialog" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-lg">
		<div class="modal-content" style="">
			<div class="modal-header bg-primary">
				<span style="font-size:13px">LR Bank Database: </span>
			</div>
		<div class="modal-body">
		
			<table id="example_db" class="table table-bordered table-striped" style="font-size:12px">
                    <thead>
                      <tr>
                        <th>#</th>
                        <th>Branch</th>
                        <th>From</th>
                        <th>To</th>
                        <th>Company</th>
                        <th>Stock</th>
                        <th>Timestamp</th>
                      </tr>
                    </thead>
                    <tbody>
	<?php
	$get_record = Qry($conn,"SELECT id,branch,from_range,to_range,company,stock,stock_left,stock-stock_left as used,timestamp FROM lr_bank_db");
	
	if(!$get_record){
		AlertErrorTopRight("Error while processing request !");
		echo getMySQLError($conn);
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		exit();
	}
	
	if(numRows($get_record)==0)
	{
		echo "<tr>
			<td colspan='7'>No record found !</td>
			<td style='display: none'></td>
			<td style='display: none'></td>
			<td style='display: none'></td>
			<td style='display: none'></td>
			<td style='display: none'></td>
			<td style='display: none'></td>
			<td style='display: none'></td>
		</tr>";
	}
	else
	{
		$i=1;
		while($row = fetchArray($get_record))
		{
			$timestamp = date("d-m-y h:i A",strtotime($row['timestamp']));
			
			echo "<tr>
				<td>$i</td>
				<td>$row[branch]</td>
				<td>$row[from_range]</td>
				<td>$row[to_range]</td>
				<td>$row[company]</td>
				<td>$row[stock]</td>
				<td>$timestamp</td>
			</tr>";
		$i++;	
		}
	}
	?>	
                    </tbody>
                  </table>

  </div>

	 <div class="modal-footer">
         <button type="button" class="btn btn-sm btn-primary" data-dismiss="modal">Close</button>
	 </div>
	 
      </div>
    </div>
  </div>
  

<script>
	$('#modal_lr_bank_completed_btn')[0].click();	
	$('#loadicon').fadeOut('slow');	
</script> 