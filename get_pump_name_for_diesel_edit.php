<?php
require_once("./function.php");

$branch=escapeString($conn,strtoupper($_POST['branch']));
$set_code=escapeString($conn,strtoupper($_POST['set_code']));

if($branch=='')
{
	echo "
	<script type='text/javascript'>
		alert('Branch not found !');
		ReLoadPage();
	</script>";
	exit();
}

$get_pump_name = MySQLQry($conn,"SELECT name,code,comp FROM diesel_pump_own WHERE branch='$branch' AND active='1'");

if(!$get_pump_name)
{
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	echo "<script type='text/javascript'>
		alert('Error while Processing Request !!!');
		ReLoadPage();
	</script>";
	exit();
}

if(numRows($get_pump_name)==0)
{
	?>
	<label>Pump Name. <sup><font color="red">*</font></sup></label>
	<select class="form-control" name="pump_name_diesel" onchange="CallCode(this.value)" id="pump_name_diesel" required="required">
		<option value="">--SELECT PUMP NAME--</option>
	</select>
	<?php
}
else
{
	echo '
	<label>Pump Name. <sup><font color="red">*</font></sup></label>
	<select class="form-control" name="pump_name_diesel" onchange="CallCode(this.value)" id="pump_name_diesel" required="required">
	<option value="">--SELECT PUMP NAME--</option>';
	
	while($row_pump = fetchArray($get_pump_name))
	{
		?>
	<option <?php if(($row_pump['code']."_".$row_pump['comp'])==$set_code) {echo "selected";} ?> value="<?php echo $row_pump['code']."_".$row_pump['comp']; ?>"><?php echo $row_pump['name']."-".$row_pump['comp']; ?></option>
		<?php

	}
	
	echo "</select>";
}
?>