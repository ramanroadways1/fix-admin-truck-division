<?php
require_once 'connect.php';

$date = escapeString($conn,$_POST['date']);

$sql = Qry($conn,"SELECT t.from_station,t.to_station,t.tno,t.lr_type,t.lrno,t.date as trip_date,t.branch,e.name as branch_user 
FROM dairy.trip AS t 
LEFT OUTER JOIN emp_attendance AS e ON e.code = t.branch_user 
WHERE date(t.timestamp)='$date' AND t.fix_lane!=0");

if(!$sql){
	echo getMySQLError($conn);
	errorLog(getMySQLError($conn),$conn,$page_url,__LINE__);
	exit();
}

?>
<table id="example" class="table table-bordered table-striped" style="font-size:13px;">
        <thead>
		<tr>
			<th>#</th>
			<th>Vehicle_No</th>
			<th>Origin</th>
			<th>Destination</th>
			<th>LR_Type</th>
			<th>LR_No</th>
			<th>Branch</th>
			<th>Username</th>
		</tr>
	
	</thead>
       <tbody id=""> 
		
<?php
if(numRows($sql)==0)
{
	echo "<tr>
		<td colspan='8'>No record found !</td>
	</tr>";  
}
else
{
	$sn=1;
	
	while($row = fetchArray($sql))
	{
		$trip_date = date("d/m/y H:i A",strtotime($row["trip_date"]));
		
		echo "<tr>
				<td>$sn</td>
				<td>$row[tno]</td>
				<td>$row[from_station]</td>
				<td>$row[to_station]</td>
				<td>$row[lr_type]</td>
				<td>$row[lrno]</td>
				<td>$row[branch]</td>
				<td>$row[branch_user]</td>
			</tr>";
		$sn++;		
    }
}
?>
	</tbody>
		</table>
<script> 
$("#loadicon").fadeOut('slow');
$(document).ready(function() {
    $('#example').DataTable( {
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ]
    } );
} );
</script> 