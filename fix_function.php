<?php
include "header.php";

$chk = Qry($conn,"SELECT id FROM _access_control WHERE username='$_SESSION[ediary_fix_admin]' AND func_id=(SELECT id FROM 
_access_control_func_list WHERE session_role='8' AND func_name='Fix_Lane') AND u_view='1'");
			  
if(numRows($chk)==0)
{
	echo "<script>window.location.href='./';</script>";
	exit();
}
?>
<script type="text/javascript">
$(function() {  
      $("#from_loc").autocomplete({
			source: function(request, response){ 
			$.ajax({
                  url: "autofill/get_location.php",
                  type: 'post',
                  dataType: "json",
                  data: {
                   search: request.term,
                  },
              success: function(data) {
              response(data);}});
             },
              select: function (event, ui) { 
              $(event.target).val(ui.item.value);   
               $('#from_loc_id').val(ui.item.id);     
               return false;
              },
              change: function (event, ui) {  
                if(!ui.item){
                $(event.target).val(""); 
                $(event.target).focus();
				$("#from_loc_id").val('');
				alert('Location does not exist !'); 
              } 
              },
			}); 
      });	

$(function() {  
      $("#to_loc").autocomplete({
			source: function(request, response){ 
			$.ajax({
                  url: "autofill/get_location.php",
                  type: 'post',
                  dataType: "json",
                  data: {
                   search: request.term,
                  },
              success: function(data) {
              response(data);}});
             },
              select: function (event, ui) { 
              $(event.target).val(ui.item.value);   
               $('#to_loc_id').val(ui.item.id);     
               return false;
              },
              change: function (event, ui) {  
                if(!ui.item){
                $(event.target).val(""); 
                $(event.target).focus();
				$("#to_loc_id").val('');
				alert('Location does not exist !'); 
              } 
              },
			}); 
      });

$(function() {  
      $("#con1_fix_find").autocomplete({
		  appendTo: "#LaneRuleModal",
			source: function(request, response){ 
			$.ajax({
                  url: "autofill/get_con1.php",
                  type: 'post',
                  dataType: "json",
                  data: {
                   search: request.term,
                  },
              success: function(data) {
              response(data);}});
             },
              select: function (event, ui) { 
              $(event.target).val(ui.item.value);   
               $('#con1_id').val(ui.item.id);     
               return false;
              },
              change: function (event, ui) {  
                if(!ui.item){
                $(event.target).val(""); 
                $(event.target).focus();
				$("#con1_id").val('');
				alert('Consignor does not exist !'); 
              } 
              },
			}); 
      });	

$(function() {  
      $("#con2_fix_find").autocomplete({
			appendTo: "#LaneRuleModal",
			source: function(request, response){ 
			$.ajax({
                  url: "autofill/get_con2.php",
                  type: 'post',
                  dataType: "json",
                  data: {
                   search: request.term,
                  },
              success: function(data) {
              response(data);}});
             },
              select: function (event, ui) { 
              $(event.target).val(ui.item.value);   
               $('#con2_id').val(ui.item.id);     
               return false;
              },
              change: function (event, ui) {  
                if(!ui.item){
                $(event.target).val(""); 
                $(event.target).focus();
				$("#con2_id").val('');
				alert('Consignee does not exist !'); 
              } 
              },
			}); 
      });

$(function() {  
      $("#con2_multi_consignee").autocomplete({
			appendTo: "#MulDelConsigneeModal",
			source: function(request, response){ 
			$.ajax({
                  url: "autofill/get_con2.php",
                  type: 'post',
                  dataType: "json",
                  data: {
                   search: request.term,
                  },
              success: function(data) {
              response(data);}});
             },
              select: function (event, ui) { 
              $(event.target).val(ui.item.value);   
               $('#con2_multi_consignee_id').val(ui.item.id);     
               return false;
              },
              change: function (event, ui) {  
                if(!ui.item){
                $(event.target).val(""); 
                $(event.target).focus();
				$("#con2_multi_consignee_id").val('');
				alert('Consignee does not exist !'); 
              } 
              },
			}); 
      });		  
</script> 

<div class="content-wrapper">
   <section class="content-header">
		<span style="font-size:15px">Fix Lane Master :</span>
   </section>
	
<script>
$(function() {
		$("#veh_no1").autocomplete({
		source: 'autofill/get_tno.php',
		appendTo: "#LaneRuleModal",
		select: function (event, ui) { 
              $('#veh_no1').val(ui.item.value);   
              $('#db_wheeler').val(ui.item.wheeler);   
             // $("#button1").attr('disabled',false);				   
             return false;
		},
		change: function (event, ui) {
		if(!ui.item){
		    $(event.target).val("");
			alert('Vehicle Number does not exists.');
			$("#veh_no1").val('');
			$("#db_wheeler").val('');
			$("#veh_no1").focus();
			// $("#button1").attr('disabled',true);
		}}, 
		focus: function (event, ui){
			return false;}
});});
</script>

<script type="text/javascript">
$(document).ready(function (e) {
$("#NewRuleForm").on('submit',(function(e) {
$("#button_new_rule").attr("disabled", true);
$("#loadicon").show();
e.preventDefault();
	$.ajax({
	url: "./fix_lane_add_new_rule.php",
	type: "POST",
	data:  new FormData(this),
	contentType: false,
	cache: false,
	processData:false,
	success: function(data){
		$("#modal_form_result").html(data);
	},
	error: function() 
	{} });}));});
</script>

<div id="function_res"></div>
		
   <section class="content">
       <div class="row">
            <div class="col-md-12">
            <div class="box"> 
			<div class="box-body">

<div class="row">
 
 <div class="form-group col-md-12">
    <br />
<?php
$qry = Qry($conn,"SELECT id FROM _access_control WHERE username='$_SESSION[ediary_fix_admin]' AND func_id=(SELECT id FROM 
_access_control_func_list WHERE session_role='8' AND func_name='Fix_Lane') AND u_insert='1'");
			  
if(numRows($qry)>0)
{
?>	
	<div class="form-group col-md-12">
	<div class="row">
		<div class="form-group col-md-3">
			<label>From location <font color="red">*</font></label>
			<input oninput="this.value=this.value.replace(/[^a-zA-Z]/,'')" type="text" class="form-control" required="required" id="from_loc">
		</div>
		
		<div class="form-group col-md-3">
			<label>To location <font color="red">*</font></label>
			<input oninput="this.value=this.value.replace(/[^a-z A-Z]/,'')" type="text" class="form-control" required="required" id="to_loc">
		</div>
		
		<input type="hidden" id="from_loc_id">
		<input type="hidden" id="to_loc_id">
		
		<div class="form-group col-md-3">
			<label>&nbsp;</label>
			<br />
			<button id="button1" type="button" onclick="AddRecord();" class="btn btn-sm btn-success">
			<span class="fa fa-folder-plus"></span> &nbsp;Add new Lane</button>
		</div>
	</div>
		
    </div>
<?php
}
?>	
	
	<div class="form-group col-md-12 table-responsive" id="func_result">
		 
        	
    </div>
    </div>
  </div>
</div>

</body>
</html>

          </div>
          </div>
		  </div>
       </div>         
    </section>
	
<script>
function AddRecord()
{
	var from_loc = $('#from_loc').val();
	var from_loc_id = $('#from_loc_id').val();
	var to_loc = $('#to_loc').val();
	var to_loc_id = $('#to_loc_id').val();
	
	if(from_loc!='' && to_loc!='')
	{
		if(from_loc_id!='' && to_loc_id!='')
		{
			$('#button1').attr('disabled',true);
			$("#loadicon").show();
			jQuery.ajax({
				url: "fix_lane_add_new.php",
				data: 'from_loc_id=' + from_loc_id + '&to_loc_id=' + to_loc_id + '&from_loc=' + from_loc + '&to_loc=' + to_loc,
				type: "POST",
				success: function(data) {
					$("#function_res").html(data);
				},
				error: function() {}
			});
		}
		else
		{
			Swal.fire({
			html: '<span style=\'font-size:13px\'>Locations not found !</span>',
			icon: 'warning',
			})
		}
	}
	else
	{
		Swal.fire({
		html: '<span style=\'font-size:13px\'>Locations not found !</span>',
		icon: 'warning',
		})
	}
}

function LaneRule(id,total_rules)
{
	// if(Number(total_rules)>0)
	// {
		var from_loc = $('#from_loc_name_'+id).val();
		var to_loc = $('#to_loc_name_'+id).val();
		$('#location_html_modal').html(from_loc+' to '+to_loc);
		$('#lane_modal')[0].click();
		LoadAllRule(id);
	// }
}

function LoadAllRule(id)
{
	$("#loadicon").show();
		jQuery.ajax({
			url: "fix_lane_load.php",
			data: 'id=' + id,
			type: "POST",
			success: function(data) {
				$("#lane_load_result").html(data);
			},
			error: function() {}
		});
}

function ActiveLane(id)
{
	$('#btn_id_'+id).attr('disabled',true);
	$("#loadicon").show();
	jQuery.ajax({
		url: "fix_lane_active_disable.php",
		data: 'id=' + id + '&action=' + '1',
		type: "POST",
		success: function(data) {
			$("#function_res").html(data);
		},
		error: function() {}
	});
}

function DisableLane(id)
{
	$('#btn_id_'+id).attr('disabled',true);
	$("#loadicon").show();
	jQuery.ajax({
		url: "fix_lane_active_disable.php",
		data: 'id=' + id + '&action=' + '0',
		type: "POST",
		success: function(data) {
			$("#function_res").html(data);
		},
		error: function() {}
	});
}

function LoadData()
{
	// $("#loadicon").show();
		jQuery.ajax({
			url: "_fetch_fix_lane.php",
			data: 'ok=' + 'ok',
			type: "POST",
			success: function(data) {
			$("#func_result").html(data);
			   $('#example').DataTable({ 
                 "destroy": true,
              });
			},
			error: function() {}
	});
}

LoadData();

function DeleteLane(id)
{
	$('#btn_dlt_lane_'+id).attr('disabled',true);
	$("#loadicon").show();
	jQuery.ajax({
		url: "_delete_fix_lane.php",
		data: 'id=' + id,
		type: "POST",
		success: function(data) {
			$("#function_res").html(data);
		},
		error: function() {}
	});
}

function RemoveConsignee(rule_id,lane_id,con2_id)
{
	var name = $('#consignee_full_name_'+con2_id).val();
	
	if(confirm("Do you really want to remove consignee: "+name+" from this rule ?")==true)
	{
		$('#remove_link_id_'+con2_id).attr('disabled',true);
		$("#loadicon").show();
		jQuery.ajax({
			url: "_delete_fix_multi_consignee.php",
			data: 'rule_id=' + rule_id + '&lane_id=' + lane_id + '&con2_id=' + con2_id,
			type: "POST",
			success: function(data) {
				$("#function_res").html(data);
			},
			error: function() {}
		});
	}
}
</script>	

<form id="NewRuleForm" autocomplete="off">
<div class="modal fade" id="LaneRuleModal" style="background:#DDD" role="dialog" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-xl-mini">
      <div class="modal-content" style="">
	  
	  <div style="font-size:13px" class="modal-header bg-primary">
			Lane : <span id="location_html_modal"></span>
      </div>
	  
	<div class="modal-body">
		
<script>
function Rule_Vehicle()
{
	if($('#rule_vehicle_check').is(":checked")){
       $("#veh_no1").attr('readonly',false);
       $("#veh_no1").val('');
	   
	   $("#veh_model").attr('disabled',true);
       $("#veh_model").val('');
    }
	else{
       $("#veh_no1").attr('readonly',true);
       $("#veh_no1").val('');
	   
       $("#veh_model").attr('disabled',false);
       $("#veh_model").val('');
    }
	
	$('#veh_model').selectpicker('refresh');
}

function Rule_Con1()
{
	if($('#rule_con1_check').is(":checked")){
       $("#con1_fix_find").attr('readonly',false);
       $("#con1_fix_find").val('');
    }
	else{
       $("#con1_fix_find").attr('readonly',true);
       $("#con1_fix_find").val('');
    }
}

function Rule_Con2()
{
	if($('#rule_con2_check').is(":checked")){
       $("#con2_fix_find").attr('readonly',false);
       $("#con2_fix_find").val('');
    }
	else{
       $("#con2_fix_find").attr('readonly',true);
       $("#con2_fix_find").val('');
    }
}

function Rule_EmptyLoaded()
{
	if($('#rule_empty_loaded_check').is(":checked")){
       $("#empty_loaded_fix_find").attr('disabled',false);
       $("#empty_loaded_fix_find").val('');
    }
	else{
       $("#empty_loaded_fix_find").attr('disabled',true);
       $("#empty_loaded_fix_find").val('');
    }
	
	$('#empty_loaded_fix_find').selectpicker('refresh');
}

function Rule_Model()
{
	$('#rule_vehicle_check').prop('checked', false);
	$('#rule_vehicle_check').attr('checked', false);
	
	$("#veh_no1").val('');
	$("#veh_no1").attr('readonly',true);
	$("#veh_model").val('');
	
	if($('#rule_model_check').is(":checked")){
       $("#veh_model").attr('disabled',false);
       $("#rule_vehicle_check").attr('disabled',true);
    }
	else{
       $("#veh_model").attr('disabled',true);
       $("#rule_vehicle_check").attr('disabled',false);
    }
	
	$('#veh_model').selectpicker('refresh');
}

// function Rule_Wheeler()
// {
	// if($('#rule_wheeler_check').is(":checked")){
       // $("#wheeler_fix_find").attr('disabled',false);
       // $("#rule_vehicle_check").attr('disabled',true);
       // $("#wheeler_fix_find").val('');
    // }
	// else{
       // $("#wheeler_fix_find").attr('disabled',true);
       // $("#wheeler_fix_find").val('');
	   // $("#rule_vehicle_check").attr('disabled',false);
    // }
// }

function DeleteRule(id)
{
	Swal.fire({
	  title: 'Are you sure ?',
	  // text: "You won't be able to revert this!",
	  icon: 'warning',
	  showCancelButton: true,
	  confirmButtonColor: '#3085d6',
	  cancelButtonColor: '#d33',
	  confirmButtonText: 'Yes, i Confirm !'
	}).then((result) => {
	  if (result.isConfirmed) {
		$('#dlt_rule_btn_'+id).attr('disabled',true);
		$("#loadicon").show();
		jQuery.ajax({
		url: "fix_lane_delete_rule.php",
		data: 'id=' + id,
		type: "POST",
		success: function(data) {
			$("#modal_form_result").html(data);
		},
		error: function() {}
		});
	  }
	})
}
</script>

<?php
$qry_insert = Qry($conn,"SELECT id FROM _access_control WHERE username='$_SESSION[ediary_fix_admin]' AND func_id=(SELECT id FROM 
_access_control_func_list WHERE session_role='8' AND func_name='Fix_Lane') AND u_insert='1'");
			  
if(numRows($qry_insert)>0)
{
?>	

	<div id="new_rule_div">
	
		<div class="row">
		
			<div class="form-group col-md-4">
				<label>Vehicle Number <sup><font color="red">*</font></sup> 
				&nbsp; <input id="rule_vehicle_check" name="rule_vehicle_check" type="checkbox" onchange="Rule_Vehicle()" /></label>
				<input style="font-size:12px !important" readonly oninput="this.value=this.value.replace(/[^a-zA-Z0-9]/,'')" type="text" id="veh_no1" name="tno" class="form-control" required />
			</div>
			
			<div class="form-group col-md-4">
				<label>Consignor <sup><font color="red">*</font></sup> 
				&nbsp; <input id="rule_con1_check" name="rule_con1_check" type="checkbox" onchange="Rule_Con1()" /></label>
				<input style="font-size:12px !important" readonly type="text" oninput="this.value=this.value.replace(/[^a-z A-Z0-9]/,'')" id="con1_fix_find" name="con1" class="form-control" required />
			</div>
			
			<div class="form-group col-md-4">
				<label>Consignee <sup><font color="red">*</font></sup> 
				&nbsp; <input id="rule_con2_check" name="rule_con2_check" type="checkbox" onchange="Rule_Con2()" /></label>
				<input style="font-size:12px !important" readonly type="text" oninput="this.value=this.value.replace(/[^a-z A-Z0-9]/,'')" id="con2_fix_find" name="con2" class="form-control" required />
			</div>
		
		</div>
		
		<input type="hidden" id="con1_id" name="con1_id">
		<input type="hidden" id="con2_id" name="con2_id">
		<input type="hidden" id="lane_id_modal" name="lane_id_modal">
		
		<div class="row">	
		
			<div class="form-group col-md-4">
				<label>Empty/Loaded <sup><font color="red">*</font></sup> 
				&nbsp; <input id="rule_empty_loaded_check" name="rule_empty_loaded_check" type="checkbox" onchange="Rule_EmptyLoaded()" /></label>
				<select style="font-size:12px !important" data-size="4" disabled id="empty_loaded_fix_find" name="empty_loaded" data-live-search="true" class="selectpicker form-control" required>
					<option style="font-size:12px !important" data-tokens="" value="">--select--</option>
					<option data-tokens="0" style="font-size:12px !important" value="0">Empty</option>
					<option data-tokens="1" style="font-size:12px !important" value="1">Loaded</option>
					<option data-tokens="2" style="font-size:12px !important" value="2">Both</option>
				</select>
			</div>
			
			<div class="form-group col-md-4" id="model_div">
				<label>Vehicle Model <sup><font color="red">*</font></sup> 
				&nbsp; <input id="rule_model_check" name="rule_model_check" type="checkbox" onchange="Rule_Model()" /></label>	
				<select style="font-size:12px !important" data-size="8" disabled id="veh_model" name="veh_model" class="selectpicker form-control" data-live-search="true" required>
					<option style="font-size:12px !important" data-tokens="" value="">--select model--</option>
					<?php
					$get_models = Qry($conn,"SELECT model FROM dairy.model_list ORDER BY model ASC");
					
					if(!$get_models){
						errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
						exit();
					}
					
					if(numRows($get_models))
					{
						while($row_model = fetchArray($get_models))
						{
							echo "<option style='font-size:12px !important' data-tokens='$row_model[model]' value='$row_model[model]'>$row_model[model]</option>";
						}
					}
					?>
				</select>
			</div>	
			<!--
			<div class="form-group col-md-4" id="wheeler_div">
				<label>Wheeler <sup><font color="red">*</font></sup> 
				&nbsp; <input id="rule_wheeler_check" name="rule_wheeler_check" type="checkbox" onchange="Rule_Wheeler()" /></label>
				<select disabled id="wheeler_fix_find" name="wheeler" class="form-control" required>
					<option value="">--select--</option>
					<option value="4">4</option>
					<option value="6">6</option>
					<option value="10">10</option>
					<option value="12">12</option>
					<option value="14">14</option>
					<option value="16">16</option>
					<option value="18">18</option>
					<option value="22">22</option>
				</select>
			</div>
			
			<div class="form-group col-md-4" style="display:none" id="db_wheeler_div">
				<label>Wheeler <sup><font color="red">*</font></sup> </label>
				<input type="text" readonly class="form-control" id="db_wheeler" />
			</div>
			-->
			<input type="hidden" readonly id="db_wheeler" />
			 
			<div class="form-group col-md-3">
				<label>&nbsp;</label>
				<br />
				<button id="button_new_rule" style="margin-top:3px;" type="submit" class="btn btn-sm btn-success">
				<span class="fa fa-plus-circle"></span> &nbsp;Add new rule</button>
			</div>
		</div>
		
	</div>
	<?php
	}
	?>
</form>

	<div id="adv_diesel_div" style="display:none">
		<div class="row">
		
			<div class="form-group col-md-3">
				<label>Advance Amount <sup><font color="red">*</font></sup></label>
				<input style="font-size:12px" type="number" id="adv_amount" name="adv_amount" min="1" class="form-control" />
			</div>
			
			<div class="form-group col-md-3">
				<label>Diesel Type <sup><font color="red">*</font></sup></label>
				<select style="font-size:12px" onchange="DieselTypeSel(this.value)" name="diesel_type" id="diesel_type" class="form-control">
					<option style="font-size:12px" value="">--select type--</option>
					<option style="font-size:12px" value="No_Diesel">No diesel</option>
					<option style="font-size:12px" disabled value="Fix_Amount">Fix Amount</option>
					<option style="font-size:12px" value="Fix_Qty">Fix Qty</option>
					<option style="font-size:12px" disabled value="Max_Amount">Max Amount</option>
					<option style="font-size:12px" disabled value="Max_Qty">Max Qty</option>
				</select>
			</div>
			
			<script>
			function DieselTypeSel(elem)
			{
				if(elem=='No_Diesel'){
					$('#dsl_value_div').hide();
				}
				else{
					$('#dsl_value_div').show();
				}
			}
			</script>
			
			<div class="form-group col-md-3" id="dsl_value_div">
				<label>Diesel Amt/Qty <sup><font color="red">*</font></sup></label>
				<input style="font-size:12px" type="number" id="diesel_value" name="diesel_value" min="1" class="form-control" />
			</div>
			
			<input type="hidden" id="adv_diesel_modal_id">
			
			<div class="form-group col-md-3">
				<label>&nbsp;</label>
				<br />
				<button id="button_update_adv_diesel" type="button" onclick="UpdateAdvDiesel()" class="btn btn-sm btn-success">
				<span class="fa fa-pen-alt"></span> Update</button>
				
				<button type="button" onclick="$('#adv_diesel_div').hide();$('#new_rule_div').show();$('.img_right').hide();" class="btn btn-sm btn-danger">
				<span class="fa fa-times-circle"></span> Close</button>
			</div>
		
		</div>
	</div>
	
		<div class="row">
			
			<div class="form-group col-md-12">
				<hr />
			</div>
			
			<div id="modal_form_result"></div>
			
			<div style="overflow:auto" class="form-group col-md-12" id="lane_load_result"></div>
		</div>
     </div> 
     
	<div class="modal-footer">
		<button type="button" onclick="$('#adv_diesel_div').hide();$('#new_rule_div').show();$('.img_right').hide();" class="btn btn-sm btn-danger" id="hide_cash" data-dismiss="modal">Close</button>
	</div>
  </div>
 </div>
</div>
  
<button type="button" style="display:none" id="lane_modal" data-toggle="modal" data-target="#LaneRuleModal"></button> 
  
<script> 
function AdvLoad(id)
{
	$("#loadicon").show();
		jQuery.ajax({
			url: "_fetch_fix_lane_adv_diesel.php",
			data: 'id=' + id,
			type: "POST",
			success: function(data) {
			$("#modal_form_result").html(data);
		},
		error: function() {}
	});
}

function ExpLoad(id,empty_loaded)
{
	if(id!='' && empty_loaded!='')
	{
		$('#exp_modal_rule_id').val(exp_modal_rule_id);
		$('#exp_modal_empty_loaded').val(empty_loaded);
		$('#lane_exp_modal')[0].click();
		ExpLoad2(id,empty_loaded);
		ExpLoad3(id);
	}
}

function AddMultiDelConsignee(rule_id,lane_id)
{
	$('#fix_rule_id_add_con2_modal').val(rule_id);
	$('#fix_lane_id_add_con2_modal').val(lane_id);
	$('#mul_del_consignee_modal_btn')[0].click();
}

function ExpLoad2(id,empty_loaded)
{
	$("#loadicon").show();
		jQuery.ajax({
			url: "_fetch_fix_lane_expense.php",
			data: 'id=' + id + '&empty_loaded=' + empty_loaded,
			type: "POST",
			success: function(data) {
			$("#exp_modal_load_exp").html(data);
		},
		error: function() {}
	});
}

function ExpLoad3(id)
{
	$("#loadicon").show();
		jQuery.ajax({
			url: "_fetch_fix_lane_expense_data.php",
			data: 'id=' + id,
			type: "POST",
			success: function(data) {
			$("#fix_lane_exp_modal_row_data").html(data);
		},
		error: function() {}
	});
}

function UpdateAdvDiesel()
{
	var id = $('#adv_diesel_modal_id').val();
	var adv_amount = $('#adv_amount').val();
	var diesel_type = $('#diesel_type').val();
	var diesel_value = $('#diesel_value').val();
	
	if(id!='')
	{
		if(Number(adv_amount)<0 || adv_amount=='')
		{
			Swal.fire({icon: 'warning',html: '<font size=\'2\' color=\'black\'>Invalid advance amount !</font>',});
		}
		else if(diesel_type=='')
		{
			Swal.fire({icon: 'warning', html: '<font size=\'2\' color=\'black\'>select diesel type first !</font>',});
		}
		else if(diesel_type!='No_Diesel' && Number(diesel_value)<=0)
		{
			Swal.fire({icon: 'warning', html: '<font size=\'2\' color=\'black\'>invalid value for diesel !</font>',});
		}
		else
		{
			$('#button_update_adv_diesel').attr('disabled',true);
			$("#loadicon").show();
				jQuery.ajax({
					url: "fix_lane_update_adv_diesel.php",
					data: 'id=' + id + '&adv_amount=' + adv_amount + '&diesel_type=' + diesel_type + '&diesel_value=' + diesel_value,
					// data: 'id=' + id + '&adv_amount=' + adv_amount,
					type: "POST",
					success: function(data) {
					$("#modal_form_result").html(data);
				},
				error: function() {}
			});
		}
	}
}

function ViewModelVehicles(id,lane_id)
{
	var model = $('#model_name_'+id).val();
	
	if(model!='')
	{
		$("#loadicon").show();
				jQuery.ajax({
					url: "_modal_list_vehicle_model_wise.php",
					data: 'model=' + model + '&lane_id=' + lane_id,
					type: "POST",
					success: function(data) {
					$("#modal_load_div1").html(data);
					$('#example2').DataTable({ 
					"destroy": true, //use for reinitialize datatable
				});
				},
				error: function() {}
		});
	}
}
</script> 
<?php
include "_modal_fix_lane_exp.php";
include "_modal_add_mul_del_consignee.php";
include "footer.php";
?>

<div id="modal_load_div1"></div>