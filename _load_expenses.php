<?php
require_once("connect.php");

?>
	<table id="example" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th>#</th>
                        <th>Expense</th>
                        <th>Exp_Code</th>
                        <th>Allowed For</th>
                        <th>Lock_On_Empty</th>
                        <th>Timestamp</th>
                      </tr>
                    </thead>
                    <tbody>
	<?php
	$get_roles = Qry($conn,"SELECT * FROM dairy.exp_head ORDER BY name ASC");
	
	if(numRows($get_roles)==0)
	{
		echo "<tr>.
			<td colspan='8'>No record found !</td>
			<td style='display: none'></td>
			<td style='display: none'></td>
			<td style='display: none'></td>
			<td style='display: none'></td>
			<td style='display: none'></td>
			<td style='display: none'></td>
			<td style='display: none'></td>
		</tr>";
	}
	else
	{
		$i=1;
		while($row = fetchArray($get_roles))
		{
			$timestamp = date("d-m-y h:i A",strtotime($row['timestamp']));
			
			if($row['visible_to_branch']=="1")
			{
				$role_branch = "checked='checked'";
			}
			else
			{
				$role_branch = "";
			}
		
			if($row['visible_to_supervisor']=="1")
			{
				$role_supervisor = "checked='checked'";
			}
			else
			{
				$role_supervisor = "";
			}
			
			if($row['lock_on_empty']=="1"){
				$lock_empty = "<font color='red'>Yes</font>";
				
				$lock_empty_btn="<button type='button' onclick='UnlockOnEmpty($row[id])' class='btn btn-xs btn-success'>Unlock</button>";
			}else{
				$lock_empty = "No";
				
				$lock_empty_btn="<button type='button' onclick='LockOnEmpty($row[id])' class='btn btn-xs btn-danger'>Lock</button>";
			}
			?>
		<script>			
		$(document).on('click','#branch_chk_<?php echo $row["id"]; ?>',function(){
		var ckbox = $('#branch_chk_<?php echo $row["id"]; ?>');
			
			if (ckbox.is(':checked'))
			{
				$('#loadicon').show();
				jQuery.ajax({
					url: "update_exp_visibility.php",
					data: 'id=' + '<?php echo $row["id"]; ?>' + '&value=' +  '1' + '&type=' +  'branch',
					type: "POST",
					success: function(data) {
					$("#func_result2").html(data);
					},
					error: function() {}
				});
			}
			else
			{
				$('#loadicon').show();
				jQuery.ajax({
					url: "update_exp_visibility.php",
					data: 'id=' + '<?php echo $row["id"]; ?>' + '&value=' +  '0' + '&type=' +  'branch',
					type: "POST",
					success: function(data) {
					$("#func_result2").html(data);
					},
					error: function() {}
				});
			}
		});
		
		$(document).on('click','#supervisor_chk_<?php echo $row["id"]; ?>',function(){
		var ckbox = $('#supervisor_chk_<?php echo $row["id"]; ?>');
			
			if (ckbox.is(':checked'))
			{
				$('#loadicon').show();
				jQuery.ajax({
					url: "update_exp_visibility.php",
					data: 'id=' + '<?php echo $row["id"]; ?>' + '&value=' +  '1' + '&type=' +  'supervisor',
					type: "POST",
					success: function(data) {
					$("#func_result2").html(data);
					},
					error: function() {}
				});
			}
			else
			{
				$('#loadicon').show();
				jQuery.ajax({
					url: "update_exp_visibility.php",
					data: 'id=' + '<?php echo $row["id"]; ?>' + '&value=' +  '0' + '&type=' +  'supervisor',
					type: "POST",
					success: function(data) {
					$("#func_result2").html(data);
					},
					error: function() {}
				});
			}
		});
		</script>	
			<?php
			echo "<tr>
				<td>$i</td>
				<td>$row[name]</td>
				<td>$row[exp_code]</td>";
				// <td><button type='button' onclick=ModifyExp('$row[id]','branch') class='btn btn-xs btn-danger'>$row[cap_amount]</button></td>
				// <td><button type='button' onclick=ModifyExp('$row[id]','supervisor') class='btn btn-xs btn-danger'>$row[cap_amount_supervisor]</button></td>
				echo "<td>
				Branch <input type='checkbox' id='branch_chk_$row[id]' $role_branch />
				Supervisor <input type='checkbox' id='supervisor_chk_$row[id]' $role_supervisor />
				</td>
				<td>$lock_empty_btn</td>
				<td>$timestamp</td>
			</tr>";
		$i++;	
		}
	}
	?>	
                    </tbody>
                  </table>
				  
<script> 
$("#loadicon").fadeOut('slow');
$(document).ready(function() {
    $('#example').DataTable();
} );
</script> 			  