<?php
require_once 'connect.php';

$sql = Qry($conn,"SELECT f.id,s1.name as from_loc,s2.name as to_loc,f.total_rules,f.is_active,f.timestamp 
FROM dairy.fix_lane AS f 
LEFT OUTER JOIN station as s1 ON s1.id=f.from_id 
LEFT OUTER JOIN station as s2 ON s2.id=f.to_id");
if(!$sql){
	echo getMySQLError($conn);
	errorLog(getMySQLError($conn),$conn,$page_url,__LINE__);
	exit();
}

?>
<table id="example" class="table table-bordered table-striped" style="font-size:13px;">
        <thead>
		<tr>
			<th>#</th>
			<th>From</th>
			<th>To</th>
			<th>Total Rules</th>
			<th>Fix Status</th>
			<th>Timestamp</th>
			<th>#</th>
			<th>#</th>
		</tr>
		</thead>
       <tbody id=""> 
		
<?php
if(numRows($sql)==0)
{
	echo "<tr>
		<td colspan='7'>Record not added yet !</td>
	</tr>";  
}
else
{
	$sn=1;
	while($row = fetchArray($sql))
	{
		if($row['timestamp']!=0){
			$timestamp=date("d/m/y H:i A",strtotime($row["timestamp"]));
		}
		else{
			$timestamp="NA";
		}
		
		if($row['is_active']!="0"){
			$is_active="<font color='green'>Active</font>";
		}
		else{
			$is_active="<font color='red'>Inactive</font>";
		}
			echo "<tr id='lane_row_$row[id]'>
				<td>$sn</td>
				<td>$row[from_loc]</td>
				<td>$row[to_loc]</td>
				<td><button type='button' id='rule_view_btn_$row[id]' class='btn btn-primary btn-xs' onclick=LaneRule('$row[id]','$row[total_rules]') title='View Rule: $row[from_loc] to $row[to_loc]' style='cursor: pointer;'>
				<span class='fa fa-hand-point-right'></span> <span id='total_rule_count_$row[id]'>$row[total_rules]</span></button></td>
				<td id='is_active_td_$row[id]'>$is_active</td>
				<td>".$timestamp."</td>
				<input type='hidden' id='from_loc_name_$row[id]' value='$row[from_loc]'>
				<input type='hidden' id='to_loc_name_$row[id]' value='$row[to_loc]'>
				<td>";
				if($row['is_active']=="1"){
					echo "<button type='button' id='btn_id_$row[id]' onclick=DisableLane('$row[id]') class='btn btn_class_act_disable_lane btn-xs btn-danger'><span class='fa fa-trash'></span> &nbsp;Disable</button>";
				}
				else{
					echo "<button type='button' id='btn_id_$row[id]' onclick=ActiveLane('$row[id]') class='btn btn_class_act_disable_lane btn-xs btn-success'><span class='fa fa-check-circle'></span> &nbsp;Active</button>";	
				}
			echo "</td>";
			
			if($row['is_active']=="0"){
					echo "<td><button type='button' id='btn_dlt_lane_$row[id]' onclick=DeleteLane('$row[id]') class='btn btn_delete_lane1 btn-xs btn-danger'><span class='fa fa-trash'></span> &nbsp;Delete</button></td>";
				}
				else{
					echo "<td>--</td>";	
				}
			
		echo "</tr>";
		$sn++;		
    }
}
?>
	</tbody>
		</table>
<script> 
$("#loadicon").hide();
$(document).ready(function() {
    $('#example').DataTable( {
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ]
    } );
} );
</script> 

<?php
$qry_rights_update = Qry($conn,"SELECT id FROM _access_control WHERE username='$_SESSION[ediary_fix_admin]' AND func_id=(SELECT id FROM 
_access_control_func_list WHERE session_role='8' AND func_name='Fix_Lane') AND u_insert='1' AND u_update='1'");
							  
if(numRows($qry_rights_update)==0)
{
	echo "<script>
		$('.btn_class_act_disable_lane').hide();
		$('.btn_class_act_disable_lane').attr('onclick','');
	</script>";
}	

$qry_rights_dlt_lane = Qry($conn,"SELECT id FROM _access_control WHERE username='$_SESSION[ediary_fix_admin]' AND func_id=(SELECT id FROM 
_access_control_func_list WHERE session_role='8' AND func_name='Fix_Lane') AND u_delete='1'");
							  
if(numRows($qry_rights_dlt_lane)==0)
{
	echo "<script>
		$('.btn_delete_lane1').hide();
		$('.btn_delete_lane1').attr('onclick','');
	</script>";
}	
?>