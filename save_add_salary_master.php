<?php
require_once("./connect.php");

$model = escapeString($conn,($_POST['model']));
$salary_amount = escapeString($conn,($_POST['salary_amount']));
$sal_pattern = escapeString($conn,($_POST['sal_pattern']));
$timestamp = date("Y-m-d H:i:s");

echo "<script>$('#add_btn').attr('disabled',true);</script>";

if(empty($model)){
	AlertErrorTopRight("Model not found !");
	echo "<script>$('#add_btn').attr('disabled',false);</script>";
	exit();
}

if(empty($salary_amount)){
	AlertErrorTopRight("SALARY amount not found !");
	echo "<script>$('#add_btn').attr('disabled',false);</script>";
	exit();
}

if($sal_pattern!='0' AND $sal_pattern!='1')
{
	AlertErrorTopRight("SALARY pattern is not valid !");
	echo "<script>$('#add_btn').attr('disabled',false);</script>";
	exit();
}
	
$chk_record = Qry($conn,"SELECT id FROM dairy.salary_master WHERE model='$model' AND salary='$salary_amount' AND type='$sal_pattern'");

if(!$chk_record){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	AlertErrorTopRight("Error while Processing Request !");
	echo "<script>$('#add_btn').attr('disabled',false);</script>";
	exit();
}

if(numRows($chk_record)>0)
{
	AlertErrorTopRight("Duplicate record found !");
	echo "<script>$('#add_btn').attr('disabled',false);</script>";
	exit();
}

// $log_data = "SALARY Updated. $row[salary_amount] to $salary_amount.";

StartCommit($conn);
$flag = true;

$insertQry = Qry($conn,"INSERT INTO dairy.salary_master(model,salary,type,timestamp) VALUES ('$model','$salary_amount','$sal_pattern','$timestamp')");

if(!$insertQry){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

if($flag)
{
	MySQLCommit($conn);
	closeConnection($conn);
	AlertRightCornerSuccess("Added Successfully !");
	echo "<script>
		LoadTable();
		$('#add_btn').attr('disabled',false);
	</script>";
	exit();
}
else
{
	MySQLRollBack($conn);
	closeConnection($conn);
	AlertErrorTopRight("Error while Processing Request !");
	echo "<script>$('#add_btn').attr('disabled',false);</script>";
	exit();
}	
?>