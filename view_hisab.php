<?php
include("header.php");

$chk = Qry($conn,"SELECT id FROM _access_control WHERE username='$_SESSION[ediary_fix_admin]' AND func_id=(SELECT id FROM 
_access_control_func_list WHERE session_role='8' AND func_name='Hisab_View') AND u_view='1'");
			  
if(numRows($chk)==0)
{
	echo "<script>window.location.href='./';</script>";
	exit();
}
?>
<script>
$(function() {
		$("#hisab_tno").autocomplete({
		source: 'autofill/get_tno.php',
		appendTo: "#LaneRuleModal",
		select: function (event, ui) { 
              $('#hisab_tno').val(ui.item.value);   
             // $("#button1").attr('disabled',false);				   
             return false;
		},
		change: function (event, ui) {
		if(!ui.item){
		    $(event.target).val("");
			alert('Vehicle Number does not exists.');
			$("#hisab_tno").val('');
			$("#hisab_tno").focus();
			// $("#button1").attr('disabled',true);
		}}, 
		focus: function (event, ui){
			return false;}
});});
</script>

<div class="content-wrapper">
      <section class="content-header">
          <h1 style="font-size:16px;">View Hisab : </h1>
       </section>
       
	   <section class="content">
          <div class="row">
            <div class="col-xs-12">
			<div class="box">
                <div class="box-body">
				
				<div class="col-md-12">&nbsp;</div>
				
				<div class="col-md-12">
					<div class="row">
						<div class="form-group col-md-2">
							<label>Trip No.</label>
							<input autocomplete="off" oninput="this.value=this.value.replace(/[^0-9]/,'');CheckInput('trip_no');" type="text" class="form-control" id="trip_no" />
						</div>
						
						<div class="form-group col-md-1">
							<label>&nbsp;</label>
							<?php if(!isMobile()) { echo "<br />"; } ?>
							<span style="font-size:12px;color:maroon">-- OR --</span>
						</div>
						
						<div class="form-group col-md-3">
							<label>Vehicle No.</label>
							<input autocomplete="off" type="text" oninput="this.value=this.value.replace(/[^A-Za-z0-9]/,'');CheckInput('tno');" class="form-control" id="hisab_tno" />
						</div>
						
						<div class="form-group col-md-1">
							<label>&nbsp;</label>
							<?php if(!isMobile()) { echo "<br />"; } ?>
							<span style="font-size:12px;color:maroon">-- AND/OR --</span>
						</div>
						
						<div class="form-group col-md-3">
							<label>Date range.</label>
							<select style="font-size:12px" name="duration" id="duration" class="form-control" required>
								<option style="font-size:12px" value="">---SELECT---</option>
								<option style="font-size:12px" value="-0 days">Today's</option>
								<option style="font-size:12px" value="-1 days">Last 2 days</option>
								<option style="font-size:12px" value="-4 days">Last 5 days</option>
								<option style="font-size:12px" value="-6 days">Last 7 days</option>
								<option style="font-size:12px" value="-9 days">Last 10 days</option>
								<option style="font-size:12px" value="-14 days">Last 15 days</option>
								<option style="font-size:12px" value="-29 days">Last 30 days</option>
								<option style="font-size:12px" value="-59 days">Last 60 days</option>
								<option style="font-size:12px" value="-89 days">Last 90 days</option>
								<option style="font-size:12px" value="-119 days">Last 120 days</option>
								<option style="font-size:12px" value="FULL">FULL REPORT</option>
							</select>
						</div>
						
						<div class="form-group col-md-2">
							<?php if(!isMobile()) { echo "<label>&nbsp;</label><br />"; } ?>
							<button type="button" style="margin-top:1px" onclick="SearchHisab()" class="btn btn-sm btn-success <?php if(isMobile()) { echo "btn-block"; } ?>" id="add_btn"><i class="fa fa-search" aria-hidden="true"></i> &nbsp; Search Hisab</button>
						</div>
					</div>
				</div>
				
<script>				
function CheckInput(elem)
{
	if(elem=='trip_no')
	{
		$('#hisab_tno').val('');
		$('#duration').val('');
	}
	else
	{
		$('#trip_no').val('');
	}
}
</script>				
				
				<div class="col-md-12">&nbsp;</div>
				
				<div class="col-md-12 table-responsive" id="load_table">
              
				 </div> 
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

<div id="func_result"></div>  

<script>	
function SearchHisab()
{
	var tno = $('#hisab_tno').val();
	var trip_no = $('#trip_no').val();
	var duration = $('#duration').val();
	
	if(tno=='' && trip_no=='' && duration=='')
	{
		Swal.fire({icon: 'warning',html: '<font size=\'2\' color=\'black\'>Atleast one field is required !</font>',});
	}
	else
	{
		$('#loadicon').show();
			jQuery.ajax({
				url: "_load_hisab_view.php",
				data: 'tno=' + tno + '&trip_no=' + trip_no + '&duration=' + duration,
				type: "POST",
				success: function(data) {
					$("#load_table").html(data);
					$('#example').DataTable({ 
						"destroy": true, //use for reinitialize datatable
					});
				},
				error: function() {}
		});
	}
}

function ViewHisab(trip_no,secret_key)
{
	$('#trip_no_1').val(trip_no);
	$('#secret_key').val(secret_key);
	
	$('#HisabViewForm')[0].submit();
}
</script>

<?php include("footer.php") ?>

<form action="https://rrpl.online/diary/hisab/hisab_view.php" id="HisabViewForm" method="POST" target="_blank">
	<input type="hidden" id="trip_no_1" name="trip_no">
	<input type="hidden" id="secret_key" name="secret_key">
</form>