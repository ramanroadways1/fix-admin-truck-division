<?php
require_once("connect.php");

$date=date("Y-m-d");
$timestamp=date("Y-m-d H:i:s");

$id = escapeString($conn,($_POST['id']));
$rule_id = escapeString($conn,($_POST['rule_id']));
$empty_loaded = escapeString($conn,($_POST['empty_loaded']));

$check_exp = Qry($conn,"SELECT exp_code,exp_name,exp_type,exp_amount,timestamp FROM dairy.fix_lane_exp WHERE id='$id'");

if(!$check_exp){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	AlertError("Error while processing request !");
	exit();
}

$row_exp = fetchArray($check_exp);

$exp_log = "Rule Id: $rule_id. Exp name: $row_exp[exp_name], Exp type: $row_exp[exp_type], Exp amt: $row_exp[exp_amount], Timestamp: $row_exp[timestamp]";

StartCommit($conn);
$flag = true;

$insert_new_exp = Qry($conn,"DELETE FROM dairy.fix_lane_exp WHERE id='$id'");

if(!$insert_new_exp){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

if(AffectedRows($conn)==0){
	$flag = false;
	errorLog("Expense not found.",$conn,$page_name,__LINE__);
}

$update_exp_count = Qry($conn,"UPDATE dairy.fix_lane_rules SET total_exp=total_exp-1 WHERE id='$rule_id'");

if(!$update_exp_count){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

$insertLog = Qry($conn,"INSERT INTO dairy.ediary_admin_log(code,action,desct,timestamp) VALUES ('$rule_id','Fix_Rule_Exp_Delete','$exp_log','$timestamp')");

if(!$insertLog){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

if($flag)
{
	MySQLCommit($conn);
	closeConnection($conn);
	AlertRightCornerSuccess("Expense Deleted !");
	
	echo "<script>
		Number($('#exp_count_$rule_id').html());
		var new_exp_count = Number($('#exp_count_$rule_id').html())-1;
		$('#exp_count_$rule_id').html(new_exp_count);
		ExpLoad2('$rule_id','$empty_loaded');
	</script>";
}
else
{
	MySQLRollBack($conn);
	closeConnection($conn);
	AlertError("Error while processing request !");
	exit();
}	
?>