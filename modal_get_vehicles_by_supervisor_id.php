<?php
require_once("connect.php");

$id = escapeString($conn,($_POST['id']));
$name = escapeString($conn,($_POST['name']));

if(empty($id)){
	AlertRightCornerError("Supervisor not found !");
	exit();
}

$get_vehicles = Qry($conn,"SELECT tno,comp as company FROM dairy.own_truck WHERE superv_id='$id' AND is_sold!='1' ORDER BY id ASC");

if(!$get_vehicles){
	AlertRightCornerError("Error while processing request !");
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	exit();
}
	
if(numRows($get_vehicles)==0)
{
	AlertRightCornerError("No record found !");
	exit();
}
?>
<button id="modal_view_vehicle_btn" style="display:none" data-toggle="modal" data-target="#ModalVehicle"></button>

<div class="modal fade" id="ModalVehicle" role="dialog" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
		<div class="modal-content" style="">
			<div class="modal-header bg-primary">
				<span style="font-size:13px">Supervisor : <?php echo $name; ?></span>
			</div>
	<div class="modal-body">
		<div class="row">
			<div style="height:300px !important" class="col-md-12 table-responsive">
				 <table id="example" class="table table-bordered table-striped">
					<tr>
                        <th>#</th>
                        <th>Vehicle_No</th>
                        <th>Company</th>
                    </tr>
				<?php
$sn=1;				
while($row = fetchArray($get_vehicles))
{
	echo "<tr>
		<td>$sn</td>
		<td>$row[tno]</td>
		<td>$row[company]</td>
	</tr>";
$sn++;	
}
				?>				
				</table>	
			</div>
		</div>
	
		<div class="modal-footer">
			<button type="button" class="btn btn-sm btn-primary" data-dismiss="modal">Close</button>
		</div>
	 
      </div>
    </div>
  </div>
</div>

<script>
$('#modal_view_vehicle_btn')[0].click();	
$('#loadicon').fadeOut('slow');	
</script> 
