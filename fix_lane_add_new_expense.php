<?php
require_once("connect.php");

$date=date("Y-m-d");
$timestamp=date("Y-m-d H:i:s");

$id = escapeString($conn,($_POST['exp_modal_rule_id']));
$empty_loaded = escapeString($conn,($_POST['exp_modal_empty_loaded']));
$exp_array = escapeString($conn,($_POST['exp_list']));
$exp_type = escapeString($conn,($_POST['exp_type']));
$amount = escapeString($conn,($_POST['exp_amount']));
$consignee = escapeString($conn,($_POST['consignee_name']));

$exp_code = explode("_",$exp_array)[0];
$exp_name = explode("_",$exp_array)[1];

if($empty_loaded=="0" AND $exp_type=="On_Weight")
{
	AlertError("On weight exp. not allowed in<br>EMPTY - trip !");
	echo "<script>
		$('#exp_submit_button').attr('disabled',false);
	</script>";
	exit();
}

if($amount<=0)
{
	AlertError("Invalid amount !");
	echo "<script>
		$('#exp_submit_button').attr('disabled',false);
	</script>";
	exit();
}

if($exp_code=='')
{
	AlertError("Expense not found !");
	echo "<script>
		$('#exp_submit_button').attr('disabled',false);
	</script>";
	exit();
}

$check_exp = Qry($conn,"SELECT id FROM dairy.fix_lane_exp WHERE rule_id='$id' AND exp_code='$exp_code' AND consignee='$consignee'");

if(!$check_exp){
	AlertError("Error while processing request !");
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	echo "<script>
		$('#exp_submit_button').attr('disabled',false);
	</script>";
	exit();
}

if(numRows($check_exp)>0)
{
	AlertError("Duplicate record found !");
	echo "<script>
		$('#exp_submit_button').attr('disabled',false);
	</script>";
	exit();
}

StartCommit($conn);
$flag = true;

$insert_new_exp = Qry($conn,"INSERT INTO dairy.fix_lane_exp(rule_id,consignee,exp_code,exp_name,exp_type,exp_amount,timestamp) 
VALUES ('$id','$consignee','$exp_code','$exp_name','$exp_type','$amount','$timestamp')");

if(!$insert_new_exp){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

$update_exp_count = Qry($conn,"UPDATE dairy.fix_lane_rules SET total_exp=total_exp+1 WHERE id='$id'");

if(!$update_exp_count){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

if($flag)
{
	MySQLCommit($conn);
	closeConnection($conn);
	
	AlertRightCornerSuccess("Expense added !");
	echo "<script>
		$('#exp_submit_button').attr('disabled',false);
		Number($('#exp_count_$id').html());
		var new_exp_count = Number($('#exp_count_$id').html())+1;
		$('#exp_count_$id').html(new_exp_count);
		ExpLoad2('$id','$empty_loaded');
		ExpLoad3('$id');
		$('#exp_list').val('');
		$('#exp_type').val('');
		$('#exp_amount').val('');
		$('#exp_list').selectpicker('refresh');
	</script>";
}
else
{
	MySQLRollBack($conn);
	closeConnection($conn);
	AlertError("Error while processing request !");
	echo "<script>
		$('#exp_submit_button').attr('disabled',false);
		$('#exp_list').val('');
		$('#exp_type').val('');
		$('#exp_amount').val('');
	</script>";
	exit();
}	
?>