<?php
require_once 'connect.php';

$sql = Qry($conn,"SELECT icode,igroup,iname,itimestamp as timestamp FROM lritems");

if(!$sql){
	echo getMySQLError($conn);
	errorLog(getMySQLError($conn),$conn,$page_url,__LINE__);
	exit();
}
	
if(numRows($sql)==0)
{
	AlertRightCornerError("No record found !");
	exit();
}
	?>
	<table id="example" class="table table-bordered table-striped" style="font-size:13px;">
        <thead>
		<tr>
			  <th>#</th>
			  <th>Code</th>
			  <th>Group</th>
			  <th>Item_Name</th>
			  <th>Timestamp</th>
		</tr>
		</thead>
    <tbody id=""> 
	
<?php
$sn=1;

while($row = fetchArray($sql))
{	
	$timestamp = date('d-m-y h:i A', strtotime($row['timestamp']));
	
	echo "<tr>	
			<td>$sn</td>
			<td>$row[icode]</td>
			<td>$row[igroup]</td>
			<td>$row[iname]</td>
			<td>$timestamp</td>
		</tr>";
$sn++;		
}
	echo "</tbody>
</table>";
?>
	
<script> 
	$("#loadicon").fadeOut('slow');
</script> 
