<?php
require_once("./connect.php");

$id = escapeString($conn,strtoupper($_POST['id']));

$delete_stock = Qry($conn,"DELETE FROM lr_bank WHERE id='$id' AND stock=stock_left");

if(!$delete_stock){
	AlertRightCornerError("Error while processing request !");
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	exit();
}
	
if(AffectedRows($conn)==0)
{
	AlertRightCornerError("Unable to delete !");
	echo "<script'>
		$('#del_button$id').attr('disabled',false);
	</script>";
	exit();
}

AlertRightCornerSuccessFadeFast("Deleted successfully !");

echo "<script>
	 LoadTable();
</script>";
		
?>