<?php
require_once("connect.php");

$id = escapeString($conn,$_POST['id']);
$date = date("Y-m-d");

$get_data = Qry($conn,"SELECT m.date,m.lrdate,m.bilty_no,m.lr_by,m.billing_type,m.veh_placer,m.plr,m.broker,m.billing_party,m.tno,m.frmstn,
m.tostn,m.awt,m.cwt,m.rate,m.tamt,m.branch,m.from_id,m.to_id,m.broker_id,m.bill_party_id,b.pan as broker_pan,p.pan as bill_party_pan 
FROM mkt_bilty AS m 
LEFT OUTER JOIN dairy.broker AS b ON b.id = m.broker_id 
LEFT OUTER JOIN dairy.broker AS p ON p.id = m.bill_party_id
WHERE m.id='$id'");

if(!$get_data)
{
	AlertErrorTopRight("Error while processing request !");
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	exit();
}

if(numRows($get_data) == 0)
{
	AlertErrorTopRight("LR not found !");
	exit();
}

$row = fetchArray($get_data);

$bill_type1 = $row['billing_type'];
$lr_by1 = $row['lr_by'];

$bilty_no = $row['bilty_no'];
$lrdate = $row['lrdate'];
$plr = $row['plr'];
$mb_from = $row['frmstn'];
$mb_to = $row['tostn'];
$mb_from_id = $row['from_id'];
$mb_to_id = $row['to_id'];
$mb_broker = $row['broker'];
$broker_id = $row['broker_id'];
$mb_broker_pan = $row['broker_pan'];
$bill_party_id = $row['bill_party_id'];
$mb_bill_party = $row['billing_party'];
$bill_party_pan = $row['bill_party_pan'];
$mb_lr_by = $row['lr_by'];
$billing_type = $row['billing_type'];
$act_weight = $row['awt'];
$charge_weight = $row['cwt'];
$rate_pmt = $row['rate'];
$freight_bilty = $row['tamt'];
$mb_veh_placer = $row['veh_placer'];
?>
 
<script> 
$('#modal_id').val('<?php echo $id; ?>');
$('#bilty_no1').html('<?php echo $bilty_no; ?>');
$('#mb_lr_date').val('<?php echo $lrdate; ?>');
$('#mb_plr').val('<?php echo $plr; ?>');
$('#mb_from').val('<?php echo $mb_from; ?>');
$('#mb_to').val('<?php echo $mb_to; ?>');
$('#mb_from_id').val('<?php echo $mb_from_id; ?>');
$('#mb_to_id').val('<?php echo $mb_to_id; ?>');
$('#mb_broker').val('<?php echo $mb_broker; ?>');
$('#mb_broker_pan').val('<?php echo $mb_broker_pan; ?>');
$('#broker_id').val('<?php echo $broker_id; ?>');
$('#mb_bill_party').val('<?php echo $mb_bill_party; ?>');
$('#billing_party_pan').val('<?php echo $bill_party_pan; ?>');
$('#billing_party_id').val('<?php echo $bill_party_id; ?>');
$('#mb_lr_by').val('<?php echo $mb_lr_by; ?>');
$('#billing_type').val('<?php echo $billing_type; ?>');
$('#act_weight').val('<?php echo $act_weight; ?>');
$('#awt_db').val('<?php echo $act_weight; ?>');
$('#charge_weight').val('<?php echo $charge_weight; ?>');
$('#cwt_db').val('<?php echo $charge_weight; ?>');
$('#rate_pmt').val('<?php echo $rate_pmt; ?>');
$('#rate_db').val('<?php echo $rate_pmt; ?>');
$('#freight_bilty').val('<?php echo $freight_bilty; ?>');
$('#freight_db').val('<?php echo $freight_bilty; ?>');
$('#mb_veh_placer').val('<?php echo $mb_veh_placer; ?>');

BillingType('<?php echo $bill_type1; ?>');
LRBy('<?php echo $lr_by1; ?>');

$('#modal_open_btn')[0].click();
$('#loadicon').fadeOut('slow');
</script> 