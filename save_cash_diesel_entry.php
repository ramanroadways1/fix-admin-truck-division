<?php
require_once("./connect.php");

$timestamp = date("Y-m-d H:i:s"); 

$trip_id = escapeString($conn,strtoupper($_POST['id']));

if(empty($trip_id) || $trip_id=='' || $trip_id==0)
{
	AlertRightCornerError("Trip not found..");
	echo "<script>$('#entry_save_btn').attr('disabled',false);</script>";
	exit();
}

$check_trip = Qry($conn,"SELECT t.id,t.tno,t.driver_code,t.trip_no,t.lr_type,t.loaded_hisab,d.name as driver_name 
FROM dairy.trip AS t 
LEFT OUTER JOIN dairy.driver AS d ON d.code=t.driver_code 
WHERE t.id='$trip_id'");

if(!$check_trip){
	AlertRightCornerError("Error..");
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	echo "<script>$('#entry_save_btn').attr('disabled',false);</script>";
	exit();
}

if(numRows($check_trip)==0)
{
	AlertRightCornerError("Trip not found..");
	errorLog("Running trip not found. TripId: $trip_id.",$conn,$page_name,__LINE__);
	echo "<script>$('#entry_save_btn').attr('disabled',false);</script>";
	exit();
}

$row_trip = fetchArray($check_trip);

$tno = $row_trip['tno'];
$branch = escapeString($conn,strtoupper($_POST['branch']));
$code = $row_trip['driver_code'];
$trip_no = $row_trip['trip_no'];

$diesel_key = $tno."_".mt_rand().$timestamp;

$amount = escapeString($conn,$_POST['amount']);
$rate = escapeString($conn,$_POST['rate']);
$qty = escapeString($conn,$_POST['qty']);

$check_fix_lane = Qry($conn,"SELECT id FROM dairy.trip WHERE id='$trip_id' AND fix_lane='1'");

if(!$check_fix_lane){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	AlertRightCornerError("Error..");
	exit();
}

if(numRows($check_fix_lane)>0)
{
	AlertRightCornerError("Trip belongs to fix lane.");
	echo "<script>$('#entry_save_btn').attr('disabled',false);</script>";
	exit();
}

$check_cache = Qry($conn,"SELECT id FROM dairy.hisab_cache WHERE tno='$tno'");

if(!$check_cache){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	AlertRightCornerError("Error..");
	exit();
}

if(numRows($check_cache)>0)
{
	AlertRightCornerError("Vehicle Hisab is in process. Please complete or reset hisab first !");
	echo "<script>$('#entry_save_btn').attr('disabled',false);</script>";
	exit();
}

$check_cache_trip = Qry($conn,"SELECT id FROM dairy.trip_cache WHERE tno='$tno'");

if(!$check_cache_trip){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	AlertRightCornerError("Error..");
	exit();
}

if(numRows($check_cache_trip)>0)
{
	AlertRightCornerError("Please wait ! Try again after some time !");
	echo "<script>$('#entry_save_btn').attr('disabled',false);</script>";
	exit();
}

$check_running_trip = Qry($conn,"SELECT id FROM dairy.running_scripts WHERE file_name!='LOAD_API_TRANS'");

if(!$check_running_trip){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	AlertRightCornerError("Error..");
	exit();
}

if(numRows($check_running_trip)>0)
{
	AlertRightCornerError("Please wait ! Try again after some time !");
	echo "<script>$('#entry_save_btn').attr('disabled',false);</script>";
	exit();
}

$date = escapeString($conn,$_POST['txn_date']);

$trans_id_Qry = GetTxnId_eDiary($conn,"DSL");
	
if(!$trans_id_Qry || $trans_id_Qry=="" || $trans_id_Qry=="0"){
	AlertRightCornerError("Error !!");
	echo "<script>$('#entry_save_btn').attr('disabled',false);</script>";
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	exit();
}
	
$trans_id = $trans_id_Qry;
	
StartCommit($conn);
$flag = true;
	
$insert_diesel_diary = Qry($conn,"INSERT INTO dairy.diesel (trip_id,trip_no,trans_id,unq_id,tno,rate,qty,amount,date,cash_diesel,narration,branch,
timestamp) VALUES ('$trip_id','$trip_no','$trans_id','$diesel_key','$tno','$rate','$qty','$amount','$date','1','XXXXX','$branch','$timestamp')");

if(!$insert_diesel_diary)
{
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}	

$insert_diesel_entry = Qry($conn,"INSERT INTO dairy.diesel_entry (unq_id,tno,diesel,card_pump,card,dsl_company,narration,branch,date,timestamp) VALUES 
('$diesel_key','$tno','$amount','PUMP','XXXXXX','XXXXXX','XXXXXX','$branch','$date','$timestamp')");

if(!$insert_diesel_entry)
{
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}	

$update_trip = Qry($conn,"UPDATE dairy.trip SET diesel=diesel+'$amount',diesel_qty=ROUND(diesel_qty+'$qty',2) WHERE id='$trip_id'");

if(!$update_trip)
{
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}	

$fetch_balance = Qry($conn,"SELECT id,amount_hold FROM dairy.driver_up WHERE down=0 AND code='$code' ORDER BY id DESC LIMIT 1");

if(!$fetch_balance)
{
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

if(numRows($fetch_balance)==0)
{
	$flag = false;
	errorLog("Driver not found. Driver Code: $code.",$conn,$page_name,__LINE__);
}

$row_bal = fetchArray($fetch_balance);

$amount_hold = $row_bal['amount_hold'];
$driver_up_id = $row_bal['id'];

$newbal = $amount_hold-$amount;

$insert_into_book=Qry($conn,"INSERT INTO dairy.driver_book(driver_code,tno,trip_id,trip_no,trans_id,desct,debit,balance,date,branch,
timestamp) VALUES ('$code','$tno','$trip_id','$trip_no','$trans_id','CASH_DIESEL','$amount','$newbal','$date','$branch','$timestamp')");

if(!$insert_into_book)
{
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

$update_driver_balance = Qry($conn,"UPDATE dairy.driver_up SET amount_hold='$newbal' WHERE id='$driver_up_id'");

if(!$update_driver_balance)
{
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

if($flag)
{
	MySQLCommit($conn);
	closeConnection($conn);
	AlertRightCornerSuccess("Cash Diesel Entry Success !");
	echo "<script>$('#modal_close_btn')[0].click();$('#add_btn')[0].click();$('#entry_save_btn').attr('disabled',false);</script>";
	exit();
}
else 
{
	MySQLRollBack($conn);
	closeConnection($conn);
	AlertRightCornerError("Error while processing request !!");
	echo "<script>$('#entry_save_btn').attr('disabled',false);</script>";
	exit();
}
?>