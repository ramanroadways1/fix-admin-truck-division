<?php
require_once("connect.php");

$timestamp = date("Y-m-d H:i:s");
$id = escapeString($conn,($_POST['id']));

echo "<script>
	$('#dlt_btn_$id').attr('disabled',true);
</script>";

$get_data = Qry($conn,"SELECT * FROM dairy.bilty_book WHERE id='$id'");

if(!$get_data){
	AlertErrorTopRight("Error while processing request !");
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	exit();
}

if(numRows($get_data) == 0)
{
	AlertErrorTopRight("Txn not found !");
	exit();
}

$log_record = array();

while($row_data = fetchArray($get_data))
{
    foreach($row_data as $key => $value)
    {
		$log_record[]=$key."->".$value;
    }
}

$log_record = implode(', ',$log_record); 

StartCommit($conn);
$flag = true;

$get_txn_data = Qry($conn,"SELECT trans_id,bilty_no,date,credit,debit,branch,trans_type FROM dairy.bilty_book WHERE id='$id'");

if(!$get_txn_data){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

$row_txn = fetchArray($get_txn_data);

$get_next_txn = Qry($conn,"SELECT id FROM dairy.bilty_book WHERE id>'$id' AND bilty_no='$row_txn[bilty_no]'");

if(!$get_next_txn){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

if(numRows($get_next_txn) > 0)
{
	if($row_txn['credit']>0)
	{
		$update_balance = Qry($conn,"UPDATE dairy.bilty_book SET balance=balance-'$row_txn[credit]' WHERE id>'$id' AND bilty_no='$row_txn[bilty_no]'");
		
		if(!$update_balance){
			$flag = false;
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		}
	}
	
	if($row_txn['debit']>0)
	{
		$update_balance = Qry($conn,"UPDATE dairy.bilty_book SET balance=balance+'$row_txn[debit]' WHERE id>'$id' AND bilty_no='$row_txn[bilty_no]'");
		
		if(!$update_balance){
			$flag = false;
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		}
	}
}

if($row_txn['credit']>0)
{
	$update_balance2 = Qry($conn,"UPDATE dairy.bilty_balance SET freight=freight-'$row_txn[credit]',balance=balance-'$row_txn[credit]' WHERE 
	bilty_no='$row_txn[bilty_no]'");
		
	if(!$update_balance2){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
}
	
if($row_txn['debit']>0)
{
	$update_balance2 = Qry($conn,"UPDATE dairy.bilty_balance SET rcvd=rcvd-'$row_txn[debit]',balance=balance+'$row_txn[debit]' WHERE 
	bilty_no='$row_txn[bilty_no]'");
		
	if(!$update_balance2){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
}
	
$insertLog = Qry($conn,"INSERT INTO dairy.ediary_admin_log(code,tno,action,desct,branch,username,timestamp) VALUES ('$id','$row[bilty_no]',
'Market_Bilty_Txn_Delete','$log_record','Fix_Admin','$_SESSION[ediary_fix_admin]','$timestamp')");

if(!$insertLog){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

if($row_txn['trans_type']=='CASH')
{
	$get_cash_entry = Qry($conn,"SELECT id,comp,credit FROM cashbook WHERE date='$row_txn[date]' AND user='$row_txn[branch]' AND vou_type='CREDIT-FREIGHT' 
	AND desct like '%$row_txn[bilty_no]%'");
	
	if(!$get_cash_entry){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
	
	if(numRows($get_cash_entry)==0)
	{
		$flag = false;
		errorLog("Cashbook credit entry not found. Biltyno : $row_txn[bilty_no]",$conn,$page_name,__LINE__);
	}
	
	$row_cashbook = fetchArray($get_cash_entry);
	
	$cashbook_id = $row_cashbook['id'];
	$cashbook_amount = $row_cashbook['credit'];
	$cashbook_company = $row_cashbook['comp'];
	
	if($cashbook_company=='RRPL')
	{
		$balance_col = "balance";
	}
	else
	{
		$balance_col = "balance2";
	}
	
	$update_cashbook_entries = Qry($conn,"UPDATE cashbook SET `$balance_col`=`$balance_col`-'$cashbook_amount' WHERE id>'$cashbook_id' AND 
	user='$row_txn[branch]' AND comp='$cashbook_company'");
	
	if(!$update_cashbook_entries){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
	
	$dlt_cash_entry = Qry($conn,"DELETE FROM cashbook WHERE id='$cashbook_id'");
	
	if(!$dlt_cash_entry){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
	
	if(AffectedRows($conn)==0)
	{
		$flag = false;
		errorLog("Cashbook entry not deleted.",$conn,$page_name,__LINE__);
	}
	
	$update_bal = Qry($conn,"UPDATE user SET `$balance_col`=`$balance_col`-'$cashbook_amount' WHERE username='$row_txn[branch]'");
	
	if(!$update_bal){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
	
	$dlt_cr_entry = Qry($conn,"DELETE FROM credit WHERE cash_id='$cashbook_id'");
	
	if(!$dlt_cr_entry){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
	
	if(AffectedRows($conn)==0)
	{
		$flag = false;
		errorLog("Credit entry not deleted.",$conn,$page_name,__LINE__);
	}
}

$delete_record = Qry($conn,"DELETE FROM dairy.bilty_book WHERE id='$id'");

if(!$delete_record){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}
		
if($flag)
{ 
	MySQLCommit($conn);
	closeConnection($conn);	
	AlertRightCornerSuccess("Deleted Successfully.");
	echo "<script>
		$('#dlt_btn_$id').attr('onclick','');
		$('#dlt_btn_$id').attr('disabled',true);
		$('#add_btn')[0].click(); 
	</script>";
	exit();
}
else
{
	MySQLRollBack($conn);
	closeConnection($conn);
	AlertError("Error while processing request..");
	echo "<script>$('#dlt_btn_$id').attr('disabled',false);</script>";
	exit();
}
?>