<?php
require_once 'connect.php';

$driver_code = escapeString($conn,($_POST['driver_code']));
$tno = escapeString($conn,($_POST['tno']));

$from_date = date('Y-m-d', strtotime(escapeString($conn,$_POST['duration']), strtotime(date("Y-m-d"))));
$to_date = date("Y-m-d");

if($driver_code!='' AND $tno=='')
{
	$sql = Qry($conn,"SELECT r.driver_code,r.tno,r.trip_no,r.from_date,r.to_date,r.amount,r.branch,r.date,r.narration,d.name as driver_name 
	FROM dairy.salary AS r 
	LEFT OUTER JOIN dairy.driver AS d ON d.code = r.driver_code 
	WHERE r.date BETWEEN '$from_date' AND '$to_date' AND r.driver_code='$driver_code' ORDER BY r.id ASC");
}
else if($tno!='' AND $driver_code=='')
{
	$sql = Qry($conn,"SELECT r.driver_code,r.tno,r.trip_no,r.from_date,r.to_date,r.amount,r.branch,r.date,r.narration,d.name as driver_name 
	FROM dairy.salary AS r 
	LEFT OUTER JOIN dairy.driver AS d ON d.code = r.driver_code 
	WHERE r.date BETWEEN '$from_date' AND '$to_date' AND r.tno='$tno' ORDER BY r.id ASC");
}
else if($tno!='' AND $driver_code!='')
{
	$sql = Qry($conn,"SELECT r.driver_code,r.tno,r.trip_no,r.from_date,r.to_date,r.amount,r.branch,r.date,r.narration,d.name as driver_name 
	FROM dairy.salary AS r 
	LEFT OUTER JOIN dairy.driver AS d ON d.code = r.driver_code 
	WHERE r.date BETWEEN '$from_date' AND '$to_date' AND r.driver_code='$driver_code' AND r.tno='$tno' ORDER BY r.id ASC");
}
else
{
	AlertRightCornerError("Invalid inputs !");
	exit();
}

if(!$sql){
	echo getMySQLError($conn);
	errorLog(getMySQLError($conn),$conn,$page_url,__LINE__);
	exit();
}
	
if(numRows($sql)==0)
{
	AlertRightCornerError("No record found !");
	exit();
}
	?>
	<table id="example" class="table table-bordered table-striped" style="font-size:13px;">
        <thead>
		<tr>
			<th>Vehicle_No</th>
			<th>Driver</th>
			<th>Trip_No</th>
			<th>From Date</th>
			<th>To Date</th>
			<th>Amount</th>
			<th>Salary Date</th>
			<th>Branch</th>
			<th>Narration</th>
		</tr>
		</thead>
    <tbody id=""> 
	
<?php
while($row = fetchArray($sql))
{	
		$txn_date = date('d-m-y', strtotime($row['date']));
		$from_sal_date = date('d-m-y', strtotime($row['from_date']));
		$to_sal_date = date('d-m-y', strtotime($row['to_date']));
	
		echo "<tr>	
			<td>$row[tno]</td>
			<td>$row[driver_name]<br>($row[driver_code])</td>
			<td>$row[trip_no]</td>
			<td>$from_sal_date</td>
			<td>$to_sal_date</td>
			<td>$row[amount]</td>
			<td>$txn_date</td>
			<td>$row[branch]</td>
			<td>$row[narration]</td>
		</tr>";
}
	echo "</tbody>
</table>";
?>
	
<script> 
	$("#loadicon").fadeOut('slow');
</script> 
