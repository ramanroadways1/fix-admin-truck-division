<?php
require_once("connect.php");

$id = escapeString($conn,($_POST['id']));

if(empty($id)){
	AlertRightCornerError("Trip id not found !");
	exit();
}

$get_trip = Qry($conn,"SELECT t.from_station,t.to_station FROM dairy.trip AS t WHERE id='$id'");

if(!$get_trip){
	AlertRightCornerError("Error while processing request !");
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	exit();
}
	
if(numRows($get_trip)==0)
{
	AlertRightCornerError("No record found !");
	exit();
}

$row = fetchArray($get_trip);

$from_station = $row['from_station'];
$to_station = $row['to_station'];
?>
<button id="modal_opn_btn" style="display:none" data-toggle="modal" data-target="#ModalTripView"></button>

<div class="modal fade" id="ModalTripView" role="dialog" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-xl-mini">
		<div class="modal-content" style="">
			<div class="modal-header bg-primary">
				<span style="font-size:13px">Trip : <?php echo $from_station." to ".$to_station; ?></span>
			</div>
	<div class="modal-body">
		<div class="row" style="height:400px !important;overflow:auto">
			<div class="col-md-6 table-responsive">
				 <table class="table table-bordered table-striped" style="font-size:12px">
					<tr>
						<th style="letter-spacing:1px;font-size:13.5px" class="bg-success" colspan="6">Advance/Naame/Freight</th>
					</tr>
					<tr>
                        <th>Txn_Type</th>
                        <th>Vou_No</th>
                        <th>Amount</th>
                        <th>Date</th>
                        <th>Narration</th>
                        <th>Branch</th>
                    </tr>
				<?php
			$get_cash = Qry($conn,"SELECT trans_type,vou_no,amount,date,narration,branch FROM dairy.cash WHERE trip_id='$id'");	
			$get_rtgs = Qry($conn,"SELECT 'RTGS' as trans_type,vou_no,amount,date,narration,branch FROM dairy.rtgs WHERE trip_id='$id'");	
			$get_frt_adv = Qry($conn,"SELECT 'FT' as trans_type,vou_id as vou_no,amount,date,narration,branch FROM dairy.freight_adv WHERE trip_id='$id'");	
			$get_naame = Qry($conn,"SELECT naame_type as trans_type,lrno as vou_no,amount,date,narration,branch FROM dairy.driver_naame WHERE trip_id='$id'");	
			
			$total_adv = 0;
			
			if(numRows($get_cash)>0)
			{
				while($row_adv = fetchArray($get_cash))
				{
					$total_adv = $total_adv+$row_adv['amount'];
					
					$date = date('d/m/y', strtotime($row_adv['date']));
						
					if($row_adv['trans_type']=='CASH'){
						$txn_type="Cash";
					}else{
						$txn_type="Atm";
					}
						
					echo "<tr>
						<td>$txn_type</td>
						<td>$row_adv[vou_no]</td>
						<td>$row_adv[amount]</td>
						<td>$date</td>
						<td>$row_adv[narration]</td>
						<td>$row_adv[branch]</td>
					</tr>";
				}
			}
			
			if(numRows($get_rtgs)>0)
			{
				while($row_rtgs = fetchArray($get_rtgs))
				{
					$date = date('d/m/y', strtotime($row_rtgs['date']));
					
					$total_adv = $total_adv+$row_rtgs['amount'];
					
					echo "<tr>
						<td>RTGS</td>
						<td>$row_rtgs[vou_no]</td>
						<td>$row_rtgs[amount]</td>
						<td>$date</td>
						<td>$row_rtgs[narration]</td>
						<td>$row_rtgs[branch]</td>
					</tr>";
				}
			}
			
			if(numRows($get_frt_adv)>0)
			{
				while($row_frt = fetchArray($get_frt_adv))
				{
					$total_adv = $total_adv+$row_frt['amount'];
					
					$date = date('d/m/y', strtotime($row_frt['date']));
						
					echo "<tr>
						<td>$row_frt[trans_type]</td>
						<td>$row_frt[vou_no]</td>
						<td>$row_frt[amount]</td>
						<td>$date</td>
						<td>$row_frt[narration]</td>
						<td>$row_frt[branch]</td>
					</tr>";
				}
			}
			
			if(numRows($get_naame)>0)
			{
				while($row_naame = fetchArray($get_naame))
				{
					$date = date('d/m/y', strtotime($row_naame['date']));
					
					$total_adv = $total_adv+$row_naame['amount'];
					
					echo "<tr>
						<td>$row_naame[trans_type]</td>
						<td>$row_naame[vou_no]</td>
						<td>$row_naame[amount]</td>
						<td>$date</td>
						<td>$row_naame[narration]</td>
						<td>$row_naame[branch]</td>
					</tr>";
				}
			}
			
				?>
		<tr>
			<td colspan="2"><b>Total : </b></td>
			<td><b><?php echo $total_adv; ?></b></td>
			<td></td>
			<td></td>
			<td></td>
		</tr>	
				</table>	
			</div>
			
		<div class="col-md-6 table-responsive">
				 <table class="table table-bordered table-striped" style="font-size:12px">
					<tr>
						<th style="letter-spacing:1px;font-size:13.5px" class="bg-success" colspan="6">Freight Collected</th>
					</tr>
					<tr>
                        <th>Vou_No</th>
                        <th>Amount</th>
                        <th>Date</th>
                        <th>Narration</th>
                        <th>Branch</th>
                    </tr>
						<?php
					$get_frt_coll = Qry($conn,"SELECT vou_id as vou_no,amount,date,narration,branch FROM dairy.freight_rcvd WHERE trip_id='$id'");	
					
					$total_collected = 0;
					
					if(numRows($get_frt_coll)>0)
					{
						while($row_ft_coll = fetchArray($get_frt_coll))
						{
							$total_collected = $total_collected+$row_ft_coll['amount'];
							$date = date('d/m/y', strtotime($row_ft_coll['date']));
								
							echo "<tr>
								<td>$row_ft_coll[vou_no]</td>
								<td>$row_ft_coll[amount]</td>
								<td>$date</td>
								<td>$row_ft_coll[narration]</td>
								<td>$row_ft_coll[branch]</td>
							</tr>";
						}
					}
					?>
				<tr>
					<td><b>Total : </b></td>
					<td><b><?php echo $total_collected; ?></b></td>
					<td></td>
					<td></td>
					<td></td>
				</tr>	
			</table>	
		</div>	
		
		<div class="col-md-7 table-responsive">
				 <table class="table table-bordered table-striped" style="font-size:12px">
					<tr>
						<th style="letter-spacing:1px;font-size:13.5px" class="bg-success" colspan="6">Diesel</th>
					</tr>
					<tr>
                        <th>Rate</th>
                        <th>Qty</th>
                        <th>Amount</th>
                        <th>Date</th>
                        <th>Narration</th>
                        <th>Branch</th>
                    </tr>
						<?php
					$get_diesel = Qry($conn,"SELECT rate,qty,amount,date,cash_diesel,narration,branch FROM dairy.diesel WHERE trip_id='$id'");	
					
					$diesel_amt = 0;
					$diesel_qty = 0;
					
					if(numRows($get_diesel)>0)
					{
						while($row_diesel = fetchArray($get_diesel))
						{
							$diesel_amt = $diesel_amt+$row_diesel['amount'];
							$diesel_qty = $diesel_qty+$row_diesel['qty'];
							
							$date = date('d/m/y', strtotime($row_diesel['date']));
							
							if($row_diesel['cash_diesel']=="1"){
								$narration = "<font color='blue'>Cash_diesel:</font> $row_diesel[narration]";
							}
							else{
								$narration = "$row_diesel[narration]";
							}
							
							echo "<tr>
								<td>$row_diesel[rate]</td>
								<td>$row_diesel[qty]</td>
								<td>$row_diesel[amount]</td>
								<td>$date</td>
								<td>$narration</td>
								<td>$row_diesel[branch]</td>
							</tr>";
						}
					}
					?>
				<tr>
					<td><b>Total : </b></td>
					<td><b><?php echo $diesel_qty; ?></b></td>
					<td><b><?php echo $diesel_amt; ?></b></td>
					<td></td>
					<td></td>
					<td></td>
				</tr>	
			</table>	
		</div>	
		
			<div class="col-md-5 table-responsive">
				 <table class="table table-bordered table-striped" style="font-size:12px">
					<tr>
						<th style="letter-spacing:1px;font-size:13.5px" class="bg-success" colspan="6">Expense</th>
					</tr>
					<tr>
                        <th>Exp.</th>
                        <th>Amount</th>
                        <th>Date</th>
                        <th>Narration</th>
                        <th>Branch</th>
                    </tr>
					<?php
					$get_exp = Qry($conn,"SELECT exp_name,amount,date,narration,branch FROM dairy.trip_exp WHERE trip_id='$id'");	
					
					$total_exp = 0;
					
					if(numRows($get_exp)>0)
					{
						while($row_exp = fetchArray($get_exp))
						{
							$total_exp = $total_exp+$row_exp['amount'];
							
							$date = date('d/m/y', strtotime($row_exp['date']));
							
							echo "<tr>
								<td>$row_exp[exp_name]</td>
								<td>$row_exp[amount]</td>
								<td>$date</td>
								<td>$row_exp[narration]</td>
								<td>$row_exp[branch]</td>
							</tr>";
						}
					}
					?>
				<tr>
					<td><b>Total : </b></td>
					<td><b><?php echo $total_exp; ?></b></td>
					<td></td>
					<td></td>
					<td></td>
				</tr>	
			</table>	
		</div>	
			
		</div>
	</div>
	
		<div class="modal-footer">
			<button type="button" class="btn btn-sm btn-primary" data-dismiss="modal">Close</button>
		</div>
	  </div>
    </div>
  </div>

<script>
$('#modal_opn_btn')[0].click();	
$('#loadicon').fadeOut('slow');	
</script> 
