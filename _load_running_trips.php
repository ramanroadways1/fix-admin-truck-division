<?php
require_once 'connect.php';

$trip_no = escapeString($conn,($_POST['trip_no']));
$tno = escapeString($conn,($_POST['tno']));

if($trip_no!='')
{
	$sql = Qry($conn,"SELECT t.id,t.trip_no,t.tno,t.branch,t.edit_branch,t.from_station,t.to_station,t.from_pincode,t.to_pincode,t.fix_lane,
	t.act_wt,t.charge_wt,t.lr_type,t.lrno,t.date,t.end_date,t.cash,t.rtgs,t.diesel,t.diesel_qty,t.expense,t.toll_tax,tu.name as 
	trip_user,te.name as trip_end_user,d.name as driver_name 
	FROM dairy.trip AS t 
	LEFT OUTER JOIN dairy.driver AS d ON d.code = t.driver_code 
	LEFT OUTER JOIN emp_attendance AS tu ON tu.code = t.branch_user 
	LEFT OUTER JOIN emp_attendance AS te ON te.code = t.trip_end_user 
	WHERE t.trip_no='$trip_no'");
}
else
{
	$sql = Qry($conn,"SELECT t.id,t.trip_no,t.tno,t.branch,t.edit_branch,t.from_station,t.to_station,t.from_pincode,t.to_pincode,t.fix_lane,
	t.act_wt,t.charge_wt,t.lr_type,t.lrno,t.date,t.end_date,t.cash,t.rtgs,t.diesel,t.diesel_qty,t.expense,t.toll_tax,tu.name as 
	trip_user,te.name as trip_end_user,d.name as driver_name 
	FROM dairy.trip AS t 
	LEFT OUTER JOIN dairy.driver AS d ON d.code = t.driver_code 
	LEFT OUTER JOIN emp_attendance AS tu ON tu.code = t.branch_user 
	LEFT OUTER JOIN emp_attendance AS te ON te.code = t.trip_end_user 
	WHERE t.tno='$tno'");
}

if(!$sql){
	errorLog(getMySQLError($conn),$conn,$page_url,__LINE__);
	exit();
}
	
if(numRows($sql)==0)
{
	AlertRightCornerError("No record found !");
	exit();
}	
?>

	<table id="example" class="table table-bordered table-striped" style="font-size:13px;">
        <thead>
		<tr>
			<td colspan="14" style="font-weight:bold;letter-spacing:1px;font-size:14px" id="top_row"></td>
		</tr>
		<tr>
			<th>#View#</th>
			<th>#SN#</th>
			<th>Trip_Branch
			<th>Edit_Branch</th>
			<th>Trip</th>
			<th>Fix_Lane</th>
			<th>Weight</th>
			<th>LR_Type</th>
			<th>Trip_Date</th>
			<th>End_Date</th>
			<th>Advance</th>
			<th>Diesel</th>
			<th>Expense</th>
			<th>Trip_End_User</th>
		</tr>
		</thead>
    <tbody id=""> 
	
	<?php
$sn=1;	
while($row = fetchArray($sql))
{	
	$driver_name = $row['driver_name'];
	$tno = $row['tno'];
	$trip_no = $row['trip_no'];
	
	if($row['fix_lane']>0){
		$fix_lane="<font color='green'>Yes</font>";
	}else{
		$fix_lane="No";
	}
	
	$start_date = date('d-m-y', strtotime($row['date']));
	
	if($row['end_date']=='' || $row['end_date']==0){
		$end_date = "<font color='green'>Running</font>";
	}
	else{
		$end_date = date('d-m-y', strtotime($row['end_date']));
	}
	
	if($row['diesel']>0){
		$diesel = "$row[diesel]<br>($row[diesel_qty] LTR)";
	}else{
		$diesel = "";
	}
	// $hisab_date = date('d-m-y', strtotime($row['hisab_date']));

	echo "<tr>	
			<td><button type='button' class='btn btn-xs btn-primary' onclick='ViewTrip($row[id])'>ViewTrip</button></td>
			<td>$sn</td>
			<td>$row[branch]<br>($row[trip_user])</td>
			<td>$row[edit_branch]</td>
			<td>$row[from_station] - $row[from_pincode]<br>$row[to_station] - $row[to_pincode]</td>
			<td>$fix_lane</td>
			<td>ActWt: $row[act_wt]<br>ChrgWt. $row[charge_wt]</td>
			<td>$row[lr_type]<br>($row[lrno])</td>
			<td>$start_date</td>
			<td>$end_date</td>
			<td>Cash/Atm: $row[cash]<br>RTGS: $row[rtgs]</td>
			<td>$diesel</td>
			<td>$row[expense]<br>Toll: $row[toll_tax]</td>
			<td>$row[trip_end_user]</td>
		</tr>";
	$sn++;	
}
	
	echo "</tbody>
</table>";
?>
	
<script> 
$("#loadicon").fadeOut('slow');
$(document).ready(function() {
    $('#example').DataTable( {
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ]
    } );
} );

$('#top_row').html('<?php echo "Vehicle_No: $tno, Trip_No: $trip_no, Driver: $driver_name."; ?>');
</script> 
