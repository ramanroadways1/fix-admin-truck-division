<?php
require_once("./connect.php");

$id = escapeString($conn,($_POST['id']));
$txn_type = escapeString($conn,($_POST['txn_type']));
$amount = escapeString($conn,($_POST['amount']));
$narration = trim(escapeString($conn,strtoupper($_POST['narration'])));

if($txn_type=='CASH')
{
	$trans_table="cash";
}
else if($txn_type=='CHQ')
{
	$trans_table="cheque";
}
else if($txn_type=='RTGS')
{
	$trans_table="rtgs";
}

$get_data = Qry($conn,"SELECT id,trip_id,trans_id,vou_no,tno,amount,date,branch,narration,timestamp FROM dairy.`$trans_table` WHERE id='$id'");

if(!$get_data)
{
	AlertErrorTopRight("Error while processing request !");
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	echo "<script> $('#save_edit_adv_modal_btn').attr('disabled',false); </script>";
	exit();
}

if(numRows($get_data) == 0)
{
	AlertErrorTopRight("Entry not found !");
	echo "<script> $('#save_edit_adv_modal_btn').attr('disabled',false); </script>";
	exit();
}

$row_db = fetchArray($get_data);

$table_id = $row_db['id'];
$tno = $row_db['tno'];
$trip_id = $row_db['trip_id'];
$trans_id_db = $row_db['trans_id'];
$amount_db = $row_db['amount'];
$vou_no = $row_db['vou_no'];
$narration_db = $row_db['narration'];

if($amount == $amount_db AND $narration == $narration_db)
{
	AlertErrorTopRight("Nothing to update !");
	echo "<script> $('#save_edit_adv_modal_btn').attr('disabled',false); </script>";
	exit();
}

require_once("./check_cache.php");

$check_trip = Qry($conn,"SELECT id FROM dairy.trip WHERE id='$trip_id'");

if(!$check_trip)
{
	AlertErrorTopRight("Error while processing request !");
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	echo "<script> $('#save_edit_adv_modal_btn').attr('disabled',false); </script>";
	exit();
}

if(numRows($check_trip)==0)
{
	AlertErrorTopRight("Running trip not found !");
	echo "<script> $('#save_edit_adv_modal_btn').attr('disabled',false); </script>";
	exit();
}

if($txn_type=='RTGS')
{
	$chk_rtgs_vou_approval = Qry($conn,"SELECT id FROM rtgs_fm WHERE fno='$vou_no' AND approval='1'");
	
	if(!$chk_rtgs_vou_approval)
	{
		AlertErrorTopRight("Error while processing request !");
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		echo "<script> $('#save_edit_adv_modal_btn').attr('disabled',false); </script>";
		exit();
	}
	
	if(numRows($chk_rtgs_vou_approval) > 0)
	{
		AlertErrorTopRight("RTGS Request approved ! Can\'t edit.");
		echo "<script> $('#save_edit_adv_modal_btn').attr('disabled',false); </script>";
		exit();
	}
}

$find_txn_driver = Qry($conn,"SELECT id,driver_code,tno FROM dairy.driver_book WHERE trans_id='$trans_id_db'");

if(!$find_txn_driver)
{
	AlertErrorTopRight("Error while processing request !");
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	echo "<script> $('#save_edit_adv_modal_btn').attr('disabled',false); </script>";
	exit();
}

if(numRows($find_txn_driver)==0)
{
	AlertErrorTopRight("Txn not found in driver-book !");
	echo "<script> $('#save_edit_adv_modal_btn').attr('disabled',false); </script>";
	exit();
}

$row_txn_db = fetchArray($find_txn_driver);

$driver_book_id = $row_txn_db['id'];
$driver_code = $row_txn_db['driver_code'];

$driver_book_id_prev = $driver_book_id - 1;

$get_d_bal_id = Qry($conn,"SELECT id,tno FROM dairy.driver_up WHERE down=0 AND code='$driver_code' ORDER BY id DESC LIMIT 1");

if(!$get_d_bal_id)
{
	AlertErrorTopRight("Error while processing request !");
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	echo "<script> $('#save_edit_adv_modal_btn').attr('disabled',false); </script>";
	exit();
}

if(numRows($get_d_bal_id)==0)
{
	AlertErrorTopRight("Active Driver not found !");
	echo "<script> $('#save_edit_adv_modal_btn').attr('disabled',false); </script>";
	exit();
}

$row_d_bal = fetchArray($get_d_bal_id);

$driver_up_id = $row_d_bal['id'];

if($row_d_bal['tno'] != $tno)
{
	AlertErrorTopRight("Vehicle number not verified !");
	echo "<script> $('#save_edit_adv_modal_btn').attr('disabled',false); </script>";
	exit();
}

if($amount != $amount_db)
{
	$amount_diff = $amount - $amount_db;
}
else
{
	$amount_diff = 0;
}

StartCommit($conn);
$flag = true;

// BRANCH CASHBOOK CODE STARTS

if($txn_type=='CASH')
{
	$sel_cash_id = Qry($conn,"SELECT id,comp,user FROM cashbook WHERE vou_no='$vou_no'");
	
	if(!$sel_cash_id)
	{
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
	
	if(numRows($sel_cash_id)==0)
	{
		$flag = false;
		errorLog("Transaction not found in Cashbook. vou_no: $vou_no.",$conn,$page_name,__LINE__);
	}
	
	$row_cash_id = fetchArray($sel_cash_id);
	
	$cash_id=$row_cash_id['id'];
	$company=$row_cash_id['comp'];
	$branch=$row_cash_id['user'];
	
	if($company=='RRPL')
	{
		$balance='balance';
		$debit_col='debit';
	}
	else if($company=='RAMAN_ROADWAYS')
	{
		$balance='balance2';
		$debit_col='debit2';
	}
	else
	{
		$flag = false;
		errorLog("Invalid Company Name : $company.",$conn,$page_name,__LINE__);
	}	
	
	$update_cashbook_entry = Qry($conn,"UPDATE cashbook SET `$debit_col`='$amount' WHERE id='$cash_id'");
	
	if(!$update_cashbook_entry)
	{
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
	
	$update_cashbook = Qry($conn,"UPDATE cashbook SET `$balance`=(`$balance`-($amount_diff)) WHERE id>='$cash_id' AND comp='$company' AND user='$branch'");
	
	if(!$update_cashbook)
	{
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
	
	$query_update_balance = Qry($conn,"update user set `$balance`=(`$balance`-($amount_diff)) where username='$branch'");
	
	if(!$query_update_balance)
	{
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
	
	// BRANCH CASHBOOK CODE ENDS
	
	$update_cash_table = Qry($conn,"UPDATE dairy.cash SET amount='$amount',narration='$narration' WHERE id='$table_id'");

	if(!$update_cash_table)
	{
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
	
	$update_trip = Qry($conn,"UPDATE dairy.trip SET cash=(cash+($amount_diff)) WHERE id='$trip_id'");

	if(!$update_trip)
	{
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
}
else
{
	$chk_rtgs_approval = Qry($conn,"SELECT id,com,approval FROM rtgs_fm WHERE fno='$vou_no'");
	
	if(!$chk_rtgs_approval)
	{
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
	
	if(numRows($chk_rtgs_approval)==0)
	{
		$flag = false;
		errorLog("Transaction not found in RTGS. vou_no: $vou_no.",$conn,$page_name,__LINE__);
	}
	
	$row_rtgs1 = fetchArray($chk_rtgs_approval);
	
	if($row_rtgs1['approval']=="1")
	{
		$flag = false;
		errorLog("RTGS approved. vou_no: $vou_no.",$conn,$page_name,__LINE__);
	}
	
	$rtgs_id = $row_rtgs1['id'];
	$company_rtgs = $row_rtgs1['com'];
	
	if($company_rtgs=='RRPL')
	{
		$update_passbook_entry = Qry($conn,"UPDATE passbook SET debit='$amount' WHERE vou_no='$vou_no'");
		
		if(!$update_passbook_entry)
		{
			$flag = false;
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		}
	}
	else if($company_rtgs=='RAMAN_ROADWAYS')
	{
		$update_passbook_entry = Qry($conn,"UPDATE passbook SET debit2='$amount' WHERE vou_no='$vou_no'");
		
		if(!$update_passbook_entry)
		{
			$flag = false;
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		}
	}
	else
	{
		$flag = false;
		errorLog("Invalid Company Name : $company_rtgs.",$conn,$page_name,__LINE__);
	}	
	
	$update_rtgs_entry = Qry($conn,"UPDATE rtgs_fm SET totalamt='$amount',amount='$amount' WHERE id='$rtgs_id' AND approval!='1'");
	
	if(!$update_rtgs_entry)
	{
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
	
	if(AffectedRows($conn) == 0)
	{
		$flag = false;
		errorLog("Rtgs amount not updated. Vou_no : $vou_no. id: $rtgs_id.",$conn,$page_name,__LINE__);
	}
	
	$update_rtgs_table = Qry($conn,"UPDATE dairy.rtgs SET amount='$amount',narration='$narration' WHERE id='$table_id'");

	if(!$update_rtgs_table)
	{
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
	
	$update_trip = Qry($conn,"UPDATE dairy.trip SET rtgs=(rtgs+($amount_diff)) WHERE id='$trip_id'");

	if(!$update_trip)
	{
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
	
	if(AffectedRows($conn)==0)
	{
		$flag = false;
		errorLog("Trip not found. TripId: $trip_id.",$conn,$page_name,__LINE__);
	}
}

	$update_driver_book = Qry($conn,"UPDATE dairy.driver_book SET credit='$amount' WHERE id='$driver_book_id'");

	if(!$update_driver_book)
	{
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}

	$update_driver_book_next = Qry($conn,"UPDATE dairy.driver_book SET balance=(balance+($amount_diff)) WHERE id>'$driver_book_id_prev' AND 
	tno='$tno'");

	if(!$update_driver_book_next)
	{
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}

	$update_driver_balance = Qry($conn,"UPDATE dairy.driver_up SET amount_hold=(amount_hold+($amount_diff)) WHERE id='$driver_up_id'");

	if(!$update_driver_balance)
	{
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}

$log_data = "Previous Values: VouId=> $vou_no, Amount=> $amount_db, Narration: $narration_db. New Values: Amount=> $amount, Narration: $narration.";

$insert_log = Qry($conn,"INSERT INTO dairy.ediary_admin_log(code,action,desct,timestamp) VALUES ('$table_id','EDIT_$txn_type','$log_data','$timestamp')");	
	
if(!$insert_log)
{
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

if($flag)
{
	MySQLCommit($conn);
	closeConnection($conn);
	AlertRightCornerSuccess("Updated Successfully !");
	echo "<script> $('#save_edit_adv_modal_btn').attr('disabled',false); </script>";
	exit();
}
else
{
	MySQLRollBack($conn);
	closeConnection($conn);
	
	AlertErrorTopRight("Error while processing Request !");
	echo "<script> $('#save_edit_adv_modal_btn').attr('disabled',false); </script>";
	exit();
}
?>