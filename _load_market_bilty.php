<?php
require_once 'connect.php';

$lrno = escapeString($conn,($_POST['lrno']));
$tno = escapeString($conn,($_POST['tno']));

$from_date = date('Y-m-d', strtotime(escapeString($conn,$_POST['duration']), strtotime(date("Y-m-d"))));
$to_date = date("Y-m-d");

if($lrno!='')
{
	$sql = Qry($conn,"SELECT m.id,m.date,m.lrdate,m.bilty_no,m.lr_by,m.billing_type,m.veh_placer,m.plr,m.broker,m.billing_party,m.tno,m.frmstn,
	m.tostn,m.awt,m.cwt,m.rate,m.tamt,m.branch,e.name as branch_user,m.bill_no,m.bill_amount,m.weight as bill_weight,m.timestamp 
	FROM mkt_bilty AS m
	LEFT OUTER JOIN emp_attendance  AS e ON e.code = m.branch_user 
	WHERE m.bilty_no='$lrno'");
}
else if($tno!='')
{
	$sql = Qry($conn,"SELECT m.id,m.date,m.lrdate,m.bilty_no,m.lr_by,m.billing_type,m.veh_placer,m.plr,m.broker,m.billing_party,m.tno,m.frmstn,
	m.tostn,m.awt,m.cwt,m.rate,m.tamt,m.branch,e.name as branch_user,m.bill_no,m.bill_amount,m.weight as bill_weight,m.timestamp 
	FROM mkt_bilty AS m 
	LEFT OUTER JOIN emp_attendance  AS e ON e.code = m.branch_user 
	WHERE m.lrdate BETWEEN '$from_date' AND '$to_date' AND m.tno='$tno'");
}
else
{
	AlertRightCornerError("Invalid inputs !");
	exit();
}

if(!$sql){
	echo getMySQLError($conn);
	errorLog(getMySQLError($conn),$conn,$page_url,__LINE__);
	exit();
}
	
if(numRows($sql)==0)
{
	AlertRightCornerError("No record found !");
	exit();
}
	?>
	<table id="example" class="table table-bordered table-striped" style="font-size:13px;">
        <thead>
		<tr>
			<th>#Sn</th>
			<th>Bilty_No</th>
			<th>Date</th>
			<th>LR_Date</th>
			<th>LR_By</th>
			<th>Bill_Type</th>
			<th>Vehicle_Placer</th>
			<th>LR_No.</th>
			<th>Broker</th>
			<th>Bill.Party</th>
			<th>Vehicle_No.</th>
			<th>From</th>
			<th>To</th>
			<th>ActWt</th>
			<th>ActWt</th>
			<th>Rate</th>
			<th>Freight</th>
			<th>Branch</th>
			<th>Username</th>
			<th>Bill_Details</th>
			<th>Timestamp</th>
			<th>#Edit</th>
			<th>#Delete</th>
		</tr>
		</thead>
    <tbody id=""> 
	
<?php
$sn=1;

while($row = fetchArray($sql))
{	
		$date = date('d-m-y', strtotime($row['date']));
		$lr_date = date('d-m-y', strtotime($row['lrdate']));
		$timestamp = date('d-m-y h:i A', strtotime($row['timestamp']));
		
		echo "<tr>	
			<td>$sn</td>
			<td>$row[bilty_no]</td>
			<td>$date</td>
			<td>$lr_date</td>
			<td>$row[lr_by]</td>
			<td>$row[billing_type]</td>
			<td>$row[veh_placer]</td>
			<td>$row[plr]</td>
			<td>$row[broker]</td>
			<td>$row[billing_party]</td>
			<td>$row[tno]</td>
			<td>$row[frmstn]</td>
			<td>$row[tostn]</td>
			<td>$row[awt]</td>
			<td>$row[cwt]</td>
			<td>$row[rate]</td>
			<td>$row[tamt]</td>
			<td>$row[branch]</td>
			<td>$row[branch_user]</td>
			<td>";
			if($row['bill_no']=='')
			{
				echo "<font color='red'>NA</font>";
			}
			else
			{
				echo "Bill-No: $row[bill_no]<br>
				Bill-Wt: $row[bill_weight]<br>
				Bill-Amt: $row[bill_amount]";
			}
			echo "</td>
			<td>$timestamp</td>
			<td><button class='btn edit_btn_1 btn-xs btn-primary' type='button' id='edit_btn_$row[id]' onclick=EditVoucher('$row[id]')><i class='fa fa-pencil-square-o' aria-hidden='true'></i></button></td>
			<td><button class='btn delete_btn_1 btn-xs btn-danger' type='button' id='dlt_btn_$row[id]' onclick=DeleteVoucher('$row[id]','$row[bilty_no]')><i class='fa fa-trash' aria-hidden='true'></i></button></td>
		</tr>";
		
$sn++;		
}
	echo "</tbody>
</table>";

$chk_update = Qry($conn,"SELECT id FROM _access_control WHERE username='$_SESSION[ediary_fix_admin]' AND func_id=(SELECT id FROM 
_access_control_func_list WHERE session_role='8' AND func_name='Market_Bilty') AND u_update='1'");
			  
if(numRows($chk_update)==0)
{
	echo "<script>
		$('.edit_btn_1').attr('disabled',true);
		$('.edit_btn_1').attr('onclick','');
	</script>";
}

$chk_delete = Qry($conn,"SELECT id FROM _access_control WHERE username='$_SESSION[ediary_fix_admin]' AND func_id=(SELECT id FROM 
_access_control_func_list WHERE session_role='8' AND func_name='Market_Bilty') AND u_delete='1'");
			  
if(numRows($chk_delete)==0)
{
	echo "<script>
		$('.delete_btn_1').attr('disabled',true);
		$('.delete_btn_1').attr('onclick','');
	</script>";
}
?>
	
<script> 
	$("#loadicon").fadeOut('slow');
</script> 
