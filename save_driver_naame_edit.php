<?php
require_once("./connect.php");

$id = escapeString($conn,($_POST['id']));

$get_data = Qry($conn,"SELECT id,trip_id,trans_id,tno,claim_vou_no,driver_code,naame_type,qty,rate,amount,date,branch,narration,timestamp 
FROM dairy.driver_naame WHERE id='$id'");

if(!$get_data)
{
	AlertErrorTopRight("Error while processing request !");
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	echo "<script> $('#naame_edit_button').attr('disabled',false); </script>";
	exit();
}

if(numRows($get_data) == 0)
{
	AlertErrorTopRight("Entry not found !");
	echo "<script> $('#naame_edit_button').attr('disabled',false); </script>";
	exit();
}

$row_db = fetchArray($get_data);

$table_id = $row_db['id'];
$tno = $row_db['tno'];
$trip_id = $row_db['trip_id'];
$trans_id_db = $row_db['trans_id'];
$qty_db = $row_db['qty'];
$rate_db = $row_db['rate'];
$amount_db = $row_db['amount'];
$driver_code_db = $row_db['driver_code'];
$claim_vou_no = $row_db['claim_vou_no'];
$naame_type_db = $row_db['naame_type'];
$narration_db = $row_db['narration'];

require_once("./check_cache.php");

$amount = escapeString($conn,($_POST['naame_amount']));

$naame_option = escapeString($conn,($_POST['naame_option'])); 
$narration = escapeString($conn,strtoupper($_POST['narration']));

if($amount == $amount_db AND $narration == $narration_db AND $naame_type_db == $naame_option)
{
	AlertErrorTopRight("Nothing to update !");
	echo "<script> $('#naame_edit_button').attr('disabled',false); </script>";
	exit();
}

if($naame_option=='OTHER')
{
	$qty = 0;
	$rate = 0;
}
else
{
	$qty = escapeString($conn,($_POST['naame_qty']));
	$rate = escapeString($conn,($_POST['naame_rate']));
}

if($naame_option=="CLAIM" || $naame_option=="FREIGHT")
{
	AlertErrorTopRight("Claim and Freight Naame is not editable !");
	echo "<script> $('#naame_edit_button').attr('disabled',false); </script>";
	exit();
}

$check_trip = Qry($conn,"SELECT id FROM dairy.trip WHERE id='$trip_id'");

if(!$check_trip)
{
	AlertErrorTopRight("Error while processing request !");
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	echo "<script> $('#naame_edit_button').attr('disabled',false); </script>";
	exit();
}

if(numRows($check_trip)==0)
{
	AlertErrorTopRight("Running trip not found !");
	echo "<script> $('#naame_edit_button').attr('disabled',false); </script>";
	exit();
}

$fetch_txn_db = Qry($conn,"SELECT id,driver_code,tno FROM dairy.driver_book WHERE trans_id='$trans_id_db'");

if(!$fetch_txn_db)
{
	AlertErrorTopRight("Error while processing request !");
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	echo "<script> $('#naame_edit_button').attr('disabled',false); </script>";
	exit();
}

if(numRows($fetch_txn_db)==0)
{
	AlertErrorTopRight("Txn not found in driver-book !");
	echo "<script> $('#naame_edit_button').attr('disabled',false); </script>";
	exit();
}

$row_db_txn = fetchArray($fetch_txn_db);

$driver_book_id = $row_db_txn['id'];
$driver_code = $row_db_txn['driver_code'];

if($driver_code!=$driver_code_db)
{
	AlertErrorTopRight("Driver not verified !");
	echo "<script> $('#naame_edit_button').attr('disabled',false); </script>";
	exit();
}

$driver_book_id_prev = $driver_book_id - 1;

$fetch_d_bal = Qry($conn,"SELECT id,tno FROM dairy.driver_up WHERE down=0 AND code='$driver_code' ORDER BY id DESC LIMIT 1");

if(!$fetch_d_bal)
{
	AlertErrorTopRight("Error while processing request !");
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	echo "<script> $('#naame_edit_button').attr('disabled',false); </script>";
	exit();
}

if(numRows($fetch_d_bal)==0)
{
	AlertErrorTopRight("Driver balance not found !");
	echo "<script> $('#naame_edit_button').attr('disabled',false); </script>";
	exit();
}

$row_d_bal = fetchArray($fetch_d_bal);

$driver_up_id = $row_d_bal['id'];

if($amount != $amount_db)
{
	$amount_diff = $amount - $amount_db;
}
else
{
	$amount_diff = 0;
}
	
StartCommit($conn);
$flag = true;

$update_driver_book = Qry($conn,"UPDATE dairy.driver_book SET credit='$amount' WHERE id='$driver_book_id'");

if(!$update_driver_book)
{
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

$update_driver_book_next = Qry($conn,"UPDATE dairy.driver_book SET balance=(balance+($amount_diff)) WHERE id>'$driver_book_id_prev' AND tno='$tno'");

if(!$update_driver_book_next)
{
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

$update_naame = Qry($conn,"UPDATE dairy.driver_naame SET naame_type='$naame_option',qty='$qty',rate='$rate',amount='$amount',narration='$narration' 
WHERE id='$table_id'");

if(!$update_naame)
{
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

$update_trip = Qry($conn,"UPDATE dairy.trip SET driver_naame=(driver_naame+($amount_diff)) WHERE id='$trip_id'");

if(!$update_trip)
{
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}	

$update_driver_balance = Qry($conn,"UPDATE dairy.driver_up SET amount_hold=(amount_hold+($amount_diff)) WHERE id='$driver_up_id'");

if(!$update_driver_balance)
{
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

$log_data = "Previous Values: NaameType=> $naame_type_db, Qty=> $qty_db, Rate=> $rate_db, Amount=> $amount_db, Narration: $narration_db. 
New Values: NaameType=> $naame_option, Qty=> $qty, Rate=> $rate, Amount=> $amount, Narration: $narration.";

$insert_log = Qry($conn,"INSERT INTO dairy.ediary_admin_log(code,action,desct,timestamp) VALUES ('$table_id','EDIT_NAAME','$log_data','$timestamp')");	
	
if(!$insert_log)
{
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

if($flag)
{
	MySQLCommit($conn);
	closeConnection($conn);
	
	AlertRightCornerSuccess("Updated Successfully !");
	echo "<script> $('#naame_edit_button').attr('disabled',false); </script>";	
	exit();
}
else
{
	MySQLRollBack($conn);
	closeConnection($conn);
	
	AlertErrorTopRight("Error while processing Request !");
	echo "<script> $('#naame_edit_button').attr('disabled',false); </script>";	
	exit();
}
?>