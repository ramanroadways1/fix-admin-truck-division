<?php
require_once("./connect.php");

$CancelBy = escapeString($conn,($_POST['cancel_by']));
$date = date("Y-m-d");
$timestamp = date("Y-m-d H:i:s");

if($CancelBy=='LR')
{
	$lrno = escapeString($conn,strtoupper($_POST['lr_no']));
}
else if($CancelBy=='RANGE')
{
	$from = escapeString($conn,strtoupper($_POST['lr_from']));
	$to = escapeString($conn,strtoupper($_POST['lr_to']));
	$stock = escapeString($conn,strtoupper($_POST['bilty_stock']));
}
else
{
	AlertErrorTopRight("Error : Invalid option selected !");
	echo "<script> $('#add_btn').attr('disabled',false); </script>";
	exit();
}

if($CancelBy=='LR')
{
	$chk_lr = Qry($conn,"SELECT id FROM lr_check WHERE lrno='$lrno'");
	
	if(!$chk_lr){
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		AlertErrorTopRight("Error while Processing Request !");
		echo "<script> $('#add_btn').attr('disabled',false); </script>";
		exit();
	}

	if(numRows($chk_lr)>0)
	{
		AlertErrorTopRight("Warning : Duplicate LR found !");
		echo "<script> $('#add_btn').attr('disabled',false); </script>";
		exit();
	}
	
	$get_bilty_id = Qry($conn,"SELECT id,branch,company FROM lr_bank WHERE $lrno>=from_range AND $lrno<=to_range");
	
	if(!$get_bilty_id){
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		AlertErrorTopRight("Error while Processing Request !");
		echo "<script> $('#add_btn').attr('disabled',false); </script>";
		exit();
	}

	if(numRows($get_bilty_id)==0)
	{
		AlertErrorTopRight("Warning : LR stock not found !");
		echo "<script> $('#add_btn').attr('disabled',false); </script>";
		exit();
	}
	else if(numRows($get_bilty_id)>1)
	{
		AlertErrorTopRight("Multiple Stock found !");
		echo "<script> $('#add_btn').attr('disabled',false); </script>";
		exit();
	}
	else
	{
		$row_bilty_id = fetchArray($get_bilty_id);
	}
	
$bilty_branch = $row_bilty_id['branch'];	
$bilty_id = $row_bilty_id['id'];	
$bilty_company = $row_bilty_id['company'];	
	
	$chk_coal_branch = Qry($conn,"SELECT z FROM user WHERE username='$lr_branch'");
	
	if(!$chk_coal_branch){
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		Redirect("Error while Processing Request","./bilty_book.php");
		exit();
	}

	if(numRows($chk_coal_branch)==0)
	{
		AlertErrorTopRight("Branch not found !");
		echo "<script> $('#add_btn').attr('disabled',false); </script>";
		exit();
	}
	
	$row_chk_branch = fetchArray($chk_coal_branch);
	
	StartCommit($conn);
	$flag = true;
	
	$insert_lr = Qry($conn,"INSERT INTO lr_check(branch,lrno,bilty_id,timestamp) VALUES ('$bilty_branch','$lrno','$bilty_id','$timestamp')");
		
	if(!$insert_lr){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
		
	$insert_lr2 = Qry($conn,"INSERT INTO lr_sample(company,branch,lrno,crossing,cancel,timestamp) VALUES ('$bilty_company','$bilty_branch','$lrno',
	'NO','1','$timestamp')");
		
	if(!$insert_lr2){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
		
	if($row_chk_branch['z']=="1")
	{
		$insert_lr_coal = Qry($conn,"INSERT INTO ship.lr_entry(lrno,branch,company,done,cancel,timestamp) VALUES ('$lrno','$bilty_branch',
		'$bilty_company','1','1','$timestamp')");
			
		if(!$insert_lr_coal){
			$flag = false;
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		}
	}
		
	$update_stock = Qry($conn,"UPDATE lr_bank SET stock_left=stock_left-1 WHERE id='$bilty_id'");
		
	if(!$update_stock){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
		
	if($flag)
	{
		MySQLCommit($conn);
		closeConnection($conn);
		
		AlertRightCornerSuccess("LR Cancelled !");
		echo "<script>$('#Form1')[0].reset(); $('#add_btn').attr('disabled',false); </script>";
		exit();
	}
	else
	{
		MySQLRollBack($conn);
		closeConnection($conn);
	
		AlertErrorTopRight("Error while Processing Request !");
		echo "<script> $('#add_btn').attr('disabled',false); </script>";
		exit();
	}
}
else
{
	$qry_chk = Qry($conn,"SELECT id FROM lr_check WHERE lrno>=$from AND lrno<=$to");

	if(!$qry_chk)
	{
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		AlertErrorTopRight("Error while Processing Request !");
		echo "<script> $('#add_btn').attr('disabled',false); </script>";
		exit();
	}

	$created_lrs = numRows($qry_chk);

	if($created_lrs >0 )
	{
		AlertErrorTopRight("$created_lrs LRs created in Series : $from to $to !");
		echo "<script> $('#add_btn').attr('disabled',false); </script>";
		exit();
	}
	
	$get_bilty_id = Qry($conn,"SELECT id,branch,company FROM lr_bank WHERE $from>=from_range AND $to<=to_range");
	
	if(!$get_bilty_id){
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		AlertErrorTopRight("Error while Processing Request !");
		echo "<script> $('#add_btn').attr('disabled',false); </script>";
		exit();
	}

	if(numRows($get_bilty_id)==0)
	{
		AlertErrorTopRight("LR stock not found !");
		echo "<script> $('#add_btn').attr('disabled',false); </script>";
		exit();
	}
	else if(numRows($get_bilty_id)>1)
	{
		AlertErrorTopRight("Multiple stock found !");
		echo "<script> $('#add_btn').attr('disabled',false); </script>";
		exit();
	}
	else
	{
		$row_bilty_id = fetchArray($get_bilty_id);
	}

$bilty_branch = $row_bilty_id['branch'];	
$bilty_id = $row_bilty_id['id'];	
$bilty_company = $row_bilty_id['company'];

	$chk_coal_branch = Qry($conn,"SELECT z FROM user WHERE username='$bilty_branch'");
	
	if(!$chk_coal_branch){
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		AlertErrorTopRight("Error while Processing Request !");
		echo "<script> $('#add_btn').attr('disabled',false); </script>";
		exit();
	}

	if(numRows($chk_coal_branch)==0)
	{
		AlertErrorTopRight("Branch not found !");
		echo "<script> $('#add_btn').attr('disabled',false); </script>";
		exit();
	}
	
	$row_chk_branch = fetchArray($chk_coal_branch);
	
	StartCommit($conn);
	$flag = true;
	
for($lrno_count=$from; $lrno_count<=$to; $lrno_count++)
{
	$insert_lr = Qry($conn,"INSERT INTO lr_check(branch,lrno,bilty_id,timestamp) VALUES ('$bilty_branch','$lrno_count','$bilty_id','$timestamp')");
	
	if(!$insert_lr){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
	
	$insert_lr2 = Qry($conn,"INSERT INTO lr_sample(company,branch,lrno,crossing,cancel,timestamp) VALUES 
	('$bilty_company','$bilty_branch','$lrno_count','NO','1','$timestamp')");
	
	if(!$insert_lr2){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
	
	if($row_chk_branch['z']=="1")
	{
		$insert_lr_coal = Qry($conn,"INSERT INTO ship.lr_entry(lrno,branch,company,done,cancel,timestamp) VALUES 
		('$lrno_count','$bilty_branch','$bilty_company','1','1','$timestamp')");
		
		if(!$insert_lr_coal){
			$flag = false;
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		}
	}
	
	$update_stock = Qry($conn,"UPDATE lr_bank SET stock_left=stock_left-1 WHERE id='$bilty_id'");
	
	if(!$update_stock){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
}
	
	if($flag)
	{
		MySQLCommit($conn);
		closeConnection($conn);
		
		AlertRightCornerSuccess("LR Series Cancelled !");
		echo "<script>$('#Form1')[0].reset(); $('#add_btn').attr('disabled',false); </script>";
		exit();
	}
	else
	{
		MySQLRollBack($conn);
		closeConnection($conn);
	
		AlertErrorTopRight("Error while Processing Request !");
		echo "<script> $('#add_btn').attr('disabled',false); </script>";
		exit();
	}
}
?>