<?php
require_once("./connect.php");

$veh_no = escapeString($conn,strtoupper($_POST['tno']));

AlertError("Function in-active !");
exit();
	
$_SESSION['tno_tank_capacity'] = $veh_no;

$get_tank_capacity = Qry($conn,"SELECT diesel_tank_cap FROM dairy.own_truck WHERE tno='$veh_no'");

if(!$get_tank_capacity){
	AlertError("Error !");
	errorLog(getMySQLError($conn),$conn,$page_url,__LINE__);
	exit();
}

if(numRows($get_tank_capacity)==0)
{
	AlertError("Vehicle not found !");
	exit();
}

$row_tank_cap = fetchArray($get_tank_capacity);

$tank_capacity = $row_tank_cap['diesel_tank_cap'];

if($tank_capacity<=0)
{
	AlertError("Tank capacity not updated !");
	exit();
}
?>
<button id="dsl_update_modal_btn" style="display:none" data-toggle="modal" data-target="#DslUpdateModal"><span class="glyphicon glyphicon-edit"></span> Add</button>

<div class="modal fade" id="DslUpdateModal" role="dialog" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-md">
      <div class="modal-content">
	  <div class="modal-header bg-primary">
			<span style="font-size:13px">Update Diesel Qty: <?php echo $veh_no; ?></span>
		</div>
	<div class="modal-body">
		<div class="row">
			
			<div class="form-group col-md-6">
				<label>Latest Trip <sup><font color="red">*</font></sup></label>
				<select style="font-size:12px" name="trip_id" class="form-control" required>
					<option style="font-size:12px" value="">--select trip--</option>
					<?php
					$get_running_trips = Qry($conn,"SELECT id,from_station,to_station,date(date) as trip_date FROM dairy.trip WHERE tno='$veh_no' ORDER BY id DESC LIMIT 1");
					
					if(numRows($get_running_trips)>0)
					{
						while($row_trips = fetchArray($get_running_trips))
						{
							echo "<option style='font-size:12px' value='$row_trips[id]'>$row_trips[from_station] to $row_trips[to_station] : ".convertDate("d-m-Y",$row_trips['trip_date'])."</option>";
						}
					}
					?>
				</select>
			</div>
			
			<div class="form-group col-md-6">
				<label>Diesel Qty <sup><font color="red">*</font></sup></label>
				<input type="number" step="any" min="10" max="<?php echo $tank_capacity; ?>" name="qty" class="form-control" required />
			</div>
		
			<input type="hidden" name="tno" value="<?php echo $veh_no; ?>">
		</div>
        </div>
        <div class="modal-footer">
          <button type="submit" id="btn_save_qty" class="btn btn-sm btn-danger">Confirm Update</button>
          <button type="button" class="btn btn-sm btn-primary" id="hide_dsl_update_modal" data-dismiss="modal">Close</button>
		</div>
      </div>
    </div>
  </div>
    
<script>
	$('#dsl_update_modal_btn')[0].click();
	$('#loadicon').fadeOut('slow');
</script>  