<?php
require_once 'connect.php';

// $date = date("Y-m-d");
$timestamp = date("Y-m-d H:i:s");

$vou_no = escapeString($conn,(trim($_POST['vou_no'])));
$type = escapeString($conn,(trim($_POST['type'])));
$date = escapeString($conn,(trim($_POST['date'])));

if(empty($vou_no)){
	AlertRightCornerError("Voucher not found !");
	exit();
}

if(empty($type)){
	AlertRightCornerError("Voucher type not found !");
	exit();
}

if(empty($date)){
	AlertRightCornerError("Validity date not found !");
	exit();
}

if($type=='OWN')
{
	$chk_trip = Qry($conn,"SELECT done,create_date FROM freight_form_lr WHERE frno='$vou_no'");
}
else
{
	$chk_trip = Qry($conn,"SELECT done,date as create_date FROM mkt_bilty WHERE bilty_no='$vou_no'");
}

if(!$chk_trip){
	AlertErrorTopRight("Error while processing request !");
	errorLog(getMySQLError($conn),$conn,$page_url,__LINE__);
	exit();
}

if(numRows($chk_trip) == 0)
{
	AlertErrorTopRight("Voucher not found !");
	exit();
}

$row_trip = fetchArray($chk_trip);

if($row_trip['done']=="1")
{
	AlertErrorTopRight("Trip already created !");
	exit();
}

if(strtotime($date) <= strtotime($row_trip['create_date']))
{
	AlertErrorTopRight("Check validity date !");
	exit();
}

StartCommit($conn);
$flag = true;

if($type=='OWN')
{
	$update = Qry($conn,"UPDATE freight_form_lr SET create_date='$date' WHERE frno='$vou_no'");
	
	if(!$update){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
}
else
{
	$update = Qry($conn,"UPDATE mkt_bilty SET date='$date' WHERE bilty_no='$vou_no'");
	
	if(!$update){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
}

if($flag)
{
	MySQLCommit($conn);
	closeConnection($conn);
	AlertRightCornerSuccess("Updated successfully !");
	echo "<script>$('#add_btn')[0].click();</script>";
	exit();
}
else
{
	MySQLRollBack($conn);
	closeConnection($conn);
	AlertErrorTopRight("Error while processing request !");
	exit();
}	
?>