<?php
require_once("./connect.php");

$id = escapeString($conn,strtoupper($_POST['id']));
$type = escapeString($conn,($_POST['type']));
$timestamp = date("Y-m-d H:i:s");

if(empty($id))
{
	AlertErrorTopRight("ID not found !");
	exit();
}

if($type !='approve' AND $type != 'reject')
{
	AlertErrorTopRight("Invalid type !");
	exit();
}

if($type == 'approve')
{
	$fetch_last = Qry($conn,"SELECT code FROM dairy.driver ORDER BY id DESC LIMIT 1");

	if(!$fetch_last){
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		AlertErrorTopRight("Error while Processing Request !");
		exit();
	}
		
	$row_last = fetchArray($fetch_last);
	$code = sprintf("%'.03d",++$row_last['code']);
		
	if($code=='')
	{
		AlertErrorTopRight("Error while Processing Request !");
		exit();
	}
					
	StartCommit($conn);
	$flag = true;
		
	$insert_driver = Qry($conn,"INSERT INTO dairy.driver(name,father_husband_name,mobile,lic,vehicle_class,transport_doi,transport_doe,date_of_issue,
	val_date,dob,blood_group,gender,issued_by,state,lic_front,lic_rear,driver_photo,aadhar_no,aadhar_front,aadhar_rear,date,branch,branch_user,addr,
	temp_addr,req_timestamp,supervisor,supervisor_timestamp,code,timestamp) SELECT name,father_husband_name,mobile,lic,vehicle_class,transport_doi,
	transport_doe,date_of_issue,val_date,dob,blood_group,gender,issued_by,state,lic_front,lic_rear,driver_photo,aadhar_no,aadhar_front,
	aadhar_rear,date,branch,branch_user,addr,temp_addr,timestamp,supervisor,supervisor_timestamp,'$code','$timestamp' FROM dairy.driver_temp WHERE id='$id'");

	if(!$insert_driver){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
		
	$insert_ac = Qry($conn,"INSERT INTO dairy.driver_ac (code,acname,acno,bank,ifsc) SELECT '$code',ac_holder,ac_no,bank,ifsc FROM dairy.driver_temp 
	WHERE id='$id'");
			
	if(!$insert_ac){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}

	$dlt_temp = Qry($conn,"DELETE FROM dairy.driver_temp WHERE id='$id'");
			
	if(!$dlt_temp){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
	
	if($flag)
	{
		MySQLCommit($conn);
		closeConnection($conn);
		
		echo "<script>
			$('#approve_btn_$id').attr('disabled',true);
			$('#reject_btn_$id').attr('disabled',true);
			$('#approve_btn_$id').html('Approved');
			$('#reject_btn_$id').html('Approved');
			
			$('#approve_btn_$id').attr('onclick','');
			$('#reject_btn_$id').attr('onclick','');
			$('#loadicon').fadeOut('slow');
		</script>";
		exit();
	}
	else
	{
		MySQLRollBack($conn);
		closeConnection($conn);
		AlertErrorTopRight("Error while Processing Request !");
		exit();
	}
}
else
{
	$qry = Qry($conn,"SELECT driver_photo,lic_front,lic_rear,aadhar_front,aadhar_rear FROM dairy.driver_temp WHERE id='$id'");
	
	if(!$qry){
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		AlertErrorTopRight("Error while Processing Request !");
		exit();
	}
	 
	if(numRows($qry)==0)
	{
		AlertErrorTopRight("Driver not found !");
		exit();
	}

	$row = fetchArray($qry);

	unlink("../diary_admin/".$row['driver_photo']);
	unlink("../diary_admin/".$row['lic_front']);
	unlink("../diary_admin/".$row['lic_rear']);
	unlink("../diary_admin/".$row['aadhar_front']);
	unlink("../diary_admin/".$row['aadhar_rear']);

	$dlt = Qry($conn,"DELETE FROM dairy.driver_temp WHERE id='$id'");
		
	if(!$dlt){
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		AlertErrorTopRight("Error while Processing Request !");
		exit();
	}

	echo "<script>
		$('#approve_btn_$id').attr('disabled',true);
		$('#reject_btn_$id').attr('disabled',true);
		
		$('#approve_btn_$id').html('Rejected');
		$('#reject_btn_$id').html('Rejected');
		
		$('#approve_btn_$id').attr('onclick','');
		$('#reject_btn_$id').attr('onclick','');
		$('#loadicon').fadeOut('slow');
	</script>";
	exit();
}
?>