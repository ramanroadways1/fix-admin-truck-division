<?php
require_once("connect.php");

$date=date("Y-m-d");
$timestamp=date("Y-m-d H:i:s");

$id = escapeString($conn,($_POST['id']));
$empty_loaded = escapeString($conn,($_POST['empty_loaded']));

$select_consignee = Qry($conn,"SELECT DISTINCT e.consignee,c.name FROM dairy.fix_lane_exp AS e 
LEFT OUTER JOIN consignee AS c ON c.id = e.consignee 
WHERE e.rule_id='$id'");

if(!$select_consignee){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	AlertError("Error while processing request !");
	exit();
}
	
if(numRows($select_consignee)>0)
{
	while($row1 = fetchArray($select_consignee))
	{
	
	if($row1['name']=='')
	{
		echo "<span style='font-size:12px !important'>Consignee : <font color='blue'>NA</font></span>";
	}
	else
	{
		echo "<span style='font-size:12px !important'>Consignee : $row1[name]</span>";
	}
	
	$load_exps = Qry($conn,"SELECT id,consignee,exp_code,exp_name,exp_type,exp_amount,timestamp FROM dairy.fix_lane_exp WHERE rule_id='$id' 
	AND consignee='$row1[consignee]'");

	if(!$load_exps){
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		AlertError("Error while processing request !");
		exit();
	}

	echo '<table class="table table-bordered table-striped" style="font-size:12px;margin-top:5px !important">
		<tr>
			<th>#</th>
			<th>Expense code</th>
			<th>Expense name</th>
			<th>Expense type</th>
			<th>Amount</th>
			<th>Timestamp</th>
			<th>#Update</th>
			<th>#Delete</th>
		</tr>';
	
	if(numRows($load_exps)==0)
	{
		echo "<tr><td colspan='7'>No record found..</td></tr>";
	}
	else
	{
		$i_sn=1;

		while($row = fetchArray($load_exps))
		{
			echo "<tr>
				<td>$i_sn</td>
				<td>$row[exp_code]</td>
				<td>";
			
			echo "<select name='exp_edit_name' id='exp_name_edit_new_$row[id]'>";
			$get_exps = Qry($conn,"SELECT exp_code,name FROM dairy.exp_head WHERE name NOT IN(SELECT exp_name FROM dairy.fix_lane_exp WHERE rule_id='$id') ORDER BY name ASC");

			echo "<option value='$row[exp_code]_$row[exp_name]' selected='selected'>$row[exp_name]</option>";
			while($row_exp1 = fetchArray($get_exps))
			{
			?>
				<option value="<?php echo $row_exp1['exp_code']."_".$row_exp1['name']; ?>"><?php echo $row_exp1['name']; ?></option>
			<?php
			}
				echo "</select></td>
				
			<td>";
			echo "<select name='exp_type_edit' id='exp_type_edit_new_$row[id]'>";
			?>
			<option value="Fix_Amount" <?php if($row['exp_type']=='Fix_Amount') { echo "selected='selected'"; } ?>>Fix_Amount</option>
			<option value="On_Weight" <?php if($row['exp_type']=='On_Weight') { echo "selected='selected'"; } ?>>On_Weight</option>
			<?php
			echo "</select></td>
				<td><input type='number' value='$row[exp_amount]' min='1' id='new_expense_value_$row[id]' /></td>
				<td>".date("d-m-y h:i A",strtotime($row["timestamp"]))."</td>
				<td>
					<button id='update_exp_fix_btn_$row[id]' onclick=UpdateExpSave('$row[id]','$id','$empty_loaded') type='button' class='btn btn_exp_update_fix btn-xs btn-default'>
					<span class='fa fa-pencil-square-o'></span></button>
				</td>
				<td>
					<button onclick=DeleteExp('$row[id]','$id','$empty_loaded') type='button' class='btn btn_exp_dlt_fix btn-xs btn-danger'>
					<span class='fa fa-trash'></span></button>
				</td>
			</tr>";
			$i_sn++;	
		}
	}

	echo "</table>";
	
	}
}

echo "<script>
		$('#exp_modal_rule_id').val('$id');
		$('#exp_modal_empty_loaded').val('$empty_loaded');
		$('#loadicon').fadeOut('slow');
	</script>";
	
$qry_rights_dlt_exp = Qry($conn,"SELECT id FROM _access_control WHERE username='$_SESSION[ediary_fix_admin]' AND func_id=(SELECT id FROM 
_access_control_func_list WHERE session_role='8' AND func_name='Fix_Lane') AND u_delete='1'");
							  
if(numRows($qry_rights_dlt_exp)==0)
{
	echo "<script>
		$('.btn_exp_dlt_fix').hide();
		$('.btn_exp_dlt_fix').attr('onclick','');
	</script>";
}

$qry_rights_update_exp = Qry($conn,"SELECT id FROM _access_control WHERE username='$_SESSION[ediary_fix_admin]' AND func_id=(SELECT id FROM 
_access_control_func_list WHERE session_role='8' AND func_name='Fix_Lane') AND u_update='1'");
							  
if(numRows($qry_rights_update_exp)==0)
{
	echo "<script>
		$('.btn_exp_update_fix').hide();
		$('.btn_exp_update_fix').attr('onclick','');
	</script>";
}		
?>