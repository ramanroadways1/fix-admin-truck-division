<?php
require_once("./connect.php");

$timestamp = date("Y-m-d H:i:s"); 

$code = escapeString($conn,strtoupper($_POST['code']));

$getConsumerPump = Qry($conn,"SELECT consumer_pump FROM dairy.diesel_pump_own WHERE code='$code'");

if(!$getConsumerPump)
{
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	echo "<script>
		window.location.href='./';
	</script>";
	exit();
}

$rowComp = fetchArray($getConsumerPump);

if($rowComp['consumer_pump']=="1")
{
	echo "<script>
		$('#rate').attr('oninput','');
		$('#dsl_amt').attr('oninput','');
		$('#rate').attr('readonly',true);
		$('#dsl_amt').attr('readonly',true);
		$('#is_consumer_pump_entry').val('1');
		$('#loadicon').fadeOut('slow');
	</script>";
}
else{
	echo "<script>
		$('#rate').attr('oninput','sum1()');
		$('#dsl_amt').attr('oninput','sum3()');
		$('#rate').attr('readonly',false);
		$('#dsl_amt').attr('readonly',false);
		$('#is_consumer_pump_entry').val('0');
		$('#loadicon').fadeOut('slow');
	</script>";
}

?>