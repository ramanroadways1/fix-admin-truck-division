<?php
require_once 'connect.php';

$from_id = escapeString($conn,$_POST['from_id']);
$to_id = escapeString($conn,$_POST['to_id']);
$from_lat_long = escapeString($conn,$_POST['from_lat_long']);
$to_lat_long = escapeString($conn,$_POST['to_lat_long']);

$timestamp = date("Y-m-d H:i:s");

if(empty($from_id))
{
	AlertError("From ID not found !");
	echo "<script>
		$('#trip_update_button').attr('disabled',true);
	</script>";
	exit();
}

if(empty($to_id))
{
	AlertError("To ID not found !");
	echo "<script>
		$('#trip_update_button').attr('disabled',true);
	</script>";
	exit();
}

$get_distance = Qry($conn,"SELECT distance FROM master_addr_book WHERE from_id = '$from_id' AND to_id = '$to_id'");

if(!$get_distance){
	AlertError("Error !");
	errorLog(getMySQLError($conn),$conn,$page_url,__LINE__);
	exit();
}

if(numRows($get_distance) > 0)
{
	$row = fetchArray($get_distance);
	
	echo "<script>
		$('#trip_km').val('$row[distance]');
		$('#trip_update_button').attr('disabled',false);
	</script>";
	exit();
}
else
{
	$origin = trim($from_lat_long);
	$destination = trim($to_lat_long);

	$url="https://maps.googleapis.com/maps/api/distancematrix/json?units=imperial&origins=".urlencode($origin)."&destinations=".urlencode($destination)."&key=AIzaSyCZ6JUFGB8r5TpRYGnJjNxUH1NZdxaPoGw";

	$api = file_get_contents($url);
	$data = json_decode($api);
				
	$api_status = $data->rows[0]->elements[0]->status;
		
	if($api_status=='NOT_FOUND')
	{
		echo "<script>
			alert('distance not found !');
			$('#trip_update_button').attr('disabled',true);
		</script>";
		exit();
	}

	if($api_status!='OK')
	{
		echo "<span style='color:red;font-size:13px'>API Error: $api_status !</span>";
		echo "<script>
			alert('API Error !');
			$('#trip_update_button').attr('disabled',true);
		</script>";
		exit();
	}
				
	$dest_addr = $data->destination_addresses[0];
	$origin_addr = $data->origin_addresses[0];
	$distance = round((int)$data->rows[0]->elements[0]->distance->value / 1000);
	$travel_time = $data->rows[0]->elements[0]->duration->text;
	$travel_time_value = $data->rows[0]->elements[0]->duration->value;
	$travel_hrs = gmdate("H", $travel_time_value);
	$travel_minutes = gmdate("i", $travel_time_value);
	$travel_seconds = gmdate("s", $travel_time_value);
	
	$insert_distance = Qry($conn,"INSERT INTO master_addr_book(from_id,to_id,distance,branch,branch_user,timestamp) VALUES ('$from_id','$to_id',
	'$distance','EDIARY_ADMIN','$_SESSION[ediary_fix_admin]','$timestamp')");
	
	if(!$insert_distance){
		AlertError("Error !");
		errorLog(getMySQLError($conn),$conn,$page_url,__LINE__);
		exit();
	}

	echo "<script>
		$('#trip_km').val('$distance');
		$('#trip_update_button').attr('disabled',false);
	</script>";
	exit();
}

?>