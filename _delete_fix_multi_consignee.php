<?php
require_once './connect.php';

$rule_id = escapeString($conn,strtoupper($_POST['rule_id']));
$lane_id = escapeString($conn,strtoupper($_POST['lane_id']));
$con2_id = escapeString($conn,strtoupper($_POST['con2_id']));
$timestamp = date("Y-m-d H:i:s");

// $get_new_record = Qry($conn,"SELECT multi_del_consignee FROM dairy.fix_lane_rules WHERE id='$rule_id'");

// if(!$get_new_record){
	// $flag = false;
	// echo getMySQLError($conn);
	// errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
// }

// $row2 = fetchArray($get_new_record);

// $curr_string = trim(str_replace($con2_id,"",$row2['multi_del_consignee']));
// $parts = explode(",",$curr_string);
// asort($parts);
// $parts = array_filter($parts);
// $new_string = (implode(",",$parts));

// echo "<script>
		// alert('$new_string');
		// $('#loadicon').fadeOut('slow');
	// </script>";
	
// exit();

StartCommit($conn);
$flag = true;

$get_new_record = Qry($conn,"SELECT multi_del_consignee FROM dairy.fix_lane_rules WHERE id='$rule_id'");

if(!$get_new_record){
	$flag = false;
	echo getMySQLError($conn);
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

$row2 = fetchArray($get_new_record);

$curr_string = trim(str_replace($con2_id,"",$row2['multi_del_consignee']));
$parts = explode(",",$curr_string);
asort($parts);
$parts = array_filter($parts);
$new_string = (implode(",",$parts));
			
$update_new = Qry($conn,"UPDATE dairy.fix_lane_rules SET multi_del_consignee = '$new_string' WHERE id='$rule_id'");

if(!$update_new){
	$flag = false;
	echo getMySQLError($conn);
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

$get_updated_record = Qry($conn,"SELECT multi_del_consignee FROM dairy.fix_lane_rules WHERE id='$rule_id'");

if(!$get_updated_record){
	$flag = false;
	echo getMySQLError($conn);
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

$row_updated = fetchArray($get_updated_record);

if( strpos($row_updated['multi_del_consignee'],",") !== false )
{
	
}
else
{
	$update_consignee = Qry($conn,"UPDATE dairy.fix_lane_rules SET consignee = '$new_string',multi_del_consignee='' WHERE id='$rule_id'");

	if(!$update_consignee){
		$flag = false;
		echo getMySQLError($conn);
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
}

$insert_log = Qry($conn,"INSERT INTO dairy.ediary_admin_log(code,action,desct,branch,username,timestamp) VALUES ('$rule_id','Multi_Consignee_Remove',
'Id: $con2_id removed.','Fix_Admin','$_SESSION[ediary_fix_admin]','$timestamp')");

if(!$insert_log){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}	

if($flag)
{
	MySQLCommit($conn);
	closeConnection($conn);
	echo "<script>
		LoadAllRule('$lane_id'); 
		// $('#loadicon').fadeOut('slow');
	</script>";
	exit();
}
else
{
	MySQLRollBack($conn);
	closeConnection($conn);
	echo "<script>alert('Error !');$('#loadicon').fadeOut('slow');</script>";
	exit();
}
?>