<?php
require_once("connect.php");

$date=date("Y-m-d");
$timestamp=date("Y-m-d H:i:s");

$id = escapeString($conn,($_POST['id']));

$chk_lane = Qry($conn,"SELECT fix_diesel_type,diesel_value,adv_amount FROM dairy.fix_lane_rules WHERE id='$id'");

if(!$chk_lane){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	AlertError("Error while processing request !");
	exit();
}

if(numRows($chk_lane)==0)
{
	AlertError("Oops.. Lane not found !");
	
	echo "<script>
		$('#adv_diesel_modal_id').val('');
		$('.img_right').hide();
		$('#new_rule_div').show();
		$('#adv_diesel_div').hide();
		$('#img_right_$id').hide();
	</script>"; 
	exit();
}

$row = fetchArray($chk_lane);	

if($row['adv_amount']!=0)
{
	echo "<script>
		$('#adv_amount').val('$row[adv_amount]');
	</script>"; 
}
else
{
	echo "<script>
		$('#adv_amount').val('');
	</script>"; 
}

if($row['diesel_value']!=0)
{
	echo "<script>
		$('#diesel_value').val('$row[diesel_value]');
	</script>"; 
}
else
{
	echo "<script>
		$('#diesel_value').val('');
	</script>"; 
}

echo "<script>
		$('#diesel_type').val('$row[fix_diesel_type]');
		$('#adv_diesel_modal_id').val('$id');
		$('.img_right').hide();
		$('#new_rule_div').hide();
		$('#adv_diesel_div').show();
		$('#img_right_$id').show();
		$('#loadicon').fadeOut('slow');
	</script>"; 
	exit();
?>