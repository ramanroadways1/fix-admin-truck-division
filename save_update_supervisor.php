<?php
require_once("connect.php");

$timestamp = date("Y-m-d H:i:s");

$truck_no = escapeString($conn,strtoupper($_POST['truck_no']));
$id = escapeString($conn,$_POST['vehicle_id']);
$supervisor = escapeString($conn,$_POST['supervisor']);

echo "<script>$('#submit_btn_supervisor').attr('disabled',true);</script>";

if($truck_no=='')
{
	AlertError("Vehicle number not found !");
	echo "<script>$('#submit_btn_supervisor').attr('disabled',false);$('#loadicon').fadeOut('slow');</script>";
	exit();
}

if($id=='')
{
	AlertError("Data not found !");
	echo "<script>$('#submit_btn_supervisor').attr('disabled',false);$('#loadicon').fadeOut('slow');</script>";
	exit();
}

if($supervisor=='')
{
	AlertError("Supervisor not found !");
	echo "<script>$('#submit_btn_supervisor').attr('disabled',false);$('#loadicon').fadeOut('slow');</script>";
	exit();
}

$chk_supervisor = Qry($conn,"SELECT title FROM dairy.user WHERE id='$supervisor' AND role='1' AND type='1'");
	
if(!$chk_supervisor){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	AlertError("Erorr !");
	echo "<script>$('#submit_btn_supervisor').attr('disabled',false);$('#loadicon').fadeOut('slow');</script>";
	exit();
}

if(numRows($chk_supervisor)==0)
{
	AlertError("Supervisor not found !");
	echo "<script>$('#submit_btn_supervisor').attr('disabled',false);$('#loadicon').fadeOut('slow');</script>";
	exit();
}

$row_supervisor = fetchArray($chk_supervisor);

$supervisor_name = $row_supervisor['title'];

$get_vehicle = Qry($conn,"SELECT t.tno,t.superv_id,u.title as ext_supervisor_name
FROM dairy.own_truck AS t 
LEFT OUTER JOIN dairy.user AS u ON u.id = t.superv_id
WHERE t.id='$id'");
	
if(!$get_vehicle){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	AlertError("Erorr !");
	echo "<script>$('#submit_btn_supervisor').attr('disabled',false);$('#loadicon').fadeOut('slow');</script>";
	exit();
}

if(numRows($get_vehicle)==0)
{
	AlertError("Vehicle not found !");
	echo "<script>$('#submit_btn_supervisor').attr('disabled',false);$('#loadicon').fadeOut('slow');</script>";
	exit();
}

$row_vehicle = fetchArray($get_vehicle);

if($row_vehicle['tno'] != $truck_no)
{
	AlertError("Vehicle not verified !");
	echo "<script>$('#submit_btn_supervisor').attr('disabled',false);$('#loadicon').fadeOut('slow');</script>";
	exit();
}

$ext_supervisor = $row_vehicle['superv_id'];
$ext_supervisor_name = $row_vehicle['ext_supervisor_name'];

if($ext_supervisor==$supervisor)
{
	AlertError("Nothing to update !");
	echo "<script>$('#submit_btn_supervisor').attr('disabled',false);$('#loadicon').fadeOut('slow');</script>";
	exit();
}

StartCommit($conn);
$flag = true;
	
$mark_sold = Qry($conn,"UPDATE dairy.own_truck SET superv_id='$supervisor' WHERE id='$id'");
	
if(!$mark_sold){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

$insert_log = Qry($conn,"INSERT INTO dairy.ediary_admin_log(code,tno,action,desct,branch,username,timestamp) VALUES ('$id','$truck_no','Supervisor_Update',
'$ext_supervisor_name ($ext_supervisor) to $supervisor_name ($supervisor)','Fix_Admin','$_SESSION[ediary_fix_admin]','$timestamp')");
	
if(!$insert_log){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

if($flag)
{
	MySQLCommit($conn);
	closeConnection($conn);
	echo "<script>
		$('#supervisor_row_$id').html('$supervisor_name');
		$('#supervisor_$id').val('$supervisor');
		$('#loadicon').fadeOut('slow');
	</script>";
	AlertRightCornerSuccessFadeFast("Update Success !");
}
else
{
	MySQLRollBack($conn);
	closeConnection($conn);
	AlertError("Error While Processing Request !");
	echo "<script>$('#submit_btn_supervisor').attr('disabled',false);$('#loadicon').fadeOut('slow');</script>";
	exit();
}
?>