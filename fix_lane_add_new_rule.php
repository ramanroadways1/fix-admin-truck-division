<?php
require_once("connect.php");

$date=date("Y-m-d");
$timestamp=date("Y-m-d H:i:s");

$id = escapeString($conn,($_POST['lane_id_modal']));

$col_array = array();
$value_array = array();

$qry_chk_duplicate = "SELECT id FROM dairy.fix_lane_rules WHERE lane_id='$id'";

if(isset($_POST['rule_vehicle_check']) AND isset($_POST['rule_model_check']))
{
	AlertError("Either select vehicle number or model !");
	echo "<script>
		$('#button_new_rule').attr('disabled',false);
	</script>";
	exit();
}

if(isset($_POST['rule_vehicle_check']))
{
	$tno = escapeString($conn,strtoupper($_POST['tno']));
	
	if(empty($tno)){
		AlertError("Error : Vehicle number not found !");
		echo "<script>
			$('#button_new_rule').attr('disabled',false);
		</script>";
		exit();
	}
	
	$col_array[]="tno";
	$value_array[]="'$tno'";
	$qry_chk_duplicate.=" AND tno='$tno' AND tno!=''";
}

if(isset($_POST['rule_con1_check']))
{
	$con1 = escapeString($conn,strtoupper($_POST['con1_id']));

	if(empty($con1)){
		
		AlertError("Error : Consignor not found !");
		echo "<script>
			$('#button_new_rule').attr('disabled',false);
		</script>";
		exit();
	}
	
	$verify_con1 = Qry($conn,"SELECT name FROM consignor WHERE id='$con1'");
	
	if(!$verify_con1){
		AlertError("Error while processing request !");
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		exit();
	}
	
	if(numRows($verify_con1)==0){
		AlertError("Consignor not found !");
		errorLog(getMySQLError("Consignor not found. Id: $con1."),$conn,$page_name,__LINE__);
		exit();
	}
	
	$row_con1 = fetchArray($verify_con1);
	
	if($row_con1['name'] != $_POST['con1'])
	{
		AlertError("Error: Please check consignor !");
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		exit();
	}

	$col_array[]="consignor";
	$value_array[]="'$con1'";
	$qry_chk_duplicate.=" AND consignor='$con1' AND consignor!=''";
}

if(isset($_POST['rule_con2_check']))
{
	$con2 = escapeString($conn,strtoupper($_POST['con2_id']));

	if(empty($con2)){
		AlertError("Error : Consignee not found !");
		echo "<script>
			$('#button_new_rule').attr('disabled',false);
		</script>";
		exit();
	}
	
	$verify_con2 = Qry($conn,"SELECT name FROM consignee WHERE id='$con2'");
	
	if(!$verify_con2){
		AlertError("Error while processing request !");
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		exit();
	}
	
	if(numRows($verify_con2)==0){
		AlertError("Consignee not found !");
		errorLog(getMySQLError("Consignee not found. Id: $con2."),$conn,$page_name,__LINE__);
		exit();
	}
	
	$row_con2 = fetchArray($verify_con2);
	
	if($row_con2['name'] != $_POST['con2'])
	{
		AlertError("Error: Please check consignee !");
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		exit();
	}
	
	$col_array[]="consignee";
	$value_array[]="'$con2'";
	$qry_chk_duplicate.=" AND consignee='$con2' AND consignee!=''";
}

if(isset($_POST['rule_empty_loaded_check']))
{
	$empty_loaded = escapeString($conn,strtoupper($_POST['empty_loaded']));

	if(empty($empty_loaded) AND $empty_loaded!="0"){
		
		AlertError("Error : Field empty/loaded not found !");
		echo "<script>
			$('#button_new_rule').attr('disabled',false);
		</script>";
		exit();
	}
	
	$col_array[]="empty_loaded";
	$value_array[]="'$empty_loaded'";
	$qry_chk_duplicate.=" AND empty_loaded='$empty_loaded' AND empty_loaded!=''";
}
else
{
	$col_array[]="empty_loaded";
	$value_array[]="'2'";
	$qry_chk_duplicate.=" AND empty_loaded='2' AND empty_loaded!=''";
}

if(isset($_POST['rule_model_check']))
{
	$model = escapeString($conn,strtoupper($_POST['veh_model']));

	if(empty($model)){
		
		AlertError("Error : Model not found !");
		echo "<script>
			$('#button_new_rule').attr('disabled',false);
		</script>";
		exit();
	}
	
	$col_array[]="model";
	$value_array[]="'$model'";
	$qry_chk_duplicate.=" AND model='$model' AND model!=''";
}

if(empty($col_array) || empty($value_array))
{
	AlertError("Oops : Nothing to save !");
	echo "<script>
		$('#button_new_rule').attr('disabled',false);
	</script>";
	exit();
}

$col_array = implode(',',$col_array);
$value_array = implode(',',$value_array);

$chk_duplicate = Qry($conn,$qry_chk_duplicate);

if(numRows($chk_duplicate)>0)
{
	AlertError("Duplicate rule found !");
	echo "<script>
		$('#button_new_rule').attr('disabled',false);
	</script>";
	exit();
}

StartCommit($conn);
$flag = true;

$insert_new_rule = Qry($conn,"INSERT INTO dairy.fix_lane_rules (lane_id,$col_array,timestamp) VALUES ('$id',$value_array,'$timestamp')");

if(!$insert_new_rule){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

$update_rule_count = Qry($conn,"UPDATE dairy.fix_lane SET total_rules=total_rules+1 WHERE id='$id'");

if(!$update_rule_count){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

if($flag)
{
	MySQLCommit($conn);
	closeConnection($conn);
	
	AlertRightCornerSuccess("Rule added successfully !");
	echo "<script>
		$('#button_new_rule').attr('disabled',false);
		Number($('#total_rule_count_$id').html());
		var new_exp_count = Number($('#total_rule_count_$id').html())+1;
		$('#total_rule_count_$id').html(new_exp_count);
		LoadAllRule('$id');
	</script>";
}
else
{
	MySQLRollBack($conn);
	closeConnection($conn);
	
	AlertError("Error while processing request !");
	echo "<script>
		$('#button_new_rule').attr('disabled',false);
	</script>";
	exit();
}	
?>