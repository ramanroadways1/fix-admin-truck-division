<?php
$check_cache = Qry($conn,"SELECT id FROM dairy.hisab_cache WHERE tno='$tno'");

if(!$check_cache){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error While Processing Request.","./");
	exit();
}

if(numRows($check_cache)>0)
{
	Redirect("Vehicle\'s Hisab in process.","./");
	exit();
}

$check_cache_trip = Qry($conn,"SELECT id FROM dairy.trip_cache WHERE tno='$tno'");

if(!$check_cache_trip){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error While Processing Request.","./");
	exit(); 
}

if(numRows($check_cache_trip)>0)
{
	Redirect("Please wait ! Try again after some time..","./");
	exit();
}
?>