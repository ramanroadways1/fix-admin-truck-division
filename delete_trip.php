<?php
require_once("./connect.php");

$date = date("Y-m-d");
$timestamp = date("Y-m-d H:i:s");

$trip_id = escapeString($conn,($_POST['trip_id']));

$qry = Qry($conn,"SELECT tno,trip_no,from_station,to_station,lr_type,lr_type2,freight,cash,cheque,rtgs,diesel,freight_collected,driver_naame,
driver_gps_naame,expense,loaded_hisab FROM dairy.trip WHERE id='$trip_id'");

if(!$qry){
	AlertErrorTopRight("Error while processing request !");
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	echo "<script>$('#del_button_$trip_id').attr('disabled',false);</script>";
	exit();
}

if(numRows($qry) == 0)
{
	AlertErrorTopRight("Trip not found !");
		echo "<script>$('#del_button_$trip_id').attr('disabled',false);</script>";
	exit();
}

$row = fetchArray($qry);

$qry_trip = Qry($conn,"SELECT id FROM dairy.trip WHERE trip_no='$row[trip_no]' AND id>'$trip_id'");

if(!$qry_trip){
	AlertErrorTopRight("Error while processing request !");
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	echo "<script>$('#del_button_$trip_id').attr('disabled',false);</script>";
	exit();
}

if(numRows($qry_trip)>0)
{
	AlertErrorTopRight("Next trip created ! Delete it first.");
	echo "<script>$('#del_button_$trip_id').attr('disabled',false);</script>";
	exit();
}

if($row['lr_type2']>1)
{
	AlertErrorTopRight("Multiple LRs attached in trip !");
	echo "<script>$('#del_button_$trip_id').attr('disabled',false);</script>";
    exit();
}

if($row['loaded_hisab']=="1")
{
	AlertErrorTopRight("Loaded hisab Trip ! You Can not delete.");
	echo "<script>$('#del_button_$trip_id').attr('disabled',false);</script>";
    exit();
}

if($row['freight']>0 || $row['cash']>0 || $row['cheque']>0 || $row['rtgs']>0 || $row['diesel']>0 || $row['freight_collected']>0 || $row['driver_naame']>0 || $row['expense']>0 || $row['driver_gps_naame']>0)
{
	AlertErrorTopRight("Txns found in trip ! Delete txns first.");
	echo "<script>$('#del_button_$trip_id').attr('disabled',false);</script>";
	exit();
}

StartCommit($conn);
$flag = true;

$log_data = "From : $row[from_station], To : $row[from_station], trip_no : $row[trip_no], TruckNo : $row[tno], Trip_id : $trip_id.";

$insert_log = Qry($conn,"INSERT INTO dairy.ediary_admin_log(code,action,desct,branch,username,timestamp) VALUES ('$row[tno]','TRIP_DELETE',
'$log_data','Fix_Admin','$_SESSION[ediary_fix_admin]','$timestamp')");

if(!$insert_log){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}
	
if(substr($row['lr_type'],3,1)=='M')
{
	$update=Qry($conn,"UPDATE mkt_bilty SET done=0 WHERE bilty_no='$row[lr_type]'");
	
	if(!$update){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
}
else if(substr($row['lr_type'],3,3)=='OLR')
{
	$update=Qry($conn,"UPDATE freight_form_lr SET done=0 WHERE frno='$row[lr_type]'");
	
	if(!$update){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
}

$delete_trip = Qry($conn,"DELETE FROM dairy.trip WHERE id='$trip_id'");

if(!$delete_trip){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

$delete_trip_fix_txn_pending = Qry($conn,"DELETE FROM dairy.fix_txn_pending WHERE trip_id='$trip_id'");

if(!$delete_trip_fix_txn_pending){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

if($flag)
{
	MySQLCommit($conn);
	closeConnection($conn);
	AlertRightCornerSuccess("Deleted successfully !");
	echo "<script>$('#search_btn')[0].click();</script>";
	exit();
}
else
{
	MySQLRollBack($conn);
	closeConnection($conn);
	AlertErrorTopRight("Error while processing request !");
	echo "<script>$('#del_button_$trip_id').attr('disabled',false);</script>";
	exit();
}
?>