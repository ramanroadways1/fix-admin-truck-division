<?php
require_once("./connect.php");

$id = escapeString($conn,($_POST['id']));
// $lrno = escapeString($conn,($_POST['lrno']));
// $lr_date = escapeString($conn,($_POST['lr_date']));
// $from_id = escapeString($conn,($_POST['from_id']));
// $to_id = escapeString($conn,($_POST['to_id']));
// $con1_id = escapeString($conn,($_POST['con1_id']));
// $con2_id = escapeString($conn,($_POST['con2_id']));
$actual_wt = escapeString($conn,($_POST['actual_wt']));
// $charge_wt = escapeString($conn,($_POST['charge_wt']));
// $consignor = escapeString($conn,($_POST['consignor']));
// $consignee = escapeString($conn,($_POST['consignee']));
// $from_loc = escapeString($conn,($_POST['from_loc']));
// $to_loc = escapeString($conn,($_POST['to_loc']));

$date = date("Y-m-d");
$timestamp = date("Y-m-d H:i:s");

if(empty($id))
{
	AlertErrorTopRight("Voucher ID not found !");
	echo "<script> $('#update_btn_$id').attr('disabled',false); </script>";
	exit();
}

if(empty($actual_wt))
{
	AlertErrorTopRight("Voucher ID not found !");
	echo "<script> $('#update_btn_$id').attr('disabled',false); </script>";
	exit();
}

// if(empty($from_id) || empty($lr_date) || empty($lrno) || empty($to_id) || empty($con1_id) || empty($con2_id) || empty($actual_wt) || empty($charge_wt))
// {
	// AlertErrorTopRight("Fill out all fields first !");
	// echo "<script> $('#update_btn_$id').attr('disabled',false); </script>";
	// exit();
// }
	
$get_current_data = Qry($conn,"SELECT f.wt12,l.wt12,f.mother_lr_id 
FROM freight_form_lr AS f 
LEFT OUTER JOIN lr_sample AS l ON l.id = f.mother_lr_id 
WHERE f.id='$id'");
	
if(!$get_current_data){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	AlertErrorTopRight("Error while Processing Request !");
	echo "<script> $('#update_btn_$id').attr('disabled',false); </script>";
	exit();
}

if(numRows($get_current_data) == 0)
{
	AlertErrorTopRight("Voucher not found !");
	echo "<script> $('#update_btn_$id').attr('disabled',false); </script>";
	exit();
}

$row = fetchArray($get_current_data);

$mother_lr_id = $row['mother_lr_id'];

$update_log = array();
$update_Qry = array();

if($actual_wt != $row['wt12'])
{
	$update_log[]="Act_WT : $row[wt12] to $actual_wt";
	$update_Qry[]="wt12='$actual_wt'";
}

$update_log = implode(', ',$update_log); 
$update_Qry = implode(', ',$update_Qry); 

if($update_log=="")
{
	AlertErrorTopRight("Nothing to update !");
	echo "<script> $('#update_btn_$id').attr('disabled',false); </script>";
	exit();
}

StartCommit($conn);
$flag = true;
	
$update_freight_form_lr = Qry($conn,"UPDATE freight_form_lr SET $update_Qry WHERE id='$id'");
		
if(!$update_freight_form_lr){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

$update_lr_sample = Qry($conn,"UPDATE lr_sample SET wt12='$actual_wt' WHERE id='$mother_lr_id'");
		
if(!$update_lr_sample){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}
		
if($flag)
{
	MySQLCommit($conn);
	closeConnection($conn);
	AlertRightCornerSuccess("Updated !");
	echo "<script>$('#update_btn_$id').attr('disabled',false); </script>";
	exit();
}
else
{
	MySQLRollBack($conn);
	closeConnection($conn);
	
	AlertErrorTopRight("Error while Processing Request !");
	echo "<script> $('#update_btn_$id').attr('disabled',false); </script>";
	exit();
}
?>