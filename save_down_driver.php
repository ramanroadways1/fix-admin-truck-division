<?php
require_once("./connect.php");

$tno = escapeString($conn,($_POST['truck_no']));
$timestamp = date("Y-m-d H:i:s");

echo "<script>$('#down_driver_btn_$tno').attr('disabled',true);</script>";
	
$get_d_code = Qry($conn,"SELECT driver_code FROM dairy.own_truck WHERE tno='$tno'");

if(!$get_d_code){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	AlertErrorTopRight("Error while Processing Request !");
	echo "<script>$('#down_driver_btn_$tno').attr('disabled',false);</script>";
	exit();
}

if(numRows($get_d_code)==0)
{
	AlertErrorTopRight("Vehicle not found !");
	echo "<script>$('#down_driver_btn_$tno').attr('disabled',false);</script>";
	exit();
}

$row_d_code = fetchArray($get_d_code);

if($row_d_code['driver_code']==0 AND $row_d_code['driver_code']=='')
{
	AlertErrorTopRight("Active driver not found !");
	echo "<script>$('#down_driver_btn_$tno').attr('disabled',false);</script>";
	exit();
}

$chk_running_trip = Qry($conn,"SELECT id FROM dairy.trip WHERE tno='$tno'");

if(!$chk_running_trip){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	AlertErrorTopRight("Error while Processing Request !");
	echo "<script>$('#down_driver_btn_$tno').attr('disabled',false);</script>";
	exit();
}

if(numRows($chk_running_trip)>0)
{
	AlertErrorTopRight("Trip Running !");
	echo "<script>$('#down_driver_btn_$tno').attr('disabled',false);</script>";
	exit();
}
	
$chk_driver_up = Qry($conn,"SELECT id,code,asset_form_up,asset_form_down FROM dairy.driver_up WHERE tno='$tno' AND down=0 ORDER BY id DESC LIMIT 1");

if(!$chk_driver_up){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	AlertErrorTopRight("Error while Processing Request !");
	echo "<script>$('#down_driver_btn_$tno').attr('disabled',false);</script>";
	exit();
}

if(numRows($chk_driver_up)==0)
{
	AlertErrorTopRight("Active driver not found. Code:2 !");
	echo "<script>$('#down_driver_btn_$tno').attr('disabled',false);</script>";
	exit();
}
	
$row_active = fetchArray($chk_driver_up);

$only_driver_up = "NO";

$asset_form_down = $row_active['asset_form_down'];

if($row_active['asset_form_up']!='1')
{
	$check_driver_up = Qry($conn,"SELECT id,desct FROM dairy.driver_book WHERE tno='$tno' ORDER BY id DESC LIMIT 1");

	if(!$check_driver_up){
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		AlertErrorTopRight("Error while Processing Request !");
		echo "<script>$('#down_driver_btn_$tno').attr('disabled',false);</script>";
		exit();
	}

	if(numRows($check_driver_up)==0)
	{
		AlertErrorTopRight("Record not found in driver-book !");
		echo "<script>$('#down_driver_btn_$tno').attr('disabled',false);</script>";
		exit();
	}

	$row_chk_up = fetchArray($check_driver_up);

	if($row_chk_up['desct']=='DRIVER_UP')
	{
		$only_driver_up = "YES";
		$driver_up_id = $row_chk_up['id'];
		
		$chk_driver_up1 = Qry($conn,"SELECT id,code FROM dairy.driver_up WHERE tno='$tno' AND down=0 ORDER BY id DESC LIMIT 1");

		if(!$chk_driver_up1){
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			AlertErrorTopRight("Error while Processing Request !");
			echo "<script>$('#down_driver_btn_$tno').attr('disabled',false);</script>";
			exit();
		}
		
		if(numRows($chk_driver_up1)==0)
		{
			AlertErrorTopRight("Driver up record not found !");
			echo "<script>$('#down_driver_btn_$tno').attr('disabled',false);</script>";
			exit();
		}
		
		$row_d_up = fetchArray($chk_driver_up1);
		
		if($row_d_up['code']!=$row_active['code'])
		{
			AlertErrorTopRight("Driver code not verified !");
			echo "<script>$('#down_driver_btn_$tno').attr('disabled',false);</script>";
			exit();
		}
		
		$driver_up_id2 = $row_d_up['id'];
	}
	else
	{
		AlertErrorTopRight("Asset form not found !");
		echo "<script>$('#down_driver_btn_$tno').attr('disabled',false);</script>";
		exit();
	}
}

if($row_active['code']!=$row_d_code['driver_code'])
{
	AlertErrorTopRight("Driver not verified !");
	echo "<script>$('#down_driver_btn_$tno').attr('disabled',false);</script>";
	exit();
}

$chk_last_record_driver_book = Qry($conn,"SELECT driver_code,trip_no,balance,date,branch,narration,timestamp 
FROM dairy.driver_book WHERE tno='$tno' ORDER BY id DESC LIMIT 1");

if(!$chk_last_record_driver_book){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	AlertErrorTopRight("Error while Processing Request !");
	echo "<script>$('#down_driver_btn_$tno').attr('disabled',false);</script>";
	exit();
}

if(numRows($chk_last_record_driver_book)==0)
{
	AlertErrorTopRight("Record not found in driver-book !");
	echo "<script>$('#down_driver_btn_$tno').attr('disabled',false);</script>";
	exit();
}

$row_db = fetchArray($chk_last_record_driver_book);

if($row_db['driver_code']!=$row_d_code['driver_code'])
{
	AlertErrorTopRight("Driver not verified. Code: 3 !");
	echo "<script>$('#down_driver_btn_$tno').attr('disabled',false);</script>";
	exit();
}

$log_data = "DriverDown: Vehicle_No: $tno. DriverCode: $row_db[driver_code]";

StartCommit($conn);
$flag = true;

if($only_driver_up=='YES')
{
	$dlt_driver_up1 = Qry($conn,"DELETE FROM dairy.driver_up WHERE id='$driver_up_id2'");

	if(!$dlt_driver_up1){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}

	$dlt_driver_up2 = Qry($conn,"DELETE FROM dairy.driver_book WHERE id='$driver_up_id'");
		
	if(!$dlt_driver_up2){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
}
else
{
	$insert_down_db = Qry($conn,"INSERT INTO dairy.driver_book(driver_code,tno,trip_no,desct,balance,date,branch,narration,timestamp) 
	VALUES ('$row_db[driver_code]','$tno','$row_db[trip_no]','DRIVER_DOWN','$row_db[balance]','$row_db[date]','$row_db[branch]',
	'$row_db[narration]','$row_db[timestamp]')");

	if(!$insert_down_db){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}

	$update_hisab_log = Qry($conn,"UPDATE dairy.log_hisab SET driver_down='1',down_type='ON_LEAVE' WHERE trip_no='$row_db[trip_no]'");

	if(!$update_hisab_log){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}

	$update_driver_up = Qry($conn,"UPDATE dairy.driver_up SET down='$row_db[date]',asset_form_down='1',status='1' 
	WHERE id='$row_active[id]'");

	if(!$update_driver_up){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}


	$update_asset_form_trip_no = Qry($conn,"UPDATE dairy.asset_form SET item_value_down=item_value_up,quantity=item_qty,
	amount=item_rate,trip_no='$row_db[trip_no]' WHERE tno='$tno'");
		
	if(!$update_asset_form_trip_no){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
		
	$copy_asset_form = Qry($conn,"INSERT INTO dairy.asset_form_done(driver_code,item_code,item_qty,item_rate,item_value_up,
	item_value_down,quantity,amount,amount_naame,branch,trip_no,tno,timestamp) SELECT driver_code,item_code,item_qty,
	item_rate,item_value_up,item_value_down,quantity,amount,amount_naame,branch,trip_no,tno,timestamp FROM dairy.asset_form 
	WHERE tno='$tno'");
		
	if(!$copy_asset_form){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	} 
} 

$dlt_asset_form = Qry($conn,"DELETE FROM dairy.asset_form WHERE tno='$tno'");
	
if(!$dlt_asset_form){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

$update_own_truck = Qry($conn,"UPDATE dairy.own_truck SET driver_code='0' WHERE tno='$tno'");
	
if(!$update_own_truck){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

$update_driver_table = Qry($conn,"UPDATE dairy.driver SET active='0' WHERE code='$row_db[driver_code]'");
	
if(!$update_driver_table){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

if($flag)
{
	MySQLCommit($conn);
	closeConnection($conn);
	AlertRightCornerSuccess("OK : Done !");
	exit();
}
else
{
	MySQLRollBack($conn);
	closeConnection($conn);
	AlertErrorTopRight("Error while Processing Request !");
	echo "<script>$('#down_driver_btn_$tno').attr('disabled',true);</script>";
	echo "<script>$('#down_driver_btn_$tno').attr('onclick','');</script>";
	exit();
}	
?>