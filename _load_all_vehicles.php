<?php
require_once 'connect.php';

$sql = Qry($conn,"SELECT t.id,t.tno,t.wheeler,t.diesel_tank_cap,t.superv_id,t.trishul_card,t.happay_active,t.lock_ag,t.lock_tel,t.comp,t.model,t.cng,s.title,d.name,
t.driver_code,(SELECT salary_amount FROM dairy.driver_up WHERE code=t.driver_code AND down=0 ORDER BY id DESC LIMIT 1) as driver_salary, 
(SELECT salary_type FROM dairy.driver_up WHERE code=t.driver_code AND down=0 ORDER BY id DESC LIMIT 1) as salary_type
FROM dairy.own_truck AS t 
LEFT OUTER JOIN dairy.user AS s ON s.id = t.superv_id
LEFT OUTER JOIN dairy.driver AS d ON d.code = t.driver_code
WHERE t.is_sold!='1'");

if(!$sql){
	echo getMySQLError($conn);
	errorLog(getMySQLError($conn),$conn,$page_url,__LINE__);
	exit();
}

?>
	<table id="example" class="table table-bordered table-striped" style="font-size:13.5px;">
        <thead>
		<tr>
			<th>#</th>
			<th>Vehicle_no</th>
			<th>Model/Wheeler</th>
			<th>Company</th>
			<th>SALARY</th>
			<th>Supervisor</th>
			<th>Trishul_Card</th>
			<th>Happay_Card</th>
			<th>AG_Tel_Lock</th>
			<th>CNG</th>
			<th>Driver</th>
			<th>#</th>
			<th>#</th>
			<th>#</th>
			<th>#</th>
			<th>#</th>
		</tr>
		</thead>
       <tbody id=""> 
<?php
if(numRows($sql)==0)
{
	echo "<tr>
		<td colspan='14'>No record found !</td>
	</tr>";  
}
else
{
	$sn=1;
	while($row = fetchArray($sql))
	{
		if($row['driver_code']!=0){
			$driver_name = $row['name'];
			$down_driver_btn = "";
			
			if($row['salary_type']=="1"){
			$salary = "$row[driver_salary]/trip";
			}
			else{
				$salary = "$row[driver_salary]/month";
			}
		
		} else {
			$driver_name = "<font color='red'>W/D</font>";
			$down_driver_btn = "disabled";
			$salary="";
		}
		
		if($row['trishul_card']!=0){
			$trishul_card = "<font color='green'>Yes</font>";
		} else {
			$trishul_card = "No";
		}
		
		if($row['happay_active']!=0){
			$happay_active = "<font color='green'>Yes</font>";
		} else {
			$happay_active = "<font color='red'>No</font>";
		}
		
		if($row['comp']=='RRPL'){
			$company = "RRPL";
		} else {
			$company = "RR";
		}
		
		if($row['cng']!=0){
			$cng = "<font color='green'>Yes</font>";
		} else {
			$cng = "No";
		}
		
		if($row['lock_ag']=="1" AND $row['lock_tel']=="1")
		{
			$lock_tel = "AG - Yes<br>Tel - Yes";
		}
		else if($row['lock_ag']=="0" AND $row['lock_tel']=="1")
		{
			$lock_tel = "AG - No<br>Tel - Yes";
		}
		else if($row['lock_ag']=="1" AND $row['lock_tel']=="0")
		{
			$lock_tel = "AG - Yes<br>Tel - No";
		}
		else
		{
			$lock_tel = "AG - No<br>Tel - No";
		}
		
		echo "<tr>
			<td>$sn</td>
			<td>$row[tno]</td>
			<td><span id='model_name_row_$row[id]'>$row[model]</span><br>$row[wheeler] w</td>
			<td>$company</td>
			<td id='salary_row_$row[driver_code]'>$salary</td>
			<td id='supervisor_row_$row[id]'>$row[title]</td>
			<td>$trishul_card</td>
			<td>$happay_active</td>
			<td>$lock_tel</td>
			<td>$cng</td>
			<td>$driver_name</td>
			<input type='hidden' value='$row[model]' id='model_$row[id]'>
			<input type='hidden' value='$row[superv_id]' id='supervisor_$row[id]'>
			<input type='hidden' value='$row[tno]' id='tno_$row[id]'>
			<input type='hidden' value='$row[driver_code]' id='driver_code_$row[id]'>
			<td><button type='button' $down_driver_btn onclick='UpdateSALARY($row[id])' class='btn btn_update1 btn-xs btn-warning'>Update<br>SALARY</button></td>
			<td><button type='button' onclick='UpdateSupervisor($row[id])' class='btn btn_update1 btn-xs btn-primary'>Update<br>Supervisor</button></td>
			<td><button type='button' onclick='UpdateModel($row[id])' class='btn btn_update1 btn-xs btn-success'>Update<br>Model</button></td>
			<td><button type='button' id='btn_mark_sold_$row[id]' onclick='MarkAsSold($row[id])' class='btn btn_update1 btn-xs btn-danger'>Mark<br>Sold</button></td>
			<td><button type='button' $down_driver_btn id='down_driver_btn_$row[id]' onclick='DownDriver($row[id])' class='btn btn_update1 btn-xs btn-danger'>Down<br>Driver</button></td>
		</tr>";
		$sn++;		
     }
}
?>
	</tbody>
		</table>
<script> 
$("#loadicon").fadeOut('slow');
$(document).ready(function() {
    $('#example').DataTable( {
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ]
    } );
} );
</script> 

<?php
$qry_rights_update = Qry($conn,"SELECT id FROM _access_control WHERE username='$_SESSION[ediary_fix_admin]' AND func_id=(SELECT id FROM 
_access_control_func_list WHERE session_role='8' AND func_name='Manage_Own_Truck') AND u_update='1'");
							  
if(numRows($qry_rights_update)==0)
{
	echo "<script>
		$('.btn_update1').attr('disabled',true);
		$('.btn_save_route1').attr('onclick','');
	</script>";
}
?>