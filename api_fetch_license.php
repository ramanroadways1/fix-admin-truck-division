<?php
require_once 'connect.php';

$date = date("Y-m-d");
$timestamp = date("Y-m-d H:i:s");

$lic_no = escapeString($conn,strtoupper(trim($_POST['lic_no'])));

if(empty($lic_no))
{
	AlertErrorTopRight("Enter License number first !");
	echo "<script>$('#lic_fetch_btn').attr('disabled', false);</script>";
	exit();
}

$data = array(
	"id_number"=>"$lic_no",
);

$payload = json_encode($data);

$curl = curl_init();

curl_setopt_array($curl, array(
  CURLOPT_URL => "$aadhar_api_url/api/v1/driving-license/driving-license",
  CURLOPT_RETURNTRANSFER => true,
  CURLOPT_ENCODING => '',
  CURLOPT_MAXREDIRS => 10,
  CURLOPT_TIMEOUT => 0,
  CURLOPT_FOLLOWLOCATION => true,
  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
  CURLOPT_CUSTOMREQUEST => 'POST',
  CURLOPT_POSTFIELDS => $payload,
  CURLOPT_HTTPHEADER => array(
    'Content-Type: application/json',
	'Content-Length: ' . strlen($payload),
	'Authorization: Bearer '.$aadhar_api_token.''
  ),
));

$response = curl_exec($curl);

curl_close($curl);

$result = json_decode($response, true);

if($result['status_code']!='200')
{
	$error_msg = clean_input($result['message']);
	$error_code = $result['status_code'];
	
	AlertErrorTopRight("$error_code: $error_msg !");
	echo "<script>
		$('#lic_fetch_btn').attr('disabled', false);
		
		$('#driver_name').attr('readonly', false);
		$('#driver_addr').attr('readonly', false);
	</script>";
	exit();
}

$client_id = $result['data']['client_id'];	
$state = $result['data']['state'];	
$name = trim(str_replace("  "," ",$result['data']['name']));
$permanent_address = trim(str_replace("  "," ",$result['data']['permanent_address']));
$permanent_zip = $result['data']['permanent_zip'];
$temporary_address = trim(str_replace("  "," ",$result['data']['temporary_address']));
$temporary_zip = $result['data']['temporary_zip'];
$citizenship = $result['data']['citizenship'];
$ola_name = $result['data']['ola_name'];
$ola_code = $result['data']['ola_code'];
$gender = $result['data']['gender'];
$father_or_husband_name = $result['data']['father_or_husband_name'];
$dob = $result['data']['dob'];
$doe = $result['data']['doe'];
$doi = $result['data']['doi'];
$blood_group = $result['data']['blood_group'];
$vehicle_classes = $result['data']['vehicle_classes'];
$image = $result['data']['profile_image']; 

// $_SESSION['admin_market_driver_']

echo "<script>
	$('#driver_name').val('$name');
	$('#driver_addr').val('$permanent_address $permanent_zip');
	
	$('#lic_fetch_btn').attr('disabled', true);
	
	$('#driver_name').attr('readonly', true);
	$('#driver_addr').attr('readonly', true);
	
	$('#loadicon').fadeOut('slow');
	$('#driver_lic_result_main_div').show();
</script>";
?>
<div class="form-group col-md-3">
	<img src="data:image/png;base64,<?php echo $image; ?>" />
</div>

<div class="form-group col-md-9">
	<table class="table table-bordered table-striped" style="font-size:11px">
		<tr>
			<th>Full Name</th>
			<td><?php echo $name; ?></td>
			
			<th>Father/Husband</th>
			<td><?php echo $father_or_husband_name; ?></td>
		</tr>	
	
		<tr>
			<th>DOB</th>
			<td><?php echo $dob; ?></td>
			<th>Citizenship</th>
			<td><?php echo $citizenship; ?></td>
		</tr>
		
		<tr>
			<th>Blood Group</th>
			<td><?php echo $blood_group; ?></td>
			<th>Gender</th>
			<td><?php echo $gender; ?></td>
		</tr>
		
		<tr>
			<th>Date of Issue</th>
			<td><?php echo $doi; ?></td>
			<th>Date of Expiry</th>
			<td><?php echo $doe; ?></td>
		</tr>
		
		<tr>
			<th>DTO</th>
			<td colspan="3"><?php echo $ola_name." ($ola_code)"; ?></td>
		</tr>
		
		<tr>
			<th>Current Address</th>
			<td colspan="3"><?php echo $temporary_address." ($temporary_zip)"; ?></td>
		</tr>
		
		<tr>
			<th>Permanent Address</th>
			<td colspan="3"><?php echo $permanent_address." ($permanent_zip)"; ?></td>
		</tr>
		
	</table>
</div>