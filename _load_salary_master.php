<?php
require_once("connect.php");

?>
	<table id="example" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th>#</th>
                        <th>Model</th>
                        <th>SALARY Pattern</th>
                        <th>SALARY</th>
                        <th>Timestamp</th>
                      </tr>
                    </thead>
                    <tbody>
	<?php
	$get_roles = Qry($conn,"SELECT model,salary,type,timestamp FROM dairy.salary_master ORDER BY id ASC");
	
	if(numRows($get_roles)==0)
	{
		echo "<tr>.
			<td colspan='5'>No record found !</td>
			<td style='display: none'></td>
			<td style='display: none'></td>
			<td style='display: none'></td>
			<td style='display: none'></td>
			<td style='display: none'></td>
		</tr>";
	}
	else
	{
		$i=1;
		while($row = fetchArray($get_roles))
		{
			$timestamp = date("d-m-y h:i A",strtotime($row['timestamp']));
			
			if($row['type']=="1")
			{
				$salary_pattern = "<font color='blue'>Per Trip</font>";
				$salary_amount = "<font color='blue'>$row[salary]/trip</font>";
			}
			else
			{
				$salary_pattern = "Monthly";
				$salary_amount = "$row[salary]/month";
			}
		
			echo "<td>$i</td>
				<td>$row[model]</td>
				<td>$salary_pattern</td>
				<td>$salary_amount</td>
				<td>$timestamp</td>
			</tr>";
		$i++;	
		}
	}
	?>	
                    </tbody>
                  </table>
				  
<script> 
$("#loadicon").fadeOut('slow');
$(document).ready(function() {
    $('#example').DataTable();
} );
</script> 			  