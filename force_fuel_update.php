<?php include("header.php"); ?>

<?php
if($_SESSION['ediary_fix_admin']!='TRUCKS')
{
	echo "<script>window.location.href='./';</script>";
	exit();
}
?>
<script>
$(function() {
		$("#veh_no").autocomplete({
		source: 'autofill/get_tno.php',
		// appendTo: "#LaneRuleModal",
		select: function (event, ui) { 
              $('#veh_no').val(ui.item.value);   
             // $("#button1").attr('disabled',false);				   
             return false;
		},
		change: function (event, ui) {
		if(!ui.item){
		    $(event.target).val("");
			alert('Vehicle Number does not exists.');
			$("#veh_no").val('');
			$("#veh_no").focus();
			// $("#button1").attr('disabled',true);
		}}, 
		focus: function (event, ui){
			return false;}
});});
</script>

<div class="content-wrapper">
      <section class="content-header">
          <h1 style="font-size:16px;">Force branch to update fuel qty : </h1>
       </section>
       
	   <section class="content">
          <div class="row">
            <div class="col-xs-12">
			<div class="box">
                <div class="box-body">
				
				<div class="col-md-12">&nbsp;</div>
				
				<div class="col-md-12">
					<div class="row">
						<div class="form-group col-md-3">
							<label style="font-size:12px !important">Vehicle Number <font color="red"><sup>*</sup></font></label>
							<input type="text" class="form-control" id="veh_no" name="veh_no" />
						</div>
						
						<div class="form-group col-md-3">	
							<?php if(!isMobile()) { echo "<label>&nbsp;</label><br />"; } ?>
							<button type="button" style="margin-top:1px;font-size:12px !important" onclick="AddRecord()" class="btn btn-sm btn-success <?php if(isMobile()) { echo "btn-block"; } ?>" id="add_btn"><i class="fa fa-plus-circle" aria-hidden="true"></i> &nbsp; Add New Record</button>
						</div>
				</div>
				
				<div class="col-md-12 table-responsive">
                  <table id="example" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th>#</th>
                        <th>Vehicle_No</th>
                        <th>Status</th>
                        <th>Update_Timestamp</th>
                        <th>Remove</th>
                      </tr>
                    </thead>
                    <tbody>
	<?php
	$get_roles = Qry($conn,"SELECT tno,diesel_left,qty_update_timestamp FROM dairy.own_truck WHERE force_update_diesel_qty='1'");
	
	if(numRows($get_roles)==0)
	{
		echo "<tr><td colspan='4'>No record found !</td></tr>";
	}
	else
	{
		$i=1;
		while($row = fetchArray($get_roles))
		{
			if($row['diesel_left']==0)
			{
				$status = "<font color='red'><b>--Pending--</b></font>";
				$timestamp = "--";
			}
			else
			{
				$status = "<font color='green'><b>Updated</b></font>";
				$timestamp = date("d-m-y h:i A",strtotime($row['qty_update_timestamp']));
			}
			
			
			
			echo "<tr>
				<td>$i</td>
				<td>$row[tno]</td>
				<td>$status</td>
				<td>$timestamp</td>
				<td><button type='button' id='remove_btn_$row[tno]' onclick=RemoveData('$row[tno]') class='btn btn-danger btn-sm'>Remove & Get Fresh Data</button></td>
			</tr>";
		$i++;	
		}
	}
	?>	
                    </tbody>
                  </table>
				 </div> 
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

<script> 
$("#loadicon").fadeOut('slow');
$(document).ready(function() {
    $('#example').DataTable();
} );
</script> 

<?php include("footer.php") ?>

<div id="func_result"></div>  
  
<script>	
function AddRecord()
{
	var tno = $('#veh_no').val();
	
	if(tno!='')
	{
		$('#loadicon').show();
		jQuery.ajax({
			url: "add_new_record_force_qty_update.php",
			data: 'tno=' + tno,
			type: "POST",
			success: function(data) {
			$("#func_result").html(data);
			},
			error: function() {}
		});
	}
}

function RemoveData(tno)
{
	if(confirm("Are you sure ?")==true)
	{
		$('#remove_btn_'+tno).attr('disabled',true);
		$('#loadicon').show();
		jQuery.ajax({
			url: "remove_vehicle_from_force_qty_update.php",
			data: 'tno=' + tno,
			type: "POST",
			success: function(data) {
			$("#func_result").html(data);
			},
			error: function() {}
		});
	}
}
</script>	