<?php
require_once("./connect.php");

$id = escapeString($conn,($_POST['id']));
$actual_wt = escapeString($conn,($_POST['actual_wt']));

$date = date("Y-m-d");
$timestamp = date("Y-m-d H:i:s");

if(empty($id))
{
	AlertErrorTopRight("Voucher ID not found !");
	echo "<script> $('#update_btn_$id').attr('disabled',false); </script>";
	exit();
}

if(empty($actual_wt))
{
	AlertErrorTopRight("Voucher ID not found !");
	echo "<script> $('#update_btn_$id').attr('disabled',false); </script>";
	exit();
}
	
$get_current_data = Qry($conn,"SELECT f.wt12,l.wt12,f.mother_lr_id,f.done,f.trip_id 
FROM freight_form_lr AS f 
LEFT OUTER JOIN lr_sample AS l ON l.id = f.mother_lr_id 
WHERE f.id='$id'");
	
if(!$get_current_data){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	AlertErrorTopRight("Error while Processing Request !");
	echo "<script> $('#update_btn_$id').attr('disabled',false); </script>";
	exit();
}

if(numRows($get_current_data) == 0)
{
	AlertErrorTopRight("Voucher not found !");
	echo "<script> $('#update_btn_$id').attr('disabled',false); </script>";
	exit();
}

$row = fetchArray($get_current_data);

$mother_lr_id = $row['mother_lr_id'];

$update_log = array();
$update_Qry = array();

if($actual_wt != $row['wt12'])
{
	$update_log[]="Act_WT : $row[wt12] to $actual_wt";
	$update_Qry[]="wt12='$actual_wt'";
}

$weight_diff = $actual_wt - $row['wt12'];

$update_log = implode(', ',$update_log); 
$update_Qry = implode(', ',$update_Qry); 

if($update_log=="")
{
	AlertErrorTopRight("Nothing to update !");
	echo "<script> $('#update_btn_$id').attr('disabled',false); </script>";
	exit();
}

StartCommit($conn);
$flag = true;
	
$update_freight_form_lr = Qry($conn,"UPDATE freight_form_lr SET $update_Qry WHERE id='$id'");
		
if(!$update_freight_form_lr){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

if($row['trip_id']!=0)
{
	$update_trip = Qry($conn,"UPDATE dairy.trip SET act_wt=act_wt+('$weight_diff') WHERE id='$row[trip_id]'");
		
	if(!$update_trip){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
	
	if(AffectedRows($conn) == 0)
	{
		$update_trip_final = Qry($conn,"UPDATE dairy.trip_final SET act_wt=act_wt+('$weight_diff') WHERE trip_id='$row[trip_id]'");
		
		if(!$update_trip_final){
			$flag = false;
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		}
	}
}

$update_lr_sample = Qry($conn,"UPDATE lr_sample SET wt12='$actual_wt' WHERE id='$mother_lr_id'");
		
if(!$update_lr_sample){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}
		
if($flag)
{
	MySQLCommit($conn);
	closeConnection($conn);
	AlertRightCornerSuccess("Updated !");
	echo "<script>$('#update_btn_$id').attr('disabled',false); </script>";
	exit();
}
else
{
	MySQLRollBack($conn);
	closeConnection($conn);
	
	AlertErrorTopRight("Error while Processing Request !");
	echo "<script> $('#update_btn_$id').attr('disabled',false); </script>";
	exit();
}
?>