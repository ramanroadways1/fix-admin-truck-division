<?php
require_once 'connect.php';

$date = date("Y-m-d");
$timestamp = date("Y-m-d H:i:s");

$id = escapeString($conn,(trim($_POST['id'])));
$vou_no = escapeString($conn,(trim($_POST['vou_no'])));

if(empty($vou_no)){
	AlertRightCornerError("Voucher number not found !");
	exit();
}

$chk_trip = Qry($conn,"SELECT done,bilty_no FROM mkt_bilty WHERE id='$id'");

if(!$chk_trip){
	AlertErrorTopRight("Error while processing request !");
	errorLog(getMySQLError($conn),$conn,$page_url,__LINE__);
	exit();
}

if(numRows($chk_trip) == 0)
{
	AlertErrorTopRight("Voucher : $vou_no not found !");
	exit();
}

$row_trip = fetchArray($chk_trip);

if($row_trip['bilty_no'] != $vou_no)
{
	AlertErrorTopRight("Voucher not verified !");
	exit();
}

if($row_trip['done']=="1")
{
	AlertErrorTopRight("Delete e-diary trip frist !");
	exit();
}

StartCommit($conn);
$flag = true;

$delete_bilty = Qry($conn,"DELETE FROM mkt_bilty WHERE id='$id'");

if(!$delete_bilty){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

$delete_bilty_txns = Qry($conn,"DELETE FROM dairy.bilty_book WHERE bilty_no='$vou_no'");

if(!$delete_bilty_txns){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

$delete_bilty_bal = Qry($conn,"DELETE FROM dairy.bilty_balance WHERE bilty_no='$vou_no'");

if(!$delete_bilty_bal){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

if($flag)
{
	MySQLCommit($conn);
	closeConnection($conn);
	AlertRightCornerSuccess("Deleted successfully !");
	echo "<script>
		$('#edit_btn_$id').attr('disabled',true);
		$('#dlt_btn_$id').attr('disabled',true);
		
		$('#edit_btn_$id').attr('onclick','');
		$('#dlt_btn_$id').attr('onclick','');
	</script>";
	exit();
}
else
{
	MySQLRollBack($conn);
	closeConnection($conn);
	AlertErrorTopRight("Error while processing request !");
	exit();
}	
?>