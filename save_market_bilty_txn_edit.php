<?php
require_once("connect.php");

$timestamp = date("Y-m-d H:i:s");
$id = escapeString($conn,($_POST['id']));
$txn_date = escapeString($conn,($_POST['txn_date']));
$adv_bal = escapeString($conn,($_POST['adv_bal']));
$txn_value = escapeString($conn,($_POST['txn_value']));
$narration = escapeString($conn,($_POST['narration']));

$get_data = Qry($conn,"SELECT bilty_no,date,advbal,trans_value,narration FROM dairy.bilty_book WHERE id='$id'");

if(!$get_data){
	AlertErrorTopRight("Error while processing request !");
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	exit();
}

if(numRows($get_data) == 0)
{
	AlertErrorTopRight("Txn not found !");
	echo "<script>
		$('#edit_modal_btn').attr('disabled',false);
	</script>";
	exit();
}

$row = fetchArray($get_data);

$update_log = array();
$update_Qry = array();

if($txn_date!=$row['date'])
{
	$update_log[]="Txn_Date : $row[date] to $txn_date";
	$update_Qry[]="date='$txn_date'";
}

if($adv_bal!=$row['advbal'])
{
	$update_log[]="Adv_Bal : $row[advbal] to $adv_bal";
	$update_Qry[]="advbal='$adv_bal'";
}

if($txn_value!=$row['trans_value'])
{
	$update_log[]="Txn_Value : $row[trans_value] to $txn_value";
	$update_Qry[]="trans_value='$txn_value'";
}

if($narration!=$row['narration'])
{
	$update_log[]="Narration : $row[narration] to $narration";
	$update_Qry[]="narration='$narration'";
}

$update_log = implode(', ',$update_log); 
$update_Qry = implode(', ',$update_Qry); 

if($update_log=="")
{
	AlertError("Nothing to update !");
	echo "<script>$('#edit_modal_btn').attr('disabled',false);</script>";
	exit();
}

StartCommit($conn);
$flag = true;

$update_bilty_main = Qry($conn,"UPDATE dairy.bilty_book SET $update_Qry WHERE id='$id'");

if(!$update_bilty_main){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

$insertLog = Qry($conn,"INSERT INTO dairy.ediary_admin_log(code,tno,action,desct,branch,username,timestamp) VALUES ('$id','$row[bilty_no]',
'Market_Bilty_Txn_Edit','$update_log','Fix_Admin','$_SESSION[ediary_fix_admin]','$timestamp')");

if(!$insertLog){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}
		
if($flag)
{ 
	MySQLCommit($conn);
	closeConnection($conn);	
	AlertRightCornerSuccess("Updated Successfully.");
	echo "<script>
		$('#modal_edit_close_btn')[0].click();
		$('#add_btn')[0].click(); 
	</script>";
	exit();
}
else
{
	MySQLRollBack($conn);
	closeConnection($conn);
	AlertError("Error while processing request..");
	echo "<script>$('#edit_modal_btn').attr('disabled',false);</script>";
	exit();
}
?>