<?php
require_once("./connect.php");

$timestamp = date("Y-m-d H:i:s"); 

$trip_id=escapeString($conn,strtoupper($_POST['trip_id']));

if(empty($trip_id) || $trip_id=='' || $trip_id==0)
{
	AlertRightCornerError("Trip not found..");
	echo "<script>$('#exp_sel_option').val('');</script>";
	exit();
}

$amount = escapeString($conn,$_POST['expense_amount']);
$exp = escapeString($conn,($_POST['elem']));
$exp_code1 = explode("_",$exp)[0];
$exp_name1 = explode("_",$exp)[1];

$exp_code = $exp_code1;
$exp = $exp_name1;

$check_trip = Qry($conn,"SELECT t.id,t.tno,t.driver_code,t.trip_no,t.lr_type,t.loaded_hisab,d.name as driver_name 
FROM dairy.trip AS t 
LEFT OUTER JOIN dairy.driver AS d ON d.code=t.driver_code 
WHERE t.id='$trip_id'");

if(!$check_trip){
	AlertRightCornerError("Error..");
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	echo "<script>$('#exp_sel_option').attr('disabled',false);$('#exp_sel_option').val('')</script>";
	exit();
}

if(numRows($check_trip)==0)
{
	AlertRightCornerError("Trip not found..");
	errorLog("Running trip not found. TripId: $trip_id.",$conn,$page_name,__LINE__);
	echo "<script>$('#exp_sel_option').attr('disabled',false);$('#exp_sel_option').val('')</script>";
	exit();
}

$row_trip = fetchArray($check_trip);

$tno = $row_trip['tno'];

$chk_exp_lock = Qry($conn,"SELECT lock_on_empty FROM dairy.exp_head WHERE exp_code='$exp_code'");

if(!$chk_exp_lock){
	AlertRightCornerError("Error..");
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	echo "<script>$('#exp_sel_option').attr('disabled',false);$('#exp_sel_option').val('')</script>";
	exit();
}

if(numRows($chk_exp_lock)==0)
{
	AlertRightCornerError("Invalid Expense !!");
	echo "<script>$('#exp_sel_option').attr('disabled',false);$('#exp_sel_option').val('')</script>";
	exit();
}

$row_code = fetchArray($chk_exp_lock);
$empty_lock = escapeString($conn,($row_code['lock_on_empty']));

$driver_name = $row_trip['driver_name'];
$driver_code = $row_trip['driver_code'];
$trip_no = $row_trip['trip_no'];

if($row_trip['lr_type']=='EMPTY' AND $empty_lock=='1')
{
	if($tno!='GJ12BV3924' AND $tno!='GJ18AZ3641' AND $tno!='GJ18AU7730')
	{
		echo "<font color='red'>Expense: $exp not allowed in EMPTY trip !</font>";
	}
}

if($row_trip['loaded_hisab']=='1' AND ($exp_code=='TR00725' || $exp_code=='TR00009'))
{
	echo "<font color='red'>Expense: $exp not allowed in loaded vehicle hisab !</font>";
}

$chk_exp_limit = Qry($conn,"SELECT exp_name,amount FROM dairy.trip_exp_limit WHERE exp='$exp_code' AND type='1' AND $amount>`amount`");

if(!$chk_exp_limit){
	AlertRightCornerError("Error..");
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	echo "<script>$('#exp_sel_option').attr('disabled',false);$('#exp_sel_option').val('');</script>";
	exit();
}

if(numRows($chk_exp_limit)>0)
{
	$row_exp_limit = fetchArray($chk_exp_limit);
	echo "<font color='red'>Max limit of expense: $row_exp_limit[exp_name] is: $row_exp_limit[amount] !";
}

$can_entry_times=Qry($conn,"SELECT exp_name,entry_limit FROM dairy.trip_exp_limit WHERE exp='$exp_code' AND type='2'");

if(!$can_entry_times){
	AlertRightCornerError("Error..");
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	echo "<script>$('#exp_sel_option').attr('disabled',false);$('#exp_sel_option').val('');</script>";
	exit();
}

if(numRows($can_entry_times) > 0)
{
	$row_times = fetchArray($can_entry_times);
	
	$fetch_total_entry_of_exp=Qry($conn,"SELECT id FROM dairy.trip_exp WHERE trip_id='$trip_id' AND exp_code='$exp_code'");
	
	if(!$fetch_total_entry_of_exp){
		AlertRightCornerError("Error..");
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		echo "<script>$('#exp_sel_option').attr('disabled',false);$('#exp_sel_option').val('');</script>";
		exit();
	}

	if(numRows($fetch_total_entry_of_exp) >= $row_times['entry_limit'])
	{
		echo "<font color='red'>Max limit of expense: $row_times[exp_name] entry is: $row_times[entry_limit] times !</font>";
	}
}

echo "<script>
	$('#loadicon').fadeOut('slow');
</script>";
?>