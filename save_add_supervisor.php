<?php
require_once 'connect.php';

$date = date("Y-m-d");
$timestamp = date("Y-m-d H:i:s");

$title=escapeString($conn,strtoupper(trim($_POST['title'])));
$username=escapeString($conn,strtoupper(trim($_POST['username'])));
$password=escapeString($conn,(trim($_POST['password'])));
$mobile_no=escapeString($conn,(trim($_POST['mobile_no'])));
$supervisor_type=escapeString($conn,(trim($_POST['supervisor_type'])));

if(!preg_match('/^[a-z A-Z\.]*$/', $title))
{
	AlertErrorTopRight("Invalid username !");
	echo "<script>$('#add_btn').attr('disabled', false);</script>";
	exit();
}

if($supervisor_type!=1 AND $supervisor_type!=2)
{
	AlertErrorTopRight("User role not found !");
	echo "<script>$('#add_btn').attr('disabled', false);</script>";
	exit();
}

if(($username)=='')
{
	AlertErrorTopRight("Please enter username !");
	echo "<script>$('#add_btn').attr('disabled', false);</script>";
	exit();
}

if(($password)=='')
{
	AlertErrorTopRight("Please enter password !");
	echo "<script>$('#add_btn').attr('disabled', false);</script>";
	exit();
}

if(!preg_match('/^[a-zA-Z0-9_.-@#\.]*$/', $password))
{
	AlertErrorTopRight("Invalid password !");
	echo "<script>$('#add_btn').attr('disabled', false);</script>";
	exit();
}

if(!preg_match('/^[a-zA-Z0-9_.-@#\.]*$/', $username))
{
	AlertErrorTopRight("Invalid username !");
	echo "<script>$('#add_btn').attr('disabled', false);</script>";
	exit();
}

if(!preg_match('/^[0-9\.]*$/', $mobile_no))
{
	AlertErrorTopRight("Invalid mobile number !");
	echo "<script>$('#add_btn').attr('disabled', false);</script>";
	exit();
}

if(strlen($mobile_no)!=10)
{
	AlertErrorTopRight("Check mobile number !");
	echo "<script>$('#add_btn').attr('disabled', false);</script>";
	exit();
}

$check_user = Qry($conn,"SELECT id FROM user WHERE username='$username'");

if(!$check_user){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	AlertErrorTopRight("Error while processing request !");
	echo "<script>$('#add_btn').attr('disabled', false);</script>";
	exit();
}

if(numRows($check_user)>0)
{
	AlertErrorTopRight("Username already exists !");
	echo "<script>$('#add_btn').attr('disabled', false);</script>";
	exit();
}

$check_user2 = Qry($conn,"SELECT id FROM dairy.user WHERE username='$username'");

if(!$check_user2){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	AlertErrorTopRight("Error while processing request !");
	echo "<script>$('#add_btn').attr('disabled', false);</script>";
	exit();
}

if(numRows($check_user2)>0)
{
	AlertErrorTopRight("Username already exists !");
	echo "<script>$('#add_btn').attr('disabled', false);</script>";
	exit();
}

StartCommit($conn);
$flag = true;

if($supervisor_type=="1")
{
	$insert_user = Qry($conn,"INSERT INTO user(title,username,password,role,active_login,mobile_no,timestamp) VALUES ('DIARY SUPERVISOR',
	'$username','".md5($password)."','40','1','$mobile_no','$timestamp')");

	if(!$insert_user){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
}

$insert_user2 = Qry($conn,"INSERT INTO user(title,username,role,type,timestamp) VALUES ('$title','$username','1','$supervisor_type','$timestamp')");

if(!$insert_user2){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

if($flag)
{
	MySQLCommit($conn);
	closeConnection($conn);
	AlertRightCornerSuccess("User successfully added !");
	echo "<script>LoadTable();</script>";
	exit();
}
else
{
	MySQLRollBack($conn);
	closeConnection($conn);
	AlertErrorTopRight("Error while processing request !");
	echo "<script>$('#add_btn').attr('disabled', false);</script>";
	exit();
}	
?>