<?php
require_once 'connect.php';

$timestamp = date("Y-m-d H:i:s");

$cfwd_db = escapeString($conn,($_POST['cfwd_db']));
$pay_db = escapeString($conn,($_POST['pay_db']));
$crho_db = escapeString($conn,($_POST['crho_db']));
$closing_balance = escapeString($conn,($_POST['closing_balance']));
$cash_paid_db = escapeString($conn,($_POST['cash_paid_db']));
$rtgs_paid_db = escapeString($conn,($_POST['rtgs_paid_db']));

$hisab_type = escapeString($conn,($_POST['hisab_type']));
$cfwd = escapeString($conn,($_POST['cfwd']));
$pay = escapeString($conn,($_POST['pay']));
$crho = escapeString($conn,($_POST['crho']));
$cash_pay = escapeString($conn,($_POST['cash_pay']));
$rtgs_pay = escapeString($conn,($_POST['rtgs_pay']));
$hisab_id = escapeString($conn,($_POST['hisab_id']));
$trip_no = escapeString($conn,($_POST['trip_no']));

$get_hisab_data = Qry($conn,"SELECT tno,trip_no,closing_balance,hisab_type,driver,branch,branch_user,date,cash_vou,chq_vou,rtgs_vou,credit_cash,
timestamp FROM dairy.log_hisab WHERE id='$hisab_id'");

if(!$get_hisab_data){
	AlertRightCornerError("Error while processing request !");
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	exit();
}
	
if(numRows($get_hisab_data)==0)
{
	AlertRightCornerError("Hisab not found !");
	exit();
}

$row = fetchArray($get_hisab_data);

$tno = $row['tno'];
$closing_balance_log_hisab = $row['closing_balance'];

if($row['trip_no'] != $trip_no)
{
	AlertRightCornerError("Trip number not verified !");
	exit();
}

$get_company = Qry($conn,"SELECT comp as company FROM dairy.own_truck WHERE tno='$tno'");

if(!$get_company){
	AlertRightCornerError("Error while processing request !");
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	exit();
}
	
if(numRows($get_company)==0)
{
	AlertRightCornerError("Company not found !");
	exit();
}

$row_get_company = fetchArray($get_company);
$company = $row_get_company['company'];

$driver_code = $row['driver'];
$hisab_type_db = $row['hisab_type'];

$get_driver_data = Qry($conn,"SELECT d.name as driver_name,d2.acname,d2.acno,d2.bank,d2.ifsc 
FROM dairy.driver AS d 
LEFT OUTER JOIN dairy.driver_ac AS d2 ON d2.code = d.code 
WHERE d.code = '$driver_code'");

if(!$get_driver_data){
	AlertRightCornerError("Error while processing request !");
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	exit();
}
	
if(numRows($get_driver_data)==0)
{
	AlertRightCornerError("Driver not found !");
	exit();
}

$row_driver = fetchArray($get_driver_data);

$driver_name = $row_driver['driver_name'];
$acname = $row_driver['acname'];
$acno = $row_driver['acno'];
$bank_name = $row_driver['bank'];
$ifsc = $row_driver['ifsc'];

if($hisab_type_db=="1" AND $hisab_type=="1")
{
	AlertRightCornerError("Nothing to update !");
	exit();
}

if($hisab_type_db=="3" AND $hisab_type=="4")
{
	AlertRightCornerError("Nothing to update !");
	exit();
}

if($hisab_type_db=="1")
{
	$get_hisab_row = Qry($conn,"SELECT id,driver_code,tno,branch,date,timestamp,'0' as paid_amount,'0' as crho_amount 
	FROM dairy.driver_book WHERE trip_no='$trip_no' AND desct='HISAB-CARRY-FWD'");
}
else if($hisab_type_db=="2")
{
	$get_hisab_row = Qry($conn,"SELECT id,driver_code,tno,branch,date,timestamp,credit as paid_amount,'0' as crho_amount 
	FROM dairy.driver_book WHERE trip_no='$trip_no' AND desct='HISAB-PAID'");
}
else if($hisab_type_db=="3")
{
	$get_hisab_row = Qry($conn,"SELECT id,driver_code,tno,branch,date,timestamp,credit as paid_amount,'0' as crho_amount 
	FROM dairy.driver_book WHERE trip_no='$trip_no' AND desct='HISAB-PAID'");
}
else if($hisab_type_db=="4")
{
	$get_hisab_row = Qry($conn,"SELECT id,driver_code,tno,branch,date,timestamp,'0' as paid_amount,debit as crho_amount 
	FROM dairy.driver_book WHERE trip_no='$trip_no' AND desct='CREDIT-HO'");
}
else
{
	AlertRightCornerError("Invalid hisab type !");
	exit();
}

if(!$get_hisab_row){
	AlertRightCornerError("Error while processing request !");
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	exit();
}
	
if(numRows($get_hisab_row)==0)
{
	AlertRightCornerError("Hisab record not found !");
	exit();
}

$row_hisab = fetchArray($get_hisab_row);

$hisab_row_id = $row_hisab['id'];
$hisab_branch = $row_hisab['branch'];
$hisab_timestamp = $row_hisab['timestamp'];
$hisab_date = $row_hisab['date'];
$paid_amount_db = $row_hisab['paid_amount'];
$crho_amount_db = $row_hisab['crho_amount'];

$get_da_and_salary = Qry($conn,"SELECT 
(SELECT desct FROM dairy.driver_book WHERE trip_no='$trip_no' AND desct like 'DA%') as 'da_desc',
(SELECT debit FROM dairy.driver_book WHERE trip_no='$trip_no' AND desct like 'DA%') as 'da_amount',
(SELECT desct FROM dairy.driver_book WHERE trip_no='$trip_no' AND desct like 'SALARY%') as 'sal_desc',
(SELECT debit FROM dairy.driver_book WHERE trip_no='$trip_no' AND desct like 'SALARY%') as 'sal_amount',
(SELECT debit FROM dairy.driver_book WHERE trip_no='$trip_no' AND desct='EXP-TELEPHONE') as 'tel_exp_amount'");

if(!$get_da_and_salary){
	AlertRightCornerError("Error while processing request !");
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	exit();
}

$row_get_da_sal = fetchArray($get_da_and_salary);
	
if($row_get_da_sal['sal_amount']>0)
{
	$nrr = "$tno/$driver_name, DA: $row_get_da_sal[da_amount], TEL: $row_get_da_sal[tel_exp_amount], AIR & GREASE : $row_get_da_sal[tel_exp_amount], $row_get_da_sal[sal_desc].";
}
else
{
	$nrr="$tno/$driver_name, DA: $row_get_da_sal[da_amount], TEL: $row_get_da_sal[tel_exp_amount], AIR & GREASE : $row_get_da_sal[tel_exp_amount].";
}

$get_closing_amount = Qry($conn,"SELECT id,balance FROM dairy.driver_book WHERE trip_no='$trip_no' AND id<'$hisab_row_id' ORDER BY id DESC LIMIT 1");

if(!$get_closing_amount){
	AlertRightCornerError("Error while processing request !");
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	exit();
}
	
if(numRows($get_closing_amount)==0)
{
	AlertRightCornerError("Closing balance not found !");
	echo "<script>$('#button_da_form').attr('disabled',false);</script>";
	exit();
}
	
$closing_row = fetchArray($get_closing_amount);
$closing_balance_db = $closing_row['balance'];
$closing_balance_id = $closing_row['id'];

if($closing_balance_db != $closing_balance)
{
	AlertRightCornerError("Closing balance not verified. VerficationType - 1 !");
	exit();
}

if($closing_balance_db != $closing_balance_log_hisab)
{
	AlertRightCornerError("Closing balance not verified. VerficationType - 2 !");
	exit();
}

if($hisab_type=="1")
{
	if($cfwd != $closing_balance_db)
	{
		AlertRightCornerError("Amount not verified !");
		exit();
	}
	
	$pay=0;
	$crho=0;
	$cash_pay=0;
	$rtgs_pay=0;
}
else if($hisab_type=="2")
{
	if($pay != abs($closing_balance_db))
	{
		AlertRightCornerError("Amount not verified !");
		exit();
	}
	
	$cfwd=0;
	$crho=0;
	
	if(($cash_pay+$rtgs_pay) != $pay)
	{
		AlertRightCornerError("Amount not verified !");
		exit();
	}
}
else if($hisab_type=="3")
{
	if(($pay+$cfwd) != abs($closing_balance_db))
	{
		AlertRightCornerError("Amount not verified !");
		exit();
	}
	
	$crho=0;
	
	if(($cash_pay+$rtgs_pay) != $pay)
	{
		AlertRightCornerError("Amount not verified !");
		exit();
	}
}
else if($hisab_type=="4")
{
	if($crho != $closing_balance_db)
	{
		AlertRightCornerError("Amount not verified !");
		exit();
	}
	
	$pay=0;
	$cfwd=0;
	$cash_pay=0;
	$rtgs_pay=0;
}

if($cash_pay>3000)
{
	AlertRightCornerError("Max cash allowed is: 3000 !");
	echo "<script>$('#button_form_hisab').attr('disabled',false);</script>";
	exit();
}

if($rtgs_pay>0 AND ($acname=='' || $acno==''))
{
	AlertRightCornerError("Driver account details not updated !");
	echo "<script>$('#button_form_hisab').attr('disabled',false);</script>";
	exit();
}

if($closing_balance_db>0)
{
	if($hisab_type=="2" || $hisab_type=="3")
	{
		AlertRightCornerError("Insufficient balance to pay hisab !");
		echo "<script>$('#button_form_hisab').attr('disabled',false);</script>";
		exit();
	}
}
else if($closing_balance_db<0)
{
	if($hisab_type=="4")
	{
		AlertRightCornerError("Insufficient balance to credit to cashbook !");
		echo "<script>$('#button_form_hisab').attr('disabled',false);</script>";
		exit();
	}
}
else
{
	if($hisab_type=="2" || $hisab_type=="3" || $hisab_type=="4")
	{
		AlertRightCornerError("Insufficient balance to pay or credit to cashbook !");
		echo "<script>$('#button_form_hisab').attr('disabled',false);</script>";
		exit();
	}
}

if($cash_pay>0)
{
	$check_bal = Qry($conn,"SELECT balance,balance2 FROM user WHERE username='$hisab_branch'");

	if(!$check_bal){
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		AlertRightCornerError("Error while processing request !");
		echo "<script>$('#button_form_hisab').attr('disabled',false);</script>";
		exit();
	}

	if(numRows($check_bal)==0)
	{
		errorLog("Branch balance not found. Branch: $hisab_branch.",$conn,$page_name,__LINE__);
		AlertRightCornerError("Branch not found !");
		echo "<script>$('#button_form_hisab').attr('disabled',false);</script>";
		exit();
	}

	$row_bal = fetchArray($check_bal);

	$rrpl_cash = $row_bal['balance'];
	$rr_cash = $row_bal['balance2'];

	if($company=='RRPL' && $rrpl_cash>=$cash_pay)
	{
	}
	else if($company=='RAMAN_ROADWAYS' && $rr_cash>=$cash_pay)
	{
	}
	else
	{
		AlertRightCornerError("Insufficient balance in company: $company !");
		echo "<script>$('#button_form_hisab').attr('disabled',false);</script>";
		exit();
	}
}

// StartCommit($conn);
// $flag = true;

$rtgs_vou_id="0";
$cash_vou_id="0";

if($hisab_type_db=="2" || $hisab_type_db=="3")
{
	$get_cash_vouchers = Qry($conn,"SELECT id,tdvid,company,user as branch,amt as amount FROM mk_tdv WHERE timestamp='$hisab_timestamp' AND 
	truckno='$tno' AND hisab_vou='1' AND mode='CASH'");

	if(!$get_cash_vouchers){
		AlertRightCornerError("Error while processing request !");
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		exit();
	}

	if(numRows($get_cash_vouchers)>0)
	{
		$row_cash = fetchArray($get_cash_vouchers);
		$cash_vou_id = $row_cash['id'];
		$cash_vou_no = $row_cash['tdvid'];
		$cash_vou_company = $row_cash['company'];
		$cash_vou_branch = $row_cash['branch'];
		$cash_vou_amount = $row_cash['amount'];
	}
	
	$get_rtgs_vouchers = Qry($conn,"SELECT id,tdvid,amt as amount FROM mk_tdv WHERE timestamp='$hisab_timestamp' AND truckno='$tno' AND 
	hisab_vou='1' AND mode='NEFT'");

	if(!$get_rtgs_vouchers){
		AlertRightCornerError("Error while processing request !");
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		exit();
	}

	if(numRows($get_rtgs_vouchers)==0 AND numRows($get_cash_vouchers)==0)
	{
		AlertRightCornerError("Hisab voucher not found !");
		exit();
	}

	if(numRows($get_rtgs_vouchers)>0)
	{
		$row_rtgs = fetchArray($get_rtgs_vouchers);
		$rtgs_vou_id = $row_rtgs['id'];
		$rtgs_vou_no = $row_rtgs['tdvid'];
		$rtgs_vou_amount = $row_rtgs['amount'];
		
		$chk_rtgs_done = Qry($conn,"SELECT id FROM rtgs_fm WHERE fno='$rtgs_vou_no' AND colset_d='1'");

		if(!$chk_rtgs_done){
			AlertRightCornerError("Error while processing request !");
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			exit();
		}

		if(numRows($chk_rtgs_done)>0)
		{
			AlertRightCornerError("RTGS/NEFT Payment request downloaded by accounts dpt !");
			echo "<script>$('#button_form_hisab').attr('disabled',false);</script>";
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			exit();
		}
	}
}
	
StartCommit($conn);
$flag = true;	

$insert_script = Qry($conn,"INSERT INTO dairy.running_scripts(file_name,timestamp) VALUES ('Hisab_Manage_Admin','$timestamp')");

if(!$insert_script){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

if($hisab_type_db=="2")
{
	$up_amount_hold_func = "amount_hold = amount_hold - ('$paid_amount_db')";
	$up_balance_func = "balance = balance - ('$paid_amount_db')";
	$up_opening_func = "opening = opening - ('$paid_amount_db')";
	$up_closing_func = "closing = closing - ('$paid_amount_db')";
}
else if($hisab_type_db=="3")
{
	$up_amount_hold_func = "amount_hold = amount_hold - ('$paid_amount_db')";
	$up_balance_func = "balance = balance - ('$paid_amount_db')";
	$up_opening_func = "opening = opening - ('$paid_amount_db')";
	$up_closing_func = "closing = closing - ('$paid_amount_db')";
}
else if($hisab_type_db=="4")
{
	$up_amount_hold_func = "amount_hold = amount_hold + ('$crho_amount_db')";
	$up_balance_func = "balance = balance + ('$crho_amount_db')";
	$up_opening_func = "opening = opening + ('$crho_amount_db')";
	$up_closing_func = "closing = closing + ('$crho_amount_db')";
}

if($hisab_type_db=="2" || $hisab_type_db=="3" || $hisab_type_db=="4")
{
	$update_d_balance = Qry($conn,"UPDATE dairy.driver_up WHERE SET $up_amount_hold_func WHERE code='$driver_code' ORDER BY id DESC LIMIT 1");
	
	if(!$update_d_balance){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
	
	$updt_bal_rows = Qry($conn,"UPDATE dairy.driver_book SET $up_balance_func WHERE id>'$closing_balance_id' AND driver_code='$driver_code'");
		
	if(!$updt_bal_rows){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
	
	$get_opening_closing_id = Qry($conn,"SELECT id FROM dairy.opening_closing WHERE trip_no='$trip_no'");
	
	if(!$get_opening_closing_id){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}

	if(numRows($get_opening_closing_id)==0)
	{
		$flag = false;
		errorLog("Opening closing not found. Trip-no: $trip_no.",$conn,$page_name,__LINE__);
	}

	$row_opn_cls = fetchArray($get_opening_closing_id);

	$update_opening_bal = Qry($conn,"UPDATE dairy.opening_closing SET $up_opening_func WHERE id>'$row_opn_cls[id]' AND driver='$driver_code'");
		
	if(!$update_opening_bal){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}

	$update_closing_bal = Qry($conn,"UPDATE dairy.opening_closing SET $up_closing_func WHERE id>='$row_opn_cls[id]' AND driver='$driver_code'");
		
	if(!$update_closing_bal){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
}	

if($rtgs_vou_id!=0)
{
	$delete_rtgs_req = Qry($conn,"DELETE FROM rtgs_fm WHERE fno='$rtgs_vou_no' AND colset_d!='1'");
	
	if(!$delete_rtgs_req){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
	
	if(AffectedRows($conn)==0)
	{
		$flag = false;
		errorLog("Unable to delete RTGS voucher. Vou_no: $rtgs_vou_no. Trip_no: $trip_no.",$conn,$page_name,__LINE__);
	}
	
	$delete_passbook = Qry($conn,"DELETE FROM passbook WHERE vou_no='$rtgs_vou_no'");
	
	if(!$delete_passbook){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
	
	$delete_rtgs_vou = Qry($conn,"DELETE FROM mk_tdv WHERE id='$rtgs_vou_id'");
	
	if(!$delete_rtgs_vou){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
	
	$update_driver_bal_rtgs = Qry($conn,"UPDATE dairy.driver_up SET amount_hold=amount_hold-'$rtgs_vou_amount' WHERE code='$driver_code' 
	ORDER BY id DESC LIMIT 1");
	
	if(!$update_driver_bal_rtgs){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
}

if($cash_vou_id!=0)
{
	if($cash_vou_company=='RRPL')
	{
		$balance_col_name="balance";
	}
	else
	{
		$balance_col_name="balance2";
	}
	
	$update_cashbook = Qry($conn,"UPDATE cashbook SET `$balance_col_name`=`$balance_col_name`+'$cash_vou_amount' WHERE id>'$cash_vou_id' AND 
	comp='$cash_vou_company' AND user='$cash_vou_branch'");
	
	if(!$update_cashbook){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
	
	$update_balance= Qry($conn,"UPDATE user SET `$balance_col_name`=`$balance_col_name`+'$cash_vou_amount' WHERE username='$cash_vou_branch'");
	
	if(!$update_balance){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
	
	$delete_rtgs_vou = Qry($conn,"DELETE FROM mk_tdv WHERE id='$cash_vou_id'");
	
	if(!$delete_rtgs_vou){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
	
	$update_driver_bal_cash = Qry($conn,"UPDATE dairy.driver_up SET amount_hold=amount_hold-'$cash_vou_amount' WHERE code='$driver_code' 
	ORDER BY id DESC LIMIT 1");
	
	if(!$update_driver_bal_cash){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
}

$delete_old_hisab_record = Qry($conn,"DELETE FROM dairy.driver_book WHERE trip_no='$trip_no' AND id>'$closing_balance_id'");
	
if(!$delete_old_hisab_record){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

$copy_db_record = Qry($conn,"INSERT INTO dairy.driver_book_temp2(driver_code,tno,trip_id,trip_no,trans_id,desct,credit,debit,balance,date,
branch,narration,timestamp) SELECT driver_code,tno,trip_id,trip_no,trans_id,desct,credit,debit,balance,date,branch,narration,timestamp 
FROM dairy.driver_book WHERE id>'$closing_balance_id'");
	
if(!$copy_db_record){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

$delete_old_record = Qry($conn,"DELETE FROM dairy.driver_book WHERE id>'$closing_balance_id'");
	
if(!$delete_old_record){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

$hisab_cfwd_id = 0;
$hisab_pay_id = 0;
$hisab_crho_id = 0;
$last_row_insert_id = 0;
$cash_credit_id = 0;
	
if($hisab_type=="1")
{
	$insert_carry_fwd = Qry($conn,"INSERT INTO dairy.driver_book(driver_code,tno,trip_no,desct,balance,date,branch,narration,timestamp) VALUES 
	('$driver_code','$tno','$trip_no','HISAB-CARRY-FWD','$cfwd','$hisab_date','$hisab_branch','HISAB_MODIFIED','$hisab_timestamp')");
	
	if(!$insert_carry_fwd){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
	
	$hisab_cfwd_id = getInsertID($conn);
	
	$last_row_insert_id = $hisab_cfwd_id;
	
	$update_driver_balance = Qry($conn,"UPDATE dairy.driver_up SET amount_hold=amount_hold-'$cfwd' WHERE code='$driver_code' ORDER BY id DESC LIMIT 1");
	
	if(!$update_driver_balance){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
	
	$update_balance_rows = Qry($conn,"UPDATE dairy.driver_book_temp2 SET balance=balance-'$cfwd' WHERE driver_code='$driver_code'");
		
	if(!$update_balance_rows){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
	
	$get_opening_closing_id = Qry($conn,"SELECT id FROM dairy.opening_closing WHERE trip_no='$trip_no'");
	
	if(!$get_opening_closing_id){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}

	if(numRows($get_opening_closing_id)==0)
	{
		$flag = false;
		errorLog("Opening closing not found. Trip-no: $trip_no.",$conn,$page_name,__LINE__);
	}

	$row_opn_cls = fetchArray($get_opening_closing_id);

	$update_opening_bal = Qry($conn,"UPDATE dairy.opening_closing SET opening=opening-'$cfwd' WHERE id>'$row_opn_cls[id]' 
	AND driver='$driver_code'");
		
	if(!$update_opening_bal){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}

	$update_closing_bal = Qry($conn,"UPDATE dairy.opening_closing SET closing=closing-'$cfwd' WHERE id>='$row_opn_cls[id]' 
	AND driver='$driver_code'");
		
	if(!$update_closing_bal){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
}
else if($hisab_type=="2")
{
	if($pay<=0)
	{
		$flag = false;
		errorLog("Invalid payment amount: $pay. Trip_no: $trip_no.",$conn,$page_name,__LINE__);
	}
	
	if(abs($closing_balance_db)-$pay!=0)
	{
		$flag = false;
		errorLog("Balance amount: $pay is not zero. Trip_no: $trip_no.",$conn,$page_name,__LINE__);
	}
	
	$insert_paid = Qry($conn,"INSERT INTO dairy.driver_book(driver_code,tno,trip_no,desct,credit,balance,date,branch,narration,timestamp) VALUES 
	('$driver_code','$tno','$trip_no','HISAB-PAID','$pay','0','$hisab_date','$hisab_branch','HISAB_MODIFIED','$hisab_timestamp')");
	
	if(!$insert_paid){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
	
	$hisab_pay_id = getInsertID($conn);
	
	$last_row_insert_id = $hisab_pay_id;
	
	$update_driver_balance = Qry($conn,"UPDATE dairy.driver_up SET amount_hold=amount_hold+'$pay' WHERE code='$driver_code' ORDER BY id DESC LIMIT 1");
	
	if(!$update_driver_balance){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
	
	$update_balance_rows = Qry($conn,"UPDATE dairy.driver_book_temp2 SET balance=balance+'$pay' WHERE driver_code='$driver_code'");
		
	if(!$update_balance_rows){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
	
	$get_opening_closing_id = Qry($conn,"SELECT id FROM dairy.opening_closing WHERE trip_no='$trip_no'");
	
	if(!$get_opening_closing_id){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}

	if(numRows($get_opening_closing_id)==0)
	{
		$flag = false;
		errorLog("Opening closing not found. Trip-no: $trip_no.",$conn,$page_name,__LINE__);
	}

	$row_opn_cls = fetchArray($get_opening_closing_id);

	$update_opening_bal = Qry($conn,"UPDATE dairy.opening_closing SET opening=opening+'$pay' WHERE id>'$row_opn_cls[id]' 
	AND driver='$driver_code'");
		
	if(!$update_opening_bal){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}

	$update_closing_bal = Qry($conn,"UPDATE dairy.opening_closing SET closing=closing+'$pay' WHERE id>='$row_opn_cls[id]' 
	AND driver='$driver_code'");
		
	if(!$update_closing_bal){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
}
else if($hisab_type=="3")
{
	if($pay<=0)
	{
		$flag = false;
		errorLog("Invalid payment amount: $pay. Trip_no: $trip_no.",$conn,$page_name,__LINE__);
	}
	
	$cfwd = $closing_balance_db+$pay;
	
	/// PAY CODE Starts here !!
	
	$insert_paid = Qry($conn,"INSERT INTO dairy.driver_book(driver_code,tno,trip_no,desct,credit,balance,date,branch,narration,timestamp) VALUES 
	('$driver_code','$tno','$trip_no','HISAB-PAID','$pay','$cfwd','$hisab_date','$hisab_branch','HISAB_MODIFIED','$hisab_timestamp')");
	
	if(!$insert_paid){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
	
	$hisab_pay_id = getInsertID($conn);
	
	/// Carry - fwd CODE Starts here !!
	
	$insert_carry_fwd = Qry($conn,"INSERT INTO dairy.driver_book(driver_code,tno,trip_no,desct,balance,date,branch,narration,timestamp) VALUES 
	('$driver_code','$tno','$trip_no','HISAB-CARRY-FWD','$cfwd','$hisab_date','$hisab_branch','HISAB_MODIFIED','$hisab_timestamp')");
	
	if(!$insert_carry_fwd){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
	
	$hisab_cfwd_id = getInsertID($conn);
	
	$last_row_insert_id = $hisab_cfwd_id;
	
	$update_driver_balance = Qry($conn,"UPDATE dairy.driver_up SET amount_hold=amount_hold-'$cfwd' WHERE code='$driver_code' ORDER BY id DESC LIMIT 1");
	
	if(!$update_driver_balance){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
	
	$update_balance_rows = Qry($conn,"UPDATE dairy.driver_book_temp2 SET balance=balance-'$cfwd' WHERE driver_code='$driver_code'");
		
	if(!$update_balance_rows){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
	
	$get_opening_closing_id = Qry($conn,"SELECT id FROM dairy.opening_closing WHERE trip_no='$trip_no'");
	
	if(!$get_opening_closing_id){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}

	if(numRows($get_opening_closing_id)==0)
	{
		$flag = false;
		errorLog("Opening closing not found. Trip-no: $trip_no.",$conn,$page_name,__LINE__);
	}

	$row_opn_cls = fetchArray($get_opening_closing_id);

	$update_opening_bal = Qry($conn,"UPDATE dairy.opening_closing SET opening=opening-'$cfwd' WHERE id>'$row_opn_cls[id]' 
	AND driver='$driver_code'");
		
	if(!$update_opening_bal){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}

	$update_closing_bal = Qry($conn,"UPDATE dairy.opening_closing SET closing=closing-'$cfwd' WHERE id>='$row_opn_cls[id]' 
	AND driver='$driver_code'");
		
	if(!$update_closing_bal){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
}
else if($hisab_type=="4")
{
	if($crho<=0)
	{
		$flag = false;
		errorLog("Invalid payment amount: $crho. Trip_no: $trip_no.",$conn,$page_name,__LINE__);
	}
	
	$insert_paid = Qry($conn,"INSERT INTO dairy.driver_book(driver_code,tno,trip_no,desct,debit,balance,date,branch,narration,timestamp) VALUES 
	('$driver_code','$tno','$trip_no','CREDIT-HO','$crho','0','$hisab_date','$hisab_branch','HISAB_MODIFIED','$hisab_timestamp')");
	
	if(!$insert_paid){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
	
	$hisab_crho_id = getInsertID($conn);
	
	$last_row_insert_id = $hisab_crho_id;
	
	$update_driver_balance = Qry($conn,"UPDATE dairy.driver_up SET amount_hold=amount_hold-'$crho' WHERE code='$driver_code' ORDER BY id DESC LIMIT 1");
	
	if(!$update_driver_balance){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
	
	$update_balance_rows = Qry($conn,"UPDATE dairy.driver_book_temp2 SET balance=balance-'$crho' WHERE driver_code='$driver_code'");
		
	if(!$update_balance_rows){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
	
	$get_opening_closing_id = Qry($conn,"SELECT id FROM dairy.opening_closing WHERE trip_no='$trip_no'");
	
	if(!$get_opening_closing_id){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}

	if(numRows($get_opening_closing_id)==0)
	{
		$flag = false;
		errorLog("Opening closing not found. Trip-no: $trip_no.",$conn,$page_name,__LINE__);
	}

	$row_opn_cls = fetchArray($get_opening_closing_id);

	$update_opening_bal = Qry($conn,"UPDATE dairy.opening_closing SET opening=opening-'$crho' WHERE id>'$row_opn_cls[id]' 
	AND driver='$driver_code'");
		
	if(!$update_opening_bal){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}

	$update_closing_bal = Qry($conn,"UPDATE dairy.opening_closing SET closing=closing-'$crho' WHERE id>='$row_opn_cls[id]' 
	AND driver='$driver_code'");
		
	if(!$update_closing_bal){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
	
	if($company=='RRPL'){
		$col_credit = "balance";
		$credit_colname = "credit";
	}
	else{
		$col_credit = "balance2";
		$credit_colname = "credit2";
	}
	
	$select_branch_bal = Qry($conn,"SELECT `$col_credit` as balance FROM user WHERE username='$hisab_branch'");

	if(!$select_branch_bal){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}

	$row_balance = fetchArray($select_branch_bal);

	$new_balance = $crho + $row_balance['balance'];

	$update_cashbook = Qry($conn,"INSERT INTO cashbook (user,date,vou_date,comp,vou_type,desct,`$credit_colname`,`$col_credit`,timestamp) 
	VALUES ('$hisab_branch','$date','$hisab_date','$company','CREDIT-HO','Driver Down Credit, Trip_No: $trip_no, TruckNo: $tno, Driver: $driver_name',
	'$crho','$new_balance','$hisab_timestamp')");

	$cash_credit_id = getInsertID($conn);

	if(!$update_cashbook){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}	

	$update_balance = Qry($conn,"UPDATE user SET `$col_credit`='$new_balance' WHERE username='$hisab_branch'");

	if(!$update_balance){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}

	$insret_credit_entry = Qry($conn,"INSERT INTO credit(credit_by,cash_id,section,tno,branch,company,amount,narr,date,timestamp) 
	VALUES ('CASH','$cash_credit_id','CREDIT-HO','$tno','$hisab_branch','$company','$crho','Driver Down Credit, Trip_No: $trip_no, TruckNo: $tno, 
	Driver: $driver_name','$hisab_date','$timestamp')");

	if(!$insret_credit_entry){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
}

$copy_record_db = Qry($conn,"INSERT INTO dairy.driver_book(driver_code,tno,trip_id,trip_no,trans_id,desct,credit,debit,balance,date,
branch,narration,timestamp) SELECT driver_code,tno,trip_id,trip_no,trans_id,desct,credit,debit,balance,date,branch,narration,timestamp 
FROM dairy.driver_book_temp2");
	
if(!$copy_record_db){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}
	
$delete_temp_record = Qry($conn,"DELETE FROM dairy.driver_book_temp2");
	
if(!$delete_temp_record){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

$get_updated_rows_id = Qry($conn,"INSERT INTO dairy._log_hisab_updated_row_ids(trip_no,da_id,sal_id,cfwd_id,pay_id,crho_id,tel_id,
ag_id,timestamp) SELECT b.trip_no, 
(SELECT id FROM dairy.driver_book WHERE trip_no=b.trip_no AND desct like 'DA%'),
(SELECT id FROM dairy.driver_book WHERE trip_no=b.trip_no AND desct like 'SALARY%'),
(SELECT id FROM dairy.driver_book WHERE trip_no=b.trip_no AND desct='HISAB-CARRY-FWD'), 
(SELECT id FROM dairy.driver_book WHERE trip_no=b.trip_no AND desct='HISAB-PAID'), 
(SELECT id FROM dairy.driver_book WHERE trip_no=b.trip_no AND desct='CREDIT-HO'), 
(SELECT id FROM dairy.driver_book WHERE trip_no=b.trip_no AND desct='EXP-TELEPHONE'), 
(SELECT id FROM dairy.driver_book WHERE trip_no=b.trip_no AND desct='EXP-AIR_AND_GREASE'),'$timestamp' 
FROM dairy.driver_book as b 
WHERE b.trip_no in(SELECT DISTINCT trip_no FROM dairy.driver_book WHERE id>'$last_row_insert_id') GROUP by b.trip_no");
	
if(!$get_updated_rows_id){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}
	
$update_new_rows_id = Qry($conn,"UPDATE dairy.log_hisab t1 
INNER JOIN dairy._log_hisab_updated_row_ids t2 
ON t1.trip_no = t2.trip_no
SET 
t1.row_cfwd = t2.cfwd_id,
t1.row_pay = t2.pay_id,
t1.row_credit_ho = t2.crho_id,
t1.row_da = t2.da_id,
t1.row_salary = t2.sal_id,
t1.row_tel_exp = t2.tel_id,
t1.row_ag_exp = t2.ag_id 
WHERE t1.trip_no = t2.trip_no");
	
if(!$update_new_rows_id){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}
	
$delete_rows_id_data = Qry($conn,"DELETE FROM dairy._log_hisab_updated_row_ids");
	
if(!$delete_rows_id_data){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}	

$cash_vou_id_new = 0;
$rtgs_vou_id_new = 0;

$branch = $hisab_branch;
$date = date("Y-m-d");

if($cash_pay>0)
{
	$check_bal = Qry($conn,"SELECT balance,balance2 FROM user WHERE username='$branch'");

	if(!$check_bal){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}

	if(numRows($check_bal)==0)
	{
		$flag = false;
		errorLog("Branch balance not found.",$conn,$page_name,__LINE__);
	}

	$row_bal = fetchArray($check_bal);

	$rrpl_cash = $row_bal['balance'];
	$rr_cash = $row_bal['balance2'];

	if($company=='RRPL' && $rrpl_cash>=$cash_pay)
	{
		$newbal_cashbook = $rrpl_cash - $cash_pay;
		$debit = 'debit';			
		$balance_cashbook = 'balance';
		
	}
	else if($company=='RAMAN_ROADWAYS' && $rr_cash>=$cash_pay)
	{
		$newbal_cashbook = $rr_cash - $cash_pay;
		$debit = 'debit2';			
		$balance_cashbook = 'balance2';
	}
	else
	{
		$flag = false;
		errorLog("Company not found. or INSUFFICIENT Balance.",$conn,$page_name,__LINE__);
	}

	$get_vou_id = GetVouId("T",$conn,"mk_tdv","tdvid",$branch);
	
	if(!$get_vou_id || $get_vou_id=="0" || $get_vou_id=="")
	{
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
	
	$tids = $get_vou_id;
	
	$update_cashbook=Qry($conn,"INSERT INTO cashbook(user,user_code,date,vou_date,comp,vou_no,vou_type,desct,`$debit`,`$balance_cashbook`,
	timestamp) VALUES ('$branch','ADMIN','$date','$hisab_date','$company','$tids','Truck_Voucher','HISAB-PAYMENT : $nrr',
	'$cash_pay','$newbal_cashbook','$hisab_timestamp')");

	if(!$update_cashbook){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}	

	$query_update = Qry($conn,"update user set `$balance_cashbook`='$newbal_cashbook' where username='$branch'");

	if(!$query_update){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}	

	if(AffectedRows($conn)==0)
	{
		$flag = false;
		errorLog("ZERO ROWS AFFECTED BALANCE UPDATE.",$conn,$page_name,__LINE__);
	}

	$insert_vou = Qry($conn,"INSERT INTO mk_tdv(user,branch_user,company,tdvid,date,newdate,truckno,dname,amt,mode,hisab_vou,dest,timestamp) 
	VALUES ('$branch','ADMIN','$company','$tids','$date','$hisab_date','$tno','$driver_name','$cash_pay','CASH','1','HISAB-PAYMENT : $nrr',
	'$hisab_timestamp')");	

	if(!$insert_vou){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
	
	$cash_vou_id_new = getInsertID($conn);
}

if($rtgs_pay>0)
{
	if($company=='RRPL'){
		$debit='debit';		
	}
	else if($company=='RAMAN_ROADWAYS'){
		$debit='debit2';
	}

	$get_vou_id = GetVouId("T",$conn,"mk_tdv","tdvid",$branch);
	
	if(!$get_vou_id || $get_vou_id=="0" || $get_vou_id=="")
	{
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
	
	$tids = $get_vou_id;
	
	$insert_vou = Qry($conn,"INSERT INTO mk_tdv(user,branch_user,company,tdvid,date,newdate,truckno,dname,amt,mode,hisab_vou,ac_name,ac_no,
	bank,ifsc,pan,dest,timestamp) VALUES ('$branch','ADMIN','$company','$tids','$date','$hisab_date','$tno','$driver_name',
	'$rtgs_pay','NEFT','1','$acname','$acno','$bank_name','$ifsc','','HISAB-PAYMENT : $nrr','$hisab_timestamp')");	

	if(!$insert_vou){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}	

	$rtgs_vou_id_new = getInsertID($conn);
	
	$update_passbook=Qry($conn,"INSERT INTO passbook(user,vou_no,date,vou_date,comp,vou_type,desct,`$debit`,timestamp) 
	VALUES ('$branch','$tids','$date','$hisab_date','$company','Truck_Voucher','HISAB-PAYMENT : $nrr','$rtgs_pay','$hisab_timestamp')");

	if(!$update_passbook){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}	

	if($company=='RRPL')
	{
		$get_Crn = GetCRN("RRPL-T",$conn);
		if(!$get_Crn || $get_Crn=="0" || $get_Crn=="")
		{
			$flag = false;
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		}
		
		$crnnew = $get_Crn;		
	}
	else if($company=='RAMAN_ROADWAYS')
	{
		$get_Crn = GetCRN("RR-T",$conn);
		if(!$get_Crn || $get_Crn=="0" || $get_Crn=="")
		{
			$flag = false;
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		}
		
		$crnnew = $get_Crn;
	}

	$qry_rtgs=Qry($conn,"INSERT INTO rtgs_fm(fno,branch,com,totalamt,amount,acname,acno,bank_name,ifsc,pay_date,fm_date,tno,type,
	crn,timestamp) VALUES ('$tids','$branch','$company','$rtgs_pay','$rtgs_pay','$acname','$acno','$bank_name','$ifsc','$date',
	'$hisab_date','$tno','TRUCK_ADVANCE','$crnnew','$hisab_timestamp')");

	if(!$qry_rtgs){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
}

$update_log_hisab = Qry($conn,"UPDATE dairy.log_hisab SET hisab_type='$hisab_type',row_cfwd='$hisab_cfwd_id',row_pay='$hisab_pay_id',
row_credit_ho='$hisab_crho_id',cash_vou='$cash_vou_id_new',rtgs_vou='$rtgs_vou_id_new',credit_cash='$cash_credit_id' WHERE trip_no='$trip_no'");

if(!$update_log_hisab){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}
 
$delete_script = Qry($conn,"DELETE FROM dairy.running_scripts WHERE file_name='Hisab_Manage_Admin'");

if(!$delete_script){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

if($flag)
{
	MySQLCommit($conn);
	closeConnection($conn);
	
	AlertRightCornerSuccessFadeFast("Update Success !");
	echo "<script>
		$('#hisab_edit_modal_cls_btn')[0].click();
		$('#search_add_btn')[0].click();
	</script>";
}
else
{
	MySQLRollBack($conn);
	closeConnection($conn);
	AlertRightCornerError("Error While Processing Request !");
	echo "<script>$('#button_form_hisab').attr('disabled',false);</script>";
	exit();
}
?>