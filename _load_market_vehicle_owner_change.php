<?php
require_once("connect.php");
?>

			<table id="example" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th>#</th>
						<th>TruckNo</th>
						<th>Current Details</th>
                        <th>Current Docs</th>
                        <th>New Details</th>
                        <th>New Docs</th>
                        <th>Branch</th>
                        <th>Timestamp</th>
                        <th>Approve</th>
                        <th>Reject</th>
                      </tr>
                    </thead>
                    <tbody>
	<?php
	$get_approvals = Qry($conn,"SELECT id,tno,oid,old_name,old_mobile,old_pan,old_addr,rc_copy_old,rc_copy_rear_old,pan_copy_old,rc_copy_new,
	rc_copy_rear_new,pan_copy_new,ac_details,new_name,new_mobile,new_pan,new_addr,branch,timestamp FROM owner_change_req WHERE done=0");
	
	if(!$get_approvals){
		AlertErrorTopRight("Error while processing request !");
		echo getMySQLError($conn);
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		exit();
	}
	
	if(numRows($get_approvals)==0)
	{
		echo "<tr>
			<td colspan='10'>No record found !</td>
			<td style='display: none'></td>
			<td style='display: none'></td>
			<td style='display: none'></td>
			<td style='display: none'></td>
			<td style='display: none'></td>
			<td style='display: none'></td>
			<td style='display: none'></td>
			<td style='display: none'></td>
			<td style='display: none'></td>
			<td style='display: none'></td>
		</tr>";
	}
	else
	{
		$i=1;
		while($row = fetchArray($get_approvals))
		{
			$timestamp = date("d-m-y h:i A",strtotime($row['timestamp']));
			
			$ext_rc = pathinfo($row['rc_copy_new'], PATHINFO_EXTENSION);
			$ext_rc_r = pathinfo($row['rc_copy_rear_new'], PATHINFO_EXTENSION);
			$ext_pan = pathinfo($row['pan_copy_new'], PATHINFO_EXTENSION);
											
			$ext_rc_o = pathinfo($row['rc_copy_old'], PATHINFO_EXTENSION);
			$ext_rc_r_o = pathinfo($row['rc_copy_rear_old'], PATHINFO_EXTENSION);
			$ext_pan_o = pathinfo($row['pan_copy_old'], PATHINFO_EXTENSION);
		
			echo "<tr>
				<td>$i</td>
				<td>$row[tno]</td>
				<td>Name: $row[old_name]<br>Mobile: $row[old_mobile]<br>PAN: $row[old_pan]</td>
				<td>
				<lable>
					<button type='button' onclick=ViewImg('$row[id]','OLD','rc','$row[old_pan]','$ext_rc_o') class='btn btn-warning btn-xs'>Rc-front</button>
					&nbsp;
					<button type='button' onclick=ViewImg('$row[id]','OLD','rc_r','$row[old_pan]','$ext_rc_r_o') class='btn btn-warning btn-xs'>Rc-rear</button>
					&nbsp;
					<button type='button' onclick=ViewImg('$row[id]','OLD','pan','$row[old_pan]','$ext_pan_o') class='btn btn-warning btn-xs'>Pan-copy</button>
				</td>	
				<td>Name: $row[new_name]<br>Mobile: $row[new_mobile]<br>PAN: $row[new_pan]</td>
				<td>
					<button type='button' onclick=ViewImg('$row[id]','NEW','https://rrpl.online/b5aY6EZzK52NA8F/$row[rc_copy_new]','$row[new_pan]','$ext_rc') class='btn btn-warning btn-xs'>Rc-front</button>
					&nbsp; &nbsp;
					<button type='button' onclick=ViewImg('$row[id]','NEW','https://rrpl.online/b5aY6EZzK52NA8F/$row[rc_copy_rear_new]','$row[new_pan]','$ext_rc_r') class='btn btn-warning btn-xs'>Rc-rear</button>
					&nbsp; &nbsp;
					<button type='button' onclick=ViewImg('$row[id]','NEW','https://rrpl.online/b5aY6EZzK52NA8F/$row[pan_copy_new]','$row[new_pan]','$ext_pan') class='btn btn-warning btn-xs'>Pan-copy</button>
				</td>	
				<td>$row[branch]</td>
				<td>$row[timestamp]</td>
				<td>
				
				<input type='hidden' id='rc_old$row[id]' value='$row[rc_copy_old]'>	
				<input type='hidden' id='rc_old_rear$row[id]' value='$row[rc_copy_rear_old]'>	
				<input type='hidden' id='pan_old$row[id]' value='$row[pan_copy_old]'>	
				<input type='hidden' id='rc_new$row[id]' value='$row[rc_copy_new]'>	
				<input type='hidden' id='rc_new_rear$row[id]' value='$row[rc_copy_rear_new]'>	
				<input type='hidden' id='pan_new$row[id]' value='$row[pan_copy_new]'>	
				<input type='hidden' id='new_name1$row[id]' value='$row[new_name]'>	
				<input type='hidden' id='old_name1$row[id]' value='$row[old_name]'>
			
			<button class='btn btn-xs btn_allow btn-success' type='button' id='approve_btn_$row[id]' onclick=Approve('$row[id]')><i class='fa fa-thumbs-o-up' aria-hidden='true'></i> Approve</button>
			</td>
			<td><button type='button' id='reject_btn_$row[id]' class='btn_allow btn btn-xs btn-danger' onclick='Reject($row[id])'><i class='fa fa-thumbs-o-down' aria-hidden='true'></i> Reject</button></td>
		</tr>";
		$i++;	
		}
	}
	?>	
                    </tbody>
                  </table>

<script>				  
$("#loadicon").fadeOut('slow');
</script>	

<?php
$chk_update = Qry($conn,"SELECT id FROM _access_control WHERE username='$_SESSION[ediary_fix_admin]' AND func_id=(SELECT id FROM 
_access_control_func_list WHERE session_role='8' AND func_name='Owner_Change_Req') AND u_update='1'");
			  
if(numRows($chk_update)>0)
{
	echo "<script>
		$('.btn_allow').attr('disabled',false);
	</script>";
}
?>	