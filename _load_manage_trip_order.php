<?php
require_once("connect.php");

$tno = escapeString($conn,$_POST['veh_no']);

?>

			<table id="example" class="table table-bordered table-striped" style="font-size:13px">
                    <thead>
                      <tr>
                        <th>#</th>
                        <th>#View</th>
						<th>Branch & user</th>
                        <th>From</th>
                        <th>To</th>
                        <th>Weight</th>
                        <th>LR_No</th>
                        <th>Trip_Date</th>
                        <th>End_Date</th>
                        <th>#Change</th>
                      </tr>
                    </thead>
                    <tbody>
	<?php
	$get_approvals = Qry($conn,"SELECT t.id,t.branch,t.from_station,t.to_station,t.act_wt,t.charge_wt,t.lr_type,t.lrno,t.date as trip_date,t.end_date 
	AS trip_end_date,e.name as trip_user 
	FROM dairy.trip AS t 
	LEFT OUTER JOIN emp_attendance AS e ON e.code = t.branch_user 
	WHERE t.tno = '$tno'");
	
	if(!$get_approvals){
		AlertErrorTopRight("Error while processing request !");
		echo getMySQLError($conn);
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		exit();
	}
	
	if(numRows($get_approvals)==0)
	{
		echo "<tr>
			<td colspan='10'>No record found !</td>
			<td style='display: none'></td>
			<td style='display: none'></td>
			<td style='display: none'></td>
			<td style='display: none'></td>
			<td style='display: none'></td>
			<td style='display: none'></td>
			<td style='display: none'></td>
			<td style='display: none'></td>
			<td style='display: none'></td>
			<td style='display: none'></td>
		</tr>";
	}
	else
	{
		$i=1;
		while($row = fetchArray($get_approvals))
		{
			// $timestamp = date("d-m-y h:i A",strtotime($row['timestamp']));
			
			$trip_date = date("d-m-y",strtotime($row['trip_date']));
			$end_date = date("d-m-y",strtotime($row['trip_end_date']));
			
			echo "<tr>
				<td>$i</td>
				<td><button type='button' onclick='ViewTrip($row[id])' class='btn btn-xs btn-primary'><i class='fa fa-street-view' aria-hidden='true'></i> View</button></td>
				<td>$row[branch]<br>($row[trip_user])</td>
				<td>$row[from_station]</td>
				<td>$row[to_station]</td>
				<td>Actual: $row[act_wt]<br>Charge: $row[charge_wt]</td>
				<td>$row[lr_type]<br>$row[lrno]</td>
				<td>$trip_date</td>
				<td>$end_date</td>
	<td>
		<label style='color:red;font-size:10px !important;'>Place after :</label>
		<br />
			<select style='border-radius:3px;font-size:11px;height:20px !important;width:100px !important' id='trip_list_$row[id]'>
				<option value=''>--select trip--</option>";
	
	$fetchQry = Qry($conn,"SELECT id,from_station,to_station,date(date) as trip_date,lr_type FROM dairy.trip WHERE tno='$tno' AND id!='$row[id]' 
	ORDER BY id ASC");
				
	if(numRows($fetchQry)>0)
	{
		while($rowTrip = fetchArray($fetchQry))
		{
			echo "<option value='$rowTrip[id]'>after: $rowTrip[from_station] - $rowTrip[to_station]. LR: $rowTrip[lr_type]. Date: $rowTrip[trip_date]</option>";
		}
	}
				
	echo "</select>
			<button id='btn_confirm_$row[id]' class='btn_confirm btn btn-default btn-xs' type='button' onclick=ChangeOrder('$row[id]')>
			<i class='fa fa-edit' aria-hidden='true'></i> Change</button>
	</td>
			</tr>";
		$i++;	
		}
	}
	?>	
                    </tbody>
                  </table>

<script>				  
$("#loadicon").fadeOut('slow');
</script>		

<?php
$chk_update = Qry($conn,"SELECT id FROM _access_control WHERE username='$_SESSION[ediary_fix_admin]' AND func_id=(SELECT id FROM 
_access_control_func_list WHERE session_role='8' AND func_name='Trip_Order') AND u_update='1'");
			  
if(numRows($chk_update)>0)
{
	echo "<script>
		$('.btn_confirm').attr('disabled',false);
	</script>";	
}
else
{
	echo "<script>
		$('.btn_confirm').attr('disabled',true);
		$('.btn_confirm').attr('onclick','');
	</script>";	
}
?>		  