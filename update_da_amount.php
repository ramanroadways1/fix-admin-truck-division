<?php
require_once("connect.php");

$trip_no = escapeString($conn,($_POST['trip_no']));
$driver_code = escapeString($conn,($_POST['driver_code']));
$tno = escapeString($conn,($_POST['tno']));
$da_per_day = escapeString($conn,($_POST['da_per_day_db']));
$hisab_id = escapeString($conn,($_POST['hisab_id']));
$timestamp = date("Y-m-d H:i:s");

if(empty($hisab_id)){
	AlertRightCornerError("Hisab ID not found !");
	exit();
}

if(empty($trip_no)){
	AlertRightCornerError("Trip number not found !");
	exit();
}

if(empty($driver_code)){
	AlertRightCornerError("Driver not found !");
	exit();
}

if(empty($tno)){
	AlertRightCornerError("Vehicle number not found !");
	exit();
}

$verify_hisab = Qry($conn,"SELECT tno,trip_no,hisab_type,row_da,driver FROM dairy.log_hisab WHERE id='$hisab_id'");

if(!$verify_hisab){
	AlertRightCornerError("Error while processing request !");
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	exit();
}
	
if(numRows($verify_hisab)==0)
{
	AlertRightCornerError("Hisab not found !");
	exit();
}

$row_hisab = fetchArray($verify_hisab);

if($row_hisab['tno'] != $tno)
{
	AlertRightCornerError("Vehicle not verified !");
	echo "<script>$('#button_da_form').attr('disabled',false);</script>";
	exit();
}

if($row_hisab['driver'] != $driver_code)
{
	AlertRightCornerError("Vehicle not verified !");
	echo "<script>$('#button_da_form').attr('disabled',false);</script>";
	exit();
}

if($row_hisab['trip_no'] != $trip_no)
{
	AlertRightCornerError("Vehicle not verified !");
	echo "<script>$('#button_da_form').attr('disabled',false);</script>";
	exit();
}

$hisab_type = $row_hisab['hisab_type'];

if($hisab_type!="1" AND $hisab_type!="2" AND $hisab_type!="3" AND $hisab_type!="4")
{
	AlertRightCornerError("Invalid hisab type !");
	echo "<script>$('#button_da_form').attr('disabled',false);</script>";
	exit();
}

$first_record_da="NO";

if($da_per_day=="0" || $da_per_day=='')
{
	if($da_from_date!='')
	{
		AlertRightCornerError("Something went wrong !");
		exit();
	}

	$get_da_min_date = Qry($conn,"SELECT id,to_date,narration FROM dairy.da_book WHERE driver_code='$driver_code' ORDER BY id DESC LIMIT 1");

	if(!$get_da_min_date){
		AlertRightCornerError("Error while processing request !");
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		exit();
	}
	
	if(numRows($get_da_min_date)==0)
	{
		AlertRightCornerError("DA record not found !");
		exit();
	}
	else
	{
		$row_min_da_date = fetchArray($get_da_min_date);
	}
	
	if($row_min_da_date['narration']=='ON_LEAVE_UP')
	{
		$da_min_date = $row_min_da_date['to_date'];
	}
	else
	{
		$da_min_date = date('Y-m-d', strtotime('+1 day',strtotime($row_min_da_date['to_date'])));
	}
}
else
{
	$get_da_min_date = Qry($conn,"SELECT id,to_date,narration FROM dairy.da_book WHERE id<(SELECT id FROM dairy.da_book WHERE trip_no='$trip_no') 
	AND driver_code='$driver_code' ORDER BY id DESC LIMIT 1");

	if(!$get_da_min_date){
		AlertRightCornerError("Error while processing request !");
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		exit();
	}
	
	if(numRows($get_da_min_date)==0)
	{
		$get_da_min_date1 = Qry($conn,"SELECT from_date,narration FROM dairy.da_book WHERE trip_no='$trip_no' AND driver_code='$driver_code'");
 
		if(!$get_da_min_date1){
			AlertRightCornerError("Error while processing request !");
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			exit();
		}
		
		$row_min_da_date = fetchArray($get_da_min_date1);
		$da_min_date = $row_min_da_date['from_date'];
		
		$first_record_da="YES";
	}
	else
	{
		$row_min_da_date = fetchArray($get_da_min_date);
		
		if($row_min_da_date['narration']=='ON_LEAVE_UP')
		{
			$da_min_date = $row_min_da_date['to_date'];
		}
		else
		{
			$da_min_date = date('Y-m-d', strtotime('+1 day',strtotime($row_min_da_date['to_date'])));
		}
	}
}

$get_max_trip_end_date = Qry($conn,"SELECT date(end_date) as end_date FROM dairy.trip_final WHERE trip_no='$trip_no' ORDER BY id DESC LIMIT 1");

if(!$get_max_trip_end_date){
	AlertRightCornerError("Error while processing request !");
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	exit();
}
	
if(numRows($get_max_trip_end_date)==0)
{
	AlertRightCornerError("Trip not found !");
	exit();
}

$row_trip_end_date = fetchArray($get_max_trip_end_date);

if($first_record_da == "YES")
{
	$get_da_date = Qry($conn,"SELECT to_date FROM dairy.da_book WHERE trip_no='$trip_no' AND driver_code='$driver_code'");
}
else
{
	$get_da_date = Qry($conn,"SELECT to_date FROM dairy.da_book WHERE id>'$row_min_da_date[id]' AND driver_code='$driver_code' ORDER BY id ASC LIMIT 1");
}

if(!$get_da_date){
	AlertRightCornerError("Error while processing request !");
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	exit();
}
	
if(numRows($get_da_date)==0)
{
	$da_max_date = $row_trip_end_date['end_date'];
}
else
{
	$row_da_date = fetchArray($get_da_date);
	
	if(strtotime($row_da_date['to_date']) > strtotime($row_trip_end_date['end_date']))
	{
		$da_max_date = date('Y-m-d', strtotime('-1 day',strtotime($row_da_date['to_date'])));
	}
	else
	{
		$da_max_date = $row_trip_end_date['end_date'];
	}
}

$da_from_date = escapeString($conn,($_POST['da_from_date']));
$da_to_date = escapeString($conn,($_POST['da_to_date']));
$da_per_day1 = escapeString($conn,($_POST['da_per_day']));
$no_of_days = escapeString($conn,($_POST['da_date_duration']));
$da_amount = escapeString($conn,($_POST['da_amount']));

$days_diff = round((strtotime($da_to_date)-strtotime($da_from_date)) / (60 * 60 * 24))+1;

if($no_of_days != $days_diff)
{
	AlertRightCornerError("Days count not verified !");
	echo "<script>$('#button_da_form').attr('disabled',false);</script>";
	exit();
}

if($da_amount != ($da_per_day1 * $no_of_days))
{
	AlertRightCornerError("Days amount not verified !");
	echo "<script>$('#button_da_form').attr('disabled',false);</script>";
	exit();
}

$get_da_row = Qry($conn,"SELECT id,driver_code,tno,debit,desct,balance FROM dairy.driver_book WHERE trip_no='$trip_no' AND desct like 'DA-%' 
ORDER BY id DESC LIMIT 1");

if(!$get_da_row){
	AlertRightCornerError("Error while processing request !");
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	exit();
}
	
if(numRows($get_da_row)==0 AND $da_per_day!='' AND $da_per_day=0)
{
	AlertRightCornerError("DA record not found !");
	echo "<script>$('#button_da_form').attr('disabled',false);</script>";
	exit();
}

$old_desct="";

if(numRows($get_da_row)>0)
{
	$row_book = fetchArray($get_da_row);
	
	$da_driver_book_id = $row_book['id'];
	
	$old_desct = "$row_book[desct]";
	
	$da_from_date_db = substr(str_replace('DA-','',str_replace(' ','',trim($row_book['desct']))),0,10);
	$da_to_date_db = str_replace('to','',str_replace('DA-','',str_replace(' ','',trim($row_book['desct']))));
	$da_to_date_db = str_replace($da_from_date_db,'',$da_to_date_db);
	
	if($da_from_date_db == $da_from_date AND $da_to_date_db == $da_to_date AND $row_book['debit'] == $da_amount)
	{
		AlertRightCornerError("Nothing to update !");
		echo "<script>$('#button_da_form').attr('disabled',false);</script>";
		exit();
	}
	
	if($row_book['driver_code'] != $driver_code)
	{
		AlertRightCornerError("Driver not verified !");
		echo "<script>$('#button_da_form').attr('disabled',false);</script>";
		exit();
	}
	
	if($row_book['tno'] != $tno)
	{
		AlertRightCornerError("Vehicle not verified !");
		echo "<script>$('#button_da_form').attr('disabled',false);</script>";
		exit();
	}
	
	StartCommit($conn);
	$flag = true;
	
	$insert_script = Qry($conn,"INSERT INTO dairy.running_scripts(file_name,timestamp) VALUES ('Hisab_Manage_Admin','$timestamp')");
	
	if(!$insert_script){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
	
	$new_de_desc = "DA- $da_from_date to $da_to_date";
	
	$update_new_da = Qry($conn,"UPDATE dairy.driver_book SET desct='$new_de_desc',debit=debit-'$da_amount',balance=balance-'$da_amount' 
	WHERE id='$da_driver_book_id'");
	
	if(!$update_new_da){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
	
	$update_db_amount = Qry($conn,"UPDATE dairy.driver_book SET balance=balance-'$da_amount' WHERE id>'$da_driver_book_id' AND 
	driver_code='$driver_code'");
	
	if(!$update_db_amount){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
	
	$update_balance = Qry($conn,"UPDATE dairy.driver_up SET amount_hold=amount_hold-'$da_amount' WHERE code='$driver_code' ORDER BY id DESC LIMIT 1");
	
	if(!$update_balance){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
	
	$update_da_book = Qry($conn,"UPDATE dairy.da_book SET from_date='$da_from_date',to_date='$da_to_date',amount='$da_amount' WHERE trip_no='$trip_no'");
	
	if(!$update_da_book){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
}
else
{
	if($row_hisab['row_da']!=0)
	{
		AlertRightCornerError("Invalid DA record !");
		echo "<script>$('#button_da_form').attr('disabled',false);</script>";
		exit();
	}
	
	if($hisab_type=="1")
	{
		$get_hisab_row_id = Qry($conn,"SELECT id,branch,balance,date,timestamp FROM dairy.driver_book WHERE trip_no='$trip_no' AND desct='HISAB-CARRY-FWD'");
		$hisab_type1 = "HISAB-CARRY-FWD";
	}
	else if($hisab_type=="2")
	{
		$get_hisab_row_id = Qry($conn,"SELECT id,branch,balance,date,timestamp FROM dairy.driver_book WHERE trip_no='$trip_no' AND desct='HISAB-PAID'");
		$hisab_type1 = "HISAB-PAID";
	}
	else if($hisab_type=="3")
	{
		$get_hisab_row_id = Qry($conn,"SELECT id,branch,balance,date,timestamp FROM dairy.driver_book WHERE trip_no='$trip_no' AND desct='HISAB-PAID'");
		$hisab_type1 = "HISAB-PAID";
	}
	else if($hisab_type=="4")
	{
		$get_hisab_row_id = Qry($conn,"SELECT id,branch,balance,date,timestamp FROM dairy.driver_book WHERE trip_no='$trip_no' AND desct='CREDIT-HO'");
		$hisab_type1 = "CREDIT-HO";
	}
	
	if(!$get_hisab_row_id){
		AlertRightCornerError("Error while processing request !");
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		echo "<script>$('#button_da_form').attr('disabled',false);</script>";
		exit();
	}
	
	if(numRows($get_hisab_row_id)==0)
	{
		errorLog("Hisab row not found ! Trip_no: $trip_no. Hisab-Type: $hisab_type - $hisab_type1.",$conn,$page_name,__LINE__);
		AlertRightCornerError("Closing record not found.");
		echo "<script>$('#button_da_form').attr('disabled',false);</script>";
		exit();
	}
	
	$row_get_hisab_row = fetchArray($get_hisab_row_id);
	
	$hisab_row_id = $row_get_hisab_row['id'];
	$hisab_date = $row_get_hisab_row['date'];
	$hisab_timestamp = $row_get_hisab_row['timestamp'];
	$hisab_branch = $row_get_hisab_row['branch'];
	
$get_closing_amount = Qry($conn,"SELECT id,balance FROM dairy.driver_book WHERE trip_no='$trip_no' AND id<'$hisab_row_id' ORDER BY id DESC LIMIT 1");

if(!$get_closing_amount){
	AlertRightCornerError("Error while processing request !");
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	exit();
}
	
if(numRows($get_closing_amount)==0)
{
	AlertRightCornerError("Closing balance not found !");
	echo "<script>$('#button_da_form').attr('disabled',false);</script>";
	exit();
}
	
$closing_row = fetchArray($get_closing_amount);
$closing_balance = $closing_row['balance'];

	if(empty($hisab_row_id))
	{
		AlertRightCornerError("Closing record not found.");
		errorLog("Hisab row id is empty ! Trip_no: $trip_no.",$conn,$page_name,__LINE__);
		echo "<script>$('#button_da_form').attr('disabled',false);</script>";
		exit();
	}
	
	StartCommit($conn);
	$flag = true;
	
	$insert_script = Qry($conn,"INSERT INTO dairy.running_scripts(file_name,timestamp) VALUES ('Hisab_Manage_Admin','$timestamp')");
	
	if(!$insert_script){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
	
	$copy_db_record = Qry($conn,"INSERT INTO dairy.driver_book_temp2(driver_code,tno,trip_id,trip_no,trans_id,desct,credit,debit,balance,date,
	branch,narration,timestamp) SELECT driver_code,tno,trip_id,trip_no,trans_id,desct,credit,debit,balance,date,branch,narration,timestamp 
	FROM dairy.driver_book WHERE id>='$hisab_row_id'");
	
	if(!$copy_db_record){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
	
	$delete_record = Qry($conn,"DELETE FROM dairy.driver_book WHERE id>='$hisab_row_id'");
	
	if(!$delete_record){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
	
	$new_de_desc = "DA- $da_from_date to $da_to_date";
	$new_balance = $closing_balance - $da_amount;
	
	$insert_da_record = Qry($conn,"INSERT INTO dairy.driver_book(driver_code,tno,trip_no,desct,debit,balance,date,branch,narration,timestamp) 
	VALUES ('$driver_code','$tno','$trip_no','$new_de_desc','$da_amount','$new_balance','$hisab_date','$hisab_branch','DA_ADDED_LATER',
	'$hisab_timestamp')");
	
	if(!$insert_da_record){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
	
	$da_insert_id = getInsertID($conn);
	
	$update_amount_diff = Qry($conn,"UPDATE dairy.driver_book_temp2 SET balance=balance-'$da_amount' WHERE driver_code='$driver_code'");
	
	if(!$update_amount_diff){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
	
	$copy_record_db = Qry($conn,"INSERT INTO dairy.driver_book(driver_code,tno,trip_id,trip_no,trans_id,desct,credit,debit,balance,date,
	branch,narration,timestamp) SELECT driver_code,tno,trip_id,trip_no,trans_id,desct,credit,debit,balance,date,branch,narration,timestamp 
	FROM dairy.driver_book_temp2");
	
	if(!$copy_record_db){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
	
	$delete_temp_record = Qry($conn,"DELETE FROM dairy.driver_book_temp2");
	
	if(!$delete_temp_record){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
	
	$get_updated_rows_id = Qry($conn,"INSERT INTO dairy._log_hisab_updated_row_ids(trip_no,da_id,sal_id,cfwd_id,pay_id,crho_id,tel_id,
	ag_id,timestamp) SELECT b.trip_no, 
	(SELECT id FROM dairy.driver_book WHERE trip_no=b.trip_no AND desct like 'DA%'),
	(SELECT id FROM dairy.driver_book WHERE trip_no=b.trip_no AND desct like 'SALARY%'),
	(SELECT id FROM dairy.driver_book WHERE trip_no=b.trip_no AND desct='HISAB-CARRY-FWD'), 
	(SELECT id FROM dairy.driver_book WHERE trip_no=b.trip_no AND desct='HISAB-PAID'), 
	(SELECT id FROM dairy.driver_book WHERE trip_no=b.trip_no AND desct='CREDIT-HO'), 
	(SELECT id FROM dairy.driver_book WHERE trip_no=b.trip_no AND desct='EXP-TELEPHONE'), 
	(SELECT id FROM dairy.driver_book WHERE trip_no=b.trip_no AND desct='EXP-AIR_AND_GREASE'),'$timestamp' 
	FROM dairy.driver_book as b 
	WHERE b.trip_no in(SELECT DISTINCT trip_no FROM dairy.driver_book WHERE id>'$da_insert_id') GROUP by b.trip_no");
	
	if(!$get_updated_rows_id){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
	
	$update_new_rows_id = Qry($conn,"UPDATE dairy.log_hisab t1 
    INNER JOIN dairy._log_hisab_updated_row_ids t2 
    ON t1.trip_no = t2.trip_no
	SET 
	t1.row_cfwd = t2.cfwd_id,
	t1.row_pay = t2.pay_id,
	t1.row_credit_ho = t2.crho_id,
	t1.row_da = t2.da_id,
	t1.row_salary = t2.sal_id,
	t1.row_tel_exp = t2.tel_id,
	t1.row_ag_exp = t2.ag_id 
	WHERE t1.trip_no = t2.trip_no");
	
	if(!$update_new_rows_id){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
	
	$delete_rows_id_data = Qry($conn,"DELETE FROM dairy._log_hisab_updated_row_ids");
	
	if(!$delete_rows_id_data){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
	
	$update_balance = Qry($conn,"UPDATE dairy.driver_up SET amount_hold=amount_hold-'$da_amount' WHERE code='$driver_code' ORDER BY id DESC LIMIT 1");
	
	if(!$update_balance){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
	
	$copy_da_record_temp = Qry($conn,"INSERT INTO dairy.da_book_temp(trip_no,driver_code,tno,from_date,to_date,amount,branch,date,on_duty_da,
	old_da_paid,narration,timestamp) SELECT trip_no,driver_code,tno,from_date,to_date,amount,branch,date,on_duty_da,old_da_paid,narration,timestamp 
	FROM dairy.da_book WHERE timestamp>='$hisab_timestamp'");
	
	if(!$copy_da_record_temp){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
	
	$delete_da_record_main = Qry($conn,"DELETE FROM dairy.da_book WHERE timestamp>='$hisab_timestamp'");
	
	if(!$delete_da_record_main){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
	
	$insert_da_row = Qry($conn,"INSERT INTO dairy.da_book(trip_no,driver_code,tno,from_date,to_date,amount,branch,date,narration,timestamp) 
	VALUES ('$trip_no','$driver_code','$tno','$da_from_date','$da_to_date','$da_amount','$hisab_branch','$hisab_date','DA_ADDED_LATER',
	'$hisab_timestamp')");
	
	if(!$insert_da_row){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
	
	$copy_da_rows_to_main_table = Qry($conn,"INSERT INTO dairy.da_book(trip_no,driver_code,tno,from_date,to_date,amount,branch,date,on_duty_da,
	old_da_paid,narration,timestamp) SELECT trip_no,driver_code,tno,from_date,to_date,amount,branch,date,on_duty_da,old_da_paid,narration,timestamp 
	FROM dairy.da_book_temp");
	
	if(!$copy_da_rows_to_main_table){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
	
	$delete_da_record_temp = Qry($conn,"DELETE FROM dairy.da_book_temp");
	
	if(!$delete_da_record_temp){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
	
	$update_da_row_id = Qry($conn,"UPDATE dairy.log_hisab SET row_da='$da_insert_id' WHERE id='$hisab_id'");
	
	if(!$update_da_row_id){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
}

$get_opening_closing_id = Qry($conn,"SELECT id FROM dairy.opening_closing WHERE trip_no='$trip_no'");
	
if(!$get_opening_closing_id){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

if(numRows($get_opening_closing_id)==0)
{
	$flag = false;
	errorLog("Opening closing not found. Trip-no: $trip_no.",$conn,$page_name,__LINE__);
}

$row_opn_cls = fetchArray($get_opening_closing_id);

$update_opening_bal = Qry($conn,"UPDATE dairy.opening_closing SET opening=opening-'$da_amount' WHERE id>'$row_opn_cls[id]' 
AND driver='$driver_code'");
	
if(!$update_opening_bal){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

$update_closing_bal = Qry($conn,"UPDATE dairy.opening_closing SET closing=closing-'$da_amount' WHERE id>='$row_opn_cls[id]' 
AND driver='$driver_code'");
	
if(!$update_closing_bal){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

$delete_running_file = Qry($conn,"DELETE FROM dairy.running_scripts WHERE file_name='Hisab_Manage_Admin'");
	
if(!$delete_running_file){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

$insert_log = Qry($conn,"INSERT INTO dairy.ediary_admin_hisab_log(code,trip_no,tno,action,old_desct,desct,timestamp) VALUES ('$driver_code',
'$trip_no','$tno','DA_MODIFIED','$old_desct','$new_de_desc','$timestamp')");
	
if(!$insert_log){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

if($flag)
{
	MySQLCommit($conn);
	closeConnection($conn);
	
	AlertRightCornerSuccessFadeFast("Update Success !");
	echo "<script>
		$('#modal_da_update_close_btn')[0].click();
		$('#search_add_btn')[0].click();
	</script>";
}
else
{
	MySQLRollBack($conn);
	closeConnection($conn);
	AlertRightCornerError("Error While Processing Request !");
	echo "<script>$('#button_da_form').attr('disabled',false);</script>";
	exit();
}
?>