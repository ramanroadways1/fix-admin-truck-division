<?php
require_once 'connect.php';

$tno = escapeString($conn,($_POST['tno']));
$trip_type = escapeString($conn,($_POST['trip_type']));
$trip_no = escapeString($conn,($_POST['trip_no']));

if($trip_type=='RUNNING')
{
	$sql = Qry($conn,"SELECT id as trip_id,trip_no,branch,lrno,from_station,to_station,date(date) as start_date,date(end_date) as end_date,expense,toll_tax,
	diesel,diesel_qty FROM dairy.trip WHERE tno='$tno'");
	
	$trip_type = "RUNNING";
}
else
{
	$sql = Qry($conn,"SELECT id,trip_id,trip_no,branch,lrno,from_station,to_station,date(date) as start_date,date(end_date) as end_date,expense,
	toll_tax,diesel,diesel_qty FROM dairy.trip_final WHERE trip_no='$trip_no'");
	
	$trip_type = "OLD";
}

if(!$sql){
	echo getMySQLError($conn);
	errorLog(getMySQLError($conn),$conn,$page_url,__LINE__);
	exit();
}
	
if(numRows($sql)==0)
{
	AlertRightCornerError("No record found !");
	exit();
}
?>
<table id="example" class="table table-bordered table-striped" style="font-size:13px;">
        <thead>
		<tr>
			  <th>#</th>
			  <th>Trip_No.</th>
			  <th>Branch</th>
			  <th>LR_No.</th>
			  <th>From</th>
			  <th>To</th>
			  <th>Trip_Date</th>
			  <th>Trip_End_Date</th>
			  <th>Diesel</th>
			  <th>Qty</th>
			  <th>#ExpEntry</th>
		</tr>
		</thead>
    <tbody id=""> 
	
<?php
$sn=1;

while($row = fetchArray($sql))
{	
	if($row['end_date']==0)
	{
		$end_date = "<font color='green'>Running</font>";
	}
	else
	{
		$end_date = date("d-m-y",strtotime($row['end_date']));
	}
	
	$start_date = date("d-m-y",strtotime($row['start_date']));
	
		echo "<tr>	
			<td>$sn</td>
			<td>$row[trip_no]</td>
			<td>$row[branch]</td>
			<td>$row[lrno]</td>
			<td>$row[from_station]</td>
			<td>$row[to_station]</td>
			<td>$start_date</td>
			<td>$end_date</td>
			<td>$row[diesel]</td>
			<td>$row[diesel_qty]</td>
			<input type='hidden' id='from_loc_$row[trip_id]' value='$row[from_station]'>			
			<input type='hidden' id='to_loc_$row[trip_id]' value='$row[to_station]'>			
			<td><button class='btn btn-xs btn-primary' type='button' onclick=AddDiesel('$row[trip_id]','$trip_type')>Add Diesel</button></td>
		</tr>";
$sn++;		
}
	echo "</tbody>
</table>";
?>
	
<script> 
	$("#loadicon").fadeOut('slow');
</script> 