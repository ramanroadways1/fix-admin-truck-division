<?php
require_once 'connect.php';

$date = date("Y-m-d");
$timestamp = date("Y-m-d H:i:s");

$id=escapeString($conn,(trim($_POST['id'])));
$value=escapeString($conn,(trim($_POST['value'])));

StartCommit($conn);
$flag = true;

$update = Qry($conn,"UPDATE dairy.exp_head SET fix_entry_to_supervisor='$value' WHERE id='$id'");

if(!$update){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

if($flag)
{
	MySQLCommit($conn);
	closeConnection($conn);
	echo "<script>LoadTable();</script>";
	exit();
}
else
{
	MySQLRollBack($conn);
	closeConnection($conn);
	AlertErrorTopRight("Error while processing request !");
	exit();
}	
?>