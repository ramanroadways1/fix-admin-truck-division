<?php
require_once("./connect.php");

$date = date("Y-m-d");
$timestamp = date("Y-m-d H:i:s");

$lrno = escapeString($conn,strtoupper($_POST['lrno']));
$cross_stn = escapeString($conn,strtoupper($_POST['cross_stn']));
$cross_sel = escapeString($conn,strtoupper($_POST['cross_sel']));
$id = escapeString($conn,strtoupper($_POST['id']));
$trip_check = escapeString($conn,strtoupper($_POST['trip_check']));
$frno = escapeString($conn,strtoupper($_POST['frno']));
$trip_id = escapeString($conn,strtoupper($_POST['trip_id']));
$to_id = escapeString($conn,strtoupper($_POST['to_id']));

if($to_id==0)
{
	AlertRightCornerError("Location ID not found !");
	exit();
}

if($cross_sel!='NO' AND $cross_sel!='YES')
{
	AlertRightCornerError("Invalid option selected !");
	exit();
}

if($trip_check=='1' AND $trip_id==0)
{
	AlertRightCornerError("Trip ID not found !");
	exit();
}

if($trip_check=='0' AND $trip_id!=0)
{
	AlertRightCornerError("Trip ID is not valid !");
	exit();
}

$chk_pod = Qry($conn,"SELECT id FROM rcv_pod WHERE lrno='$lrno' AND frno='$frno'");

if(!$chk_pod){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	AlertRightCornerError("Error while processing request !");
	exit();
}
	
if(numRows($chk_pod)>0)
{
	$is_pod_rcv="YES";
	$row_pod = fetchArray($chk_pod);
	$pod_id = $row_pod['id'];
}
else
{
	$is_pod_rcv="NO";
}

$trip_kms=0;

if($trip_id!=0)
{
	$chk_trip = Qry($conn,"SELECT id,tno,from_id FROM dairy.trip WHERE id='$trip_id'");
	
	if(!$chk_trip){
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		AlertRightCornerError("Error while processing request !");
		exit();
	}
	
	if(numRows($chk_trip)==0)
	{
		AlertRightCornerError("Running trip not found !");
		exit();
	}
	else
	{
		$row_trip2 = fetchArray($chk_trip);
		
		$chk_nxt_trip = Qry($conn,"SELECT id,from_station,from_id FROM dairy.trip WHERE id>'$trip_id' AND tno='$row_trip2[tno]' ORDER BY id ASC LIMIT 1");
	
		if(!$chk_nxt_trip){
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			AlertRightCornerError("Error while processing request !");
			exit();
		}
		
		if(numRows($chk_nxt_trip)>0)
		{
			$row_nxt = fetchArray($chk_nxt_trip);
			
			if($row_nxt['from_id']!=$to_id)
			{
				AlertRightCornerError("Next trip created and from location is: $row_nxt[from_station] !");
				exit();
			}
		}
	}
}

if($cross_sel=='NO')
{
	StartCommit($conn);
	$flag = true;

	if($is_pod_rcv=='YES')
	{
		$update_pod = Qry($conn,"UPDATE rcv_pod SET crossing='0' WHERE id='$pod_id'");
		
		if(!$update_pod){
			$flag = false;
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		}
	}

	$get_main_dest = Qry($conn,"SELECT id,tstation,to_id FROM lr_sample WHERE lrno='$lrno'");
	
	if(!$get_main_dest){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
	
	$row_dest = fetchArray($get_main_dest);
	
	$to_loc_trip = $row_dest['tstation'];
	$to_loc_trip_id = $row_dest['to_id'];
	$lr_id = $row_dest['id'];
	
	$qry_fm_lr_update=Qry($conn,"UPDATE freight_form_lr SET tstation='$row_dest[tstation]',to_id='$row_dest[to_id]',crossing='NO',
	cross_to='',cross_stn_id='' WHERE id='$id'");
	
	if(!$qry_fm_lr_update){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
	
	$qry_fm_Update = Qry($conn,"UPDATE freight_form SET to1='$row_dest[tstation]',to_id='$row_dest[to_id]' WHERE frno='$frno'");
	
	if(!$qry_fm_Update){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
	
	$lr_update = Qry($conn,"UPDATE lr_sample SET crossing='NO' WHERE id='$lr_id'");
	
	if(!$lr_update){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
	
	if($trip_id!=0)
	{
		if(empty($row_trip2['from_id']))
		{
			$flag = false;
			errorLog("From id not found..",$conn,$page_name,__LINE__);
		}
		
		$fetch_km = Qry($conn,"SELECT distance as km FROM master_addr_book WHERE from_id='$row_trip2[from_id]' AND to_id='$row_dest[to_id]'");
		
		if(!$fetch_km){
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			echo "<script>alert('Error..');$('#loadicon').hide();</script>";exit();
		}
	
		$row_km = fetchArray($fetch_km);
		
		$update_trip_diary = Qry($conn,"UPDATE dairy.trip SET to_station='$row_dest[tstation]',to_id='$row_dest[to_id]',km='$row_km[km]' 
		WHERE id='$trip_id'");
		
		if(!$update_trip_diary){
			$flag = false;
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		}
		
		if(AffectedRows($conn)==0){
			$flag = false;
			errorLog("Location not updated in trip.",$conn,$page_name,__LINE__);
		}
	}
}
else
{
	StartCommit($conn);
	$flag = true;

	if($is_pod_rcv=='YES')
	{
		$update_pod = Qry($conn,"UPDATE rcv_pod SET crossing='1' WHERE id='$pod_id'");
		
		if(!$update_pod){
			$flag = false;
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		}
	}

	$qry_fm_old=Qry($conn,"UPDATE freight_form_lr SET tstation='$cross_stn',to_id='$to_id',crossing='YES',
	cross_to='$cross_stn',cross_stn_id='$to_id' WHERE id='$id'");
	
	if(!$qry_fm_old){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
	
	$qry_fm_Update = Qry($conn,"UPDATE freight_form SET to1='$cross_stn',to_id='$to_id' WHERE frno='$frno'");
	
	if(!$qry_fm_Update){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
	
	$qry_lr_update=Qry($conn,"UPDATE lr_sample SET crossing='YES' WHERE lrno='$lrno'");
	
	if(!$qry_lr_update){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
	
	if($trip_id!=0)
	{
		if(empty($row_trip2['from_id']))
		{
			$flag = false;
			errorLog("From id not found..",$conn,$page_name,__LINE__);
		}
		
		$fetch_km = Qry($conn,"SELECT distance as km FROM master_addr_book WHERE from_id='$row_trip2[from_id]' AND to_id='$to_id'");
		
		if(!$fetch_km){
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			echo "<script>alert('Error..');$('#loadicon').hide();</script>";exit();
		}
	
		$row_km = fetchArray($fetch_km);
		
		$update_trip_diary = Qry($conn,"UPDATE dairy.trip SET to_station='$cross_stn',to_id='$to_id',km='$row_km[km]' WHERE id='$trip_id'");
		
		if(!$update_trip_diary){
			$flag = false;
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		}
		
		// if(AffectedRows($conn)==0){
			// $flag = false;
			// errorLog("Location not updated in trip.",$conn,$page_name,__LINE__);
		// }
	}
}

$log_data = "LR Number : $lrno, Crossing Station : $cross_stn. Mode : $cross_sel. Trip Created: $trip_check. TripId : $trip_id.";
	
$insert_log = Qry($conn,"INSERT INTO dairy.ediary_admin_log(code,action,desct,branch,username,timestamp) VALUES ('$lrno','CROSSING_$cross_sel',
'$log_data','Fix_Admin','$_SESSION[ediary_fix_admin]','$timestamp')");	

if(!$insert_log){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}
	
if($flag)
{
	MySQLCommit($conn);
	closeConnection($conn);
	AlertRightCornerSuccess("Success !");
	// echo "<script>document.getElementById('add_btn').click();</script>";
	exit();
}
else
{
	MySQLRollBack($conn);
	closeConnection($conn);
	AlertRightCornerError("Error while processing request !");
	exit();
}
?>