<?php
require_once 'connect.php';

$date = date("Y-m-d");
$timestamp = date("Y-m-d H:i:s");

$tno = escapeString($conn,strtoupper(trim($_POST['tno'])));

if($tno=='')
{
	AlertErrorTopRight("Vehicle number not found !");
	echo "<script>$('#add_btn').attr('disabled', false);</script>";
	exit();
}

StartCommit($conn);
$flag = true;

$update = Qry($conn,"UPDATE dairy.own_truck SET force_update_diesel_qty='0',diesel_tank_cap='0',diesel_left='0',diesel_trip_id='0',
qty_update_timestamp=NULL,supervisor_qty_approval_timestamp=NULL WHERE tno='$tno'");

if(!$update){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

if($flag)
{
	MySQLCommit($conn);
	closeConnection($conn);
	AlertRightCornerSuccess("Record deleted !");
	echo "<script>$('#remove_btn_$tno').attr('disabled',true);</script>";
	echo "<script>$('#remove_btn_$tno').attr('onclick','');</script>";
	exit();
}
else
{
	MySQLRollBack($conn);
	closeConnection($conn);
	AlertErrorTopRight("Error while processing request !");
	echo "<script>$('#remove_btn_$tno').attr('disabled',true);</script>";
	echo "<script>$('#remove_btn_$tno').attr('onclick','');</script>";
	exit();
}	
?>