<?php
include("header.php");

$chk = Qry($conn,"SELECT id FROM _access_control WHERE username='$_SESSION[ediary_fix_admin]' AND func_id=(SELECT id FROM 
_access_control_func_list WHERE session_role='8' AND func_name='Manage_Fuel_Station') AND u_view='1'");
			  
if(numRows($chk)==0)
{
	echo "<script>window.location.href='./';</script>";
	exit();
}
?>
<div class="content-wrapper">
      <section class="content-header">
          <h1 style="font-size:16px;">Manage Fuel Station : Market Vehicle </h1>
       </section>
       
	   <section class="content">
          <div class="row">
            <div class="col-xs-12">
			<div class="box">
                <div class="box-body">
				
				<div class="col-md-12">&nbsp;</div>
				
				<div class="col-md-12">
					<div class="row">
						
						<div class="form-group col-md-3">
							<label style="font-size:12px !important">Pump Name <font color="red"><sup>*</sup></font></label>
							<input type="text" oninput="this.value=this.value.replace(/[^a-z 0-9A-Z.-]/,'');" class="form-control" style="font-size:12px !important;text-transform:uppercase" required="required" id="pump_name" />
						</div>
						
						<div class="form-group col-md-2">
							<label style="font-size:12px !important">Pump Code <font color="red"><sup>*</sup></font></label>
							<input type="text" oninput="this.value=this.value.replace(/[^a-z0-9A-Z-]/,'');" class="form-control" style="font-size:12px !important;text-transform:uppercase" required="required" id="pump_code" />
						</div>
						
						<div class="form-group col-md-3">
							<label style="font-size:12px !important">For Branch <font color="red"><sup>*</sup></font></label>
							<select class="form-control" style="font-size:12px !important" required="required" id="branch">
								<option value="" style="font-size:12px !important">--select branch--</option>
								<?php
								$get_branches = Qry($conn,"SELECT username FROM user WHERE role='2' AND username NOT IN('HEAD','DUMMY')");
								
								if(numRows($get_branches) > 0)
								{
									while($row_branch = fetchArray($get_branches))
									{
										echo "<option value='$row_branch[username]' style='font-size:12px !important'>$row_branch[username]</option>";
									}
								}
								?>
							</select>
						</div>
						
						<div class="form-group col-md-2">
							<label style="font-size:12px !important">Fuel Company <font color="red"><sup>*</sup></font></label>
							<select class="form-control" style="font-size:12px !important" required="required" id="fuel_company">
								<option value="" style="font-size:12px !important">--select company--</option>
								<?php
								$get_company = Qry($conn,"SELECT name FROM diesel_api.dsl_company WHERE status='1'");
								
								if(numRows($get_company) > 0)
								{
									while($row_c = fetchArray($get_company))
									{
										echo "<option value='$row_c[name]' style='font-size:12px !important'>$row_c[name]</option>";
									}
								}
								?>
							</select>
						</div>
						
						<div class="form-group col-md-2">
							<?php if(!isMobile()) { echo "<label>&nbsp;</label><br />"; } ?>
							<button type="button" style="margin-top:1px;font-size:12px !important" onclick="AddPump()" class="btn btn-sm btn-success <?php if(isMobile()) { echo "btn-block"; } ?>" id="add_btn"><i class="fa fa-plus-circle" aria-hidden="true"></i> &nbsp; ADD NEW</button>
						</div>
					</div>
				</div>
				
				<div class="col-md-12">&nbsp;</div>
				
				<div class="col-md-12 table-responsive" id="load_table">
              
				 </div> 
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

<div id="func_result"></div>  
<div id="modal_res1"></div>  

<script>	
function LoadData()
{
	$('#loadicon').show();
			jQuery.ajax({
				url: "_load_manage_market_vehicle_pump.php",
				data: 'pump_type=' + 'MARKET',
				type: "POST",
				success: function(data) {
					$("#load_table").html(data);
					$('#example').DataTable({ 
					"lengthMenu": [ [50, 100, 500, -1], [50, 100,500, "All"] ], 
						"destroy": true, //use for reinitialize datatable
					});
				},
				error: function() {}
	});
}

function AddPump()
{
	var pump_name = $('#pump_name').val();
	var pump_code = $('#pump_code').val();
	var branch = $('#branch').val();
	var fuel_company = $('#fuel_company').val();
	
	if(pump_name=='' || pump_code=='' || branch=='' || fuel_company=='')
	{
		Swal.fire({icon: 'warning',html: '<font size=\'2\' color=\'black\'>Fill all inputs first !</font>',});
	}
	else
	{
		$('#add_btn').attr('disabled',true);
		jQuery.ajax({
			url: "save_add_new_pump.php",
			data: 'pump_name=' + pump_name + '&pump_code=' + pump_code + '&branch=' + branch + '&fuel_company=' + fuel_company + '&type=' + 'MARKET',
			type: "POST",
			success: function(data) {
				$("#func_result").html(data);
			},
			error: function() {}
		});
	}
}

LoadData();
</script>

<?php include("footer.php") ?>