<?php
require_once("connect.php");

$timestamp = date("Y-m-d H:i:s");

echo "<script>$('#btn_save').attr('disabled',true);</script>";

$truck_no = escapeString($conn,strtoupper($_POST['truck_no']));
$route_type = escapeString($conn,$_POST['route_type']);
$empty_avg = escapeString($conn,$_POST['empty_avg']);
$loaded_avg = escapeString($conn,$_POST['loaded_avg']);
$overload_avg = escapeString($conn,$_POST['overload_avg']);

if($truck_no=='')
{
	AlertError("Enter vehicle number first !");
	echo "<script>$('#btn_save').attr('disabled',false);$('#loadicon').fadeOut('slow');</script>";
	exit();
}

if($route_type=='')
{
	AlertError("Select route type first !");
	echo "<script>$('#btn_save').attr('disabled',false);$('#loadicon').fadeOut('slow');</script>";
	exit();
}

if($route_type!='normal' AND $route_type!='congested')
{
	AlertError("Invalid route type !");
	echo "<script>$('#btn_save').attr('disabled',false);$('#loadicon').fadeOut('slow');</script>";
	exit();
}

if($empty_avg=='' || $empty_avg<=0)
{
	AlertError("Check empty average !");
	echo "<script>$('#btn_save').attr('disabled',false);$('#loadicon').fadeOut('slow');</script>";
	exit();
}

if($loaded_avg=='' || $loaded_avg<=0)
{
	AlertError("Check loaded average !");
	echo "<script>$('#btn_save').attr('disabled',false);$('#loadicon').fadeOut('slow');</script>";
	exit();
}

if($overload_avg=='' || $overload_avg<=0)
{
	AlertError("Check overload average !");
	echo "<script>$('#btn_save').attr('disabled',false);$('#loadicon').fadeOut('slow');</script>";
	exit();
}

$chk_vehicle = Qry($conn,"SELECT o.wheeler,m.max_weight 
FROM dairy.own_truck AS o 
LEFT OUTER JOIN max_weight_wheeler_wise AS m ON m.wheeler=o.wheeler 
WHERE o.tno='$truck_no'");

if(!$chk_vehicle){
	AlertError("Error while processing request !");
	errorLog(getMySQLError($conn),$conn,$page_url,__LINE__);
	exit();
}

if(numRows($chk_vehicle)==0)
{
	AlertError("Vehicle not found !");
	echo "<script>$('#btn_save').attr('disabled',false);$('#loadicon').fadeOut('slow');</script>";
	exit();
}

$row_vehicle = fetchArray($chk_vehicle);


$wheeler = $row_vehicle['wheeler'];
$max_weight = $row_vehicle['max_weight'];

if($max_weight=='' || $max_weight==0)
{
	AlertError("Weight capacity not found !");
	echo "<script>$('#btn_save').attr('disabled',false);$('#loadicon').fadeOut('slow');</script>";
	exit();
}

$chk_record = Qry($conn,"SELECT id FROM dairy._avg_data WHERE veh_no='$truck_no' AND route_type='$route_type'");

if(!$chk_record){
	AlertError("Error while processing request !");
	errorLog(getMySQLError($conn),$conn,$page_url,__LINE__);
	exit();
}

if(numRows($chk_record)>0)
{
	AlertError("Duplicate record found !");
	echo "<script>$('#btn_save').attr('disabled',false);$('#loadicon').fadeOut('slow');</script>";
	exit();
}

StartCommit($conn);
$flag = true;
	
$insert_avg = Qry($conn,"INSERT INTO dairy._avg_data(veh_no,wheeler,max_capacity,route_type,empty_avg,normal_avg,overload_avg,timestamp) 
VALUES ('$truck_no','$wheeler','$max_weight','$route_type','$empty_avg','$loaded_avg','$overload_avg','$timestamp')");
	
if(!$insert_avg){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

if($flag)
{
	MySQLCommit($conn);
	closeConnection($conn);
	echo "<script>
		$('#truck_no').val('');
		$('#route_type').val('');
		$('#empty_avg').val('');
		$('#loaded_avg').val('');
		$('#overload_avg').val('');
		$('#btn_save').attr('disabled',false);
		$('#loadicon').fadeOut('slow');
	</script>";
	AlertRightCornerSuccessFadeFast("Avg data updated for route: $route_type !");
}
else
{
	MySQLRollBack($conn);
	closeConnection($conn);
	AlertError("Error While Processing Request !");
	echo "<script>$('#btn_save').attr('disabled',false);$('#loadicon').fadeOut('slow');</script>";
	exit();
}
?>