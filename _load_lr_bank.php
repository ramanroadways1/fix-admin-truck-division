<?php
require_once("connect.php");
?>

			<table id="example" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th>#</th>
                        <th>Branch</th>
                        <th>From</th>
                        <th>To</th>
                        <th>Company</th>
                        <th>Stock</th>
                        <th>Used</th>
                        <th>Left</th>
                        <th>Edit</th>
                        <th>Delete</th>
                        <th>Timestamp</th>
                      </tr>
                    </thead>
                    <tbody>
	<?php
	$get_approvals = Qry($conn,"SELECT id,branch,from_range,to_range,company,stock,stock_left,stock-stock_left as used,timestamp 
	FROM lr_bank WHERE mark!='1' AND stock_left>0");
	
	if(!$get_approvals){
		AlertErrorTopRight("Error while processing request !");
		echo getMySQLError($conn);
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		exit();
	}
	
	if(numRows($get_approvals)==0)
	{
		echo "<tr>
			<td colspan='11'>No record found !</td>
			<td style='display: none'></td>
			<td style='display: none'></td>
			<td style='display: none'></td>
			<td style='display: none'></td>
			<td style='display: none'></td>
			<td style='display: none'></td>
			<td style='display: none'></td>
			<td style='display: none'></td>
			<td style='display: none'></td>
			<td style='display: none'></td>
			<td style='display: none'></td>
		</tr>";
	}
	else
	{
		$i=1;
		while($row = fetchArray($get_approvals))
		{
			$timestamp = date("d-m-y h:i A",strtotime($row['timestamp']));
			
			echo "<tr>
				<td>$i</td>
				<td>$row[branch]</td>
				<td>$row[from_range]</td>
				<td>$row[to_range]</td>
				<td>$row[company]</td>
				<td>$row[stock]</td>
				<td>$row[used]</td>
				<td>$row[stock_left]</td>
				<td><button id='edit_button$row[id]' class='update_lr_bank_btn btn btn-primary btn-xs' type='button' onclick=Edit('$row[branch]','$row[id]','$row[from_range]','$row[to_range]','$row[company]')>Edit</button></td>
				<td><button id='del_button$row[id]' class='delete_lr_bank_btn btn btn-danger btn-xs' type='button' onclick=Delete1('$row[id]','$row[stock]','$row[stock_left]')>Delete</button></td>
				<td>$timestamp</td>
			</tr>";
		$i++;	
		}
	}
	?>	
                    </tbody>
                  </table>

<script>				  
$("#loadicon").fadeOut('slow');
</script>		

<?php
$chk_update = Qry($conn,"SELECT id FROM _access_control WHERE username='$_SESSION[ediary_fix_admin]' AND func_id=(SELECT id FROM 
_access_control_func_list WHERE session_role='8' AND func_name='LR_Bank') AND u_update='1'");
			  
if(numRows($chk_update)>0)
{
	echo "<script>
		$('.update_lr_bank_btn').attr('disabled',false);
	</script>";	
}
else
{echo "<script>
		$('.update_lr_bank_btn').attr('disabled',true);
	</script>";	
	
}

$chk_delete = Qry($conn,"SELECT id FROM _access_control WHERE username='$_SESSION[ediary_fix_admin]' AND func_id=(SELECT id FROM 
_access_control_func_list WHERE session_role='8' AND func_name='LR_Bank') AND u_delete='1'");
			  
if(numRows($chk_delete)>0)
{
	echo "<script>
		$('.delete_lr_bank_btn').attr('disabled',false);
	</script>";	
}
else
{
	echo "<script>
		$('.delete_lr_bank_btn').attr('disabled',true);
	</script>";	
}
?>		  