<?php
require_once("./connect.php");

$id = escapeString($conn,strtoupper($_POST['id']));
$timestamp = date("Y-m-d H:i:s");

if(empty($id))
{
	AlertErrorTopRight("ID not found !");
	exit();
}

$fetch_request = Qry($conn,"SELECT tno,oid,rc_copy_old,rc_copy_rear_old,pan_copy_old,rc_copy_new,rc_copy_rear_new,pan_copy_new,wheeler,new_name,
new_addr,branch,new_mobile,new_pan FROM owner_change_req WHERE id='$id'");

if(!$fetch_request){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	AlertErrorTopRight("Error while Processing Request !");
	exit();
}

if(numRows($fetch_request) == 0)
{
	AlertErrorTopRight("Request not found !");
	exit();
}

$row = fetchArray($fetch_request);	
	
$new_wheeler = $row['wheeler'];
$new_name = $row['new_name'];
$new_addr = $row['new_addr'];
$branch = $row['branch'];
$new_mobile = $row['new_mobile'];
$new_pan = $row['new_pan'];
$oid = $row['oid'];

$getMail_Id = Qry($conn,"SELECT email FROM user WHERE username = '$branch'");

if(!$getMail_Id){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	AlertErrorTopRight("Error while Processing Request !");
	exit();
}

if(numRows($getMail_Id) == 0)
{
	AlertErrorTopRight("Branch not found !");
	exit();
}
else
{
	$row_mail = fetchArray($getMail_Id);
	$mail_id = $row_mail['email'];
}

if($mail_id=='')
{
	AlertErrorTopRight("Branch mail ID not updated !");
	exit();
}

require '../MailServer/PHPMailerAutoload.php';
	
$email = 'noreply@ramanroadways.com';                   
$password = 'rrpl123#';
$to_id = $mail_id;


$rc_old = $row['rc_copy_old'];
$rc_old_rear = $row['rc_copy_rear_old'];
$pan_old = $row['pan_copy_old'];

$rc_new = $row['rc_copy_new'];
$rc_new_rear = $row['rc_copy_rear_new'];
$pan_new = $row['pan_copy_new'];
	
if($rc_old!='NA')
{
	$rc_old = substr($rc_old, strpos($rc_old, "8F/") + 3); 
	@unlink("../b5aY6EZzK52NA8F/".$rc_old);
}

if($rc_old_rear!='NA')
{
	$rc_old_rear = substr($rc_old_rear, strpos($rc_old_rear, "8F/") + 3);  
	@unlink("../b5aY6EZzK52NA8F/".$rc_old_rear);
}

if($pan_old!='NA')
{
	$pan_old = substr($pan_old, strpos($pan_old, "8F/") + 3);    
	@unlink("../b5aY6EZzK52NA8F/".$pan_old);
}

StartCommit($conn);
$flag = true;
	
$update_new_owner = Qry($conn,"UPDATE mk_truck SET wheeler='$new_wheeler',name='$new_name',mo1='$new_mobile',pan='$new_pan',up1='$rc_new',
rc_rear='$rc_new_rear',up4='$pan_new',full='$new_addr',up6='',dec_upload_time=NULL,tds='',bank='',acname='',accno='',ifsc='' WHERE id='$oid'");

if(!$update_new_owner){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

$update_req = Qry($conn,"UPDATE owner_change_req SET done='1',done_time='$timestamp' WHERE id='$id'");

if(!$update_req){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

if($flag)
{
	MySQLCommit($conn);
	closeConnection($conn);
}
else
{
	MySQLRollBack($conn);
	closeConnection($conn);
	AlertErrorTopRight("Error while Processing Request !");
	exit();
}
	
$message = '<h3 style="font-family:Verdana">Owner Change Request Approved : '.$tno.'.</h3>
<b>NAME :</b> '.$new_name.' <br>
<b>MOBILE : </b>'.$new_mobile.'<br>
<b>PAN NO :</b> '.$new_pan.'<br>
<b>ADDRESS :</b> '.$new_addr.'
<br>
<br>
<b><font color="green">Request Approved. </font></b>
';

$subject = 'Owner Change Request : '.$tno.'';
$mail = new PHPMailer;
$mail->isSMTP();
$mail->Host = 'smtp.rediffmailpro.com';
$mail->Port = 587;
$mail->SMTPSecure = 'tls';
$mail->SMTPAuth = true;
$mail->Username = $email;
$mail->Password = $password;
$mail->setFrom('noreply@ramanroadways', 'noReplyRamanRoadways');
$mail->addReplyTo('noreply@ramanroadways', 'noReplyRamanRoadways');
$mail->addAddress($to_id);
$mail->AddCC('noreply@ramanroadways', 'noReplyRamanRoadways');
$mail->Subject = $subject;
$mail->msgHTML($message);

if(!$mail->send())
{
	$error = "Mailer Error: " . $mail->ErrorInfo;
	AlertErrorTopRight("$error");
}
	
	echo "<script>
		$('#approve_btn_$id').attr('disabled',true);
		$('#reject_btn_$id').attr('disabled',true);
		$('#approve_btn_$id').html('Approved');
		$('#reject_btn_$id').html('Approved');
			
		$('#approve_btn_$id').attr('onclick','');
		$('#reject_btn_$id').attr('onclick','');
		$('#loadicon').fadeOut('slow');
	</script>";
?>