<?php
require_once("./connect.php");

$id = escapeString($conn,($_POST['trip_id']));
$edit_branch = escapeString($conn,($_POST['edit_branch']));
$from_station = escapeString($conn,($_POST['from_station']));
$to_station = escapeString($conn,($_POST['to_station']));
$from_id = escapeString($conn,($_POST['from_id1']));
$to_id = escapeString($conn,($_POST['to_id1']));
$from_pincode = escapeString($conn,($_POST['from_pincode']));
$to_pincode = escapeString($conn,($_POST['to_pincode']));
$from_lat_long = escapeString($conn,($_POST['from_lat_long']));
$to_lat_long = escapeString($conn,($_POST['to_lat_long']));
$trip_km = escapeString($conn,($_POST['trip_km']));

$timestamp = date("Y-m-d H:i:s");

if(strlen($from_lat_long)<=5)
{
	AlertErrorTopRight("From location POI not found !");
	exit();
}

if(strlen($to_lat_long)<=5)
{
	AlertErrorTopRight("To location POI not found !");
	exit();
}


if(empty($trip_km))
{
	AlertErrorTopRight("Trip KM is not valid !");
	exit();
}

if(empty($from_id))
{
	AlertErrorTopRight("From-ID not found !");
	exit();
}
	
if(empty($to_id))
{
	AlertErrorTopRight("To-ID not found !");
	exit();
}
	
if(strlen($from_pincode)!=6)
{
	AlertErrorTopRight("Invalid from pincode !");
	exit();
}

if(strlen($to_pincode)!=6)
{
	AlertErrorTopRight("Invalid destination pincode !");
	exit();
}

$get_current_data = Qry($conn,"SELECT tno,edit_branch,from_station,to_station,from_pincode,to_pincode,km,dsl_consumption FROM dairy.trip 
WHERE id='$id'");

if(!$get_current_data){
	AlertErrorTopRight("Error while processing request !");
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	exit();
}

$row_current_data = fetchArray($get_current_data);

$edit_branch_old = $row_current_data['edit_branch'];
$from_old = $row_current_data['from_station'];
$to_old = $row_current_data['to_station'];
$from_pincode_old = $row_current_data['from_pincode'];
$to_pincode_old = $row_current_data['to_pincode'];
$trip_km_old = $row_current_data['km'];
$dsl_consumption_old = $row_current_data['dsl_consumption'];
$tno = $row_current_data['tno'];

$get_avg = sprintf("%.2f",($trip_km_old/$dsl_consumption_old));

$dsl_consumption = sprintf("%.2f",($trip_km/$get_avg));

$log_data = "TripId : $id. From_Location : Old-> $from_old and New-> $from_station, To_Location : Old-> $to_old and New-> $to_station, 
Edit_Branch : Old-> $edit_branch_old and New-> $edit_branch, From_Pincode : Old-> $from_pincode_old and New-> $from_pincode, 
To_Pincode : Old-> $to_pincode_old and New-> $to_pincode, Dsl_Cons. : Old-> $dsl_consumption_old and New-> $dsl_consumption.";

$chk_prev_trip =  Qry($conn,"SELECT to_station FROM dairy.trip WHERE tno='$tno' AND id<'$id' ORDER BY id DESC LIMIT 1");

if(!$chk_prev_trip){
	AlertErrorTopRight("Error while processing request !");
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	exit();
}

$chk_next_trip =  Qry($conn,"SELECT id,from_station,to_station,to_id,lr_type FROM dairy.trip WHERE tno='$tno' AND id>'$id' ORDER BY id ASC LIMIT 1");

if(!$chk_next_trip){
	AlertErrorTopRight("Error while processing request !");
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	exit();
}

if(numRows($chk_prev_trip)>0)
{
	$row_prev = fetchArray($chk_prev_trip);
	
	if($row_prev['to_station'] != $from_station)
	{
		AlertErrorTopRight("From location is $from_station. and Previous Trip\'s destination is $row_prev[to_station] !");
		exit();
	}
}

if(numRows($chk_next_trip)>0)
{
	$row_next = fetchArray($chk_next_trip);
	$lr_type_next = $row_next['lr_type'];
	$to_station_next = $row_next['to_station'];
	$to_id_next = $row_next['to_id'];
	$next_trip_id = $row_next['id'];
	
	if($row_next['from_station'] != $to_station)
	{
		if($lr_type_next=='EMPTY' || $lr_type_next=='CON20' || $lr_type_next=='CON40')
		{
			$update_next_trip = "YES";
		}
		else
		{
			AlertErrorTopRight("Destination is $to_station. and Next Trip\'s from location is $row_next[from_station] !");
			exit();
		}
	}
	else
	{
		$update_next_trip = "NO";
	}
}
else
{
	$update_next_trip = "NO";
}

if($update_next_trip == 'YES')
{
	$fetch_km_for_next_trip = Qry($conn,"SELECT distance FROM master_addr_book WHERE from_id='$to_id' AND to_id='$to_id_next'");
	
	if(!$fetch_km_for_next_trip){
		AlertErrorTopRight("Error while processing request !");
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		exit();
	}
	
	if(numRows($fetch_km_for_next_trip)==0)
	{
		$get_from_lat_long = Qry($conn,"SELECT _lat,_long FROM station WHERE id='$to_id'");
		
		if(!$get_from_lat_long){
			AlertErrorTopRight("Error while processing request !");
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			exit();
		}
	
		$get_to_lat_long = Qry($conn,"SELECT _lat,_long FROM station WHERE id='$to_id_next'");
		
		if(!$get_to_lat_long){
			AlertErrorTopRight("Error while processing request !");
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			exit();
		}
		
		$row_from_lat_lng = fetchArray($get_from_lat_long);
		$row_to_lat_lng = fetchArray($get_to_lat_long);
		
		$origin = trim($row_from_lat_lng['_lat'].",".$row_from_lat_lng['_long']);
		$destination = trim($row_to_lat_lng['_lat'].",".$row_to_lat_lng['_long']);

		$url="https://maps.googleapis.com/maps/api/distancematrix/json?units=imperial&origins=".urlencode($origin)."&destinations=".urlencode($destination)."&key=AIzaSyCZ6JUFGB8r5TpRYGnJjNxUH1NZdxaPoGw";

		$api = file_get_contents($url);
		$data = json_decode($api);
					
		$api_status = $data->rows[0]->elements[0]->status;
			
		if($api_status=='NOT_FOUND')
		{
			AlertErrorTopRight("distance not found !");
			exit();
		}

		if($api_status!='OK')
		{
			echo "<span style='color:red;font-size:13px'>API Error: $api_status !</span>";
			AlertErrorTopRight("Distance API error !");
			exit();
		}
					
		$dest_addr = $data->destination_addresses[0];
		$origin_addr = $data->origin_addresses[0];
		$distance = round((int)$data->rows[0]->elements[0]->distance->value / 1000);
		$travel_time = $data->rows[0]->elements[0]->duration->text;
		$travel_time_value = $data->rows[0]->elements[0]->duration->value;
		$travel_hrs = gmdate("H", $travel_time_value);
		$travel_minutes = gmdate("i", $travel_time_value);
		$travel_seconds = gmdate("s", $travel_time_value);
		
		$insert_distance = Qry($conn,"INSERT INTO master_addr_book(from_id,to_id,distance,branch,branch_user,timestamp) VALUES ('$to_id',
		'$to_id_next','$distance','EDIARY_ADMIN','$_SESSION[ediary_fix_admin]','$timestamp')");
		
		if(!$insert_distance){
			AlertError("Error !");
			errorLog(getMySQLError($conn),$conn,$page_url,__LINE__);
			exit();
		}
		
		$km_for_next = $distance;
	}
	else
	{
		$row_km_for_next = fetchArray($fetch_km_for_next_trip);
		$km_for_next = $row_km_for_next['distance'];
	}
}

StartCommit($conn);
$flag = true;

if($update_next_trip=='YES')
{
	$update_next_trip_qry = Qry($conn,"UPDATE dairy.trip SET from_station='$to_station',from_pincode='$to_pincode',from_id='$to_id',
	from_poi='$to_lat_long',km='$km_for_next' WHERE id='$next_trip_id'");
	
	if(!$update_next_trip_qry){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
}

$update_qry = Qry($conn,"UPDATE dairy.trip SET edit_branch='$edit_branch',from_station='$from_station',from_id='$from_id',from_poi='$from_lat_long',
to_poi='$to_lat_long',from_pincode='$from_pincode',to_station='$to_station',to_id='$to_id',to_pincode='$to_pincode',km='$trip_km',
pincode='$to_pincode' WHERE id='$id'");

if(!$update_qry) 
{
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

$insert_log = Qry($conn,"INSERT INTO dairy.ediary_admin_log(code,action,desct,timestamp) VALUES ('$tno','TRIP_UPDATE','$log_data','$timestamp')");

if(!$insert_log)
{
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

if($flag)
{
	MySQLCommit($conn);
	closeConnection($conn);
	
	AlertRightCornerSuccess("Updated Successfully !");
	echo "<script>
		ReLoadPage();
	</script>";
}
else
{
	MySQLRollBack($conn);
	closeConnection($conn);
	Redirect("Error While Processing Request.","./");
	exit();
}
?>