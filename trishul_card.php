<?php
include("header.php");

$chk = Qry($conn,"SELECT id FROM _access_control WHERE username='$_SESSION[ediary_fix_admin]' AND func_id=(SELECT id FROM 
_access_control_func_list WHERE session_role='8' AND func_name='Trishul_Card') AND u_view='1'");
			  
if(numRows($chk)==0)
{
	echo "<script>window.location.href='./';</script>";
	exit();
}
?>

<script>
$(function() {
		$("#tno").autocomplete({
		source: 'autofill/get_tno.php',
		// appendTo: "#LaneRuleModal",
		select: function (event, ui) { 
              $('#tno').val(ui.item.value);   
             // $("#button1").attr('disabled',false);				   
             return false;
		},
		change: function (event, ui) {
		if(!ui.item){
		    $(event.target).val("");
			alert('Vehicle Number does not exists.');
			$("#tno").val('');
			$("#tno").focus();
			// $("#button1").attr('disabled',true);
		}}, 
		focus: function (event, ui){
			return false;}
});});
</script>

<div class="content-wrapper">
      <section class="content-header">
          <h1 style="font-size:16px;">Manage Trishul Card</h1>
       </section>
       
	   <section class="content">
          <div class="row">
            <div class="col-xs-12">
			<div class="box">
                <div class="box-body">
				
				<div class="col-md-12">&nbsp;</div>
				
				<?php
$chk1 = Qry($conn,"SELECT id FROM _access_control WHERE username='$_SESSION[ediary_fix_admin]' AND func_id=(SELECT id FROM 
_access_control_func_list WHERE session_role='8' AND func_name='Trishul_Card') AND u_insert='1'");
			  
if(numRows($chk1)>0)
{
?>
				<div class="col-md-12">
					<div class="row">
						
						<div class="form-group col-md-3">
							<label style="font-size:12px !important">Vehicle Number <font color="red"><sup>*</sup></font></label>
							<input id="tno" name="tno" oninput="this.value=this.value.replace(/[^a-zA-Z0-9]/,'')" type="text" class="form-control" required />
						</div>
						
						<div class="form-group col-md-2">
							<?php if(!isMobile()) { echo "<label>&nbsp;</label><br />"; } ?>
							<button type="button" style="margin-top:1px;font-size:12px !important" onclick="Search2()" class="btn btn-sm btn-success <?php if(isMobile()) { echo "btn-block"; } ?>" id="add_btn"><i class="fa fa-plus-circle" aria-hidden="true"></i> &nbsp; Add New Card</button>
						</div>
					</div>
				</div>
				<?php
				}
				?>
				<div class="col-md-12">&nbsp;</div>
				
				<div class="col-md-12 table-responsive" id="load_table">
              
				 </div> 
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

<div id="func_result"></div>  
<div id="modal_res1"></div>  

<script>	
function Search2()
{
	var tno = $('#tno').val();
	
	if(tno=='')
	{
		Swal.fire({icon: 'warning',html: '<font size=\'2\' color=\'black\'>Enter vehicle number first !</font>',});
	}
	else
	{
		$('#add_btn').attr('disabled',true);
		$('#loadicon').show();
			jQuery.ajax({
			url: "save_add_new_trishul_card.php",
			data: 'tno=' + tno,
			type: "POST",
			success: function(data) {
				$("#func_result").html(data);
			},
			error: function() {}
		});
	}
}

function RemoveCard(tno)
{
	$('#remove_btn_'+tno).attr('disabled',true);
		$('#loadicon').show();
			jQuery.ajax({
			url: "remove_trishul_card.php",
			data: 'tno=' + tno,
			type: "POST",
			success: function(data) {
				$("#func_result").html(data);
			},
			error: function() {}
	});
}

function LoadData()
{
	$('#loadicon').show();
			jQuery.ajax({
				url: "_load_trishul_card.php",
				data: 'ok=' + 'ok',
				type: "POST",
				success: function(data) {
					$("#load_table").html(data);
					$('#example').DataTable({ 
					"lengthMenu": [ [10, 100, 500, -1], [10, 100,500, "All"] ], 
						"destroy": true, //use for reinitialize datatable
					});
				},
				error: function() {}
		});
}

LoadData();
</script>

<?php include("footer.php") ?>
