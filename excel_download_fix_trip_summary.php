<?php
require_once 'connect.php';

$output = '';

$timestamp=date("Y-m-d H:i:s");

$date = escapeString($conn,$_REQUEST['date']);

	$qry=Qry($conn,"SELECT t.from_station,t.to_station,t.tno,t.lr_type,t.lrno,t.date as trip_date,t.branch,e.name as branch_user 
	FROM dairy.trip AS t 
	LEFT OUTER JOIN emp_attendance AS e ON e.code = t.branch_user 
	WHERE date(t.timestamp)='$date' AND t.fix_lane!=0");
	
	if(!$qry)
	{
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		Redirect("Error while Processing Request.","./");
		exit();
	}
	
	if(numRows($qry)>0)
	{
		$output .= '
		 <table border="1">  
           <tr>  
            <th>Vehicle_No</th>
			<th>Origin</th>
			<th>Destination</th>
			<th>LR_Type</th>
			<th>LR_No</th>
			<th>Branch</th>
			<th>Username</th>
            </tr>';
			
		 while($row = fetchArray($qry))
		  {
		    $output .= '
			<tr>  
				<td>'.$row["tno"].'</td>  
				<td>'.$row["from_station"].'</td>  
				<td>'.$row["to_station"].'</td>  
				<td>'.$row["lr_type"].'</td>  
				<td>'.$row["lrno"].'</td>  
				<td>'.$row["branch"].'</td>  
				<td>'.$row["branch_user"].'</td>  
			</tr>
		   ';
		  }
		  $output .= '</table>';
		  header('Content-Type: application/xls');
		  header('Content-Disposition: attachment; filename=Fix_Lane_Summary.xls');
		  echo $output;
		  mysqli_close($conn);
	}
	else
	{
		 mysqli_close($conn);
		echo "<script>
			alert('NO RESULT FOUND.');
			window.close();
		</script>";
		exit();
	}

?>