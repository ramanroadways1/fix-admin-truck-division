<?php
require_once("./connect.php");

$branch = escapeString($conn,strtoupper($_POST['branch']));

$get_pump = Qry($conn,"SELECT name,code,comp FROM dairy.diesel_pump_own WHERE branch='$branch' AND code!='' AND active='1'");

if(!$get_pump){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while Processing Request","./diesel_add_trip.php");
	exit();
}

if(numRows($get_pump)==0)
{
	echo "<option value='' style='font-size:12px !important'>select pump</option>
	<script>
		$('#entry_save_btn').attr('disabled',true);
		$('#loadicon').fadeOut('slow');
	</script>";
}
else
{
	echo "<option value='' style='font-size:12px !important'>select pump</option>";
	while($row = fetchArray($get_pump))
	{
		echo "<option style='font-size:12px !important' value='$row[code]'>$row[name]-$row[comp]</option>";
	}
	
	echo "<script>
		$('#entry_save_btn').attr('disabled',false);
		$('#loadicon').fadeOut('slow');
	</script>";
}
?>