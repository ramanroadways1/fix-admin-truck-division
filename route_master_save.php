<?php
require_once("connect.php");

$timestamp = date("Y-m-d H:i:s");
$from_id = escapeString($conn,$_POST['from_id']);
$to_id = escapeString($conn,$_POST['to_id']);
$route_type = escapeString($conn,strtolower($_POST['route_type']));
$from_loc = escapeString($conn,strtoupper($_POST['from_loc']));
$to_loc = escapeString($conn,strtoupper($_POST['to_loc']));

echo "<script>$('#add_btn').attr('disabled',true);</script>";

if($route_type!='normal' AND $route_type!='congested')
{
	AlertError("Invalid route type !");
	errorLog("Invalid route type: $route_type !",$conn,$page_url,__LINE__);
	exit();
}
	
$get_from_loc = Qry($conn,"SELECT name FROM station WHERE id='$from_id'");

if(!$get_from_loc){
	AlertError("Error while processing request !");
	errorLog(getMySQLError($conn),$conn,$page_url,__LINE__);
	exit();
}

if(numRows($get_from_loc)==0)
{
	AlertError("From location not found !");
	exit();
}

$get_to_loc = Qry($conn,"SELECT name FROM station WHERE id='$to_id'");

if(!$get_to_loc){
	AlertError("Error while processing request !");
	errorLog(getMySQLError($conn),$conn,$page_url,__LINE__);
	exit();
}

if(numRows($get_to_loc)==0)
{
	AlertError("To location not found !");
	exit();
}

$row_from = fetchArray($get_from_loc);
$row_to = fetchArray($get_to_loc);

if($row_from['name']!=$from_loc)
{
	AlertError("From location is not valid !");
	exit();
}

if($row_to['name']!=$to_loc)
{
	AlertError("To location is not valid !");
	exit();
}
	
$chk_record = Qry($conn,"SELECT id FROM dairy.master_lane WHERE from_id='$from_id' AND to_id='$to_id'");

if(!$chk_record){
	AlertError("Error while processing request !");
	errorLog(getMySQLError($conn),$conn,$page_url,__LINE__);
	exit();
}

if(numRows($chk_record)>0){
	AlertError("Duplicate route found !<br><br><font color=\'maroon\'>$from_loc to $to_loc.</font>");
	echo "<script>$('#add_btn').attr('disabled',false);</script>";
	exit();
}

StartCommit($conn);
$flag = true;
	
$insert_route = Qry($conn,"INSERT INTO dairy.master_lane(from_id,to_id,route_type,branch,branch_user,timestamp) VALUES ('$from_id',
'$to_id','$route_type','Fix-Admin','$_SESSION[ediary_fix_admin]','$timestamp')");
	
if(!$insert_route){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}
	
if($flag)
{
	MySQLCommit($conn);
	closeConnection($conn);
	echo "<script>
	$('.form_route_master').val('');
	$('#add_btn').attr('disabled',false);
	
	$('#loadicon').fadeOut('slow');
		Swal.fire({
		  title: 'Route successfully added !',
		  // text: 'You won\'t be able to revert this !',
		  icon: 'success',
		  // showCancelButton: true,
		  confirmButtonColor: '#3085d6',
		  cancelButtonColor: '#d33',
		  confirmButtonText: 'OK'
		}).then((result) => {
		  LoadTable();
		})
	</script>";
}
else
{
	MySQLRollBack($conn);
	closeConnection($conn);
	AlertError("Error While Processing Request !");
	exit();
}
?>