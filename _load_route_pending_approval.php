<?php
require_once 'connect.php';

$sql = Qry($conn,"SELECT m.id,m.tno,m.branch,m.timestamp,e.name as emp_name,s.name as from_loc,s2.name as to_loc 
FROM dairy.pending_master_lane AS m 
LEFT JOIN station as s ON s.id=m.from_id 
LEFT JOIN station as s2 ON s2.id=m.to_id 
LEFT JOIN emp_attendance as e ON e.code=m.branch_user 
WHERE m.is_approved='0'");

if(!$sql){
	echo getMySQLError($conn);
	errorLog(getMySQLError($conn),$conn,$page_url,__LINE__);
	exit();
}

?>
	<table id="example" class="table table-bordered table-striped" style="font-size:13.5px;">
        <thead>
		<tr>
			<th>#</th>
			<th>From</th>
			<th>To</th>
			<th>Vehicle_no</th>
			<th>Branch</th>
			<th>User</th>
			<th>Timestamp</th>
			<th>Route_type</th>
		</tr>
		</thead>
       <tbody id=""> 
<?php
if(numRows($sql)==0)
{
	echo "<tr>
		<td colspan='8'>No record found !</td>
	</tr>";  
}
else
{
	$sn=1;
	while($row = fetchArray($sql))
	{
		echo "<tr>
			<td>$sn</td>
			<td>$row[from_loc]</td>
			<td>$row[to_loc]</td>
			<td>$row[tno]</td>
			<td>$row[branch]</td>
			<td>$row[emp_name]</td>
			<td>".date("d-m-y H:i A",strtotime($row["timestamp"]))."</td>
			<td id='approval_row_$row[id]'>
				<select class='form-control' id='route_type_$row[id]' style='font-size:12px:width:100%;height:32px !important'>
					<option style='font-size:13px' value=''>--route type--</option>
					<option style='font-size:13px' value='normal'>Normal</option>
					<option style='font-size:13px' value='congested'>Congested/Ghaat</option>
				</select>
			<button type='button' id='btn_save_$row[id]' class='btn btn-sm btn_save_route1 btn-success' onclick='SaveRoute($row[id])'>Save</button></td>
		</tr>
		";
		$sn++;		
     }
}
?>
	</tbody>
		</table>
<script> 
$("#loadicon").fadeOut('slow');
$(document).ready(function() {
    $('#example').DataTable( {
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ]
    } );
} );
</script> 

<?php
$qry_rights_save = Qry($conn,"SELECT id FROM _access_control WHERE username='$_SESSION[ediary_fix_admin]' AND func_id=(SELECT id FROM 
_access_control_func_list WHERE session_role='8' AND func_name='Route_Approval') AND u_update='1'");
							  
if(numRows($qry_rights_save)==0)
{
	echo "<script>
		$('.btn_save_route1').hide();
		$('.btn_save_route1').attr('onclick','');
	</script>";
}
?>