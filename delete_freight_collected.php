<?php
require_once("./connect.php");

$id = escapeString($conn,($_POST['id']));

$get_data = Qry($conn,"SELECT id,trip_id,trans_id,vou_id,tno,amount,date,branch,timestamp FROM dairy.freight_rcvd WHERE id='$id'");

if(!$get_data)
{
	AlertErrorTopRight("Error while processing request !");
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	echo "<script> $('#dlt_f2_btn_$id').attr('disabled',false); </script>";
	exit();
}

if(numRows($get_data) == 0)
{
	AlertErrorTopRight("Entry not found !");
	echo "<script> $('#dlt_f2_btn_$id').attr('disabled',false); </script>";
	exit();
}

$row = fetchArray($get_data);

$tno = $row['tno'];
$trip_id = $row['trip_id'];
$table_id = $row['id'];
$trans_id = $row['trans_id'];
$amount = $row['amount'];
$vou_id = $row['vou_id'];
$trans_date = $row['date'];

require_once("./check_cache.php");

$chk_trip = Qry($conn,"SELECT id FROM dairy.trip WHERE id='$trip_id'");

if(!$chk_trip)
{
	AlertErrorTopRight("Error while processing request !");
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	echo "<script> $('#dlt_f2_btn_$id').attr('disabled',false); </script>";
	exit();
}

if(numRows($chk_trip)==0)
{
	AlertErrorTopRight("Running trip not found !");
	echo "<script> $('#dlt_f2_btn_$id').attr('disabled',false); </script>";
	exit();
}

StartCommit($conn);
$flag = true;

$log_data = "Branch : $branch, Vou_id : $vou_id, TruckNo : $tno, Amount : $amount, Trans_date : $trans_date, Trans_id : $trans_id, Trip_id : $trip_id.";

$insert_log = Qry($conn,"INSERT INTO dairy.ediary_admin_log(code,action,desct,timestamp) VALUES ('$tno','FRTCOL_DELETE','$log_data','$timestamp')");

if(!$insert_log){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}
	
$delete = Qry($conn,"DELETE FROM dairy.freight_rcvd WHERE id='$id'");
	
if(!$delete){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}
	
$sel_cash_id = Qry($conn,"SELECT id,comp FROM cashbook WHERE vou_no = '$trans_id'");

if(!$sel_cash_id){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}
	
if(numRows($sel_cash_id)==0)
{
	$flag = false;
	errorLog("Transaction not found in Cashbook. Branch : $branch. TransId: $trans_id.",$conn,$page_name,__LINE__);
}
	
$row_cash_id = fetchArray($sel_cash_id);
$cash_id = $row_cash_id['id'];
$company = $row_cash_id['comp'];
	
if($company=='RRPL')
{
	$balance='balance';
}
else if($company=='RAMAN_ROADWAYS')
{
	$balance='balance2';
}
else
{
	$flag = false;
	errorLog("Invalid Company Name : $company.",$conn,$page_name,__LINE__);
}	
	
$update_cashbook = Qry($conn,"UPDATE cashbook SET `$balance`=`$balance`-$amount WHERE id>'$cash_id' AND comp='$company' AND user='$branch'");
	
if(!$update_cashbook){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}
	
$query_update = Qry($conn,"update user set `$balance`=`$balance`-'$amount' where username='$branch'");

if(!$query_update){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}
	
if(AffectedRows($conn)==0)
{
	$flag = false;
	errorLog("Branch Balance not updated. Branch : $branch. TransId: $trans_id.",$conn,$page_name,__LINE__);
}

$delete_cashbook = Qry($conn,"DELETE FROM cashbook WHERE id='$cash_id'");
	
if(!$delete_cashbook){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

$delete_from_cr_table = Qry($conn,"DELETE FROM credit WHERE trans_id='$trans_id'");
	
if(!$delete_from_cr_table){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

$select_book_id = Qry($conn,"SELECT id,driver_code FROM dairy.driver_book WHERE trans_id='$trans_id'");
	
if(!$select_book_id){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}
	
if(numRows($select_book_id)==0)
{
	$flag = false;
	errorLog("Transaction not found in driver book. Trans_id: $trans_id.",$conn,$page_name,__LINE__);
}
	
$row_book_id = fetchArray($select_book_id);
	
$book_id = $row_book_id['id'];
$driver_code = $row_book_id['driver_code'];
	
$update_book = Qry($conn,"UPDATE dairy.driver_book SET balance=balance+'$amount' WHERE id>'$book_id' AND tno='$tno'");

if(!$update_book){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}
	
$delete_row = Qry($conn,"DELETE FROM dairy.driver_book WHERE id='$book_id'");
		
if(!$delete_row){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}	
	
if(AffectedRows($conn)==0)
{
	$flag = false;
	errorLog("Transaction not found in driver book. Trans_id: $trans_id. DriverBook Id: $book_id.",$conn,$page_name,__LINE__);
}
	
$update_d_bal = Qry($conn,"UPDATE dairy.driver_up SET amount_hold=amount_hold+'$amount' WHERE down=0 AND code='$driver_code' ORDER BY id DESC LIMIT 1");
	
if(!$update_d_bal){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}	
	
$update_trip = Qry($conn,"UPDATE dairy.trip SET freight_collected=freight_collected-'$amount' WHERE id='$trip_id'");
			
if(!$update_trip){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}	
	
if($flag)
{
		MySQLCommit($conn);
		closeConnection($conn);
		
		AlertRightCornerSuccess("Deleted Successfully !");
		echo "<script>
			ReLoadPage();
		</script>";
		exit();
}
else
{
		MySQLRollBack($conn);
		closeConnection($conn);
	
		AlertErrorTopRight("Error while processing request !");
		echo "<script> $('#dlt_f2_btn_$id').attr('disabled',false); </script>";
		exit();
}
	
?>