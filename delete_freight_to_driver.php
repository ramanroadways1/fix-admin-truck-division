<?php
require_once("./connect.php");

$id = escapeString($conn,($_POST['id']));

$get_data = Qry($conn,"SELECT trip_id,trans_id,vou_id,tno,amount,date,branch,timestamp FROM dairy.freight_adv WHERE id='$id'");

if(!$get_data)
{
	AlertErrorTopRight("Error while processing request !");
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	echo "<script> $('#dlt_f_adv_btn_$id').attr('disabled',false); </script>";
	exit();
}

if(numRows($get_data) == 0)
{
	AlertErrorTopRight("Entry not found !");
	echo "<script> $('#dlt_f_adv_btn_$id').attr('disabled',false); </script>";
	exit();
}		

$row = fetchArray($get_data);

$tno = $row['tno'];

require_once("./check_cache.php");

$trip_id = $row['trip_id'];
$trans_id = $row['trans_id'];
$amount = $row['amount'];
$vou_id = $row['vou_id'];
$trans_date = $row['date'];

$chk_trip = Qry($conn,"SELECT id FROM dairy.trip WHERE id='$trip_id'");

if(!$chk_trip)
{
	AlertErrorTopRight("Error while processing request !");
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	echo "<script> $('#dlt_f_adv_btn_$id').attr('disabled',false); </script>";
	exit();
}

if(numRows($chk_trip)==0)
{
	AlertErrorTopRight("Running trip not found !");
	echo "<script> $('#dlt_f_adv_btn_$id').attr('disabled',false); </script>";
	exit();
}

if(substr($vou_id,3,1)=='M')
{
	// $chk_for_next_entry = Qry($conn,"SELECT id FROM dairy.bilty_book WHERE id>(select min(id) from dairy.bilty_book where trans_id='$trans_id') 
	// and bilty_no='$vou_id'");
	
	// if(!$chk_for_next_entry)
	// {
		// AlertErrorTopRight("Error while processing request !");
		// errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		// echo "<script> $('#dlt_f_adv_btn_$id').attr('disabled',false); </script>";
		// exit();
	// }
	
	// if(numRows($chk_for_next_entry)>0)
	// {
		// AlertErrorTopRight("Market bilty entries found. Delete them first ! Bilty No: $vou_id.");
		// echo "<script> $('#dlt_f_adv_btn_$id').attr('disabled',false); </script>";
		// exit();
	// }
	
	$market_bilty = "1";
}
else
{
	$market_bilty = "0";
}

StartCommit($conn);
$flag = true;

$log_data = "Vou_id : $vou_id, TruckNo : $tno, Amount : $amount, Trans_date : $trans_date, Trans_id : $trans_id, Trip_id : $trip_id.";

$insert_log = Qry($conn,"INSERT INTO dairy.ediary_admin_log(code,action,desct,timestamp) VALUES ('$tno','FRTADV_DELETE','$log_data','$timestamp')");

if(!$insert_log){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}
	
if($market_bilty=="1")
{
	$dlt_bilty_book = Qry($conn,"SELECT id,debit FROM dairy.bilty_book WHERE trans_id='$trans_id'");
	
	if(!$dlt_bilty_book){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
	
	if(numRows($dlt_bilty_book)==0)
	{
		$flag = false;
		errorLog("Transaction not found in BiltyBook. TransId: $trans_id.",$conn,$page_name,__LINE__);
	}
	
	$row_bilty_book = fetchArray($dlt_bilty_book);
	
	$bilty_id = $row_bilty_book['id'];	
	
	$debit_amount = $row_bilty_book['debit'];	
	
	$update_bilty_balance_1 = Qry($conn,"UPDATE dairy.bilty_book SET balance=balance+'$debit_amount' WHERE id>'$bilty_id' AND bilty_no='$vou_id'");
	
	if(!$update_bilty_balance_1){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
	
	$update_bilty_balance = Qry($conn,"UPDATE dairy.bilty_balance SET rcvd=rcvd-'$debit_amount',balance=balance+'$debit_amount' 
	WHERE bilty_no='$vou_id'");
	
	if(!$update_bilty_balance){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
	 
	if(AffectedRows($conn)==0)
	{
		$flag = false;
		errorLog("balance not updated in bilty balance. Vou_No: $vou_id.",$conn,$page_name,__LINE__);
	}
	
	$delete_bilty_book = Qry($conn,"DELETE FROM dairy.bilty_book WHERE id='$bilty_id'");
	
	if(!$delete_bilty_book){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
	
	if(AffectedRows($conn)==0)
	{
		$flag = false;
		errorLog("Transaction not deleted from bilty book. TransId: $trans_id.",$conn,$page_name,__LINE__);
	}
}
	
$delete = Qry($conn,"DELETE FROM dairy.freight_adv WHERE id='$id'");

if(!$delete){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}
	
$select_book_id = Qry($conn,"SELECT id,driver_code FROM dairy.driver_book WHERE trans_id='$trans_id'");

if(!$select_book_id){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}
	
if(numRows($select_book_id)==0)
{
	$flag = false;
	errorLog("Transaction not found in driver book. Trans_id: $trans_id.",$conn,$page_name,__LINE__);
}
	
$row_book_id = fetchArray($select_book_id);
	
$book_id = $row_book_id['id'];
$driver_code = $row_book_id['driver_code'];
	
$update_book = Qry($conn,"UPDATE dairy.driver_book SET balance=balance-'$amount' WHERE tno='$tno' AND id>'$book_id'");

if(!$update_book){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}
	
$delete_row = Qry($conn,"DELETE FROM dairy.driver_book WHERE id='$book_id'");
		
if(!$delete_row){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);	
}
	
if(AffectedRows($conn)==0)
{
	$flag = false;
	errorLog("Transaction not found in driver book. Trans_id: $trans_id. DriverBook Id: $book_id.",$conn,$page_name,__LINE__);
}
	
$update_driver_balance = Qry($conn,"UPDATE dairy.driver_up SET amount_hold=amount_hold-'$amount' WHERE down=0 AND code='$driver_code' 
ORDER BY id DESC LIMIT 1");
	
if(!$update_driver_balance){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);	
}
	
$update_trip = Qry($conn,"UPDATE dairy.trip SET freight=freight-'$amount' WHERE id='$trip_id'");
			
if(!$update_trip){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);	
}		
		
if($flag)
{
	MySQLCommit($conn);
	closeConnection($conn);
	
	AlertRightCornerSuccess("Deleted Successfully !");
	echo "<script>
		ReLoadPage();
	</script>";
	exit();
}
else
{
	MySQLRollBack($conn);
	closeConnection($conn);
	
	AlertErrorTopRight("Error while processing Request !");
	echo "<script> $('#dlt_f_adv_btn_$id').attr('disabled',false); </script>";
	exit();
}
 
// AlertErrorTopRight("ok !");
	// echo "<script> $('#dlt_f_adv_btn_$id').attr('disabled',false); </script>";
	// exit();
?>