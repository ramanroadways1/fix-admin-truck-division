<?php
require_once("./connect.php");

$timestamp = date("Y-m-d H:i:s");
$location = escapeString($conn,strtoupper($_POST['location_name']));
$state = escapeString($conn,strtoupper($_POST['state']));
$pincode_db = escapeString($conn,($_POST['pincode_db']));

$lat = escapeString($conn,($_POST['lat']));
$long = escapeString($conn,($_POST['lng']));

if(strlen($pincode_db)!=6)
{
	AlertErrorTopRight("Pincode not found !");
	echo "<script>$('#add_btn').attr('disabled', false);</script>";
	exit();
}

if($lat=='' || $long=='')
{
	AlertErrorTopRight("Google location not found !");
	echo "<script>$('#add_btn').attr('disabled', false);</script>";
	exit();
}

$chk_location = Qry($conn,"SELECT id FROM station WHERE name='$location'");

if(!$chk_location){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	AlertErrorTopRight("Error while processing request !");
	echo "<script>$('#add_btn').attr('disabled', false);</script>";
	exit();
}
	
if(numRows($chk_location)>0)
{
	AlertErrorTopRight("Duplicate location found ");
	echo "<script>$('#add_btn').attr('disabled', false);</script>";
	exit();
}

// $get_pincode = getZipcode($lat.",".$long);

// if(strlen($get_pincode)!=6)
// {
	// AlertErrorTopRight("Unable to fetch pincode !");
	// echo "<script>$('#add_btn').attr('disabled', false);</script>";
	// exit();
// }
	
// $pincode = $get_pincode;
$pincode = $pincode_db;

$insert = Qry($conn,"INSERT INTO station (name,state,_lat,_long,pincode,timestamp) VALUES ('$location','$state','$lat','$long','$pincode','$timestamp')");

if(!$insert){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	AlertErrorTopRight("Error while processing request !");
	echo "<script>$('#add_btn').attr('disabled', false);</script>";
	exit();
}

AlertRightCornerSuccess("Added Successfully !");
echo "<script>$('#add_btn').attr('disabled', false);$('#Form1')[0].reset();$('#searchInput').attr('readonly',false);</script>";
?>