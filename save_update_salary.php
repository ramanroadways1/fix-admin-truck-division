<?php
require_once("./connect.php");

$driver_code = escapeString($conn,($_POST['driver_code']));
$salary_array = escapeString($conn,($_POST['salary']));

$salary_amount = explode("_",$salary_array)[0];
$salary_type = explode("_",$salary_array)[1];

$timestamp = date("Y-m-d H:i:s");

if($salary_type=="1"){
	$salary_desc = "$salary_amount/trip";
}else{
	$salary_desc = "$salary_amount/month";
}

echo "<script>$('#submit_btn_salary_update').attr('disabled',true);</script>";
	
$chk_record = Qry($conn,"SELECT id,salary_amount,salary_type FROM dairy.driver_up WHERE down=0 AND code='$driver_code' ORDER BY id DESC LIMIT 1");

if(!$chk_record){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	AlertErrorTopRight("Error while Processing Request !");
	echo "<script>$('#submit_btn_salary_update').attr('disabled',false);</script>";
	exit();
}

if(numRows($chk_record)==0)
{
	AlertErrorTopRight("Active driver not found !");
	echo "<script>$('#submit_btn_salary_update').attr('disabled',false);</script>";
	exit();
}

$row = fetchArray($chk_record);

$driver_record_id = $row['id'];

if($row['salary_amount']==$salary_amount AND $row['salary_type']==$salary_type)
{
	AlertErrorTopRight("Nothing to update !");
	echo "<script>$('#submit_btn_salary_update').attr('disabled',false);</script>";
	exit();
}

$log_data = "SALARY Updated. $row[salary_amount] to $salary_amount.";

StartCommit($conn);
$flag = true;

$updateQry = Qry($conn,"UPDATE dairy.driver_up SET salary_amount='$salary_amount',salary_type='$salary_type' WHERE id='$driver_record_id'");

if(!$updateQry){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

if($flag)
{
	MySQLCommit($conn);
	closeConnection($conn);
	AlertRightCornerSuccess("Updated Successfully !");
	echo "<script>
		$('#sal_update_modal_cls_btn')[0].click();
		$('#salary_row_$driver_code').html('$salary_desc');
		$('#submit_btn_salary_update').attr('disabled',false);
	</script>";
	exit();
}
else
{
	MySQLRollBack($conn);
	closeConnection($conn);
	AlertErrorTopRight("Error while Processing Request !");
	echo "<script>$('#submit_btn_salary_update').attr('disabled',false);</script>";
	exit();
}	
?>