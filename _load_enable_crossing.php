<?php
require_once 'connect.php';

$lrno = escapeString($conn,($_POST['lrno']));

if($lrno=='')
{
	echo "<br><br><center>LR number not found !</center>";
	exit();
}

?>
<head>
	<meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate" />
	<meta http-equiv="Pragma" content="no-cache" />
	<meta http-equiv="Expires" content="0" />
	<meta name="robots" content="noindex,nofollow"/>
	<title>RAMAN ROADWAYS PVT. LTD. || A RAMAN GROUP OF COMPANY.</title>
	<link rel="icon" type="image/png" href="../b5aY6EZzK52NA8F/favicon.png" />
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
	<script src="//code.jquery.com/jquery-1.10.2.js"></script>
	<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>  
	<script src="../b5aY6EZzK52NA8F/js/lumino.glyphs.js"></script>  
	<link rel="stylesheet" href="../b5aY6EZzK52NA8F/font-awesome-4.7.0/css/font-awesome.min.css">
	<link href="../b5aY6EZzK52NA8F/google_font.css" rel="stylesheet">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.1/css/bootstrap-select.css" />
	
	<script src="../diary/sweet_alert_lib/dist/sweetalert2.min.js"></script>
	<link rel="stylesheet" href="../diary/sweet_alert_lib/dist/sweetalert2.min.css">
</head>

<style>
@media (min-width: 768px) {
  .modal-xl {
    width: 90%;
   max-width:1200px;
  }
}

body {
    overflow-x: auto;
}

.ui-autocomplete { z-index:2147483647; }

::-webkit-scrollbar{
    width: 6px;
	height:6px;
}
::-webkit-scrollbar-track {
    -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.2); 
    border-radius: 5px;
}
::-webkit-scrollbar-thumb {
    border-radius: 5px;
    -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.9); 
}

.ui-widget {
  font-family: Verdana;
  font-size: 12px !important;
}

input{
	font-size:12px !important;
}
</style>

<script>
function CrossingUpdate(lrno,id,trip_check,frno,trip_id)
{
		var cross_stn  = $('#cross_stn'+id).val();
		var cross_sel  = $('#cross_sel'+id).val();
		var cross_stn_id  = $('#cross_stn_id'+id).val();
		
		if(cross_sel=='YES' && cross_stn=='')
		{
			Swal.fire({icon: 'warning',html: '<font size=\'2\' color=\'black\'>nter crossing station first !</font>',});
		}
		else
		{		
			$("#loadicon").show();
			jQuery.ajax({
			url: "save_enable_crossing.php",
			data: 'lrno=' + lrno + '&cross_stn=' + cross_stn + '&cross_sel=' + cross_sel + '&id=' + id + '&trip_check=' + trip_check + '&frno=' + frno + '&trip_id=' + trip_id + '&to_id=' + cross_stn_id,
			type: "POST",
			success: function(data) {
				$("#func_result").html(data);
			},
			error: function() {}
			});
		}	
}
</script>

<div id="func_result"></div>

<body style="font-family: 'Open Sans', sans-serif !important">
<br />
<br />
<div class="container-fluid">
	<div class="row">
		<div class="form-group col-md-12">
			<h4>Enable Crossing : LR No: <?php echo $lrno; ?></h4>
		</div>
	</div>
	<div class="row">
		<div class="form-group col-md-12 table-responsive">
<?php
$qry = Qry($conn,"SELECT f.id,f.frno,f.branch,f.date,f.truck_no,f.fstation,f.tstation,f.consignee,f.crossing,f.cross_to,f.cross_stn_id,
f.done,f.trip_id,f.oxygen_lr,l.tstation as lr_dest 
FROM freight_form_lr AS f 
LEFT OUTER JOIN lr_sample AS l ON l.lrno = f.lrno 
WHERE f.lrno='$lrno' ORDER BY f.id ASC");

if(!$qry){
	echo getMySQLError($conn);
	errorLog(getMySQLError($conn),$conn,$page_url,__LINE__);
	exit();
}
	
if(numRows($qry)==0)
{
	AlertRightCornerError("No record found !");
	echo "<b><font color='red'>No result found.. !</font></b>";
	exit();
}
else if(numRows($qry)>1)
{
	$crossing_time = numRows($qry)-1;
	
	if($crossing_time>1){
		$times_text="times";
	}
	else{
		$times_text="time";
	}
	
	echo "<b><font color='red'>already crossed $crossing_time $times_text !</font></b>";
	
	$sn=1;
	
	echo "<table id='example' class='table table-bordered table-striped' style='font-size:12px !important'>
	<thead>
	<tr>
		<th>Vou_No</th>
		<th>Branch</th>
		<th>LRdate</th>
		<th>TruckNo</th>
		<th>From</th>
		<th>To</th>
		<th>LR_Dest.</th>
		<th>Crossing</th>
		<th>Cross.Stn</th>
		<th>#</th>
	</tr>
	</thead>
	
	<tbody>";
	
	while($row=fetchArray($qry))
	{
		echo "
			<tr>
				<td>$row[frno]</td>
				<td>$row[branch]</td>
				<td>$row[date]</td>
				<td>$row[truck_no]</td>
				<td>$row[fstation]</td>
				<td>$row[tstation]</td>
				<td style='color:red'>$row[lr_dest]</td>";
					
		if(numRows($qry)==$sn)
		{
				?>
			<script>
			 $(function() {
				$("#cross_stn<?php echo $row['id']; ?>").autocomplete({
				  source: '../b5aY6EZzK52NA8F/autofill/get_location.php',
				  change: function (event, ui) {
					if(!ui.item){
						$(event.target).val("");
						$(event.target).focus();
						$("#cross_stn_id<?php echo $row['id']; ?>").val("");
						alert('Station not Found Contact Head-Office.');
					}
				}, 
				select: function (event, ui) {
					$("#cross_stn<?php echo $row['id']; ?>").val(ui.item.value);  
					$("#cross_stn_id<?php echo $row['id']; ?>").val(ui.item.id);		
					$("#button1<?php echo $row['id']; ?>").attr("disabled",false);
					return false;
				}
				});
			  });	  
			</script>
			<?php
			if($row['crossing']=='YES'){
				$opt1="selected";
				$opt2="";
			}
			else{
				$opt1="";
				$opt2="selected";
			}
				
			if($row['oxygen_lr']=="1")
			{
				echo "
				<td colspan='3'><font color='red'>Oxygen LR</font></td>
				<td></td>
				<td></td>";
			}
			else
			{
				echo "<td>
					<select id='cross_sel$row[id]' style='font-size:12px !important;height:28px !important' class='form-control'>
						<option style='font-size:12px !important' $opt1 value='YES'>YES</option>
						<option style='font-size:12px !important' $opt2 value='NO'>NO</option>
					</select>
				</td>
								
				<td>
					<input type='text' style='font-size:12px !important;height:28px !important' class='form-control crossing_stn_get' id='cross_stn$row[id]' value='$row[cross_to]'>
					<input type='hidden' class='form-control' id='cross_stn_id$row[id]' value='$row[cross_stn_id]'>
				</td>
							
				<td><button type='button' id='button1$row[id]' onclick=CrossingUpdate('$lrno','$row[id]','$row[done]','$row[frno]','$row[trip_id]') class='btn btn_update btn-danger btn-sm'>Update</button></td>";
			}
		}
		else
		{
			echo "<td>$row[crossing]</td>
			<td>$row[cross_to]</td>
			<td></td>";
		}
				
		echo "
		</tbody></tr>";
		$sn++;
	}	
	
	echo "</table>";
}
else if(numRows($qry)==1)
{
	$sn=1;
	
	echo "
	<table id='example' class='table table-bordered table-striped' style='font-size:12px !important'>
	<thead>
	<tr>
		<th>Vou_No</th>
		<th>Branch</th>
		<th>LR Date</th>
		<th>Truck No</th>
		<th>From</th>
		<th>To</th>
		<th>LR_Dest.</th>
		<th>Crossing</th>
		<th>Crossing Station</th>
		<th>#</th>
	</tr>
	</thead>	
	
	<tbody>
	";
	
	while($row=fetchArray($qry))
	{
		?>
	<script>
	 $(function() {
		$("#cross_stn<?php echo $row['id']; ?>").autocomplete({
		  source: '../b5aY6EZzK52NA8F/autofill/get_location.php',
		  change: function (event, ui) {
			if(!ui.item){
				$(event.target).val("");
				$(event.target).focus();
				$("#cross_stn_id<?php echo $row['id']; ?>").val("");
				alert('Station not Found Contact Head-Office.');
			}
		}, 
		select: function (event, ui) {
			$("#cross_stn<?php echo $row['id']; ?>").val(ui.item.value);  
			$("#cross_stn_id<?php echo $row['id']; ?>").val(ui.item.id);		
			$("#button1<?php echo $row['id']; ?>").attr("disabled",false);
			return false;
		}
		});
	  });	  
	</script>
		<?php
		
		if($row['crossing']=='YES')
		{
			$opt1="selected";
			$opt2="";
		}
		else
		{
			$opt1="";
			$opt2="selected";
		}
		
		echo "
		<tr>
				<td>$row[frno]</td>
				<td>$row[branch]</td>
				<td>".convertDate("d/m/y",$row['date'])."</td>
				<td>$row[truck_no]</td>
				<td>$row[fstation]</td>
				<td>$row[tstation]</td>
				<td style='color:red'>$row[lr_dest]</td>
				";
			if($row['oxygen_lr']=="1")
			{
				echo "<td colspan='3'><font color='red'>Oxygen LR</font></td>
				<td></td>
				<td></td>
				";
			}	
			else			
			{
				echo "
					<td>
						<select style='font-size:12px !important;height:28px !important' id='cross_sel$row[id]' class='form-control'>
							<option style='font-size:12px !important;' $opt1 value='YES'>YES</option>
							<option style='font-size:12px !important;' $opt2 value='NO'>NO</option>
						</select>
					</td>
					<td>
						<input type='text' style='font-size:12px !important;height:28px !important' class='form-control crossing_stn_get' id='cross_stn$row[id]' value='$row[cross_to]'>
						<input type='hidden' class='form-control' id='cross_stn_id$row[id]' value='$row[cross_stn_id]'>
					</td>
					<td><button type='button' id='button1$row[id]' onclick=CrossingUpdate('$lrno','$row[id]','$row[done]','$row[frno]','$row[trip_id]') class='btn btn_update btn-danger btn-sm'>Update</button></td>
				";
			}
			echo "</tr>";
		}
	$sn++;
}	
	echo "</thead></table>";
?>

</div>	
</div>	
</div>	
<script> 
	$("#loadicon").fadeOut('slow');
</script> 

<?php
$chk = Qry($conn,"SELECT id FROM _access_control WHERE username='$_SESSION[ediary_fix_admin]' AND func_id=(SELECT id FROM 
_access_control_func_list WHERE session_role='8' AND func_name='Enable_Crossing') AND u_update='1'");
			  
if(numRows($chk) == 0)
{
	echo "<script>
		$('.btn_update').attr('disabled',true);
		$('.btn_update').hide();
	</script>";
}
?>