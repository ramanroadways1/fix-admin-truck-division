<?php
include("header.php");

$chk = Qry($conn,"SELECT id FROM _access_control WHERE username='$_SESSION[ediary_fix_admin]' AND func_id=(SELECT id FROM 
_access_control_func_list WHERE session_role='8' AND func_name='Manage_Own_LR') AND u_view='1'");
			  
if(numRows($chk)==0)
{
	echo "<script>window.location.href='./';</script>";
	exit();
}
?>
<script>
$(function() {
		$("#own_tno").autocomplete({
		source: '../diary/autofill/own_tno.php',
		// appendTo: '#appenddiv',
		select: function (event, ui) { 
            $('#own_tno').val(ui.item.value);   
            return false;},
		change: function (event, ui) {
		if(!ui.item){
			$(event.target).val("");
            $(event.target).focus();
			$('#own_tno').val("");   
			alert('Vehicle does not exists.');
		}}, 
	focus: function (event, ui){
	return false;
	}
});});
</script>

<div class="content-wrapper">
      <section class="content-header">
          <h1 style="font-size:16px;">Manage : Own Vehicle LR </h1>
       </section>
       
	   <section class="content">
          <div class="row">
            <div class="col-xs-12">
			<div class="box">
                <div class="box-body">
				
				<div class="col-md-12">&nbsp;</div>
				
				<div class="col-md-12">
					<div class="row">
						
						<div class="form-group col-md-3">
							<label style="font-size:12px !important">LR Number</label>
							<input style="font-size:12px !important" autocomplete="off" oninput="this.value=this.value.replace(/[^A-Z .a-z0-9]/,'');CheckInput('lrno');" type="text" class="form-control" id="lrno" />
						</div>
						
						<div class="form-group col-md-1">
							<label>&nbsp;</label>
							<?php if(!isMobile()) { echo "<br />"; } ?>
							<span style="font-size:12px !important;color:maroon">-- OR --</span>
						</div>
						
						<div class="form-group col-md-3">
							<label style="font-size:12px !important">Vehicle No. <sup style="color:red">(Own Vehicle)</sup></label>
							<input style="font-size:12px !important" autocomplete="off" type="text" oninput="this.value=this.value.replace(/[^A-Za-z0-9]/,'');CheckInput('tno');" class="form-control" id="own_tno" />
						</div>
						
						<div class="form-group col-md-3">
							<label style="font-size:12px !important">Date range.</label>
							<select style="font-size:12px !important" name="duration" id="duration" class="form-control" required>
								<option style="font-size:12px !important" value="">---SELECT---</option>
								<option style="font-size:12px !important" value="-0 days">Today's</option>
								<option style="font-size:12px !important" value="-1 days">Last 2 days</option>
								<option style="font-size:12px !important" value="-4 days">Last 5 days</option>
								<option style="font-size:12px !important" value="-6 days">Last 7 days</option>
								<option style="font-size:12px !important" value="-9 days">Last 10 days</option>
								<option style="font-size:12px !important" value="-14 days">Last 15 days</option>
								<option style="font-size:12px !important" value="-29 days">Last 30 days</option>
								<option style="font-size:12px !important" value="-59 days">Last 60 days</option>
								<option style="font-size:12px !important" value="-89 days">Last 90 days</option>
								<option style="font-size:12px !important" value="-119 days">Last 120 days</option>
								<option style="font-size:12px !important" value="FULL">FULL REPORT</option>
							</select>
						</div>
						
						<div class="form-group col-md-2">
							<?php if(!isMobile()) { echo "<label>&nbsp;</label><br />"; } ?>
							<button type="button" style="margin-top:1px;font-size:12px !important" onclick="Search()" class="btn btn-sm btn-success <?php if(isMobile()) { echo "btn-block"; } ?>" id="add_btn"><i class="fa fa-search" aria-hidden="true"></i> &nbsp; Search</button>
						</div>
					</div>
				</div>
				
<script>				
function CheckInput(elem)
{
	if(elem=='tno')
	{
		$('#lrno').val('');
	}
	else if(elem=='lrno')
	{
		$('#own_tno').val('');
		$('#duration').val('');
	}
}
</script>				
				
				<div class="col-md-12">&nbsp;</div>
				
				<div class="col-md-12 table-responsive" id="load_table">
              
				 </div> 
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

<div id="func_result"></div>  
<div id="modal_load_res"></div>  

<script>	
function Search()
{
	var tno = $('#own_tno').val();
	var lrno = $('#lrno').val();
	var duration = $('#duration').val();
	
	if(lrno=='' && (tno=='' || duration==''))
	{
		Swal.fire({icon: 'warning',html: '<font size=\'2\' color=\'black\'>Atleast one field is required !</font>',});
	}
	else
	{
		$('#loadicon').show();
			jQuery.ajax({
				url: "_load_manage_own_lr.php",
				data: 'tno=' + tno + '&duration=' + duration + '&lrno=' + lrno,
				type: "POST",
				success: function(data) {
					$("#load_table").html(data);
					$('#example').DataTable({ 
					"lengthMenu": [ [50, 100, 500, -1], [50, 100,500, "All"] ], 
						"destroy": true, //use for reinitialize datatable
					});
				},
				error: function() {}
		});
	}
}

function EditVoucher(vou_no)
{
	$('#vou_no_1').val(vou_no);
	$('#FormEditLR')[0].submit();
}

function DeleteVoucher(vou_no)
{
	jQuery.ajax({
		url: "delete_fm_olr_voucher.php",
		data: 'vou_no=' + vou_no,
		type: "POST",
		success: function(data) {
			$("#func_result").html(data);
		},
		error: function() {}
	});
}
</script>

<form id="FormEditLR" action="./edit_own_lr_full.php" target="_blank" method="POST">
	<input type="hidden" name="vou_no" id="vou_no_1">
</form>

<?php include("footer.php") ?>
