<?php
require_once("connect.php");

$id = escapeString($conn,$_POST['id']);

$get_data = Qry($conn,"SELECT trip_id,trans_id,exp_name,exp_code,copy,tno,amount,date,branch,narration,timestamp FROM dairy.trip_exp WHERE id='$id'");

if(!$get_data)
{
	AlertErrorTopRight("Error while processing request !");
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	exit();
}

if(numRows($get_data) == 0)
{
	AlertErrorTopRight("Entry not found !");
	exit();
}

$row = fetchArray($get_data);

$tno = $row['tno'];
$trip_id = $row['trip_id'];
$trans_id = $row['trans_id'];
$exp_name = $row['exp_name'];
$exp_code = $row['exp_code'];
$amount = $row['amount'];
$trans_date = $row['date'];
$narration = $row['narration'];
$exp_attachment = $row['copy'];

if($exp_attachment != ''){
	$disable_exp = "disabled";
}else{
	$disable_exp = "";
}
?>

<button id="exp_modal_button" style="display:none" data-toggle="modal" data-target="#ExpModal"></button>

<form id="EditExpForm" autocomplete="off" style="font-size:12px !important">   
<div class="modal fade" id="ExpModal" role="dialog" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-md">
      <div class="modal-content" style="">
		
		<div class="modal-header bg-primary">
			Edit Expense :
		  </div>
	  
		<div class="modal-body">
		<div class="row">
			
			<div class="form-group col-md-6">
				<label>Vehicle No. <sup><font color="red">*</font></sup></label>
				<input style="font-size:12px !important" type="text" name="tno" value="<?php echo $tno; ?>" class="form-control" readonly required />
			</div>
			
			<div class="form-group col-md-6">
				<label>Amount. <sup><font color="red">*</font></sup></label>
				<input style="font-size:12px !important" type="number" value="<?php echo $amount; ?>"  min="1" name="amount" id="exp_amount" class="form-control" required />
			</div>
			
			<input type="hidden" name="id" value="<?php echo $id; ?>" />
			
			<script>
			function ExpenseSel(elem)
			{
				$('#rto_upload').val('');
				
				if(elem=='9')
				{
					$('#rto_upload').attr('required',true);
					$('#rto_upload_div').show();
				}
				else
				{
					$('#rto_upload').attr('required',false);
					$('#rto_upload_div').hide();
				}
			}
			</script>
			
			<div class="form-group col-md-12">
				<label>Select Expense. <sup><font color="red">*</font></sup></label>
				<select style="font-size:12px !important" name="expense" onchange="ExpenseSel(this.value)" class="form-control" required>
				<option <?php echo $disable_exp; ?> style="font-size:12px !important" value="">--select expense--</option>
				<?php
				$fetch_exp = Qry($conn,"SELECT id,exp_code,name FROM dairy.exp_head ORDER BY name ASC");
				
				if(numRows($fetch_exp)>0)
				{
					while($row_exp=fetchArray($fetch_exp))
					{
					?>
						<option <?php echo $disable_exp; ?> <?php if($row_exp['exp_code'] == $exp_code AND $row_exp['name'] == $exp_name) { echo "selected"; } ?> 
						style="font-size:12px !important" value="<?php echo $row_exp['id']; ?>"><?php echo $row_exp['name']; ?></option>
					<?php
					}					
				}
				?>
				</select>
			</div>
			
			<div id="rto_upload_div" style="display:none" class="form-group col-md-12">
				<label>Upload RTO Receipt. <sup><font color="red">* (multiple upload support)</font></sup></label>
				<input style="font-size:12px !important" type="file" name="rto_upload[]" accept=".pdf,.jpg,.jpeg,.png" multiple="multiple" required style="padding-bottom:28px;" id="rto_upload" class="form-control" />
			</div>
			
			<div class="form-group col-md-12">
				<label>Txn Date <sup><font color="red">*</font></sup></label>
				<input value="<?php echo $trans_date; ?>" max="<?php echo date("Y-m-d"); ?>" style="font-size:12px !important" name="trans_date" type="date" class="form-control" required pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}" />
			</div>
			
			<div class="form-group col-md-12">
				<label>Narration. <sup><font color="red">*</font></sup></label>
				<textarea style="font-size:12px !important" class="form-control" name="narration" required><?php echo $narration; ?></textarea>
			</div>
		</div>
        </div>
        <div class="modal-footer">
          <button type="submit" id="edit_exp_button" class="btn btn-sm btn-danger">Update</button>
          <button type="button" class="btn-sm btn btn-primary" id="modal_exp_hide" data-dismiss="modal">Close</button>
		</div>
      </div>
    </div>
  </div>
 </form>

 <script> 
$('#exp_modal_button')[0].click();
$('#loadicon').fadeOut('slow');
</script> 

<script type="text/javascript">
$(document).ready(function (e) {
$("#EditExpForm").on('submit',(function(e) {
$("#loadicon").show();
$("#edit_exp_button").attr("disabled", true);
e.preventDefault();
	$.ajax({
	url: "./save_expense_edit.php",
	type: "POST",
	data:  new FormData(this),
	contentType: false,
	cache: false,
	processData:false,
	success: function(data){
		$("#result_edits").html(data);
	},
	error: function() 
	{} });}));});
</script> 