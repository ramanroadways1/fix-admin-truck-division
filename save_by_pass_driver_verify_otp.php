<?php
require_once("./connect.php");

$driver_id = escapeString($conn,strtoupper($_POST['driver_id']));
$date1 = escapeString($conn,($_POST['date1']));
$timestamp = date("Y-m-d H:i:s");

if(empty($driver_id))
{
	AlertErrorTopRight("Driver not found !");
	exit();
}

if(empty($date1) || $date1==0)
{
	AlertErrorTopRight("Date not found !");
	exit();
}

$fetch_data = Qry($conn,"SELECT last_verify FROM dairy.driver WHERE id='$driver_id'");

if(!$fetch_data){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	AlertErrorTopRight("Error while Processing Request !");
	exit();
}

if(numRows($fetch_data)==0)
{
	AlertErrorTopRight("Driver not found !");
	exit();
}

$row = fetchArray($fetch_data);
		
if($row['last_verify']!='' AND $row['last_verify']!=0)
{
	if(strtotime($row['last_verify']) > strtotime($date1))
	{
		AlertErrorTopRight("Driver verified already !");
		exit();
	}
}
		
StartCommit($conn);
$flag = true;
		
$update = Qry($conn,"UPDATE dairy.driver SET last_verify='$date1' WHERE id='$driver_id'");

if(!$update){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}
		
if($flag)
{
	MySQLCommit($conn);
	closeConnection($conn);
		
	echo "<script>
		alert('Update success !');
		$('#loadicon').fadeOut('slow');
	</script>";
	exit();
}
else
{
	MySQLRollBack($conn);
	closeConnection($conn);
	AlertErrorTopRight("Error while Processing Request !");
	exit();
}
?>