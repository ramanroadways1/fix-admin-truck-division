<?php
require_once("./connect.php");

$tno = escapeString($conn,strtoupper($_POST['tno']));
$timestamp = date("Y-m-d H:i:s");

$chk_card = Qry($conn,"SELECT trishul_card FROM dairy.own_truck WHERE tno='$tno'");

if(!$chk_card){
	AlertRightCornerError("Error while Processing Request !");
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	echo "<script>$('#add_btn').attr('disabled',false);</script>";
	exit();
}
	
if(numRows($chk_card)==0)
{
	AlertRightCornerError("Vehicle not found !");
	echo "<script>$('#add_btn').attr('disabled',false);</script>";
	exit();
}

$row_veh =  fetchArray($chk_card);

if($row_veh['trishul_card']=="1")
{
	AlertRightCornerError("Card already active on this vehicle !");
	echo "<script>$('#add_btn').attr('disabled',false);</script>";
	exit();
}

StartCommit($conn);
$flag = true;
	
$insert_record = Qry($conn,"UPDATE dairy.own_truck SET trishul_card='1',trishul_active_from='$timestamp' WHERE tno='$tno'");
	
if(!$insert_record){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}
	
$log_data = "Trishul Active";
	
$insert_log = Qry($conn,"INSERT INTO dairy.ediary_admin_log(code,action,timestamp) VALUES ('$tno','Trishul_Active','$timestamp')");	
	
if(!$insert_log){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}
	
if($flag)
{
	MySQLCommit($conn);
	closeConnection($conn);
	AlertRightCornerSuccess("Card Added Successfully !");
	echo "<script>
		LoadData();
		$('#add_btn').attr('disabled',false);
	</script>";
	exit();
}
else
{
	MySQLRollBack($conn);
	AlertRightCornerError("Error while Processing Request !");
	echo "<script>$('#add_btn').attr('disabled',false);</script>";
	closeConnection($conn);
	exit();
}
?>