<?php
require_once("./connect.php");

$date = date("Y-m-d");

$id = escapeString($conn,($_POST['trip_id']));

$qry_trip = Qry($conn,"SELECT tno,edit_branch,from_station,to_station,from_id,from_poi,to_poi,to_id,from_pincode,to_pincode,pincode,act_wt,charge_wt,lr_type,km,lrno,date(date) as start_date,
date(end_date) as end_date,status,active_trip,freight,cash,cheque,rtgs,diesel,diesel_qty,freight_collected,driver_naame,expense,pod 
FROM dairy.trip WHERE id='$id'");

if(!$qry_trip){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while Processing Request.","./");
	exit();
}
	
if(numRows($qry_trip)==0)
{
	
}

$row_trip = fetchArray($qry_trip);

$from_station  = $row_trip['from_station'];
$to_station  = $row_trip['to_station'];
$from_id  = $row_trip['from_id'];
$to_id  = $row_trip['to_id'];
$from_pincode  = $row_trip['from_pincode'];
$to_pincode  = $row_trip['to_pincode'];
$trip_km  = $row_trip['km'];
$tno = $row_trip['tno'];
$from_poi = $row_trip['from_poi'];
$to_poi = $row_trip['to_poi'];
	
	if($row_trip['lr_type']=='EMPTY' || $row_trip['lr_type']=='CON20' || $row_trip['lr_type']=='CON40' || $row_trip['lr_type']=='ATLOADING')
	{
		$lock_field="";
	}
	else
	{
		$lock_field="readonly";
	}
?>

<title>Edit Trip : <?php echo $tno; ?></title>

<meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate" />
	<meta http-equiv="Pragma" content="no-cache" />
	<meta http-equiv="Expires" content="0" />
	<meta name="robots" content="noindex,nofollow"/>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>  
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link href="https://fonts.googleapis.com/css?family=Open+Sans:600&display=swap" rel="stylesheet">

<script src="../diary/sweet_alert_lib/dist/sweetalert2.min.js"></script>
<link rel="stylesheet" href="../diary/sweet_alert_lib/dist/sweetalert2.min.css">
<script src="https://kit.fontawesome.com/d5ec20b4be.js" crossorigin="anonymous"></script>

<div id="loadicon" style="display:none;position: fixed; right: 0px; top: 0px; width: 100%;height: 100%; background-color:#FFF; z-index: 30001; opacity:1; cursor: wait">
	<center><img style="margin-top:140px" src="./loading_truck1.gif" /><br><br><span style="letter-spacing:1px;font-weight:bold;font-size:14px">कृप्या प्रतीक्षा करे ..</span></center>
</div> 
	
<style>
.ui-autocomplete {
  z-index: 215000000 !important;
  font-family: Verdana !important;
  font-size:12px !important;
}

input{
	font-size:12px !important;
}

input[type='date'] { font-size: 12px !important;}
input[type='text'] { font-size: 12px !important;}
select { font-size: 12px !important; }
textarea { font-size: 12px !important; }

select,select>option{
	font-size:12px !important;
}

::-webkit-scrollbar{
    width:4px;
    height:4px;
}
::-webkit-scrollbar-track {
    -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.2); 
    border-radius: 5px;
}
::-webkit-scrollbar-thumb {
    border-radius: 5px;
    -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.9); 
}

.ui-widget {
  font-family: Verdana !important;
  font-size: 10px !important;
}
</style>
	  
<body style="font-family: 'Open Sans', sans-serif !important" onkeypress="return disableCtrlKeyCombination(event);" onkeydown = "return disableCtrlKeyCombination(event);">

<script>
$(function() {
		$("#from_station").autocomplete({
		source: '../b5aY6EZzK52NA8F/autofill/get_location.php',
		// appendTo: '#appenddiv',
		select: function (event, ui) { 
			$('#trip_update_button').attr('disabled',true)
            $('#from_station').val(ui.item.value);   
            $('#from_id1').val(ui.item.id);      
            $('#from_pincode').val(ui.item.pincode);      
            $('#from_lat_long').val(ui.item.lat_lng);      
            return false;},
		change: function (event, ui) {
		if(!ui.item){
			$(event.target).val("");
            $(event.target).focus();
			$('#from_station').val("");   
			$('#from_id1').val("");   
			$('#from_pincode').val("");   
			$('#from_lat_long').val("");   
			alert('Location does not exists.');
		}}, 
	focus: function (event, ui){
	return false;
	}
});});

$(function() {
		$("#to_station").autocomplete({
		source: '../b5aY6EZzK52NA8F/autofill/get_location.php',
		// appendTo: '#appenddiv',
		// $('#trip_update_button').attr('disabled',true)
		select: function (event, ui) { 
			$('#trip_update_button').attr('disabled',true)
            $('#to_station').val(ui.item.value);   
            $('#to_id1').val(ui.item.id);      
            $('#to_pincode').val(ui.item.pincode);      
            $('#to_lat_long').val(ui.item.lat_lng);      
            return false;},
		change: function (event, ui) {
		if(!ui.item){
			$(event.target).val("");
            $(event.target).focus();
			$('#to_station').val("");   
			$('#to_id1').val("");   
			$('#to_pincode').val("");   
			$('#to_lat_long').val("");   
			alert('Location does not exists.');
		}}, 
	focus: function (event, ui){
	return false;
	}
});});
</script>


<script type="text/javascript">
$(document).ready(function (e) {
$("#UpdateTrip").on('submit',(function(e) {
$("#trip_update_button").attr("disabled", true);
$("#load_button").show();
$("#edit_btn_icon").hide();
$("#button_name").html("Please wait..");
e.preventDefault();
	$.ajax({
	url: "./save_edit_trip.php",
	type: "POST",
	data:  new FormData(this),
	contentType: false,
	cache: false,
	processData:false,
	success: function(data){
		$("#result_update").html(data);
		$("#trip_update_button").attr("disabled", false);
		$("#load_button").hide();
		$("#edit_btn_icon").show();
		$("#button_name").html("Update TRIP");
	},
	error: function() 
{} });}));});
</script>

<button onclick="window.close();" class="btn btn-sm btn-danger" style="margin:10px;" type="button"><i class="fa fa-window-close-o" aria-hidden="true"></i> &nbsp; Close Window</button>
<button type="button" id="pod_button" class="btn btn-sm btn-primary" onclick="ResetPOD11('<?php echo $id; ?>')"><i class="fa fa-refresh" aria-hidden="true"></i> &nbsp; Reset POD</button>
<?php
if($row_trip['status']=="1")
{
	if($row_trip['active_trip']=='1')
	{
		$btn_dsble_1="disabled";
		$btn_dsble_2="";
	}
	else
	{
		$btn_dsble_1="";
		$btn_dsble_2="disabled";
	}
?>
	<button type="button" <?php echo $btn_dsble_1; ?> id="active_trip_btn" class="btn btn-sm btn-success" style="margin-left:10px;" onclick="ActiveTrip('<?php echo $id; ?>','ACTIVE')"><i class="fa fa-check" aria-hidden="true"></i> &nbsp; Active Trip</button>
	<button type="button" <?php echo $btn_dsble_2; ?> id="block_trip_btn" class="btn btn-sm btn-danger" style="margin-left:10px;" onclick="ActiveTrip('<?php echo $id; ?>','BLOCK')"><i class="fa fa-stop-circle" aria-hidden="true"></i> &nbsp; Lock Trip</button>
<?php
}
?>

<form action="edit_trip_by_id.php" method="POST" id="Form1">
	<input type="hidden" value="<?php echo $id; ?>" name="trip_id" required>
</form>
	
<script>
function ReLoadPage()
{
	document.getElementById('Form1').submit();
}
</script>

<div id="result_update"></div>
<div id="result_data2"></div>
	
<div class="container-fluid" style="font-family: 'Open Sans', sans-serif !important;font-size:12px">
	
	<div class="row">
		<h5 style="padding:7px;font-size:13px;" class="bg-success form-group col-md-12">
			TRIP Summary : <?php echo $row_trip['tno']." => ".$row_trip['from_station']." to ".$row_trip['to_station']; ?>
		</h5>
	</div>
	
<form id="UpdateTrip" autocomplete="off">
		 
	<input type="hidden" name="trip_id" value="<?php echo $id; ?>">
	<input type="hidden" id="truck_no" name="tno" value="<?php echo $row_trip['tno']; ?>">
	
<div class="row">
	
	<div class="form-group col-md-2">
         <label>Edit Branch <font color="red"><sup>*</sup></font></label>
         <select name="edit_branch" value="<?php echo $row_trip['edit_branch']; ?>" id="edit_branch" class="form-control" required>
			<option value="">--select branch--</option>
			<option value="0">Unknown</option>
			<?php
			$qry_fetch_branch = Qry($conn,"SELECT username FROM user WHERE role='2' ORDER BY username ASC");
			
			if(!$qry_fetch_branch){
				errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
				Redirect("Error while Processing Request.","./");
				exit();
			}
			
			if(numRows($qry_fetch_branch)>0)
			{
				while($row_branch=fetchArray($qry_fetch_branch))
				{
				?>
					<option <?php if($row_branch['username']==$row_trip['edit_branch']) { echo "selected"; } ?> 
					value="<?php echo $row_branch['username']; ?>"><?php echo $row_branch['username']; ?></option>	
				<?php
				}
			}
			?>
		</select>
     </div>
	 
<script>	 
function GetDistance()
{
	var from_id_ext = '<?php echo $from_id ?>';
	var to_id_ext = '<?php $to_id ?>';
	
	var from_id = $('#from_id1').val();
	var to_id = $('#to_id1').val();
	var from_lat_long = $('#from_lat_long').val();
	var to_lat_long = $('#to_lat_long').val();
	
	if(from_id == "" || to_id == "")
	{
		alert('Locations not found !');
	}
	else
	{
			if(from_lat_long=='' || to_lat_long=='')
			{
				alert('Location coordinates not found !');
			}
			else
			{
				$("#loadicon").show();
					jQuery.ajax({
					url: "get_distance_km.php",
					data: 'from_id=' + from_id + '&to_id=' + to_id + '&from_lat_long=' + from_lat_long + '&to_lat_long=' + to_lat_long,
					type: "POST",
					success: function(data) {
						$("#result_data_3").html(data);
						$("#loadicon").fadeOut('slow');
					},
					error: function() {}
				});
			}
}
	
	// $('#trip_update_button').attr('disabled',false);
}
</script>	

<div id="result_data_3"></div>
	 
	 <div class="form-group col-md-2">
         <label>From Station <font color="red"><sup>*</sup></font></label>
         <input type="text" onblur="GetDistance()" <?php echo $lock_field; ?> value="<?php echo $from_station; ?>" name="from_station" id="from_station" class="form-control" required>		
     </div>
	 
	 <input type="hidden" id="from_id1" value="<?php echo $from_id; ?>" name="from_id1" />
	 
	 <div class="form-group col-md-2">
         <label>To Station <font color="red"><sup>*</sup></font></label>
         <input type="text" onblur="GetDistance()" <?php echo $lock_field; ?> value="<?php echo $to_station; ?>" name="to_station" id="to_station" class="form-control" required>		
     </div>
	 
	 <input type="hidden" id="to_id1" value="<?php echo $to_id; ?>" name="to_id1" />
	 
	 <input type="hidden" id="from_lat_long" value="<?php echo $from_poi; ?>" name="from_lat_long" />
	 <input type="hidden" id="to_lat_long" value="<?php echo $to_poi; ?>" name="to_lat_long" />
	 
	 <div class="form-group col-md-2">
         <label>From Pincode <font color="red"><sup>*</sup></font></label>
         <input type="text" readonly value="<?php echo $from_pincode; ?>" name="from_pincode" id="from_pincode" class="form-control" required>		
     </div>
	 
	  <div class="form-group col-md-2">
         <label>To Pincode <font color="red"><sup>*</sup></font></label>
         <input type="text" readonly value="<?php echo $to_pincode; ?>" name="to_pincode" id="to_pincode" class="form-control" required>		
     </div>
	 
	  <div class="form-group col-md-2">
         <label>Trip Distance <font color="red"><sup>* (in KM)</sup></font></label>
		 <input type="text" value="<?php echo $trip_km; ?>" name="trip_km" id="trip_km" class="form-control" readonly required />		
     </div>
	 
	<div class="form-group col-md-2">
         <label>Act Wt <font color="red"><sup>*</sup></font></label>
         <input type="number" readonly min="0" value="<?php echo $row_trip['act_wt']; ?>" step="any" name="act_weight" id="act_weight" class="form-control" required>		
     </div>
	 
	 <div class="form-group col-md-2">
         <label>Charge Wt <font color="red"><sup>*</sup></font></label>
         <input type="number" readonly min="0" value="<?php echo $row_trip['charge_wt']; ?>" step="any" name="charge_weight" id="charge_weight" class="form-control" required>		
     </div>
	 
	<div class="form-group col-md-3">
         <label>LR Type <font color="red"><sup>*</sup></font></label>
		 <input style="font-size:10px !important" type="text" value="<?php echo $row_trip['lr_type']; ?>" id="lr_type" class="form-control" readonly required>		
     </div>
	 
	 <div class="form-group col-md-5">
         <label>LR Number <font color="red"><sup>*</sup></font></label>
		 <input style="font-size:10px !important" type="text" value="<?php echo $row_trip['lrno']; ?>" id="lr_no" class="form-control" readonly required>		
     </div>
	 
	 <div class="form-group col-md-2">
        <label>Trip Start Date <font color="red"><sup>*</sup></font></label>
		<input type="date" value="<?php echo $row_trip['start_date']; ?>" name="start_date" id="start_date" class="form-control" readonly required pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}">		
     </div>
	 
	 <div class="form-group col-md-2">
         <label>Trip End Date <font color="red"><sup>*</sup></font></label>
		 <input type="date" value="<?php echo $row_trip['end_date']; ?>" name="end_date" id="end_date" class="form-control" readonly required pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}">		
     </div>
	 
	 <div class="form-group col-md-2">
		  <label>&nbsp;</label>
			<?php
			if(!isMobile()){
				echo "<br />";
			}		
			?>
         <button id="trip_update_button" class="btn btn-success btn-sm" type="submit">
		 <i id="load_button" style="display:none" class="fa fa-refresh fa-spin"></i> <i id="edit_btn_icon" class="fa fa-pencil-square-o" aria-hidden="true"></i> <span id="button_name">&nbsp; Update TRIP</span></button>
     </div>

</div>	  
 </form> 
 
<div class="row">
				
	<div class="form-group col-md-4 table-responsive">
		<div class="col-md-12 bg-primary" style="width:100%;color:#FFF;padding:4px;">
			<span style="font-size:12px;">Freight to driver : <?php echo "₹ ".$row_trip['freight']; ?></span>
		</div>
		
	<div class="col-md-12 table-responsive">
		<div class="row">		
		<?php
		echo "<table class='table table-bordered' style='color:#000;font-size:11px;'>";
		
		if($row_trip['freight']>0)
		{
			echo "<tr>
				<th>Amount</th>
				<th>Date</th>
				<th>Branch</th>
				<th>#Delete</th>
				<th>#Edit</th>
				<th>#View</th>
			</tr>";
		
			$qry_freight_adv = Qry($conn,"SELECT id,trans_id,vou_id,amount,date,narration,branch FROM dairy.freight_adv WHERE trip_id='$id'");
			
			if(!$qry_freight_adv){
				errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
				exit();
			}
			
			if(numRows($qry_freight_adv)>0)
			{
				while($row_f1=fetchArray($qry_freight_adv))
				{
					echo "<input type='hidden' id='trans_date$row_f1[id]' value='$row_f1[date]'>";
					echo "<input type='hidden' id='narration_frt_adv$row_f1[id]' value='$row_f1[narration]'>";
					
					$f_adv_date = date("d/m/y", strtotime($row_f1['date']));
					
					echo "<tr>
						<td>$row_f1[amount]</td>
						<td>$f_adv_date</td>
						<td>$row_f1[branch]</td>
						<td><center><button id='dlt_f_adv_btn_$row_f1[id]' style='padding:3px' class='btn btn-xs btn-danger' 
						onclick=DeleteFreightToDriver('$row_f1[id]')><span class='fa fa-trash'></span></button></center></td>
						
						<td><center><button style='padding:3px' class='btn btn-xs btn-default' id='edit_f_adv_btn_$row_f1[id]' 
						onclick=EditFreightAdvance('$row_f1[id]')><span class='fa fa-pencil-square-o'></span></button></center></td>
						
						<td><center><button style='padding:3px;' class='btn btn-xs btn-default' onclick=ViewEntry('$row_f1[id]','FRT_ADV')>
						<span class='fa fa-search'></span></button></center></td>
					</tr>";
				}
			}
		}
		else
		{
			echo "<td colspan='6'>No result found.</td>";
		}	
		echo "</table>";
		?>
   </div>
   </div>
   </div>
   
   <div class="form-group col-md-4">
		
		<div class="col-md-12 bg-primary" style="width:100%;color:#FFF;padding:4px;">
			<span style="font-size:12px;">Advance (Cash/Happay) : <?php echo "₹ ".$row_trip['cash']; ?></span>
		</div>
		
	<div class="col-md-12 table-responsive">
		<div class="row">	
		<?php
		echo "<table class='table table-bordered' style='color:#000;font-size:11px;'>";
		
		if($row_trip['cash']>0)
		{
			echo "<tr>
					<th>Vou_no</th>
					<th>Amount</th>
					<th>Txn_type</th>
					<th>Date</th>
					<th>Branch</th>
					<th>#Delete</th>
					<th>#Edit</th>
					<th>#View</th>
				</tr>";
		
			$qry_cash_adv = Qry($conn,"SELECT id,vou_no,trans_id,trans_type,amount,date,narration,branch FROM dairy.cash WHERE trip_id='$id'");
			
			if(!$qry_cash_adv){
				errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
				exit();
			}
			
			if(numRows($qry_cash_adv)>0)
			{
				while($row_cash_adv=fetchArray($qry_cash_adv))
				{
					echo "<input type='hidden' id='trans_date$row_cash_adv[id]' value='$row_cash_adv[date]'>";
					echo "<input type='hidden' id='narration_cash_adv$row_cash_adv[id]' value='$row_cash_adv[narration]'>";
					
					$cash_adv_date = date("d/m/y", strtotime($row_cash_adv['date']));
					
					echo "<tr>
						<td><a style='color:maroon' onclick=ViewAdvVoucher('$row_cash_adv[vou_no]') href='#'><b>$row_cash_adv[vou_no]</b></a></td>
						<td>$row_cash_adv[amount]</td>
						<td>$row_cash_adv[trans_type]</td>
						<td>$cash_adv_date</td>
						<td>$row_cash_adv[branch]</td>";
						
						if($row_cash_adv['trans_type']=='CASH')
						{
						echo "
							<td><center><button style='padding:3px' class='btn btn-xs btn-danger' id='delete_cash_btn_$row_cash_adv[id]' 
						onclick=DeleteAdvCash('$row_cash_adv[id]')><span class='fa fa-trash'></span></button> </center></td>
						
						<td><center><button style='padding:3px' class='btn btn-xs btn-default' id='edit_cash_btn_$row_cash_adv[id]' 
						onclick=EditAdvance('$row_cash_adv[id]','CASH')><span class='fa fa-pencil-square-o'></span></button></center></td>";
						}
						else
						{
							echo "<td></td><td></td>";
						}
						
					echo "
						<td><center><button style='padding:3px;' class='btn btn-xs btn-default' onclick=ViewEntry('$row_cash_adv[id]','CASH')>
						<span class='fa fa-search'></span></button></center></td>
					</tr>";
				}
			}
		}
		else
		{
			echo "<td colspan='8'>No result found.</td>";
		}	
		echo "</table>";
		?>
   </div>
   </div>
   </div>
   
    <div class="form-group col-md-4 table-responsive">
		<div class="col-md-12 bg-primary" style="width:100%;color:#FFF;padding:4px;">
			<span style="font-size:12px;">Advance (RTGS) : <?php echo "₹ ".$row_trip['rtgs']; ?></span>
		</div>
		
		<div class="col-md-12 table-responsive">
		<div class="row">	
		<?php
		echo "<table class='table table-bordered' style='color:#000;font-size:11px;'>";
		
		if($row_trip['rtgs']>0)
		{
			echo "<tr>
					<th>Vou_No</th>
					<th>Amount</th>
					<th>Date</th>
					<th>Branch</th>
					<th>#Delete</th>
					<th>#Edit</th>
					<th>#View</th>
				</tr>";
		
			$qry_rtgs_adv = Qry($conn,"SELECT id,vou_no,trans_id,amount,date,narration,branch FROM dairy.rtgs WHERE trip_id='$id'");
			
			if(!$qry_rtgs_adv){
				errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
				exit();
			}
			
			if(numRows($qry_rtgs_adv)>0)
			{
				while($row_rtgs_adv=fetchArray($qry_rtgs_adv))
				{
					echo "<input type='hidden' id='trans_date$row_rtgs_adv[id]' value='$row_rtgs_adv[date]'>";
					echo "<input type='hidden' id='narration_rtgs_adv$row_rtgs_adv[id]' value='$row_rtgs_adv[narration]'>";
					
					$rtgs_adv_date = date("d/m/y", strtotime($row_rtgs_adv['date']));
					
					echo "<tr>
						<td><a style='color:maroon' onclick=ViewAdvVoucher('$row_rtgs_adv[vou_no]') href='#'><b>$row_rtgs_adv[vou_no]</b></a></td>
						<td>$row_rtgs_adv[amount]</td>
						<td>$rtgs_adv_date</td>
						<td>$row_rtgs_adv[branch]</td>
						
						<td><center><button id='delete_rtgs_btn_$row_rtgs_adv[id]' style='padding:3px' class='btn btn-xs btn-danger'
						onclick=DeleteAdvRtgs('$row_rtgs_adv[id]')><span class='fa fa-trash'></span></button></center></td>
						
						<td><center><button style='padding:3px' class='btn btn-xs btn-default' id='edit_rtgs_btn_$row_rtgs_adv[id]' 
						onclick=EditAdvance('$row_rtgs_adv[id]','RTGS')><span class='fa fa-pencil-square-o'></span></button></center></td>
						
						<td><center><button style='padding:3px' class='btn btn-xs btn-default' onclick=ViewEntry('$row_rtgs_adv[id]','RTGS')>
						<span class='fa fa-search'></span></button></center></td>
					</tr>";
				}
			}
		}
		else
		{
			echo "<td colspan='7'>No result found.</td>";
		}	
		echo "</table>";
		?>
   </div>
   </div>
   </div>
   
    </div>

<div class="row">  
   
   <div class="form-group col-md-4 table-responsive">
		<div class="col-md-12 bg-primary" style="width:100%;color:#FFF;padding:4px;">
			<span style="font-size:12px;">Driver Naame : <?php echo "₹ ".$row_trip['driver_naame']; ?></span>
		</div>
		
	<div class="col-md-12 table-responsive">
		<div class="row">		
		<?php
		echo "<table class='table table-bordered' style='color:#000;font-size:11px;'>";
		
		if($row_trip['driver_naame']>0)
		{
			echo "<tr>
					<th>Amount</th>
					<th>Txn_type</th>
					<th>Date</th>
					<th>Branch</th>
					<th>#Delete</th>
					<th>#Edit</th>
					<th>#View</th>
				</tr>";
		
			$qry_naame = Qry($conn,"SELECT id,trans_id,naame_type,qty,rate,amount,date,narration,branch FROM dairy.driver_naame WHERE trip_id='$id'");
			
			if(!$qry_naame){
				errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
				exit();
			}
			
			if(numRows($qry_naame)>0)
			{
				while($row_naame=fetchArray($qry_naame))
				{
					echo "<input type='hidden' id='trans_date$row_naame[id]' value='$row_naame[date]'>";
					echo "<input type='hidden' id='narration_naame$row_naame[id]' value='$row_naame[narration]'>";
					
					$naame_date = date("d/m/y", strtotime($row_naame['date']));
					
					echo "<tr>
						<td>$row_naame[amount]</td>
						<td>$row_naame[naame_type]</td>
						<td>$naame_date</td>
						<td>$row_naame[branch]</td>
						
						<td><center><button id='dlt_naame_btn_$row_naame[id]' style='padding:3px' class='btn btn-xs btn-danger'
						onclick=DeleteDriverNaame('$row_naame[id]')><span class='fa fa-trash'></span></button></center></td>
						
						<td><center><button style='padding:3px' class='btn btn-xs btn-default' id='edit_naame_btn_$row_naame[id]' 
						onclick=EditDriverNaame('$row_naame[id]','$row_naame[naame_type]')><span class='fa fa-pencil-square-o'></span></button></center></td>
						
						<td><center><button style='padding:3px' class='btn btn-xs btn-default' onclick=ViewEntry('$row_naame[id]','NAAME')>
						<span class='fa fa-search'></span></button></center></td>
					</tr>";
				}
			}
		}
		else
		{
			echo "<td colspan='7'>No result found.</td>";
		}	
		echo "</table>";
		?>
   </div>
   </div>
   </div>
   
   
   <div class="form-group col-md-4 table-responsive">
		<div class="col-md-12 bg-primary" style="width:100%;color:#FFF;padding:4px;">
			<span style="font-size:12px;">Diesel : <?php echo "₹ ".$row_trip['diesel']." (".$row_trip['diesel_qty'].") LTR."; ?></span>
		</div>
		
<div class="col-md-12 table-responsive">
		<div class="row">			
		<?php
	
	echo "<table class='table table-bordered' style='color:#000;font-size:11px;'>";
		
		if($row_trip['diesel']>0)
		{
			echo "<tr>
					<th>Rate</th>
					<th>Qty</th>
					<th>Amount</th>
					<th>Date</th>
					<th>Branch</th>
					<th>#View</th>
				</tr>";
		
			$qry_diesel = Qry($conn,"SELECT id,rate,trans_id,qty,amount,date,narration,branch FROM dairy.diesel WHERE trip_id='$id'");
			
			if(!$qry_diesel){
				errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
				exit();
			}
			
			if(numRows($qry_diesel)>0)
			{
				while($row_diesel=fetchArray($qry_diesel))
				{
					$get_diesel_entry = Qry($conn,"SELECT d.card_pump,d.card,d.dsl_company,p.consumer_pump 
					FROM dairy.diesel_entry AS d 
					LEFT OUTER JOIN dairy.diesel_pump_own AS p ON p.code=d.card 
					WHERE d.id='$row_diesel[id]'");
					
					if(!$get_diesel_entry)
					{
						errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
						exit();
					}
					
					$row_diesel_entry = fetchArray($get_diesel_entry);
					
					echo "<input type='hidden' id='trans_date$row_diesel[id]' value='$row_diesel[date]'>";
					echo "<input type='hidden' id='narration_diesel$row_diesel[id]' value='$row_diesel[narration]'>";
					echo "<input type='hidden' id='card_pump$row_diesel[id]' value='$row_diesel_entry[card_pump]'>";
					echo "<input type='hidden' id='card_no$row_diesel[id]' value='$row_diesel_entry[card]'>";
					echo "<input type='hidden' id='dsl_company$row_diesel[id]' value='$row_diesel_entry[dsl_company]'>";
					
					if($row_diesel_entry['card_pump']=='PUMP' AND $row_diesel_entry['consumer_pump']==0)
					{
						$edit_disable="";
					}
					else
					{
						$edit_disable="disabled";
					}
					
					$diesel_date = date("d/m/y", strtotime($row_diesel['date']));
					
					echo "<tr>
						<td>$row_diesel[rate]</td>
						<td>$row_diesel[qty]</td>
						<td>$row_diesel[amount]</td>
						<td>$diesel_date</td>
						<td>$row_diesel[branch]</td>
						<td><center><button style='padding:3px' class='btn btn-xs btn-default' onclick=ViewEntry('$row_diesel[id]','DIESEL')>
						<span class='fa fa-search'></span></button></center></td>
					</tr>";
					
					// <td><button id='dlt_diesel_button$row_diesel[id]' style='padding:3px' class='btn btn-xs btn-danger'
						// onclick=DeleteDiesel('$row_diesel[amount]','$row_diesel[id]','$row_diesel[trans_id]','$row_diesel[qty]')>
						// <span class='fa fa-trash'></span></button></td>
						// <td><button $edit_disable style='padding:3px' class='btn btn-xs btn-primary' id='edit_diesel_button$row_diesel[id]' 
						// onclick=EditDiesel('$row_diesel[id]','edit_diesel_button','$row_diesel[qty]','$row_diesel[rate]','$row_diesel[amount]','$row_diesel[branch]')>
						// <span class='fa fa-pencil-square-o'></span></button></td>
				}
			}
		}
		else
		{
			echo "<td colspan='6'>No result found.</td>";
		}	
		echo "</table>";
		?>
   </div>
   </div>
   </div>
   
      <div class="form-group col-md-4 table-responsive">
		<div class="col-md-12 bg-primary" style="width:100%;color:#FFF;padding:4px;">
			<span style="font-size:12px;">Freight Collected : <?php echo "₹ ".$row_trip['freight_collected']; ?></span>
		</div>
		
		<div class="col-md-12 table-responsive">
		<div class="row">	
		<?php
		
		echo "<table class='table table-bordered' style='color:#000;font-size:11px;'>";
		
		if($row_trip['freight_collected']>0)
		{
			echo "<tr>
					<th>Amount</th>
					<th>Date</th>
					<th>Branch</th>
					<th>#Delete</th>
					<th>#Edit</th>
					<th>#View</th>
				</tr>";
		
			$qry_f_coll = Qry($conn,"SELECT id,amount,trans_id,date,narration,branch FROM dairy.freight_rcvd WHERE trip_id='$id'");
			
			if(!$qry_f_coll){
				errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
				exit();
			}
			
			if(numRows($qry_f_coll)>0)
			{
				while($row_f_coll=fetchArray($qry_f_coll))
				{
					echo "<input type='hidden' id='trans_date$row_f_coll[id]' value='$row_f_coll[date]'>";
					echo "<input type='hidden' id='narration_frt_coll$row_f_coll[id]' value='$row_f_coll[narration]'>";
					
					$f_coll_date = date("d/m/y", strtotime($row_f_coll['date']));
					
					echo "<tr>
						<td>$row_f_coll[amount]</td>
						<td>$f_coll_date</td>
						<td>$row_f_coll[branch]</td>
						
						<td><center><button id='dlt_f2_btn_$row_f_coll[id]' style='padding:3px' class='btn btn-xs btn-danger'
						onclick=DeleteFreightCollected('$row_f_coll[id]')><span class='fa fa-trash'></span></button></center></td>
						
						<td><center><button style='padding:3px' class='btn btn-xs btn-default' id='edit_f2_btn_$row_f_coll[id]' 
						onclick=EditFreightColl('$row_f_coll[id]')><span class='fa fa-pencil-square-o'></span></button></center></td>
						
						<td><center><button style='padding:3px' class='btn btn-xs btn-default' onclick=ViewEntry('$row_f_coll[id]','FRT_COLL')>
						<span class='fa fa-search'></span></button></center></td>
					</tr>";
				}
			}
		}
		else
		{
			echo "<td colspan='4'>No result found.</td>";
		}	
		echo "</table>";
		?>
   </div>
   </div>
   </div>
   
 </div>
 
 <div class="row">
   
      <div class="form-group col-md-4 table-responsive">
		<div class="col-md-12 bg-primary" style="width:100%;color:#FFF;padding:4px;">
			<span style="font-size:12px;">Expenses : <?php echo "₹ ".$row_trip['expense']; ?></span>
		</div>
		
	<div class="col-md-12 table-responsive">
		<div class="row">		
		<?php
		
		echo "<table class='table table-bordered col-md-12' style='color:#000;font-size:11px;'>";
		
		if($row_trip['expense']>0)
		{
			echo "<tr>
					<th>Exp</th>
					<th>Amount</th>
					<th>Date</th>
					<th>Branch</th>
					<th>#Delete</th>
					<th>#Edit</th>
					<th>#View</th>
				</tr>";
		
			$qry_exp = Qry($conn,"SELECT id,exp_name,exp_code,trans_id,amount,date,narration,branch FROM dairy.trip_exp WHERE trip_id='$id'");
			
			if(!$qry_exp){
				errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
				exit();
			}
			
			if(numRows($qry_exp)>0)
			{
				while($row_exp=fetchArray($qry_exp))
				{
					echo "<input type='hidden' id='exp_head$row_exp[id]' value='$row_exp[exp_name]'>";
					echo "<input type='hidden' id='trans_date$row_exp[id]' value='$row_exp[date]'>";
					echo "<input type='hidden' id='trans_dateExp$row_exp[id]' value='$row_exp[date]'>";
					echo "<input type='hidden' id='narration_exp$row_exp[id]' value='$row_exp[narration]'>";
					echo "<input type='hidden' id='exp_name1$row_exp[id]' value='$row_exp[exp_name]'>";
					
					$exp_date = date("d/m/y", strtotime($row_exp['date']));
					
					echo "<tr>
						<td>$row_exp[exp_name]</td>
						<td>$row_exp[amount]</td>
						<td>$exp_date</td>
						<td>$row_exp[branch]</td>
						
						<td><center>
						<button id='dlt_exp_btn_$row_exp[id]' style='padding:3px' class='btn btn-xs btn-danger' 
						onclick=DeleteExp('$row_exp[id]')><span class='fa fa-trash'></span></button></center></td>
						
						<td><center><button style='padding:3px' class='btn btn-xs btn-default' id='edit_exp_btn_$row_exp[id]' 
						onclick=EditExpense('$row_exp[id]')><span class='fa fa-pencil-square-o'></span></button></center></td>
						
						<td><center><button style='padding:3px;' class='btn btn-xs btn-default' onclick=ViewEntry('$row_exp[id]','EXPENSE')>
						<span class='fa fa-search'></span></button></center></td>
					</tr>";
				}
			}
		}
		else
		{
			echo "<td colspan='7'>No result found.</td>";
		}	
		echo "</table>";
		?>
   </div>
   </div>
   </div>
   
</div>
			
<script>
function DeleteFreightToDriver(id)
{
	if(confirm('Confirm Delete Freight to Driver ?'))
	{
		$("#dlt_f_adv_btn_"+id).attr('disabled',true);
		
		$("#loadicon").show();
			jQuery.ajax({
			url: "delete_freight_to_driver.php",
			data: 'id=' + id,
			type: "POST",
			success: function(data) {
				$("#result_data2").html(data);
			},
			error: function() {}
		});
	}
}

function DeleteAdvCash(id)
{
	if(confirm('Confirm Delete Cash Advance ?'))
	{
		$("#delete_cash_btn_"+id).attr('disabled',true);
		
		$("#loadicon").show();
			jQuery.ajax({
			url: "delete_advance.php",
			data: 'id=' + id + '&txn_type=' + 'CASH',
			type: "POST",
			success: function(data) {
				$("#result_data2").html(data);
			},
			error: function() {}
		});
	}
}

function EditAdvance(id,txn_type)
{
	$("#loadicon").show();
			jQuery.ajax({
			url: "modal_advance_edit.php",
			data: 'id=' + id + '&txn_type=' + txn_type,
			type: "POST",
			success: function(data) {
				$("#result_data2").html(data);
			},
			error: function() {}
		});
}

function EditFreightAdvance(id)
{
	$("#loadicon").show();
			jQuery.ajax({
			url: "modal_freight_advance_edit.php",
			data: 'id=' + id,
			type: "POST",
			success: function(data) {
				$("#result_data2").html(data);
			},
			error: function() {}
	});
}

function EditDriverNaame(id,naame_type)
{
	if(naame_type=='CLAIM'|| naame_type=='FREIGHT')
	{
		alert('Claim and Freight naame is not editable..');
	}
	else
	{
		$("#loadicon").show();
			jQuery.ajax({
			url: "modal_driver_naame_edit.php",
			data: 'id=' + id,
			type: "POST",
			success: function(data) {
				$("#result_data2").html(data);
			},
			error: function() {}
		});
	}
}

function EditDiesel(id,button_id,qty,rate,amount,branch)
{
	// on hold
	// $("#loadicon").show();
			// jQuery.ajax({
			// url: "modal_driver_naame_edit.php",
			// data: 'id=' + id,
			// type: "POST",
			// success: function(data) {
				// $("#result_data2").html(data);
			// },
			// error: function() {}
		// });
}

function EditFreightColl(id,button_id,amount)
{
	$("#loadicon").show();
			jQuery.ajax({
			url: "modal_freight_collected_edit.php",
			data: 'id=' + id,
			type: "POST",
			success: function(data) {
				$("#result_data2").html(data);
			},
			error: function() {}
		});
}

function EditExpense(id,button_id,amount,exp_code)
{
	$("#loadicon").show();
			jQuery.ajax({
			url: "modal_expense_edit.php",
			data: 'id=' + id,
			type: "POST",
			success: function(data) {
				$("#result_data2").html(data);
			},
			error: function() {}
		});
}

function ViewEntry(id,trans_type)
{
	$("#loadicon").show();
			jQuery.ajax({
			url: "modal_view_txns.php",
			data: 'id=' + id + '&trans_type=' + trans_type,
			type: "POST",
			success: function(data) {
				$("#get_view_data").html(data);
			},
			error: function() {}
	});
}

function ActiveTrip(id,type1)
{
	if(type1=='ACTIVE')
	{
		$("#loadicon").show();
			jQuery.ajax({
			url: "trip_status_toggle.php",
			data: 'id=' + id + '&value=' + '1',
			type: "POST",
			success: function(data) {
				$("#result_edits").html(data);
			},
			error: function() {}
		});
	}
	else if(type1=='BLOCK')
	{
		$("#loadicon").show();
			jQuery.ajax({
			url: "trip_status_toggle.php",
			data: 'id=' + id + '&value=' + '0',
			type: "POST",
			success: function(data) {
				$("#result_edits").html(data);
			},
			error: function() {}
		});
	}
}
</script>	

<div id="result_edits"></div>

<div id="get_view_data"></div>
 
<form action="../_view/vouchers.php" target="_blank" method="POST" id="view_vou_form">
	<input type="hidden" id="view_vou_vou_no" name="vou_no" />
</form>

<script>
function ViewAdvVoucher(vou_no)
{
	$('#view_vou_vou_no').val(vou_no);
	$('#view_vou_form')[0].submit();
}

function DeleteAdvRtgs(id)
{
	if(confirm('Are you sure ?'))
	{
		$("#delete_rtgs_btn_"+id).attr('disabled',true);
		
		$("#loadicon").show();
			jQuery.ajax({
			url: "delete_advance.php",
			data: 'id=' + id + '&txn_type=' + 'RTGS',
			type: "POST",
			success: function(data) {
				$("#result_data2").html(data);
			},
			error: function() {}
		});
	
	}
}
</script>	   
			 
<script>
function DeleteDiesel(amount,id,trans_id,qty)
{
	// on hold
	
	// $("#dlt_diesel_button"+id).attr('disabled',true);
	
	// if(confirm('Are you sure you want to delete Diesel of Rs: '+amount+' ?'))
	// {
		
	// var trans_date = $('#trans_date'+id).val();
	// $("#loadicon").show();
		// jQuery.ajax({
		// url: "delete_diesel.php",
		// data: 'id=' + id + '&trip_id=' + '<?php echo $id; ?>' + '&trans_id=' + trans_id + '&tno=' + '<?php echo $tno; ?>' + '&amount=' + amount + '&qty=' + qty + '&trans_date=' + trans_date,
		// type: "POST",
		// success: function(data) {
			// $("#result_data2").html(data);
			// $("#loadicon").hide();
		// },
		// error: function() {}
	// });
	
	// }
	// else
	// {
		// $("#dlt_diesel_button"+id).attr('disabled',false);
	// }
}
</script>	
			  
<script>
function DeleteFreightCollected(id)
{
	if(confirm('Are you sure ?'))
	{
		$("#dlt_f2_btn_"+id).attr('disabled',true);	
		$("#loadicon").show();
			jQuery.ajax({
			url: "delete_freight_collected.php",
			data: 'id=' + id,
			type: "POST",
			success: function(data) {
				$("#result_data2").html(data);
				$("#loadicon").hide();
			},
			error: function() {}
		});
	}
}
</script>

<script>
function DeleteDriverNaame(id)
{
	if(confirm('Are you sure ?'))
	{
		$("#dlt_naame_btn_"+id).attr('disabled',true);
		
		$("#loadicon").show();
			jQuery.ajax({
			url: "delete_driver_naame.php",
			data: 'id=' + id,
			type: "POST",
			success: function(data) {
				$("#result_data2").html(data);
			},
			error: function() {}
		});
	}
}
</script>
			  
<script>
function DeleteExp(id)
{
	if(confirm('Are you sure ?'))
	{
		$("#dlt_exp_btn_"+id).attr('disabled',true);
		
		$("#loadicon").show();
			jQuery.ajax({
			url: "delete_exp.php",
			data: 'id=' + id,
			type: "POST",
			success: function(data) {
				$("#result_data2").html(data);
			},
			error: function() {}
		});
	}
}		   
</script>

<script>
// function DeleteAdvChq(id)
// {
	// $("#delete_cheque_button").attr('disabled',true);	
	// $("#loadicon").show();
		// jQuery.ajax({
		// url: "delete_advance.php",
		// data: 'id=' + id + '&trip_id=' + '<?php echo $id; ?>' + '&type=' + 'CHQ',
		// type: "POST",
		// success: function(data) {
		// $("#result_data2").html(data);
		// $("#loadicon").hide();
		// $("#delete_cheque_button").attr('disabled',false);
		// },
		// error: function() {}
	// });
// }
</script>	
			  
<script> 
function ResetPOD(id)
{
	// on hold
	
	// $("#loadicon").show();
	// jQuery.ajax({
		// url: "reset_pod.php",
		// data: 'id=' + id,
		// type: "POST",
		// success: function(data) {
			// $("#result_data2").html(data);
		// },
		// error: function() {}
	// });
}
</script>
 
<?php
if($row_trip['pod']>0)
{
	echo "<script>
		$('#pod_button').attr('disabled',true);
	</script>";
}
else
{
	echo "<script>
		$('#pod_button').attr('disabled',true);
	</script>";
}	
?>