<?php
require_once("connect.php");

$driver_id = escapeString($conn,($_POST['driver_id']));
$driver_code = escapeString($conn,($_POST['driver_code']));
$driver_name = escapeString($conn,($_POST['driver_name']));
// exit();
if(empty($driver_id))
{
	AlertErrorTopRight("Select driver first !");
	echo "<script>$('#search_btn').attr('disabled',false);</script>";
	exit();
}

$get_driver = Qry($conn,"SELECT name,code,open_close_verified FROM dairy.driver WHERE id='$driver_id'");

if(!$get_driver){
	AlertErrorTopRight("Error while processing request !");
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	exit();
}

$row_driver = fetchArray($get_driver);

if($row_driver['open_close_verified']=='1')
{
	AlertErrorTopRight("Opening closing already verified !");
	echo "<script>$('#search_btn').attr('disabled',false);</script>";
	exit();
}

if($row_driver['name'] != $driver_name)
{
	AlertErrorTopRight("Driver name not verified !");
	echo "<script>$('#search_btn').attr('disabled',false);</script>";
	exit();
}

if($row_driver['code'] != $driver_code)
{
	AlertErrorTopRight("Driver code not verified !");
	echo "<script>$('#search_btn').attr('disabled',false);</script>";
	exit();
}

$get_first_opening_balance = Qry($conn,"SELECT id,trip_no,credit,debit,balance FROM dairy.driver_book WHERE date<='2020-12-31' AND 
driver_code='$driver_code' AND trip_no!='' AND desct='DRIVER_UP' ORDER BY id DESC LIMIT 1");

if(!$get_first_opening_balance){
	AlertErrorTopRight("Error while processing request !");
	echo "<script>$('#search_btn').attr('disabled',false);</script>";
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	exit();
}

if(numRows($get_first_opening_balance) == 0)
{
	AlertErrorTopRight("Driver up data not found !");
	echo "<script>$('#search_btn').attr('disabled',false);</script>";
	exit();
}

$row_first_data = fetchArray($get_first_opening_balance);

if($row_first_data['credit'] != $row_first_data['balance'])
{
	AlertErrorTopRight("Something went wrong. Contact system-admin !");
	echo "<script>$('#search_btn').attr('disabled',false);</script>";
	exit();
}

$first_trip_no = $row_first_data['trip_no'];
$opening_balance = $row_first_data['balance'];
$first_id = $row_first_data['id'];

$update_driver_balance = Qry($conn,"UPDATE dairy.driver SET driver_balance='$opening_balance' WHERE code='$driver_code'");

if(!$update_driver_balance){
	AlertErrorTopRight("Error while processing request !");
	echo "<script>$('#search_btn').attr('disabled',false);</script>";
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	exit();
}

StartCommit($conn);
$flag = true;

$get_trips1 = Qry($conn,"SELECT id,credit,debit,desct,balance FROM dairy.driver_book WHERE id>='$first_id' AND driver_code='$driver_code' 
ORDER BY trip_no ASC,id ASC");

if(!$get_trips1){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

while($row2 = fetchArray($get_trips1))
{
	$get_driver_balance = Qry($conn,"SELECT driver_balance FROM dairy.driver WHERE code='$driver_code'");

	if(!$get_driver_balance){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}

	$row1 = fetchArray($get_driver_balance);

	$driver_balance = $row1['driver_balance'];

	if($row2['desct']!='DRIVER_UP' AND $row2['desct']!='DRIVER_DOWN')
	{
		if($row2['desct']=='CREDIT-HO')
		{
			$new_balance = $driver_balance - $row2['debit'];

			$update_new_balance = Qry($conn,"UPDATE dairy.driver_book SET balance='$new_balance' WHERE id='$row2[id]'");

			if(!$update_new_balance){
				$flag = false;
				errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			}

			$update_new_balance2 = Qry($conn,"UPDATE dairy.driver SET driver_balance='$new_balance' WHERE code='$driver_code'");

			if(!$update_new_balance2){
				$flag = false;
				errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			}
		}
		else if($row2['desct']=='HISAB-CARRY-FWD')
		{
			$update_new_balance = Qry($conn,"UPDATE dairy.driver_book SET balance='$driver_balance' WHERE id='$row2[id]'");

			if(!$update_new_balance){
				$flag = false;
				errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			}
		}
		else if($row2['desct']=='HISAB-PAID')
		{
			$new_balance = $driver_balance + $row2['credit'];

			$update_new_balance = Qry($conn,"UPDATE dairy.driver_book SET balance='$new_balance' WHERE id='$row2[id]'");

			if(!$update_new_balance){
				$flag = false;
				errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			}

			$update_new_balance2 = Qry($conn,"UPDATE dairy.driver SET driver_balance='$new_balance' WHERE code='$driver_code'");

			if(!$update_new_balance2){
				$flag = false;
				errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			}
		}
		else
		{
			if($row2['credit']>0)
			{
				$new_balance = $driver_balance + $row2['credit']; 

				$update_new_balance = Qry($conn,"UPDATE dairy.driver_book SET balance='$new_balance' WHERE id='$row2[id]'");

				if(!$update_new_balance){
					$flag = false;
					errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
				}

				$update_new_balance2 = Qry($conn,"UPDATE dairy.driver SET driver_balance='$new_balance' WHERE code='$driver_code'");

				if(!$update_new_balance2){
					$flag = false;
					errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
				}

			}
			else if($row2['debit']>0)
			{
				$new_balance = $driver_balance - $row2['debit'];

				$update_new_balance = Qry($conn,"UPDATE dairy.driver_book SET balance='$new_balance' WHERE id='$row2[id]'");

				if(!$update_new_balance){
					$flag = false;
					errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
				}

				$update_new_balance2 = Qry($conn,"UPDATE dairy.driver SET driver_balance='$new_balance' WHERE code='$driver_code'");

				if(!$update_new_balance2){
					$flag = false;
					errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
				}
			}
			else if($row2['desct']=='HISAB-CARRY-FWD')
			{
				$update_new_balance = Qry($conn,"UPDATE dairy.driver_book SET balance='$driver_balance' WHERE id='$row2[id]'");

				if(!$update_new_balance){
					$flag = false;
					errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
				}
			}
		}
	}
	else
	{
		// if($row2['desct']=='DRIVER_UP' AND $row2['credit']!=0 AND $row2['id']!=$first_id)
		// {
			// $new_balance = $driver_balance + ($row2['credit']);

			// $update_new_balance = Qry($conn,"UPDATE dairy.driver_book SET balance='$new_balance' WHERE id='$row2[id]'");

			// if(!$update_new_balance){
				// $flag = false;
				// errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			// }

			// $update_new_balance2 = Qry($conn,"UPDATE dairy.driver SET driver_balance='$new_balance' WHERE code='$driver_code'");

			// if(!$update_new_balance2){
				// $flag = false;
				// errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			// }
		// }
		
		if($row2['desct']=='DRIVER_UP' AND $row2['id']!=$first_id)
		{
			$update_new_balance = Qry($conn,"UPDATE dairy.driver_book SET credit='$driver_balance',balance='$driver_balance' WHERE id='$row2[id]'");

			if(!$update_new_balance){
				$flag = false;
				errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			}
		}
		
		if($row2['desct']=='DRIVER_DOWN' AND $row2['id']!=$first_id)
		{
			$update_new_balance = Qry($conn,"UPDATE dairy.driver_book SET credit='$driver_balance',balance='$driver_balance' WHERE id='$row2[id]'");

			if(!$update_new_balance){
				$flag = false;
				errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			}
		}
	}
}

$get_all_trip_nos = Qry($conn,"SELECT DISTINCT trip_no FROM dairy.driver_book WHERE id>='$first_id' AND driver_code='$driver_code' AND trip_no!=''");

if(!$get_all_trip_nos){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

while($row_all_trips = fetchArray($get_all_trip_nos))
{
	$get_opening = Qry($conn,"SELECT credit,debit,balance FROM dairy.driver_book WHERE id=(SELECT min(id) 
	FROM dairy.driver_book WHERE trip_no='$row_all_trips[trip_no]' AND desct NOT IN('DRIVER_UP','DRIVER_DOWN'))");
	
	if(!$get_opening){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
	
	$row_get_opening = fetchArray($get_opening);
	
	$opening_new = ($row_get_opening['balance']) + ($row_get_opening['debit']) - ($row_get_opening['credit']);
	
	$get_closing = Qry($conn,"SELECT credit,debit,balance FROM dairy.driver_book WHERE id=(SELECT max(id) 
	FROM dairy.driver_book WHERE trip_no='$row_all_trips[trip_no]' AND desct NOT IN('DRIVER_UP','DRIVER_DOWN'))");
	
	if(!$get_closing){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
	
	$row_get_closing = fetchArray($get_closing);
	
	$closing_new = $row_get_closing['balance'];
	
	$update_new_opening_closing = Qry($conn,"UPDATE dairy.opening_closing SET opening='$opening_new',closing='$closing_new' 
	WHERE trip_no='$row_all_trips[trip_no]'");
	
	if(!$update_new_opening_closing){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
}

$get_driver_balance_end = Qry($conn,"SELECT driver_balance FROM dairy.driver WHERE code='$driver_code'");

if(!$get_driver_balance_end){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

$row_balance_end = fetchArray($get_driver_balance_end);

$driver_balance_end = $row_balance_end['driver_balance'];

$update_driver_balance_new = Qry($conn,"UPDATE dairy.driver_up SET amount_hold='$driver_balance_end' WHERE code='$driver_code' ORDER BY id DESC LIMIT 1");

if(!$update_driver_balance_new){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

$update_driver = Qry($conn,"UPDATE dairy.driver SET open_close_verified='1' WHERE id='$driver_id'");

if(!$update_driver){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}
				
if($flag)
{
	AlertRightCornerSuccess("OK : Success !");
	MySQLCommit($conn);
	closeConnection($conn);
	echo "<script>$('#search_btn').attr('disabled',false);</script>";
	exit();
}
else
{
	MySQLRollBack($conn);
	closeConnection($conn);
	AlertError("Error While Processing Request !");
	exit();
}
?>