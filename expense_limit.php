<?php include("header.php"); ?>

<?php
$chk = Qry($conn,"SELECT id FROM _access_control WHERE username='$_SESSION[ediary_fix_admin]' AND func_id=(SELECT id FROM 
_access_control_func_list WHERE session_role='8' AND func_name='Expense_limit') AND u_view='1'");
			  
if(numRows($chk)==0)
{
	echo "<script>window.location.href='./';</script>";
	exit();
}
?>

<div class="content-wrapper">
      <section class="content-header">
          <h1 style="font-size:16px;">Manage Expense Limit : </h1>
       </section> 
       
	   <section class="content">
          <div class="row">
            <div class="col-xs-12">
			<div class="box">
                <div class="box-body">
<?php
$chk_insert = Qry($conn,"SELECT id FROM _access_control WHERE username='$_SESSION[ediary_fix_admin]' AND func_id=(SELECT id FROM 
_access_control_func_list WHERE session_role='8' AND func_name='Expense_limit') AND u_insert='1'");
			  
if(numRows($chk_insert)>0)
{
?>		
					<div class="col-md-12">
					<div class="row">
						
						<div class="form-group col-md-3">
							<label>Select Expense <font color="red"><sup>*</sup></font></label>
							<select style='font-size:12px !important' name="expense" data-size="8" id="expense" class="form-control selectpicker" data-live-search="true" required="required">
								<option style='font-size:12px !important' data-tokens="" value="">--select expense--</option>
								<?php
								$qry1 = Qry($conn,"SELECT exp_code,name FROM dairy.exp_head ORDER BY name ASC");
								
								while($row1 = fetchArray($qry1))
								{
									echo "<option style='font-size:12px !important' data-tokens='$row1[exp_code]#$row1[name]' value='$row1[exp_code]#$row1[name]'>$row1[name] - $row1[exp_code]</option>";
								}
								?>
							</select>
						</div>
						
						<div class="form-group col-md-2">
							<label>Limit Type ? <font color="red"><sup>*</sup></font></label>
							<select name="limit_type" id="limit_type" class="form-control" required="required">
								<option value="">--select option--</option>
								<option value="1">Amount Limit</option>
								<option value="2">Entry Limit</option>
							</select>
						</div>
						
						<div class="form-group col-md-3">
							<label>Limit Value <font color="red"><sup>*</sup></font></label>
							<input type="number" class="form-control" required id="limit_value">
						</div>
						
						<div class="form-group col-md-2">
							<label>Limit For ? <font color="red"><sup>*</sup></font></label>
							<select name="limit_for" id="limit_for" class="form-control" required="required">
								<option value="">--select option--</option>
								<option value="BRANCH">Branch</option>
								<option value="SUPERVISOR">Supervisor</option>
							</select>
						</div>
						
						<div class="form-group col-md-2">
							<?php if(!isMobile()) { echo "<label>&nbsp;</label><br />"; } ?>
							<button type="button" onclick="AddUserFunc()" class="btn btn-sm btn-success <?php if(isMobile()) { echo "btn-block"; } ?>" id="add_btn"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> &nbsp; Set Limit</button>
						</div>
					</div>
				</div>
				
<script>				
function AddUserFunc()
{
	var exp = $('#expense').val();
	var limit_type = $('#limit_type').val();
	var limit_value = $('#limit_value').val();
	var limit_for = $('#limit_for').val();
	
	if(exp=='' || limit_type=='' || limit_value='' || limit_for=='')
	{
		Swal.fire({icon: 'warning',html: '<font size=\'2\' color=\'black\'>Fill all fields first !</font>',});
	}
	else
	{
		$('#loadicon').show();
		jQuery.ajax({
			url: "save_expense_limit.php",
			data: 'exp=' + exp + '&limit_type=' +  limit_type + '&limit_value=' +  limit_value + '&limit_for=' +  limit_for,
			type: "POST",
			success: function(data) {
			$("#func_result").html(data);
			},
			error: function() {}
		});
	}
}
</script>				
	<?php
}
?>	
				<div class="col-md-12">&nbsp;</div>
				
				<div class="col-md-12" id="load_table">
				</div>
				
				 
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

<div id="func_result"></div>  
<div id="load_modal_div"></div>  
<div id="func_result2"></div>  
 
 <script>	
function LoadTable()
{
	jQuery.ajax({
		url: "_load_exp_limit.php",
		data: 'id=' + 'ok',
		type: "POST",
		success: function(data) {
			$("#load_table").html(data);
			$('#example').DataTable({ 
				"destroy": true, //use for reinitialize datatable
			});
		},
		error: function() {}
	});
}
LoadTable();

</script>

<?php
$chk_update = Qry($conn,"SELECT id FROM _access_control WHERE username='$_SESSION[ediary_fix_admin]' AND func_id=(SELECT id FROM 
_access_control_func_list WHERE session_role='8' AND func_name='Expense_limit') AND u_update='1'");
			  
if(numRows($chk_update)>0)
{
?>
<script>
function ModifyExp(id)
{
	$('#loadicon').show();
		jQuery.ajax({
			url: "modal_edit_exp_limit.php",
			data: 'id=' + id,
			type: "POST",
			success: function(data) {
			$("#func_result").html(data);
			},
			error: function() {}
		});
}
</script>

<?php
}
?>

<?php include("footer.php") ?>