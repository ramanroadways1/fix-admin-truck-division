<?php
require_once("./connect.php");

$id = escapeString($conn,$_POST['id']);
$timestamp = date("Y-m-d H:i:s");

StartCommit($conn);
$flag = true;

$update = Qry($conn,"UPDATE dairy.opening_closing SET hisab_check='1',hisab_check_timestamp='$timestamp' WHERE id='$id'");

if(!$update)
{
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}
	
if($flag)
{
	MySQLCommit($conn);
	closeConnection($conn);
	echo "<script>
		$('#approve_btn_$id').html('Approved');
		$('#approve_btn_$id').attr('onclick','');
		$('#loadicon').fadeOut('slow');
	</script>";
}
else
{
	MySQLRollBack($conn);
	closeConnection($conn);
	AlertError("Error While Processing Request !");
	echo "<script>$('#approve_btn_$id').attr('disabled',false);</script>";
	exit();
}
?>