<?php
require_once 'connect.php';

$lrno = escapeString($conn,($_POST['lrno']));
$tno = escapeString($conn,($_POST['tno']));

$from_date = date('Y-m-d', strtotime(escapeString($conn,$_POST['duration']), strtotime(date("Y-m-d"))));
$to_date = date("Y-m-d");
$max_date = date("Y-m-d");

if($lrno!='')
{
	if(substr($lrno,3,3)=='OLR')
	{
		$sql = Qry($conn,"SELECT date as lr_date,truck_no as tno,date as create_date,frno as vou_no,fstation as from_loc,tstation as to_loc 
		FROM freight_form_lr WHERE frno='$lrno'");
		
		$type1 = 'OWN';
	}
	else
	{
		$sql = Qry($conn,"SELECT lrdate as lr_date,tno,date as create_date,bilty_no as vou_no,frmstn as from_loc,tostn as to_loc FROM mkt_bilty 
		WHERE bilty_no='$lrno'");
	
		$type1 = 'MARKET';
	}
	
	if(!$sql){
		echo getMySQLError($conn);
		errorLog(getMySQLError($conn),$conn,$page_url,__LINE__);
		exit();
	}
	
	if(numRows($sql)==0)
	{
		AlertRightCornerError("No record found !");
		exit();
	}
	
	?>
	<table id="example" class="table table-bordered table-striped" style="font-size:13px;">
        <thead>
			<tr>
				<th>Vou_No</th>
				<th>Vehicle_No</th>
				<th>LR_Date</th>
				<th>Create_Date/Current_Validity</th>
				<th>From</th>
				<th>To</th>
				<th>#Select_Validity</th>
				<th>#Save</th>
			</tr>
		</thead>
    
	<tbody id=""> 
<?php
	$sn=1;
	
	while($row = fetchArray($sql))
	{	
			$lr_date = date('d-m-y', strtotime($row['lr_date']));
			$create_date = date('d-m-y', strtotime($row['create_date']));
			
			echo "<tr>	
				<td>$row[vou_no]</td>
				<td>$row[tno]</td>
				<td>$lr_date</td>
				<td>$create_date</td>
				<td>$row[from_loc]</td>
				<td>$row[to_loc]</td>
				<td><input id='create_date_$row[vou_no]' type='date' value='$row[create_date]' class='form-control' max='$max_date' required pattern='[0-9]{4}-[0-9]{2}-[0-9]{2}' /></td>
				<td><button class='btn update_btn_1 btn-xs btn-success' type='button' onclick=SaveExtend('$row[vou_no]','$type1')><i class='fa fa-floppy-o' aria-hidden='true'></i> Save</button></td>
			</tr>";
			
	$sn++;		
	}
		echo "</tbody>
	</table>";
}
else if($tno!='')
{
	$sql1 = Qry($conn,"SELECT date as lr_date,truck_no as tno,date as create_date,frno as vou_no,fstation as from_loc,tstation as to_loc 
	FROM freight_form_lr WHERE create_date BETWEEN '$from_date' AND '$to_date' AND truck_no='$tno' GROUP BY frno");
	
	if(!$sql1){
		echo "Qry1 : ".getMySQLError($conn);
		errorLog(getMySQLError($conn),$conn,$page_url,__LINE__);
		exit();
	}
	
	$sql2 = Qry($conn,"SELECT lrdate as lr_date,tno,date as create_date,bilty_no as vou_no,frmstn as from_loc,tostn as to_loc FROM mkt_bilty WHERE 
	date BETWEEN '$from_date' AND '$to_date' AND tno='$tno'");
	
	if(!$sql2){
		echo "Qry2 : ".getMySQLError($conn);
		errorLog(getMySQLError($conn),$conn,$page_url,__LINE__);
		exit();
	}
	?>
	<table id="example" class="table table-bordered table-striped" style="font-size:13px;">
        <thead>
			<tr>
				<th>Vou_No</th>
				<th>Vehicle_No</th>
				<th>LR_Date</th>
				<th>Create_Date/Current_Validity</th>
				<th>From</th>
				<th>To</th>
				<th>#Select_Validity</th>
				<th>#Save</th>
			</tr>
		</thead>
    
	<tbody id=""> 
<?php
	// $sn=1;
	
	while($row = fetchArray($sql1))
	{	
			$lr_date = date('d-m-y', strtotime($row['lr_date']));
			$create_date = date('d-m-y', strtotime($row['create_date']));
			
			echo "<tr>	
				<td>$row[vou_no]</td>
				<td>$row[tno]</td>
				<td>$lr_date</td>
				<td>$create_date</td>
				<td>$row[from_loc]</td>
				<td>$row[to_loc]</td>
				<td><input id='create_date_$row[vou_no]' type='date' value='$row[create_date]' class='form-control' max='$max_date' required pattern='[0-9]{4}-[0-9]{2}-[0-9]{2}' /></td>
				<td><button class='btn update_btn_1 btn-xs btn-success' type='button' onclick=SaveExtend('$row[vou_no]','OWN')><i class='fa fa-floppy-o' aria-hidden='true'></i> Save</button></td>
			</tr>";
			
	// $sn++;		
	}
	
	while($row2 = fetchArray($sql1))
	{	
			$lr_date = date('d-m-y', strtotime($row2['lr_date']));
			$create_date = date('d-m-y', strtotime($row2['create_date']));
			
			echo "<tr>	
				<td>$row2[vou_no]</td>
				<td>$row2[tno]</td>
				<td>$lr_date</td>
				<td>$create_date</td>
				<td>$row2[from_loc]</td>
				<td>$row2[to_loc]</td>
				<td><input id='create_date_$row2[vou_no]' value='$row[create_date]' type='date' class='form-control' max='$max_date' required pattern='[0-9]{4}-[0-9]{2}-[0-9]{2}' /></td>
				<td><button class='btn update_btn_1 btn-xs btn-success' type='button' onclick=SaveExtend('$row2[vou_no]','MARKET')><i class='fa fa-floppy-o' aria-hidden='true'></i> Save</button></td>
			</tr>";
			
	// $sn++;		
	}
		echo "</tbody>
	</table>";
}
else
{
	AlertRightCornerError("Invalid inputs !");
	exit();
}
	
$chk_update = Qry($conn,"SELECT id FROM _access_control WHERE username='$_SESSION[ediary_fix_admin]' AND func_id=(SELECT id FROM 
_access_control_func_list WHERE session_role='8' AND func_name='Extend_Vou_Val') AND u_update='1'");
			  
if(numRows($chk_update)==0)
{
	echo "<script>
		$('.update_btn_1').attr('disabled',true);
		$('.update_btn_1').attr('onclick','');
	</script>";
}
?>
	
<script> 
	$("#loadicon").fadeOut('slow');
</script> 
