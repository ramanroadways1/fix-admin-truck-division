<?php
require_once 'connect.php';

$sql = Qry($conn,"SELECT m.route_type,m.timestamp,m.branch,m.branch_user,s.name as from_loc,s2.name as to_loc 
FROM dairy.master_lane AS m 
LEFT JOIN station as s ON s.id=m.from_id 
LEFT JOIN station as s2 ON s2.id=m.to_id");

if(!$sql){
	echo getMySQLError($conn);
	errorLog(getMySQLError($conn),$conn,$page_url,__LINE__);
	exit();
}

?>
	<table id="example" class="table table-bordered table-striped" style="font-size:13px;">
        <thead>
		<tr>
			<th>#</th>
			<th>From</th>
			<th>To</th>
			<th>Route Type</th>
			<th>Branch</th>
			<th>User</th>
			<th>Timestamp</th>
		</tr>
		</thead>
       <tbody id=""> 
<?php
if(numRows($sql)==0)
{
	echo "<tr>
		<td colspan='7'>No record found !</td>
	</tr>";  
}
else
{
	$sn=1;
	while($row = fetchArray($sql))
	{
		echo "<tr>
			<td>$sn</td>
			<td>$row[from_loc]</td>
			<td>$row[to_loc]</td>
			<td>$row[route_type]</td>
			<td>$row[branch]</td>
			<td>$row[branch_user]</td>
			<td>".date("d-m-y H:i A",strtotime($row["timestamp"]))."</td>
		</tr>
		";
		$sn++;		
     }
}
?>
	</tbody>
		</table>
<script> 
// $("#loadicon").fadeOut('slow');
$(document).ready(function() {
    $('#example').DataTable( {
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ]
    } );
} );
</script> 