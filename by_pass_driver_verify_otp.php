<?php
include("header.php");

$chk = Qry($conn,"SELECT id FROM _access_control WHERE username='$_SESSION[ediary_fix_admin]' AND func_id=(SELECT id FROM 
_access_control_func_list WHERE session_role='8' AND func_name='Report_Driver_Book') AND u_view='1'");
			  
if(numRows($chk)==0)
{
	echo "<script>window.location.href='./';</script>";
	exit();
}
?>
<script>
$(function() {
		$("#driver_name").autocomplete({
		source: 'autofill/get_own_truck_driver.php',
		// appendTo: "#LaneRuleModal",
		select: function (event, ui) { 
              $('#driver_name').val(ui.item.value);   
              $('#driver_id').val(ui.item.id);   
              $('#last_verified_on').val(ui.item.last_verify);  
			// $("#button1").attr('disabled',false);				   
             return false;
		},
		change: function (event, ui) {
		if(!ui.item){
		    $(event.target).val("");
			alert('Vehicle Number does not exists.');
			$("#driver_name").val('');
			$("#driver_id").val('');
			$("#last_verified_on").val('');
			$("#driver_name").focus();
			// $("#button1").attr('disabled',true);
		}}, 
		focus: function (event, ui){
			return false;}
});});
</script>

<div class="content-wrapper">
      <section class="content-header">
          <h1 style="font-size:16px;">Report : Driver Book </h1>
       </section>
       
	   <section class="content">
          <div class="row">
            <div class="col-xs-12">
			<div class="box">
                <div class="box-body">
				
				<div class="col-md-12">&nbsp;</div>
				
				<div class="col-md-12">
					<div class="row">
						<div class="form-group col-md-3">
							<label style="font-size:12px !important">Driver Name</label>
							<input style="font-size:12px !important" autocomplete="off" oninput="this.value=this.value.replace(/[^A-Z .a-z0-9]/,'')" type="text" class="form-control" id="driver_name" />
						</div>
						
						<input type="hidden" id="driver_id" name="driver_id">
						
						<div class="form-group col-md-3">
							<label style="font-size:12px !important">Last Verified On</label>
							<input id="last_verified_on" type="text" class="form-control" readonly />
						</div>
		 				
						<div class="form-group col-md-3">
							<label style="font-size:12px !important">Last Verified On</label>
							<input id="last_verified_on_1" type="date" name="date" min="<?php echo date('Y-m-d', strtotime("+1 day")); ?>" max="<?php echo date('Y-m-d', strtotime("+3 day")); ?>" class="form-control" required pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}" />
						</div>
						
						<div class="form-group col-md-3">
							<?php if(!isMobile()) { echo "<label>&nbsp;</label><br />"; } ?>
							<button type="button" style="margin-top:1px;font-size:12px !important" onclick="SaveResult()" class="btn btn-sm btn-success <?php if(isMobile()) { echo "btn-block"; } ?>" id="add_btn"><i class="fa fa-save" aria-hidden="true"></i> &nbsp; Save</button>
						</div>
					</div>
				</div>
				
				<div class="col-md-12">&nbsp;</div>
				
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

<div id="func_result"></div>  

<script>	
function SaveResult()
{
	var date1 = $('#last_verified_on_1').val();
	var last_verified_on = $('#last_verified_on').val();
	var driver_id = $('#driver_id').val();
	
	if(date1=='')
	{
		Swal.fire({icon: 'warning',html: '<font size=\'2\' color=\'black\'>Select date first !</font>',});
	}
	else
	{
		$('#loadicon').show();
				jQuery.ajax({
					url: "save_by_pass_driver_verify_otp.php",
					data: 'date1=' + date1 + '&driver_id=' + driver_id,
					type: "POST",
					success: function(data) {
						$("#func_result").html(data);
					},
					error: function() {}
		});
	}
}
</script>

<?php include("footer.php") ?>
