<?php
require_once("./connect.php");

$id = escapeString($conn,($_POST['id']));

$get_data = Qry($conn,"SELECT trip_id,trans_id,tno,amount,date,branch,timestamp FROM dairy.driver_naame WHERE id='$id'");

if(!$get_data)
{
	AlertErrorTopRight("Error while processing request !");
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	echo "<script> $('#dlt_naame_btn_$id').attr('disabled',false); </script>";
	exit();
}

if(numRows($get_data) == 0)
{
	AlertErrorTopRight("Entry not found !");
	echo "<script> $('#dlt_naame_btn_$id').attr('disabled',false); </script>";
	exit();
}

$row = fetchArray($get_data);

$tno = $row['tno'];
$trip_id = $row['trip_id'];
$trans_id = $row['trans_id'];
$amount = $row['amount'];
$trans_date = $row['date'];

require_once("./check_cache.php");

$chk_trip = Qry($conn,"SELECT id FROM dairy.trip WHERE id='$trip_id'");

if(!$chk_trip)
{
	AlertErrorTopRight("Error while processing request !");
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	echo "<script> $('#dlt_naame_btn_$id').attr('disabled',false); </script>";
	exit();
}

if(numRows($chk_trip)==0)
{
	AlertErrorTopRight("Running trip not found !");
	echo "<script> $('#dlt_naame_btn_$id').attr('disabled',false); </script>";
	exit();
}

StartCommit($conn);
$flag = true;

$fetch_nrr = Qry($conn,"SELECT lrno,claim_vou_no,driver_code,naame_type,qty,rate,amount,narration FROM dairy.driver_naame WHERE id='$id'");

if(!$fetch_nrr)
{
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

$row_nrr = fetchArray($fetch_nrr);

$lrno_claim = $row_nrr['lrno'];
$claim_vou_no = $row_nrr['claim_vou_no'];

$delete = Qry($conn,"DELETE FROM dairy.driver_naame WHERE id='$id'");

if(!$delete)
{
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

if($naame_type=='CLAIM')
{
	if(substr($lrno_claim,3,1)=='M' || substr($lrno_claim,3,1)=='m')
	{
		$fetch_bilty_book_trans = Qry($conn,"SELECT id FROM dairy.bilty_book WHERE trans_id='$trans_id'");

		if(!$fetch_bilty_book_trans)
		{
			$flag = false;
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		}
		
		if(numRows($fetch_bilty_book_trans)==0)
		{
			$flag = false;
			errorLog("Txn not found in bilty_book. Transid: $trans_id, LRno: $lrno_claim.",$conn,$page_name,__LINE__);
		}
		
		$row_bilty_book = fetchArray($fetch_bilty_book_trans);
		
		$update_bal_1 = Qry($conn,"UPDATE dairy.bilty_book SET balance=balance+'$amount' WHERE id>'$row_bilty_book[id]' AND bilty_no='$lrno_claim'");

		if(!$update_bal_1)
		{
			$flag = false;
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		}
		
		$update_bal_2 = Qry($conn,"UPDATE dairy.bilty_balance SET rcvd=rcvd-'$amount',balance=balance+'$amount' WHERE bilty_no='$lrno_claim'");

		if(!$update_bal_2)
		{
			$flag = false;
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		}
		
		$dlt_record = Qry($conn,"DELETE FROM dairy.bilty_book WHERE id='$row_bilty_book[id]'");

		if(!$dlt_record)
		{
			$flag = false;
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		}
	}
	else
	{
		$fetch_claim_trans = Qry($conn,"SELECT id,lrno,vou_no FROM claim_book_trans WHERE trans_id='$trans_id'");

		if(!$fetch_claim_trans)
		{
			$flag = false;
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		}
		
		if(numRows($fetch_claim_trans)==0)
		{
			$flag = false;
			errorLog("Txn not found in claim_book. Transid: $trans_id, LRno: $lrno_claim.",$conn,$page_name,__LINE__);
		}
		
		$row_claim_book = fetchArray($fetch_claim_trans);
		
		$update_balance_claim_book = Qry($conn,"UPDATE claim_book_trans SET balance=balance+'$amount' WHERE id>'$row_claim_book[id]' AND 
		lrno='$row_claim_book[lrno]' AND vehicle_type='OWN' AND vou_no='$row_claim_book[vou_no]'");

		if(!$update_balance_claim_book)
		{
			$flag = false;
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		}
		
		// $update_claim_balance = Qry($conn,"UPDATE claim_records_admin SET rcvd_amount=rcvd_amount-'$amount',
		// balance_amount=balance_amount+'$amount' WHERE lrno='$lrno_claim'");

		// if(!$update_claim_balance)
		// {
			// $flag = false;
			// errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		// }
		
		$dlt_record = Qry($conn,"DELETE FROM claim_book_trans WHERE id='$row_claim_book[id]'");

		if(!$dlt_record)
		{
			$flag = false;
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		}
	}
}

if($naame_type=='FREIGHT')
{
	if(substr($lrno_claim,3,1)=='M' || substr($lrno_claim,3,1)=='m')
	{
		$fetch_bilty_book_trans = Qry($conn,"SELECT id FROM dairy.bilty_book WHERE trans_id='$trans_id'");

		if(!$fetch_bilty_book_trans)
		{
			$flag = false;
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		}
		
		if(numRows($fetch_bilty_book_trans)==0)
		{
			$flag = false;
			errorLog("Txn not found in bilty_book. Transid: $trans_id, LRno: $lrno_claim.",$conn,$page_name,__LINE__);
		}
		
		$row_bilty_book = fetchArray($fetch_bilty_book_trans);
		
		$update_bal_1 = Qry($conn,"UPDATE dairy.bilty_book SET balance=balance+'$amount' WHERE id>'$row_bilty_book[id]' AND bilty_no='$lrno_claim'");

		if(!$update_bal_1)
		{
			$flag = false;
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		}
		
		$update_bal_2 = Qry($conn,"UPDATE dairy.bilty_balance SET rcvd=rcvd-'$amount',balance=balance+'$amount' WHERE bilty_no='$lrno_claim'");

		if(!$update_bal_2)
		{
			$flag = false;
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		}
		
		$dlt_record = Qry($conn,"DELETE FROM dairy.bilty_book WHERE id='$row_bilty_book[id]'");

		if(!$dlt_record)
		{
			$flag = false;
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		}
	}
}

$naame_type = $row_nrr['naame_type'];

$log_data = "Type : $row_nrr[naame_type], QTY : $row_nrr[qty], ClaimLRno : $lrno_claim, Narr : $row_nrr[narration], TruckNo : $tno, 
Amount : $row_nrr[amount], Trans_date : $trans_date, Trans_id : $trans_id, Trip_id : $trip_id.";

$insert_log = Qry($conn,"INSERT INTO dairy.ediary_admin_log(code,action,desct,timestamp) VALUES ('$row_nrr[driver_code]','NAAME_DELETE',
'$log_data','$timestamp')");

if(!$insert_log){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

	$select_book_id = Qry($conn,"SELECT id,driver_code FROM dairy.driver_book WHERE trans_id='$trans_id'");
	
	if(!$select_book_id)
	{
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
	
	if(numRows($select_book_id)==0)
	{
		$flag = false;
		errorLog("Transaction not found in driver book. Trans_id: $trans_id.",$conn,$page_name,__LINE__);
	}
	
	$row_book_id = fetchArray($select_book_id);
	
	$book_id = $row_book_id['id'];
	$driver_code = $row_book_id['driver_code'];
	
	$update_book = Qry($conn,"UPDATE dairy.driver_book SET balance=balance-'$amount' WHERE id>'$book_id' AND tno='$tno'");
	
	if(!$update_book)
	{
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
	
	$delete_row = Qry($conn,"DELETE FROM dairy.driver_book WHERE id='$book_id'");
		
	if(!$delete_row)
	{
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);	
	}
	
	if(AffectedRows($conn)==0)
	{
		$flag = false;
		errorLog("Transaction not found in driver book. Trans_id: $trans_id. DriverBook Id: $book_id.",$conn,$page_name,__LINE__);
	}
	
	$update_driver_balance = Qry($conn,"UPDATE dairy.driver_up SET amount_hold=amount_hold-'$amount' WHERE down=0 AND code='$driver_code' 
	ORDER BY id DESC LIMIT 1");
	
	if(!$update_driver_balance)
	{
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);	
	}
	
	$update_trip = Qry($conn,"UPDATE dairy.trip SET driver_naame=driver_naame-'$amount' WHERE id='$trip_id'");
			
	if(!$update_trip)
	{
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);	
	}
	
// if($naame_type=='CLAIM')
// {
	// $claim_book_entry = Qry($conn,"SELECT id,");
	
	// if(!$claim_book_entry)
	// {
		// $flag = false;
		// errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);	
	// }
// }
// else if($naame_type=='FREIGHT')
// {
	
// }
		
if($flag)
{
		MySQLCommit($conn);
		closeConnection($conn);
		
		AlertRightCornerSuccess("Deleted Successfully !");
		echo "<script>
			ReLoadPage();
		</script>";
		exit();
}
else
{
		MySQLRollBack($conn);
		closeConnection($conn);
		
		AlertErrorTopRight("Error while processing Request !");
		echo "<script> $('#dlt_naame_btn_$id').attr('disabled',false); </script>";
		exit();
}		
?>