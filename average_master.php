<?php
include "header.php";

$chk = Qry($conn,"SELECT id FROM _access_control WHERE username='$_SESSION[ediary_fix_admin]' AND func_id=(SELECT id FROM 
_access_control_func_list WHERE session_role='8' AND func_name='Avg_Master') AND u_view='1'");
			  
if(numRows($chk)==0)
{
	echo "<script>window.location.href='./';</script>";
	exit();
}
?>
<script>
$(function() {
		$("#truck_no").autocomplete({
		source: 'autofill/get_tno.php',
		// appendTo: '#appenddiv',
		select: function (event, ui) { 
            $('#truck_no').val(ui.item.value);   
            return false;},
		change: function (event, ui) {
		if(!ui.item){
			$(event.target).val("");
            $(event.target).focus();
			$('#truck_no').val("");   
			alert('Vehicle does not exists.');
		}}, 
	focus: function (event, ui){
	return false;
	}
});});
</script>

<style>
label{
	font-size:12px;
}
</style>

<div class="content-wrapper">
    <section class="content-header">
      <span style="font-size:15px">Average Master: </span>
	</section>
		
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box"> 
			<div class="box-body">

<div class="row">

<div class="from-group col-md-12">

	<div class="from-group col-md-2">
		<label>Vehicle No</label>
		<input style="font-size:12px" type="text" id="truck_no" class="form-control">
	</div>

	<div class="from-group col-md-2">
		<label>Route type</label>
		<select style="font-size:12px" id="route_type" class="form-control">
			<option style="font-size:12px" value="">--select--</option>
			<option style="font-size:12px" value="normal">normal</option>
			<option style="font-size:12px" value="congested">congested</option>
		</select>
	</div>

	<div class="from-group col-md-2">
		<label>Empty Avg</label>
		<input style="font-size:12px" step="any" type="number" id="empty_avg" class="form-control">
	</div>

	<div class="from-group col-md-2">
		<label>Loaded Avg</label>
		<input style="font-size:12px" step="any" type="number" id="loaded_avg" class="form-control">
	</div>

	<div class="from-group col-md-2">
		<label>Overload Avg</label>
		<input style="font-size:12px" step="any" type="number" id="overload_avg" class="form-control">
	</div>

	<div class="from-group col-md-2">
		<label>&nbsp;</label>
		<br />
		<button type="button" id="btn_save" onclick="AddAvg()" class="btn btn-primary btn-sm">Add new</button>
	</div>
</div>

<div class="form-group col-md-12"></div>

<div class="form-group col-md-12">
   <div class="form-group col-md-12 table-responsive" id="load_table_div"></div>
</div>

  </div>
</div>

</body>
</html>

          </div>
        </div>
	</div>
</section>
</div>    
	
<script type="text/javascript">
function AddAvg()
{
	var truck_no = $('#truck_no').val();
	var route_type = $('#route_type').val();
	var empty_avg = $('#empty_avg').val();
	var loaded_avg = $('#loaded_avg').val();
	var overload_avg = $('#overload_avg').val();
	
	if(truck_no=='')
	{
		Swal.fire({icon: 'warning',html: '<font size=\'2\' color=\'black\'>Enter vehicle number first !</font>',});
	}
	else if(route_type=='')
	{
		Swal.fire({icon: 'warning',html: '<font size=\'2\' color=\'black\'>Select route type first !</font>',});
	}
	else if(empty_avg=='')
	{
		Swal.fire({icon: 'warning',html: '<font size=\'2\' color=\'black\'>Enter empty average first !</font>',});
	}
	else if(loaded_avg=='')
	{
		Swal.fire({icon: 'warning',html: '<font size=\'2\' color=\'black\'>Enter loaded average first !</font>',});
	}
	else if(overload_avg=='')
	{
		Swal.fire({icon: 'warning',html: '<font size=\'2\' color=\'black\'>Enter overloaded average first !</font>',});
	}
	else
	{
		$('#btn_save').attr('disabled',true);
		$("#loadicon").show();
			jQuery.ajax({
			url: "./save_avg_data.php",
			data: 'truck_no=' + truck_no + '&route_type=' + route_type + '&empty_avg=' + empty_avg + '&loaded_avg=' + loaded_avg + '&overload_avg=' + overload_avg,
			type: "POST",
			success: function(data) {
				$("#func_result2").html(data);
			},
			error: function() {}
		});
	}
}

function LoadTable()
{
	// $("#loadicon").show();
	jQuery.ajax({
	url: "_load_average_master.php",
	data: 'ok=' + 'ok',
	type: "POST",
	success: function(data) {
		$("#load_table_div").html(data);
		$('#example').DataTable({ 
        "destroy": true, //use for reinitialize datatable
    });
	},
	error: function() {}
	});
}
</script>

<script>
LoadTable();
</script>

<div id="func_result2"></div>

<?php
include "footer.php";
?>