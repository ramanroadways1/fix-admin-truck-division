<?php
require_once("connect.php");

$date=date("Y-m-d");
$timestamp=date("Y-m-d H:i:s");

$id = escapeString($conn,($_POST['id']));
$adv_amount = escapeString($conn,($_POST['adv_amount']));
$diesel_type = escapeString($conn,($_POST['diesel_type']));
$diesel_value = escapeString($conn,($_POST['diesel_value']));

if($adv_amount<0)
{
	AlertError("Invalid amount !");
	echo "<script>
		$('#button_update_adv_diesel').attr('disabled',false);
	</script>"; 
	exit();
}

if($diesel_type!='Fix_Amount' AND $diesel_type!='Fix_Qty' AND $diesel_type!='Max_Amount' AND $diesel_type!='Max_Qty' AND $diesel_type!='No_Diesel')
{
	AlertError("Error while processing request !");
	echo "<script>
		Swal.fire({
		  icon: 'error',
		  title: 'Oops...',
		  text: 'Invalid diesel type !!',
		})
		$('#button_update_adv_diesel').attr('disabled',false);
		$('#loadicon').hide();
	</script>"; 
	exit();
}

if($diesel_type!='No_Diesel' AND $diesel_value<=0)
{
	AlertError("Error while processing request !");
	echo "<script>
		Swal.fire({
		  icon: 'error',
		  title: 'Oops...',
		  text: 'Invalid diesel amount/qty !!',
		})
		$('#button_update_adv_diesel').attr('disabled',false);
		$('#loadicon').hide();
	</script>"; 
	exit();
}

$chk_lane = Qry($conn,"SELECT fix_diesel_type,diesel_value,adv_amount FROM dairy.fix_lane_rules WHERE id='$id'");

if(!$chk_lane){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	AlertError("Error while processing request !");
	exit();
}

if(numRows($chk_lane)==0)
{
	AlertError("Error while processing request !");
	echo "<script>
		Swal.fire({
		  icon: 'error',
		  title: 'Oops...',
		  text: 'Rule not found !!',
		})
		$('#loadicon').hide();
	</script>"; 
	exit();
}

$row = fetchArray($chk_lane);	

$update_log=array();
$update_Qry=array();

if($row['adv_amount']!=$adv_amount)
{
	$update_log[]="adv_amount : $row[adv_amount] to $adv_amount";
	$update_Qry[]="adv_amount='$adv_amount'";
}

if($row['fix_diesel_type']!=$diesel_type)
{
	$update_log[]="diesel_type : $row[fix_diesel_type] to $diesel_type";
	$update_Qry[]="fix_diesel_type='$diesel_type'";
}

if($row['diesel_value']!=$diesel_value)
{
	$update_log[]="diesel_value : $row[diesel_value] to $diesel_value";
	$update_Qry[]="diesel_value='$diesel_value'";
}

$update_log = implode(', ',$update_log); 
$update_Qry = implode(', ',$update_Qry); 

if($update_log=="" || empty($update_log))
{
	AlertError("Nothing to update !");
	echo "<script>
		$('#button_update_adv_diesel').attr('disabled',false);
	</script>"; 
	exit();
}

StartCommit($conn);
$flag = true;

$update_Qry_temp = Qry($conn,"UPDATE dairy.fix_lane_rules SET $update_Qry WHERE id='$id'");

if(!$update_Qry_temp){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

$insertLog = Qry($conn,"INSERT INTO dairy.ediary_admin_log(code,action,desct,timestamp) VALUES ('$id','Fix_Rule_Adv_Diesel_Edit','$update_log','$timestamp')");

if(!$insertLog){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

if($flag)
{
	MySQLCommit($conn);
	closeConnection($conn);
	AlertRightCornerSuccess("Rule updated successfully !");
	echo "<script>
		$('#advance_value_row_$id').html('$adv_amount');
		$('#diesel_value_row_$id').html('$diesel_value ($diesel_type)');
		$('#button_update_adv_diesel').attr('disabled',false);
	</script>";
}
else
{
	MySQLRollBack($conn);
	closeConnection($conn);
	AlertError("Error while processing request !");
	echo "<script>
		$('#button_update_adv_diesel').attr('disabled',false);	
	</script>";
	exit();
}
?>