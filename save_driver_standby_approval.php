<?php
require_once("connect.php");

$timestamp = date("Y-m-d H:i:s");

$trip_no = escapeString($conn,strtoupper($_POST['trip_no']));
$narration = escapeString($conn,$_POST['narration']);

if(empty($trip_no))
{
	AlertError("Enter trip number first !");
	echo "<script> $('#add_btn').attr('disabled',false); </script>";
	exit();
}

if(empty($narration))
{
	AlertError("Enter narration first !");
	echo "<script> $('#add_btn').attr('disabled',false); </script>";
	exit();
}

$get_trip_no = Qry($conn,"SELECT tno FROM dairy.trip WHERE trip_no='$trip_no'");
	
if(!$get_trip_no){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	AlertError("Error While Processing Request !");
	echo "<script> $('#add_btn').attr('disabled',false); </script>";
	exit();
}
	
if(numRows($get_trip_no)==0)
{
	AlertError("Running trip not found !");
	echo "<script> $('#add_btn').attr('disabled',false); </script>";
	exit();
}
	
$row_trip = fetchArray($get_trip_no);
$tno = $row_trip['tno'];

$chk = Qry($conn,"SELECT id FROM dairy.driver_standby_approval WHERE trip_no='$trip_no'");

if(!$chk){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	AlertError("Error While Processing Request !");
	echo "<script> $('#add_btn').attr('disabled',false); </script>";
	exit();
}
	
if(numRows($chk)>0)
{
	AlertError("Duplicate record found. !");
	echo "<script> $('#add_btn').attr('disabled',false); </script>";
	exit();
}

StartCommit($conn);
$flag = true;
	
$update_approval = Qry($conn,"UPDATE dairy.opening_closing SET stand_by_approval='1' WHERE trip_no='$trip_no'");
	
if(!$update_approval){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

$insert_record1 = Qry($conn,"INSERT INTO dairy.driver_standby_approval(tno,trip_no,narration,timestamp) VALUES ('$tno',
'$trip_no','$narration','$timestamp')");	
	
if(!$insert_record1){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

if($flag)
{
	MySQLCommit($conn);
	closeConnection($conn);
	AlertRightCornerSuccessFadeFast("Approved !");
	echo "<script>LoadTable();</script>";
	exit();
}
else
{
	MySQLRollBack($conn);
	closeConnection($conn);
	AlertError("Error While Processing Request !");
	echo "<script>$('#submit_btn_model_update').attr('disabled',false);$('#loadicon').fadeOut('slow');</script>";
	exit();
}
?>