<?php
require_once("./connect.php");

$tno = escapeString($conn,strtoupper($_POST['tno']));
$trip_no = escapeString($conn,strtoupper($_POST['trip_no']));
$timestamp1 = date("Y-m-d H:i:s");

$chk = Qry($conn,"SELECT timestamp FROM dairy.hisab_branches_lock_allow WHERE trip_no='$trip_no'");

if(!$chk){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	AlertErrorTopRight("Error While Processing Request !");
	exit();
}

if(numRows($chk)>0)
{
	$row1 = fetchArray($chk);
	
	$hisab_hours =  round((strtotime($timestamp1) - strtotime($row1['timestamp']))/3600, 1);
	
	$last_allowed_timestamp = date("d-m-y h:i A",strtotime($row1['timestamp']));
	
	if($hisab_hours <= 24)
	{
		AlertErrorTopRight("Duplicate entry in Last 24 Hours.<br>Last Allowed on $last_allowed_timestamp !");
		exit();
	}
}

$delete_old_records = Qry($conn,"DELETE FROM dairy.hisab_branches_lock_allow WHERE tno='$tno'");

if(!$delete_old_records){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	AlertErrorTopRight("Error While Processing Request !");
	exit();
}
	
$allow = Qry($conn,"INSERT INTO dairy.hisab_branches_lock_allow(tno,trip_no,timestamp) VALUES ('$tno','$trip_no','$timestamp1')");

if(!$allow){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	AlertErrorTopRight("Error While Processing Request !");
	exit();
}

AlertRightCornerSuccessFadeFast("Allowed !");

echo "<script>
		LoadTable();
</script>";
?>