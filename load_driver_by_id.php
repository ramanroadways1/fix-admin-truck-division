<?php
require_once("connect.php");

$date=date("Y-m-d");
$timestamp=date("Y-m-d H:i:s");

$driver_id = escapeString($conn,($_POST['driver_id']));

$fetch_data = Qry($conn,"SELECT d.mobile,d.mobile2,g.name as guarantor,d.code,d2.acname,d2.acno,d2.bank,d2.ifsc 
FROM dairy.driver AS d 
LEFT OUTER JOIN dairy.driver_ac AS d2 ON d2.code = d.code 
LEFT OUTER JOIN dairy.driver AS g ON g.code = d.guarantor 
WHERE d.id='$driver_id'");

if(!$fetch_data){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	AlertError("Error while processing request !");
	exit();
}
							  
if(numRows($fetch_data)==0)
{
	AlertErrorTopRight("Driver not found !");
	
	echo "<script>
		$('#driver_name').attr('readonly',false);
		$('#submit_btn').attr('disabled',true);
		$('#search_btn').attr('disabled',false);
		
		$('#submit_btn').hide();
	</script>";
	exit();
}

$row = fetchArray($fetch_data);

$_SESSION['driver_edit_code'] = $row['code'];

echo "<script>
		$('#driver_name').attr('readonly',true);
		
		$('#submit_btn').attr('disabled',false);
		$('#submit_btn').show();
		$('#search_btn').attr('disabled',true);
		
		$('#mobile1').val('$row[mobile]');
		$('#mobile2').val('$row[mobile2]');
		$('#driver_code').val('$row[code]');
		$('#guarantor').val('$row[guarantor]');
		$('#ac_holder').val('$row[acname]');
		$('#ac_no').val('$row[acno]');
		$('#bank_name').val('$row[bank]');
		$('#ifsc_code').val('$row[ifsc]');
		
		$('#loadicon').fadeOut('slow');
	</script>";
	exit();
?>