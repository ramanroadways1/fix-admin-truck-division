<?php include("header.php"); ?>

<?php
$chk = Qry($conn,"SELECT id FROM _access_control WHERE username='$_SESSION[ediary_fix_admin]' AND func_id=(SELECT id FROM 
_access_control_func_list WHERE session_role='8' AND func_name='ManageSupervisors') AND u_view='1'");
			  
if(numRows($chk)==0)
{
	echo "<script>window.location.href='./';</script>";
	exit();
}
?>

<div class="content-wrapper">
      <section class="content-header">
          <h1 style="font-size:16px;">Supervisors : </h1>
       </section>
       
	   <section class="content">
          <div class="row">
            <div class="col-xs-12">
			<div class="box">
                <div class="box-body">
				<div class="col-md-12">
					<div class="row">
						
						<div class="form-group col-md-3">
							<label>Title <font color="red"><sup>* (visible to branch)</sup></font></label>
							<input placeholder="e.g. PAPPU BHAI" autocomplete="off" oninput="this.value=this.value.replace(/[^A-Z a-z]/,'')" type="text" class="form-control" id="title" />
						</div>
						
						<div class="form-group col-md-3">
							<label>Username <font color="red"><sup>*</sup></font></label>
							<input autocomplete="off" oninput="this.value=this.value.replace(/[^A-Za-z0-9-@_#.]/,'')" type="text" class="form-control" id="username" />
						</div>
						
						<div class="form-group col-md-3">
							<label>Password <font color="red"><sup>*</sup></font></label>
							<input autocomplete="off" type="text" oninput="this.value=this.value.replace(/[^A-Za-z0-9-_@#.]/,'')" class="form-control" id="password" />
						</div>
						
						<div class="form-group col-md-3">
							<label>Mobile No. <font color="red"><sup>*</sup></font></label>
							<input autocomplete="off" maxlength="10" type="text" oninput="this.value=this.value.replace(/[^0-9]/,'')" class="form-control" id="mobile_no" />
						</div>
						
						<div class="form-group col-md-3">
							<label>Type <font color="red"><sup>*</sup></font></label>
							<select name="supervisor_type" id="supervisor_type" class="form-control" required="required">
								<option value="">--select option--</option>
								<option value="1">Supervisor</option>
								<option value="2">Vehicle Placer</option>
							</select>
						</div>
						
						<div class="form-group col-md-3">
							<?php if(!isMobile()) { echo "<label>&nbsp;</label><br />"; } ?>
							<button type="button" onclick="AddUserFunc()" class="btn btn-sm btn-success <?php if(isMobile()) { echo "btn-block"; } ?>" id="add_btn"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> &nbsp; Add Supervisor</button>
						</div>
					</div>
				</div>
				
				<div class="col-md-12">&nbsp;</div>
				
				<div class="col-md-12 table-responsive" id="load_table">
              
				 </div> 
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

<div id="func_result"></div>  
<div id="load_modal_div"></div>  
 
<script>
function AddUserFunc()
{
	var title = $('#title').val();
	var username = $('#username').val();
	var password = $('#password').val();
	var mobile_no = $('#mobile_no').val();
	var supervisor_type = $('#supervisor_type').val();
	
	if(title=='')
	{
		Swal.fire({icon: 'warning',html: '<font size=\'2\' color=\'black\'>Enter title first !</font>',});
	}
	else if(username=='')
	{
		Swal.fire({icon: 'warning',html: '<font size=\'2\' color=\'black\'>Enter username first !</font>',});
	}
	else if(password=='')
	{
		Swal.fire({icon: 'warning',html: '<font size=\'2\' color=\'black\'>Enter password first !</font>',});
	}
	else if(mobile_no=='')
	{
		Swal.fire({icon: 'warning',html: '<font size=\'2\' color=\'black\'>Enter mobile number first !</font>',});
	}
	else if(supervisor_type=='')
	{
		Swal.fire({icon: 'warning',html: '<font size=\'2\' color=\'black\'>Select user type first !</font>',});
	}
	else
	{
		$('#add_btn').attr('disabled',true);
		$('#loadicon').show();
		jQuery.ajax({
			url: "save_add_supervisor.php",
			data: 'username=' + username + '&password=' + password + '&mobile_no=' + mobile_no + '&supervisor_type=' + supervisor_type + '&title=' + title,
			type: "POST",
			success: function(data) {
			$("#func_result").html(data);
			},
			error: function() {}
		});
	}
}
</script>
 
<script>	
function ShowVehicle(supervisor_id)
{
	var name = $('#s_name_'+supervisor_id).val();
	
	$('#loadicon').show();
		jQuery.ajax({
			url: "modal_get_vehicles_by_supervisor_id.php",
			data: 'id=' + supervisor_id + '&name=' + name,
			type: "POST",
			success: function(data) {
			$("#load_modal_div").html(data);
			},
			error: function() {}
		});
}

function ToggleUserStatus(id)
{
	var value1 = $('#user_status_html_'+id).val();
		
	if(value1=="1"){
		var value2 = "0";
	}
	else{
		var value2 = "1";
	}
		
	$('#loadicon').show();
		jQuery.ajax({
		url: "manage_supervisor_save.php",
		data: 'id=' + id + '&value=' + value2,
		type: "POST",
		success: function(data) {
		$("#func_result").html(data);
		},
	error: function() {}
	});
}
</script>	

<script>	
function LoadTable()
{
	jQuery.ajax({
		url: "_load_supervisor.php",
		data: 'id=' + 'ok',
		type: "POST",
		success: function(data) {
			$("#load_table").html(data);
			$('#example').DataTable({ 
				"destroy": true, //use for reinitialize datatable
			});
		},
		error: function() {}
	});
}

LoadTable();
</script>

<?php include("footer.php") ?>