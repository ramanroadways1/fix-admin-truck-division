<?php
require_once 'connect.php';

$get_approved = escapeString($conn,($_POST['get_approved']));
$hisab_date = escapeString($conn,($_POST['hisab_date']));

// $from_date = date('Y-m-d', strtotime(escapeString($conn,$_POST['duration']), strtotime(date("Y-m-d"))));
// $to_date = date("Y-m-d");

if($get_approved=='' || $hisab_date=='')
{
	AlertRightCornerError("Invalid inputs !");
	exit();
}

if($get_approved=='NO')	
{
	$sql = Qry($conn,"SELECT h.id,h.trip_no,h.opening,h.opening_branch,h.opening_date,h.closing,h.closing_branch,
	h.closing_date,h.gps_km_of_trip,h.avg,h.hisab_check,l.tno,l.driver,l.branch as hisab_branch,l.timestamp as hisab_datetime,
	truck.comp
	FROM dairy.opening_closing as h 
	LEFT OUTER JOIN dairy.log_hisab AS l ON l.trip_no=h.trip_no 
	LEFT OUTER JOIN dairy.own_truck AS truck ON truck.tno=l.tno 
	WHERE h.closing_date='$hisab_date' AND h.hisab_check='0'");
}
else
{
	$sql = Qry($conn,"SELECT h.id,h.trip_no,h.opening,h.opening_branch,h.opening_date,h.closing,h.closing_branch,
	h.closing_date,h.gps_km_of_trip,h.avg,h.hisab_check,l.tno,l.driver,l.branch as hisab_branch,l.timestamp as hisab_datetime,
	truck.comp 
	FROM dairy.opening_closing as h 
	LEFT OUTER JOIN dairy.log_hisab AS l ON l.trip_no=h.trip_no 
	LEFT OUTER JOIN dairy.own_truck AS truck ON truck.tno=l.tno 
	WHERE h.closing_date='$hisab_date'");
}		

if(!$sql){
	echo getMySQLError($conn);
	errorLog(getMySQLError($conn),$conn,$page_url,__LINE__);
	exit();
}
	
if(numRows($sql)==0)
{
	AlertRightCornerError("No record found !");
	exit();
}
	?>
	<table id="example" class="table table-bordered table-striped" style="font-size:13px;">
        <thead>
		<tr>
			  <th>#</th>
			  <th>Trip_No.</th>
			  <th>Truck_No.</th>
			  <th>Company</th>
			  <th>Opening Branch/Date</th>
			  <th>Opening Balance</th>
			  <th>Closing Branch/Date</th>
			  <th>Closing Balance</th>
			  <th>GPS KM</th>
			  <th>AVG.</th>
			  <th>#Approve</th>
		</tr>
		</thead>
    <tbody id=""> 
	
<?php
$sn=1;

while($row = fetchArray($sql))
{	
	$opening_date = date('d-m-y', strtotime($row['opening_date']));
	$closing_date = date('d-m-y', strtotime($row['closing_date']));
	$hisab_timestamp = date('d-m-y h:i A', strtotime($row['hisab_datetime']));
	
	if($row['hisab_check']=="1")
	{
		$approve_btn_text = "Approved";
		$approve_btn_disable = "disabled";
	}
	else
	{
		$approve_btn_text = "Approve";
		$approve_btn_disable = "";
	}
	
		echo "<tr>	
			<td>$sn</td>
			<td><button class='btn btn-xs btn-primary' onclick=ViewHisab('$row[id]')>$row[trip_no]</button></td>
			<td>$row[tno]</td>
			<td>$row[comp]</td>
			<td>$row[opening_branch]<br>$opening_date</td>
			<td>$row[opening]</td>
			<td>$row[closing_branch]<br>$closing_date</td>
			<td>$row[closing]</td>
			<td>$row[gps_km_of_trip]</td>
			<td>$row[avg]</td>
			<td><button $approve_btn_disable class='btn btn-success btn-xs btn_approve' id='approve_btn_$row[id]' onclick='ApproveHisab($row[id])'><i class='fa fa-thumbs-o-up' aria-hidden='true'></i> $approve_btn_text</button></td>
		</tr>";
$sn++;		
}
	echo "</tbody>
</table>";
?>
	
<script> 
	$("#loadicon").fadeOut('slow');
</script> 

<?php
$chk = Qry($conn,"SELECT id FROM _access_control WHERE username='$_SESSION[ediary_fix_admin]' AND func_id=(SELECT id FROM 
_access_control_func_list WHERE session_role='8' AND func_name='HISAB_Check') AND u_update='1'");
			  
if(numRows($chk) == 0)
{
	echo "<script>
		$('.btn_approve').attr('disabled',true);
		$('.btn_approve').hide();
	</script>";
}
else
{
	
}
?>