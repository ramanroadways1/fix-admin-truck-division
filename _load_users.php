<?php
require_once("connect.php");

$get_total_roles = Qry($conn,"SELECT id FROM _access_control_func_list WHERE session_role='8'");
?>
<style>
  .slow .toggle-group { transition: left 0.7s; -webkit-transition: left 0.7s; }
</style>

<link href="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.6.1/css/bootstrap4-toggle.min.css" rel="stylesheet">
<script src="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.6.1/js/bootstrap4-toggle.min.js"></script>

    <table id="example" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th>#</th>
                        <th>Username</th>
                        <th>Mobile</th>
                        <th>Rights out of <?php echo ": <font color='red'>(".numRows($get_total_roles).")</font>"; ?></th>
                        <th>Added On</th>
                        <th>#Status#</th>
                      </tr>
                    </thead>
                    <tbody>
	<?php
	$get_roles = Qry($conn,"SELECT u.id,u.username,u.branch,u.active_login,u.mobile_no,u.timestamp,COUNT(a.id) as total_count,SUM(a.u_view) as view_count,
	SUM(a.u_insert) as insert_count,SUM(a.u_update) as update_count,SUM(a.u_delete) AS delete_count 
	FROM user AS u 
	LEFT OUTER JOIN _access_control AS a ON a.username = u.username AND a.session_role = u.role 
	WHERE u.role='8' GROUP BY u.username ORDER BY u.id ASC");
	
	if(numRows($get_roles)==0)
	{
		echo "<tr>.
			<td colspan='7'>No record found !</td>
			<td style='display: none'></td>
			<td style='display: none'></td>
			<td style='display: none'></td>
			<td style='display: none'></td>
			<td style='display: none'></td>
			<td style='display: none'></td>
			<td style='display: none'></td>
		</tr>";
	}
	else
	{
		$i=1;
		while($row = fetchArray($get_roles))
		{
			$timestamp = date("d-m-y h:i A",strtotime($row['timestamp']));
			
			if($row['active_login']=="1"){
				$enable_login_btn = "disabled";
				$disabled_login_btn = "";
			}else{
				$enable_login_btn = "";
				$disabled_login_btn = "disabled";
			}
			
			if($row['branch']=='MAIN_ADMIN')
			{
				$enable_login_btn = "disabled";
				$disabled_login_btn = "disabled";
				$username="$row[username]<br><font color='red'>(Superadmin)</font>";
			}
			else
			{
				$username="$row[username]";
			}
			
			echo "<tr>
				<td>$i</td>
				<td>$username</td>
				<td>$row[mobile_no]</td>
				<td>
					View : $row[view_count] Insert : $row[insert_count] Update : $row[update_count] Delete : $row[delete_count]
				</td>
				<td>$timestamp</td>";
				if($row['branch']=='MAIN_ADMIN')
				{
					echo "<td></td>";
				}
				else
				{
				?>
				<td>
				<input type="hidden"  value="<?php echo $row['active_login']; ?>" id="user_status_html_<?php echo $row['id']; ?>" />
				<input type="checkbox" onchange="ToggleUserStatus('<?php echo $row['id']; ?>')" id="button_check_<?php echo $row['id']; ?>" checked data-toggle="toggle" 
				<?php if($row['active_login']=='1') { echo "data-on='Active' data-off='Disabled' data-onstyle='success' data-offstyle='danger'"; } else { echo "data-on='Disabled' data-off='Active' data-onstyle='danger' data-offstyle='success'"; }?> data-style="slow" />
				</td>
				<?php				
					// echo "
					// <td><button type='button' class='btn btn-xs btn-success' $enable_login_btn id='enable_btn_$row[id]' onclick='EnableUser($row[id])'>Enable</button></td>
					// <td><button type='button' class='btn btn-xs btn-danger' $disabled_login_btn id='disable_btn_$row[id]' onclick='DisableUser($row[id])'>Disable</button></td>";
				}
		
			echo "</tr>";
		$i++;	
		}
	}
	?>	
                    </tbody>
                  </table>
				  
<script> 
$("#loadicon").fadeOut('slow');
$(document).ready(function() {
    $('#example').DataTable();
} );
</script> 			  