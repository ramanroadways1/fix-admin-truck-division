<?php
require_once 'connect.php';

$pump_type = escapeString($conn,($_POST['pump_type']));

if($pump_type=='MARKET')
{
	$sql = Qry($conn,"SELECT branch,name,code,comp,active FROM diesel_pump WHERE code!='' ORDER BY name ASC");
}
else
{
	$sql = Qry($conn,"SELECT branch,name,code,comp,active FROM dairy.diesel_pump_own WHERE code!='' ORDER BY name ASC");
}

if(!$sql){
	echo getMySQLError($conn);
	errorLog(getMySQLError($conn),$conn,$page_url,__LINE__);
	exit();
}
	
if(numRows($sql)==0)
{
	AlertRightCornerError("No record found !");
	exit();
}
?>
<table id="example" class="table table-bordered table-striped" style="font-size:13px;">
        <thead>
		<tr>
			  <th>#</th>
			  <th>Pump_Name</th>
			  <th>Pump_Code</th>
			  <th>Fuel_Company</th>
			  <th>Branch</th>
			  <th>Status</th>
		</tr>
		</thead>
    <tbody id=""> 
	
<?php
$sn=1;

while($row = fetchArray($sql))
{	
	if($row['active']=="1")
	{
		$status = "<font color='green'>Active</font>";
	}
	else
	{
		$status = "<font color='red'>Inactive</font>";
	}
	
		echo "<tr>	
			<td>$sn</td>
			<td>$row[name]</td>
			<td>$row[code]</td>
			<td>$row[comp]</td>
			<td>$row[branch]</td>
			<td>$status</td>
		</tr>";
$sn++;		
}
	echo "</tbody>
</table>";
?>
	
<script> 
	$("#loadicon").fadeOut('slow');
</script> 