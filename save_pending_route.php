<?php
require_once("connect.php");

$timestamp = date("Y-m-d H:i:s");
$id = escapeString($conn,$_POST['id']);
$route_type = escapeString($conn,strtolower($_POST['route_type']));

echo "<script>
	$('#btn_save_$id').attr('disabled',true);
	$('#route_type_$id').attr('disabled',true);
</script>";

if($route_type!='normal' AND $route_type!='congested')
{
	AlertError("Invalid route type !");
	errorLog("Invalid route type: $route_type !",$conn,$page_url,__LINE__);
	exit();
}

$chk_record = Qry($conn,"SELECT tno,from_id,to_id,trip_id,is_approved FROM dairy.pending_master_lane WHERE id='$id'");

if(!$chk_record){
	AlertError("Error while processing request !");
	errorLog(getMySQLError($conn),$conn,$page_url,__LINE__);
	exit();
}

if(numRows($chk_record)==0){
	AlertError("Route not found !");
	exit();
}

$row = fetchArray($chk_record);

if($row['is_approved']!="0")
{
	AlertError("This route approved already !");
	exit();
}

$from_id = $row['from_id'];
$to_id = $row['to_id'];
$trip_id = $row['trip_id'];
$veh_no = $row['tno'];

StartCommit($conn);
$flag = true;
	
$insert_route = Qry($conn,"INSERT INTO dairy.master_lane(from_id,to_id,route_type,branch,branch_user,timestamp) VALUES ('$from_id',
'$to_id','$route_type','Fix-Admin','$_SESSION[ediary_fix_admin]','$timestamp')");
	
if(!$insert_route){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

$mark_approved = Qry($conn,"UPDATE dairy.pending_master_lane SET is_approved='1',approval_timestamp='$timestamp',
approval_user='$_SESSION[ediary_fix_admin]' WHERE id='$id'");
	
if(!$mark_approved){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

if($trip_id!='0' AND $route_type!='normal')
{
	$chk_trip = Qry($conn,"SELECT lr_type,km,dsl_consumption FROM dairy.trip WHERE id='$trip_id'");
		
	if(!$chk_trip){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
	
	if(numRows($chk_trip)==0){
		$flag = false;
		errorLog("Trip not found. Trip_id: $trip_id.",$conn,$page_name,__LINE__);
	}
	
	$row_trip = fetchArray($chk_trip);
	
	$get_avg_data = Qry($conn,"SELECT max_capacity,empty_avg,normal_avg,overload_avg FROM dairy._avg_data WHERE veh_no='$veh_no' AND 
	route_type='$route_type'");
		
	if(!$get_avg_data){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
	
	if(numRows($get_avg_data)==0){
		$flag = false;
		errorLog("Avg. data not found. Vehno: $veh_no.",$conn,$page_name,__LINE__);
	}
	
	$row_avg_data = fetchArray($get_avg_data);

	if($row_trip['lr_type']=='EMPTY')
	{
		$avg_value = $row_avg_data['empty_avg'];
	}
	else
	{
		if($actual_wt>$row_avg_data['max_capacity']){
			$avg_value = $row_avg_data['overload_avg'];
		}
		else{
			$avg_value = $row_avg_data['normal_avg'];
		}
	}
	
	$dsl_consumption_new = sprintf("%.2f",($row_trip['km']/$avg_value));
	
	$dsl_diff = $dsl_consumption_new - $row_trip['dsl_consumption'];
	
	$minus_dsl_left = Qry($conn,"UPDATE dairy.own_truck SET diesel_left=diesel_left+('$dsl_diff') WHERE tno='$veh_no'");

	if(!$minus_dsl_left){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
	
	$update_trip_data = Qry($conn,"UPDATE dairy.trip SET dsl_consumption='$dsl_consumption_new' WHERE id='$trip_id'");

	if(!$update_trip_data){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
}

if($flag)
{
	MySQLCommit($conn);
	closeConnection($conn);
	echo "<script>
		$('#approval_row_$id').html('<font color=\'green\'><b>-- OK Approved --</b></font>');
		$('#loadicon').fadeOut('slow');
	</script>";
	// AlertRightCornerSuccessFadeFast("Route approved !");
}
else
{
	MySQLRollBack($conn);
	closeConnection($conn);
	AlertError("Error While Processing Request !");
	exit();
}
?>