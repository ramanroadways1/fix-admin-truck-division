<?php
require_once("./connect.php");
 
$timestamp = date("Y-m-d H:i:s");
$date = date("Y-m-d");

$exp_array = escapeString($conn,($_POST['exp']));

$exp_name = explode("#",$exp_array)[1];
$exp_code = explode("#",$exp_array)[0];

$limit_type = escapeString($conn,($_POST['limit_type']));
$limit_value = escapeString($conn,($_POST['limit_value']));
$limit_for = escapeString($conn,($_POST['limit_for']));

if(empty($exp_name))
{
	AlertErrorTopRight("Select expense first !");
	exit();
}

if(empty($exp_code))
{
	AlertErrorTopRight("Select expense first !");
	exit();
}

if(empty($limit_type))
{
	AlertErrorTopRight("Select expense limit first !");
	exit();
}

if(empty($limit_value))
{
	AlertErrorTopRight("Enter limit value first !");
	exit();
}

if(empty($limit_for))
{
	AlertErrorTopRight("Select limit for option first !");
	exit();
}

$chk_data = Qry($conn,"SELECT id FROM dairy.trip_exp_limit WHERE exp='$exp_code' AND type='$limit_type' AND limit_for='$limit_for'");

if(!$chk_data){
	AlertErrorTopRight("Error while processing request !");
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	exit();
}

if(numRows($chk_data)>0)
{
	AlertErrorTopRight("Duplicate record found !");
	exit();
}

StartCommit($conn);
$flag = true;	

if($limit_type=="1")
{
	$insert = Qry($conn,"INSERT INTO dairy.trip_exp_limit(limit_for,exp_name,exp,type,amount,timestamp) VALUES ('$limit_for',$exp_name','$exp_code','$limit_type',
	'$limit_value','$timestamp')");

	if(!$insert){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
}
else 
{
	$insert = Qry($conn,"INSERT INTO dairy.trip_exp_limit(limit_for,exp_name,exp,type,entry_limit,timestamp) VALUES ('$limit_for','$exp_name','$exp_code','$limit_type',
	'$limit_value','$timestamp')");

	if(!$insert){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
}

if($flag)
{
	MySQLCommit($conn);
	closeConnection($conn);
	
	AlertRightCornerSuccess("Record Added Successfully !");
	echo "<script>
			LoadTable();
		</script>";
	exit();
}
else
{
	MySQLRollBack($conn);
	closeConnection($conn);
	AlertErrorTopRight("Error while processing request !");
	exit();
}	
?>