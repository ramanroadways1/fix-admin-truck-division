<?php
include("header.php");

$chk = Qry($conn,"SELECT id FROM _access_control WHERE username='$_SESSION[ediary_fix_admin]' AND func_id=(SELECT id FROM 
_access_control_func_list WHERE session_role='8' AND func_name='Report_Driver_Book') AND u_view='1'");
			  
if(numRows($chk)==0)
{
	echo "<script>window.location.href='./';</script>";
	exit();
}
?>
<script>
$(function() {
		$("#tno").autocomplete({
		source: 'autofill/get_tno.php',
		// appendTo: "#LaneRuleModal",
		select: function (event, ui) { 
              $('#tno').val(ui.item.value);   
             // $("#button1").attr('disabled',false);				   
             return false;
		},
		change: function (event, ui) {
		if(!ui.item){
		    $(event.target).val("");
			alert('Vehicle Number does not exists.');
			$("#tno").val('');
			$("#tno").focus();
			// $("#button1").attr('disabled',true);
		}}, 
		focus: function (event, ui){
			return false;}
});});

$(function() {
		$("#driver_name").autocomplete({
		source: 'autofill/get_own_truck_driver.php',
		// appendTo: "#LaneRuleModal",
		select: function (event, ui) { 
              $('#driver_name').val(ui.item.value);   
              $('#driver_code').val(ui.item.code);   
             // $("#button1").attr('disabled',false);				   
             return false;
		},
		change: function (event, ui) {
		if(!ui.item){
		    $(event.target).val("");
			alert('Vehicle Number does not exists.');
			$("#driver_name").val('');
			$("#driver_code").val('');
			$("#driver_name").focus();
			// $("#button1").attr('disabled',true);
		}}, 
		focus: function (event, ui){
			return false;}
});});

$(function() {
		$("#sold_tno").autocomplete({
		source: 'autofill/get_sold_tno.php',
		// appendTo: "#LaneRuleModal",
		select: function (event, ui) { 
              $('#sold_tno').val(ui.item.value);   
             // $("#button1").attr('disabled',false);				   
             return false;
		},
		change: function (event, ui) {
		if(!ui.item){
		    $(event.target).val("");
			alert('Vehicle Number does not exists.');
			$("#sold_tno").val('');
			$("#sold_tno").focus();
			// $("#button1").attr('disabled',true);
		}}, 
		focus: function (event, ui){
			return false;}
});});
</script>

<div class="content-wrapper">
      <section class="content-header">
          <h1 style="font-size:16px;">Report : Driver Book </h1>
       </section>
       
	   <section class="content">
          <div class="row">
            <div class="col-xs-12">
			<div class="box">
                <div class="box-body">
				
				<div class="col-md-12">&nbsp;</div>
				
				<div class="col-md-12">
					<div class="row">
						<div class="form-group col-md-2">
							<label style="font-size:12px !important">Driver Name</label>
							<input style="font-size:12px !important" autocomplete="off" oninput="this.value=this.value.replace(/[^A-Z .a-z0-9]/,'');CheckInput('driver_name');" type="text" class="form-control" id="driver_name" />
						</div>
						
						<input type="hidden" name="driver_code" id="driver_code">
						
						<div class="form-group col-md-1">
							<label>&nbsp;</label>
							<?php if(!isMobile()) { echo "<br />"; } ?>
							<span style="font-size:12px !important;color:maroon">-- OR --</span>
						</div>
						
						<div class="form-group col-md-2">
							<label style="font-size:12px !important">Vehicle No.</label>
							<input style="font-size:12px !important" autocomplete="off" type="text" oninput="this.value=this.value.replace(/[^A-Za-z0-9]/,'');CheckInput('tno');" class="form-control" id="tno" />
						</div>
						
						<div class="form-group col-md-1">
							<label>&nbsp;</label>
							<?php if(!isMobile()) { echo "<br />"; } ?>
							<span style="font-size:12px;color:maroon">-- OR --</span>
						</div>
						
						<div class="form-group col-md-2">
							<label style="font-size:12px !important">Sold Vehicle No.</label>
							<input style="font-size:12px !important" autocomplete="off" type="text" oninput="this.value=this.value.replace(/[^A-Za-z0-9]/,'');CheckInput('sold_tno');" class="form-control" id="sold_tno" />
						</div>
						
						<div class="form-group col-md-2">
							<label style="font-size:12px !important">Date range.</label>
							<select style="font-size:12px !important" name="duration" id="duration" class="form-control" required>
								<option style="font-size:12px !important" value="">---SELECT---</option>
								<option style="font-size:12px !important" value="-0 days">Today's</option>
								<option style="font-size:12px !important" value="-1 days">Last 2 days</option>
								<option style="font-size:12px !important" value="-4 days">Last 5 days</option>
								<option style="font-size:12px !important" value="-6 days">Last 7 days</option>
								<option style="font-size:12px !important" value="-9 days">Last 10 days</option>
								<option style="font-size:12px !important" value="-14 days">Last 15 days</option>
								<option style="font-size:12px !important" value="-29 days">Last 30 days</option>
								<option style="font-size:12px !important" value="-59 days">Last 60 days</option>
								<option style="font-size:12px !important" value="-89 days">Last 90 days</option>
								<option style="font-size:12px !important" value="-119 days">Last 120 days</option>
								<option style="font-size:12px !important" value="FULL">FULL REPORT</option>
							</select>
						</div>
						
						<div class="form-group col-md-2">
							<?php if(!isMobile()) { echo "<label>&nbsp;</label><br />"; } ?>
							<button type="button" style="margin-top:1px;font-size:12px !important" onclick="Search()" class="btn btn-sm btn-success <?php if(isMobile()) { echo "btn-block"; } ?>" id="add_btn"><i class="fa fa-search" aria-hidden="true"></i> &nbsp; Search</button>
						</div>
					</div>
				</div>
				
<script>				
function CheckInput(elem)
{
	if(elem=='driver_name')
	{
		$('#tno').val('');
		$('#sold_tno').val('');
	}
	else if(elem=='tno')
	{
		$('#driver_name').val('');
		$('#driver_code').val('');
		$('#sold_tno').val('');
	}
	else if(elem=='sold_tno')
	{
		$('#driver_name').val('');
		$('#driver_code').val('');
		$('#tno').val('');
	}
}
</script>				
				
				<div class="col-md-12">&nbsp;</div>
				
				<div class="col-md-12 table-responsive" id="load_table">
              
				 </div> 
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

<div id="func_result"></div>  

<script>	
function Search()
{
	var tno = $('#tno').val();
	var sold_tno = $('#sold_tno').val();
	var driver_code = $('#driver_code').val();
	var duration = $('#duration').val();
	
	if(tno=='' && sold_tno=='' && driver_code=='')
	{
		Swal.fire({icon: 'warning',html: '<font size=\'2\' color=\'black\'>Atleast one field is required !</font>',});
	}
	else
	{
		if(duration=='')
		{
			Swal.fire({icon: 'warning',html: '<font size=\'2\' color=\'black\'>Select date range first !</font>',});
		}
		else
		{
			$('#loadicon').show();
				jQuery.ajax({
					url: "_load_report_driver_book.php",
					data: 'tno=' + tno + '&sold_tno=' + sold_tno + '&duration=' + duration + '&driver_code=' + driver_code,
					type: "POST",
					success: function(data) {
						$("#load_table").html(data);
						$('#example').DataTable({ 
						"lengthMenu": [ [50, 100, 500, -1], [50, 100,500, "All"] ], 
							"destroy": true, //use for reinitialize datatable
						});
					},
					error: function() {}
			});
		}
	}
}

function ViewHisab(trip_no,secret_key)
{
	$('#trip_no_1').val(trip_no);
	$('#secret_key').val(secret_key);
	
	$('#HisabViewForm')[0].submit();
}
</script>

<?php include("footer.php") ?>

<form action="https://rrpl.online/diary/hisab/Report_Driver_Book.php" id="HisabViewForm" method="POST" target="_blank">
	<input type="hidden" id="trip_no_1" name="trip_no">
	<input type="hidden" id="secret_key" name="secret_key">
</form>