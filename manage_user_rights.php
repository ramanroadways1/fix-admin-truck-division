<?php include("header.php"); ?>

<?php
if($_SESSION['ediary_fix_admin']!='TRUCKS')
{
	echo "<script>window.location.href='./';</script>";
	exit();
}
?>

<div class="content-wrapper">
      <section class="content-header">
          <h1 style="font-size:16px;">Manage Users Rights : </h1>
       </section>
       
	   <section class="content">
          <div class="row">
            <div class="col-xs-12">
			<div class="box">
                <div class="box-body">
				<div class="col-md-12 table-responsive">
                  <table id="example" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th>#</th>
                        <th>Username</th>
                        <th>Function Description</th>
                        <th>View</th>
                        <th>Insert</th>
                        <th>Update</th>
                        <th>Delete</th>
                      </tr>
                    </thead>
                    <tbody>
	<?php
	$get_roles = Qry($conn,"SELECT a.id,a.username,a.u_view,a.u_insert,a.u_update,a.u_delete,l.func_desc 
	FROM _access_control AS a 
	LEFT OUTER JOIN _access_control_func_list AS l ON l.id = a.func_id 
	WHERE a.session_role='8' AND a.username!='TRUCKS' ORDER BY a.id ASC,a.username ASC");
	
	if(numRows($get_roles)==0)
	{
		echo "<tr><td colspan='7'>No record found !</td></tr>";
	}
	else
	{
		$i=1;
		while($row = fetchArray($get_roles))
		{
			if($row['u_view']){
				$view_check="checked";
			}else{
				$view_check="";
			}
			
			if($row['u_insert']){
				$insert_check="checked";
			}else{
				$insert_check="";
			}
			
			if($row['u_update']){
				$update_check="checked";
			}else{
				$update_check="";
			}
			
			if($row['u_delete']){
				$delete_check="checked";
			}else{
				$delete_check="";
			}
			
			if($row['username']=='TRUCKS')
			{
				$disabled_view_c="disabled";
				$disabled_insert_c="disabled";
				$disabled_update_c="disabled";
				$disabled_delete_c="disabled";
			}
			else
			{
				$disabled_view_c="";
				$disabled_insert_c="";
				$disabled_update_c="";
				$disabled_delete_c="";
			}
			
			if($row['username']=='TRUCKS')
			{
				$username="$row[username]<br><font color='red'>(Superadmin)</font>";
			}
			else
			{
				$username="$row[username]";
			}
			
			echo "<tr>
				<td>$i</td>
				<td>$username</td>
				<td>$row[func_desc]</td>
				<td><input type='checkbox' $disabled_view_c $view_check id='view_checkbox_$row[id]' value='$row[u_view]' onchange='FuncView($row[id])' /></td>
				<td><input type='checkbox' $disabled_insert_c $insert_check id='insert_checkbox_$row[id]' value='$row[u_insert]' onchange='FuncInsert($row[id])' /></td>
				<td><input type='checkbox' $disabled_update_c $update_check id='update_checkbox_$row[id]' value='$row[u_update]' onchange='FuncUpdate($row[id])' /></td>
				<td><input type='checkbox' $disabled_delete_c $delete_check id='delete_checkbox_$row[id]' value='$row[u_delete]' onchange='FuncDelete($row[id])' /></td>
			</tr>";
		$i++;	
		}
	}
	?>	
                    </tbody>
                  </table>
				 </div> 
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

<script> 
$("#loadicon").fadeOut('slow');
$(document).ready(function() {
    $('#example').DataTable();
} );
</script> 

<?php include("footer.php") ?>

<div id="func_result"></div>  
  
<script>	
function FuncView(id)
{
	var value1 = $("#view_checkbox_"+id).val();
	$('#loadicon').show();
	jQuery.ajax({
		url: "manage_users_rights_save.php",
		data: 'id=' + id + '&value1=' + value1 + '&type=' + 'view',
		type: "POST",
		success: function(data) {
		$("#func_result").html(data);
		},
		error: function() {}
	});
}

function FuncDelete(id)
{
	var value1 = $("#delete_checkbox_"+id).val();
	$('#loadicon').show();
	jQuery.ajax({
		url: "manage_users_rights_save.php",
		data: 'id=' + id + '&value1=' + value1 + '&type=' + 'delete',
		type: "POST",
		success: function(data) {
		$("#func_result").html(data);
		},
		error: function() {}
	});
}

function FuncInsert(id)
{
	var value1 = $("#insert_checkbox_"+id).val();
	$('#loadicon').show();
	jQuery.ajax({
		url: "manage_users_rights_save.php",
		data: 'id=' + id + '&value1=' + value1 + '&type=' + 'insert',
		type: "POST",
		success: function(data) {
		$("#func_result").html(data);
		},
		error: function() {}
	});
}

function FuncUpdate(id)
{
	var value1 = $("#update_checkbox_"+id).val();
	$('#loadicon').show();
	jQuery.ajax({
		url: "manage_users_rights_save.php",
		data: 'id=' + id + '&value1=' + value1 + '&type=' + 'update',
		type: "POST",
		success: function(data) {
		$("#func_result").html(data);
		},
		error: function() {}
	});
}
</script>	