<?php
require_once 'connect.php';

$bilty_no = escapeString($conn,($_POST['bilty_no']));
	
$sql = Qry($conn,"SELECT id,trans_id,branch,date,trans_type,advbal,trans_value,credit,debit,balance,narration,timestamp FROM 
dairy.bilty_book WHERE bilty_no='$bilty_no'");

if(!$sql){
	echo getMySQLError($conn);
	errorLog(getMySQLError($conn),$conn,$page_url,__LINE__);
	exit();
}
	
if(numRows($sql)==0)
{
	AlertRightCornerError("No record found !");
	exit();
}
	?>
	<table id="example" class="table table-bordered table-striped" style="font-size:13px;">
        <thead>
		<tr>
			<th>#Id</th>
			<th>Txn_id</th>
			<th>Branch</th>
			<th>Txn_date</th>
			<th>Txn_type</th>
			<th>Adv/Bal</th>
			<th>Txn_value</th>
			<th>Cr</th>
			<th>Dr</th>
			<th>Balance</th>
			<th>Narration</th>
			<th>Timestamp</th>
			<th>#Edit</th>
			<th>#Delete</th>
		</tr>
		</thead>
    <tbody id=""> 
	
<?php
$sn=1;

while($row = fetchArray($sql))
{	
		$txn_date = date('d-m-y', strtotime($row['date']));
		$timestamp = date('d-m-y h:i A', strtotime($row['timestamp']));
		
		echo "<tr>	
			<td>$sn</td>
			<td>$row[trans_id]</td>
			<td>$row[branch]</td>
			<td>$txn_date</td>
			<td>$row[trans_type]</td>
			<td>$row[advbal]</td>
			<td>$row[trans_value]</td>
			<td>$row[credit]</td>
			<td>$row[debit]</td>
			<td>$row[balance]</td>
			<td>$row[narration]</td>
			<td>$timestamp</td>";
		
		if($row['credit']==$row['balance'])
		{
			echo "
			<td></td>
			<td></td>";
		}	
		else
		{
			if($row['branch']=='CGROAD' || $row['trans_id']=='')
			{
				echo "<td><button class='btn edit_btn_1 btn-xs btn-primary' type='button' onclick=EditVoucher('$row[id]')><i class='fa fa-pencil-square-o' aria-hidden='true'></i></button></td>";
				echo "<td><button class='btn delete_btn_1 btn-xs btn-danger' id='dlt_btn_$row[id]' type='button' onclick=DeleteVoucher('$row[id]')><i class='fa fa-trash' aria-hidden='true'></i></button></td>";
			}	
			else
			{
				echo "<td></td>";
				echo "<td></td>";
			}
		}
		
		echo "</tr>";
	$sn++;		
}
	echo "</tbody>
</table>";

$chk_update = Qry($conn,"SELECT id FROM _access_control WHERE username='$_SESSION[ediary_fix_admin]' AND func_id=(SELECT id FROM 
_access_control_func_list WHERE session_role='8' AND func_name='Market_Bilty_dr_cr') AND u_update='1'");
			  
if(numRows($chk_update)==0)
{
	echo "<script>
		$('.edit_btn_1').attr('disabled',true);
		$('.edit_btn_1').attr('onclick','');
	</script>";
}

$chk_delete = Qry($conn,"SELECT id FROM _access_control WHERE username='$_SESSION[ediary_fix_admin]' AND func_id=(SELECT id FROM 
_access_control_func_list WHERE session_role='8' AND func_name='Market_Bilty_dr_cr') AND u_delete='1'");
			  
if(numRows($chk_delete)==0)
{
	echo "<script>
		$('.delete_btn_1').attr('disabled',true);
		$('.delete_btn_1').attr('onclick','');
	</script>";
}
?>
	
<script> 
	$("#loadicon").fadeOut('slow');
</script> 
