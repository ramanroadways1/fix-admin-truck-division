<?php
include "header.php";

$chk = Qry($conn,"SELECT id FROM _access_control WHERE username='$_SESSION[ediary_fix_admin]' AND func_id=(SELECT id FROM 
_access_control_func_list WHERE session_role='8' AND func_name='Gps_Status') AND u_view='1'");
			  
if(numRows($chk)==0)
{
	echo "<script>window.location.href='./';</script>";
	exit();
}
?>
<style>
label{
	font-size:12px;
}
</style>

<div class="content-wrapper">
    <section class="content-header">
      <span style="font-size:15px">GPS Status : </span>
	</section>
		
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box"> 
			<div class="box-body">

<div class="row">

<div class="form-group col-md-12"><font color="red">Refresh rate : 5 minutes.</font></div>

<div class="form-group col-md-12">
   <div class="form-group col-md-12 table-responsive" style="overflow:auto" id="load_table_div"></div>
</div>

  </div>
</div>

</body>
</html>

          </div>
        </div>
	</div>
</section>
</div>    
	
<script type="text/javascript">
function AddAvg()
{
	var truck_no = $('#truck_no').val();
	var route_type = $('#route_type').val();
	var empty_avg = $('#empty_avg').val();
	var loaded_avg = $('#loaded_avg').val();
	var overload_avg = $('#overload_avg').val();
	
	if(truck_no=='')
	{
		Swal.fire({icon: 'warning',html: '<font size=\'2\' color=\'black\'>Enter vehicle number first !</font>',});
	}
	else if(route_type=='')
	{
		Swal.fire({icon: 'warning',html: '<font size=\'2\' color=\'black\'>Select route type first !</font>',});
	}
	else if(empty_avg=='')
	{
		Swal.fire({icon: 'warning',html: '<font size=\'2\' color=\'black\'>Enter empty average first !</font>',});
	}
	else if(loaded_avg=='')
	{
		Swal.fire({icon: 'warning',html: '<font size=\'2\' color=\'black\'>Enter loaded average first !</font>',});
	}
	else if(overload_avg=='')
	{
		Swal.fire({icon: 'warning',html: '<font size=\'2\' color=\'black\'>Enter overloaded average first !</font>',});
	}
	else
	{
		$('#btn_save').attr('disabled',true);
		$("#loadicon").show();
			jQuery.ajax({
			url: "./save_avg_data.php",
			data: 'truck_no=' + truck_no + '&route_type=' + route_type + '&empty_avg=' + empty_avg + '&loaded_avg=' + loaded_avg + '&overload_avg=' + overload_avg,
			type: "POST",
			success: function(data) {
				$("#func_result2").html(data);
			},
			error: function() {}
		});
	}
}

function LoadTable()
{
	// $("#loadicon").show();
	jQuery.ajax({
	url: "_load_gps_status.php",
	data: 'ok=' + 'ok',
	type: "POST",
	success: function(data) {
		$("#load_table_div").html(data);
		$('#example').DataTable({ 
        "destroy": true, //use for reinitialize datatable
    });
	},
	error: function() {}
	});
}
</script>

<script>
LoadTable();
</script>

<div id="func_result2"></div>

<?php
include "footer.php";
?>