<?php
require_once("connect.php");

$rule_id = escapeString($conn,($_POST['id']));

$get_consignee = Qry($conn,"SELECT consignee,multi_del_consignee FROM dairy.fix_lane_rules WHERE id='$rule_id'");

if(!$get_consignee){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	AlertError("Error while processing request !");
	exit();
}

$row = fetchArray($get_consignee);

?>
<div class="form-group col-md-3">
	<label>Consignee <sup><font color="red">*</font></sup></label> 
	<select style="font-size:12px !important" name="consignee_name" class="form-control">
<?php	
if($row['multi_del_consignee']=='' AND $row['consignee']==0)
{
	echo "<option style='font-size:12px !important' value='0'><font color='blue'>NA</font></option>";
}
else
{
	if($row['multi_del_consignee']!='')
	{
		$select_consignee_1 = Qry($conn,"SELECT id,name FROM consignee 
		WHERE FIND_IN_SET(id,(SELECT multi_del_consignee from dairy.fix_lane_rules WHERE id='$rule_id'))");
				
		while($row_11 = fetchArray($select_consignee_1))
		{
			echo "<option style='font-size:12px !important' value='$row_11[id]'>$row_11[name]</option>";
		}
	}
	else
	{
		$select_consignee_1 = Qry($conn,"SELECT id,name FROM consignee WHERE id='$row[consignee]'");
				
		while($row_11 = fetchArray($select_consignee_1))
		{
			echo "<option style='font-size:12px !important' value='$row_11[id]'>$row_11[name]</option>";
		}
	}
}
?>			
	</select>
</div>