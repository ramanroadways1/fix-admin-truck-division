<?php
include "header.php";

$today_date = date("Y-m-d");

$chk = Qry($conn,"SELECT id FROM _access_control WHERE username='$_SESSION[ediary_fix_admin]' AND func_id=(SELECT id FROM 
_access_control_func_list WHERE session_role='8' AND func_name='Fix_Queue') AND u_view='1'");
			  
if(numRows($chk) == 0)
{
	echo "<script>window.location.href='./';</script>";
	exit();
}
?>
<div class="content-wrapper">
   <section class="content-header">
		<span style="font-size:15px">Fix Lane Summary :</span>
   </section>
	
<div id="function_res"></div>
		
   <section class="content">
       <div class="row">
            <div class="col-md-12">
            <div class="box"> 
			<div class="box-body">

<div class="row">
 
 <div class="form-group col-md-12">
    <br />

	<div class="form-group col-md-12">
	<div class="row">
	
		<div class="form-group col-md-3">
			<label>Trip Date <font color="red">*</font></label>
			<input id="trip_date" name="date" type="date" class="form-control" max="<?php echo $today_date; ?>" required pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}" />
		</div>
		
		<div class="form-group col-md-2">
			<label>&nbsp;</label>
			<br />
			<button id="button1" type="button" onclick="AddRecord2()" class="btn btn-sm btn-success">
			<span class="fa fa-search"></span> &nbsp;Get Records</button>
		</div>
		
		<div class="form-group col-md-2">
			<label>&nbsp;</label>
			<br />
			OR
		</div>
		
		<form action="./excel_download_fix_trip_summary.php" target="_blank" method="POST">
		<div class="form-group col-md-3">
			<label>Trip Date <font color="red">*</font></label>
			<input id="trip_date" name="date" type="date" class="form-control" max="<?php echo $today_date; ?>" required pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}" />
		</div>
		
		<div class="form-group col-md-2">
			<label>&nbsp;</label>
			<br />
			<button id="button1" type="submit" class="btn btn-sm btn-danger">
			<span class="fa fa-file-excel-o"></span> &nbsp;Excel Download</button>
		</div>
		</form>
		
	</div>
		
    </div>

	<div class="form-group col-md-12 table-responsive" id="func_result">
		 
        	
    </div>
    </div>
  </div>
</div>

</body>
</html>

          </div>
          </div>
		  </div>
       </div>         
    </section>
	
<script>
function AddRecord(date)
{
	$("#loadicon").show();
		jQuery.ajax({
			url: "_fetch_fix_lane_summary.php",
			data: 'date=' + date,
			type: "POST",
			success: function(data) {
			$("#func_result").html(data);
			   $('#example').DataTable({ 
                 "destroy": true,
              });
			},
			error: function() {}
	});
}

function ExcelDownload()
{
	var date = $('#trip_date').val();
	
	if(date!='')
	{
		window.open("./excel_download_fix_trip_summary.php?date="+date+"", "_blank", "toolbar=yes,scrollbars=yes,resizable=yes,top=500,left=500,width=400,height=400");
	}
	else
	{
		$('#trip_date').focus();
	}
}

function AddRecord2()
{
	var date = $('#trip_date').val();
	
	$("#loadicon").show();
		jQuery.ajax({
			url: "_fetch_fix_lane_summary.php",
			data: 'date=' + date,
			type: "POST",
			success: function(data) {
			$("#func_result").html(data);
			   $('#example').DataTable({ 
                 "destroy": true,
              });
			},
			error: function() {}
	});
}

AddRecord('<?php echo $today_date; ?>');
</script>	

<?php
include "footer.php";
?>
