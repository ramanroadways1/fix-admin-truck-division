<?php
require_once("./connect.php");

$timestamp = date("Y-m-d H:i:s"); 

$trip_id = escapeString($conn,($_POST['trip_id']));
$trip_type = escapeString($conn,($_POST['trip_type']));
$is_consumer_pump_entry = escapeString($conn,($_POST['is_consumer_pump_entry']));
$branch = escapeString($conn,($_POST['branch']));

if(empty($trip_id) || $trip_id=='' || $trip_id==0)
{
	AlertRightCornerError("Trip ID not found !");
	echo "<script>$('#entry_save_btn').attr('disabled',false);</script>";
	exit();
}

if($trip_type!='RUNNING' AND $trip_type!='OLD')
{
	AlertRightCornerError("Trip type not found !");
	echo "<script>$('#entry_save_btn').attr('disabled',false);</script>";
	exit();
}

if($trip_type=='RUNNING')
{
	$check_trip = Qry($conn,"SELECT t.id,t.tno,t.driver_code,t.trip_no,t.lr_type,t.loaded_hisab,d.mobile,d.mobile2,d.name as driver_name 
	FROM dairy.trip AS t 
	LEFT OUTER JOIN dairy.driver AS d ON d.code=t.driver_code 
	WHERE t.id='$trip_id'");
}
else
{
	$check_trip = Qry($conn,"SELECT t.id,t.tno,t.driver_code,t.trip_no,t.lr_type,t.loaded_hisab,d.mobile,d.mobile2,d.name as driver_name 
	FROM dairy.trip_final AS t 
	LEFT OUTER JOIN dairy.driver AS d ON d.code=t.driver_code 
	WHERE t.trip_id='$trip_id'");
}

if(!$check_trip){
	AlertRightCornerError("Error..");
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	echo "<script>$('#entry_save_btn').attr('disabled',false);</script>";
	exit();
}

if(numRows($check_trip)==0)
{
	AlertRightCornerError("Trip not found !");
	errorLog("Running trip not found. TripId: $trip_id.",$conn,$page_name,__LINE__);
	echo "<script>$('#entry_save_btn').attr('disabled',false);</script>";
	exit();
}

$row_trip = fetchArray($check_trip);

$tno = $row_trip['tno'];
$d_code = $row_trip['driver_code'];
$trip_no = $row_trip['trip_no'];
$driver_name = $row_trip['driver_name'];
$mobile1 = $row_trip['mobile'];
$mobile2 = $row_trip['mobile2'];

if($trip_type=='RUNNING')
{
	$check_fix_lane = Qry($conn,"SELECT id FROM dairy.trip WHERE id='$trip_id' AND fix_lane='1'");
}
else
{
	$check_fix_lane = Qry($conn,"SELECT id FROM dairy.trip_final WHERE trip_id='$trip_id' AND fix_lane='1'");
}

if(!$check_fix_lane){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	AlertRightCornerError("Error..");
	exit();
}

if(numRows($check_fix_lane)>0)
{
	AlertRightCornerError("Trip belongs to fix lane.");
	echo "<script>$('#entry_save_btn').attr('disabled',false);</script>";
	exit();
}

$check_cache = Qry($conn,"SELECT id FROM dairy.hisab_cache WHERE tno='$tno'");

if(!$check_cache){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	AlertRightCornerError("Error..");
	exit();
}

if(numRows($check_cache)>0)
{
	AlertRightCornerError("Vehicle Hisab is in process. Please complete or reset hisab first !");
	echo "<script>$('#entry_save_btn').attr('disabled',false);</script>";
	exit();
}

$check_cache_trip = Qry($conn,"SELECT id FROM dairy.trip_cache WHERE tno='$tno'");

if(!$check_cache_trip){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	AlertRightCornerError("Error..");
	exit();
}

if(numRows($check_cache_trip)>0)
{
	AlertRightCornerError("Please wait ! Try again after some time !");
	echo "<script>$('#entry_save_btn').attr('disabled',false);</script>";
	exit();
}

$check_running_trip = Qry($conn,"SELECT id FROM dairy.running_scripts WHERE file_name!='LOAD_API_TRANS'");

if(!$check_running_trip){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	AlertRightCornerError("Error..");
	exit();
}

if(numRows($check_running_trip)>0)
{
	AlertRightCornerError("Please wait ! Try again after some time !");
	echo "<script>$('#entry_save_btn').attr('disabled',false);</script>";
	exit();
}

$pump_code = escapeString($conn,strtoupper($_POST['pump_name']));
$trans_date = escapeString($conn,strtoupper($_POST['txn_date']));
$qty = escapeString($conn,strtoupper($_POST['qty']));
$rate = escapeString($conn,strtoupper($_POST['rate']));
$amount = escapeString($conn,strtoupper($_POST['amount']));

$dateMsg = date("d/m/y",strtotime($trans_date));

$diesel_key = $tno."_".mt_rand().$timestamp;

$get_pump_details = Qry($conn,"SELECT name,comp,consumer_pump FROM dairy.diesel_pump_own WHERE code='$pump_code'");

if(!$get_pump_details){
	AlertRightCornerError("Error while Processing Request !");
	echo "<script>$('#entry_save_btn').attr('disabled',false);</script>";
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	exit();
}	

if(numRows($get_pump_details)==0)
{
	AlertRightCornerError("Fuel station not found !");
	echo "<script>$('#entry_save_btn').attr('disabled',false);</script>";
	exit();
}	

$row_pump = fetchArray($get_pump_details);
$pump_company = $row_pump['comp'];
$pump_name = $row_pump['name'];

if($row_pump['consumer_pump']!=$is_consumer_pump_entry)
{
	AlertRightCornerError("Fuel station not verified !");
	echo "<script>$('#entry_save_btn').attr('disabled',false);</script>";
	exit();
}

if($is_consumer_pump_entry!='1')
{
	if($qty <= 0)
	{
		AlertRightCornerError("Error : Invalid Qty !");
		echo "<script>$('#entry_save_btn').attr('disabled',false);</script>";
		exit();
	}
}

if($row_pump['consumer_pump']=='1')
{
	$check_stock = Qry($conn,"SELECT SUM(balance) as balance FROM dairy.diesel_pump_stock WHERE pumpcode='$pump_code'");
	
	if(!$check_stock){
		AlertRightCornerError("Error while Processing Request !");
		echo "<script>$('#entry_save_btn').attr('disabled',false);</script>";
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		exit();
	}
	
	$row_balance = fetchArray($check_stock);
	
	if($row_balance['balance']<$qty)
	{
		AlertRightCornerError("Error : Fuel not in Stock !");
		echo "<script>$('#entry_save_btn').attr('disabled',false);</script>";
		exit();
	}
	
	$get_qty = Qry($conn,"SELECT id,purchaseid,rate,balance FROM dairy.diesel_pump_stock WHERE pumpcode='$pump_code' AND balance>0");

	if(!$get_qty){
		AlertRightCornerError("Error while Processing Request !");
		echo "<script>$('#entry_save_btn').attr('disabled',false);</script>";
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		exit();
	}

	$numRowsQty = numRows($get_qty);
	
	if($numRowsQty==1)
	{
		$row_Qty = fetchArray($get_qty);
		
		if($row_Qty['balance']<$qty)
		{
			AlertRightCornerError("Fuel not in Stock. Stock : 01 !");
			echo "<script>$('#entry_save_btn').attr('disabled',false);</script>";
			exit();
		}
		
		$remainQty = sprintf("%.2f",$row_Qty['balance']-$qty);
		$StockId = $row_Qty['id'];
		$PurchaseId = $row_Qty['purchaseid'];
		
		$rate = $row_Qty['rate'];
		$amount = round($qty*$rate);
		$consumer_pump = "1";
		
		$total_qty = $qty;
		$total_amount = $amount;
	}
	else if($numRowsQty==2)
	{
		$consumer_pump = "1";
		
		$get_first_pump = Qry($conn,"SELECT id,purchaseid,rate,balance FROM dairy.diesel_pump_stock WHERE pumpcode='$pump_code' AND balance>0 
		ORDER BY id ASC LIMIT 1");
		
		if(!$get_first_pump){
			AlertRightCornerError("Error while Processing Request !");
			echo "<script>$('#entry_save_btn').attr('disabled',false);</script>";
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			exit();
		}

		$get_second_pump = Qry($conn,"SELECT id,purchaseid,rate,balance FROM dairy.diesel_pump_stock WHERE pumpcode='$pump_code' AND balance>0 
		ORDER BY id DESC LIMIT 1");
		
		if(!$get_second_pump){
			AlertRightCornerError("Error while Processing Request !");
			echo "<script>$('#entry_save_btn').attr('disabled',false);</script>";
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			exit();
		}

		$rowFirstPump = fetchArray($get_first_pump);
		$rowSecondPump = fetchArray($get_second_pump);
		
		if(($rowFirstPump['balance']+$rowSecondPump['balance'])<$qty)
		{
			AlertRightCornerError("Fuel not in Stock. Stock : 02 !");
			echo "<script>$('#entry_save_btn').attr('disabled',false);</script>";
			exit();
		}
		
		if($rowFirstPump['balance']>=$qty)
		{
			$split_entry="0";
			$rate = $rowFirstPump['rate'];
			$amount = round($qty*$rate);
			$remainQty = sprintf("%.2f",$rowFirstPump['balance']-$qty);
			$StockId = $rowFirstPump['id'];
			$PurchaseId = $rowFirstPump['purchaseid'];
			
			$total_qty = $qty;
			$total_amount = $amount;
		}
		else
		{
			$split_entry="1";
			
			$rate = $rowFirstPump['rate'];
			$rate2 = $rowSecondPump['rate'];
			$qty1 = $rowFirstPump['balance'];
			$qty2 = sprintf("%.2f",$qty-$qty1);
			$amount = round($rate * $qty1);
			$amount2 = round($rate2 * $qty2);
			$StockId = $rowFirstPump['id'];
			$StockId2 = $rowSecondPump['id'];
			$remainQty = 0;
			$remainQty2 = sprintf("%.2f",$rowSecondPump['balance']-$qty2);
			$PurchaseId = $rowFirstPump['purchaseid'];
			$PurchaseId2 = $rowSecondPump['purchaseid'];
			
			$total_qty = $qty1+$qty2;
			$total_amount = $amount+$amount2;
		}
		
	}
	else if($numRowsQty>2)
	{
		AlertRightCornerError("'Multiple Records found of fuel station !");
		echo "<script>$('#entry_save_btn').attr('disabled',false);</script>";
		exit();
	}
}
else
{
	$consumer_pump = "0";
	$total_qty = $qty;
	$total_amount = $amount;
}

$trans_id_Qry = GetTxnId_eDiary($conn,"DSL");
	
if(!$trans_id_Qry || $trans_id_Qry=="" || $trans_id_Qry=="0")
{
	AlertError("Error..");
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	echo "<script>$('#diesel_adv_button').attr('disabled',false);</script>";
	exit();
}
	
$trans_id = $trans_id_Qry;

StartCommit($conn);
$flag = true;

if($consumer_pump=="1")
{
	if($numRowsQty==1)
	{
	
		$insert_diesel_diary = Qry($conn,"INSERT INTO dairy.diesel(unq_id,trip_id,trip_no,trans_id,tno,rate,qty,amount,date,narration,branch,
		branch_user,timestamp,stockid) VALUES ('$diesel_key','$trip_id','$trip_no','$trans_id','$tno','$rate','$qty','$amount','$trans_date',
		'$pump_company-$pump_code','$branch','$_SESSION[ediary_fix_admin]','$timestamp','$PurchaseId')");

		if(!$insert_diesel_diary){
			$flag = false;
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		}	

		$insert_diesel_entry=Qry($conn,"INSERT INTO dairy.diesel_entry (unq_id,bill_no,tno,diesel,card_pump,card,veh_no,dsl_company,
		narration,branch,date,timestamp,dsl_mobileno) VALUES ('$diesel_key','','$tno','$amount','PUMP','$pump_code','$pump_code',
		'$pump_company','$pump_company-$pump_code','$branch','$trans_date','$timestamp','')");

		if(!$insert_diesel_entry){
			$flag = false;
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		}

		$updateStock1 = Qry($conn,"UPDATE dairy.diesel_pump_stock SET balance='$remainQty' WHERE id='$StockId'");

		if(!$updateStock1){
			$flag = false;
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		}

	}
	else
	{
	
		if($split_entry=="0")
		{
			
			$insert_diesel_diary=Qry($conn,"INSERT INTO dairy.diesel(unq_id,trip_id,trip_no,trans_id,tno,rate,qty,amount,date,narration,branch,
			branch_user,timestamp,stockid) VALUES ('$diesel_key','$trip_id','$trip_no','$trans_id','$tno','$rate','$qty','$amount','$trans_date',
			'$pump_company-$pump_code','$branch','$_SESSION[ediary_fix_admin]','$timestamp','$PurchaseId')");

			if(!$insert_diesel_diary)
			{
				$flag = false;
				errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			}	

			$insert_diesel_entry=Qry($conn,"INSERT INTO dairy.diesel_entry (unq_id,bill_no,tno,diesel,card_pump,card,veh_no,dsl_company,
			narration,branch,date,timestamp,dsl_mobileno) VALUES ('$diesel_key','','$tno','$amount','PUMP','$card','$pump_code',
			'$pump_company','$pump_company-$pump_code','$branch','$trans_date','$timestamp','')");

			if(!$insert_diesel_entry){
				$flag = false;
				errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			}

			$updateStock1 = Qry($conn,"UPDATE dairy.diesel_pump_stock SET balance='$remainQty' WHERE id='$StockId'");

			if(!$updateStock1){
				$flag = false;
				errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			}	
		
		}
		else
		{
		
			$insert_diesel_diary=Qry($conn,"INSERT INTO dairy.diesel(unq_id,trip_id,trip_no,trans_id,tno,rate,qty,amount,date,narration,branch,branch_user,
			timestamp,stockid) VALUES ('$diesel_key','$trip_id','$trip_no','$trans_id','$tno','$rate','$qty1','$amount','$trans_date','$pump_company-$pump_code',
			'$branch','$_SESSION[ediary_fix_admin]','$timestamp','$PurchaseId')");

			if(!$insert_diesel_diary)
			{
				$flag = false;
				errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			}	

			$insert_diesel_entry=Qry($conn,"INSERT INTO dairy.diesel_entry(unq_id,bill_no,tno,diesel,card_pump,card,veh_no,dsl_company,
			narration,branch,date,timestamp,dsl_mobileno) VALUES ('$diesel_key','','$tno','$amount','PUMP','$card','$pump_code',
			'$pump_company','$pump_company-$pump_code','$branch','$trans_date','$timestamp','')");

			if(!$insert_diesel_entry){
				$flag = false;
				errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			}

			$updateStock1 = Qry($conn,"UPDATE dairy.diesel_pump_stock SET balance='0' WHERE id='$StockId'");

			if(!$updateStock1){
				$flag = false;
				errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			}	

			$insert_diesel_diary2=Qry($conn,"INSERT INTO dairy.diesel(unq_id,trip_id,trip_no,trans_id,tno,rate,qty,amount,date,narration,branch,branch_user,
			timestamp,stockid) VALUES ('$diesel_key','$trip_id','$trip_no','$trans_id','$tno','$rate2','$qty2','$amount2','$trans_date',
			'$pump_company-$pump_code','$branch','$_SESSION[ediary_fix_admin]','$timestamp','$PurchaseId2')");

			if(!$insert_diesel_diary2){
				$flag = false;
				errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			}	

			$insert_diesel_entry2=Qry($conn,"INSERT INTO dairy.diesel_entry (unq_id,bill_no,tno,diesel,card_pump,card,veh_no,dsl_company,
			narration,branch,date,timestamp,dsl_mobileno) VALUES ('$diesel_key','','$tno','$amount2','PUMP','$card','$pump_code',
			'$pump_company','$pump_company-$pump_code','$branch','$trans_date','$timestamp','')");

			if(!$insert_diesel_entry2){
				$flag = false;
				errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			}

			$updateStock2 = Qry($conn,"UPDATE dairy.diesel_pump_stock SET balance='$remainQty2' WHERE id='$StockId2'");

			if(!$updateStock2){
				$flag = false;
				errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			}			
		}
	}	
}
else
{	
	$insert_diesel_diary=Qry($conn,"INSERT INTO dairy.diesel(unq_id,trip_id,trip_no,trans_id,tno,rate,qty,amount,date,narration,branch,
	branch_user,timestamp) VALUES ('$diesel_key','$trip_id','$trip_no','$trans_id','$tno','$rate','$qty','$amount','$trans_date',
	'$pump_company-$pump_code','$branch','$_SESSION[ediary_fix_admin]','$timestamp')");

	if(!$insert_diesel_diary){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}	

	$insert_diesel_entry=Qry($conn,"INSERT INTO dairy.diesel_entry(unq_id,tno,diesel,card_pump,card,veh_no,dsl_company,narration,
	branch,date,timestamp) VALUES ('$diesel_key','$tno','$amount','PUMP','$pump_code','$pump_code','$pump_company',
	'$pump_company-$pump_code','$branch','$trans_date','$timestamp')");

	if(!$insert_diesel_entry){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}	
}	

if($trip_type=='RUNNING')
{
	$update_trip=Qry($conn,"UPDATE dairy.trip SET diesel=diesel+'$total_amount',diesel_qty=ROUND(diesel_qty+'$total_qty',2) WHERE id='$trip_id'");
	if(!$update_trip){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
}
else
{
	$update_trip=Qry($conn,"UPDATE dairy.trip_final SET diesel=diesel+'$total_amount',diesel_qty=ROUND(diesel_qty+'$total_qty',2) WHERE trip_id='$trip_id'");
	if(!$update_trip){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
	
	$get_total_qty = Qry($conn,"SELECT SUM(qty) as qty1 FROM dairy.diesel WHERE trip_id IN(SELECT trip_id FROM dairy.trip_final WHERE trip_no='$trip_no')");
	if(!$get_total_qty){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
	
	$get_gps_km = Qry($conn,"SELECT id,gps_km_of_trip FROM dairy.opening_closing WHERE trip_no='$trip_no'");
	if(!$get_gps_km){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
	
	$row_gps_km = fetchArray($get_gps_km);
	$row_diesel_qty = fetchArray($get_total_qty);
	
	$new_avg = sprintf("%.2f",($row_gps_km['gps_km_of_trip']/$row_diesel_qty['qty1']));
	
	$update_new_km = Qry($conn,"UPDATE dairy.opening_closing SET avg='$new_avg' WHERE id='$row_gps_km[id]'");
	if(!$update_new_km){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
}

$update_qty_stock = Qry($conn,"UPDATE dairy.own_truck SET diesel_left = diesel_left + '$total_qty' WHERE tno='$tno' AND diesel_trip_id!='-1'");

if(!$update_qty_stock){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}
	
if($flag)
{	
	if(strlen($mobile2)==10)
	{
		$adv_msg_mobile = $mobile2;
	}
	else if(strlen($mobile1)==10)
	{
		$adv_msg_mobile = $mobile1;
	}
	
	if(strlen($adv_msg_mobile)==10)
	{
		MsgDieselDiary($adv_msg_mobile,$tno,"PUMP",$total_qty,$branch,$dateMsg);
	}
	
	MySQLCommit($conn);
	closeConnection($conn);
	
	AlertRightCornerSuccess("Diesel Entry Success !");
	echo "<script>$('#modal_close_btn')[0].click();$('#add_btn')[0].click();$('#entry_save_btn').attr('disabled',false);</script>";
	exit();
}
else
{
	MySQLRollBack($conn);
	closeConnection($conn);
	AlertRightCornerError("Error while processing request !!");
	echo "<script>$('#entry_save_btn').attr('disabled',false);</script>";
	exit();
}
?>