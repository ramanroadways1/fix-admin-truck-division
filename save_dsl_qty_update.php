<?php
require_once("connect.php");

$timestamp = date("Y-m-d H:i:s");

echo "<script>$('#btn_save_qty').attr('disabled',true);</script>";
	
if(!isset($_SESSION['tno_tank_capacity']))
{
	AlertError("Vehicle number not found !");
	echo "<script>
		$('#btn_save_qty').attr('disabled',false);
	</script>";
	exit();
}

$tno = escapeString($conn,$_POST['tno']);
$trip_id = escapeString($conn,$_POST['trip_id']);
$qty = escapeString($conn,$_POST['qty']);

if($_SESSION['tno_tank_capacity'] != $tno)
{
	AlertError("Vehicle number not verified !");
	echo "<script>
		$('#btn_save_qty').attr('disabled',false);
	</script>";
	exit();
}

$chk_trip = Qry($conn,"SELECT id FROM dairy.trip WHERE tno='$tno' ORDER BY id DESC LIMIT 1");

if(!$chk_trip){
	AlertError("Error while processing request !");
	errorLog(getMySQLError($conn),$conn,$page_url,__LINE__);
	exit();
}

if(numRows($chk_trip)==0)
{
	AlertError("Running trip not found !");
	exit();
}

$row = fetchArray($chk_trip);

if($row['id'] != $trip_id)
{
	AlertError("Trip not verified !");
	exit();
}

$chk_tank_cap = Qry($conn,"SELECT diesel_tank_cap,diesel_trip_id FROM dairy.own_truck WHERE tno='$tno'");

if(!$chk_tank_cap){
	AlertError("Error while processing request !");
	errorLog(getMySQLError($conn),$conn,$page_url,__LINE__);
	exit();
}

if(numRows($chk_tank_cap)==0)
{
	AlertError("Vehicle not found !");
	exit();
}

$row_tank_cap = fetchArray($chk_tank_cap);

if($row_tank_cap['diesel_trip_id'] != 0)
{
	AlertError("Qty already updated !");
	exit();
}

if($qty > $row_tank_cap['diesel_tank_cap'] != 0)
{
	AlertError("Check Qty : Tank capacity is: $row_tank_cap[diesel_tank_cap] !");
	exit();
}

StartCommit($conn);
$flag = true;
	
$update_qry = Qry($conn,"UPDATE dairy.own_truck SET diesel_left='$qty',diesel_trip_id='$trip_id',
qty_update_user='$_SESSION[ediary_fix_admin]',qty_update_timestamp='$timestamp' WHERE tno='$tno'");
	
if(!$update_qry){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

if($flag)
{
	MySQLCommit($conn);
	closeConnection($conn);
	echo "<script>
		$('#hide_dsl_update_modal')[0].click();
		$('#update_qty_btn_$tno').attr('disabled',true);
		// $('#update_qty_btn_$tno').html('Updated');
		$('#loadicon').fadeOut('slow');
	</script>";
	// AlertRightCornerSuccessFadeFast("Route approved !");
}
else
{
	MySQLRollBack($conn);
	closeConnection($conn);
	AlertError("Error While Processing Request !");
	exit();
}
?>