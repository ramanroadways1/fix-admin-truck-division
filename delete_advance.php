<?php
require_once("./connect.php");

$id = escapeString($conn,($_POST['id']));
$type = escapeString($conn,($_POST['txn_type']));

if($type !='CASH' AND $type != 'CHQ' AND $type != 'RTGS')
{
	AlertErrorTopRight("Invalid Txn type !");
	echo "<script> $('#delete_cash_btn_$id').attr('disabled',false); </script>";
	exit();
}

if($type=='CASH')
{
	$trans_table="cash";
}
else if($type=='CHQ')
{
	$trans_table="cheque";
}
else if($type=='RTGS')
{
	$trans_table="rtgs";
}

$get_data = Qry($conn,"SELECT trip_id,trans_id,vou_no,tno,amount,date,branch,timestamp FROM dairy.`$trans_table` WHERE id='$id'");

if(!$get_data)
{
	AlertErrorTopRight("Error while processing request !");
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	echo "<script> $('#delete_cash_btn_$id').attr('disabled',false); </script>";
	exit();
}

if(numRows($get_data) == 0)
{
	AlertErrorTopRight("Entry not found !");
	echo "<script> $('#delete_cash_btn_$id').attr('disabled',false); </script>";
	exit();
}

$row = fetchArray($get_data);

$tno = $row['tno'];
$trip_id = $row['trip_id'];
$trans_id = $row['trans_id'];
$amount = $row['amount'];
$vou_no = $row['vou_no'];
$trans_date = $row['date'];
$branch = $row['branch'];

require_once("./check_cache.php");

if(strpos($trans_id,'HAPPAY')!== false)
{
	AlertErrorTopRight("You can\'t delete. Happay Txns !");
	echo "<script> $('#delete_cash_btn_$id').attr('disabled',false); </script>";
	exit();
}

if(strpos($trans_id,$type)!== false)
{
}
else
{
	AlertErrorTopRight("$type Txn not verified !");
	echo "<script> $('#delete_cash_btn_$id').attr('disabled',false); </script>";
	exit();
}

$chk_trip = Qry($conn,"SELECT id FROM dairy.trip WHERE id='$trip_id'");

if(!$chk_trip)
{
	AlertErrorTopRight("Error while processing request !");
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	echo "<script> $('#delete_cash_btn_$id').attr('disabled',false); </script>";
	exit();
}

if(numRows($chk_trip)==0)
{
	AlertErrorTopRight("Running trip not found !");
	echo "<script> $('#delete_cash_btn_$id').attr('disabled',false); </script>";
	exit();
}

if($type=='RTGS')
{
	$chk_rtgs_approval = Qry($conn,"SELECT id FROM rtgs_fm WHERE fno='$vou_no' AND approval='1'");
	
	if(!$chk_rtgs_approval)
	{
		AlertErrorTopRight("Error while processing request !");
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		echo "<script> $('#delete_cash_btn_$id').attr('disabled',false); </script>";
		exit();
	}
	
	if(numRows($chk_rtgs_approval)>0)
	{
		AlertErrorTopRight("RTGS Request Approved !");
		echo "<script> $('#delete_cash_btn_$id').attr('disabled',false); </script>";
		exit();
	}
}
	
StartCommit($conn);
$flag = true;

$log_data = "Type : $type, Branch : $branch, VouNo : $vou_no, TruckNo : $tno, Amount : $amount, Trans_date : $trans_date, Trans_id : $trans_id, Trip_id : $trip_id.";

$insert_log = Qry($conn,"INSERT INTO dairy.ediary_admin_log(code,action,desct,timestamp) VALUES ('$tno','ADV_DELETE','$log_data','$timestamp')");

if(!$insert_log){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

if($type=='CASH')
{
	$sel_cash_id = Qry($conn,"SELECT id,comp FROM cashbook WHERE vou_no='$vou_no'");
	
	if(!$sel_cash_id){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
	
	if(numRows($sel_cash_id) == 0){
		$flag = false;
		errorLog("Cashbook entry not found.",$conn,$page_name,__LINE__);
	}
	
	$row_cash_id = fetchArray($sel_cash_id);
	
	$cash_id = $row_cash_id['id'];
	$company = $row_cash_id['comp'];
	
	if($company=='RRPL')
	{
		$col_cash = "balance";
	}
	else
	{
		$col_cash = "balance2";
	}

	$query_update = Qry($conn,"update user set `$col_cash`=`$col_cash`+'$amount' where username='$branch'");
	
	if(!$query_update)
	{
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
	
	if(AffectedRows($conn)==0)
	{
		$flag = false;
		errorLog("Branch Balance not updated. Branch : $branch. TransId: $trans_id, VouNo: $vou_no.",$conn,$page_name,__LINE__);
	}

	$delete_cashbook = Qry($conn,"DELETE FROM cashbook WHERE id='$cash_id'");
	
	if(!$delete_cashbook)
	{
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
	
	if(AffectedRows($conn)==0)
	{
		$flag = false;
		errorLog("Cashbook Entry not deleted. Branch : $branch. TransId: $trans_id, VouNo: $vou_no.",$conn,$page_name,__LINE__);
	}

	$update_cashbook = Qry($conn,"UPDATE cashbook SET `$col_cash`=`$col_cash`+'$amount' WHERE id>'$cash_id' AND comp='$company' AND user='$branch'");
	
	if(!$update_cashbook)
	{
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
}
else if($type=='CHQ')
{
	$delete_passbook = Qry($conn,"DELETE FROM passbook WHERE vou_no='$vou_no'");
	
	if(!$delete_passbook)
	{
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
}
else if($type=='RTGS')
{
	$sel_rtgs = Qry($conn,"SELECT id FROM rtgs_fm WHERE fno='$vou_no' AND approval='1'");
	
	if(!$sel_rtgs)
	{
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
	
	if(numRows($sel_rtgs)>0)
	{
		$flag = false;
		errorLog("RTGS Approved. Vou_no: $vou_no",$conn,$page_name,__LINE__);
	}
	
	$delete_passbook = Qry($conn,"DELETE FROM passbook WHERE vou_no='$vou_no'");
	
	if(!$delete_passbook)
	{
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
	
	$delete_rtgs_data = Qry($conn,"DELETE FROM rtgs_fm WHERE fno='$vou_no' AND approval!=1");
	
	if(!$delete_rtgs_data)
	{
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
}

$delete_truck_vou = Qry($conn,"DELETE FROM mk_tdv WHERE tdvid='$vou_no'");

if(!$delete_truck_vou)
{
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

$delete = Qry($conn,"DELETE FROM dairy.`$trans_table` WHERE id='$id'");
	
if(!$delete)
{
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

	$select_book_id = Qry($conn,"SELECT id,driver_code FROM dairy.driver_book WHERE trans_id='$trans_id'");
	
	if(!$select_book_id)
	{
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
	
	if(numRows($select_book_id)==0)
	{
		$flag = false;
		errorLog("Transaction not found in driver book. Trans_id: $trans_id.",$conn,$page_name,__LINE__);
	}
	
	$row_book_id = fetchArray($select_book_id);
	
	$book_id = $row_book_id['id'];
	$driver_code = $row_book_id['driver_code'];
	
	$update_book = Qry($conn,"UPDATE dairy.driver_book SET balance=balance-'$amount' WHERE id>'$book_id' AND tno='$tno'");
	
	if(!$update_book)
	{
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
	
	$delete_row = Qry($conn,"DELETE FROM dairy.driver_book WHERE id='$book_id'");
		
	if(!$delete_row)
	{
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);	
	}
	
	if(AffectedRows($conn)==0)
	{
		$flag = false;
		errorLog("Transaction not found in driver book. Trans_id: $trans_id. DriverBook Id: $book_id.",$conn,$page_name,__LINE__);
	}
		
	$update_driver_balance = Qry($conn,"UPDATE dairy.driver_up SET amount_hold=amount_hold-'$amount' WHERE down=0 AND code='$driver_code' 
	ORDER BY id DESC LIMIT 1");
	
	if(!$update_driver_balance)
	{
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);	
	}
			
	$update_trip = Qry($conn,"UPDATE dairy.trip SET `$trans_table`=`$trans_table`-'$amount' WHERE id='$trip_id'");
			
	if(!$update_trip)
	{
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);	
	}
		
	if($flag)
	{
		MySQLCommit($conn);
		closeConnection($conn);
		
		AlertRightCornerSuccess("Deleted Successfully !");
		echo "<script>
			ReLoadPage();
		</script>";
		exit();
	}
	else
	{
		MySQLRollBack($conn);
		closeConnection($conn);
		
		AlertErrorTopRight("Error while processing Request !");
		echo "<script> $('#delete_cash_btn_$id').attr('disabled',false); </script>";
		exit();
	}
?> 