<?php include("header.php"); ?>

<?php
$chk = Qry($conn,"SELECT id FROM _access_control WHERE username='$_SESSION[ediary_fix_admin]' AND func_id=(SELECT id FROM 
_access_control_func_list WHERE session_role='8' AND func_name='Add_Market_Driver') AND u_view='1'");
			  
if(numRows($chk)==0)
{
	echo "<script>window.location.href='./';</script>";
	exit();
}
?>

<div class="content-wrapper">
      <section class="content-header">
          <h1 style="font-size:16px;">Add Market Vehicle Driver : </h1>
       </section>
       
	   <section class="content">
          <div class="row">
            <div class="col-xs-12"> 
			<div class="box">
                <div class="box-body">
<?php
$chk_insert = Qry($conn,"SELECT id FROM _access_control WHERE username='$_SESSION[ediary_fix_admin]' AND func_id=(SELECT id FROM 
_access_control_func_list WHERE session_role='8' AND func_name='Add_Market_Driver') AND u_insert='1'");
			  
if(numRows($chk_insert)>0)
{
?>				
<div class="col-md-12">&nbsp;</div>

<form autocomplete="off" id="Form1">
				
				<div class="col-md-8 col-md-offset-2">
					<div class="row">
						
						<div class="form-group col-md-6">
							<label>License Number <font color="red"><sup>*</sup></font></label>
							<input autocomplete="off" required="required" oninput="this.value=this.value.replace(/[^A-Za-z0-9]/,'')" type="text" class="form-control"  id="lic_no" name="lic_no" />
						</div>
						
						<div class="form-group col-md-6">
							<label>&nbsp;</label>
							<?php if(!isMobile()) { echo "<label>&nbsp;</label><br />"; } ?>
							<button type="button" onclick="FetchLicenseDetails()" id="lic_fetch_btn" class="btn btn-primary btn-sm"><i class="fa fa-refresh" aria-hidden="true"></i> Fetch Details </button>
						</div>
					</div>
					
					<div class="form-group col-md-12" id="driver_lic_result_main_div" style="display:none">
						<div class="row" id="driver_lic_result" style="border:1px solid #ddd;border-radius:10px">
							
							
						</div>
					</div>
							
					<div class="row">					
						<div class="form-group col-md-6">
							<label>Add for Branch <font color="red"><sup>*</sup></font></label>
							<select style="font-size:12px" data-size="8" name="branch" id="branch" class="form-control selectpicker" data-live-search="true" required="required">
								<option style="font-size:12px" data-tokens="" value="">--select option--</option>
							<?php
							$get_branches = Qry($conn,"SELECT username FROM user WHERE role='2' AND username NOT IN('HEAD','DUMMY') ORDER BY username ASC");

							if(numRows($get_branches)>0)
							{
								while($row_branch = fetchArray($get_branches))
								{
									echo "<option style='font-size:12px' data-tokens='$row_branch[username]' value='$row_branch[username]'>$row_branch[username]</option>";
								}
							}
							?>
							</select>
						</div>
						
						<div class="form-group col-md-6">
							<label>Driver Name <font color="red"><sup>*</sup></font></label>
							<input autocomplete="off" required="required" oninput="this.value=this.value.replace(/[^a-z A-Z0-9,-/]/,'')" type="text" class="form-control" id="driver_name" name="driver_name" />
						</div>
						
						<div class="form-group col-md-4">
							<label>Mobile Number <font color="red"><sup>*</sup></font></label>
							<input autocomplete="off" required="required" maxlength="10" oninput="this.value=this.value.replace(/[^0-9]/,'')" type="text" class="form-control" name="mobile" />
						</div>
						
						<div class="form-group col-md-4">
							<label>Alt. Mobile Number </label>
							<input autocomplete="off" maxlength="10" oninput="this.value=this.value.replace(/[^0-9]/,'')" type="text" class="form-control" name="mobile2" />
						</div>
						
						<div class="form-group col-md-4">
							<label>Aadhar Number <font color="red"><sup>*</sup></font></label>
							<input autocomplete="off" maxlength="12" required="required" oninput="this.value=this.value.replace(/[^0-9]/,'')" type="text" class="form-control" name="aadhar_no" />
						</div>
						
						<div class="form-group col-md-4">
							<label>License Copy <font color="red"><sup>(front) *</sup></font></label>
							<input style="font-size:12px !important" autocomplete="off" required="required" type="file" class="form-control" name="lic_front" />
						</div>
						
						<div class="form-group col-md-4">
							<label>License Copy <font color="red"><sup>(rear) *</sup></font></label>
							<input style="font-size:12px !important" autocomplete="off" required="required" type="file" class="form-control" name="lic_rear" />
						</div>
						
						<div class="form-group col-md-4">
							<label>Aadhar Copy <font color="red"><sup>(front) *</sup></font></label>
							<input style="font-size:12px !important" autocomplete="off" required="required" type="file" class="form-control" name="addhar_front" />
						</div>
						
						<div class="form-group col-md-4">
							<label>Aadhar Copy <font color="red"><sup>(rear) *</sup></font></label>
							<input style="font-size:12px !important" autocomplete="off" required="required" type="file" class="form-control" name="addhar_rear" />
						</div>
						
						<div class="form-group col-md-8">
							<label>Address <font color="red"><sup>*</sup></font></label>
							<textarea autocomplete="off" required="required" oninput="this.value=this.value.replace(/[^A-Z a-z0-9*@#/,-]/,'')" class="form-control" id="driver_addr" name="addr"></textarea>
						</div>
						
						<div class="form-group col-md-12">
							<button type="submit" class="btn btn-sm btn-success <?php if(isMobile()) { echo "btn-block"; } ?>" id="add_btn"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> &nbsp; Add Driver</button>
						</div>
					</div>
				</div>
			</form>	
				<?php
				}
				?>
				<div class="col-md-12">&nbsp;</div>
				
				 </div> 
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

<div id="func_result"></div>  
<div id="load_modal_div"></div>  
<div id="func_result2"></div>  
 
<script type="text/javascript">
$(document).ready(function (e) {
$("#Form1").on('submit',(function(e) {
$("#loadicon").show();
$("#add_btn").attr("disabled", true);
e.preventDefault();
	$.ajax({
	url: "./save_add_market_driver.php",
	type: "POST",
	data:  new FormData(this),
	contentType: false,
	cache: false,
	processData:false,
	success: function(data){
		$("#func_result").html(data);
	},
	error: function() 
	{} });}));});
</script>
 
<script>
function FetchLicenseDetails()
{
	var lic_no = $('#lic_no').val();
	
	if(lic_no=='')
	{
		Swal.fire({icon: 'warning',html: '<font size=\'2\' color=\'black\'>Enter license number first !</font>',});
	}
	else
	{
		$('#lic_fetch_btn').attr('disabled',true); 
		$('#loadicon').show();
		jQuery.ajax({
			url: "api_fetch_license.php",
			data: 'lic_no=' + lic_no,
			type: "POST",
			success: function(data) {
			$("#driver_lic_result").html(data);
			},
			error: function() {}
		});
	}
}
</script>
 
<?php include("footer.php") ?>