<?php
include "../connect.php";

$type = escapeString($conn,$_REQUEST['type']);
?>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate" />
	<meta http-equiv="Pragma" content="no-cache" />
	<meta http-equiv="Expires" content="0" />
	<meta name="robots" content="noindex,nofollow"/>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Happay-Card : Control Panel</title>
	<link rel="shortcut icon" type="image/png" href="../happay_small.jpg"/>
	<link rel="shortcut icon" type="image/png" href="../happay_small.png"/>
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>  
<link href="https://fonts.googleapis.com/css?family=Baumans" rel="stylesheet">
<link rel="stylesheet" href="../../b5aY6EZzK52NA8F/font-awesome-4.7.0/css/font-awesome.min.css">
<link href="../../b5aY6EZzK52NA8F/google_font.css" rel="stylesheet">
<script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.flash.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.html5.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.print.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.colVis.min.js"></script>
<script type="text/javascript" src="https://datatables.net/release-datatables/extensions/Scroller/js/dataTables.scroller.js"></script>
<link href="https://cdn.datatables.net/buttons/1.5.1/css/buttons.dataTables.min.css" rel="stylesheet" type="text/css" />
<link href="../../b5aY6EZzK52NA8F/data_table_custom.css" rel="stylesheet" type="text/css" />

<script>
var message="Function Disabled!";
function clickIE4(){
if (event.button==2){
alert(message);
return false;
}
}
function clickNS4(e){
if (document.layers||document.getElementById&&!document.all){
if (e.which==2||e.which==3){
alert(message);
return false;
}
}
}
if (document.layers){
document.captureEvents(Event.MOUSEDOWN);
document.onmousedown=clickNS4;
}
else if (document.all&&!document.getElementById){
document.onmousedown=clickIE4;
}
document.oncontextmenu=new Function("return false")

function disableCtrlKeyCombination(e)
{
        //list all CTRL + key combinations you want to disable
        var forbiddenKeys = new Array('a', 'c', 'x', 'v');
        var key;
        var isCtrl;
        if(window.event)
        {
                key = window.event.keyCode;     //IE
                if(window.event.ctrlKey)
                        isCtrl = true;
                else
                        isCtrl = false;
        }
        else
        {
                key = e.which;     //firefox
                if(e.ctrlKey)
                        isCtrl = true;
                else
                        isCtrl = false;
        }
        //if ctrl is pressed check if other key is in forbidenKeys array
        if(isCtrl)
        {
                for(i=0; i<forbiddenKeys.length; i++)
                {
                        //case-insensitive comparation
                        if(forbiddenKeys[i].toLowerCase() == String.fromCharCode(key).toLowerCase())
                        {
                                // alert('Key combination CTRL + ' +String.fromCharCode(key)+' has been disabled.');
                                return false;
                        }
                }
        }
        return true;
}
</script>

<style>
 .dataTables_scroll{ margin-bottom: 20px;}
 .table {margin:0px !important;}
</style>

</head>

<div id="loadicon" style="display:none;position: fixed; right: 0px; top: 0px; width: 100%;height: 100%; background-color:#FFF; z-index: 30001; opacity: 0.9;">
	<center><img style="margin-top:150px" src="../loader.gif" /></center>
</div>		  

<body style="font-family: 'Open Sans', sans-serif !important" onkeypress="return disableCtrlKeyCombination(event);" onkeydown = "return disableCtrlKeyCombination(event);" class="hold-transition skin-blue sidebar-mini">

<div class="container-fluid">
	
	<div class="row">
		
<div style="background-color:;padding-top:6px;padding-bottom:6px;" class="bg-primary form-group col-md-12">
	<div class="row">
		<div class="col-md-4">
			<button type="button" onclick="window.close();" class="btn btn-sm btn-default pull-left"><span class="glyphicon glyphicon-cross"></span> Close window</button>
		</div>
		<div class="col-md-4">
			<center><h5 id="header_text">
			<?php
			if($type=='TOTAL_CARD')
			{
				echo "Total Cards";
			}
			else if($type=='ACTIVE_CARD')
			{
				echo "Active Cards";
			}
			else if($type=='ASSIGNED_CARD')
			{
				echo "Assigned Cards";
			}
			else if($type=='TODAY_TRANS')
			{
				echo "All Transactions";
			}
			else if($type=='WALLET_LOAD')
			{
				echo "Wallet Load";
			}
			else if($type=='WALLET_WDL')
			{
				echo "Wallet Withdrawn";
			}
			else if($type=='COMPANY_WALLET')
			{
				echo "Company Wallet Load";
			}
			else if($type=='ATM_EXP')
			{
				echo "ATM Transactions";
			}
			else if($type=='FEE_EXP')
			{
				echo "ATM Fee Transactions";
			}
			else if($type=='CARD_TRANS')
			{
				echo "Card Transactions";
			}
			else if($type=='REVERSAL')
			{
				echo "Reversal Transactions";
			}
			?>
			</span></h5>
		</div>
	</div>	
</div>

	
<div class="form-group col-md-12 table-responsive" id="getPAGEDIV">
<div class="card-body" style="min-height: 670px; background-color: #fff;"> 
	<table id="user_data" class="table table-bordered table-hover" style="background-color:#fff;">
 <thead style="" class="thead-light bg-success">
	<tr>
		<?php
			if($type=='TOTAL_CARD')
			{
			?>
			<th>Id</th>  
			<th>Vehicle_No</th>  
			<th>Card_Kit_Id</th>  
			<th>Is_Assigned</th>  
			<th>Active</th>  
			<th>Assigned_To</th>  
			<?php			
			}
			else if($type=='COMPANY_WALLET')
			{
			?>
			<th>Id</th>  
			<th>Merchant</th>  
			<th>Trans_Id</th>  
			<th>Trans_Type <span class="glyphicon glyphicon-filter"></span></th>  
			<th>Amount</th>  
			<th>Trans_Date <span class="glyphicon glyphicon-filter"></span></th>  
			<?php				
			}
			else
			{
			?>
			<th>Id</th>  
			<th>Vehicle_No <span class="glyphicon glyphicon-filter"></span></th>  
			<th>Merchant</th>  
			<th>Trans_Id</th>  
			<th>Trans_Id(RRPL)</th>  
			<th>Trans_Date <span class="glyphicon glyphicon-filter"></span></th>  
			<th>Trans_Type <span class="glyphicon glyphicon-filter"></span></th>  
			<th>Amount</th>  
			<th>Date <span class="glyphicon glyphicon-filter"></span></th>  
			<?php				
			}
		?>
	</tr>	
</thead> 
</table>
		</div>
	</div>
	</div>
</div>
</body>
</html>

<script type="text/javascript">
function Load_Data_One(type1){
	// alert(type1)
$("#loadicon").show();
 var table = jQuery("#user_data").dataTable({ 
		"scrollY": 500,
        "scrollX": true,
		"lengthMenu": [ [50, 100, 500, -1], [50, 100, 500, "All"] ], 
		"bProcessing": true,
		"scroller": true,
        "scrollCollapse": true,
		"sPaginationType":"full_numbers",
		"dom": "lBfrtip",
		"responsive": true,
		"ordering": true,
		"buttons": [
			"copy", "excel", "print", "colvis",
		],
		"language": {
            "loadingRecords": "&nbsp;",
            "processing": "<center> <font color=brown> Please wait while Loading </font> <img src=../../b5aY6EZzK52NA8F/load_table.gif height=20> </center>"
        },
		"order": [[0, "asc" ]],
		"columnDefs":[
	{ 
    "targets": 0, //Comma separated values
    "visible": true,
    "searchable": false 
	},
	 ], 
        "serverSide": true,
		 "ajax": {
            "url": "_load_data.php",
            "type": "POST",
			"data": function ( d ) {
                d.type_load = type1;
                // d.custom = $('#myInput').val();
                // etc
            }
		},
		"columns": [
			{"data": "0"},
			{"data": "1"},
			{"data": "2"},
			{"data": "3"},
			{"data": "4"},
			{"data": "5"},
		],
		"initComplete": function(settings, json) {
 		$("#loadicon").hide();
 		}
    } );
}

function Load_Data_Two(type1){
	// alert(type1)
$("#loadicon").show();
 var table = jQuery("#user_data").dataTable({ 
		"scrollY": 500,
        "scrollX": true,
		"lengthMenu": [ [50, 100, 500, -1], [50, 100, 500, "All"] ], 
		"bProcessing": true,
		"scroller": true,
        "scrollCollapse": true,
		"sPaginationType":"full_numbers",
		"dom": "lBfrtip",
		"responsive": true,
		"ordering": true,
		"buttons": [
			"copy", "excel", "print", "colvis",
		],
		"language": {
            "loadingRecords": "&nbsp;",
            "processing": "<center> <font color=brown> Please wait while Loading </font> <img src=../../b5aY6EZzK52NA8F/load_table.gif height=20> </center>"
        },
		"order": [[0, "asc" ]],
		"columnDefs":[
	{ 
    "targets": 0, //Comma separated values
    "visible": true,
    "searchable": false 
	},
	 ], 
        "serverSide": true,
		 "ajax": {
            "url": "_load_data.php",
            "type": "POST",
			"data": function ( d ) {
                d.type_load = type1;
             }
		},
		"columns": [
			{"data": "0"},
			{"data": "1"},
			{"data": "2"},
			{"data": "3"},
			{"data": "4"},
			{"data": "5"},
		],
		"initComplete": function(settings, json) {
 		$("#loadicon").hide();
 		}
    } );
}

function Load_Data_Three(type1){
	// alert(type1)
$("#loadicon").show();
 var table = jQuery("#user_data").dataTable({ 
		"scrollY": 500,
        "scrollX": true,
		"lengthMenu": [ [50, 100, 500, -1], [50, 100, 500, "All"] ], 
		"bProcessing": true,
		"scroller": true,
        "scrollCollapse": true,
		"sPaginationType":"full_numbers",
		"dom": "lBfrtip",
		"responsive": true,
		"ordering": true,
		"buttons": [
			"copy", "excel", "print", "colvis",
		],
		"language": {
            "loadingRecords": "&nbsp;",
            "processing": "<center> <font color=brown> Please wait while Loading </font> <img src=../../b5aY6EZzK52NA8F/load_table.gif height=20> </center>"
        },
		"order": [[0, "asc" ]],
		"columnDefs":[
	{ 
    "targets": 0, //Comma separated values
    "visible": true,
    "searchable": false 
	},
	 ], 
        "serverSide": true,
		 "ajax": {
            "url": "_load_data.php",
            "type": "POST",
			"data": function ( d ) {
                d.type_load = type1;
            }
		},
		"columns": [
			{"data": "0"},
			{"data": "1"},
			{"data": "2"},
			{"data": "3"},
			{"data": "4"},
			{"data": "5"},
			{"data": "6"},
			{"data": "7"},
			{"data": "8"},
		],
		"initComplete": function(settings, json) {
 		$("#loadicon").hide();
 		}
    } );
}
</script>

<?php
		if($type=='TOTAL_CARD')
			{
			?>
			<script>
Load_Data_One('<?php echo $type; ?>');
</script>
			<?php			
			}
			else if($type=='COMPANY_WALLET')
			{
			?>
			 <script>
Load_Data_Two('<?php echo $type; ?>');
</script>
			<?php				
			}
			else
			{
				?>
			<script>
Load_Data_Three('<?php echo $type; ?>');
</script>
			<?php	
			}
			?>
