<?php
include "../connect.php";

$type = escapeString($conn,$_POST['type_load']);
$date = date("Y-m-d");

if($type=='TOTAL_CARD')
{
	$sql ="SELECT id,veh_no,card_kit_id,IF(card_assigned=1,'Assigned','NULL') as assigned,IF(card_status=1,'Active','In-Active') as status,
	assigned_to FROM dairy.happay_card_inventory";
}
else if($type=='COMPANY_WALLET')
{
	$sql ="SELECT * FROM dairy.happay_webhook_load_wallet WHERE date(txn_date)='$date'";
}
else
{
	if($type=='ATM_EXP'){
		$type2="AND trans_type='ATM Expenses'";
	}
	else if($type=='FEE_EXP'){
		$type2="AND trans_type='Fee Expenses'";
	}
	else if($type=='CARD_TRANS'){
		$type2="AND trans_type='Card Transaction'";
	}
	else if($type=='REVERSAL'){
		$type2="AND trans_type IN('ATM Reversal','FEE Reversal')";
	}
	else if($type=='TODAY_TRANS'){
		$type2="";
	}
	else if($type=='WALLET_LOAD'){
		$type2="AND trans_type='Wallet Load Credit'";
	}
	else if($type=='WALLET_WDL'){
		$type2="AND trans_type='Wallet Withdraw Debit'";
	}
	
	$sql ="SELECT id,card_no,merchant,txn_id,trans_id,trans_date,trans_type,IF(credit>0,credit,debit) as amount,date FROM dairy.happay_webhook_live WHERE 
	date='$date' $type2";
}
			
$table = "(
    ".$sql."
) temp";
  
  
$primaryKey = 'id';
 
if($type=='TOTAL_CARD')
{
	$columns = array(
    array( 'db' => $primaryKey, 'dt' => 0),
    array( 'db' => 'veh_no', 'dt' => 1),
    array( 'db' => 'card_kit_id', 'dt' => 2),
    array( 'db' => 'assigned', 'dt' => 3),
    array( 'db' => 'status', 'dt' => 4),
    array( 'db' => 'assigned_to', 'dt' => 5),
);
}
else if($type=='COMPANY_WALLET')
{
	$columns = array(
    array( 'db' => $primaryKey, 'dt' => 0),
    array( 'db' => 'merchant', 'dt' => 1),
    array( 'db' => 'txn_id', 'dt' => 2),
    array( 'db' => 'txn_type', 'dt' => 3),
    array( 'db' => 'currency_amount', 'dt' => 4),
    array( 'db' => 'txn_date', 'dt' => 5),
);
}
else
{
	$columns = array(
    array( 'db' => $primaryKey, 'dt' => 0),
    array( 'db' => 'card_no', 'dt' => 1),
    array( 'db' => 'merchant', 'dt' => 2),
    array( 'db' => 'txn_id', 'dt' => 3),
    array( 'db' => 'trans_id', 'dt' => 4),
    array( 'db' => 'trans_date', 'dt' => 5),
    array( 'db' => 'trans_type', 'dt' => 6),
    array( 'db' => 'amount', 'dt' => 7),
    array( 'db' => 'date', 'dt' => 8),
);
}


 
 $sql_details = array(
    'user' => $username,
    'pass' => $password,
    'db'   => $db_name,
    'host' => $host
);
 
require( '../../b5aY6EZzK52NA8F/scripts/ssp.class.php' );
 
echo json_encode(
    SSP::simple( $_POST, $sql_details, $table, $primaryKey, $columns )
);

?>