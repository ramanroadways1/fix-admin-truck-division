<?php
require_once("connect.php");

$date=date("Y-m-d");
$timestamp=date("Y-m-d H:i:s");

$id = escapeString($conn,($_POST['id']));
$value = escapeString($conn,($_POST['value']));

// active_trip_btn
// block_trip_btn

StartCommit($conn);
$flag = true;

$update_status = Qry($conn,"UPDATE dairy.trip SET active_trip='$value' WHERE id='$id'");

if(!$update_status){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}	

if($value=='1')
{
	echo "<script>
		$('#active_trip_btn').attr('disabled',true);
		$('#block_trip_btn').attr('disabled',false);
	</script>"; 
}
else
{
	echo "<script>
		$('#active_trip_btn').attr('disabled',false);
		$('#block_trip_btn').attr('disabled',true);
	</script>";
}

if($flag)
{
	MySQLCommit($conn);
	closeConnection($conn);
	
	echo "<script>
		$('#loadicon').fadeOut('slow');
	</script>"; 
	exit();
}
else
{
	MySQLRollBack($conn);
	closeConnection($conn);
	AlertError("Error while processing request !");
	exit();
}	
?>