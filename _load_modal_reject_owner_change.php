<?php
require_once("connect.php");

$id = escapeString($conn,($_POST['id']));

if(empty($id))
{
	AlertRightCornerError("ID not found !");
	exit();
}

$get_request = Qry($conn,"SELECT tno FROM owner_change_req WHERE id='$id'");

if(!$get_request){
	AlertRightCornerError("Error while processing request !");
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	exit();
}
	
if(numRows($get_request)==0)
{
	AlertRightCornerError("Request not found !");
	exit();
}

$row = fetchArray($get_request);

$tno = $row['tno'];
$branch = $row['branch'];
?>
<button id="modal_reject_open_btn" style="display:none" data-toggle="modal" data-target="#ModalRejectOwnerChange"></button>

<div class="modal fade" id="ModalRejectOwnerChange" role="dialog" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
		<div class="modal-content" style="">
			<div class="modal-header bg-primary">
				<span style="font-size:13px">Reject Owner Change : <?php echo $tno; ?> </span>
			</div>
		<div class="modal-body">
		
	<div class="row">
		
		<div class="form-group pay_options col-md-12">
			<label>Reject Narration <font color="red">*</font></label>
			<textarea id="reject_narration" name="reject_narration" class="form-control"></textarea>
		</div>
		
		<div class="form-group col-md-12" id="result_modal_22"></div>
		
		</div>
        </div>

	 <div class="modal-footer">
          <button type="button" onclick="ConfirmReject()" id="button_reject_modal" class="pull-left btn btn-sm btn-danger">Confirm, Reject</button>
          <button type="button" id="close_btn_req_reject_modal" class="btn btn-sm btn-primary" data-dismiss="modal">Close</button>
	 </div>
	 
      </div>
    </div>
  </div>
 
<script>
function ConfirmReject()
{
	var narration = $('#reject_narration').val();
	var id = '<?php echo $id; ?>';
	var branch = '<?php echo $branch; ?>';
	
	if(narration=='')
	{
		Swal.fire({icon: 'warning',html: '<font size=\'2\' color=\'black\'>Enter reject narration first !</font>',});
	}
	else
	{
		$('#loadicon').show();
		$('#button_reject_modal').attr('disabled',true);
			jQuery.ajax({
			url: "owner_change_request_reject.php",
			data: 'id=' + id + '&narration=' + narration + '&branch=' + branch,
			type: "POST",
			success: function(data) {
				$("#result_modal_22").html(data);
			},
			error: function() {}
		});
	}
}
	
$('#modal_reject_open_btn')[0].click();	
$('#loadicon').fadeOut('slow');	
</script> 
