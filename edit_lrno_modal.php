<?php
require_once("connect.php");

$id = escapeString($conn,$_POST['id']); 

if(isset($_POST['lr_type']))
{
	$lr_type = 'OWN';
}
else
{
	$lr_type = 'MARKET';
}

$get_data = Qry($conn,"SELECT frno,branch,lrno,brk_id,con1_id,done,trip_id,market_pod_date,pod_rcv_date_own FROM freight_form_lr WHERE id='$id'");

if(!$get_data)
{
	AlertErrorTopRight("Error while processing request !");
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	exit();
}

if(numRows($get_data) == 0)
{
	AlertErrorTopRight("No record found !");
	exit();
}

$row = fetchArray($get_data);

if($row['brk_id']!=0)
{
	AlertErrorTopRight("Breaking LRs not allowed !");
	exit();
}

$lrno = $row['lrno'];
$brk_id = $row['brk_id'];
$con1_id = $row['con1_id'];
$lr_branch = $row['branch'];

$check_lr_record = Qry($conn,"SELECT id FROM freight_form_lr WHERE lrno='$lrno' AND id!='$id'");

if(!$check_lr_record)
{
	AlertErrorTopRight("Error while processing request !");
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	exit();
}

if(numRows($check_lr_record) > 0)
{
	AlertErrorTopRight("Crossing LRs not allowed !");
	exit();
}

$chk_if_coal_branch = Qry($conn,"SELECT z FROM user WHERE username='$lr_branch'");

if(!$chk_if_coal_branch)
{
	AlertErrorTopRight("Error while processing request !");
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	exit();
}

if(numRows($chk_if_coal_branch) == 0)
{
	AlertErrorTopRight("Branch not found !");
	exit();
}

$row_branch = fetchArray($chk_if_coal_branch);

if($row_branch['z']=="1")
{
	AlertErrorTopRight("Coal branch LRs not allowed to updated LR No. !");
	exit();
}
?>

<button id="modal_open_button" style="display:none" data-toggle="modal" data-target="#LRNoEditModal"></button>

<form id="FromLRnoChange" autocomplete="off" style="font-size:12px !important">   
<div class="modal fade" id="LRNoEditModal" role="dialog" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
      <div class="modal-content" style="">
	  
	  <div class="modal-header bg-primary">
			Update LR No : <?php echo $lrno; ?>
		  </div>
		  
		<div class="modal-body">
		<div class="row">
			
			<div class="form-group col-md-12">
				<label>LR No. <sup><font color="red">*</font></sup></label>
				<input style="font-size:12px !important" type="text" name="lrno_old" value="<?php echo $lrno; ?>" class="form-control" readonly required />
			</div>
			
			<div class="form-group col-md-12">
				<label>New LR No. <sup><font color="red">*</font></sup></label>
				<input oninput="this.value=this.value.replace(/[^a-zA-Z0-9]/,'')" type="text" style="font-size:12px !important;text-transform:uppercase" name="lrno_new" class="form-control" name="narration" required />
			</div>
		</div>
        </div>
		
		<input type="hidden" name="con1_id" value="<?php echo $con1_id; ?>">
		<input type="hidden" name="ediary_trip_done" value="<?php echo $row['done']; ?>">
		<input type="hidden" name="ediary_trip_id" value="<?php echo $row['trip_id']; ?>">
		<input type="hidden" name="market_veh_pod" value="<?php echo $row['market_pod_date']; ?>">
		<input type="hidden" name="own_veh_pod" value="<?php echo $row['pod_rcv_date_own']; ?>">
		<input type="hidden" name="vou_no" value="<?php echo $row['frno']; ?>">
		<input type="hidden" name="is_coal_branch" value="<?php echo $row_branch['z']; ?>">
		<input type="hidden" name="id" value="<?php echo $id; ?>">
		<input type="hidden" name="lr_type" value="<?php echo $lr_type; ?>">
		
		<div class="modal-footer">
          <button type="submit" id="update_btn_modal" class="btn btn-sm btn-danger">Update</button>
          <button type="button" class="btn-sm btn btn-primary" id="close_modal_btn" data-dismiss="modal">Close</button>
		</div>
      </div>
      </form>
    </div>
  </div>
  
<script> 
$('#modal_open_button')[0].click();
$('#loadicon').fadeOut('slow');
</script> 

<script type="text/javascript">
$(document).ready(function (e) {
$("#FromLRnoChange").on('submit',(function(e) {
$("#loadicon").show();
$("#update_btn_modal").attr("disabled", true);
e.preventDefault();
	$.ajax({
	url: "./save_lrno_update.php",
	type: "POST",
	data:  new FormData(this),
	contentType: false,
	cache: false,
	processData:false,
	success: function(data){
		$("#func_result").html(data);
	},
	error: function() 
	{} });}));});
</script>