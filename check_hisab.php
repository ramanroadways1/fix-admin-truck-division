<?php
include("header.php");

$chk = Qry($conn,"SELECT id FROM _access_control WHERE username='$_SESSION[ediary_fix_admin]' AND func_id=(SELECT id FROM 
_access_control_func_list WHERE session_role='8' AND func_name='HISAB_Check') AND u_view='1'");
			  
if(numRows($chk)==0)
{
	echo "<script>window.location.href='./';</script>";
	exit();
}
?>
<div class="content-wrapper">
      <section class="content-header">
          <h1 style="font-size:16px;">Check/Approve Hisab </h1>
       </section>
       
	   <section class="content">
          <div class="row">
            <div class="col-xs-12">
			<div class="box">
                <div class="box-body">
				
				<div class="col-md-12">&nbsp;</div>
				
				<div class="col-md-12">
					<div class="row">
						
						<div class="form-group col-md-3">
							<label style="font-size:12px !important">Get Approved Hisab <font color="red"><sup>*</sup></font></label>
							<select style="font-size:12px !important" name="get_approved" id="get_approved" class="form-control" required>
								<option style="font-size:12px !important" value="YES">YES</option>
								<option selected style="font-size:12px !important" value="NO">NO</option>
							</select>
						</div>
						
						<div class="form-group col-md-3">
							<label style="font-size:12px !important">Hisab Date <font color="red"><sup>*</sup></font></label>
							<input id="hisab_date" value="<?php echo date("Y-m-d") ?>" name="hisab_date" type="date" class="form-control" max="<?php echo date("Y-m-d"); ?>" required pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}" />
						</div>
						
						<div class="form-group col-md-2">
							<?php if(!isMobile()) { echo "<label>&nbsp;</label><br />"; } ?>
							<button type="button" style="margin-top:1px;font-size:12px !important" onclick="Search2()" class="btn btn-sm btn-success <?php if(isMobile()) { echo "btn-block"; } ?>" id="add_btn"><i class="fa fa-search" aria-hidden="true"></i> &nbsp; Search</button>
						</div>
					</div>
				</div>
				
				<div class="col-md-12">&nbsp;</div>
				
				<div class="col-md-12 table-responsive" id="load_table">
              
				 </div> 
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

<div id="func_result"></div>  
<div id="modal_res1"></div>  

<script>	
function Search(get_approved,hisab_date)
{
	if(get_approved=='' || hisab_date=='')
	{
		Swal.fire({icon: 'warning',html: '<font size=\'2\' color=\'black\'>All fields are required !</font>',});
	}
	else
	{
		$('#loadicon').show();
			jQuery.ajax({
				url: "_load_check_hisab.php",
				data: 'get_approved=' + get_approved + '&hisab_date=' + hisab_date,
				type: "POST",
				success: function(data) {
					$("#load_table").html(data);
					$('#example').DataTable({ 
					"lengthMenu": [ [50, 100, 500, -1], [50, 100,500, "All"] ], 
						"destroy": true, //use for reinitialize datatable
					});
				},
				error: function() {}
		});
	}
}

function Search2()
{
	var get_approved = $('#get_approved').val();
	var hisab_date = $('#hisab_date').val();
	
	if(get_approved=='' || hisab_date=='')
	{
		Swal.fire({icon: 'warning',html: '<font size=\'2\' color=\'black\'>All fields are required !</font>',});
	}
	else
	{
		$('#loadicon').show();
			jQuery.ajax({
				url: "_load_check_hisab.php",
				data: 'get_approved=' + get_approved + '&hisab_date=' + hisab_date,
				type: "POST",
				success: function(data) {
					$("#load_table").html(data);
					$('#example').DataTable({ 
					"lengthMenu": [ [50, 100, 500, -1], [50, 100,500, "All"] ], 
						"destroy": true, //use for reinitialize datatable
					});
				},
				error: function() {}
		});
	}
}

function ApproveHisab(id)
{
	$('#approve_btn_'+id).attr('disabled',true);
	
	jQuery.ajax({
		url: "approve_hisab_check_hisab.php",
		data: 'id=' + id,
		type: "POST",
		success: function(data) {
			$("#func_result").html(data);
		},
		error: function() {}
	});
}

function ViewHisab(id)
{
	$('#hisab_id').val(id);
	$('#HisabViewForm')[0].submit();
}

Search('NO','<?php echo date("Y-m-d"); ?>');
</script>

<?php include("footer.php") ?>

<form action="_hisab_summary_view_hisab.php" id="HisabViewForm" method="POST" target="_blank">
	<input type="hidden" id="hisab_id" name="hisab_id">
</form>