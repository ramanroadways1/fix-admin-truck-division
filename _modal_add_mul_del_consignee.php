<button type="button" style="display:none" id="mul_del_consignee_modal_btn" data-toggle="modal" data-target="#MulDelConsigneeModal"></button>

<script type="text/javascript">
$(document).ready(function (e) {
$("#ConsigneeForm").on('submit',(function(e) {
$("#save_btn_fix_add_consignee").attr("disabled", true);
$("#loadicon").show();
e.preventDefault();
	$.ajax({
	url: "./save_add_consignee_in_fix_rule.php",
	type: "POST",
	data:  new FormData(this),
	contentType: false,
	cache: false,
	processData:false,
	success: function(data){
		$("#modal_consignee_result").html(data);
	},
	error: function() 
	{} });}));});
</script>


<form id="ConsigneeForm" autocomplete="off">	

<div class="modal fade" id="MulDelConsigneeModal" style="background:#DDD" role="dialog" data-backdrop="static" data-keyboard="false">
   <div class="modal-dialog">
      <div class="modal-content" style="">
	  
	<div class="modal-header bg-primary">
		Add Consignee :
    </div>
	  
	<div class="modal-body">

		<div class="row">
		
			<div class="form-group col-md-12">
				<label>Select Consignee <sup><font color="red">*</font></sup> </label>
				<input style="font-size:12px !important" type="text" oninput="this.value=this.value.replace(/[^a-z A-Z0-9]/,'')" id="con2_multi_consignee" name="con2" class="form-control" required />
			</div>
			
			<input type="hidden" id="con2_multi_consignee_id" name="con2_id">
			<input type="hidden" id="fix_rule_id_add_con2_modal" name="rule_id">
			<input type="hidden" id="fix_lane_id_add_con2_modal" name="lane_id">
			
		</div>
	
		<div id="modal_consignee_result"></div>
		
        </div> 
        <div class="modal-footer">
			<button type="submit" id="save_btn_fix_add_consignee" class="btn btn-primary btn-sm">Save</button>
			<button type="button" class="btn btn-danger btn-sm" id="hide_modal_btn_con2_add" data-dismiss="modal">Close</button>
		</div>
  </div>
 </div>
</div>
</form>