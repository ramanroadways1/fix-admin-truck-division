<?php
require_once("connect.php");

$timestamp = date("Y-m-d H:i:s");
$id = escapeString($conn,($_POST['id']));
$lr_date = escapeString($conn,($_POST['lr_date']));
$lr_no = escapeString($conn,($_POST['lr_no']));
$from = escapeString($conn,($_POST['from']));
$to = escapeString($conn,($_POST['to']));
$from_id = escapeString($conn,($_POST['from_id']));
$to_id = escapeString($conn,($_POST['to_id']));
$broker = escapeString($conn,($_POST['broker']));
$broker_pan = escapeString($conn,($_POST['broker_pan']));
$broker_id = escapeString($conn,($_POST['broker_id']));
$billing_party_id = escapeString($conn,($_POST['billing_party_id']));
$billing_party = escapeString($conn,($_POST['billing_party']));
$billing_party_pan = escapeString($conn,($_POST['billing_party_pan']));
$lr_by = escapeString($conn,($_POST['lr_by']));
$billing_type = escapeString($conn,($_POST['billing_type']));
$billing_type2 = escapeString($conn,($_POST['billing_type2']));
$act_weight = escapeString($conn,($_POST['act_weight']));
$charge_weight = escapeString($conn,($_POST['charge_weight']));
$rate = escapeString($conn,($_POST['rate_pmt']));
$freight = escapeString($conn,($_POST['freight_bilty']));
$veh_placer = escapeString($conn,($_POST['veh_placer']));

$get_data = Qry($conn,"SELECT lrdate,bilty_no,lr_by,billing_type,tno,veh_placer,plr,broker,billing_party,broker_id,bill_party_id,frmstn,tostn,awt,
cwt,rate,tamt,done,trip_id FROM mkt_bilty WHERE id='$id'");

if(!$get_data){
	AlertErrorTopRight("Error while processing request !");
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	exit();
}

if(numRows($get_data) == 0)
{
	AlertErrorTopRight("Market bilty not found !");
	echo "<script>
		$('#edit_modal_btn').attr('disabled',false);
		$('#loadicon').fadeOut('slow');
	</script>";
	exit();
}

$row = fetchArray($get_data);

if($freight != round($charge_weight * $rate))
{
	AlertErrorTopRight("Please check rate and freight !");
	echo "<script>
		$('#edit_modal_btn').attr('disabled',false);
		$('#loadicon').fadeOut('slow');
	</script>";
	exit();
}

$update_log = array();
$update_Qry = array();

if($lr_date!=$row['lrdate'])
{
	$update_log[]="LRDate : $row[lrdate] to $lr_date";
	$update_Qry[]="lrdate='$lr_date'";
}

if($lr_no!=$row['plr'])
{
	$update_log[]="Party_LRNo : $row[plr] to $lr_no";
	$update_Qry[]="plr='$lr_no'";
}

if($from!=$row['frmstn'])
{
	$update_log[]="From : $row[frmstn] to $from";
	$update_Qry[]="frmstn='$from'";
	$update_Qry[]="from_id='$from_id'";
}

if($to!=$row['tostn'])
{
	$update_log[]="To : $row[tostn] to $to";
	$update_Qry[]="tostn='$to'";
	$update_Qry[]="to_id='$to_id'";
}

if($broker!=$row['broker'])
{
	$update_log[]="Broker : $row[broker] to $broker";
	$update_Qry[]="broker='$broker'";
	$update_Qry[]="broker_id='$broker_id'";
}

if($billing_party!=$row['billing_party'])
{
	$update_log[]="Billing_Party : $row[billing_party] to $billing_party";
	$update_Qry[]="billing_party='$billing_party'";
	$update_Qry[]="bill_party_id='$billing_party_id'";
}

if($lr_by!=$row['lr_by'])
{
	$update_log[]="LR_By : $row[lr_by] to $lr_by";
	$update_Qry[]="lr_by='$lr_by'";
}

if($billing_type!=$row['billing_type'])
{
	$update_log[]="Billing_Type : $row[billing_type] to $billing_type";
	$update_Qry[]="billing_type='$billing_type'";
}

$act_weight_diff = 0;
$charge_weight_diff = 0;

if($act_weight!=$row['awt'])
{
	$update_log[]="Act_Wt : $row[awt] to $act_weight";
	$update_Qry[]="awt='$act_weight'";
	
	$act_weight_diff = $act_weight - $row['awt'];
}

if($charge_weight!=$row['cwt'])
{
	$update_log[]="Chrg_Wt : $row[cwt] to $charge_weight";
	$update_Qry[]="cwt='$charge_weight'";
	
	$charge_weight_diff = $charge_weight - $row['cwt'];
}

if($rate!=$row['rate'])
{
	$update_log[]="Rate : $row[rate] to $rate";
	$update_Qry[]="rate='$rate'";
}

if($freight!=$row['tamt'])
{
	$update_log[]="Freight : $row[tamt] to $freight";
	$update_Qry[]="tamt='$freight'";
}

if($veh_placer!=$row['veh_placer'])
{
	$update_log[]="Veh_Placer : $row[veh_placer] to $veh_placer";
	$update_Qry[]="veh_placer='$veh_placer'";
}

$update_log = implode(', ',$update_log); 
$update_Qry = implode(', ',$update_Qry); 

if($update_log=="")
{
	AlertError("Nothing to update !");
	echo "<script>$('#edit_modal_btn').attr('disabled',false);</script>";
	exit();
}

if($row['trip_id']!=0)
{
	$check_fix_lane = Qry($conn,"SELECT id FROM dairy.trip WHERE id='$row[trip_id]' AND fix_lane!=0");

	if(!$check_fix_lane){
		AlertError("Error while processing request !");
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		echo "<script>$('#edit_modal_btn').attr('disabled',false);</script>";
		exit();
	}
	
	if(numRows($check_fix_lane) > 0)
	{
		AlertError("Unable to edit. Trip belongs to fix lane !");
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		echo "<script>$('#edit_modal_btn').attr('disabled',false);</script>";
		exit();
	}
}

StartCommit($conn);
$flag = true;
$error_msg = array();

if($row['tamt'] != $freight)
{
	$get_bilty_book = Qry($conn,"SELECT id FROM dairy.bilty_book WHERE bilty_no='$row[bilty_no]' ORDER BY id ASC LIMIT 1");

	if(!$get_bilty_book){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}

	if(numRows($get_bilty_book) == 0)
	{
		$flag = false;
		errorLog("Bilty book record not found. Bilty_no: $row[bilty_no].",$conn,$page_name,__LINE__);
	}

	$row_bilty_book = fetchArray($get_bilty_book);

	$freight_diff = $freight - $row['tamt'];

	$update_freight = Qry($conn,"UPDATE dairy.bilty_book SET credit='$freight',balance='$freight' WHERE id='$row_bilty_book[id]'");

	if(!$update_freight){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}

	$update_freight_nxt = Qry($conn,"UPDATE dairy.bilty_book SET balance = balance+('$freight_diff') WHERE id>'$row_bilty_book[id]' 
	AND bilty_no='$row[bilty_no]'");

	if(!$update_freight_nxt){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}

	$update_balance = Qry($conn,"UPDATE dairy.bilty_balance SET freight='$freight',balance = balance+('$freight_diff') 
	WHERE bilty_no='$row[bilty_no]'");

	if(!$update_balance){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}

	if($row['billing_type']=='TO_PAY' AND $row['trip_id']!=0)
	{
		$update_trip_1 = Qry($conn,"UPDATE dairy.trip SET mb_amount='$freight' WHERE id='$row[trip_id]'");

		if(!$update_trip_1){
			$flag = false;
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		}
		
		if(AffectedRows($conn) == 0)
		{
			$update_trip_2 = Qry($conn,"UPDATE dairy.trip_final SET mb_amount='$freight' WHERE trip_id='$row[trip_id]'");

			if(!$update_trip_2){
				$flag = false;
				errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			}
		}
	}
}

if($row['billing_type'] != $billing_type)
{
	if($row['billing_type']=='TO_PAY' AND $row['trip_id']!=0)
	{
		$update_trip_1 = Qry($conn,"UPDATE dairy.trip SET mb_to_pay='0',mb_amount='0' WHERE id='$row[trip_id]'");

		if(!$update_trip_1){
			$flag = false;
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		}
	}

	if($billing_type=='TO_PAY' AND $row['trip_id']!=0)
	{
		$update_trip_1 = Qry($conn,"UPDATE dairy.trip SET mb_to_pay='1',mb_amount='$freight' WHERE id='$row[trip_id]'");

		if(!$update_trip_1){
			$flag = false;
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		}
	}

	$update_bilty_book_1 = Qry($conn,"UPDATE dairy.bilty_book SET type='$billing_type' WHERE bilty_no='$row[bilty_no]'");

	if(!$update_bilty_book_1){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}

	$update_bilty_bal_1 = Qry($conn,"UPDATE dairy.bilty_balance SET type='$billing_type' WHERE bilty_no='$row[bilty_no]'");

	if(!$update_bilty_bal_1){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}

	$update_mkt_bilty_1 = Qry($conn,"UPDATE mkt_bilty SET billing_type='$billing_type' WHERE id='$id'");

	if(!$update_mkt_bilty_1){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
}

if($row['frmstn'] != $from)
{
	if($row['done']!=0)
	{
		$get_to_last_trip = Qry($conn,"SELECT to_station FROM dairy.trip WHERE tno='$row[tno]' AND id<'$row[trip_id]' ORDER BY id DESC LIMIT 1");

		if(!$get_from_loc_last_trip){
			$flag = false;
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		}
		
		if(numRows($get_from_loc_last_trip) == 0)
		{
			$get_to_last_trip1 = Qry($conn,"SELECT to_station FROM dairy.trip_final WHERE tno='$row[tno]' ORDER BY id DESC LIMIT 1");

			if(!$get_to_last_trip1){
				$flag = false;
				errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			}
			
			$row_last_to_stn = fetchArray($get_to_last_trip1);
			$last_to_loc = $row_last_to_stn['to_station'];
		}
		else
		{
			$row_last_to_stn = fetchArray($get_from_loc_last_trip);
			$last_to_loc = $row_last_to_stn['to_station'];
		}
		
		if($last_to_loc != $from)
		{
			$flag = false;
			$error_msg[]="<font color='red'>Last trip's to location: $last_to_loc not matching with LR's from location: $from</font>";
		}
	}
}

if($row['tostn'] != $to)
{
	if($row['done']!=0)
	{
		$get_to_next_trip = Qry($conn,"SELECT from_station FROM dairy.trip WHERE tno='$row[tno]' AND id>'$row[trip_id]' ORDER BY id ASC LIMIT 1");

		if(!$get_to_next_trip){
			$flag = false;
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		}
		
		if(numRows($get_to_next_trip)>0)
		{
			$row_next_from_stn = fetchArray($get_to_next_trip);
			$next_from_loc = $row_next_from_stn['from_station'];
		
			if($next_from_loc != $to)
			{
				$flag = false;
				$error_msg[]="<font color='red'>Next trip's from location: $next_from_loc not matching with LR's to location: $to</font>";
			}
		}
	}
}

if($row['done']=="1")
{
	if($from!=$row['frmstn'] || $to!=$row['tostn'])
	{
		$get_from_lat_long = Qry($conn,"SELECT _lat,_long,pincode FROM station WHERE id='$from_id'");
			
		if(!$get_from_lat_long){
			$flag = false;
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		}
		
		$get_to_lat_long = Qry($conn,"SELECT _lat,_long,pincode FROM station WHERE id='$to_id'");
			
		if(!$get_to_lat_long){
			$flag = false;
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		}
			
		$row_from_lat_lng = fetchArray($get_from_lat_long);
		$row_to_lat_lng = fetchArray($get_to_lat_long);
			
		$origin = trim($row_from_lat_lng['_lat'].",".$row_from_lat_lng['_long']);
		$destination = trim($row_to_lat_lng['_lat'].",".$row_to_lat_lng['_long']);
		$from_pincode = $row_from_lat_lng['pincode'];
		$to_pincode = $row_to_lat_lng['pincode'];
		
		$fetch_km = Qry($conn,"SELECT distance FROM master_addr_book WHERE from_id='$from_id' AND to_id='$to_id'");
		
		if(!$fetch_km){
			$flag = false;
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		}
		
		if(numRows($fetch_km)==0)
		{
			$url="https://maps.googleapis.com/maps/api/distancematrix/json?units=imperial&origins=".urlencode($origin)."&destinations=".urlencode($destination)."&key=AIzaSyCZ6JUFGB8r5TpRYGnJjNxUH1NZdxaPoGw";

			$api = file_get_contents($url);
			$data = json_decode($api);
						
			$api_status = $data->rows[0]->elements[0]->status;
				
			if($api_status=='NOT_FOUND')
			{
				$flag = false;
				$error_msg[]="distance not found !";
			}

			if($api_status!='OK')
			{
				$flag = false;
				$error_msg[]="API Error: $api_status !";
			}
						
			$dest_addr = $data->destination_addresses[0];
			$origin_addr = $data->origin_addresses[0];
			$distance = round((int)$data->rows[0]->elements[0]->distance->value / 1000);
			$travel_time = $data->rows[0]->elements[0]->duration->text;
			$travel_time_value = $data->rows[0]->elements[0]->duration->value;
			$travel_hrs = gmdate("H", $travel_time_value);
			$travel_minutes = gmdate("i", $travel_time_value);
			$travel_seconds = gmdate("s", $travel_time_value);
			
			$insert_distance = Qry($conn,"INSERT INTO master_addr_book(from_id,to_id,distance,branch,branch_user,timestamp) VALUES ('$from_id',
			'$to_id','$distance','EDIARY_ADMIN','$_SESSION[ediary_fix_admin]','$timestamp')");
			
			if(!$insert_distance){
				$flag = false;
				errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			}
			
			$new_km = $distance;
		}
		else
		{
			$row_km_for_next = fetchArray($fetch_km);
			$new_km = $row_km_for_next['distance'];
		}
		
		$update_trip_main = Qry($conn,"UPDATE dairy.trip SET from_station='$from',from_id='$from_id',from_poi='$origin',from_pincode='$from_pincode',
		to_station='$to',to_id='$to_id',to_poi='$destination',to_pincode='$to_pincode',pincode='$to_pincode',km='$new_km',
		narration=CONCAT(narration,'Trip_Edited') WHERE id='$row[trip_id]'");
		
		if(!$update_trip_main){
			$flag = false;
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		}
	}
	
	
	$update_trip_main = Qry($conn,"UPDATE dairy.trip SET act_wt=act_wt+('$act_weight_diff'),charge_wt=charge_wt+('$charge_weight_diff'),
	narration=CONCAT(narration,'Trip_Edited') WHERE id='$row[trip_id]'");
		
	if(!$update_trip_main){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
}

$update_bilty_main = Qry($conn,"UPDATE mkt_bilty SET $update_Qry WHERE id='$id'");

if(!$update_bilty_main){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

$insertLog = Qry($conn,"INSERT INTO dairy.ediary_admin_log(code,tno,action,desct,branch,username,timestamp) VALUES ('$id','$row[bilty_no]',
'Market_Bilty_Edit','$update_log','Fix_Admin','$_SESSION[ediary_fix_admin]','$timestamp')");

if(!$insertLog){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}
		
if($flag)
{ 
	MySQLCommit($conn);
	closeConnection($conn);	
	AlertRightCornerSuccess("Updated Successfully.");
	echo "<script>
		$('#edit_modal_btn').attr('disabled',false);
		$('#modal_edit_mb_close_btn')[0].click();
		$('#add_btn')[0].click(); 
	</script>";
	exit();
}
else
{
	MySQLRollBack($conn);
	closeConnection($conn);
	
	if(!empty($error_msg))
	{
		echo implode('<br>',$error_msg); 
	}
	
	AlertError("Error while processing request..");
	echo "<script>$('#edit_modal_btn').attr('disabled',false);</script>";
	exit();
}
?>