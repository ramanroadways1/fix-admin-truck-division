<?php
require_once 'connect.php';

$sql = Qry($conn,"SELECT t.tno,t.wheeler,t.diesel_tank_cap,t.trishul_card,t.happay_active,t.lock_ag,t.lock_tel,t.comp,t.model,t.cng,s.title,d.name,
t.driver_code,g.odometer,g.ignition,g.speed,g.gps_timestamp 
FROM dairy.own_truck AS t 
LEFT OUTER JOIN dairy.user AS s ON s.id = t.superv_id
LEFT OUTER JOIN dairy.driver AS d ON d.code = t.driver_code
LEFT OUTER JOIN dairy._gps_position AS g ON g.tno = t.tno
WHERE t.is_sold!='1'");

if(!$sql){
	echo getMySQLError($conn);
	errorLog(getMySQLError($conn),$conn,$page_url,__LINE__);
	exit();
}

?>
	<table id="example" class="table table-bordered table-striped" style="font-size:13.5px;">
        <thead>
		<tr>
			<th>#</th>
			<th>Vehicle_no</th>
			<th>Company</th>
			<th>Supervisor</th>
			<th>Model/Wheeler</th>
			<th>Driver</th>
			<th>Last_Active</th>
			<th>Odometer</th>
			<th>Ignition & Speed</th>
		</tr>
		</thead>
       <tbody id=""> 
<?php
if(numRows($sql)==0)
{
	echo "<tr>
		<td colspan='11'>No record found !</td>
	</tr>";  
}
else
{
	$sn=1;
	while($row = fetchArray($sql))
	{
		if($row['driver_code']!=0){
			$driver_name = $row['name'];
		} else {
			$driver_name = "<font color='red'>W/D</font>";
		}
		
		if($row['ignition']!='Off'){
			$ignition = "$row[speed] kmph";
		} else {
			$ignition = "<font color='red'>Off</font>";
		}
		
		if($row['gps_timestamp']!=0){
			$gps_timestamp = date("d-m-y h:i A",strtotime($row['gps_timestamp']));
		} else {
			$gps_timestamp = "<font color='red'>Offline</font>";
		}
		
		echo "<tr>
			<td>$sn</td>
			<td>$row[tno]</td>
			<td>$row[comp]</td>
			<td>$row[title]</td>
			<td>$row[model]<br>$row[wheeler] w</td>
			<td>$driver_name</td>
			<td>$gps_timestamp</td>
			<td>$row[odometer]</td>
			<td>$ignition</td>
		</tr>";
		$sn++;		
     }
}
?>
	</tbody>
		</table>
<script> 
$("#loadicon").fadeOut('slow');
$(document).ready(function() {
    $('#example').DataTable( {
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ]
    } );
} );
</script> 