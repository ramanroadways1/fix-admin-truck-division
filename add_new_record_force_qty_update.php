<?php
require_once 'connect.php';

$date = date("Y-m-d");
$timestamp = date("Y-m-d H:i:s");

$tno = escapeString($conn,strtoupper(trim($_POST['tno'])));

if($tno=='')
{
	AlertErrorTopRight("Enter vehicle number first !");
	echo "<script>$('#add_btn').attr('disabled', false);</script>";
	exit();
}

$check_tno = Qry($conn,"SELECT force_update_diesel_qty FROM dairy.own_truck WHERE tno='$tno'");

if(!$check_tno){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	AlertErrorTopRight("Error while processing request !");
	echo "<script>$('#add_btn').attr('disabled', false);</script>";
	exit();
}

if(numRows($check_tno) == 0)
{
	AlertErrorTopRight("Invalid vehicle number !");
	echo "<script>$('#add_btn').attr('disabled', false);</script>";
	exit();
}

$row = fetchArray($check_tno);

if($row['force_update_diesel_qty']!=0)
{
	AlertErrorTopRight("Duplicate record found !");
	echo "<script>$('#add_btn').attr('disabled', false);</script>";
	exit();
}

StartCommit($conn);
$flag = true;

$update = Qry($conn,"UPDATE dairy.own_truck SET force_update_diesel_qty='1' WHERE tno='$tno'");

if(!$update){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

if($flag)
{
	MySQLCommit($conn);
	closeConnection($conn);
	AlertRightCornerSuccess("Record successfully added !");
	exit();
}
else
{
	MySQLRollBack($conn);
	closeConnection($conn);
	AlertErrorTopRight("Error while processing request !");
	echo "<script>$('#add_btn').attr('disabled', false);</script>";
	exit();
}	
?>