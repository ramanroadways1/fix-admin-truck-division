<?php
require_once 'connect.php';

$date = date("Y-m-d");
$timestamp = date("Y-m-d H:i:s");

$id=escapeString($conn,(trim($_POST['id'])));
$type=escapeString($conn,(trim($_POST['type'])));
$value=escapeString($conn,(trim($_POST['value'])));

if($type=='branch')
{
	$col_name="visible_to_branch";
}
else
{
	$col_name="visible_to_supervisor";
}

StartCommit($conn);
$flag = true;

$update = Qry($conn,"UPDATE dairy.exp_head SET `$col_name`='$value' WHERE id='$id'");

if(!$update){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

if($flag)
{
	MySQLCommit($conn);
	closeConnection($conn);
	echo "<script>LoadTable();</script>";
	exit();
}
else
{
	MySQLRollBack($conn);
	closeConnection($conn);
	AlertErrorTopRight("Error while processing request !");
	exit();
}	
?>