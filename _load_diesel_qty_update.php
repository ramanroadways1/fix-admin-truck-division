<?php
require_once 'connect.php';

$sql = Qry($conn,"SELECT o.tno,o.wheeler,o.comp as company,o.model,o.diesel_tank_cap,o.diesel_left,d.name as driver_name,u.title as supervisor  
FROM dairy.own_truck AS o 
LEFT OUTER JOIN dairy.driver AS d ON d.code = o.driver_code 
LEFT OUTER JOIN dairy.user AS u ON u.id = o.superv_id
WHERE o.diesel_trip_id=0");

if(!$sql){
	echo getMySQLError($conn);
	errorLog(getMySQLError($conn),$conn,$page_url,__LINE__);
	exit();
}

?>
	<table id="example" class="table table-bordered table-striped" style="font-size:13px;">
        <thead>
		<tr>
			<th>#</th>
			<th>Vehicle_no</th>
			<th>Company</th>
			<th>Wheeler</th>
			<th>Spervisor</th>
			<th>Model</th>
			<th>Tank_Cap.</th>
			<th>Diesel_Left</th>
			<th>Driver</th>
		<!--	<th>#DieselQty#</th>-->
		</tr>
		</thead>
       <tbody id=""> 
<?php
if(numRows($sql)==0)
{
	echo "<tr>
		<td colspan='6'>No record found !</td>
	</tr>";  
}
else
{
	$sn=1;
	while($row = fetchArray($sql))
	{
		if($row['driver_name']==''){
			$driver_name = "<font color='red'>W/D</font>";
		} else {
			$driver_name = $row['driver_name'];
		}
		
		// <td>
				// <button disabled type='button' id='update_qty_btn_$row[tno]' onclick=UpdateDieselQty('$row[tno]') class='btn btn-sm btn-success'>Update</button>
			// </td>
		echo "<tr>
			<td>$sn</td>
			<td>$row[tno]</td>
			<td>$row[company]</td>
			<td>$row[model]</td>
			<td>$row[wheeler]</td>
			<td>$row[supervisor]</td>
			<td>$row[diesel_tank_cap]</td>
			<td>$row[diesel_left]</td>
			<td>$driver_name</td>
		</tr>";
		$sn++;		
     }
}
?>
	</tbody>
		</table>
<script> 
// $("#loadicon").fadeOut('slow');
$(document).ready(function() {
    $('#example').DataTable( {
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ]
    } );
} );
</script>
