<?php include("header.php"); ?>

<?php
$chk = Qry($conn,"SELECT id FROM _access_control WHERE username='$_SESSION[ediary_fix_admin]' AND func_id=(SELECT id FROM 
_access_control_func_list WHERE session_role='8' AND func_name='Expenses') AND u_view='1'");
			  
if(numRows($chk)==0)
{
	echo "<script>window.location.href='./';</script>";
	exit();
}
?>

<div class="content-wrapper">
      <section class="content-header">
          <h1 style="font-size:16px;">Manage Expenses : </h1>
       </section>
       
	   <section class="content">
          <div class="row">
            <div class="col-xs-12">
			<div class="box">
                <div class="box-body">
<?php
$chk_insert = Qry($conn,"SELECT id FROM _access_control WHERE username='$_SESSION[ediary_fix_admin]' AND func_id=(SELECT id FROM 
_access_control_func_list WHERE session_role='8' AND func_name='Expenses') AND u_insert='1'");
			  
if(numRows($chk_insert)>0)
{
?>				
				
				<div class="col-md-12">
					<div class="row">
						
						<div class="form-group col-md-3">
							<label>Title <font color="red"><sup>* (visible to branch)</sup></font></label>
							<input placeholder="WEIGHT" autocomplete="off" oninput="this.value=this.value.replace(/[^A-Za-z0-9-@_#.]/,'')" type="text" class="form-control" id="title" />
						</div>
						
						<div class="form-group col-md-3">
							<label>System Name <font color="red"><sup>*</sup></font></label>
							<input autocomplete="off" placeholder="TRUCK WEIGHING CHARGES" oninput="this.value=this.value.replace(/[^A-Za-z0-9-@_#.]/,'')" type="text" class="form-control" id="exp_name" />
						</div>
						
						<div class="form-group col-md-2">
							<label>Exp. Code <font color="red"><sup>*</sup></font></label>
							<input autocomplete="off" type="text" oninput="this.value=this.value.replace(/[^A-Za-z0-9]/,'')" class="form-control" id="exp_code" />
						</div>
						
						<div class="form-group col-md-2">
							<label>LockOn Empty ? <font color="red"><sup>*</sup></font></label>
							<select name="lock_on_empty" id="lock_on_empty" class="form-control" required="required">
								<option value="">--select option--</option>
								<option value="1">Yes</option>
								<option value="0">No</option>
							</select>
						</div>
						
						<div class="form-group col-md-2">
							<?php if(!isMobile()) { echo "<label>&nbsp;</label><br />"; } ?>
							<button type="button" onclick="AddUserFunc()" class="btn btn-sm btn-success <?php if(isMobile()) { echo "btn-block"; } ?>" id="add_btn"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> &nbsp; Add Expense</button>
						</div>
					</div>
				</div>
				
				<?php
				}
				?>
				<div class="col-md-12">&nbsp;</div>
				
				<div class="col-md-12 table-responsive" id="load_table">
              
				 </div> 
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

<div id="func_result"></div>  
<div id="load_modal_div"></div>  
<div id="func_result2"></div>  
 
<script>
function AddUserFunc()
{
	var title = $('#title').val();
	var exp_name = $('#exp_name').val();
	var exp_code = $('#exp_code').val();
	var lock_on_empty = $('#lock_on_empty').val();
	
	if(title=='')
	{
		Swal.fire({icon: 'warning',html: '<font size=\'2\' color=\'black\'>Enter title !</font>',});
	}
	else if(exp_name=='')
	{
		Swal.fire({icon: 'warning',html: '<font size=\'2\' color=\'black\'>Enter expense name !</font>',});
	}
	else if(exp_code=='')
	{
		Swal.fire({icon: 'warning',html: '<font size=\'2\' color=\'black\'>Enter expense code !</font>',});
	}
	else if(lock_on_empty=='')
	{
		Swal.fire({icon: 'warning',html: '<font size=\'2\' color=\'black\'>Select lock on empty option !</font>',});
	}
	else
	{
		$('#add_btn').attr('disabled',true); 
		$('#loadicon').show();
		jQuery.ajax({
			url: "save_add_expense.php",
			data: 'title=' + title + '&exp_name=' + exp_name + '&exp_code=' + exp_code + '&lock_on_empty=' + lock_on_empty,
			type: "POST",
			success: function(data) {
			$("#func_result").html(data);
			},
			error: function() {}
		});
	}
}
</script>
 
<script>	
function LoadTable()
{
	jQuery.ajax({
		url: "_load_expenses.php",
		data: 'id=' + 'ok',
		type: "POST",
		success: function(data) {
			$("#load_table").html(data);
			$('#example').DataTable({ 
				"destroy": true, //use for reinitialize datatable
			});
		},
		error: function() {}
	});
}
LoadTable();

</script>

<?php
$chk_update = Qry($conn,"SELECT id FROM _access_control WHERE username='$_SESSION[ediary_fix_admin]' AND func_id=(SELECT id FROM 
_access_control_func_list WHERE session_role='8' AND func_name='Expenses') AND u_update='1'");
			  
if(numRows($chk_update)>0)
{
?>
<script>
function ModifyExp(id,type)
{
	$('#loadicon').show();
		jQuery.ajax({
			url: "modal_edit_exp_cap_amount.php",
			data: 'id=' + id + '&type=' +  type,
			type: "POST",
			success: function(data) {
			$("#func_result").html(data);
			},
			error: function() {}
		});
}

function UnlockOnEmpty(id)
{
	$('#loadicon').show();
		jQuery.ajax({
			url: "lock_unlock_exp_on_empty.php",
			data: 'id=' + id + '&type=' + '0',
			type: "POST",
			success: function(data) {
			$("#func_result").html(data);
			},
			error: function() {}
		});
}

function LockOnEmpty(id)
{
	$('#loadicon').show();
		jQuery.ajax({
			url: "lock_unlock_exp_on_empty.php",
			data: 'id=' + id + '&type=' + '1',
			type: "POST",
			success: function(data) {
			$("#func_result").html(data);
			},
			error: function() {}
		});
}
</script>

<?php
}
?>

<?php include("footer.php") ?>