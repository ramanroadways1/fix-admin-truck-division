<?php include("header.php"); ?>

<?php
$chk = Qry($conn,"SELECT id FROM _access_control WHERE username='$_SESSION[ediary_fix_admin]' AND func_id=(SELECT id FROM 
_access_control_func_list WHERE session_role='8' AND func_name='Add_Trips') AND u_view='1'");
			  
if(numRows($chk)==0)
{
	echo "<script>window.location.href='./';</script>";
	exit();
}
?>

<script>
$(function() {
		$("#veh_no").autocomplete({
		source: 'autofill/get_tno.php',
		// appendTo: "#LaneRuleModal",
		select: function (event, ui) { 
              $('#veh_no').val(ui.item.value);   
             return false;
		},
		change: function (event, ui) {
		if(!ui.item){
		    $(event.target).val("");
			Swal.fire({icon: 'warning',html: '<font size=\'2\' color=\'black\'>Vehicle does not exists !</font>',});
			$("#veh_no").val('');
			$("#veh_no").focus();
		}}, 
		focus: function (event, ui){
			return false;}
});});
</script>

<div class="content-wrapper">
      <section class="content-header">
          <h1 style="font-size:16px;">Add trip : </h1>
       </section>
       
	   <section class="content">
          <div class="row">
            <div class="col-xs-12">
			<div class="box">
                <div class="box-body">
	<div class="col-md-12">&nbsp;</div>			
<form action="#" method="POST" id="Form1" autocomplete="off">
			
				<div class="col-md-12">
					
					<div class="row">
						
						<div class="form-group col-md-3">
							<label>Trip Type <font color="red"><sup>*</sup></font></label>
							<select onchange="TripType(this.value)" name="trip_type" id="trip_type" class="form-control" required="required">
								<option value="RUNNING">Running Trips</option>
								<option value="OLD">Completed Trips</option>
						   </select>
						</div>
						
						<script>
					   function TripType(elem)
					   {
						   if(elem=='RUNNING')
						   {
							   $('#tno_div').show();
							   $('#veh_no').attr('required',true);
								$('#trip_div').hide();
							   $('#trip_no').attr('required',false);
						   }
						   else
						   {
							   $('#tno_div').hide();
							   $('#veh_no').attr('required',false);
								$('#trip_div').show();
							   $('#trip_no').attr('required',true);
						   }
					   }
					</script>
						
						<div class="form-group col-md-3" id="tno_div">
							<label>Vehicle Number <font color="red"><sup>*</sup></font></label>
							<input autocomplete="off" oninput="this.value=this.value.replace(/[^A-Za-z 0-9]/,'');" type="text" class="form-control" required="required" name="veh_no" id="veh_no" />
						</div>
						
						<div class="form-group col-md-3" id="trip_div" style="display:none">
							<label>Trip Number <font color="red"><sup>*</sup></font></label>
							<input autocomplete="off" name="trip_no" oninput="this.value=this.value.replace(/[^0-9]/,'');" id="trip_no" class="form-control" type="text" />
						</div>
						
						<div class="form-group col-md-2">
							<?php if(!isMobile()) { echo "<label>&nbsp;</label><br />"; } ?>
							<button type="submit" class="btn btn-sm btn-success <?php if(isMobile()) { echo "btn-block"; } ?>" id="search_btn"><i class="fa fa-search" aria-hidden="true"></i> &nbsp; Search</button>
						</div>
					</div>
				</div>
</form>				

				<div class="col-md-12 table-responsive" id="load_table">
				</div> 
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

<div id="func_result"></div>  
<div id="load_modal_div"></div>  
<div id="modal_load_div"></div>  


<script type="text/javascript">
$(document).ready(function (e) {
$("#Form1").on('submit',(function(e) {
$("#loadicon").show();
// $("#search_btn").attr("disabled", true);
e.preventDefault();
	$.ajax({
	url: "./_load_add_trip.php",
	type: "POST",
	data:  new FormData(this),
	contentType: false,
	cache: false,
	processData:false,
	success: function(data){
		$("#load_table").html(data);
		$('#example').DataTable({ 
			"destroy": true, //use for reinitialize datatable
		});
	},
	error: function() 
	{} });}));});
</script>

<script>	
	function Insert(id,tno,trip_type,trip_no)
	{
		$("#trip_id").val('');
		$("#truck_no").val('');
		$("#tno1").val('');
		
		$("#trip_id").val(id);
		$("#truck_no").val(tno);
		$("#tno1").val(tno);
		$("#trip_type_db").val(trip_type);
		$("#trip_no_db").val(trip_no);
		
		$("#loadicon").show();
		jQuery.ajax({
		url: "fetch_prev_trip.php",
		data: 'tno=' + tno + '&id=' + id + '&trip_type=' + trip_type + '&trip_no=' + trip_no,
		type: "POST",
		success: function(data) {
			$("#result_data").html(data);
		},
		error: function() {}
		});
	}
</script>	
	
<script>	
function ChangeOrder(trip_id)
{
	var add_after_trip = $('#trip_list_'+trip_id).val();
		
	if(trip_id=='')
	{
		alert('Trip not found !');
	}
	else if(add_after_trip=='')
	{
		alert('Select trip first !');
	}
	else
	{
		$('#btn_confirm_'+trip_id).attr('disabled',true);
		$("#loadicon").show();
			jQuery.ajax({
			url: "save_manage_trip_order.php",
			data: 'trip_id=' + trip_id + '&add_after_trip=' + add_after_trip,
			type: "POST",
			success: function(data) {
			$("#func_result").html(data);
		},
		error: function() {}
		});
	}
}  
</script>	
	
<script>
function ViewTrip(id)
{
	$('#loadicon').show();
		jQuery.ajax({
			url: "_load_trip_by_id.php",
			data: 'id=' + id,
			type: "POST",
			success: function(data) {
				$("#modal_load_div").html(data);
			},
		error: function() {}
	});
}
</script>

<form action="edit_trip_by_id.php" target="_blank" method="POST" id="TripEditForm">
	<input type="hidden" name="trip_id" id="trip_id_edit_form" />
</form>
			 
<?php include("footer.php") ?>