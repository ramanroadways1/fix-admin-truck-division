<?php
require_once 'connect.php';

$sql = Qry($conn,"SELECT id,tno,trishul_active_from FROM dairy.own_truck WHERE trishul_card='1'");

if(!$sql){
	echo getMySQLError($conn);
	errorLog(getMySQLError($conn),$conn,$page_url,__LINE__);
	exit();
}
	
if(numRows($sql)==0)
{
	AlertRightCornerError("No record found !");
	exit();
}
	?>
	<table id="example" class="table table-bordered table-striped" style="font-size:13px;">
        <thead>
		<tr>
			  <th>#</th>
			  <th>Vehicle Number</th>
			  <th>Active From</th>
			  <th>#Remove#</th>
		</tr>
		</thead>
    <tbody id=""> 
	
<?php
$sn=1;

while($row = fetchArray($sql))
{	
	$trishul_active_from = date('d-m-y h:i A', strtotime($row['trishul_active_from']));
	
	echo "<tr>	
			<td>$sn</td>
			<td>$row[tno]</td>
			<td>$trishul_active_from</td>
			<td><button type='button' id='remove_btn_$row[tno]' disabled onclick=RemoveCard('$row[tno]') class='remove_btn btn btn-xs btn-danger'><span class='fa fa-trash'></span> Remove</button></td>
		</tr>";
$sn++;		
}
	echo "</tbody>
</table>";

$chk1 = Qry($conn,"SELECT id FROM _access_control WHERE username='$_SESSION[ediary_fix_admin]' AND func_id=(SELECT id FROM 
_access_control_func_list WHERE session_role='8' AND func_name='Trishul_Card') AND u_delete='1'");
			  
if(numRows($chk1)==0)
{
	echo "<script>
		$('.remove_btn').attr('disabled',true);
		$('.remove_btn').attr('onclick','');
	</script>";
}
else
{
	echo "<script>
		$('.remove_btn').attr('disabled',false);
	</script>";
}
?>
	
<script> 
	$("#loadicon").fadeOut('slow');
</script> 
