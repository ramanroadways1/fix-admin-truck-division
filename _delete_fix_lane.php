<?php
require_once 'connect.php';

$id = escapeString($conn,$_POST['id']);

$sql = Qry($conn,"SELECT total_rules FROM dairy.fix_lane WHERE id='$id'");

if(!$sql){
	AlertError("Error !");
	errorLog(getMySQLError($conn),$conn,$page_url,__LINE__);
	exit();
}

if(numRows($sql)==0)
{
	AlertError("Lane not found !");
	exit();
}

$row = fetchArray($sql);

if($row['total_rules']!="0")
{
	AlertError("Please delete rules first !");
	echo "<script>$('#btn_dlt_lane_$id').attr('disabled',false);</script>";
	exit();
}

$dlt_lane = Qry($conn,"DELETE FROM dairy.fix_lane WHERE id='$id'");

if(!$dlt_lane){
	AlertError("Error !");
	errorLog(getMySQLError($conn),$conn,$page_url,__LINE__);
	exit();
}

	AlertRightCornerSuccessFadeFast("OK : Lane Deleted !");
	echo "<script>
		$('#lane_row_$id').attr('class','bg-danger');
		$('#lane_row_$id').hide();
		$('#btn_dlt_lane_$id').attr('disabled',true);
		$('#btn_id_$id').attr('disabled',true);
		$('#btn_id_$id').hide();
		$('#rule_view_btn_$id').attr('disabled',true);
		$('#rule_view_btn_$id').hide();
		// $('#loadicon).fadeOut('slow');
	</script>";
	exit();
?>