<?php
require_once 'connect.php';

$date = date("Y-m-d");
$timestamp = date("Y-m-d H:i:s");

$id = escapeString($conn,(trim($_POST['id'])));
$type = escapeString($conn,(trim($_POST['type'])));
$value = escapeString($conn,(trim($_POST['value'])));

if(empty($id)){
	AlertRightCornerError("ID not found !");
	exit();
}

if($type=='Amount'){
	$col_name="amount";
}
else if($type=='Entry'){
	$col_name="entry_limit";
}
else{
	AlertErrorTopRight("Invalid type !");
	exit();
}

StartCommit($conn);
$flag = true;

$update = Qry($conn,"UPDATE dairy.trip_exp_limit SET `$col_name`='$value' WHERE id='$id'");

if(!$update){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

if($flag)
{
	MySQLCommit($conn);
	closeConnection($conn);
	AlertRightCornerSuccess("Updated successfully !");
	echo "<script>LoadTable();$('#button_edit_close_btn')[0].click();</script>";
	exit();
}
else
{
	MySQLRollBack($conn);
	closeConnection($conn);
	AlertErrorTopRight("Error while processing request !");
	exit();
}	
?>