<?php
require_once("connect.php");
?>	
<div class="row">
<div class="col-md-6 table-responsive">
	<table class="table table-bordered table-striped" style="font-size:12px">
      <tr><th colspan="5">For Branch :</th></tr>
        <tr>
            <th>#</th>
            <th>Expense</th>
            <th>Limit_Type</th>
            <th>Amount_Limit</th>
            <th>Entry_Limit</th>
		</tr>
	<?php
	$get_exp = Qry($conn,"SELECT id,exp_name,exp as exp_code,type,amount,entry_limit,timestamp FROM dairy.trip_exp_limit WHERE limit_for='BRANCH'");
	
	if(numRows($get_exp)==0)
	{
		echo "<tr><td colspan='5'>No record found !</td></tr>";
	}
	else
	{
		$i=1;
		while($row = fetchArray($get_exp))
		{
			$timestamp = date("d-m-y h:i A",strtotime($row['timestamp']));
			
			if($row['type']=="1"){
				$type1 = "Amount Limit";
			}
			else{
				$type1 = "Entry Limit";
			}
		
			echo "<tr>
				<td>$i</td>
				<td>$row[exp_name]<br>($row[exp_code])</td>
				<td>$type1</td>
				";
				if($row['type']=="1")
				{
			echo "<td><button type='button' onclick=ModifyExp('$row[id]','branch') class='btn btn-xs btn-primary'><i class='fa fa-pencil-square-o' aria-hidden='true'></i> $row[amount]</button></td>
				<td>$row[entry_limit]</td>";		
				}
				else
				{
			echo "<td>$row[amount]</td>
				<td><button type='button' onclick=ModifyExp('$row[id]','branch') class='btn btn-xs btn-primary'><i class='fa fa-pencil-square-o' aria-hidden='true'></i> $row[entry_limit]</button></td>";	
				}
			// echo "<td>$timestamp</td>
		echo "</tr>";
		$i++;	
		}
	}
	?>	
   </table>
</div>

<div class="col-md-6 table-responsive">
	<table class="table table-bordered table-striped" style="font-size:12px">
        
		<tr><th colspan="5">For Supervisor :</th></tr>
        <tr>
            <th>#</th>
            <th>Expense</th>
            <th>Limit_Type</th>
            <th>Amount_Limit</th>
            <th>Entry_Limit</th>
		</tr>
    <?php
	$get_exp = Qry($conn,"SELECT id,exp_name,exp as exp_code,type,amount,entry_limit,timestamp FROM dairy.trip_exp_limit WHERE limit_for='SUPERVISOR'");
	
	if(numRows($get_exp)==0)
	{
		echo "<tr><td colspan='5'>No record found !</td></tr>";
	}
	else
	{
		$i=1;
		while($row = fetchArray($get_exp))
		{
			$timestamp = date("d-m-y h:i A",strtotime($row['timestamp']));
			
			if($row['type']=="1"){
				$type1 = "Amount Limit";
			}
			else{
				$type1 = "Entry Limit";
			}
		
			echo "<tr>
				<td>$i</td>
				<td>$row[exp_name]<br>($row[exp_code])</td>
				<td>$type1</td>
				";
				if($row['type']=="1")
				{
			echo "<td><button type='button' onclick=ModifyExp('$row[id]','supervisor') class='btn btn-xs btn-primary'><i class='fa fa-pencil-square-o' aria-hidden='true'></i> $row[amount]</button></td>
				<td>$row[entry_limit]</td>";		
				}
				else
				{
			echo "<td>$row[amount]</td>
				<td><button type='button' onclick=ModifyExp('$row[id]','supervisor') class='btn btn-xs btn-primary'><i class='fa fa-pencil-square-o' aria-hidden='true'></i> $row[entry_limit]</button></td>";	
				}
				// echo "<td>$timestamp</td>
			echo "</tr>";
		$i++;	
		}
	}
	?>	
   </table>
</div>
</div>

<script> 
$("#loadicon").fadeOut('slow');
$(document).ready(function() {
    $('#example').DataTable();
} );
</script> 			  				  