<?php
include "header.php";

$chk = Qry($conn,"SELECT id FROM _access_control WHERE username='$_SESSION[ediary_fix_admin]' AND func_id=(SELECT id FROM 
_access_control_func_list WHERE session_role='8' AND func_name='Manage_Own_Truck') AND u_view='1'");
			  
if(numRows($chk)==0)
{
	echo "<script>window.location.href='./';</script>";
	exit();
}
?>

<div id="result"></div>

<script type="text/javascript">
$(document).ready(function (e) {
$("#UpdateSupervisor").on('submit',(function(e) {
$("#loadicon").show();
$("#submit_btn_supervisor").attr("disabled", true);
e.preventDefault();
	$.ajax({
	url: "./save_update_supervisor.php",
	type: "POST",
	data:  new FormData(this),
	contentType: false,
	cache: false,
	processData:false,
	success: function(data){
		$("#result").html(data);
	},
	error: function() 
	{} });}));});
</script>

<script type="text/javascript">
$(document).ready(function (e) {
$("#UpdateModelForm").on('submit',(function(e) {
$("#loadicon").show();
$("#submit_btn_model_update").attr("disabled", true);
e.preventDefault();
	$.ajax({
	url: "./save_update_model.php",
	type: "POST",
	data:  new FormData(this),
	contentType: false,
	cache: false,
	processData:false,
	success: function(data){
		$("#result").html(data);
	},
	error: function() 
	{} });}));});
</script>

<script type="text/javascript">
$(document).ready(function (e) {
$("#Form22").on('submit',(function(e) {
$("#loadicon").show();
$("#submit_btn_salary_update").attr("disabled", true);
e.preventDefault();
	$.ajax({
	url: "./save_update_salary.php",
	type: "POST",
	data:  new FormData(this),
	contentType: false,
	cache: false,
	processData:false,
	success: function(data){
		$("#result").html(data);
	},
	error: function() 
	{} });}));});
</script>

<script>
$(function() {
		$("#truck_no").autocomplete({
		source: 'autofill/get_tno.php',
		// appendTo: '#appenddiv',
		select: function (event, ui) { 
            $('#truck_no').val(ui.item.value);   
            return false;},
		change: function (event, ui) {
		if(!ui.item){
			$(event.target).val("");
            $(event.target).focus();
			$('#truck_no').val("");   
			alert('Vehicle does not exists.');
		}}, 
	focus: function (event, ui){
	return false;
	}
});});
</script>

<style>
label{
	font-size:12px;
}
</style>

<div class="content-wrapper">
    <section class="content-header">
      <span style="font-size:15px">List ALL Vehicles : </span>
	</section>
		
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box"> 
			<div class="box-body">

<div class="row">

<div class="form-group col-md-12">
   <div class="form-group col-md-12 table-responsive" style="overflow:auto" id="load_table_div"></div>
</div>

  </div>
</div>

</body>
</html>

          </div>
        </div>
	</div>
</section>
</div>    
	
<script type="text/javascript">
function UpdateSupervisor(id)
{
	var truck_no = $('#tno_'+id).val();
	var supervisor = $('#supervisor_'+id).val();
	var model = $('#model_'+id).val();

	if(truck_no=='')
	{
		Swal.fire({icon: 'warning',html: '<font size=\'2\' color=\'black\'>Vehicle not found !</font>',});
	}
	else if(id=='')
	{
		Swal.fire({icon: 'warning',html: '<font size=\'2\' color=\'black\'>Data not found !</font>',});
	}
	else
	{
		$('#supervisor_modal_btn')[0].click();
		$('.modal_tno').html(truck_no);
		$('.modal_tno').val(truck_no);
		$('.vehicle_id').val(id);
		
		if(supervisor==0){
			var supervisor = "63";
		}
		
		$('#supervisor_id').val(supervisor);
		$('#supervisor_id').selectpicker('refresh');
	}
}

function UpdateSALARY(id)
{
	var driver_code = $('#driver_code_'+id).val();
	var tno = $('#tno_'+id).val();
	var model = $('#model_'+id).val();
	
	$("#loadicon").show();
		jQuery.ajax({
		url: "./get_salary_pattern.php",
		data: 'model=' + model + '&tno=' + tno + '&driver_code=' + driver_code,
		type: "POST",
		success: function(data) {
			$("#salary_select").html(data);
		},
		error: function() {}
	});
}

function UpdateModel(id)
{
	var truck_no = $('#tno_'+id).val();
	var supervisor = $('#supervisor_'+id).val();
	var model = $('#model_'+id).val();
	
	if(truck_no=='')
	{
		Swal.fire({icon: 'warning',html: '<font size=\'2\' color=\'black\'>Vehicle not found !</font>',});
	}
	else if(id=='')
	{
		Swal.fire({icon: 'warning',html: '<font size=\'2\' color=\'black\'>Data not found !</font>',});
	}
	else
	{
		$('#model_update_modal_btn')[0].click();
		$('.modal_tno').html(truck_no);
		$('.modal_tno').val(truck_no);
		$('.vehicle_id').val(id);
		$('#model_name').val(model);
		$('#model_name').selectpicker('refresh');
	}
}

function MarkAsSold(id)
{
	var truck_no = $('#tno_'+id).val();
	var supervisor = $('#supervisor_'+id).val();
	var model = $('#model_'+id).val();

	if(truck_no=='')
	{
		Swal.fire({icon: 'warning',html: '<font size=\'2\' color=\'black\'>Vehicle not found !</font>',});
	}
	else if(id=='')
	{
		Swal.fire({icon: 'warning',html: '<font size=\'2\' color=\'black\'>Data not found !</font>',});
	}
	else
	{
		Swal.fire({
		  title: 'Are you sure ??',
		  text: "Mark Vehicle: "+truck_no+" as sold ?",
		  icon: 'warning',
		  showCancelButton: true,
		  confirmButtonColor: '#3085d6',
		  cancelButtonColor: '#d33',
		  confirmButtonText: 'Yes, i Confirm !'
		}).then((result) => {
		  if (result.isConfirmed) {
				$("#loadicon").show();
				jQuery.ajax({
				url: "./save_mark_as_sold.php",
				data: 'truck_no=' + truck_no + '&id=' + id,
				type: "POST",
				success: function(data) {
					$("#func_result2").html(data);
				},
				error: function() {}
			});
		  }
		})
	}
}

function DownDriver(id)
{
	var truck_no = $('#tno_'+id).val();
	
	Swal.fire({
		  title: 'Are you sure ??',
		  text: "Down Driver of : "+truck_no+" ?",
		  icon: 'warning',
		  showCancelButton: true,
		  confirmButtonColor: '#3085d6',
		  cancelButtonColor: '#d33',
		  confirmButtonText: 'Yes, i Confirm !'
		}).then((result) => {
		  if (result.isConfirmed) {
				$("#loadicon").show();
				jQuery.ajax({
				url: "./save_down_driver.php",
				data: 'truck_no=' + truck_no + '&id=' + id,
				type: "POST",
				success: function(data) {
					$("#func_result2").html(data);
				},
				error: function() {}
			});
		  }
	})
}

function LoadTable()
{
	// $("#loadicon").show();
	jQuery.ajax({
	url: "_load_all_vehicles.php",
	data: 'ok=' + 'ok',
	type: "POST",
	success: function(data) {
		$("#load_table_div").html(data);
		$('#example').DataTable({ 
        "destroy": true, //use for reinitialize datatable
    });
	},
	error: function() {}
	});
}
</script>

<script>
LoadTable();
</script>

<div id="func_result2"></div>

<?php
include "footer.php";
?>

<button id="model_update_salary_btn" style="display:none"  data-toggle="modal" data-target="#SALARYUpdateModel"></button>

<form id="Form22" autocomplete="off" style="font-size:13px">   
  <div class="modal fade" id="SALARYUpdateModel" role="dialog" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-md">
      <div class="modal-content" style="">
		<div class="modal-header bg-primary">
			<span style="font-size:13px">Update SALARY : <span class="modal_tno"></span></span>
		</div>
	<div class="modal-body">
		<div class="row">
			
			<div class="form-group col-md-12">
				<label>Truck No. <sup><font color="red">*</font></sup></label>
				<input type="text" name="truck_no" class="modal_tno form-control" readonly required />
			</div>
			 
			<div class="form-group col-md-12">
				<label>Select SALARY <sup><font color="red">*</font></sup></label>
				<select  style="font-size:12px" name="salary" id="salary_select" class="form-control" required="required">
					<option style="font-size:12px" value="">--Select Salary--</option>
				</select>
			</div>
			
			<input type="hidden" id="driver_code_model" name="driver_code">
		</div>
        </div> 
        <div class="modal-footer">
			<button type="submit" id="submit_btn_salary_update" class="btn btn-sm btn-danger">Confirm update</button>
			<button type="button" id="sal_update_modal_cls_btn" class="btn btn-sm btn-primary" data-dismiss="modal">Close</button>
		</div>
      </div>
    </div>
 </div>
</form> 

<button id="supervisor_modal_btn" style="display:none"  data-toggle="modal" data-target="#SupervisorModel"></button>

<form id="UpdateSupervisor" autocomplete="off" style="font-size:13px">   
  <div class="modal fade" id="SupervisorModel" role="dialog" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-md">
      <div class="modal-content" style="">
		<div class="modal-header bg-primary">
			<span style="font-size:13px">Update Supervisor : <span class="modal_tno"></span></span>
		</div>
	<div class="modal-body">
		<div class="row">
			
			<div class="form-group col-md-12">
				<label>Truck No. <sup><font color="red">*</font></sup></label>
				<input type="text" name="truck_no" class="modal_tno form-control" readonly required />
			</div>
			 
			<div class="form-group col-md-12">
				<label>Select Supervisor <sup><font color="red">*</font></sup></label>
				<select data-size="5" style="font-size:12px" name="supervisor" id="supervisor_id" class="form-control selectpicker" data-live-search="true" required="required">
					<option style="font-size:12px" data-tokens="" value="">--Select Supervisor--</option>
					<?php
					$get_data = Qry($conn,"SELECT id,title FROM dairy.user WHERE role='1' AND type='1'");
					
					if(!$get_data){
						errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
						Redirect("Error while Processing Request.","./");
					}
					
					if(numRows($get_data)>0)
					{
						while($row = fetchArray($get_data))
						{
							echo "<option data-tokens='$row[id]' style='font-size:12px' value='$row[id]'>$row[title]</option>";
						}
					}
					?>
				</select>
			</div>
			
			<input type="hidden" class="vehicle_id" name="vehicle_id">
		</div>
        </div> 
        <div class="modal-footer">
			<button type="submit" id="submit_btn_supervisor" class="btn btn-sm btn-danger">Confirm update</button>
			<button type="button" class="btn btn-sm btn-primary" data-dismiss="modal">Close</button>
		</div>
      </div>
    </div>
 </div>
</form> 

<button id="model_update_modal_btn" style="display:none"  data-toggle="modal" data-target="#ModelUpdateModal"></button>

<form id="UpdateModelForm" autocomplete="off" style="font-size:13px">   
  <div class="modal fade" id="ModelUpdateModal" role="dialog" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-md">
      <div class="modal-content" style="">
		<div class="modal-header bg-primary">
			<span style="font-size:13px">Update Model : <span class="modal_tno"></span></span>
		</div>
	<div class="modal-body">
		<div class="row">
			
			<div class="form-group col-md-12">
				<label>Truck No. <sup><font color="red">*</font></sup></label>
				<input type="text" name="truck_no" class="modal_tno form-control" readonly required />
			</div>
			
			<div class="form-group col-md-12"> 
				<label>Select Model <sup><font color="red">*</font></sup></label>
				<select data-size="5" style="font-size:12px" id="model_name" name="model_name" class="form-control selectpicker" data-live-search="true" required="required">
					<option style="font-size:12px" data-tokens="" value="">--Select Model--</option>
					<?php
					$get_model = Qry($conn,"SELECT model FROM dairy.model_list");
					
					if(!$get_model){
						errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
						Redirect("Error while Processing Request.","./");
					}
					
					if(numRows($get_model)>0)
					{
						while($row_get_model = fetchArray($get_model))
						{
							echo "<option data-tokens='$row_get_model[model]' style='font-size:12px' value='$row_get_model[model]'>$row_get_model[model]</option>";
						}
					}
					?>
				</select>
			</div>
			
			<input type="hidden" class="vehicle_id" name="vehicle_id">
			
		</div>
        </div> 
        <div class="modal-footer">
			<button type="submit" id="submit_btn_model_update" class="btn btn-sm btn-danger">Confirm update</button>
			<button type="button" class="btn btn-sm btn-primary" data-dismiss="modal">Close</button>
		</div>
      </div>
      </form>
    </div>
  </div>