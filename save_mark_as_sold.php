<?php
require_once("connect.php");

$timestamp = date("Y-m-d H:i:s");

$truck_no = escapeString($conn,strtoupper($_POST['truck_no']));
$id = escapeString($conn,$_POST['id']);

echo "<script>$('#btn_mark_sold_$id').attr('disabled',true);</script>";

if($truck_no=='')
{
	AlertError("Enter vehicle number first !");
	echo "<script>$('#btn_mark_sold_$id').attr('disabled',false);$('#loadicon').fadeOut('slow');</script>";
	exit();
}

if($id=='')
{
	AlertError("Data not found !");
	echo "<script>$('#btn_mark_sold_$id').attr('disabled',false);$('#loadicon').fadeOut('slow');</script>";
	exit();
}

$get_vehicle = Qry($conn,"SELECT tno,driver_code,happay_active FROM dairy.own_truck WHERE id='$id'");
	
if(!$get_vehicle){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	AlertError("Erorr !");
	echo "<script>$('#btn_mark_sold_$id').attr('disabled',false);$('#loadicon').fadeOut('slow');</script>";
	exit();
}

if(numRows($get_vehicle)==0)
{
	AlertError("Vehicle not found !");
	echo "<script>$('#btn_mark_sold_$id').attr('disabled',false);$('#loadicon').fadeOut('slow');</script>";
	exit();
}

$row_vehicle = fetchArray($get_vehicle);

if($row_vehicle['tno'] != $truck_no)
{
	AlertError("Vehicle not verified !");
	echo "<script>$('#btn_mark_sold_$id').attr('disabled',false);$('#loadicon').fadeOut('slow');</script>";
	exit();
}

if($row_vehicle['driver_code']!=0)
{
	AlertError("Driver active. Down driver first !");
	echo "<script>$('#btn_mark_sold_$id').attr('disabled',false);$('#loadicon').fadeOut('slow');</script>";
	exit();
}

if($row_vehicle['happay_active']!=0)
{
	AlertError("Deactivate happay-card first !");
	echo "<script>$('#btn_mark_sold_$id').attr('disabled',false);$('#loadicon').fadeOut('slow');</script>";
	exit();
}

StartCommit($conn);
$flag = true;
	
$mark_sold = Qry($conn,"UPDATE dairy.own_truck SET is_sold='1' WHERE id='$id'");
	
if(!$mark_sold){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

$insert_log = Qry($conn,"INSERT INTO dairy.ediary_admin_log(code,tno,action,desct,timestamp) VALUES ('$id','$truck_no','MARK_AS_SOLD',
'Vehicle mark as sold.','$timestamp')");
	
if(!$insert_log){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

if($flag)
{
	MySQLCommit($conn);
	closeConnection($conn);
	echo "<script>
		$('#btn_mark_sold_$id').attr('onclick','');
		$('#loadicon').fadeOut('slow');
	</script>";
	AlertRightCornerSuccessFadeFast("Update Success !");
}
else
{
	MySQLRollBack($conn);
	closeConnection($conn);
	AlertError("Error While Processing Request !");
	echo "<script>$('#btn_mark_sold_$id').attr('disabled',false);$('#loadicon').fadeOut('slow');</script>";
	exit();
}
?>