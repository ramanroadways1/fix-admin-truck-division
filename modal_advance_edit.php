<?php
require_once("connect.php");

$id = escapeString($conn,$_POST['id']);
$txn_type = escapeString($conn,$_POST['txn_type']);

if($txn_type !='CASH' AND $txn_type != 'RTGS')
{
	AlertErrorTopRight("Invalid Txn type !");
	exit();
}

if($type=='CASH')
{
	$trans_table="cash";
}
else if($type=='CHQ')
{
	$trans_table="cheque";
}
else if($type=='RTGS')
{
	$trans_table="rtgs";
}

$get_data = Qry($conn,"SELECT trip_id,trans_id,vou_no,tno,amount,date,branch,narration,timestamp FROM dairy.`$trans_table` WHERE id='$id'");

if(!$get_data)
{
	AlertErrorTopRight("Error while processing request !");
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	exit();
}

if(numRows($get_data) == 0)
{
	AlertErrorTopRight("Entry not found !");
	exit();
}

$row = fetchArray($get_data);

$tno = $row['tno'];
$trip_id = $row['trip_id'];
$trans_id = $row['trans_id'];
$amount = $row['amount'];
$vou_no = $row['vou_no'];
$trans_date = $row['date'];
$narration = $row['narration'];

$chk_trip = Qry($conn,"SELECT id FROM dairy.trip WHERE id='$trip_id'");

if(!$chk_trip)
{
	AlertErrorTopRight("Error while processing request !");
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	exit();
}

if(numRows($chk_trip)==0)
{
	AlertErrorTopRight("Running trip not found !");
	exit();
}
?>

<button id="modal_adv_button" style="display:none" data-toggle="modal" data-target="#AdvanceEditModal"></button>

<form id="EditAdvanceForm" autocomplete="off" style="font-size:12px !important">   
<div class="modal fade" id="AdvanceEditModal" role="dialog" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-md">
      <div class="modal-content" style="">
	  
	  <div class="modal-header bg-primary">
			Edit Advance : 
		  </div>
		  
		<div class="modal-body">
		<div class="row">
			
			<div class="form-group col-md-4">
				<label>Vehicle No. <sup><font color="red">*</font></sup></label>
				<input style="font-size:12px !important" type="text" name="tno" value="<?php echo $tno; ?>" class="form-control" readonly required />
			</div>
			
			<div class="form-group col-md-4">
				<label>Transaction Type. <sup><font color="red">*</font></sup></label>
				<input style="font-size:12px !important" type="text" name="trans_type" value="<?php echo $txn_type; ?>" class="form-control" readonly required />
			</div>
			
			<div class="form-group col-md-4">
				<label>Amount. <sup><font color="red">*</font></sup></label>
				<input style="font-size:12px !important" type="number" min="1" name="amount" value="<?php echo $amount; ?>" class="form-control" required />
			</div>
			
			<input type="hidden" name="id" value="<?php echo $id; ?>" />
			<input type="hidden" name="txn_type" value="<?php echo $txn_type; ?>" />
			
			<div class="form-group col-md-12">
				<label>Narration. <sup><font color="red">*</font></sup></label>
				<textarea style="font-size:12px !important" class="form-control" name="narration" required><?php echo $narration; ?></textarea>
			</div>
		</div>
        </div>
        <div class="modal-footer">
          <button type="submit" id="save_edit_adv_modal_btn" class="btn btn-sm btn-danger">Update</button>
          <button type="button" class="btn-sm btn btn-primary" onclick="ReLoadPage();" id="hide_modal_adv" data-dismiss="modal">Close</button>
		</div>
      </div>
      </form>
    </div>
  </div>
  
<script> 
$('#modal_adv_button')[0].click();
$('#loadicon').fadeOut('slow');
</script> 

<script type="text/javascript">
$(document).ready(function (e) {
$("#EditAdvanceForm").on('submit',(function(e) {
$("#loadicon").show();
$("#save_edit_adv_modal_btn").attr("disabled", true);
e.preventDefault();
	$.ajax({
	url: "./save_advance_edit.php",
	type: "POST",
	data:  new FormData(this),
	contentType: false,
	cache: false,
	processData:false,
	success: function(data){
		$("#result_edits").html(data);
	},
	error: function() 
	{} });}));});
</script>