<?php
require_once("./connect.php");

$id = escapeString($conn,($_POST['id']));
$expense = escapeString($conn,($_POST['expense']));
$trans_date = escapeString($conn,($_POST['trans_date']));
$amount = escapeString($conn,($_POST['amount']));
$narration = escapeString($conn,strtoupper($_POST['narration']));

$get_data = Qry($conn,"SELECT id,trip_id,trans_id,exp_name,exp_code,copy,tno,amount,date,branch,narration,timestamp 
FROM dairy.trip_exp WHERE id='$id'");

if(!$get_data)
{
	AlertErrorTopRight("Error while processing request !");
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	echo "<script> $('#edit_exp_button').attr('disabled',false); </script>";
	exit();
}

if(numRows($get_data) == 0)
{
	AlertErrorTopRight("Entry not found !");
	echo "<script> $('#edit_exp_button').attr('disabled',false); </script>";
	exit();
}

$row_db = fetchArray($get_data);

$table_id = $row_db['id'];
$tno = $row_db['tno'];
$trip_id = $row_db['trip_id'];
$trans_id_db = $row_db['trans_id'];
$exp_name_db = $row_db['exp_name'];
$exp_code_db = $row_db['exp_code'];
$amount_db = $row_db['amount'];
$trans_date_db = $row_db['date'];
$narration_db = $row_db['narration'];
$exp_attachment = $row_db['copy'];

if($exp_attachment != '')
{
	if($amount == $amount_db AND $narration == $narration_db AND $trans_date == $trans_date_db)
	{
		AlertErrorTopRight("Nothing to update !");
		echo "<script> $('#edit_exp_button').attr('disabled',false); </script>";
		exit();
	}
	
	$exp_name = $exp_name_db;
	$exp_code = $exp_code_db;
}
else
{
	$get_expense = Qry($conn,"SELECT exp_code,name FROM dairy.exp_head WHERE id='$expense'");

	if(!$get_expense)
	{
		AlertErrorTopRight("Error while processing request !");
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		echo "<script> $('#edit_exp_button').attr('disabled',false); </script>";
		exit();
	}

	if(numRows($get_expense)==0)
	{
		AlertErrorTopRight("Expense not found !");
		echo "<script> $('#edit_exp_button').attr('disabled',false); </script>";
		exit();
	}

	$row_get_exp = fetchArray($get_expense);

	$exp_name = $row_get_exp['name'];
	$exp_code = $row_get_exp['exp_code'];

	if($amount == $amount_db AND $narration == $narration_db AND $trans_date == $trans_date_db AND $exp_code_db == $exp_code AND $exp_name_db == $exp_name)
	{
		AlertErrorTopRight("Nothing to update !");
		echo "<script> $('#edit_exp_button').attr('disabled',false); </script>";
		exit();
	}
}

require_once("./check_cache.php");

$exp_desct = "EXP-".$exp_name;

$check_trip = Qry($conn,"SELECT toll_tax FROM dairy.trip WHERE id='$trip_id'");

if(!$check_trip)
{
	AlertErrorTopRight("Error while processing request !");
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	echo "<script> $('#edit_exp_button').attr('disabled',false); </script>";
	exit();
}

if(numRows($check_trip)==0)
{
	AlertErrorTopRight("Running trip not found !");
	echo "<script> $('#edit_exp_button').attr('disabled',false); </script>";
	exit();
}

$row_chk_trip = fetchArray($check_trip);

$find_txn_db = Qry($conn,"SELECT id,driver_code,tno FROM dairy.driver_book WHERE trans_id='$trans_id_db'");

if(!$find_txn_db)
{
	AlertErrorTopRight("Error while processing request !");
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	echo "<script> $('#edit_exp_button').attr('disabled',false); </script>";
	exit();
}

if(numRows($find_txn_db)==0)
{
	AlertErrorTopRight("Txn not found in driver-book !");
	echo "<script> $('#edit_exp_button').attr('disabled',false); </script>";
	exit();
}

$row_txn_db = fetchArray($find_txn_db);

$tno_db = $row_txn_db['tno'];
$driver_book_id = $row_txn_db['id'];
$driver_code = $row_txn_db['driver_code'];

if($tno_db != $tno)
{
	AlertErrorTopRight("Vehicle not verified !");
	echo "<script> $('#edit_exp_button').attr('disabled',false); </script>";
	exit();
}

$driver_book_id_prev = $driver_book_id - 1;

$fetch_d_bal = Qry($conn,"SELECT id,tno FROM dairy.driver_up WHERE down=0 AND code='$driver_code' ORDER BY id DESC LIMIT 1");

if(!$fetch_d_bal)
{
	AlertErrorTopRight("Error while processing request !");
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	echo "<script> $('#edit_exp_button').attr('disabled',false); </script>";
	exit();
}

if(numRows($fetch_d_bal)==0)
{
	AlertErrorTopRight("Drive balance not found !");
	echo "<script> $('#edit_exp_button').attr('disabled',false); </script>";
	exit();
}

$row_d_bal = fetchArray($fetch_d_bal);

$driver_up_id = $row_d_bal['id'];

if($row_d_bal['tno'] != $tno)
{
	AlertErrorTopRight("Vehicle not verified. Code - 002 !");
	echo "<script> $('#edit_exp_button').attr('disabled',false); </script>";
	exit();
}

if(count($_FILES['rto_upload']['name']) > 0)
{
	$valid_types = array("image/jpg", "image/jpeg", "image/bmp", "image/gif","image/png");
	
	foreach($_FILES['rto_upload']['type'] as $file_types)
	{
		if(!in_array($file_types, $valid_types))
		{
			AlertErrorTopRight("Only Image Upload Allowed !");
			echo "<script> $('#edit_exp_button').attr('disabled',false); </script>";
			exit();
		}
	}
	
	$upload_file = "1";
}
else
{		
	$upload_file = "0";
}

if($amount != $amount_db)
{
	$amount_diff = $amount - $amount_db;
}
else
{
	$amount_diff = 0;
}

StartCommit($conn);
$flag = true;

if($upload_file == "1")
{
	for($i=0; $i<count($_FILES['rto_upload']['name']); $i++)
	{
		$sourcePath = $_FILES['rto_upload']['tmp_name'][$i];

        $fix_name = $trip_id."-".date('dmYHis').$i;	
		
		$shortname = "rto_receipt/".$fix_name.".".pathinfo($_FILES['rto_upload']['name'][$i],PATHINFO_EXTENSION);
		$targetPath = "../diary/rto_receipt/".$fix_name.".".pathinfo($_FILES['rto_upload']['name'][$i],PATHINFO_EXTENSION);

		ImageUpload(1000,1000,$sourcePath);
				
		if(move_uploaded_file($sourcePath, $targetPath))
		{
			$files[] = $shortname;
		}
		else
		{
			$flag = false;
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		}
	}
		
	$file_name = implode(',',$files);
}
else
{
	$file_name = "";
}

$update_driver_book = Qry($conn,"UPDATE dairy.driver_book SET desct='$exp_desct',debit='$amount' WHERE id='$driver_book_id'");

if(!$update_driver_book)
{
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

$update_driver_book_next = Qry($conn,"UPDATE dairy.driver_book SET balance=(balance-($amount_diff)) WHERE id>'$driver_book_id_prev' AND tno='$tno'");

if(!$update_driver_book_next)
{
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

if($upload_file=='1')
{
	$update_exp = Qry($conn,"UPDATE dairy.trip_exp SET exp_name='$exp_name',exp_code='$exp_code',amount='$amount',copy='$file_name',
	date='$trans_date',narration='$narration' WHERE id='$table_id'");
}
else
{
	$update_exp = Qry($conn,"UPDATE dairy.trip_exp SET exp_name='$exp_name',exp_code='$exp_code',amount='$amount',
	date='$trans_date',narration='$narration' WHERE id='$table_id'");
}

if(!$update_exp)
{
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

$get_toll_tax = Qry($conn,"SELECT SUM(amount) as toll_tax FROM dairy.trip_exp WHERE trip_exp='$trip_id' AND exp_code='TR00015'");

if(!$get_toll_tax)
{
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

if(numRows($get_toll_tax) > 0)
{
	$row_toll_tax = fetchArray($get_toll_tax);
	$toll_tax_amount = $row_toll_tax['toll_tax'];
}
else
{
	$toll_tax_amount = 0;
}

$update_trip = Qry($conn,"UPDATE dairy.trip SET expense=(expense+($amount_diff)),toll_tax='$toll_tax_amount' WHERE id='$trip_id'");

if(!$update_trip)
{
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}	

$update_driver_balance = Qry($conn,"UPDATE dairy.driver_up SET amount_hold=(amount_hold-($amount_diff)) WHERE id='$driver_up_id'");

if(!$update_driver_balance)
{
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

$log_data = "Previous Values: ExpName=> $exp_name_db,ExpCode=> $exp_code_db,Amount=> $amount_db. New Values: ExpName=> $exp_name,ExpCode=> $exp_code,Amount=> $amount.";

$insert_log = Qry($conn,"INSERT INTO dairy.ediary_admin_log(code,action,desct,timestamp) VALUES ('$table_id','EDIT_EXP','$log_data','$timestamp')");	
	
if(!$insert_log)
{
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

if($flag)
{
	MySQLCommit($conn);
	closeConnection($conn);
	
	AlertRightCornerSuccess("Updated Successfully !");
	echo "<script> $('#edit_exp_button').attr('disabled',false); </script>";
	exit();
}
else
{
	MySQLRollBack($conn);
	closeConnection($conn);
	
	if($upload_file=='1')
	{
		foreach(explode(",",$file_name) as $uploaded_files)
		{
			unlink($uploaded_files);
		}
	}
	
	AlertErrorTopRight("Error while processing request !");
	echo "<script> $('#edit_exp_button').attr('disabled',false); </script>";
	exit();
}
?>